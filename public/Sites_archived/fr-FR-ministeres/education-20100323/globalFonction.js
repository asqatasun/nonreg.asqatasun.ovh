function showPageList(id) { 
  var indexItem = document.getElementById(id).selectedIndex; 
  var redirection = document.getElementById(id).options[indexItem].value; 
  if (redirection!='||') {
    var aRedirection = redirection.split('||'); 
    if (aRedirection[1]!='_blank') {
      document.location.target='_self'; 
      document.location.href=aRedirection[0];
    } else {
      window.open(aRedirection[0],'_blank');
    }
  }
}