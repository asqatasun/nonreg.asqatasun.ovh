document.writeln("<div id=\"layer\" style=\"display:none; position: absolute; width: 790px; height: 532px; top: 20px; left: 50px; z-index: 11000;\">");

document.writeln("<div style=\"padding:15px;font-size:13px;border: 5px solid #690; margin: 0px;background-color: #FFFFFF; width: 790px; height: 490px; z-index: 11000;\"><a href=\"javascript:void(0);\" style=\"font-weight:bold; text-decoration: none; color: rgb(80, 80, 80); font-family: Arial; font-size: 11px;float:right;\" onclick=\"layerFadeOut();\">Fermer [X]</a>");
 
document.writeln("<h1 style=\"font-size:24px;color:#690;border-bottom:2px solid #690\">Ne partez pas sans avoir r&eacute;pondu &agrave; notre enqu&ecirc;te !</h1><p>Avant la fin de votre visite, cliquez sur le lien \"Donnez votre opinion\" qui appara&icirc;t en haut &agrave; droite de chacune de nos pages.</p><p>Vous pouvez aussi r&eacute;pondre d&egrave;s maintenant &agrave; l\'enqu&ecirc;te en cliquant sur l\'image ci-dessous :</p><p><a href=\"#\" onclick=\"javascript=bvatrack_connect()\"><img border=0 align=\"right\" src=\"http://agriculture.gouv.fr/portal_themes/agricms_site/backgrounds/medaillon.jpg\"></a></p><p style=\"clear:right\">R&eacute;pondre au questionnaire ne vous prendra pas plus de 5 mn.<br>Les r&eacute;ponses seront trait&eacute;es de fa&ccedil;on anonyme.<br>Le dernier num&eacute;ro d'alim'agri, le magazine du minist&egrave;re, vous sera offert en remerciement.</p>");

document.writeln("</div></div>");
 
 var layerWidth = 790;
 var layerHeight = 490;
 var layerPadding = 10;
 var layerStepSize = 20;
 var layerSpeed = 25;
 var layerXLeft = layerPadding - layerWidth - (layerStepSize*8);
 var layerXCurrent = layerXLeft;
 var layerFadeInterval;
 var layerScrollInterval;
 
 function layerInit() {
 if (screen.width > 790) {
 document.getElementById('layer').style.width = layerWidth + "px";
 document.getElementById('layer').style.height = layerHeight + "px";
 document.getElementById('layer').style.left = layerXLeft + "px";
 document.getElementById('layer').style.top = layerPadding + "px";
 window.setTimeout("layerFadeIn()", 3000);
 //window.setTimeout("layerFadeOut()", 9000);
 
 if (navigator.appName == "Netscape" && parseInt(navigator.appVersion) >= 5) {
 document.getElementById('layer').style.position = "fixed";
 } else {
 layerScrollFix();
 }
 }
 }
 
 
 function layerFadeIn() {
 document.getElementById('layer').style.display = "block";
 layerFadeInterval = window.setInterval("layerFadeInEx()", layerSpeed);
 }
 
 
 function layerFadeInEx() {
 layerXCurrent = layerXCurrent + layerStepSize;
 document.getElementById('layer').style.left = layerXCurrent + "px";
 
 if (layerXCurrent >= layerPadding) {
 window.clearInterval(layerFadeInterval);
 }
 }
 
 
 function layerFadeOut() {
 layerFadeInterval = window.setInterval("layerFadeOutEx()", layerSpeed);
 }
 
 
 function layerFadeOutEx() {
 layerXCurrent = layerXCurrent - (layerStepSize*2.5);
 document.getElementById('layer').style.left = layerXCurrent + "px";
 if (layerXCurrent <= layerXLeft) {
 document.getElementById('layer').style.display = "none";
 window.clearInterval(layerFadeInterval);
 window.clearInterval(layerScrollInterval);
 }
 }
 
 
 function layerScrollFix() {
 layerScrollInterval = window.setInterval("layerScrollFixEx()", 1);
 }
 
 
 function layerScrollFixEx() {
 if (layerGetScrollPosition() != (document.getElementById('layer').offsetTop - layerPadding)) {
 document.getElementById('layer').style.top = layerGetScrollPosition() + layerPadding + "px";
 }
 }
 
 
 function layerGetScrollPosition() {
 if (typeof window.pageYOffset != 'undefined') {
 return window.pageYOffset;
 } else if (typeof document.compatMode != 'undefined' && document.compatMode != 'BackCompat') {
 return document.documentElement.scrollTop;
 } else if (typeof document.body != 'undefined') {
 return document.body.scrollTop;
 }
 }