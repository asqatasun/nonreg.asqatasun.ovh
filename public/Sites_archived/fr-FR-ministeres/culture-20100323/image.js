$(document).ready(function(){
	
	var ezimage = {
		crop:null,
		ratio:0,
		loading:'',
		alias_width:0,
		alias_height:0,
		min_alias_width:0,
		min_alias_height:0,
		tab_list: new Array(),
		init:function() {
			
			// init loading html
		
			this.loading = $('div.ajax-loading').html() ;
		
			// init tabs
		
			$('ul.image-filter-tab').tabs({remote: false}) ;
			
			// handle forms

		    $('a.image-filter-submit').click(function() {

		        var container = $(this).parents('div.image-container') ;
		        var filter = container.find('div.image-filter') ;
		        var tab = container.find('div.ui-tabs-panel:visible') ;

		        var value = '' ;
		        var href = $(this).attr('href') ;

		        tab.find('.image-field').each(function(i) {

		        	if (i > 0)
		            {
		                value += '&' ;
		            }

		            value += $(this).attr('name') + '=' + $(this).attr('value') ;

		        }) ;

		        filter.html(ezimage.loading) ;

		        $.ajax({
		            type: 'POST',
		            data: value,
		            url: href,
		            success: function(result){
		                filter.html(result);
		                
		                ezimage.init_filter() ;
		            },
		            error: function(){
		                filter.html('');
		                
		                ezimage.init_filter() ;
		            },
		            cache:false
		        });

		        return false ;
		    }) ;
		    
		    // handle color picker
		    
		    $('input.image-colorpicker').each(function(){
		    	var input = $(this) ;
		    	
		    	input.ColorPicker({
		        	color: '#000000',
		        	onChange: function (hsb, hex, rgb) {
		        		input.css('background', '#' + hex);
		        		input.attr('value', hex);
		        	}
		    	}) ;
		    }) ;
			
			// handle crop select
			
			$('select.image-alias').change(function() {

		        var preview = $(this).parents('div.image-container') ;
		        var id = preview.attr('id') ;
		        var tab = id + '-preview-tab' ;

		        var value = $(this).attr('value') ;

		        if (value.length)
		        {
		            ezimage.tab_list[tab].tabs('select', ezimage.tab_list[tab].tabs('length')-1) ;

		            var alias = value.split('x') ;

		            ezimage.alias_width = parseInt(alias[0], 10) ;
		            ezimage.alias_height = parseInt(alias[1], 10) ;

		            ezimage.select('#' + id) ;
		        }

		    }) ;
			
			// load preview
			
			this.load_preview() ;
			
			// load filter
			
			this.load_filter() ;
		},
		select:function(id) {
			
			var tab = $(id + '-preview').find('div.image-preview-tab-panel:visible') ;

			var preview = tab.find('img.image-preview') ;

			var preview_width =  preview.attr('width') ;
			var preview_height = preview.attr('height') ;
			var original_width = tab.find('.image-preview-width').text() ;
			var original_height = tab.find('.image-preview-height').text() ;

			if (original_width < ezimage.alias_width || original_height < ezimage.alias_height)
			{
				return ;
			}
			
			if (this.crop)
			{
				this.crop.setOptions({
					minSize: [ this.alias_width, this.alias_height ],
					aspectRatio: this.alias_width/this.alias_height
				}) ;
			}
	        else
	        {
	            this.crop = $.Jcrop(
	                '#' + preview.attr('id'), {
	                    onChange: function () {

	                        var scaled_coord = ezimage.crop.tellScaled() ;
	
	                        if (scaled_coord.w < ezimage.min_alias_width)
	                        {
	                        	ezimage.crop.setSelect(
	                        		[ scaled_coord.x, scaled_coord.y, scaled_coord.x + ezimage.min_alias_width, scaled_coord.y + ezimage.min_alias_height ]	
	                        	) ;
	                        }
	                        else
	                        {
		                        var true_coord = ezimage.crop.tellSelect() ;
		                        
		                        $(id + '-selection-left').attr('value', true_coord.x) ;
		                        $(id + '-selection-top').attr('value', true_coord.y) ;
		                        $(id + '-selection-width').attr('value', true_coord.w) ;
		                        $(id + '-selection-height').attr('value', true_coord.h) ;
	                        }
	                        
	                    },
	                    boxWidth: preview_width,
	                    boxHeight: preview_height,
	                    trueSize: [ original_width, original_height],
	                    aspectRatio: this.alias_width/this.alias_height
	            }) ;
	        }
	
	        this.ratio = preview_width/original_width ;
	
	        this.min_alias_width = Math.floor(this.alias_width * this.ratio) ;
	        this.min_alias_height = Math.floor(this.alias_height * this.ratio) ;
	
	        x1 = Math.round((preview_width - this.min_alias_width)/2) ;        
	        y1 = Math.round((preview_height - this.min_alias_height)/2) ;
	
	        x2 = x1 + this.min_alias_width ;
	        y2 = y1 + this.min_alias_height ;
	
	        this.crop.animateTo([ x1, y1, x2, y2 ]);
		},
		load_preview:function() {
			$('div.image-preview').each(function() {
				
		        var href = $(this).prev('a.image-preview').attr('href') ;
		        var container = $(this).parents('div.image-container') ;
		        var filter = $('#' + container.attr('id') + '-filter') ;
		        var preview = $(this) ;
		
		        preview.html(this.loading) ;
		
		        $.ajax({
		            type: 'POST',
		            url: href,
		            data: 'value=null',
		            success: function(result){
		        	
		        		preview.html(result);

	                    var tab = preview.find('ul.image-preview-tab') ;

	                    ezimage.tab_list[tab.attr('id')] = tab.tabs() ;

	                    $('a.image-preview-submit').click(function() {

	                    	if (!confirm($(this).attr('title')))
	                        {
	                            return false ;
	                        }
	                    	
	                    	preview.html(ezimage.loading) ;
	                    	filter.html(ezimage.loading) ;
	                    	
	                        var href = $(this).attr('href') ;

	                        $.ajax({
	                            type: 'POST',
	                            url: href,
	                            data: 'value=null',
	                            success: function(result){
	                        	
	                        		filter.html(result) ;
	                        	
	                        		ezimage.load_preview() ;
	                        		
	                            },
	                            error: function(){

	                            }
	                        }) ;

	                        return false ;
	                    }) ;
	                    
		            },
		            error: function(){
		                preview.html('');
		            },
		            cache:false
		        }) ;
		    }) ;
		},
		init_filter: function() {
	        $('a.image-filter-link').click(function(){
	            var href = $(this).attr('href') ;
	            var filter = $(this).parents('div.image-filter') ;

	            if ($(this).hasClass('image-confirm-submit'))
	            {
	                if (!confirm($(this).attr('title')))
	                {
	                    return false ;
	                }
	            }

	            $(filter).html(ezimage.loading) ;

	            $.ajax({
	                type: 'POST',
	                url: href,
	                data: 'value=null',
	                success: function(result){
	                    filter.html(result);
	                    
	                    ezimage.init_filter() ;
	                },
	                error: function(){
	                    filter.html('');
	                    
	                    ezimage.init_filter() ;
	                },
	                cache:false
	            }) ;

	            return false ;
	        }) ;
	        
	        $('a.image-filter-apply').click(function(){
	        	
	        	var href = $(this).attr('href') ;
	        	var filter = $(this).parents('div.image-filter') ;
	            
	        	$(this).parents('div.image-container').find('div.image-preview').html(ezimage.loading) ;
	        	
	        	filter.html(ezimage.loading) ;
	        	
	        	$.ajax({
	                type: 'POST',
	                url: href,
	                data: 'value=null',
	                success: function(result){

	        			ezimage.crop = null ;
	        			
	        			filter.html(result) ;
	        		
	        			ezimage.load_preview() ;
	                    
	                },
	                error: function(){
	                	ezimage.load_preview() ;
	                },
	                cache:false
	            }) ;
	        	
	        	return false ;
	        }) ;
	    },
		load_filter:function() {
		    $('div.image-filter').each(function(){
		
		        var href = $(this).prev('a.image-filter').attr('href') ;
		        var filter = this ;
		        
		        $(filter).html(ezimage.loading) ;
		        
		        $.ajax({
		            type: 'POST',
		            url: href,
		            data: 'value=null',
		            success: function(result){
		                $(filter).html(result);
		
		                ezimage.init_filter() ;
		            },
		            error: function(){
		                $(filter).html('');
		            },
		            cache:false
		        }) ;
		    }) ;
	    }
	} ;
	
	ezimage.init() ;
}) ;
