// JavaScript Document
function fixColumns(){
				
	var c1 = document.getElementById("colonneA");
	var c2 = document.getElementById("colonneB");
	var c3 = document.getElementById("colonneC");
	
	if(c1.clientHeight && c2.clientHeight && c3.clientHeight ){
		maxheight=Math.max(c1.clientHeight,c2.clientHeight,c3.clientHeight)+'px';
	}
	var max2 = Math.max(c1.offsetHeight,c2.offsetHeight,c3.offsetHeight)+'px';				
	c1.style.height = max2;
	c2.style.height = max2;	
	c3.style.height = 'auto';
}
 
 
// GETELEMENTSBYCLASSNAME
	function getElementsByClassName(oElm, strTagName, strClassName){
	    var arrElements = (strTagName == "*" && oElm.all)? oElm.all : oElm.getElementsByTagName(strTagName);
	    var arrReturnElements = new Array();
	    strClassName = strClassName.replace(/\-/g, "\\-");
	    var oRegExp = new RegExp("(^|\\s)" + strClassName + "(\\s|$)");
	    var oElement;
	    for(var i=0; i<arrElements.length; i++){
	        oElement = arrElements[i];      
	        if(oRegExp.test(oElement.className)){
	            arrReturnElements.push(oElement);
	        }   
	    }
	    return (arrReturnElements)
	}
// FIN GETELEMENTSBYCLASSNAME
/*******************************************************************************
		Afficher les bloc jeune psyco resultat 
********************************************************************************/		
	function article() {
		var getEls = getElementsByClassName(document, "ul", "last_article");
		for (i=0 ; i< getEls.length ; i++) 
		{
			var list = getEls[i].getElementsByTagName("li") ;
			
			for (j=0 ; j<list.length ; j++) {
				
				if( i!=0 && list[j].id && list[j].id != ("archive_navigator_year_" + current_year) ) {
					list[j].className = '';
				}
				
				var link_year = getElementsByClassName( list[j] , "a", "year");
				
				if (link_year[0]) { 
					link_year[0].onclick = function() {
						var UlList = this.getElementsByTagName("ul");
						
						if (this.parentNode.className == 'open') {
							this.parentNode.className = '' ;
						}
						else {
							this.parentNode.className = 'open';
						}
						
						var c1 = document.getElementById("colonneA");
						var c2 = document.getElementById("colonneB");
						var c3 = document.getElementById("colonneC");
						
						c1.style.height="auto";
						c2.style.height="auto";
						c3.style.height="auto";
						
						fixColumns();
						
						return false;
					}
				}
			}
		}
	}


/*******************************************************************************
Afficher le bloc Themes sur les rubriques accueil
********************************************************************************/		

/* Fonction qui sert a afficher / masquer le bloc des themes en haut d'un contenu de type rubrique accueil */
function blocthemesaction( state , nomcookie, blocjquery )
{
  if (state == 'show')
  {
    $('#liste_theme').show();
    blocjquery.text(consultthemestext2);
    blocjquery.removeClass("liste_theme_plie");
    blocjquery.addClass("liste_theme_deplie");
    $.cookie( nomcookie, 'show');
  }
  else
  {
    $('#liste_theme').hide();
    blocjquery.text(consultthemestext);
    blocjquery.removeClass("liste_theme_deplie");
    blocjquery.addClass("liste_theme_plie");
    $.cookie( nomcookie, 'hide');
  }

}

/* Fonction lancee au chargement du site pour gerer les blocs themes des contenus de type rubrique accueil */
$(function(){
      var nomcookie = 'rubracc';
      var cookvalue = $.cookie( nomcookie );

      // Si pas de cookie, on en cree un a la volee
      if ( cookvalue  == null)
      {
        $.cookie( nomcookie, 'show', { expires: 7 });
      }

      // On reprend les donnees du cookie
      cookvalue = $.cookie(nomcookie);

      // au charegement de la page, on s'en refere au cookie
      $('#rubthemesswitch').each(function(){
        blocthemesaction(cookvalue, nomcookie, $(this));
      });

      // Gestion de l'ouverture / fermeture classique au clique
      $('#rubthemesswitch').click(function(){
        if($('#liste_theme').css('display') == 'block'){
          blocthemesaction('hide', nomcookie, $(this));
        }
        else {
          blocthemesaction('show', nomcookie, $(this));
        }
    });
});

	




/*******************************************************************************
Afficher les blocs
********************************************************************************/	
function updateThickboxLinks() {
	$(".thickbox").each(function(i) {
		/* la presence de 'mcc' dans l'url n'est plus obligatoire pour que rewrite == OK
		   on remplace pathname (chemin vers contenu - commence sans '/' sous IE6) 
		   et non href qui peut contenir http://host.domain.tld 
	    */
		var target = $(this).attr( 'pathname' );
		var lSlash = ( target.indexOf( '/' ) == 0 );
		 
		target = target.replace( /^\// , '' ) ; 
        if ( target.match( /^mcc\// ) )
        {
            var newTarget = target.replace( /^mcc\//, "mcc/layout/set/thickbox/" );
        }
        else
        {
            var newTarget = "layout/set/thickbox/" + target;
        }
        newTarget = ( lSlash ) ? '/' + newTarget : newTarget;
        
		$(this).attr( 'pathname', newTarget );
	});
}



