/**
 * @author syban
 */
YAHOO.namespace("smile.calendar");

function smileCalendarHandleSelect(type,args,calendar) {
	var dates = args[0]; 
	var date = dates[0];
	var year = date[0], month = date[1], day = date[2];
	calendar.txtYear.value = year;
	calendar.txtMonth.value = month;
	calendar.txtDay.value = day;
	calendar.hide();
}

function smileCalendarUpdate(e,calendar) {
	if (calendar.txtMonth.value && calendar.txtDay.value && calendar.txtYear.value) {
		calendar.select(calendar.txtMonth.value + "/" + calendar.txtDay.value + "/" + calendar.txtYear.value);
		var firstDate = calendar.getSelectedDates()[0];
		calendar.cfg.setProperty("pagedate", (firstDate.getMonth()+1) + "/" + firstDate.getFullYear());
	}
	else {
		calendar.select(calendar.today);
		var firstDate = calendar.getSelectedDates()[0];
		calendar.cfg.setProperty("pagedate", (firstDate.getMonth()+1) + "/" + firstDate.getFullYear());
	}
	calendar.show();
	calendar.render();
}

