var playlist = new Array();
var previousMediaId = null;

if(typeof(top.players) != 'object')
	top.players = new Object();

function playerReady(thePlayer) {
	
	//console.log('GPRadio playerReady :: Player : ' + thePlayer.id + ' READY');
	
	top.players[thePlayer.id] = document.getElementById(thePlayer.id);
	addListeners(players[thePlayer.id]);
	
}
	




function addListeners(player) {
	
	if (player) {
		
		player.addControllerListener("ITEM", "itemListener");
		player.addControllerListener("STOP", "stopListener");
		player.addControllerListener("MUTE", "muteListener");

		player.addModelListener("LOADED", "loadedListener");
		//player.addModelListener("STATE", "stateListener");
		player.addModelListener("TIME", "positionListener");

		player.addViewListener("VOLUME", "volumeListener");
		
	} else {
		
		setTimeout(addListeners,100, player);
		
	}
}

function itemListener(obj) {
	
	currentMedia = playlist[obj.index];
	
	if(jQuery('#TB_iframeContent').size() > 0) {
		
		var iframe_name = jQuery('#TB_iframeContent').attr('name');
		var iframe_window = frames[iframe_name]
		var iframe_document = iframe_window.document;
		
		//make the top iframe document element a JQuery Object
		var context = jQuery(iframe_document.documentElement);
		
	}
	else {
		
		var context = window.document;
		
	}
	
	var previousLink = null;
	var currentLink = null;
	
	if(typeof(previousMedia) == 'object') {
		
		if(jQuery('#playTrack_' + previousMedia.mediaId, context).size() > 0) {
			
			var previousLink = jQuery('#playTrack_' + previousMedia.mediaId, context);
			
		}		
		
	}
	
	if(typeof(currentMedia) == 'object') {
		
		if(jQuery('#playTrack_' + currentMedia.mediaId, context).size() > 0) {
			
			var currentLink = jQuery('#playTrack_' + currentMedia.mediaId, context);
			
		}
		
	}
	
	if(currentLink) {
		
		currentLink.toggleClass('play-button');
		currentLink.toggleClass('stop-button');
		unbindPlayTrack(currentLink);
		
		var e = jQuery.Event("click");
		e.data = {media: currentMedia};
		
		bindPlayTrack(currentLink, e.data, stopTrackHandler);				
		
	}
	
	if(previousLink  && (previousMedia.mediaId != currentMedia.mediaId)) {
		
		previousLink.toggleClass('play-button');
		previousLink.toggleClass('stop-button');
		
		unbindPlayTrack(previousLink);
		
		var e = jQuery.Event("click");
		e.data = {media: previousMedia};
		
		bindPlayTrack(previousLink, e.data, playTrackHandler);
	
	}
	
	
	previousMedia = currentMedia;
	currentMedia = null;
	delete currentMedia;
	
	
}

function stopListener(obj) {
	
	if(jQuery('#TB_iframeContent').size() > 0) {
		
		var iframe_name = jQuery('#TB_iframeContent').attr('name');
		var iframe_window = frames[iframe_name]
		var iframe_document = iframe_window.document;
		
		//make the top iframe document element a JQuery Object
		var context = jQuery(iframe_document.documentElement);
		
	}
	else {
		
		var context = window.document;
		
	}
	
	var previousLink = null;
	
	if(typeof(previousMedia) == 'object') {
		
		if(jQuery('#playTrack_' + previousMedia.mediaId, context).size() > 0) {
			
			var previousLink = jQuery('#playTrack_' + previousMedia.mediaId, context);
			
		}		
		
	}
	
	if(previousLink !== null) {
		
		previousLink.toggleClass('play-button');
		previousLink.toggleClass('stop-button');
		
		unbindPlayTrack(previousLink);
		
		var e = jQuery.Event("click");
		e.data = {media: previousMedia};
		
		bindPlayTrack(previousLink, e.data, playTrackHandler);
	
	}
	
	previousMedia = null;
	delete previousMedia;
	
}

function stateListener(obj) { //IDLE, BUFFERING, PLAYING, PAUSED, COMPLETED
	
		
}

bindPlayTrack = function(jo, data, func) {
	
	jo.bind('click', data, func);

}

unbindPlayTrack = function(jo) {
	
	jo.unbind('click');

}

function playTrackHandler(e) { 
	
	e.preventDefault();
	
	if(e.data.media.isPublic) {
		
		playTrackByMediaId(e.data.media.mediaId, 1);
		
	}
	else {
		
		addToPlaylist(e.data.media);
		loadPlaylist(playlist);
		
		playTrackByMediaId(e.data.media.mediaId, e.data.media.isPublic);
		
	}
		

}

function stopTrackHandler(e) { 
	
	e.preventDefault();
	
	stopTrack();

}

function loadPlaylist(playlist) {
	
	if(players.gpRadioPlayer) {
		players.gpRadioPlayer.sendEvent('LOAD', playlist);
	}
	else
		setTimeout(function() {loadPlaylist(playlist); },100);
	
};

function addToPlaylist(media) {
	
	if(typeof(playlist) == 'undefined' || !playlist) {
		
		playlist = new Array();
		
	}
	
	if(getMedia(media.mediaId)) {
		
		return;
		
	}
	
	playlist.push(media);
	
};

function playTrack(file) {
	
	if(players.gpRadioPlayer) {
		
		players.gpRadioPlayer.sendEvent('STOP');
		players.gpRadioPlayer.sendEvent('LOAD', file + '.mp3');
		players.gpRadioPlayer.sendEvent('PLAY');
		
	}
	else {
		
		setTimeout(function() {playTrack(file)},100);
		
	}
	
}

function playPlaylistTrack(index, isPublic) {
	
	if(index === false) return;
	
	if(players.gpRadioPlayer) {
		players.gpRadioPlayer.sendEvent('ITEM', index);
	}
	else {
		setTimeout(function() {playPlaylistTrack(index);},100);
	}
		
	
};

function stopTrack() {
	
	if(players.gpRadioPlayer) {
		players.gpRadioPlayer.sendEvent('STOP');
	}
	
};

function playTrackByMediaId(mediaId, isPublic) {
	
	playPlaylistTrack(getMediaIndex(mediaId), isPublic);
	
};


function getMedia(mediaId) {
	
	for(var index = 0; index < playlist.length; index++) {
		
		if(playlist[index].mediaId == mediaId) {
			
			return playlist[index];
			
		}
		
	}
	
	return false;
		
};

function getMediaIndex(mediaId) {
	
	for(var index = 0; index < playlist.length; index++) {
		
		if(playlist[index].mediaId == mediaId) {
			
			return index;
			
		}
		
	}
	
	return false;
		
};
