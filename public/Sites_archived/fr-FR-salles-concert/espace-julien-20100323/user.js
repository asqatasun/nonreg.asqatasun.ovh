/* Définition des comportements javascript du site */
/* Activer les effets sur les formulaires  */
function setCSSBehaviours() {
	$$('.themes select').addEvent('change', function() { this.form.submit(); });
	$$('a.back, a.retour').addEvent('click', function(e) { history.back(); new Event(e).stop(); });
	$$('#nav-contenu .print').addEvent('click', function(e) { window.print(); new Event(e).stop(); });

	$$('.accordion').each(function (accordion) {
		var drags = accordion.getElements('.drag');
		var contents = accordion.getElements('.content');
		var options = { opacity: true };
		if (accordion.hasClass('hideall')) {
			options.alwaysHide = true;
			options.show = -1;
		}
		drags.each( function(e) { e.setStyle('cursor', 'pointer'); } );
		var acc = new Accordion(drags, contents, options, accordion);
	});
	buildTableAccordion();
	buildAjaxListForms();
	// Textarea editables
	editable_textareas = $$('div.html textarea');
	if (editable_textareas.length > 0) {
		var mooed = new MooEditable(editable_textareas);
	}
	// Séparateurs dans le texte
	var separateur = new Element('div', {'class': 'separateur'});
	$$('hr').each( function(hr) { hr.replaceWith(separateur.clone()); } )
}

window.addEvent('domready', setCSSBehaviours);
window.addEvent('load', function() {
	$$('#contenu img').each (function (img) {
		if (img.hasClass('noauto')) return;
		legende = '';
		// Vérifier si on n'est pas dans un <div> gauche droite ou centre
		done = false;
		$$('div.gauche, div.droite, div.centre').each( function(div) {
			if (!done) if (div.hasChild(img)) { done = true; }
		});
		if (done) return;
		// Mettre le ALT de l'image en légende dessous
		if (img.getProperty('alt') && !img.hasClass('nolegend')) {
			legende = new Element('span').setText(img.getProperty('alt'));
		}
		// Détecter les zooms
		a = img.getParent();
		if (a.getTag() == 'a') {
			if (!a.getProperty('rel') && a.href.match(/(jpg|png|gif)$/)) {
				a.setProperties({ 'rel': 'lightbox[]', 'title': img.getProperty('alt') });
			}
		} else { a = false; }
		// Mettre un conteneur div autour des images alignées
		var iclass, align;
		if (!img.getProperty('align') && img.getProperty('class')) {
			iclass = img.getProperty('class');
			img.removeProperty('class');
			img.removeProperty('align');
		} else if (img.getProperty('align')) {
			align = img.getProperty('align');
			if (align == 'left') { iclass = 'gauche'; }
			else if (align == 'right') { iclass = 'droite'; }
			else { iclass = 'centre'; }
			img.removeProperty('align');
		} else iclass = 'centre';
		div = new Element('div', { 'class': iclass });
		if (legende) legende.injectInside(div);
		if (a != false) {
			// On injecte le lien
			a.clone().injectTop(div);
			a.replaceWith(div);
		} else {
			// Pas de lien, on injecte l'image
			img.clone().injectTop(div);
			img.replaceWith(div);
		}
	});
	Lightbox.init();

	// Mettre la taille des div droite & gauche à celle de l'image...
	$$('div.gauche, div.droite, div.centre').each( function(div) {
		// Trouver l'image
		var img = $E('img', div);
		if (img) {
			img.addEvent('load', function(e) { if (this.width > 0) { div.setStyle('width', this.width+'px'); } });
			div.setStyle('text-align', 'center');
		}
	} );
	// Vérification des liens
	var uri = location.protocol+'//'+location.host;
	$$('#contenu a[href]').each( function(a) {
		var ext, infos, flength;
		if (a.hasClass('noauto')) return;
		if (a.getElement('img')) return;
		if (a.href.search(/(pdf|doc|rtf|xls|xla|ppt|pps|zip|rar|jpg|png|gif)$/) != -1) {
			ext = RegExp.$1;
			infos = ext.toUpperCase();
			a.addClass('icone fichier '+ext);
			flength = '';
			new XHR({ method: 'HEAD',
				async: true,
				onSuccess: function() {
					if (flength = this.transport.getResponseHeader('Content-Length')) {
						infos += ', '+(flength/1024).round()+' Ko';
					}
					new Element('span').setText(' ('+infos+')').injectInside(a);
					a.setProperty('title', a.title+' ('+infos+')');
				},
				onFailure: function() {
					a.addClass('broken');
					new Element('span').setText(' ('+infos+')').injectInside(a);
					a.setProperty('title', a.title+' ('+infos+')');
				}
			}).send(a.href);
		} else if (a.href.indexOf(uri) != 0) {
			//a.addClass('icone url');
			if (!a.getProperty('target')) a.setProperty('target', '_blank');
		}
	});
});



function buildAjaxListForms() {
	if ($$('form.liste').length > 0) {
		new Asset.images('images/wait.gif');
		$$('form.liste select').addEvent('change', function(e) { this.form.fireEvent('submit'); });
		$$('form.liste').addEvent('submit', function(e) {
			qs = this.toQueryString();
			$('results').setHTML('&nbsp;');
			new Element('img', { 'src': 'images/wait.gif', 'width': '100%', 'height': 19, 'vspace': 5}).injectInside('results');
			new Ajax(this.action, {
				'data': qs+'&envoi=1&tpl=contenu',
				'method': 'get',
				'update': 'contenu',
				'onComplete': function() { buildTableAccordion();buildAjaxListForms();  }
			}).request();
			if (e) new Event(e).stop();
		});
	}
}

function buildTableAccordion() {
	/* make sliding segments */
	if ($('tableaccordion')) {

		var slides = new Object();
		$$('#tableaccordion div.s').each(function (slide) {
			slide.setStyle('display', 'none');
			slides[slide.id] = new Fx.Slide(slide, { duration : 500 }).hide();
			// IE hack
		});
		$$('tr.t').each(function(tr) {
			tr.setStyle('cursor', 'pointer');
			tr.addEvent('click', function (e) {
				var slide = $(this.id.replace(/^t/, 's'));
				// IE hack
				slide.setStyle('display', 'block');
				if (slides[slide.id].open) {
					slides[slide.id].options.transition = Fx.Transitions['Back']['easeIn'];
				} else {
					slides[slide.id].options.transition = Fx.Transitions['Back']['easeOut'];
				}
				this.toggleClass('select');
				slides[slide.id].toggle();
			});
		} );
	}
}


