/*** Module init ***/
var idalgoCurrentPos = new Array();
var idalgoRefreshDelta = 60000;

window.onload=function () {
	setInterval("reloadContent()",idalgoRefreshDelta);
};
function reloadContent() {
	var idalgo_menu = document.getElementById("idalgo_menu");
	var idalgoScores   = idalgo_menu.getAttribute("totalscore");
	var refresh        = idalgo_menu.getAttribute("refresh");
	refresh -= idalgoRefreshDelta;
	if(refresh<=0) {
		loadModuleHtmlTarget('idalgo_content','ticker','championnat='+idalgoChampionnat+'&journee='+idalgoAffiche+'&totalscore='+idalgoScores);
	} else {
		idalgo_menu.setAttribute("refresh",refresh);
	}
}
function showCompetition(Championnat,live) { showCompetitionDay(Championnat,"current",live); }
function showCompetitionDay(Championnat,Affiche,live) {
	document.getElementById("button_idalgo_"+idalgoChampionnat+"_matchlist").className="champ_"+idalgoChampionnat+"-but_off"+idalgoLive;
	document.getElementById("idalgo_"+idalgoChampionnat+"_matchlist_"+idalgoAffiche).className="off";
	idalgoChampionnat = Championnat;
	idalgoAffiche     = Affiche;
	idalgoLive        = live;
	document.getElementById("button_idalgo_"+idalgoChampionnat+"_matchlist").className="champ_"+Championnat+"-but_on"+idalgoLive;
	document.getElementById("idalgo_"+idalgoChampionnat+"_matchlist_"+idalgoAffiche).className="matchlist "+Affiche+" on";
}
function idalgo_event_sound(sUrl){
	var sObject='';
	sObject+='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" width="1" height="1" id="iDalgoMp3">';
		sObject+='<param name=movie value="media/swf/iDalgoMp3.swf?sound='+sUrl+'">';
		sObject+='<param name=quality value=high>';
		sObject+='<param name=bgcolor value=#FFFFFF>';
		sObject+='<embed src="media/swf/iDalgoMp3.swf?sound='+sUrl+'" quality="high" bgcolor="#FFFFFF" width="1" height="1" name="iDalgoMp3" align="" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>';
	sObject+='</object>';
	var oDivMediaPlayer=document.getElementById('div_audio');
	oDivMediaPlayer.innerHTML=sObject;
}

/*** Module jsr_class ***/

// jsr_class.js
//
// JSONscriptRequest -- a simple class for making HTTP requests
// using dynamically generated script tags and JSON
//
// Author: Jason Levitt
// Date: December 7th, 2005
//
// A SECURITY WARNING FROM DOUGLAS CROCKFORD:
// "The dynamic <script> tag hack suffers from a problem. It allows a page 
// to access data from any server in the web, which is really useful. 
// Unfortunately, the data is returned in the form of a script. That script 
// can deliver the data, but it runs with the same authority as scripts on 
// the base page, so it is able to steal cookies or misuse the authorization 
// of the user with the server. A rogue script can do destructive things to 
// the relationship between the user and the base server."
//
// So, be extremely cautious in your use of this script.
//
//
// Sample Usage:
//
// <script type="text/javascript" src="jsr_class.js"></script>
// 
// function callbackfunc(jsonData) {
//      alert('Latitude = ' + jsonData.ResultSet.Result[0].Latitude + 
//            '  Longitude = ' + jsonData.ResultSet.Result[0].Longitude);
//      aObj.removeScriptTag();
// }
//
// request = 'http://api.local.yahoo.com/MapsService/V1/geocode?appid=YahooDemo&
//            output=json&callback=callbackfunc&location=78704';
// aObj = new JSONscriptRequest(request);
// aObj.buildScriptTag();
// aObj.addScriptTag();
//
//


// Constructor -- pass a REST request URL to the constructor
//
function JSONscriptRequest(fullUrl) {
    // REST request path
    this.fullUrl = fullUrl; 
    // Keep IE from caching requests
    this.noCacheIE = '&noCacheIE=' + (new Date()).getTime();
    // Get the DOM location to put the script tag
    this.headLoc = document.getElementsByTagName("head").item(0);
    // Generate a unique script tag id
    this.scriptId = 'JscriptId' + JSONscriptRequest.scriptCounter++;
}

// Static script ID counter
JSONscriptRequest.scriptCounter = 1;

// buildScriptTag method
//
JSONscriptRequest.prototype.buildScriptTag = function () {

    // Create the script tag
    this.scriptObj = document.createElement("script");
    
    // Add script object attributes
    this.scriptObj.setAttribute("type", "text/javascript");
    this.scriptObj.setAttribute("charset", "utf-8");
    this.scriptObj.setAttribute("src", this.fullUrl + this.noCacheIE);
    this.scriptObj.setAttribute("id", this.scriptId);
}
 
// removeScriptTag method
// 
JSONscriptRequest.prototype.removeScriptTag = function () {
    // Destroy the script tag
    this.headLoc.removeChild(this.scriptObj);  
}

// addScriptTag method
//
JSONscriptRequest.prototype.addScriptTag = function () {
    // Create the script tag
    this.headLoc.appendChild(this.scriptObj);
}

/*** Module loadmodule ***/
var vg_loadmodule_url = "loadmodule.php";
var vg_req = new Array(); // tableau de chargement des contenus en modules //

function loadModuleHtmlDone(target) {
    if (vg_req[target].readyState == 4) {
        if (vg_req[target].status == 200) {
			var reponse = vg_req[target].responseText;
            document.getElementById(target).innerHTML = reponse;
        } else {
            document.getElementById(target).innerHTML="Erreur ("+vg_req[target].statusText+") lors du chargement du contenu.";
        }
    }			
}
function loadModuleHtml(module, args) { loadModuleHtmlTarget(module, module, args); }
function loadModuleHtmlTarget(target, module, args) {
	var url = vg_loadmodule_url+"?type=html&module="+module;
	if(args != "") url += "&args="+escape(args);
    // -- Si on veut on peut placer un message d'attente pour le chargement ici --
	//document.getElementById(target).innerHTML = 'chargement en cours...';
    if (window.XMLHttpRequest) { vg_req[target] = new XMLHttpRequest(); vg_req[target].onreadystatechange = function() {loadModuleHtmlDone(target);}; vg_req[target].open("GET", url, true); vg_req[target].send(null); }
 	else // IE/Windows ActiveX version
		if (window.ActiveXObject) { vg_req[target] = new ActiveXObject("Microsoft.XMLHTTP"); if (vg_req[target]) { vg_req[target].onreadystatechange = function() {loadModuleHtmlDone(target);}; vg_req[target].open("GET", url, true); vg_req[target].send(); } }
}
