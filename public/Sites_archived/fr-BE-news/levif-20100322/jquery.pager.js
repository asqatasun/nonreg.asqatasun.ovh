$.fn.pager = function(clas, options) {
	
	var settings = {		
		navId: 'paging',
		navClass: 'pagenav',
		navAttach: 'before',
		whereToAttach: '#keywordsDiv',
		highlightClass: 'currentpage',
        hiddenClass: 'hidden',
		prevText: '&laquo;',
		nextText: '&raquo',
		linkText: null,
		linkWrap: null,
		height: null
	}
	if(options) $.extend(settings, options);
	
		
	return this.each( function () {
		
		var me = $(this);
		var size;
	  	var i = 0;		
		var navid = '#'+settings.navId;
		
		function init () {
			size = $(clas, me).not(navid).size();
			if(settings.height == null) {			
				settings.height = getHighest();
			}
			if(size > 1) {
				makeNav();
				show();
				highlight();
			}			
			/*sizePanel();*/
			if(settings.linkWrap != null) {
				linkWrap();
			}
		}
		function makeNav () {		
			var str = '<div id="'+settings.navId+'" class="pagenav">';
			str += '';
			for(var j = 1; j <= size; j++) {
				str += '<a href="#" rel="'+j+'" class="currentpage">';
				str += (settings.linkText == null) ? j : settings.linkText[j-1];				
				str += '</a> ';
			}
			str += '<a href="#" rel="next" class="nextpage">'+settings.nextText+'</a>';
			str += '</div>';
			switch (settings.navAttach) {		
				case 'before':
					$(settings.whereToAttach).before(str);
					break;
				case 'after':		
					$(settings.whereToAttach).after(str);
					break;
				case 'prepend':
					$(settings.whereToAttach).prepend(str);
					break;
				default:
					$(settings.whereToAttach).append(str);
					break;
			}
		}
		function show () {
			$(me).find(clas).not(navid).addClass(settings.hiddenClass);
			var show = $(me).find(clas).not(navid).get(i);
			$(show).removeClass(settings.hiddenClass);
		}		
		function highlight () {
			$(me).find(navid).find('a').removeClass(settings.highlightClass);
			var show = $(me).find(navid).find('a').get(i+1);			
			$(show).addClass(settings.highlightClass);
		}

		function sizePanel () {
			if($.browser.msie) {
				$(me).find(clas).not(navid).css( {
					height: settings.height
				});	
			} else {
				$(me).find(clas).not(navid).css( {
					minHeight: settings.height
				});
			}
		}
		function getHighest () {
			var highest = 0;
			$(me).find(clas).not(navid).each(function () {
				
				if(this.offsetHeight > highest) {
					highest = this.offsetHeight;
				}
			});
			highest = highest + "px";
			return highest;
		}
		function getNavHeight () {
			var nav = $(navid).get(0);
			return nav.offsetHeight;
		}
		function linkWrap () {
			$(me).find(navid).find("a").wrap(settings.linkWrap);
		}
		init();
		$(this).find(navid).find("a").click(function () {

			if($(this).attr('rel') == 'next') {
				if(i + 1 < size) {
					i = i+1;
				} else {
                    i = 0;
                }
			} else if($(this).attr('rel') == 'prev') { 
				if(i > 0) {	
					i = i-1;
				} else {
                    i = size - 1;
                }
			} else {		
				var j = $(this).attr('rel');	
				i = j-1;		
			}
			show();
			highlight();
			return false;
		});
	});	
}