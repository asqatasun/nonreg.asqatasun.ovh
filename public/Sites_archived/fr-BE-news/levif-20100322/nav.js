$(function() {
    $("#nav-main li.closed").each(function() {
        $(this).children("a").append("&nbsp;�");
        $(this).children("a").click(function() {
            var id = null;
            if($("#nav-main li.open ul").length > 0) {
                id = $("#nav-main li.open").attr("id");
                slideUp($("#nav-main li.open"));
            }
            if(id != $(this).parent().attr("id")) {
                slideDown($(this).parent());
            }
        });
        $(this).children("ul").children("li").children("a").each(function() {
            var loc = document.location.href;
            var href = $(this).attr("href");
            if(loc.indexOf(href) != -1) {
                slideDown($(this).parent().parent().parent());
            }
        });
    });
});

function slideUp(element) {
    element.removeClass("open");
    element.addClass("closed");
    element.children("ul").slideUp("slow");
}

function slideDown(element) {
     element.removeClass("closed");
     element.addClass("open");
     element.children("ul").slideDown("slow");
}