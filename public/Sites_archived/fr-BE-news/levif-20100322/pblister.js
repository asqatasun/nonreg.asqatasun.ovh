function pblister(campagne, formulier, taal, siteId, pwidth, pheight)
{
    if (window.pblisterScherm)	{
        if (!pblisterScherm.closed) {
			pblisterScherm.focus();
            return;
		}
	}
    
	var w,h;

	w=480;
	h=340;

    if (document.all || document.layers) {
        w=screen.availWidth;
        h=screen.availHeight
    }

    var leftPos = (w-pwidth)/2;
    var topPos = (h-pheight)/2;

    pblisterScherm=window.open('http://www.abonnementen.be/pblister/init.do'
    + '?CAMP=' + campagne
    + '&CODE=' + formulier
    + '&TAAL=' + taal
    + '&SITEID='+ siteId,
    'popup','width='+pwidth+',height='+pheight+',location=no,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,top='+topPos+',left='+leftPos);
}