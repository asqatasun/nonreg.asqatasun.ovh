##ACAP version=1.0

# Conventional policies...
User-agent: *
Disallow: /sendarticle/
Disallow: /Users/
Disallow: /users/
Disallow: /*/print$
Disallow: /email/
Disallow: /contactus/
Disallow: /share/
Disallow: /websearch
Disallow: /*?commentpage=
Disallow: /whsmiths/
Disallow: /external/overture/
Disallow: /*?showallcomments=true

User-agent: msnbot
Crawl-delay: 1
Disallow: /sendarticle/
Disallow: /Users/
Disallow: /*/print$
Disallow: /email/
Disallow: /contactus/
Disallow: /share/
Disallow: /websearch
Disallow: /*?commentpage=
Disallow: /whsmiths/
Disallow: /external/overture/
Disallow: /*?showallcomments=true

User-agent: Googlebot-Mobile
Disallow: /

# ACAP policies...
ACAP-crawler: *
ACAP-disallow-crawl: /sendarticle/
ACAP-disallow-crawl: /Users/
ACAP-disallow-crawl: /*/print$
ACAP-disallow-crawl: /email/
ACAP-disallow-crawl: /contactus/
ACAP-disallow-crawl: /share/
ACAP-disallow-crawl: /websearch
ACAP-disallow-crawl: /*?commentpage=
ACAP-disallow-crawl: /whsmiths/
ACAP-disallow-crawl: /external/overture/
ACAP-disallow-crawl: /*?showallcomments=true
