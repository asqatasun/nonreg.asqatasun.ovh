<!--
// -----------------------
// FUNCTION: fChangeTickerEntry
// DESCRIPTION: A function that changes the ticker entry.
// ARGUMENTS: None
// RETURN: None
// -----------------------
function fChangeTickerEntry() {
	nTickerToShow = nTickerToShow + 1;
	if (nTickerToShow >= aTickerText.length-1) {
		nTickerToShow = 0;
	}
	var eTicker = document.getElementById('ticker');
	var sHTML = '<a class="ticker" href="' + aTickerUrl[nTickerToShow] +'">' + aTickerText[nTickerToShow] +  '</a>';
	eTicker.innerHTML = sHTML;
}
//-->