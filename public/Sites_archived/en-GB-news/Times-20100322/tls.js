// Fetching host url for TLS search
var url = window.location.host;


// -----------------------
// FUNCTION: fValidateTLSSearchText
// DESCRIPTION: A function that validates the text field for alphanumeric chars. 
// ARGUMENTS: queryKeywords
// RETURN: Integer value
// AUTHOR: Vikas Parmar
// -----------------------

function fValidateTLSSearchText(searchFname){
	var myChar = "";
	var totArrLength = 0;
	var isValid;
	var isThereValue = 0;
	var specialChars= new Array('!','�','#','$','%','*','/',':',';','\\','^','|','~','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�',' ','@','&','(',')','=','+','-','_','[','{','}',']','\"','<','>','','?','`','\'',',','.');
	
	searchFname = fTrim(searchFname);
	
	if(searchFname  == "" && searchFname.length<=0){
		isThereValue = 1;
	}else{
		for(i=0;i<26;i++){
			//A to Z
			myChar = myChar + String.fromCharCode(65+i);
		}
		for(i=0;i<26;i++){
			//a- z
			myChar = myChar + String.fromCharCode(97+i);
		}
		for(i=0;i<10;i++){
			//0 to 9
			myChar = myChar + String.fromCharCode(48+i);
		}
		for(i=0;i<specialChars.length;i++){
			//0 to 9
			myChar = myChar + specialChars[i];
		}
		for(i=0;i<searchFname.length;i++){
			var char = searchFname.charAt(i);
			isValid = myChar.indexOf(char);
			//alert("char["+i+"]: "+char+", isValid : "+isValid );
			if(isValid == -1){
				isThereValue = 2;
			}
		}
	}
	return isThereValue;
}

//-----------------------
//FUNCTION: doTLSSearch
//DESCRIPTION: A function that validates the text field and assigns the action urls as per the the selected value 
//ARGUMENTS: none
//RETURN: true/false
//AUTHOR: Vikas Parmar
//-----------------------

function doTLSSearch(){
	var isKeywordSplChar = fValidateTLSSearchText(document.searchResultId.queryKeywords.value);
	if(isKeywordSplChar == 0){
		document.searchResultId.action='http://'+ url + '/tol/tlskeywordsearch.tls' + '?queryKeywords=' + document.searchResultId.queryKeywords.value;
		document.searchResultId.submit();
		return true;
	}else if(isKeywordSplChar == 1){
		alert("You have not entered any search terms. Please enter at least one word into the search box.");
	}else{
		alert("Please enter a valid search term.");
	}
	document.searchResultId.queryKeywords.focus();
	return false;
	
}

function doTLSRedirectSearch(){
	var isKeywordSplChar = fValidateTLSSearchText(document.searchResultId.queryKeywords.value);
	if(isKeywordSplChar == 0){
		document.searchResultId.action='http://'+ url + '/tol/tlskeywordsearch.tls' + '?queryKeywords=' + document.searchResultId.queryKeywords.value;
		document.searchResultId.submit();
		return true;
	}else if(isKeywordSplChar == 1){
		alert("You have not entered any search terms. Please enter at least one word into the search box.");
	}else{
		alert("Please enter a valid search term.");
	}
	document.searchResultId.queryKeywords.focus();
	return false;
	
}

//-----------------------
//DESCRIPTION: Functions have been created for TLS Search page pagination 
//AUTHOR: Vikas Parmar
//-----------------------

function gotoPage(pg,queryKeywords,sortBy,resultsPerPage,fromDate,toDate,addFilters,removeFilters,additionalQueryKeywords)
{
	document.searchResultId.currentPageNumber.value = eval(pg)
	document.searchResultId.queryKeywords.value= queryKeywords;
	document.searchResultId.sortBy.value= sortBy;
	document.searchResultId.resultsPerPage.value= resultsPerPage;
	document.searchResultId.fromDate.value= fromDate;
	document.searchResultId.toDate.value= toDate;
	document.searchResultId.addFilters.value= addFilters;
	document.searchResultId.removeFilters.value= removeFilters;
	document.searchResultId.additionalQueryKeywords.value= additionalQueryKeywords;
	
	document.searchResultId.action='http://'+ url + '/tol/tlskeywordsearch.tls' + '?currentPageNumber=' + eval(pg) + '&queryKeywords=' + queryKeywords + '&sortBy=' + sortBy + '&resultsPerPage=' + resultsPerPage + '&fromDate=' + fromDate + '&toDate=' + toDate + '&addFilters=' + addFilters + '&removeFilters=' + removeFilters + '&additionalQueryKeywords=' + additionalQueryKeywords;
	document.searchResultId.submit();
}

function viewResultsPerPage(pg,queryKeywords,resultsPerPage,sortBy,fromDate,toDate,addFilters,removeFilters,additionalQueryKeywords)
{
	document.searchResultId.currentPageNumber.value = eval(pg)
	document.searchResultId.resultsPerPage.value= resultsPerPage;
	document.searchResultId.queryKeywords.value= queryKeywords;
	document.searchResultId.sortBy.value= sortBy;
	document.searchResultId.fromDate.value= fromDate;
	document.searchResultId.toDate.value= toDate;
	document.searchResultId.addFilters.value= addFilters;
	document.searchResultId.removeFilters.value= removeFilters;
	document.searchResultId.additionalQueryKeywords.value= additionalQueryKeywords;
	
	document.searchResultId.action='http://'+ url + '/tol/tlskeywordsearch.tls' + '?currentPageNumber=' + eval(pg) + '&queryKeywords=' + queryKeywords + '&sortBy=' + sortBy + '&resultsPerPage=' + resultsPerPage + '&fromDate=' + fromDate + '&toDate=' + toDate + '&addFilters=' + addFilters + '&removeFilters=' + removeFilters + '&additionalQueryKeywords=' + additionalQueryKeywords;
	document.searchResultId.submit();
}

function sortBy(pg,queryKeywords,sortBy,resultsPerPage,fromDate,toDate,addFilters,removeFilters,additionalQueryKeywords)
{
	document.searchResultId.currentPageNumber.value = eval(pg)
	document.searchResultId.sortBy.value= sortBy;
	document.searchResultId.queryKeywords.value= queryKeywords;
	document.searchResultId.resultsPerPage.value= resultsPerPage;
	document.searchResultId.fromDate.value= fromDate;
	document.searchResultId.toDate.value= toDate;
	document.searchResultId.addFilters.value= addFilters;
	document.searchResultId.removeFilters.value= removeFilters;
	document.searchResultId.additionalQueryKeywords.value= additionalQueryKeywords;
	
	document.searchResultId.action='http://'+ url + '/tol/tlskeywordsearch.tls' + '?currentPageNumber=' + eval(pg) + '&queryKeywords=' + queryKeywords + '&sortBy=' + sortBy + '&resultsPerPage=' + resultsPerPage + '&fromDate=' + fromDate + '&toDate=' + toDate + '&addFilters=' + addFilters + '&removeFilters=' + removeFilters + '&additionalQueryKeywords=' + additionalQueryKeywords;
	document.searchResultId.submit();
}	



//-----------------------
//DESCRIPTION: Functions have been created for TLS Date base Search  
//AUTHOR: Vikas Parmar
//-----------------------	
	
function doTLSDatePreciseSearch(pg,queryKeywords,sortBy,resultsPerPage,addFilters,removeFilters,additionalQueryKeywords)
{
	var inputFromDate = document.getElementById("date_dd_from").value;
	var inputFromMonth = document.getElementById("date_mm_from").value;
	var inputFromYear = document.getElementById("date_yyyy_from").value;
	var inputToDate = document.getElementById("date_dd_to").value;
	var inputToMonth = document.getElementById("date_mm_to").value;
	var inputToYear = document.getElementById("date_yyyy_to").value;
	
	if(inputFromDate == "null" || inputFromMonth == "null" || inputFromYear == "null" || inputToDate == "null" || inputToMonth == "null" || inputToYear == "null"){
		alert("Please choose a valid date.");
		return false;
	}
	
	if(inputToYear < inputFromYear){
		alert("Please choose a valid date range.");
		return false;
	}else if(inputToYear == inputFromYear){
		if(inputToMonth < inputFromMonth){
			alert("Please choose a valid date range.");
			return false;
		}else if(inputToMonth == inputFromMonth){
			if(inputToDate < inputFromDate){
				alert("Please choose a valid date range.");
				return false;
			}
		}
	}
	
	var maxInputFromDate = daysInMonth(inputFromMonth - 1,inputFromYear);
	var maxInputToDate = daysInMonth(inputToMonth - 1,inputToYear);
	
	if(inputFromDate > maxInputFromDate){
		alert("Please choose a valid date range for from date.");
		return false;
	}
	if(inputToDate > maxInputToDate){
		alert("Please choose a valid date range for to date.");
		return false;
	}
	
	var fromDate = inputFromYear + "-" + inputFromMonth + "-" + inputFromDate;
	var toDate = inputToYear + "-" + inputToMonth + "-" + inputToDate;
	
	document.tlsDatePreciseForm.fromDate.value= fromDate;
	document.tlsDatePreciseForm.toDate.value= toDate;
	document.tlsDatePreciseForm.action='http://'+ url + '/tol/tlskeywordsearch.tls' + '?currentPageNumber=' + eval(pg) + '&queryKeywords=' + queryKeywords + '&sortBy=' + sortBy + '&resultsPerPage=' + resultsPerPage + '&fromDate=' + fromDate + '&toDate=' + toDate + '&addFilters=' + addFilters + '&removeFilters=' + removeFilters + '&additionalQueryKeywords=' + additionalQueryKeywords;
	document.tlsDatePreciseForm.submit();
	//alert("fromDate : "+fromDate+" toDate : "+toDate);

	return true;
}

function doTLSDateRangeSearch(pg,queryKeywords,sortBy,resultsPerPage,addFilters,removeFilters,additionalQueryKeywords)
{
	var inputDateRange = document.getElementById("date_refine").value;
	var today = new Date();
	var modifiedToday = new Date();
	var todayDate = today.getDate();
	var todayMonth = today.getMonth() + 1;
	var todayYear = today.getFullYear();
	var fromDate;
	var toDate;
	var modifiedTodayDate = todayDate;
	var modifiedTodayMonth = todayMonth;
	var modifiedTodayYear = todayYear;
	
	if(inputDateRange == null || inputDateRange == "null"){
		alert("Please choose a valid date.");
		return false;
	}
	else if(inputDateRange == "all"){
		
		fromDate = "";
		toDate = "";
	
	}else if(inputDateRange == "today" ){
	
		if(modifiedTodayDate < 10) {
			modifiedTodayDate = "0" + modifiedTodayDate;
		}
		if(modifiedTodayMonth < 10) {
			modifiedTodayMonth = "0" + modifiedTodayMonth;
		}
		fromDate = modifiedTodayYear  + "-" + modifiedTodayMonth + "-" +  modifiedTodayDate;
		toDate = modifiedTodayYear  + "-" +  modifiedTodayMonth  + "-" +  modifiedTodayDate;
		
		
	
	}else if(inputDateRange == "yesterday"){

		modifiedToday.setDate(modifiedToday.getDate()-1);
		modifiedTodayDate = modifiedToday.getDate();
		
		if(modifiedTodayDate < 10) {
			modifiedTodayDate = "0" + modifiedTodayDate;
		}
		if(modifiedTodayMonth < 10) {
			modifiedTodayMonth = "0" + modifiedTodayMonth;
		}
		fromDate = modifiedTodayYear  + "-" + modifiedTodayMonth + "-" +  modifiedTodayDate;
		toDate = modifiedTodayYear  + "-" +  modifiedTodayMonth  + "-" +  modifiedTodayDate;
		
	}else if(inputDateRange == "week"){
	
		modifiedToday.setDate(modifiedToday.getDate()-7);
		modifiedTodayDate = modifiedToday.getDate();
		
		if(todayDate < 10) {
			todayDate = "0" + todayDate;
		}
		if(modifiedTodayDate < 10) {
			modifiedTodayDate = "0" + modifiedTodayDate;
		}
		if(modifiedTodayMonth < 10) {
			modifiedTodayMonth = "0" + modifiedTodayMonth;
		}
		fromDate = modifiedTodayYear  + "-" + modifiedTodayMonth + "-" +  modifiedTodayDate;
		toDate = modifiedTodayYear  + "-" +  modifiedTodayMonth  + "-" +  todayDate;
	
	}else if(inputDateRange == "month"){
		
		modifiedTodayDate = 1;
			
		if(todayDate < 10) {
			todayDate = "0" + todayDate;
		}
		if(modifiedTodayDate < 10) {
			modifiedTodayDate = "0" + modifiedTodayDate;
		}
		if(modifiedTodayMonth < 10) {
			modifiedTodayMonth = "0" + modifiedTodayMonth;
		}
		fromDate = modifiedTodayYear  + "-" + modifiedTodayMonth + "-" +  modifiedTodayDate;
		toDate = modifiedTodayYear  + "-" +  modifiedTodayMonth  + "-" +  todayDate;

	}else if(inputDateRange == "year"){
		
		modifiedTodayDate = 1;
		modifiedTodayMonth = 1;

		if(todayDate < 10) {
			todayDate = "0" + todayDate;
		}
		if(todayMonth < 10) {
			todayMonth = "0" + todayMonth;
		}
		if(modifiedTodayDate < 10) {
			modifiedTodayDate = "0" + modifiedTodayDate;
		}
		if(modifiedTodayMonth < 10) {
			modifiedTodayMonth = "0" + modifiedTodayMonth;
		}
		fromDate = modifiedTodayYear  + "-" + modifiedTodayMonth + "-" +  modifiedTodayDate;
		toDate = todayYear  + "-" +  todayMonth  + "-" +  todayDate;

	}else {
		
		fromDate = inputDateRange  + "-" + "01" + "-" +  "01";
		toDate = inputDateRange  + "-" +  "12"  + "-" +  "31";
	}
	
	document.tlsDateRangeForm.fromDate.value= fromDate;
	document.tlsDateRangeForm.toDate.value= toDate;
	document.tlsDateRangeForm.action='http://'+ url + '/tol/tlskeywordsearch.tls' + '?currentPageNumber=' + eval(pg) + '&queryKeywords=' + queryKeywords + '&sortBy=' + sortBy + '&resultsPerPage=' + resultsPerPage + '&fromDate=' + fromDate + '&toDate=' + toDate + '&addFilters=' + addFilters + '&removeFilters=' + removeFilters + '&additionalQueryKeywords=' + additionalQueryKeywords;
	document.tlsDateRangeForm.submit();
	
//	alert("fromDate : "+fromDate+" toDate : "+toDate);
	return true;
}


//-----------------------
//DESCRIPTION: Function has been created to return number of days in a month  
//AUTHOR: Vikas Parmar
//-----------------------	

function daysInMonth(iMonth, iYear){    

	return 32 - new Date(iYear, iMonth, 32).getDate();
}


//-----------------------
//DESCRIPTION: Functions have been created for TLS Additional word Search  
//AUTHOR: Vikas Parmar
//-----------------------	

function refineByAdditionalKeywords(pg,queryKeywords,sortBy,resultsPerPage,fromDate,toDate,addFilters,removeFilters,searchType){

	var isKeywordSplChar = fValidateTLSSearchText(document.tlsAdditionalWordForm.additionalKeywords.value);
	if(isKeywordSplChar == 0){
		refineSearchResults(pg,queryKeywords,sortBy,resultsPerPage,fromDate,toDate,addFilters+"@tlscontent:"+document.tlsAdditionalWordForm.additionalKeywords.value,removeFilters,searchType,"");
	}else if(isKeywordSplChar == 1){
		alert("You have not entered any search terms. Please enter at least one word into the search box.");
	}else{
		alert("Please enter a valid search term.");
	}
	document.tlsAdditionalWordForm.additionalKeywords.focus();
	return false;

}

function refineSearchResults(pg,queryKeywords,sortBy,resultsPerPage,fromDate,toDate,addFilters,removeFilters,searchType,additionalQueryKeywords){	
	
	document.tlsAdditionalWordForm.dateSearchType.value = searchType;        		
	document.tlsAdditionalWordForm.addFilters.value = addFilters;
	document.tlsAdditionalWordForm.removeFilters.value = removeFilters;		
	document.tlsAdditionalWordForm.additionalQueryKeywords.value = additionalQueryKeywords;		
	document.tlsAdditionalWordForm.isDateSearch.value='false';
	document.tlsAdditionalWordForm.action ='http://'+ url + '/tol/tlskeywordsearch.tls' + '?currentPageNumber=' + eval(pg) + '&queryKeywords=' + queryKeywords + '&sortBy=' + sortBy + '&resultsPerPage=' + resultsPerPage + '&fromDate=' + fromDate + '&toDate=' + toDate + '&addFilters=' + addFilters + '&removeFilters=' + removeFilters + '&dateSearchType=' + searchType + '&additionalQueryKeywords=' + additionalQueryKeywords;
	document.tlsAdditionalWordForm.submit();
	
	//alert("Add Filters : "+addFilters+" Remove Filters : "+removeFilters);
	return true;
}

//-----------------------
//DESCRIPTION: Functions have been created for TLS ISBN Search  
//AUTHOR: Vikas Parmar
//-----------------------	

function refineByISBN(pg,queryKeywords,sortBy,resultsPerPage,fromDate,toDate,addFilters,removeFilters,searchType){

	var isKeywordSplChar = fValidateTLSSearchText(document.tlsISBNForm.additionalKeywordsISBN.value);
	if(isKeywordSplChar == 0){
		refineSearchResultsISBN(pg,queryKeywords,sortBy,resultsPerPage,fromDate,toDate,addFilters+"@tlscontent:"+document.tlsISBNForm.additionalKeywordsISBN.value,removeFilters,searchType);
	}else if(isKeywordSplChar == 1){
		alert("You have not entered any search terms. Please enter at least one word into the search box.");
	}else{
		alert("Please enter a valid search term.");
	}
	document.tlsISBNForm.additionalKeywordsISBN.focus();
	return false;

}

function refineSearchResultsISBN(pg,queryKeywords,sortBy,resultsPerPage,fromDate,toDate,addFilters,removeFilters,searchType){	
	
	document.tlsISBNForm.dateSearchType.value = searchType;        		
	document.tlsISBNForm.addFilters.value = addFilters;
	document.tlsISBNForm.removeFilters.value = removeFilters;		
	document.tlsISBNForm.isDateSearch.value='false';
	document.tlsISBNForm.action ='http://'+ url + '/tol/tlskeywordsearch.tls' + '?currentPageNumber=' + eval(pg) + '&queryKeywords=' + queryKeywords + '&sortBy=' + sortBy + '&resultsPerPage=' + resultsPerPage + '&fromDate=' + fromDate + '&toDate=' + toDate + '&addFilters=' + addFilters + '&removeFilters=' + removeFilters + '&dateSearchType=' + searchType;
	document.tlsISBNForm.submit();
	
	//alert("Add Filters : "+addFilters+" Remove Filters : "+removeFilters);
	return true;
}

