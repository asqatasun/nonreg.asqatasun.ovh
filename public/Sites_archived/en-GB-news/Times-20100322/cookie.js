// -----------------------
// FUNCTION: set_cookie
// DESCRIPTION: Sets cookie in browser.
// ARGUMENTS: name - Cookie name , value - cookie value, seconds - TTL cookie, path - cookie path
// RETURN: None
// -----------------------

function set_cookie ( name, value, seconds, path )
{
  var cookie_string = name + "=" + escape ( value );

  if ( seconds )
  {
    var expires = new Date();
    expires.setTime(expires.getTime() + (seconds*1000));
    cookie_string += "; expires=" + expires.toGMTString();
  }

  if ( path )
        cookie_string += "; path=" + escape ( path );
  else
		cookie_string += "; path=/";

   document.cookie = cookie_string;
}


// -----------------------
// FUNCTION: get_cookie
// DESCRIPTION: gets cookie with supplied name.
// ARGUMENTS: cookie_name - name of cookie to be retrived 
// RETURN: cookie value
// 
function get_cookie ( cookie_name )
{
 var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
  if ( results )
    return ( unescape ( results[2] ) );
  else
    return null;
}

// -----------------------
// FUNCTION: delete_cookie
// DESCRIPTION: deletes cookie with supplied name.
// ARGUMENTS: cookie_name - name of cookie to be deleted 
// RETURN: none
// 
function delete_cookie ( cookie_name )
{
  var cookie_date = new Date ( );  // current date & time
  cookie_date.setTime ( cookie_date.getTime() - 1 );
  document.cookie = cookie_name += "=; expires=" + cookie_date.toGMTString();
}