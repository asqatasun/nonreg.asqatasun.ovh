

function refineArticlesKeyword() {

	var refineKw = trim(document.refineSearchKeywordForm['refineKw'].value);

	action = document.refineSearchKeywordForm.action;

	var idx = action.indexOf("?nq=");
	if (idx >= 0) {
		action = action.substring(0, idx+4) + "nFullText:" + refineKw + "," + action.substring(idx+4);
	} else {
		action = action + "?nq=nFullText:" + refineKw;  
	}

	document.refineSearchKeywordForm.action = action;
	document.refineSearchKeywordForm.submit();
}


function refineArticlesDaterange() {

	var fromDay = parseInt(document.refineSearchDateForm['fromDay'].value) + 1;
	var fromMonth = parseInt(document.refineSearchDateForm['fromMonth'].value) + 1;
	var fromYear = trim(document.refineSearchDateForm['fromYear'].value);

	var toDay = parseInt(document.refineSearchDateForm['toDay'].value) + 1;
	var toMonth = parseInt(document.refineSearchDateForm['toMonth'].value) + 1;
	var toYear = trim(document.refineSearchDateForm['toYear'].value);

	if (fromDay < 10) {
		fromDay = "0" + fromDay;
	}
	if (fromMonth < 10) {
		fromMonth = "0" + fromMonth;
	}
	if (toDay < 10) {
		toDay = "0" + toDay;
	}
	if (toMonth < 10) {
		toMonth = "0" + toMonth;
	}

	action = document.refineSearchDateForm.action;

	// If there is any publish date it in, remove that one
	var idx = action.indexOf(",publishdate:");
	if (idx >= 0) {
		action = action.substring(0, idx) + action.substring(idx+34);
	}

	action = action + ",publishdate:" + fromYear + "-" + fromMonth + "-" + fromDay + "*" + toYear + "-" + toMonth + "-" + toDay;

	document.refineSearchDateForm.action = action;
	document.refineSearchDateForm.submit();
}