isLogin = false;
isLogout = false;
isMoving = false;

function clickLogin() {
	if(!isMoving) {
		if(!isLogin) {
			isLogin = true;
			openLogin();
		} else {
			isLogin = false;
			closeLogin();
		}
	}
}	

function clickLogout() {
	if(!isMoving) {
		if(!isLogout) {
			isLogout = true;
			openLogout();
		} else {
			isLogout = false;
			closeLogout();
		}
	}
}	

function openLogin() {
	obj('loginWindow').style.display='block';
	moveObj(-80, 0, 'login');
}

function closeLogin() {
	moveObj(0, -81, 'login');
	if(!isMoving && !login) {
		obj('loginWindow').style.display='none';
	}
}

function openLogout() {
	obj('logoutWindow').style.display='block';
	moveObj(-30, 0, 'logout');
}

function closeLogout() {
	moveObj(0, -31, 'logout');
	if(!isMoving && !logout) {
		obj('logoutWindow').style.display='none';
	}
}


function moveObj(from, to, id) {
	isMoving = true;
	if(from!=to && isMoving) {
		if(from < to) {
			from+=Math.round((to - from)/10)+1;
		} else {
			from-=Math.round((from - to)/10)+1;
		}
		obj(id).style.top=from + "px";
		setTimeout("moveObj("+from+", "+to+", \""+id+"\")", 20);
	} else {
		isMoving = false;
	}
}

function obj(id) {
	return document.getElementById(id);
}

function val(id) {
	return document.getElementById(id).value;
}

function checkGoogle(){
	var rightBannerContent=document.getElementById("bannerRight").innerHTML;
	var found=0;
	found=rightBannerContent.search(/Google/i);
	if(found>0){
		return true;
	}else{
		return false;
	}
}

/*
*	floating banner
*/
function moveBanner() {
	if(checkGoogle()){
		//nothing
	}else{
		if(scrollPosition() > 95) {
			if(!isMoving) {
				moveObj(obj('bannerRight').offsetTop, scrollPosition(), 'bannerRight');
			} else {
				setTimeout("moveBanner()", 200);
			}
		} else {
			if(!isMoving) {
				moveObj(obj('bannerRight').offsetTop, 95, 'bannerRight');
			} else {
				setTimeout("moveBanner()", 200);
			}
		}
	}
}

function windowHeight() {
	if (document.all) {
		return parseInt(document.documentElement.clientHeight)
	} else {
		return parseInt(window.document.documentElement.clientHeight)
	}
}

function scrollPosition() {
	if(document.all) {
		return document.documentElement.scrollTop;;
	} else {
		return window.pageYOffset;
	}
}

if(windowHeight() > 600) {
	window.onscroll = moveBanner;
}

/*
* create a XMLHttp object
* try for different versions in IE
*/
function createXMLHttp(){
	if(typeof XMLHttpRequest != "undefined") {
		return new XMLHttpRequest();
	} else if(window.ActiveXObject) {
		var arrOfVersions = [
			"MSXML2.XMLHttp.5.0",
			"MSXML2.XMLHttp.4.0",
			"MSXML2.XMLHttp.3.0",
			"MSXML2.XMLHttp",
			"Microsoft.XMLHttp"
		];
		for(var i = 0; i < arrOfVersions.length; i++) {
			try {
				var oXmlHttp = new ActiveXObject(arrOfVersions[i]);
				return oXmlHttp;
			} catch(oError){}
		}
	}
}

/*
* load data from a json file
* evaluate the file and return the data
*/
/*function loadFile(file) {
	try{
		var oXMLHttp = createXMLHttp();
		
		oXMLHttp.open("GET", file, false);
		oXMLHttp.send(null);		

		return oXMLHttp.responseText;
	}catch(ex){
		alert(ex);
	}
}*/

/*
* highlight searched words in a text
*/
function highlightSearch(inputText, searchTerm) {
  var newText = "";
  var i = -1;
  var lowerSearchTerm = searchTerm.toLowerCase();
  var lowerInputText = inputText.toLowerCase();
    
  while (inputText.length > 0) {
    i = lowerInputText.indexOf(lowerSearchTerm, i+1);
    if (i < 0) {
      newText += inputText;
      inputText = "";
    } else {
      if (inputText.lastIndexOf(">", i) >= inputText.lastIndexOf("<", i)) {
        if (lowerInputText.lastIndexOf("/script>", i) >= lowerInputText.lastIndexOf("<script", i)) {
          newText += inputText.substring(0, i) + "<strong style=\"background-color:#ccc;\">" + inputText.substr(i, searchTerm.length) + "</strong>";
          inputText = inputText.substr(i + searchTerm.length);
          lowerInputText = inputText.toLowerCase();
          i = -1;
        }
      }
    }
  }
  
  return newText;
}

function coverGoogle(){
	//alert("coverGoogle");
	var rightBannerContent=document.getElementById("bannerRight").innerHTML;
	var found=0;
	found=rightBannerContent.search(/Google/i);
//	alert(found);
	if(found>0){
		document.getElementById("coverSky").style.display="block";
	}
	
	
	leadBannerContent=document.getElementById("bannerLead").innerHTML;
	found=0;
	found=leadBannerContent.search(/Google/i);
	
	if(found>0){
		document.getElementById("coverLead").style.display="block";
	}
	
	
	topRightBannerContent=document.getElementById("headBanner").innerHTML;
	found=0;
	found=topRightBannerContent.search(/Google/i);
	
	if(found>0){
		document.getElementById("coverBannerTop").style.display="block";
	}
}


function showComments(){
	if(document.getElementById("comment").style.display=="none"){
		document.getElementById("comment").style.display="block";
	}else{
		document.getElementById("comment").style.display="none";
	}
}