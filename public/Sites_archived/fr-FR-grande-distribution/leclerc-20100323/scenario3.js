function Intro()
{
ACTOR.Stop();
ACTOR.PlaceAtVisibleFrame(0.55,0.65);
ACTOR.Show();
ACTOR.Play("Wave");
ACTOR.Speak("[PlaySound(/c2k/cantoche/LAData/sons/3Intro_01.mp3)]Bonjour !");
ACTOR.Play("Argue_02");
ACTOR.Speak("[PlaySound(/c2k/cantoche/LAData/sons/3Intro_02.mp3)]Soyez les bienvenus sur le site e-leclerc.com...");
ACTOR.Play("Explain");
ACTOR.Speak("[PlaySound(/c2k/cantoche/LAData/sons/3Intro_03.mp3)]Vous voulez recevoir des offres sur mesure directement dans votre boîte mail ?");
ACTOR.Play("Explain");
ACTOR.Speak("[PlaySound(/c2k/cantoche/LAData/sons/3Intro_04.mp3)]...et être certain de bénéficier,  chaque instant, des meilleures offres des promotions...");
ACTOR.Speak("[PlaySound(/c2k/cantoche/LAData/sons/3Intro_05.mp3)]...et des bonus de votre magasin E.Leclerc ?");
ACTOR.Play("GestureUser");
ACTOR.Speak("[PlaySound(/c2k/cantoche/LAData/sons/3Intro_06.mp3)]Alors créez maintenant votre propre Compte sur ce site !");
ACTOR.Play("Proud");
ACTOR.Speak("[PlaySound(/c2k/cantoche/LAData/sons/3Intro_07.mp3)]Je vous ouvre la page...<WEB_LINK,Now,Maintenant...><ou ><WEB_LINK,Later,plus tard...>");
}

function Plus_tard()
{
ACTOR.Stop();
ACTOR.PlaceAtVisibleFrame(0.55,0.65);
ACTOR.Show();
ACTOR.Play("Proud");
ACTOR.Speak("[PlaySound(/c2k/cantoche/LAData/sons/3Intro_08.mp3)]Je vous montre comment gagner du temps et de l'argent ?<WEB_LINK,Now,Maintenant...><ou ><WEB_LINK,Never,plus tard...>");
}

function Never()
{
ACTOR.Stop();
ACTOR.PlaceAtVisibleFrame(0.55,0.65);
ACTOR.Show();
ACTOR.Play("Proud");
ACTOR.Speak("[PlaySound(/c2k/cantoche/LAData/sons/3Intro_10.mp3)]Alors je vous laisse naviguer...");
ACTOR.Play("Wave");
ACTOR.Speak("[PlaySound(/c2k/cantoche/LAData/sons/3Intro_11.mp3)]A bientôt !");
ACTOR.Hide();
}

function Later()
{
ACTOR.Stop();
ACTOR.PlaceAtVisibleFrame(0.1,0.90);
ACTOR.Show();
setTimeout("CkeckLater()",30000);
}
