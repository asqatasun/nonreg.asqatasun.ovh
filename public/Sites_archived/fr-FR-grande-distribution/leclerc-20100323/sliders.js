function fHomeSlideShow(){
	var sServerRoot = "";
	var sImagePath = sServerRoot+"/meaportail/images/";
	var sProductImagePath = sServerRoot+"/c2k/magasin/images/produits/medium/";
	var sLinkPath = sServerRoot+"/c2k/portail/promotion/home.asp?from=niv1";
	// create data
	var oContainer = $("galleryProductsSlide");
	for (var i = 0; i<arrProductId.length; i++) {
		// create main DIV
		var oDiv = new Element("div").addClass("slideTag").injectInside(oContainer);
		// base divs

		var oDivLev1 = new Element("div").addClass("productTitle").injectInside(oDiv);



		var urlImage = sProductImagePath+((arrImage[i]==1) ? arrProductId[i] +".jpg" : "nondispo_m.gif");
		var imageLink = new Element("a").setProperties({
			href: sLinkPath+"&productid="+arrProductId[i]+"&prod_ref="+arrProdRef[i]+"&ta=open&op_code="+arrCodeOP[i]+"#"+arrProductId[i]
		}).setHTML("<img src=\""+urlImage+"\" />").injectInside(oDivLev1);
		
		var textLinkParagraph = new Element("p").injectInside(oDivLev1);
		
		var textLink = new Element("a").setProperties({
			href: sLinkPath+"&productid="+arrProductId[i]+"&prod_ref="+arrProdRef[i]+"&ta=open&op_code="+arrCodeOP[i]+"#"+arrProductId[i]
		}).setHTML(arrProductName[i]).injectInside(textLinkParagraph);
			
		if(arrreductionCaisse[i]=="0")
		{
			var withReductionTotal = (!(arrTicket[i]=="0")||!(arrreductionCaisse[i]=="0"));
		}
		else if(arrreductionCaisse[i]=="")
		{
			var withReductionTotal = (!(arrTicket[i]=="0")||!(arrreductionCaisse[i]==""));
		}
		else
		{
			var withReductionTotal = (!(arrTicket[i]=="0")||!(arrreductionCaisse[i]==""));
		}
		
		var oDivLev2 = new Element("div").addClass(withReductionTotal?"productPriceWithTicket":"productPrice").injectInside(oDiv);

		var withTicket = !(arrTicket[i]=="0");
		
		
		
		if(arrreductionCaisse[i]=="0")
		{
			var withEnCaisse = !(arrreductionCaisse[i]=="0");
		}
		else if(arrreductionCaisse[i]=="")
		{
			var withEnCaisse = !(arrreductionCaisse[i]=="");
		}
		else
		{
			var withEnCaisse = !(arrreductionCaisse[i]=="0");
		}

		if (withReductionTotal) 
		{
			if (withTicket) 
			{
				// if there are ticket and/or reduction at cash
				var oDivSubstraction = new Element("div").addClass("substraction").injectInside(oDivLev2);
			    var oSpanPrice1 = new Element("span").addClass("price1").injectInside(oDivSubstraction).setHTML(arrPrixEuro[i] + " &euro; - ");
			    //gestion couleur pastille


			   if(arrCouleurTel[i]=="J")
  				{
						var sCouleurPast = "ticket";
					}
					else
					{
						var sCouleurPast = "ticket" + arrCouleurTel[i];
					}
			  
			  
			    
				if (arrTicket[i].indexOf("%") != -1) 
				{
  				// In case that ticket is %
						var oSpanTicket = new Element("span").addClass(sCouleurPast).injectInside(oDivSubstraction).setHTML(arrTicket[i]);	
				}
				else 
				{
					// In case that ticket is not %			
							var oSpanTicket = new Element("span").addClass(sCouleurPast).injectInside(oDivSubstraction).setHTML(arrTicket[i] + "&nbsp;&euro;");	
				}
				if((arrreductionCaisse[i]!="0") && (arrreductionCaisse[i]!=""))
				{
							var oPEnCaisse = new Element("p").addClass("enCaisse").injectInside(oDivSubstraction).setHTML("- " + arrreductionCaisse[i] + " &euro; en caisse=");	
				}	
			 }

			// if there is only reduction at cash without ticket  
			else {
				var oDivSubstraction = new Element("div").addClass("substraction2").injectInside(oDivLev2);
				var oSpanPrice1 = new Element("span").addClass("price2").injectInside(oDivSubstraction).setHTML(arrPrixEuro[i] + " &euro;");
					if((arrreductionCaisse[i]!="0") && (arrreductionCaisse[i]!=""))
					{
							var oPEnCaisse = new Element("p").addClass("enCaisse").injectInside(oDivSubstraction).setHTML("- " + arrreductionCaisse[i] + " &euro; en caisse=");	
					}	
				}
		}
			
		/* price + unit+ ecopartipation after all calculation*/	
		/*gestion of space for UL*/
						
			var nbrCpt = 1;
			if (arrecoParticipation[i]!="")	{nbrCpt = nbrCpt +  1;}
				if (arrLabelPrix[i]!="") {nbrCpt = nbrCpt + 1;}
				var nbrUL="";		
				if (nbrCpt == 1) {nbrUL = "uneLigne";}
					
				if (nbrCpt == 2) {nbrUL ="deuxLignes";}
							
				if (nbrCpt == 3) {nbrUL ="troisLignes";}
				
		var oDivTotalPrice = new Element("div").injectInside(oDivLev2);
		
		var oUlTotalPrice = new Element("ul").addClass(nbrUL).injectInside(oDivTotalPrice);
		var withunit = !(arrLabelPrix[i]==""); 
		if (withunit) {
		var oLiTotalUnit= new Element("li").addClass("unit").injectInside(oUlTotalPrice).setHTML(arrLabelPrix[i]);}
		var oLiTotalPrice = new Element("li").addClass("price").injectInside(oUlTotalPrice).setHTML(arrPrixSoustrait[i] + " &euro;");
		var oLiEcoParticipation = new Element("li").addClass("ecoParticipation").injectInside(oUlTotalPrice).setHTML(arrecoParticipation[i]);	
		
		
	}
	//
	// slide control
	var nIndex = 0;
	var aProduct = oContainer.getChildren();
	var oSlider =$("galleryProductsInner");
	oSlider.scrollTo(0, 0);
	oSlider.fx = new Fx.Scroll(oSlider, {
		duration: 600,
		transition: Fx.Transitions.Quad.easeInOut
	});
	//
	$("btnNext").addEvent('click', function(event) {
		new Event(event).stop();
		nIndex = Math.min(nIndex+3, aProduct.length-3);
		if (aProduct[nIndex]) {
			oSlider.fx.stop().toElement(aProduct[nIndex]);
		}
	});
	$("btnPre").addEvent('click', function(event) {
		new Event(event).stop();
		nIndex = Math.max(0,nIndex-3);
		if (aProduct[nIndex]) {
			oSlider.fx.toElement(aProduct[nIndex]);
		}
	});
}
//
window.addEvent("load", function(e){
	
	if(arrreductionCaisse.length != 0)
	{
		fHomeSlideShow();
	}
	else
	{
			document.getElementById("mention").style.display="none"
			document.getElementById("galleryProducts").style.display="none"
		
	}
});