/******************************************************/
//This Lib is Provide As IF By S.Bernard and result of
//compatibility module for Xiti All Tag In One project
/******************************************************/


var xtnv = "";
var xtsd = "";
var xtsite = "";
var xtn2 = "";
var xtpage = "";
var xtmcl = "";
var xt_npg = "";

function XitiProject(vsXTSD,vsXTSITE)
{
	//Variables Xiti
	xtsd = vsXTSD;
	xtsite = vsXTSITE;
	
	//Variables permettant de gerer les chapitrages et niveau 2 du site.
	this.arraySiteLevel2Name = new Array();
	this.arraySiteLevel2ID = new Array();
	this.arraySearchName = new Array();
	this.arraySearchID = new Array();
	
	//Les informations propres à l'URL
	this.vsRealURL = ""+document.location;
	this.vsURL = this.vsRealURL;
	this.arrayURL = null;
	this.vsParameter = '';

	//Les informations propres au moteur de recherche
	this.vsSearchTerm = ''; //terme recherche
	this.viIDUniversSeach = ''; //univers de recherche
	this.viNbResult = ''; //nombre de resultat


	//Informations propre a Xiti
	this.vsLevel = 99; // Par defaut le site de niveau 2 est 99 soit autre.
	this.vsPage = ""; //Le chapitrage de la page ou la page elle meme.

	//**********************//
	// FONCTIONS 'EXTERNES' //
	//**********************//
	
	//Cette fonction permet de 'rendre conforme' une URL.
	this.URLNormalize = function(vsURL){ try { return DoXiti_URLNormalize(vsURL); } catch(e) { return vsURL; } }
	
	//Cette fonction permet d'extraire les informations du moteur de recherche
	this.extractRechercheInfo = function () { try { DoXiti_GetSearchParam(this); } catch(e) {}; }

	//Parametrage des URLs clés.
	this.paramURLsKey = function() { try { DoXiti_paramURLsKey(this); } catch(e) {}; }
	
	//Paramétrage des index du moteur de recherche
	this.paramIndexSearch = function() { try { DoXiti_paramIndexSearch(this); } catch(e) {}; }

	//Cette fonction permet d'ajouter des regles lors de la découpe d'une URL spécifique au site.
	this.arrayForUrlExtra = function(name,vsParameter,index,maxi) { try { return DoXiti_ArrayForUrlExtra(name,vsParameter,index,maxi); } catch(e) { return name; }; }


	//*************************//
	// FONCTIONS REUTILISABLES //
	//*************************//

	//retrait des espaces sur une chaine.
	this.trim = function (vsString) { return vsString.replace(/^\s+|\s+$/, ''); };

	//formalisation d'une chaine de caractere (remplace les accents)
	this.formaliseString = function (vsString) { eval(unescape("%76%73%53%74%72%69%6E%67%3D%76%73%53%74%72%69%6E%67%2E%72%65%70%6C%61%63%65%28%2F%5B%E0%E1%E2%E3%E4%E5%E6%5D%2F%67%69%2C%22%61%22%29%3B%20%76%73%53%74%72%69%6E%67%3D%76%73%53%74%72%69%6E%67%2E%72%65%70%6C%61%63%65%28%2F%5B%E8%E9%EA%EB%5D%2F%67%69%2C%22%65%22%29%3B%20%76%73%53%74%72%69%6E%67%3D%76%73%53%74%72%69%6E%67%2E%72%65%70%6C%61%63%65%28%2F%5B%EC%ED%EE%EF%5D%2F%67%69%2C%22%69%22%29%3B%20%76%73%53%74%72%69%6E%67%3D%76%73%53%74%72%69%6E%67%2E%72%65%70%6C%61%63%65%28%2F%5B%F2%F3%F4%F5%F6%F8%5D%2F%67%69%2C%22%6F%22%29%3B%20%76%73%53%74%72%69%6E%67%3D%76%73%53%74%72%69%6E%67%2E%72%65%70%6C%61%63%65%28%2F%5B%F9%FA%FB%FC%5D%2F%67%69%2C%22%75%22%29%3B%20%76%73%53%74%72%69%6E%67%3D%76%73%53%74%72%69%6E%67%2E%72%65%70%6C%61%63%65%28%2F%5B%E7%5D%2F%67%69%2C%22%63%22%29%3B%20%76%73%53%74%72%69%6E%67%3D%76%73%53%74%72%69%6E%67%2E%72%65%70%6C%61%63%65%28%2F%5B%FF%FD%FE%5D%2F%67%69%2C%22%79%22%29%3B%20%76%73%53%74%72%69%6E%67%3D%76%73%53%74%72%69%6E%67%2E%72%65%70%6C%61%63%65%28%2F%5B%F1%5D%2F%67%69%2C%22%6E%22%29%3B%20%76%73%53%74%72%69%6E%67%3D%76%73%53%74%72%69%6E%67%2E%72%65%70%6C%61%63%65%28%2F%20%2F%67%69%2C%22%5F%22%29%3B")); return vsString; }
	
	//Cette fonction permet de décoder un texte en UTF-8
	this._utf8_decode = function (utftext) { var string = ""; var i = c = c1 = c2 = 0;  while ( i < utftext.length )  { c = utftext.charCodeAt(i); if (c < 128)  { string += String.fromCharCode(c); i++; }  else  if((c > 191) && (c < 224))  { c2 = utftext.charCodeAt(i+1); string += String.fromCharCode(((c & 31) << 6) | (c2 & 63)); i += 2; }  else  { c2 = utftext.charCodeAt(i+1); c3 = utftext.charCodeAt(i+2); string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63)); i += 3; } } return string; }

	//****************************//
	// LES FONCTIONS DE LA CLASSE //
	//****************************//
	
	//Cette fonction permet de retourner un tableau en rapport à une chaine de caractere avec pour separateur un '/'.
	this.newArrayForUrl = function (vsString) 
	{ 
		var arrayTmp = vsString.split('/');  
		var arrayReturn = new Array();  
		
		for(i=2;i<arrayTmp.length;i++) 
		{ 
			vsArray2 = arrayTmp[i].split('?'); 
			if(vsArray2.length > 1) this.vsParameter = this.trim(unescape(vsArray2[1]));  
			
			arrayReturn[i-2] = this.arrayForUrlExtra(this.trim(vsArray2[0]),this.vsParameter,i,arrayTmp.length);
		} 
		
		return arrayReturn; 
	}


	//Cette fonction permet de retourner la page avec au besoin le chapitrage.
	this.initPage = function()
	{
		this.vsPage = '';
		
		try
		{
			this.vsPage = '';
			for(i=2;i<this.arrayURL.length;i++)
			{
				if(this.arrayURL[i] != '')
				{
					if(this.vsPage != '')
						this.vsPage = this.vsPage+'::';
				
					this.vsPage = this.vsPage+this.arrayURL[i];
				}
			}
			
			this.extractRechercheInfo();
			
			if(this.vsPage == '')
			{
				if(this.vsLevel == 99)
					this.vsPage = document.title;
				else
					this.vsPage = 'Accueil';
			}
		}
		catch(e) { this.vsPage = 'Accueil';}

		this.vsPage = this.formaliseString(this.vsPage);
	}

	//Cette méthode à pour finalité d'initialiser l'objet.
	this.init = function () 
	{
		//Nous considérons que l'url ne doit pas prendre en compte certaines informations.
		this.vsURL = this.URLNormalize(this.vsURL);
		
		//Nous initialisons les URLs clés qui permettent de remonter page, niveau et chapitrage.
		this.paramURLsKey();
		
		//Nous initialisons les index clés sur le moteur de recherche
		this.paramIndexSearch();
		
		//Nous construisons le tableau de reference des URLS.
		this.arrayURL = this.newArrayForUrl(this.vsURL);
		
		//L'ensemble des objets etant initialisé, nous recherchons le numero de site de niveau 2
		this.initLevel();

		//Nous initialisons le nom de la page et son chapitrage.
		this.initPage();
	}
	
	
	//Cette fonction initialise le numero du site de niveau 2
	this.initLevel = function ()
	{
		//Par defaut le niveau est 99 (autre)
		this.vsLevel = 99;
		try
		{
			//Nous cherchons le niveau à atteindre.
			for(i=0;i<this.arraySiteLevel2Name.length;i++)
			{
				vsCmp = (this.arrayURL[1] == '')?this.arrayURL[0]:this.arrayURL[1];
				if(vsCmp == this.arraySiteLevel2Name[i]) { this.vsLevel = this.arraySiteLevel2ID[i]; i = this.arraySiteLevel2Name.length; }
			}
		}
		catch(e) { this.vsLevel = 99; }
	}

	//Cette fonction permet de mettre en place le tag Xiti.
	this.DoXiti = function() 
	{ 
		xtnv = document;
		xtn2 = this.vsLevel;
		xtpage = this.vsPage;
		
		//Intégration du moteur de recherche si nous sommes dans la recherche (var vsSearchTerm)
		//do not modify below 
		if(this.vsSearchTerm != '')
		{
			//positionnement du moteur de recherche
			xt_form = "&f1="+this.vsSearchTerm+"&f2="+this.viIDUniversSeach+"&f3="+this.viNbResult+"&f4=&f5=&f6=&f7=&f8=&f9=&f10="; 
		
			if (window.xtparam!=null)
			{
				window.xtparam+=xt_form;
			} 
			else
			{
				window.xtparam = xt_form;
			};
		
			xt_mtcl = this.vsSearchTerm;        //keyword value
			xt_npg = this.viNbResult;        //result page number (0 when no result)
			//do not modify below
			if (window.xtparam!=null){window.xtparam+="&mc="+xt_mtcl+"&np="+xt_npg;}
			else{window.xtparam ="&mc="+xt_mtcl+"&np="+xt_npg;};
		}
		//end do not modify
	}

	//Cette fonction permet d'indiquer un nouveau site de niveau 2.
	this.addLevel = function (name,index) { var i = this.arraySiteLevel2Name.length; this.arraySiteLevel2Name[i] = name; this.arraySiteLevel2ID[i] = index; }
	
	//Cette fonction permet d'indiquer un univers de recherche
	this.addSearchUnivers = function (name,index) { var i = this.arraySearchName.length; this.arraySearchName[i] = name; this.arraySearchID[i] = index; }
	
	//***********************//
	//LANCEMENT DE LA CLASSE //
	//***********************//

	//Initialisation de l'objet
	this.init();

	//Une fois l'objet configuré, nous pouvons appeler notre méthode.
	this.DoXiti();
}

//Normalisation de l'URL -> nous retirons certaines informations.
function DoXiti_URLNormalize(vsURL)
{
	vsURL = vsURL.replace("/portal/site/carrefour/","/");
	return vsURL;
}

//Cette fonction est appellé par l'objet Xiti elle permet de mettre en place les index du moteur de recherche
function DoXiti_paramIndexSearch(obj)
{
	//Nous positionnons les noms des index du moteur de recherche
	obj.addSearchUnivers("",0);
	obj.addSearchUnivers("1",1);
	obj.addSearchUnivers("CASSURENLIGNE",2);
	obj.addSearchUnivers("CDIRECTENLIGNE",3);
	obj.addSearchUnivers("CFRENLIGNE",4);
	obj.addSearchUnivers("CMOBILEENLIGNE",5);
	obj.addSearchUnivers("CONLINEENLIGNE",6);
	obj.addSearchUnivers("CPASSENLIGNE",7);
	obj.addSearchUnivers("CSPECTENLIGNE",8);
	obj.addSearchUnivers("CVOYAGESENLIGNE",9);
	obj.addSearchUnivers("VODENLIGNE",10);
	obj.addSearchUnivers("2",11);
	obj.addSearchUnivers("CFRCATALOGUES",12);
	obj.addSearchUnivers("CFRPROMOTION",13);
	obj.addSearchUnivers("CFREVENMENT",14);
	obj.addSearchUnivers("CFRMAGASIN",15);
	obj.addSearchUnivers("3",16);
}

//Cette fonction est appellé par l'objet Xiti elle permet de mettre en place les URLs de recherche
function DoXiti_paramURLsKey(obj)
{
	//Nous positionnons les URLs clés [Autres -> 99]
	obj.addLevel('www.carrefour.fr',1);
	obj.addLevel('baby_kids',2);
	obj.addLevel('comptefidelite',3);
	obj.addLevel('contact',4);
	obj.addLevel('culture_loisir',5);
	obj.addLevel('developpement_durable',6);
	obj.addLevel('en-ce-moment',7);
	obj.addLevel('espaceperso',8);
	obj.addLevel('espacetraiteur',9);
	obj.addLevel('faq',10);
	obj.addLevel('adherer_en_ligne',11);
	obj.addLevel('infoconso',12);
	obj.addLevel('innovation-multimedia',13);
	obj.addLevel('legal',14);
	obj.addLevel('plus-carrefour',15);
	obj.addLevel('magasin',16);
	obj.addLevel('maison-decoration',17);
	obj.addLevel('newsletter',18);
	obj.addLevel('piedpage',19);
	obj.addLevel('presse',20);
	obj.addLevel('programme-fidelite',21);
	obj.addLevel('promotions-catalogues',22);
	obj.addLevel('services-en-magasin',23);
	obj.addLevel('solidarite',24);
	obj.addLevel('produits',25);
}


//Cette fonction permet de remonter les informations du moteur de recherche
function DoXiti_GetSearchParam(obj)
{
		
	var vbIsRecherche = 0;

	if(obj.vsLevel == 99)
	{
		if(obj.arrayURL[1] == 'recherche')
		{
			vbIsRecherche=1;
		}
	}

	if(vbIsRecherche == 1)
	{
		var strSite = "";
		arrayTmp = obj.vsParameter.split('&');

		try
		{
			for(i=0;i<arrayTmp.length;i++)
			{
				arrayTmp2 = arrayTmp[i].split('=');
				if(arrayTmp2[0] == 'crText')
				{
					obj.vsSearchTerm = obj.formaliseString(obj._utf8_decode(arrayTmp2[1])); //terme
				}
				else
				if(arrayTmp2[0] == 'crSite')
				{
					strSite = arrayTmp2[1]; //univers
				}
			}
		}
		catch(e) { obj.vsSearchTerm = ''; }

		try
		{
			for(i=0;i<obj.arraySearchName.length;i++)
			{
				if(strSite == obj.arraySearchName[i])
				{
					obj.viIDUniversSeach = obj.arraySearchID[i];
					i = obj.arraySearchName.length;
				}
			}
		}
		catch(e) { obj.viIDUniversSeach = '0'; }

		try
		{
			var vsStringTmp = viewResultConteneur.innerText;
			viDep = vsStringTmp.indexOf('trouv');
			viFin = viDep;

			for(i=viDep;i>=0;i--)
			{
				if(vsStringTmp.charAt(i) == 'r')
				{
					viDep = i+1;
					i = -1;		
				}
			}

			obj.viNbResult = obj.trim(vsStringTmp.substr(viDep,(viFin-viDep)));
		}
		catch(e) { obj.viNbResult='0';}
	}
}

var objXiti = null;
if(document.location.protocol == 'http:')
{
	objXiti = new XitiProject("http://logc174","437171");
}
else
{
	objXiti = new XitiProject("https://logs128","437171");
}


/************************/
//  Part XTCLIKS72
/************************/

//-- AT Ergonomics A-C 3.1.005 -- Copyright 2008 AT INTERNET, All Rights Reserved.
//-- (to be used with AT Tag 3.3.001 or later)
var scriptOnClickZone=2,xtczv='31005',xtdtmp=0,xtdt2=new Date(),xtel=new xtE(xtdt2.getTime()),xtn=navigator,un=undefined,nu=null,cZ='xtcz',oC='onclick',cL='xtclib',cT='xtcltype',tR=true,fA=false,isI=(/MSIE/.test(xtn.userAgent))?tR:fA,isOP=(/Opera/.test(xtn.userAgent))?tR:fA,isS=(/Safari/.test(xtn.userAgent))?tR:fA,isM=(xtn.appVersion.indexOf('Mac',0)>=0)?tR:fA,larg=0,haut=0,hit=fA,xt_perdz=nu,fO=fA;
function xtIdxOf(tab,v,n){n=(n==nu)?0:n;var m=tab.length;for(var i=n;i<m;i++)if(tab[i]==v)return i;return -1;}
function xtT(xl){if(xl.innerHTML){var xtx=xl.innerHTML,r=new RegExp('<script[^>]*>','gi');if(r.test(xtx.toString()))return fA;xtx=(xtx.toString()).replace(/<\/?[^>]+>/gi,'');var regex=new RegExp('(&nbsp;)','g');xtx=(xtx.toString()).replace(regex,'');xtx=xtEn(xtx);var regex2=new RegExp('(%C2%A0)','g');xtx=(xtx.toString()).replace(regex2,'');try{xtx=xtDe(xtx)}catch(e){}xtx=(xtx.toString()).replace(/[\s]/gm,'');if((xtx.length==0)||(xtx==un)||(xtx==nu))return fA;return xtx}return fA}
function xtE(st){this.xttab=new Array();this.xst=st;this.xc=0;this.yc=0;this.sx=0;this.sy=0;this.px=0;this.py=0;this.xr=0;this.yr=0;this.bf='';this.af='';this.curr='';this.cliccz='';this.dest='';this.s=0;this.pcz='';this.s2cz='';this.t=0;this.idmod=0;this.p='';this.s2='';this.idpage=-1}
function xtH(){var d2=new Date();return(d2.getTime()-xtel.xst)}
function xtC(e){if(!e){if(xw.event){e=xw.event}else{return{x:-1,y:-1}}}if(typeof(e.pageX)=='number'){xc=e.pageX;yc=e.pageY}else if(typeof(e.clientX)=='number'){xc=e.clientX;yc=e.clientY;var bad=(xw.xtn.userAgent.indexOf('Opera')+1)||(xw.ScriptEngine&&ScriptEngine().indexOf('InScript')+1)||(xtn.vendor=='KDE');if(!bad){if(xd.body&&(xd.body.scrollLeft||xd.body.scrollTop)){xc+=xd.body.scrollLeft;yc+=xd.body.scrollTop;}else if(xd.documentElement&&(xd.documentElement.scrollLeft||xd.documentElement.scrollTop)){xc+=xd.documentElement.scrollLeft;yc+=xd.documentElement.scrollTop;}}}else{return{x:-1,y:-1}}return{x:xc,y:yc}}
function xtAs(xl){var rect=xtAb(xl),width=rect.right-rect.left,height=rect.bottom-rect.top;return{w:width,h:height}}
function xtAb(xl){if(xl['rectDefined'])return{left:xl.rLeft,top:xl.rTop,right:xl.rRight,bottom:xl.rBottom};if(!xl.shape)xl.shape='rect';var coords=xl.coords.split(','),result;if(xl.shape.toLowerCase()=='rectangle'||xl.shape.toLowerCase()=='rect'){result={left:parseInt(coords[0]),top:parseInt(coords[1]),right:parseInt(coords[2]),bottom:parseInt(coords[3])}}if(xl.shape.toLowerCase()=='circle'||xl.shape.toLowerCase()=='circ'){result={left:parseInt(coords[0])-parseInt(coords[2]),top:parseInt(coords[1])-parseInt(coords[2]),right:parseInt(coords[0])+parseInt(coords[2]),bottom:parseInt(coords[1])+parseInt(coords[2])}}if(xl.shape.toLowerCase()=='polygon'||xl.shape.toLowerCase()=='poly'){var l_ex,t_ex,r_ex,b_ex;for(var i=0;i<coords.length;i+=2){var co=parseInt(coords[i]);if(l_ex==un||co<l_ex)l_ex=co;if(r_ex==un||co>r_ex)r_ex=co;}for(var i=1;i<coords.length;i+=2){var co=parseInt(coords[i]);if(t_ex==un||co<t_ex)t_ex=co;if(b_ex==un||co>b_ex)b_ex=co;}result={left:l_ex,top:t_ex,right:r_ex,bottom:b_ex}}xl.rectDefined=tR;xl.rLeft=result.left;xl.rRight=result.right;xl.rTop=result.top;xl.rBottom=result.bottom;return result}
function xtAp(area,target){var map=xtpN(area);if(!map.dstElement){if(!target){target=xd;}var elts=target.getElementsByTagName('*');if(elts['toArray'])elts=elts.toArray();for(var i=0;i<elts.length;i++){var xl=elts[i];if(xl.useMap){if(xl.useMap.replace('#','')==map.name)break;}xl=nu;}map.dstElement=xl;}if(map.dstElement){var basePx=xtP(map.dstElement,'Left'),basePy=xtP(map.dstElement,'Top'),rect=xtAb(area);return{x:(basePx+rect.left),y:(basePy+rect.top)}}else{return{x:-1,y:-1}}}
function xtSx(xl){var szx=(xtnN(xl)=='AREA')?xtAs(xl).w:xl.offsetWidth;if((szx==nu)||(szx==un)||(szx=='')){if(xl.style.width!=nu)szx=xl.style.width;else szx=0;}if(isS&&isM&&(xtnN(xl)=='TR')&&(xl.firstChild!=nu)&&(xl.lastChild!=nu))szx=xl.lastChild.offsetLeft+xl.lastChild.offsetWidth-xl.firstChild.offsetLeft;if((szx==nu)||(szx==un)||(szx=='')){szx=0;}return parseInt(szx,10)}
function xtSy(xl){var szy=(xtnN(xl)=='AREA')?xtAs(xl).h:xl.offsetHeight;if((szy==nu)||(szy==un)||(szy=='')){if(xl.style.height!=nu)szy=xl.style.height;else szy=0;}if(isS&&isM&&(xtnN(xl)=='TR')&&(xl.firstChild!=nu)&&(xl.lastChild!=nu))szy=xl.lastChild.offsetTop+xl.lastChild.offsetHeight-xl.firstChild.offsetTop;if((szy==nu)||(szy==un)||(szy=='')){szy=0;}return parseInt(szy,10)}
function xtPx(xl){var psx=(xtnN(xl)=='AREA')?xtAp(xl).x:xtP(xl,'Left');return parseInt(psx,10)}
function xtPy(xl){var psy=(xtnN(xl)=='AREA')?xtAp(xl).y:xtP(xl,'Top');if(isS&&isM&&(xtnN(xl)=='TR')&&(xl.firstChild!=nu))psy+=xl.firstChild.offsetTop;return parseInt(psy,10)}
function xtP(oEl,inTYPE){if(typeof(oEl.offsetParent)!=un){var sType='oEl.offset'+inTYPE;for(var iVal=0;oEl;oEl=oEl.offsetParent){iVal+=eval(sType);}return iVal;}else{if(inTYPE=='Left')return oEl.x;if(inTYPE=='Top')return oEl.y;}return-1}
function xtCt(xl){var xclict='',xext='',xurld='',xurls=(xd.location.href).toString(),xdoms=(xw.xt1!=nu&&xw.xt1!='')?xtSub(xw.xt1,8,xw.xt1.length):xtSub(xurls,7,xurls.indexOf('/',7)),xdomd='';if(xl){if((xtnN(xl)=='INPUT')&&(xl.type=='submit')){try{xurld=xtG(xl.form,'action').toString();}catch(e){}xext=xtSub(xurld,xurld.lastIndexOf('.'),xurld.length);if(xurld.indexOf('http://',0)>=0)xdomd=xtSub(xurld,7,xurld.indexOf('/',7));else xdomd='';}if((xtnN(xl)!='IMG')&&xl.href){xurld=(xl.href).toString();xext=xtSub(xurld,xurld.lastIndexOf('.'),xurld.length);if(xurld.indexOf('http://',0)>=0)xdomd=xtSub(xurld,7,xurld.indexOf('/',7));else xdomd='';}else{var xlp=xtpN(xl);while(xlp&&xtnN(xlp)!='BODY'){if(xlp.href){xurld=(xlp.href).toString();xext=xtSub(xurld,xurld.lastIndexOf('.'),xurld.length);if(xurld.indexOf('http://',0)>=0)xdomd=xtSub(xurld,7,xurld.indexOf('/',7));else xdomd='';break;}xlp=xtpN(xlp);}}}if(xtEx(xext))xclict='T';else if((xdomd!='')&&(xdomd.indexOf(xdoms,0)<0))xclict='S';else{xclict='N';}if(xurld.length>255)xurld=xtSub(xurld,0,255);var ch=xurld;if(ch.charAt(ch.length-1)=='/')ch=xtSub(ch,0,ch.length-1);var pos=ch.lastIndexOf('/?',ch.length);if(pos>=0)ch=ch.replace('/?','?');xurld=ch;try{if((xtDe(xurld)!=nu)&&(xtDe(xurld)!=un))xurld=xtDe(xurld);}catch(e){}return{typ:xclict,url:xurld}}
function xtEx(xext){var valext=['.aac','.ace','.ape','.art','.avi','.bak','.bat','.bin','.bmp','.bsp','.cab','.ccd','.cda','.chm','.clp','.css','.csv','.cue','dic','dll','.doc','.dot','.exe','.fla','.flac','.gif','.gz','.hlp','.ico','.img','.iso','.jpeg','.jpg','.js','.lnk','.m2a','.m2v','.m3u','.mdb','.mdf','.mds','.mid','.midi','.mkv','.mod','.mov','.mp2','.mp3','.mp4','.mpc','.mpg','.mpeg','.msi','.nfo','.nrg','.obd','.ocx','.ogg','.old','.ogm','.pdf','.png','.pps','.ppt','.psd','.psp','.rar','.raw','.reg','.rm','.ram','.rtf','.swf','.tar','.tga','.tgz','.theme','.tif','.tiff','.tmp','.torrent','.ttf','.txt','.url','.vbs','.vob','.wab','.wav','.wdb','.wks','.wml','.wma','.wmv','.wpf','.xls','.xml','.zip','.7z'];for(var i=0;i<valext.length;i++){if(xext==valext[i])return tR;}return fA}
function xtL(xl){var xlel=['INPUT','SELECT','IFRAME','OBJECT','AREA','BUTTON'],xeln=xtnN(xl);if((xeln=='EMBED')&&(xtnN(xtpN(xl)))!='OBJECT'){return tR;}if((xeln=='DIV')&&(xtG(xl,cL))){return tR;}if((xeln=='SPAN')&&(xtG(xl,oC))){return tR;}if((xeln=='SELECT')&&((xtG(xl,'onchange')==nu)||(xtG(xl,'onchange')==un)||(xtG(xl,'onchange')==''))){return fA;}if((xeln=='INPUT')&&(xl.type!='submit')&&(xl.type!='image')&&(xl.type!='button')){return fA;}if((xeln=='BUTTON')&&(xl.type!='submit')){return fA;}if(xeln=='IMG'){var xlp=xtpN(xl);var xtTr=fA;while(xlp){if(xtnN(xlp)=='A'){xtTr=tR;if((((xtG(xlp,oC)!=nu)&&(xtG(xlp,oC)!=un)&&(xtG(xlp,oC)!=''))||((xtG(xlp,'href')!=nu)&&(xtG(xlp,'href')!=un)&&(xtG(xlp,'href')!='')))){return tR;}}xlp=xtpN(xlp);}if((xtG(xl,oC)!=nu)&&(xtG(xl,oC)!=un)&&(xtG(xl,oC)!='')&&!xtTr)return tR;}if((xeln=='A')&&(((xtG(xl,oC)!=nu)&&(xtG(xl,oC)!=un)&&(xtG(xl,oC)!=''))||((xtG(xl,'href')!=nu)&&(xtG(xl,'href')!=un)&&(xtG(xl,'href')!='')))){var xlp=xtpN(xl);while(xlp){if(((xtnN(xlp)=='DIV')&&(xtG(xlp,cL)))||((xtnN(xlp)=='SPAN')&&(xtG(xlp,oC))))return fA;xlp=xtpN(xlp);}var xtImg=fA,xtTxt=fA;if(xl.childNodes){var xtChild=xl.childNodes,j=0;while(j<xtChild.length&&!(xtImg&&xtTxt)){if(xtnN(xtChild[j])=='IMG'){xtImg=tR;}else{xtTxt=tR;}j++;}}if(xtImg&&xtTxt) return tR;else if(xtImg) return fA;else return tR;}for(var i=0;i<xlel.length;i++){if(xeln==xlel[i])return tR;}return fA}
function xtLCz(xl){var xlel=['DIV','TABLE','TR','TD','UL','LI'],xeln=xtnN(xl);for(var i=0;i<xlel.length;i++){if((xeln==xlel[i])&&(xl.nodeType==1)&&(xtG(xl,cZ)!=nu)&&(xtG(xl,cZ)!=un)){return tR;}}return fA;}
function xtNa(xl){if(xl&&!xtL(xl)){var xlp=xtpN(xl);while(xlp){if(xtL(xlp)){xl=xlp;break;}xlp=xtpN(xlp);}}var xid='',xtmedat='',xtmedp='',xtmeds='',type='',xurld='',xtTr=fA;if(xl){if((xtnN(xl)=='A')&&(xl.childNodes)){var xtChild=xl.childNodes,j=0;while((j<xtChild.length)&&!xtTr){if((xtChild[j].nodeType==1)&&!xtL(xtChild[j])&&(xtG(xtChild[j],cL))){xid=cL+xtG(xtChild[j],cL);xtTr=tR;}j++;}}if(!xtTr){if(xtG(xl,oC)&&xtV(xtG(xl,oC)))xid=xtG(xl,oC);else if(xl.href&&xtV(xl.href)){xid=xl.href;try{if((xtDe(xid)!=nu)&&(xtDe(xid)!=un))xid=xtDe(xid);}catch(e){}}else if(xtG(xl,cL))xid=cL+xtG(xl,cL);else if(xtT(xl))xid=xtT(xl);else if(xl.id)xid=xl.id;else if(xl.name)xid=xl.name;else if(xl.title)xid=xl.title;else if(xl.value)xid=xl.value;else if(xl.href){var ch=xl.href.toString();if(ch.charAt(ch.length-1)=='/')ch=xtSub(ch,0,ch.length-1);var pos=ch.lastIndexOf('/?',ch.length);if(pos>=0)ch=ch.replace('/?','?');xid=ch;try{if((xtDe(xid)!=nu)&&(xtDe(xid)!=un))xid=xtDe(xid);}catch(e){}}else if(xl.src){var ch=xl.src.toString();if(ch.charAt(ch.length-1)=='/')ch=xtSub(ch,0,ch.length-1);var pos=ch.lastIndexOf('/?',ch.length);if(pos>=0)ch=ch.replace('/?','?');xid=ch;try{if((xtDe(xid)!=nu)&&(xtDe(xid)!=un))xid=xtDe(xid);}catch(e){}}else{var xlp=xtpN(xl);while(xlp&&xtnN(xlp)!='BODY'){if(xtG(xlp,oC)&&xtV(xtG(xlp,oC))){xid=xtG(xlp,oC);break;}else if(xlp.href&&xtV(xlp.href)){xid=xlp.href;try{if((xtDe(xid)!=nu)&&(xtDe(xid)!=un))xid=xtDe(xid);}catch(e){}break;}else if(xtG(xl,cL)){xid=cL+xtG(xl,cL);break;}else if(xtT(xlp)){xid=xtT(xlp);break;}else if(xlp.id){xid=xlp.id;break;}else if(xlp.name){xid=xlp.name;break;}else if(xlp.title){xid=xlp.title;break;}else if(xlp.value){xid=xlp.value;break;}else if(xlp.href){var ch=xlp.href.toString();if(ch.charAt(ch.length-1)=='/')ch=xtSub(ch,0,ch.length-1);var pos=ch.lastIndexOf('/?',ch.length);if(pos>=0)ch=ch.replace('/?','?');xid=ch;try{if((xtDe(xid)!=nu)&&(xtDe(xid)!=un))xid=xtDe(xid);}catch(e){}break;}else if(xlp.src){var ch=xlp.src.toString();if(ch.charAt(ch.length-1)=='/')ch=xtSub(ch,0,ch.length-1);var pos=ch.lastIndexOf('/?',ch.length);if(pos>=0)ch=ch.replace('/?','?');xid=ch;try{if((xtDe(xid)!=nu)&&(xtDe(xid)!=un))xid=xtDe(xid);}catch(e){}break;}xlp=xtpN(xlp);}}}xid=(xid.toString()).replace(/[\s]/gm,'');if(xtV(xid)){type=xtExT(xid).typ;xtmedp=xtExT(xid).pag;xtmeds=xtExT(xid).sec;if(xtG(xl,cL))xid=cL+xtG(xl,cL);else xid=xtExT(xid).pagcl;}else{var xlp=xtpN(xl);while(xlp&&(xtnN(xlp)!='BODY')){if(xtG(xlp,oC)&&xtV(xtG(xlp,oC))){xtmedat=xtG(xlp,oC);xtmedat=(xtmedat.toString()).replace(/[\s]/gm,'');type=xtExT(xtmedat).typ;xtmedp=xtExT(xtmedat).pag;xtmeds=xtExT(xtmedat).sec;break;}else if(xlp.href&&xtV(xlp.href)){xtmedat=xlp.href;try{if((xtDe(xtmedat)!=nu)&&(xtDe(xtmedat)!=un))xtmedat=xtDe(xtmedat);}catch(e){}xtmedat=(xtmedat.toString()).replace(/[\s]/gm,'');type=xtExT(xtmedat).typ;xtmedp=xtExT(xtmedat).pag;xtmeds=xtExT(xtmedat).sec;break;}xlp=xtpN(xlp);}}if(type==''){if(xtG(xl,cT)!=nu){type=xtG(xl,cT);}else{var xlp=xtpN(xl);while(xlp&&(xtnN(xlp)!='BODY')){if(xtG(xlp,cT)!=nu){type=xtG(xlp,cT);break;}xlp=xtpN(xlp);}}}if(type=='')type=xtCt(xl).typ;xurld=xtCt(xl).url;if(xtmedp==''){xtmedp=(xid.indexOf(cL,0)>=0)?xtExTc(xid):xid;if(xtmeds=='')xtmeds=xtEx2(xw.xt9);}var regex=new RegExp('(&)|[?]','g');xtmedp=(xtmedp.toString()).replace(regex,'_');if(xtmeds=='')xtmeds='0';var r=new RegExp('(::)','g');xid=(xid.toString()).replace(r,'/');var k=0,xlp=xtpN(xl);while(xlp&&(xtnN(xlp)!='BODY')){if(xtLCz(xlp)&&(k<2)){xid=xtG(xlp,cZ)+'::'+xid;k+=1;}xlp=xtpN(xlp);}}if((xid.length>255)&&(k==0))xid=xtSub(xid,0,255);xtmedp=xtSub(xtmedp,0,255);return{id:xid,pag:xtmedp,sec:xtmeds,typ:type,url:xurld}}
function xtTr(xl){var xut=['BODY','HTML'];for(var i=0;i<xut.length;i++){if(xl.tagName==xut[i])return fA;}return tR}
function xtExTc(xid){var r=new RegExp(cL,'gi');xid=xid.replace(r,'');return xid}
function xtEx2(xat){var r=new RegExp('&s2=','gi');if(r.test(xat.toString())){xat=xat.replace(r,'');}return xat}
function xtV(xat){var r=new RegExp('xt_med','gi'),r2=new RegExp('xt_click','gi');if(r.test(xat.toString())||r2.test(xat.toString()))return tR;return fA}
function xtExT(xat){var page='',pageclz='',type='',section=-1,idx=(xat.indexOf('xt_med')>=0)?0:1,IndexStart=(idx==0)?xat.indexOf('xt_med'):xat.indexOf('xt_click'),start=xat.indexOf('(',IndexStart),stop=xat.indexOf(')',IndexStart),content=xtSub(xat,start+1,stop),reg1=new RegExp('[,]','g');xat=content.split(reg1);if(xat[idx+1]){section=xtSub(xat[idx+1],1,xat[idx+1].length-1);}if(xat[idx+2]){page=xtSub(xat[idx+2],1,xat[idx+2].length-1);}if(xat[idx+3]){type=xtSub(xat[idx+3],1,xat[idx+3].length-1);}else if(xat[idx]){type=xtSub(xat[idx],1,xat[idx].length-1);}section=(section!=nu)?section:'';page=(page!=nu)?page:'';type=(type!=nu)?type:'';var reg2=new RegExp('(::)','g');pageclz=(page.toString()).replace(reg2,'/');return{typ:type,pag:page,pagcl:pageclz,sec:section}}
function xtNodesload(){if(xw.xt8!=0){var desc=xd.getElementsByTagName('*');for(var i=0;i<desc.length;i++){if(xtL(desc[i]))xtel.xttab.push(desc[i]);}for(var j=0;j<xtel.xttab.length;j++){if(xd.addEventListener)xtel.xttab[j].addEventListener('mousedown',xtR,fA);else if(xd.attachEvent)xtel.xttab[j].attachEvent('onmousedown',xtR);}xtResol();if(xw.addEventListener){xw.addEventListener('scroll',xtResol,fA);xw.addEventListener('beforeunload',xtHoC,fA);}else if(xw.attachEvent){xw.attachEvent('onscroll',xtResol);xw.attachEvent('onbeforeunload',xtHoC);}}}
function xtAff(){var lY=xtdH(),scTop=xtsT(),pY=Math.round((scTop+haut)/lY*100),pY=(pY>100)?100:pY;pY=(pY<0)?0:pY;if(scTop==0)fO=tR;if(fO)xtCSv(pY);}
function xtResol(){larg=xtcW();haut=xtcH();xtAff();}
function xtHoC(){var s=xw.xt8,pcz=xw.xtp,s2cz=xtEx2(xw.xt9),idpage=(xw.xtidpg!=nu)?xw.xtidpg:-1,idmod=(xw.xtidmod!=nu)?xw.xtidmod:0;if(!hit){xt_imgc=new Image();xt_imgc.src=xtsd+'.xiti.com/hit.xiti?s='+s+'&pcz='+xtEn(pcz)+'&idpcz='+idpage+'&s2cz='+encodeURIComponent(s2cz)+'&pv='+xt_perdz+'&xtczv='+xtczv;}}
function xtEv(evt){var e_out,ie_var='srcElement',moz_var='target';evt[moz_var]?e_out=evt[moz_var]:e_out=evt[ie_var];return(e_out)}
function xtBdEv(evt){var e_out=(xw.event)?(xw.event.button==2):(evt.which==3);return(e_out)}
function xtR(e){xtel.t=Math.round(xtH()/1000);if(xtdtmp!=0&&(xtel.t-xtdtmp<1))return;xtdtmp=xtel.t;if(!e)e=xw.event;if(xtBdEv(e))return;var xel=xtEv(e);if(xtTr(xel)){if(xtnN(xel)=='OPTION'){var xelp=xtpN(xel);while(xelp){xel=xelp;if(xtnN(xel)=='SELECT')break;xelp=xtpN(xelp);}}xtel.s=xw.xt8;xtel.pcz=xw.xtp;xtel.s2cz=xtEx2(xw.xt9);xtel.idmod=(xw.xtidmod!=nu)?xw.xtidmod:0;xtel.curr=xtNa(xel).id;xtel.cliccz=xtNa(xel).typ;xtel.dest=xtNa(xel).url;xtel.p=xtNa(xel).pag;xtel.s2=xtNa(xel).sec;xtel.p=((xtel.p!='')&&(xtel.cliccz!='F'))?'&pmed='+xtEn(xtel.p):'';xtel.s2=((xtel.s2!='')&&(xtel.cliccz!='F'))?'&s2med='+xtEn(xtel.s2):'';if(xtel.cliccz=='F'){xtel.cliccz=xtCt(xl).typ;}xtel.sx=xtSx(xel);xtel.sy=xtSy(xel);xtel.px=xtPx(xel);xtel.py=xtPy(xel);xtel.idpage=(xw.xtidpg!=nu)?xw.xtidpg:-1;if((xtel.px==-1)&&(xtel.py==-1))return;var tmpelt=xtEv(e);xtel.xc=((xtnN(tmpelt)=='OPTION')&&!isOP)?xtC(e).x+xtel.px:xtC(e).x;xtel.yc=xtC(e).y;if((xtel.xc==-1)&&(xtel.yc==-1))return;if(xd.compatMode=='BackCompat'&&isI){xtel.xc-=2;xtel.yc-=2;}xtel.xr=(xtel.xc-xtel.px)/xtel.sx;xtel.yr=(xtel.yc-xtel.py)/xtel.sy;if((0<xtel.sx)&&(xtel.sx<=40))xtel.xr=0.5;if((40<xtel.sx)&&(xtel.sx<=250))xtel.xr=(Math.round(xtel.xr*10))/10;if(250<xtel.sx)xtel.xr=(Math.round(xtel.xr*100))/100;if((0<xtel.sy)&&(xtel.sy<=40))xtel.yr=0.5;if((40<xtel.sy)&&(xtel.sy<=250))xtel.yr=(Math.round(xtel.yr*10))/10;if(250<xtel.sy)xtel.yr=(Math.round(xtel.yr*100))/100;if(xtel.xr<0)xtel.xr=0;if(xtel.yr<0)xtel.yr=0;if(xtel.xr>1)xtel.xr=1;if(xtel.yr>1)xtel.yr=1;if((xtel.curr).indexOf(cL,0)<0){var idx=xtIdxOf(xtel.xttab,xel);var xelp=xtpN(xel);while((idx==-1)&&xelp){idx=xtIdxOf(xtel.xttab,xelp);xelp=xtpN(xelp);}xtel.bf=xtNa(xtel.xttab[idx-1]).id;xtel.af=xtNa(xtel.xttab[idx+1]).id;xtel.bf=((xtel.bf).indexOf(cL,0)>=0)?xtExTc(xtel.bf):xtel.bf;xtel.af=((xtel.af).indexOf(cL,0)>=0)?xtExTc(xtel.af):xtel.af;}else{xtel.curr=xtExTc(xtel.curr);xtel.bf='';xtel.af='';xtel.dest='';}var name='';if((xtnN(xel)=='EMBED')&&isOP&&(xtpN(xel)!=nu)&&(xtpN(xel)!=undefined))name=xtpN(xel).name;else name=xel.name;if((name!=nu)&&(name!=un)&&((name).indexOf(cT)>=0))xtel.cliccz=name.replace(cT,'');var type=xtCt(xel).typ;var hitn=fA;if((xtel.cliccz=='N')&&(type!='N'))hitn=tR;var regex=new RegExp('(&)|[?]','g');xtel.curr=(xtel.curr.toString()).replace(regex,'_');xtel.bf=(xtel.bf.toString()).replace(regex,'_');xtel.af=(xtel.af.toString()).replace(regex,'_');xtel.dest=(xtel.dest.toString()).replace(regex,'_');var res='&xtczv='+xtczv+'&idmod='+xtel.idmod+'&current='+xtEn(xtel.curr)+'&before='+xtEn(xtel.bf)+'&after='+xtEn(xtel.af)+'&cliccz='+xtel.cliccz+'&dest='+xtEn(xtel.dest)+'&posx='+xtel.xr+'&posy='+xtel.yr+'&time='+xtel.t+xtel.p+xtel.s2;if(((scriptOnClickZone==2)&&(xtel.cliccz!='N'))||(hitn==tR)){xt_imgc=new Image();var sv='';if(xtel.cliccz=='S'){hit=tR;sv='&pv='+xt_perdz;}xt_imgc.src=xtsd+'.xiti.com/hit.xiti?s='+xtel.s+'&pcz='+xtEn(xtel.pcz)+'&idpcz='+xtel.idpage+'&s2cz='+xtEn(xtel.s2cz)+res+sv;}else if((scriptOnClickZone!=2)&&(xtel.cliccz=='S')){hit=tR;xt_imgc=new Image();xt_imgc.src=xtsd+'.xiti.com/hit.xiti?s='+xtel.s+'&pcz='+xtEn(xtel.pcz)+'&idpcz='+xtel.idpage+'&s2cz='+xtEn(xtel.s2cz)+'&pv='+xt_perdz;}else if(xtel.cliccz=='N'){hit=tR;xtCzW('&idpcz='+xtel.idpage+res+'&pv='+xt_perdz);}}}
function xtCSv(p){var lY=xtdH(),percWin=Math.round((haut/lY)*100),oldP=0;if(xt_perdz!=nu){oldP=parseInt(xt_perdz,10);if((p>=oldP)&&(p<(oldP+percWin))){xt_perdz=p;}}else{xt_perdz=p;}}
function xtCzW(v){var xtcznb=new Date();xtcznb.setTime(xtcznb.getTime()+45000);xd.cookie='xtvalCZ='+v+';expires='+xtcznb.toGMTString()+' ;path=/'+xw.xt1;}
function xtdH(){var off=xd.documentElement?parseInt(xd.documentElement.offsetHeight,10):0,sc=xd.documentElement?parseInt(xd.documentElement.scrollHeight,10):0,boff=xd.body?parseInt(xd.body.offsetHeight,10):0;bsc=xd.body?parseInt(xd.body.scrollHeight,10):0;return xtMax(xtMax(off,boff),xtMax(sc,bsc));}
function xtcW(){var val=xtfR(xw.innerWidth?parseInt(xw.innerWidth,10):0,xd.documentElement?parseInt(xd.documentElement.clientWidth,10):0),val2=xd.body?parseInt(xd.body.clientWidth,10):0;return((val==0)?val2:val);}
function xtcH(){var val=xtfR(xw.innerHeight?parseInt(xw.innerHeight,10):0,xd.documentElement?parseInt(xd.documentElement.clientHeight,10):0),val2=xd.body?parseInt(xd.body.clientHeight,10):0;return((val==0)?val2:val);}
function xtsT(){var pag=xw.pageYOffset?parseInt(xw.pageYOffset,10):0,st=xd.documentElement?parseInt(xd.documentElement.scrollTop,10):0,bst=xd.body?parseInt(xd.body.scrollTop,10):0;return xtMax(xtMax(pag,st),bst);}
function xtMax(a,b){return ((a>b)?a:b);}
function xtfR(w,d){var n_result=w?w:0;if(d&&(!n_result||(n_result>d)))n_result=d;return n_result;}
function xtEn(v){return encodeURIComponent(v);}
function xtDe(v){return decodeURIComponent(v);}
function xtG(o,a){var att=o.getAttribute(a);return (att==null)?null:(((a==cZ)&&(att.length>255))?xtSub(att,0,255):att);}
function xtnN(o){return o.nodeName;}
function xtpN(o){return o.parentNode;}
function xtSub(o,d,f){return o.substring(d,f);}

/***************************/
//  Part XTCORE25.TXT
/**************************/

//-- Copyright 2009 AT Internet, All Rights Reserved.
//-- AT Internet Tag 3.4.002
var xt1=".carrefour.fr",xtcode="",xt46="",xt48="",xtdocl=false,xtud="undefined",xt2="0",xt3=3650,xtadch=new Array,xt4=new Array;xt4["sec"]="20";xt4["rss"]="20";xt4["epr"]="20";xt4["erec"]="20";xt4["adi"]="20";xt4["adc"]="20";xt4["al"]="AL";xt4["es"]="EPR";xt4["ad"]="ADI";
//do not modify below
var xt49=null,xt5=30,xw=window,xd=document,xtg=navigator,xtv=(xw.xtczv!=null)?"34002-"+xw.xtczv:"34002",xt1=(xw.xtdmc!=null&&xw.xtdmc!='')?";domain="+xw.xtdmc:(xt1!='')?";domain="+xw.xt1:"",xt6=(xw.xtnv!=null)?xw.xtnv:xd,xt7=(xw.xtsd!=null)?xw.xtsd:"http://log",xt8=(xw.xtsite!=null)?xw.xtsite:0,xt9=(xw.xtn2!=null)?'&s2='+xw.xtn2:'',xtp=(xw.xtpage!=null)?xw.xtpage:"",xt10=((xw.xto_force!=null)&&(xw.xto_force!=""))?xw.xto_force.toLowerCase():null,xt11=(xt8=="redirect")?true:false,xtdi=((xw.xtdi!=null)&&(xw.xtdi!=""))?"&di="+xw.xtdi:"",xt12=((xw.xtidp!=null)&&(xw.xtidp!=""))?"&idpays="+xw.xtidp:"",xt13=((xw.xtidprov!=null)&&(xw.xtidprov!=""))?"&idprov="+xw.xtidprov:"",xtm=(xw.xtparam!=null)?xw.xtparam:"",xtclzone=(xw.scriptOnClickZone!=null)?xw.scriptOnClickZone:0,xt15=(xw.xt_orderid!=null)?xw.xt_orderid:"",xt17=(xw.xtidcart!=null)?xw.xtidcart:"",xt44=(xw.xtprod_load!=null)?"&pdtl="+xw.xtprod_load:"",xt47=(xw.xtcode!="")?"&code="+xw.xtcode:"";
if(xw.addEventListener){xw.addEventListener('unload',function(){},false)}else{if(xw.attachEvent){xw.attachEvent('onunload',function(){});}}
var xt18=((xw.roimt!=null)&&(xw.roimt!="")&&(xtm.indexOf("&roimt",0)<0))?"&roimt="+xw.roimt:"",xtmc=((xw.xtmc!=null)&&(xw.xtmc!="")&&(xtm.indexOf("&mc",0)<0))?"&mc="+xw.xtmc:"",xtac=((xw.xtac!=null)&&(xw.xtac!="")&&(xtm.indexOf("&ac",0)<0))?"&ac="+xw.xtac:"",xtan=((xw.xtan!=null)&&(xw.xtan!="")&&(xtm.indexOf("&an",0)<0))?"&an="+xw.xtan:"",xtnp=((xw.xtnp!=null)&&(xw.xtnp!="")&&(xtm.indexOf("&np",0)<0))?"&np="+xw.xtnp:"",xt19=((xw.xtprm!=null)&&(xtm.indexOf("&x",0)<0))?xw.xtprm:"";xtm+=xt18+xtmc+xtac+xtan+xtnp+xt19;
try {var xt20=top.document.referrer;}catch(e){var xt20=xt6.referrer;};var xts=screen,xt21=new Date(),xt22=xt21.getTime()/(1000*3600);
function xtclURL(ch){return ch.replace(/%3C/g,'<').replace(/%3E/g,'>').replace(/[<>]/g,'');};function xtf1(nom,xtenc){xtenc=((xtenc!=null)&&(xtenc!=xtud))?xtenc:"0";var arg=nom+"=",i=0;while(i<xd.cookie.length){var j=i+arg.length;if(xd.cookie.substring(i,j)==arg){return xtf2(j,xtenc);}i=xd.cookie.indexOf(" ",i)+1;if(i==0){break;}}return null;};function xtf2(index,xtenc){var fin=xd.cookie.indexOf(";",index);if(fin==-1){fin=xd.cookie.length;};if(xtenc!="1"){return unescape(xtclURL(xd.cookie.substring(index,fin)));}else{return xtclURL(xd.cookie.substring(index,fin));}};try{xt_adch();}catch(e){""};function xt_addchain(val,varch){xtvarch=((varch!=null)&&(varch!=""))?varch:"abmv";itemp=(!xtadch[xtvarch])?0:xtadch[xtvarch];itemp++;xtm+="&"+xtvarch+""+itemp+"="+val;xtadch[xtvarch]=itemp;}
function wck(p1,p2,p3,p4,fmt){p2=(fmt==0)?p2:escape(p2);xd.cookie=p1+"="+p2+";expires="+p3.toGMTString()+";path=/"+p4;};function xtf3(param,chba){try{xtdeb=xt6.location.href;}catch(e){xtdeb=xw.location.href;}if((chba==null)||(chba==xtud)){var xturl=xtclURL(xtdeb.toLowerCase().replace(/%3d/g,'='));}else{var xturl=chba;};var xtpos=xturl.indexOf(param+"=");if(xtpos>0){var chq=xturl.substring(1,xturl.length),mq=chq.substring(chq.indexOf(param+"="),chq.length),pos3=mq.indexOf("&");if(pos3==-1)pos3=mq.indexOf("%26");if(pos3==-1)pos3=mq.length;return mq.substring(mq.indexOf("=")+1, pos3);}else{return null;}};function xt_med(type,section,page,x1,x2,x3,x4,x5){xt_ajout=((type=='F')&&((x1==null)||(x1==xtud)))?'':(type=='M')?'&a='+x1+'&m1='+x2+'&m2='+x3+'&m3='+x4+'&m4='+x5:'&clic='+x1;xtf4(type,"&s2="+section+"&p="+page+xt_ajout,x2,x3);}function xt_ad(x1,x2,x3){xtf4("AT","&atc="+x1+"&type=AT",x2,x3);}function xt_click(obj,type,n2,page,x1,x2,x3){xt_ajout=((type=='F')&&((x1==null)||(x1==xtud)))?'':'&clic='+x1;xtf4(type,"&s2="+n2+"&p="+page+xt_ajout,x2,x3);var tgt=null,href=null;if(obj.nodeName!='A'){var xelp=obj.parentNode;while(xelp){if(xelp.nodeName=='A'){href=xelp.href;tgt=xelp.target||'_self';break;}xelp=xelp.parentNode;}}else{href=obj.href;tgt=obj.target||'_self';}if((tgt!=null)&&(tgt!='_blank')&&(tgt!='_search')){setTimeout("(xw.open('"+href+"','"+tgt+"')).focus();", 500);return false;}return true;}
function xt_rm(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14){var rmprm="&p="+x3+"&s2="+x2+"&type="+x1+"&a="+x4+"&m5="+x11+"&m6="+x12;rmprm+=((x5!=null)&&(x5!="0"))?'&'+x5:'';rmprm+=((x7!=null)&&(x4!='pause')&&(x4!='stop'))?"&m1="+x7+"&"+x8+"&m3="+x9+"&m4="+x10+"&m7="+x13+"&m8="+x14+"&prich="+xtp+"&s2rich="+xw.xtn2:"";rmprm+=((x6!=null)&&(x6!='0')&&(x7!=null))?"&rfsh="+x6:"";xtf4(x1,rmprm);if((x6!=null)&&(x6!='0')&&((x4=='play')||(x4=='play&buf=1')||(x4=='refresh'))){xtrmdl=(Math.floor(x6)>1500)?1500000:(Math.floor(x6)<5)?5000:Math.floor(x6)*1000;xtoid=xw.setTimeout("xt_rm('"+x1+"','"+x2+"','"+x3+"','refresh','0','"+x6+"',null,'"+x8+"','"+x9+"','"+x10+"','"+x11+"','"+x12+"')",xtrmdl);}else{if(((x4=='pause')||(x4=='stop'))&&(xw.xtoid!=null)){xw.clearTimeout(xtoid)}}}function xtf4(x1,x2,x3,x4){if(((xtclzone==0)||(xtclzone==3)||(x1!='C'))&&(x1!="P")){xt_img=new Image();var xt22=new Date();xt_im=xt7+'.xiti.com/hit.xiti?s='+xt8+x2+'&hl='+xt22.getHours()+'x'+xt22.getMinutes()+'x'+xt22.getSeconds();if(parseFloat(xtg.appVersion)>=4){xt_im+='&r='+xts.width+'x'+xts.height+'x'+xts.pixelDepth+'x'+xts.colorDepth;};xt_img.src=xt_im;}if((x3!=null)&&(x3!=xtud)&&(x1!='M')){if((x4=='')||(x4==null)){xd.location=x3}else{xfen=window.open(x3,'xfen','');xfen.focus();}}else{return;}}
function f_nb(a){a=a-Math.floor(a/100)*100;if(a<10){return "0"+a;}else{return a;}}var xtidpg=f_nb(xt21.getHours())+''+f_nb(xt21.getMinutes())+''+f_nb(xt21.getSeconds())+''+Math.floor(Math.random()*9999999),xt23=0,xt16="",xt43=0;function xt_addProduct(rg,pdt,qtt,unp,dsc,dscc){xt23++;xt16+="&pdt"+xt23+"=";xt16+=((rg!=null)&&(rg!="")&&(rg!=xtud))?rg+"::":"";xt16+=((pdt!=null)&&(pdt!="")&&(pdt!=xtud))?pdt:"";xt16+=((qtt!=null)&&(qtt!="")&&(qtt!=xtud))?"&qte"+xt23+"="+qtt:"";xt16+=((unp!=null)&&(unp!="")&&(unp!=xtud))?"&mt"+xt23+"="+unp:"";xt16+=((dsc!=null)&&(dsc!="")&&(dsc!=xtud))?"&dsc"+xt23+"="+dsc:"";xt16+=((dscc!=null)&&(dscc!="")&&(dscc!=xtud))?"&pcode"+xt23+"="+dscc:"";}function xt_addProduct_v2(rg,pdt,qtt,unp,unpht,dsc,dscht,dscc,roimtp){xt23++;xt16+="&pdt"+xt23+"=";xt16+=((rg!=null)&&(rg!="")&&(rg!=xtud))?rg+"::":"";xt16+=((pdt!=null)&&(pdt!="")&&(pdt!=xtud))?pdt:"";xt16+=((qtt!=null)&&(qtt!="")&&(qtt!=xtud))?"&qte"+xt23+"="+qtt:"";xt16+=((unp!=null)&&(unp!="")&&(unp!=xtud))?"&mt"+xt23+"="+unp:"";xt16+=((unpht!=null)&&(unpht!="")&&(unpht!=xtud))?"&mtht"+xt23+"="+unpht:"";xt16+=((dsc!=null)&&(dsc!="")&&(dsc!=xtud))?"&dsc"+xt23+"="+dsc:"";xt16+=((dscht!=null)&&(dscht!="")&&(dscht!=xtud))?"&dscht"+xt23+"="+dscht:"";xt16+=((roimtp!=null)&&(roimtp!="")&&(roimtp!=xtud))?"&roimt"+xt23+"="+roimtp:"";xt16+=((dscc!=null)&&(dscc!="")&&(dscc!=xtud))?"&pcode"+xt23+"="+dscc:"";}function xt_addProduct_load(rg,pdt,xv){if(pdt!=''){xt43++;xt44+=(xt43==1)?"&pdtl=":"|";xt44+=((rg!=null)&&(rg!="")&&(rg!=xtud))?rg+"::":"";xt44+=((pdt!=null)&&(pdt!="")&&(pdt!=xtud))?pdt:"";xt44+=((xv!=null)&&(xv!="")&&(xv!=xtud))?";"+xv:"";}}
try{xt_cart();}catch(e){xt16="";}function xt_ParseUrl(hit,xtch,xtrefP,thit){var tabUrl=new Array;if(xtch.length>0){var xtlg=1600-xtrefP.length,i=0,j=0,xtch_prec="",xterr=0;while((xtch.length>xtlg)&&(xtch_prec!=xtch)&&(xterr==0)){xtch_prec=xtch;var xsep="&pdt";if(xtch.lastIndexOf(xsep,xtlg)<=0){if(xtch.lastIndexOf("&",xtlg)<=0){xterr=1}else{xsep="&";}}if(xterr==1){tabUrl[i]=xtch.substring(0,1600)+"&mherr=1";}else{tabUrl[i]=xtch.substring(0,xtch.lastIndexOf(xsep,xtlg));xtch=xtch.substring(xtch.lastIndexOf(xsep,xtlg),xtch.length);i++;xtlg=1600;}}if (xterr==0){tabUrl[i]=xtch;}for(j=0;j<=i;j++){if(i>0){tabUrl[j]+=((xt15!="")||(xt17!=""))?"&idhit="+(j+1)+"-"+(i+1)+"-"+xt8+"-"+xt15+"-"+xt17:"&mh="+(j+1)+"-"+(i+1)+"-"+xtidpg;}if(j>0){tabUrl[j]=((xt15!="")||(xt17!=""))?"s="+xt8+"&cmd="+xt15+"&idcart="+xt17+tabUrl[j]:"s="+xt8+tabUrl[j];}else{tabUrl[j]+=xtrefP;}if((thit=='')||(thit==null)){xd.write('<img width="1" height="1" src="'+hit+tabUrl[j]+'">');}else{xt_img=new Image();xt_img.src=hit+tabUrl[j];}}}}function xt_ParseUrl2(hit,xtcst,xtch,thit){var tabUrl=new Array;if(xtch.length>0){var xtlg=1600,i=0,j=0,xtch_prec="";while(xtch.length>xtlg && xtch_prec!=xtch){xtch_prec=xtch;var xsep="&p";tabUrl[i]=xtch.substring(0,xtch.lastIndexOf(xsep,xtlg));xtch=xtch.substring(xtch.lastIndexOf(xsep,xtlg),xtch.length);i++;}tabUrl[i]=xtch;for(j=0;j<=i;j++){if((thit=='')||(thit==null)){xd.write('<img width="1" height="1" src="'+hit+xtcst+tabUrl[j]+'">');}else{xt_img=new Image();xt_img.src=hit+xtcst+tabUrl[j];}}}}
if((xt8!=0)||(xt11)){if(xt48!=''){var xtvid=xtf1('xtvid');if(!xtvid){xtvid=xt21.getTime()+''+Math.round(Math.random()*1000000,0);xt49=xtvid;}var xtexp=new Date();xtexp.setMinutes(xtexp.getMinutes()+30);wck('xtvid',xtvid,xtexp,'',1);}var xtpm="xtor"+ xt8,xtpmd="xtdate"+ xt8,xtpmc="xtocl"+ xt8,xtpan="xtan"+ xt8,xtpant="xtant"+ xt8,xt24=xtf3("xtor"),xtdtgo=xtf3("xtdt"),xt25=xtf3("xtref"),xt26=xtf3("xtan"),xt27=xtf3("an",xtm),xt28=xtf3("ac",xtm),xtocl=(xtf1(xtpmc)!=null)?xtf1(xtpmc):"$",xtord=(xtf1("xtgo")=="0")?xtf1("xtord"):null,xtgord=(xtf1("xtgo")!=null)?xtf1("xtgo"):"0",xtvrn=(xtf1("xtvrn")!=null)?xtf1("xtvrn"):"$",xtgmt=xt21.getTime()/60000,xtgo=(xtdtgo!=null)?(((xtgmt-xtdtgo)<30)&&(xtgmt-xtdtgo)>=0)?"2":"1":xtgord,xtpgt=(xtgord=="1")?"&pgt="+xtf1("xtord"):((xtgo=="1")&&(xt24!=null))?"&pgt="+xt24:"",xto=(xt10!=null)?xt10:((xt24!=null)&&(xtgo=="0"))?xt24:(!xt11)?xtord:null;
xto=((xtocl.indexOf('$'+xto+'$')<0)||(xtocl=="$"))?xto:null;var xtock=(xtgo=="0")?xto:(xtgord=="2")?xtf1("xtord"):(xtgo=="2")?xt24:null;
if(xtock!=null){tmpxto=xtock.substring(0,xtock.indexOf("-"));var xtdrm=xt4[tmpxto];}else{xtdrm="1";}if((xtdrm==null)||(xtdrm==xtud)){xtdrm=xt4["ad"];}if((xt26==null)&&(!xt11)){xt26=xtf1("xtanrd");}var xtanc=xtf1(xtpan),xtanct=xtf1(xtpant),xtxp=new Date(),xt29=new Date(),xt30=new Date();
if(!xt11){xtxp.setTime(xtxp.getTime()+(xtdrm*24*3600*1000));}else{xtxp.setTime(xtxp.getTime()+(xt5*1000));};xt30.setTime(xt30.getTime()+1800000);xt29.setTime(xt29.getTime()+(xt3*24*3600*1000));var xt31=(xt26!=null)?xt26.indexOf("-"):0,xtan2=(xt27!=null)? "":((xt26!=null)&&(xt31>0))?"&ac="+xt26.substring(0,xt31)+"&ant=0&an="+xt26.substring(xt31+1,xt26.length):(xtanc!=null)?"&anc="+xtanc+"&anct="+xtanct:"",xt32=(xtvrn.indexOf('$'+xt8+'$')<0)?"&vrn=1":"",xt35=((xtf3("xtatc")!=null)&&(xtf3("atc",xtm)==null))?"&atc="+xtf3("xtatc"):"";
if(xt32!=""){wck("xtvrn",xtvrn+xt8+'$',xt29,xt1,0);};xt32+=(xto==null)?"":"&xto="+xto;xt32+=xtan2+xtpgt+xt35;if(xt27!=null){wck(xtpan,xt28+"-"+xt27,xt29,xt1,1);wck(xtpant,"1",xt29,xt1,1);}else{if((xt26!=null)&&(xtanct!="1")){wck(xtpan,xt26,xt29,xt1,1);wck(xtpant,"0",xt29,xt1,1);}}
var xtor=xtf1(xtpm),xtor_duree=xtf1(xtpmd),xtdate2=(xtor_duree!=null)?new Date(xtor_duree):new Date(),xt34=xtdate2.getTime()/(1000*3600),xtecart=(Math.floor(xt22-xt34)>=0)?Math.floor(xt22-xt34):0;
xt32+=(xtor==null)?"":"&xtor="+xtor+"&roinbh="+xtecart;var xt33="",Xt_r=(xt25!=null)?xt25.replace(/[<>]/g, ''):xtf1('xtref');if(Xt_r==null){Xt_r=xt20.replace(/[<>]/g, '');}
if (!xt11){if((xtock!=null)&&((xtocl.indexOf('$'+escape(xtock)+'$')<0)||(xtocl=="$"))){wck(xtpmc,xtocl+xtock+'$',xt30,xt1,1);};xt33+=xtg.javaEnabled()?"&jv=1":"&jv=0";var xtnav=xtg.appName+" "+xtg.appVersion,xtIE=(xtnav.indexOf('MSIE'));if(xtIE>=0){var xtvers=parseInt(xtnav.substr(xtIE+5));xtIE=true;}else{xtvers=parseFloat(xtg.appVersion);xtIE=false;}
var xtnet=(xtnav.indexOf('Netscape')>=0),xtmac=(xtnav.indexOf('Mac')>=0),xtOP=(xtg.userAgent.indexOf('Opera')>=0);if((xtIE)&&(xtvers >=5)&&(!xtmac)&&(!xtOP)&&(!xt11)){xd.body.addBehavior("#default#clientCaps");var xtconn='&cn='+xd.body.connectionType;xtconn+='&ul='+xd.body.UserLanguage;xd.body.addBehavior("#default#homePage");var xthome=(xd.body.isHomePage(location.href))?'&hm=1':'&hm=0',xtresr='&re='+xd.body.offsetWidth+'x'+xd.body.offsetHeight;}else{var xtconn='',xthome='';if(xtvers>=5){xtresr='&re='+xw.innerWidth+'x'+xw.innerHeight;}else{xtresr=''};}if((xtnet)&&(xtvers >=4)||(xtOP)){var xtlang='&lng='+xtg.language;}else{if((xtIE)&&(xtvers >=4)&&(!xtOP)){var xtlang='&lng='+xtg.userLanguage;}else{xtlang='';}}
wck("xtord","",xt21,xt1,1);if(xtock!=null){if(((xtor==null)&&(xt2!="1"))||(xt2=="1")){wck(xtpm,xtock,xtxp,xt1,1);wck(xtpmd,xt21,xtxp,xt1,0);}}
var xthl='&hl='+xt21.getHours()+'x'+xt21.getMinutes()+'x'+xt21.getSeconds(),xt45=(xtdocl)?"&docl="+encodeURIComponent(xt6.location.href.replace(/&/g,'#ec#')):"",Xt_param='s='+xt8+xt9+'&p='+xtp+xthl+xtdi+xt12+xt13+xt32+xt45+xt47+xtm+xtconn+xthome+xtlang+'&vtag='+xtv+"&idp="+xtidpg;
var xtvalCZ=xtf1('xtvalCZ',1);if(xtvalCZ!=null){Xt_param+=xtvalCZ;var xtdateo=new Date();xtdateo.setTime(xtdateo.getTime()-3600000);wck("xtvalCZ",xtvalCZ,xtdateo,xt1,1);};var Xt_id=xt7+'.xiti.com/hit.xiti?';if(xtvers >=4){xt33+='&r='+xts.width+'x'+xts.height+'x'+xts.pixelDepth+'x'+xts.colorDepth;};Xt_param+=xt33+xtresr+xt16;var Xt_i=Xt_id+Xt_param+'&ref='+Xt_r.replace(/&/g,'$');if(xt49){Xt_param+='&lnk='+xt48+'&vid='+xt49;}xt_ParseUrl(Xt_id,Xt_param,'&ref='+Xt_r.replace(/&/g, '$'),xt46);if(xt44!=''){xt_ParseUrl2(Xt_id,'s='+xt8+'&type=PDT'+xthl,xt44);}}
else{wck("xtgo",xtgo,xtxp,xt1,1);if(xt24!=null){wck("xtord",xt24,xtxp,xt1,1);}if(xt26!=null){wck("xtanrd",xt26,xtxp,xt1,1);}if(Xt_r!=""){wck("xtref",Xt_r.replace(/&/g,'$'),xtxp,xt1,0);}if(xw.xtloc!=null){xt6.location=xw.xtloc;}}}

try { if (xtclzone>0) {xtNodesload(xd); } } catch(e) {}

