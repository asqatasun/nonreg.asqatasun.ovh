var vgnExtNewComponentAdded = false;
var vgnExtCmaSidePickupURL = null;
function submitCMD(command) {
	if (vgnExtNewComponentAdded) {
       	 window.location.href = vgnExtCmaSidePickupURL;
	} else {
	    	window.location.reload();
    	}
}

// Opens the content publish window for the channel page
// flag - indicates if the associated Channel for the page is approved or not
//			for Template/Content Instance preview, this is always true
// message -- message to be displayed when channel is not approved.
function publishManagedObject(flag, cmId, siteVCMId, channelVCMId, message) {
	
	if (flag != 'true')
	{
		alert(message);
		return;
	}

	var windowTitle = 'configureTemplating' + cmId;
	var context = '/jobsp';
	var path = '/secure/newJob.do';
	path += '?spfSOs=' + cmId;
	path += '&vgnPubType=publish';
	path += '&vgnContentType=content';
	path += '&spfRP=/placeholder';
	path += '&spfLP=/' + siteVCMId + '/' + channelVCMId;
	SimplePopwin(context + path, windowTitle, '', '800', '600');
}

function startProcessOnManagedObject(vcmId) {
	var windowTitle = 'configureTemplating' + vcmId;
	var context = '/workflow';
	var path = '/secure/startProcessRouter.do';
	path += '?spfSOs=' + vcmId;
	SimplePopwin(context + path, windowTitle, '', '800', '600');
}

function approveManagedObject(vcmId, returnURL) {
	var href = '/vgn-ext-templating-cma/secure/page/approve.jsp';
	href += '?vgnextoid=' + vcmId;
	href += '&returnURL=' + returnURL;
	window.location = href;
}
