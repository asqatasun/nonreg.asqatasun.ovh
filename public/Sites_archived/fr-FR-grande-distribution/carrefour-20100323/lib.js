var timeStart = new Date().getTime();
/*
	Version livr�e par Fullsix le 05/08/2009
	Fichier : lib.js
	Livraison : Mardi 4 novembre 2008 - V2.8
	
	var tabs (ligne 968) - Modifi� par Angelo FILS-AIME (Webmaster Carrefour.fr)
	le 04 novembre 2009
	
*/

var IS_IE = document.all && window.print && !window.opera && ( !document.compatMode || /MSIE [56]/.test(navigator.userAgent) || (document.compatMode && document.compatMode=="BackCompat"));
var IE_NG = document.all && window.print && !window.opera && /MSIE [7-9]/.test(navigator.userAgent) && document.compatMode && document.compatMode!="BackCompat"; //variable pour IE7 et + si besoin.
var IS_quirks = IS_IE && document.compatMode && document.compatMode=="BackCompat"; // variable qui declare le quirksmode seulement utile pour IE
var IS_Webkit = /Konqueror|Safari|KHTML/.test(navigator.userAgent);
var IS_MS = /MSIE /.test(navigator.userAgent);
var heightPropertyToUse = IS_IE ? "height" : "minHeight"; //variable utilisee pour l'alignement en hauteur des elements.
var TrDisplayPropertyToUse =  IS_MS ? "block" : "table-row";  // utilisee pour le toggle des trs d'un table
document.documentElement.className =" hasJS";
document.documentElement.className+= IS_IE ? " IS_IE" : "";
window.Retraitement = [];

/** *********************************************************************************************************
****** HELPERS FUNCTIONS ******
*********************************************************************************************************  */

/** *********************************************************************************************************
     REMPLACEMENT DES METHODES getElementById ET getElementsByTagName
     EXEMPLES :
	var oEl = $('test');              // retourne l'objet ayant l'id 'test'
	var oEl2 = $(oEl);               // retourne oEl
	var aEls = $(oEl, 'a');         // retourne tous les liens de oEl
	var aEls2 = $('test', 'a');    // retourne tous les liens du conteneur ayant pour id 'test'
	var oEl3 = $(oEl, 'a')[0];    // retourne le premier lien de oEl
*/

var $ = {
	select: function() {
		var aArgs = arguments;
		if(aArgs.length > 1 && typeof aArgs[1] == 'string') {
			return typeof aArgs[0] == 'string' ?
				document.getElementById(aArgs[0]).getElementsByTagName(aArgs[1]):
				aArgs[0].getElementsByTagName(aArgs[1]);
		}
		else switch(typeof aArgs[0]) {
			case 'string':
				return document.getElementById(aArgs[0]);
			case 'object':
				return aArgs[0];
		}
		return false;
	}
};
$ = $.select;

/** *********************************************************************************************************
     FONCTION D'EXTENTION DE PROPRIETES D'UN OBJET - Extrait de Mootools
     EXEMPLES :
	$extend(myObj, {alerte: function(sMsg) { alert(sMsg); });      //  Ajout de la methode alerte a l'objet myObj
	$extend(myObj, anotherObj);                                             //  Ajout des methodes de l'objet anotherObj a l'objet myObj
*/

var $extend = {
	extend: function(){
		var args = arguments;
		if (!args[1])
			args = [this, args[0]];
		for (var property in args[1])
			args[0][property] = args[1][property];
		return args[0];
	}
};
$extend = $extend.extend;

/** *********************************************************************************************************
    OBJET DE GESTION DE CLASSE - INSPIRE DE http://www.onlinetools.org/articles/unobtrusivejavascript/cssjsseparation.html
    EXEMPLES :
	$c.add(oEl, 'test');                    //  Ajout d'une classe a l'element oEl
	$c.has(oEl, 'test');                    //  Verification de l'existence de la classe 'test' sur l'element oEl via une chaine texte
	$c.has(oEl, /\btest\b/);             //  Verification de l'existence de la classe 'test' sur l'element oEl via une expression reguliere
	$c.swap(oEl, 'test', 'test2');      //  Modification de la classe 'test' de l'element oEl en classe 'test2' (ou inversement)
	$c.remove(oEl, 'test2');            //  Suppression de la classe 'test2' de l'element oEl
 */

var $c = {
	remove: function(oEl, sClass) {
		var rep = oEl.className.match(' ' + sClass) ? ' ' + sClass : sClass;
		oEl.className = oEl.className.replace(rep, '');
	},
	add: function(oEl, sClass) {
		if(!$c.has(oEl, sClass))
			oEl.className += oEl.className ? ' ' + sClass : sClass;
	},
	swap: function(oEl, sClass1, sClass2) {
		oEl.className = !$c.has(oEl, sClass1) ?
			oEl.className.replace(sClass2, sClass1):
			oEl.className.replace(sClass1, sClass2);
	},
	has: function() {
		return typeof arguments[1] == 'string' ?
			new RegExp('\\b' + arguments[1] + '\\b').test(arguments[0].className):
			arguments[1].test(arguments[0] != null ? arguments[0].className : "");
	}
};

/** *********************************************************************************************************
    GESTIONNAIRE D'EVENEMENT
    EXEMPLES :
	$e.add('domready', alerte);                                                    // Lance le gestionnaire alerte au chargement de la page
	$e.add(oEl, 'click', alerte);                                                     // Ajoute le gestionnaire alerte au clic sur oEl en mode 'effervescence'
	$e.add(oEl, 'click', alerte, true);                                             // Ajoute le gestionnaire alerte au clic sur oEl en mode 'capture' (IE ne sait pas faire)
	$e.remove(oEl, 'click', alerte);                                               // Supprime le gestionnaire alerte au clic sur oEl
	$e.add(oEl, 'click', function(e) { $e.stop(e); });                        // Si oEl est un lien, $e.stop(e); stoppe la transmission de l'evenement et annule l'action normale du lien
	$e.add(oEl, 'click', function(e) { alert($e.getSrc(e)); });            // Retourne la source de l'evenement
	$e.add(oEl, 'mouseover', function(e) { alert($e.relSrc(e)); });    // Retourne l'element survole precedent le mouseover
	$e.add(oEl, 'mouseout', function(e) { alert($e.relSrc(e)); });     // Retourne l'element survole suivant le mouseout
*/

var $e = {
	// Ajout d'un gestionnaire d'evenement sur un element lors d'un evenement donne
	add: function() {
		var a = arguments;
		if(a[0] == 'domready')
			return $e.domready(a[1]);
		return document.addEventListener ?
			a[0].addEventListener(a[1], a[2], a[3] || false):
			a[0].attachEvent ?
				a[0].attachEvent('on' + a[1], a[2]):
				false;
	},
	// Suppression d'un gestionnaire d'evenement sur un element pour un evenement donne
	remove: function(oElem, sEvType, fn, bCapture) {
		return document.addEventListener ?
			oElem.removeEventListener(sEvType, fn, bCapture || false):
			oElem.detachEvent ?
				oElem.detachEvent('on' + sEvType, fn):
				false;
	},
	// Annulation de la propagation d'un evenement et de l'action par defaut d'un element
	stop: function(e) {
		if(e && e.stopPropagation && e.preventDefault) {
			e.stopPropagation();
			e.preventDefault();
		}
		else if(e && window.event) {
			window.event.cancelBubble = true;
			window.event.returnValue = false;
		}
		return false; // Indispensable pour Safari
	},
	// Retourne la source de l'evenement
	getSrc: function(e) {
		return e.target || e.srcElement;
	},
	relSrc: function(e) {
		switch(e.type) {
			case 'mouseover': // Retourne l'element survole precedent l'element source de l'evenement
				return e.relatedTarget(e) || e.fromElement;
			case 'mouseout': // Retourne l'element survole suivant l'element source de l'evenement
				return e.relatedTarget(e) || e.toElement;
		}
	},
	// Detecte le chargement du DOM - http://dean.edwards.name/weblog/2006/06/again/#comment5338
	domready: function(fn) {
		// Internet Explorer
		if(window.attachEvent) {
			document.write('<script id="ieScriptLoad" defer src="//:"><\/script>');
			document.getElementById('ieScriptLoad').onreadystatechange = function() {
				if(this.readyState == 'complete')
					$e.init(fn);
			};
		}
		// Mozilla/Opera 9
		if(document.addEventListener)
			document.addEventListener('DOMContentLoaded', function() { $e.init(fn); }, false);
		// Safari
		if(navigator.userAgent.search(/WebKit/i) != -1){
		    $e.loadTimer = setInterval(function (){
				if(document.readyState.search(/loaded|complete/i) != -1)
					$e.init(fn);
			}, 10);
		}
		// Other web browsers
		if($e)
			$e.add(window, 'load', function() { $e.init(fn); });
	},
	// Initialise le script
	init: function(fn) {
		if (arguments.callee.done) return;
		arguments.callee.done = true;
		if ($e.loadTimer) clearInterval($e.loadTimer);
			fn();
	}
};

/** *********************************************************************************************************
    GESTIONNAIRE DE STYLES
*/

var $s = {
	// Retourne la valeur d'une propriete CSS appliquee a un element
	get: function(oElm, strCssRule) {
		var strValue = "";
		if(document.defaultView && document.defaultView.getComputedStyle) {
			try{
				strValue = document.defaultView.getComputedStyle(oElm, null).getPropertyValue(strCssRule);
			}
			catch(e) { strValue = ""; }
		}
		else if(oElm.currentStyle) {
			try{
				strCssRule = strCssRule.replace(/\-(\w)/g, function (strMatch, p1){
					return p1.toUpperCase();
				});
				strValue = oElm.currentStyle[strCssRule];
			} catch(e) {
				strValue = "";
			}
		}
		return strValue;
	},
	// Retourne la valeur entiere d'un style
	integer: function(oElm, strCSSRule) {
		var val = parseInt($s.get(oElm, strCSSRule));
		if (isNaN(val)) val=0;
		return val;
	},
	// Retourne la somme de tous les styles verticaux appliques (border-width+padding)
	getV: function(elm) {
		return IS_quirks ?
			0 :
			$s.integer(elm, "padding-top") +
			$s.integer(elm, "padding-bottom") +
			$s.integer(elm, "border-top-width") +
			$s.integer(elm, "border-bottom-width");
	},
	// Retourne la somme de tous les styles horizontaux appliques (border-width+padding)
	getH: function(elm) {
		return IS_quirks ?
			0 :
			$s.integer(elm, "padding-left") +
			$s.integer(elm, "padding-right");
	}
};

// openclose
function swapContent(elm) {
	var par = getParent(elm, {className:/\bcontent(show|hide)/i});
	par.className.match(/contenthide/i) ? toggleClass(par, "contentHide", "contentShow") : toggleClass(par, "contentShow", "contentHide");
	//fixColumns();
}

/*******************************************************************************************************************************/
/** FIN DES MODIFS *********************************************************************************************************/
/*******************************************************************************************************************************/

/**********
* $n : objet de parcours du DOM, facile. Les fonctions ne font que les nodes HTML
***********/
var $n = {
	/* 	hasAttributes : retourne true si l'element passe en parametre correspond a tous les attributs passes, on peut aussi donner des attributs que l'on ne veut pas, afin de filtrer tous les elements
		ex : if (hasAttributes(div, {nodeName:"div", className:"foobar"), {className:"idontwant"} ) doStuff();
		ici on recherche tous les DIV qui on la classe "foobar", mais on ne prend pas ceux qui ont la classe "idontwant" ex : <div class="foobar idontwant"> ne sera pas recupere.
	*/
	hasAttr : function(n, a, not) {
		var re, at;
		if (n.nodeType!=1) return false;
		function check(attr) {
			for (var i in attr) {
				at = (typeof n[i]) !="undefined" ? n[i] : n.getAttribute(i);
				re = attr[i] instanceof RegExp ? attr[i] : new RegExp("\\b" + attr[i] + "\\b","i");
				if (!at || !re.test(at))
					return false;
			}
			return true;
		};
		if (not && check(not))	return false;
		if (check(a)) return true;
		return false;
	},
	/* getByTagName : equivalent a element.getElementsByTagName, mais compatible avec IE5 et IE5.5 pour l'histoire du "*" */
	getByTagName : function(n, tag) {
		return  (tag=="*") ? (n.all ? n.all : n.getElementsByTagName("*")) : n.getElementsByTagName(tag);
	},
	/* fonction qui retourne le premier element correspondant aux attributs donnes */
	node : function(n, a, not) {
		return $n.nodes(n, a, not, true);
	},
	/* fonction qui retourne tous les elements correspondant selon "a" */
	nodes : function(n, a, not, oneNode, arrElms) {
		var aRetElms=[];
		if (!a) a = {};
		if (typeof a == "string") a = {nodeName:a}; //si une chaine de caracteres passee en parametre, cela signifie qu'on ne veut que recuperer des tags
		if (a.nodeName && a.nodeName=="*") delete a.nodeName;
		var elms = arrElms || $n.getByTagName(n, (a.nodeName || "*"));
		for (var i=0; i<elms.length; i++) {
			var x = elms[i];
			if ($n.hasAttr(x, a, not)) {
				if (oneNode) return x;
				else aRetElms.push(x);
			}
		}
		if (oneNode) return null;
		return aRetElms;
	},
	/* childs : retourne tous les noeuds enfants de l'element  */
	childs : function(n, a, not) {
		return $n.nodes(n, a, not, false, n.childNodes);
	},
	firstChild : function(n, a, not) {
		return $n.nodes(n, a, not, true, n.childNodes);
	},
	lastChild : function(n, a, not) {
		var node = $n.nodes(n, a, not, false, n.childNodes);
		return node[node.length-1];
	},
	move : function(n, a, not, action) {
		while (n) {
			if ($n.hasAttr(n, a, not)) return n;
			n = n[action];
		}
		return null;
	},
	after : function(n, a, not) {
		return $n.move(n, a, not, "nextSibling");
	},
	before : function(n, a, not) {
		return $n.move(n, a, not, "previousSibling");
	},
	parent : function(n, a, not) {
		return $n.move(n, a, not, "parentNode");
	}
}
/* fonctions raccourcis */
var getNode = $n.node,
	getNodes = $n.nodes,
	getChildNodes = $n.childs,
	getNextSibling = $n.after,
	getPreviousSibling = $n.before,
	getParent = $n.parent,
	hasAttributes = $n.hasAttr,
	getElementsByTagName = $n.getByTagName;




/******
*  IE fixes
******/
var CSSBottomCorners = [], currentBlockToFixCorners, CSSHeightCorners = [];
function cssRight(elm) {
	if (elm.currentStyle.right != "auto") {
		elm.style.right = (parseInt(elm.currentStyle.right)-(elm.parentNode.offsetWidth % 2)) + "px";
	} else {
		elm.style.right = "auto";
	}
}
function cssBottom(elm, pushElement) {
	if (pushElement && !elm.CSSBottomAlreadyCSS) {
		CSSBottomCorners.push(elm);
		elm.CSSBottomAlreadyCSS = true;
	}
	if (elm.currentStyle.bottom != "auto") {
		elm.style.bottom = (parseInt(elm.currentStyle.bottom)-(elm.parentNode.offsetHeight % 2)) + "px";
	} else {
		elm.style.bottom = "auto";
	}
}
function fixCorners(block) {
	if (IS_IE) {
		if (block) {
			var corners = $n.nodes(block, {nodeName:'span', className:'(bl|br|tr)'});
			for (var i = CSSBottomCorners.length - 1; i > -1; --i) {
				CSSBottomCorners[i].style.bottom = "";
				CSSBottomCorners[i].style.right = "";
			}
		} else {
			for (var i = CSSBottomCorners.length - 1; i > -1; --i) {
				CSSBottomCorners[i].style.bottom = "";
			}
		}
	} else {
		if (IS_Webkit || /Gecko\/200[56]|Opera 8.5/i.test(navigator.userAgent) || IE_NG || IS_MS) {
			fixCornersOnBlocks(block);
		}
	}
}
function fixCornersOnBlocks(block) {
	var blockToFix = block || document.body;
	blockToFix.className += " hideCorners";
	setTimeout(function() {
		if (blockToFix)
			blockToFix.className = blockToFix.className.replace(/\bhidecorners\b/gi, "");
	}, 2);
}
function cssHeight(elm, pushElement) {
	if (pushElement && !elm.CSSHeightAlreadyCSS) {
		CSSHeightCorners.push(elm);
		elm.CSSHeightAlreadyCSS = true;
	}
	elm.style.height = elm.parentNode.offsetHeight + "px";
}

function fixHeights(block) {
	if (IS_IE) {
		var array = block ? $n.nodes(block, {nodeName:"span", className:"tr|bl"}) : CSSHeightCorners;
		array.eachInv(function(x) {
			x.style.height = "";
		});
	}
}
function fixAll(actions, element) {
	actions = actions ? actions.toLowerCase().split('') : ['c','h','p'];
	actions.each(function(action) {
		switch(action) {
			case 'c' : fixCorners(element); break;
			case 'h' : fixHeights(element); break;
			//case 'p' : fixColumns(element); break;
		}
	});
}

function addHover(elm, className, iframeTag) {
	className = className || "hover";
	elm.style.behavior = " "; //reecriture du style behavior
	elm.hoverClassName = className;
	if (iframeTag) {
		elm.iframeElm = getNode(elm, iframeTag);
	}
	elm.onmouseenter = function() {
	   this.className+= ' ' + this.hoverClassName;
	   if (this.iframeElm) ifrlayer.make(this.iframeElm);
	}
	elm.onmouseleave = function() {
	   this.className = this.className.replace(new RegExp("\\b" + this.hoverClassName + "\\b", "g"), "");
	   if (this.iframeElm) ifrlayer.hide(this.iframeElm);
	}
}

function navAddHover(elm, position) {
	elm.style.behavior = " ";
	var ul = elm.getElementsByTagName("ul");
	if (ul.length > 0) {
		elm.theUl = ul[0];
		ifrlayer.make(elm.theUl);
		if (position == 'right') {
			elm.onmouseenter = function() {
				this.className += ' rightover';
				ifrlayer.make(elm.theUl);
			}
		} else {
			elm.onmouseenter = function() {
				this.className += ' over';
				ifrlayer.make(elm.theUl);
			}
		}
		elm.onmouseleave = function() {
			this.className = this.className.replace(/\b(right)?over\b/, "");
			ifrlayer.hide(this.theUl);
		}
	}
}

var ifrlayer = {
	ie: document.all && window.print && !window.opera && document.getElementById,
	$: function(obj) {
		if (!obj) return null;
		return (typeof(obj)=="string") ? document.getElementById(obj) : obj;
	},
	make: function(obj) {
		obj = this.$(obj);
		if(!obj) return;
		if(this.ie) {
			if (!obj.iframelayer) {
				var ifr = document.createElement('iframe');
				ifr.src = "javascript:false";
				obj.parentNode.insertBefore(ifr, obj);
				obj.iframelayer = ifr;
			}
			var ifr = obj.iframelayer;
			if(obj.currentStyle.zIndex != "" && parseInt(obj.currentStyle.zIndex) > 1) {
				ifr.style.zIndex = parseInt(obj.currentStyle.zIndex)-1;
			}
			with(ifr.style) {
				filter = "mask()";
				position = "absolute";
			}
			obj.iframelayer.style.visibility = "visible";
			ifrlayer.move(obj,true);
		}
	},
	hide: function(obj) {
		obj = this.$(obj);
		if(!obj) return;
		if(obj.iframelayer) {
			obj.iframelayer.style.visibility = "hidden";
		}
	},
	kill: function(obj) {
		obj = this.$(obj);
		if(!obj) return;
		if(obj.iframelayer) {
			obj.iframelayer.parentNode.removeChild(obj.iframelayer);
			obj.iframelayer = null;
		}
	},
	move: function(obj, size) {
		obj = this.$(obj);
		if(obj && obj.iframelayer) {
			with(obj.iframelayer.style) {
				top = obj.offsetTop + "px";
				left = obj.offsetLeft + "px";
				if(size) {
					width  =  obj.offsetWidth + "px";
					height =  obj.offsetHeight + "px";
				}
			}
		}
	}
};



/*************
* PopLayer
**************/
var popLayer = {

	pop : [],
	popContent : [],
	mask: [],
	levels:0,
	elmSource : [],
	template : '<span class="popt"><span></span></span><div class="popInside"><span class="popl"></span><span class="popr"></span><div class="popbody"><div class="popHead"><span class="close">Fermer</span></div><div class="popContent">Content Here</div></div></div><span class="popb"><span></span></span>',
	eventsArray : ["click", "mouseover", "mouseout", "mousemove", "mouseup", "mousedown", "keyup", "keydown", "keypress", "abort", "blur", "change", "dblclick", "error", "load", "reset", "resize", "select", "submit"],

	init : function(level) {
		level = level || 0;
		level = level>(popLayer.levels+1) ? popLayer.levels+1 : level;
		popLayer.levels=level;


		if (!popLayer.pop[level]){
			popLayer.pop[level] = $(document.createElement("div")).injectTop($(document.body)); //mootools.net
			popLayer.pop[level].id = "popLayer_"+level;
			popLayer.pop[level].addClass('popLayer');
			popLayer.pop[level].innerHTML = popLayer.template;
		}else{
			popLayer.pop[level].removeClass('hidden');
		}

		popLayer.popContent[level] = getNode(popLayer.pop[level],  {nodeName:"div", className:"popContent"});
		popLayer.pop[level].style.marginLeft = -popLayer.pop[level].offsetWidth/2+"px";

		var close = getNode(popLayer.pop[level], {className:"close"});
		close.onclick = function() {
			popLayer.close(level);
		};

		//popLayer.close();
		popLayer.pop[level].style.width = "";
		popLayer.pop[level].style.display = "block";
		popLayer.pop[level].style.zIndex = 9004+(level*4);
		popLayer.popContent[level].innerHTML = "";
		popLayer.popContent[level].style.height = "";
	},
	openTag : function(options) {
		content = $pick(options.content,null);
		width = $pick(options.width,null);
		height = $pick(options.height,null);
		mask = $pick(options.mask,true);
		level = $pick(options.level,0);
		callBack = $pick(options.callBack,null);

		popLayer.init(level);

		content = typeof(content)=="string" ? document.getElementById(content) : content;
		popLayer.copyNodes(content, popLayer.popContent[level]);

		if (mask) popLayer.showMask(level);

		setTimeout(function() {
			if (popLayer.iframe) {
				var pWin = popLayer.iframe.contentWindow || popLayer.iframe.window;
			}
		}, 2);

		popLayer.resize(level,width, height);
		popLayer.setPosition(level);
		popLayer.pop[level].style.visibility = "visible";
		ifrlayer.make(popLayer.pop[level]);
		if (callBack) callBack();
	},
	close : function(level) {
		var closeIn = function(index){
			popLayer.hideMask(index);
			ifrlayer.kill(popLayer.pop[index]);
			popLayer.pop[index].addClass('hidden');
		}
		if (level){
			closeIn(level);
		}else{
			for (var i=0; i<=popLayer.levels; i++){
				closeIn(i);
			}
		}

	},
	resize: function(level,width, height) {
		var doc, objectSized;
		if (width) {
			popLayer.pop[level].style.width = width + ((width+"").match(/%/) ? "%" : "px");
		}

		var overflown = popLayer.popContent[level].getElement('div.overflown');
		if (overflown) {
			var totalH = 0;
			height = $pick(height,(window.innerHeight?window.innerHeight-100:document.documentElement.offsetHeight-100));
			popLayer.popContent[level].setStyle('height',height);
			var parent = overflown.getParent();
			while (parent != popLayer.popContent[level]){
				height = height - (parent.getStyle('margin-top').toInt() + parent.getStyle('margin-bottom').toInt());
				parent = parent.getParent();
			}

			overflown.getParent().getChildren().each(function(elm,i){
				if (elm !== overflown){
					totalH = totalH + elm.offsetHeight + elm.getStyle('margin-top').toInt() + elm.getStyle('margin-bottom').toInt();
				}
			});
			overflown.setStyle('height', (height-totalH-10));
		}

		var sides = getNodes(popLayer.pop[level], {nodeName:"b", className:"pop(r|l)"});
		sides.each(function(side) { side.style.height = side.parentNode.offsetHeight + "px"; });
		ifrlayer.make(popLayer.pop[level]);
	},
	setPosition : function(level, top, left) {
		popLayer.pop[level].style.marginLeft = -popLayer.pop[level].offsetWidth/2+"px";
		popLayer.pop[level].style.top = document.documentElement.clientHeight>popLayer.pop[level].offsetHeight ? parseInt((document.documentElement.clientHeight-popLayer.pop[level].offsetHeight)/2) + document.documentElement.scrollTop + "px" :  document.documentElement.scrollTop + 10 + "px";
		ifrlayer.move(popLayer.pop[level]);
	},
	copyNodes : function(sourceElm, destElm) {
		for (var i=0; i<sourceElm.childNodes.length; i++) {
			var clone = sourceElm.childNodes[i].cloneNode(true);
			var newElem = $(destElm).adopt(clone); //mootools.net
			$(sourceElm).getElements('*').each(function(elm,i){  //mootools.net
				if (elm.id && elm.id!='' && !!!elm.id.match(/_clone/g)){
					elm.id = elm.id+'_clone';
				}
			});
			newElem.getElements('*').each(function(elm,i){  //mootools.net
				if (elm.id && elm.id!='' && !!elm.id.match(/_clone/g)){
					elm.id = elm.id.replace(/_clone/g,'');
				}
			});
		}
		var allSourceNodes = $n.getByTagName(sourceElm, "*");
		var allDestNodes = $n.getByTagName(destElm, "*");
		for (var i=0; i<allSourceNodes.length; i++) {
			popLayer.copyEvents(allSourceNodes[i], allDestNodes[i]);
		}
	},
	copyEvents : function(sourceElm, destElm) {
		for (var i=0; i<popLayer.eventsArray.length; i++) {
			var evt = "on"+popLayer.eventsArray[i];
			destElm[evt] = sourceElm[evt];
		}
	},
	showMask : function(level) {
		if (!popLayer.mask[level]) {
			var div = new Element('div');
			div.id = 'popLayerMask_'+level;
			div.addClass('popLayerMask');
			var tuihreuighr = $(document.body).appendChild(div);//variable temporaire pour IE
			popLayer.mask[level] = $(tuihreuighr);
		}

		var height = document.documentElement.scrollHeight>document.documentElement.clientHeight ? document.documentElement.scrollHeight : document.documentElement.clientHeight;
		if(height<200){height=5000}//securite au-cas-ou
		var width  = document.documentElement.scrollWidth>document.documentElement.clientWidth ? document.documentElement.scrollWidth : document.documentElement.clientWidth;
		popLayer.mask[level].setStyle('height', height +'px');
		//popLayer.mask.style.width = width + 30 +'px';
		popLayer.mask[level].setStyle('display','block');
		popLayer.mask[level].setStyle('opacity',0.8);
		popLayer.mask[level].setStyle('zIndex',(9002+(level*4)));
		ifrlayer.make(popLayer.mask[level]);
		$(document.documentElement).setStyle('overflow','hidden');
	},
	hideMask : function(level) {
		if (popLayer.mask[level] && popLayer.mask[level].clientHeight>0) {//evite d'avoir a tester display
			popLayer.mask[level].setStyle('display','none');
			document.documentElement.setStyle('overflow','auto');
			ifrlayer.kill(popLayer.mask[level]);
		}
	},
	refixSize : function(level) {
		if (popLayer.mask[level] && popLayer.mask[level].style.display!='none') {
			for (var i=0; i<popLayer.levels; i++){
				popLayer.showMask(i);
			}
		}
	}
}



/********************
* Extensions d'objets
 ********************/
var extend = function (object, extender) {
	var props = extender || {}; // fail-safe
	for (var property in props)
		object.prototype[property] = props[property];
	return object;
};

Array.Utils = {
	each: function(f) {
		var i;
		for(i=0;i<this.length;i++) {
			f(this[i]);
		}
	},
	eachInv: function(f) {
		for(var i=this.length-1;i>=0;i--) {
			f(this[i]);
		}
	},
	last: function() {
		return this.length>0 ? this[this.length-1] : null;
	}
};
extend(Array, Array.Utils);

/******
* resizing d'objets
*******/
/* generates corners and others elements if needed */
function generateElements(parent, stringClasses) {
	var i, x;
	parent = (typeof parent == "string") ? document.getElementById(parent) : parent;
	var content = parent || document.body;
	var aDiv = new Array(content.getElementsByTagName("div"));
	var aUls = new Array(content.getElementsByTagName('ul'));
	aAllBlock = aDiv.concat(aUls);

	for (i = aAllBlock.length - 1; i >= 0; i--)
	{
		for (ii = aAllBlock[i].length - 1; ii >= 0; ii--)
		{
			x = aAllBlock[i][ii];
			if (!x.alreadyProcessed) {
				if (x.className.indexOf("blockToggle")!=-1)
				{
					toggleBlock.init(x);
				}
				if ($c.has(x, /\bblockTabs\b/)) //block d'onglets en general (gere tous types d'onglets).
				{
					tabs.init(x);
				}
				if($c.has(x,/\bblockMagasin\b/)){
					blockMag.init(x);
				}
				if($c.has(x, /\bblockScrollH\b/)) {
					x = $(x);
					if (!x.$tmp)
					{
						x.$tmp = {};
					}
					x.$tmp.scroll = new blockScroll(x,{type:'H'});
				}
				if($c.has(x, /\bblockScrollV\b/)) {
					x = $(x);
					if (!x.$tmp)
					{
						x.$tmp = {};
					}
					x.$tmp.scroll = new blockScroll(x,{type:'V'});
				}
//				if(ii==0){trace.log((new Date()).getTime()-timeStart + 'ms initother');}
			}
		}
	}
}

/* initOtherBlocks() : fonction rajoute d'autres fonctionnalites sur differents blocks  (toggle, onglets),
   Cette fonction est forcement lancee depuis generateElements, et cela evite de faire une deuxieme passe sur les divs de la page
 */
function initOtherBlocks(x,a,z) {
	// block a onglets
}


function fixColumns() {
	function fix() {
		var colonnes = ['main','rightCol'];
		var colonnesInside = ['', ''];
		var hMax = minMax = sum = 0, i, b; hToU = heightPropertyToUse;

		function each(f) { //fonction d'iteration
			for (i = 0; i < colonnes.length; i++){
				b = $(colonnes[i]);
				f();
			}
		}

		// on remet la hauteur par defaut a toutes les colonnes (hauteur minimum)

		each(function() {
			var bToSize = $(colonnesInside[i]) || b;
			if (bToSize) bToSize.style[hToU] = "5px";
		});
		// on recupere la hauteur la plus grande
		/* hMax = $('body').offsetHeight; */

		// on applique la nouvelle hauteur sur les colonnes
		each(function() {
			var bIsd = $(colonnesInside[i]);
			if (b && b.scrollHeight>0) {
				if (bIsd) {
					var diff = (hMax - b.offsetHeight) + bIsd.clientHeight - $s.getV(bIsd);
					bIsd.style[hToU] = diff + 'px';
				} else
					if(hMax > $s.getV(b)) b.style[hToU] = hMax  - $s.getV(b) + 'px';
			}
		});
	};
	//setTimeout(fix,1);
	fix();
}

/* ToggleBlock :  block qui s'ouvre et qui se ferme */
var toggleBlock = {
	init : function(elm) {
		var head = getNode(elm, {className:"head"});
		if (head)
		//pour les besoins du design, jajoute une classe toggle a linit en mode closed
			if (elm.className.match(/\btoggleClosed\b/)) toggleClass(head, 'toggleClosed');
			var a = getNode(head, {nodeName:"a"});
			if (a)
				a.onclick = function() {
					toggleBlock.toggle(this);
					return false;
				}
	},
	// ajout les fonctionnalites du open/close (toggle);
	toggle : function(elm) {
		elm.blur();
		var scrollTop = document.body.scrollTop;

		var block = getParent(elm, {nodeName:"div", className:"blockToggle"});
		toggleClass(block, 'toggleClosed');
		//pour les besoins du design, jajoute une classe toggle au clic en mode closed
		var head = getNode(block, {className:"head"});
		toggleClass(head, 'toggleClosed');

		//fixColumns();
		document.body.scrollTop = scrollTop;
		document.body.style.zoom = 1;
		setTimeout(function() {document.body.style.zoom=0}, 1);
		setTimeout(fixCorners,2);
	}
};
//removeClass
function removeClass(element, className) {
	element.className = element.className.replace(new RegExp("\\b"+className+"\\b","g"),"");
};

//addClass
function addClass(element, className) {
	element.className += " " + className;
};

//toggleClass
function toggleClass(element, className) {
	if (element.className.match(className)) {
		removeClass(element, className)
	} else {
		addClass(element, className)
	}
};
/** *********************************************************************************************************

*/

var afterLoad = {
	functions : [],
	add : function(f) {
		this.functions.push(f);
	},
	start : function() {
		this.functions.each(function(f) {
			f();
		});
	}
};

/******
* Block Magazin (Special Carrefour)
********/
var blockMag = {
	init : function(elm){
		var next = $(elm).getNext();
		var blockScroll;
		if ($c.has(next,/(\bblockScrollH)|(\bblockScrollV)/)){
			blockScroll = next;
			blockScroll.setStyle('display','none');
		}else{
			//alert('aucun blockScroll associe au block magasin !');
			return;
		}
		var linkMag = $$("div.blockMagasin a");

		blockScroll.getElement('a.allEvents').addEvent('click',function(e){
			e = new Event(e); e.stop();
			blockScroll.setStyle('display','none');
			elm.setStyle('display','');
		});

		linkMag.each(function(elem,i){
			elem.addEvent('click',function(e){
				e = new Event(e);
				var id = elem.hash;
				if (!!id.match(/#/)){
					id = id.substr(1);
					e.stop();
				}else{
					return;
				}

				var lis = blockScroll.getElements('div.scrollMask ul li');
				var nbrLi;
				if (blockScroll.$tmp.scroll.options.infinity){
					nbrLi = lis.length==1?lis.length:lis.length-2;
				}else{
					nbrLi = lis.length;
				}
				var chk = id.charAt(0).toInt();
				if (!isNaN(chk)){
					id = id.toInt();
					if (id > nbrLi){
						return;
					}
				}else if(!$(id)){
					return;
				}
				blockScroll.setStyle('display','');
				elm.setStyle('display','none');

				blockScroll.$tmp.scroll.gotoId(id);
			});
		});
	}
}


/*******
* Tabs
 *******/
var tabs = {
	iInit: 0,
	obj:{
		'aObj':[]
	},
	init : function(elm) {
		var iArrayToUse = tabs.iInit;
		elm = $(elm);
		var uls = elm.getElements('.tabs');
		var allTabContent = elm.getElements('.tabCtn');
		var theLinks = uls.getElements('a');
		var iAllTabContent = allTabContent.length;
		var iTheLinks = theLinks[0].length;
		var as = [];
		var bRotate = elm.hasClass('rotate');

		theLinks.each(function(array) {
			as = as.extend(array);
		})
		tabs.obj.aObj[iArrayToUse] = {
			'theLinks':theLinks[0],
			'tabctn':allTabContent,
			'length':iTheLinks,
			'current':0
		};
		as.each(function(link, index, array) {
/*		si		: la class "nochange" est sur le lien, alors on ne switch pas
		si		: le nombre de liens et le nombre de tabCtn ne sont pas egales correspondent , alors pas de switch non plus
*/
			if (!link.hasClass('nochange')){
			if (iAllTabContent == iTheLinks){
				link.parentNode.indexOfTab = index;
				link.parentNode.numberOfTabs = array.length;
				link.parentNode.allTabs = array.map(function(link) {return link.parentNode});
				link.parentNode.allTabsContent = allTabContent;
					link.parentNode.onclick = function() {
					tabs.change(this, link.parentNode.indexOfTab, tabs.obj.aObj[iArrayToUse]);
					return false;
				};
				link.onclick = function() {this.parentNode.onclick()};
				$e.add(link, 'click', $e.stop);
			}}
		})
		function rotate()
		{
			setInterval(
				function()
				{
			 		tabs.change(null, null, tabs.obj.aObj[iArrayToUse]);
				},
				4000
			);
			
		}
		if(bRotate){
			rotate();
		}
		afterLoad.add(function() {tabs.sizeContents(elm)});
		tabs.iInit++;
	},
	sizeTab : function(a, ul) {
		if ($c.has(ul, 'noresize')) return;
		var newSize =  a.clientHeight + (ul.offsetHeight-a.clientHeight) - $s.getV(a);
		a.style[heightPropertyToUse] = (newSize<0) ? 0 : newSize + "px"; //IE doesn't compute size under 0
	},
	sizeContents : function(block) {
		if (block.className.match(/\b(noresize|blockTabs|blockTabsVertical)\b/)) return;
		var body = $n.node(block, {nodeName:"div", className:"body"});
		var tabsCtn = $n.childs(body, {nodeName:"div", className:"tabCtn"});
		var maxHeight = body.offsetHeight;
		tabsCtn.each(function(x) {
			x.style.display = 'block';
			if (x.offsetHeight > maxHeight) maxHeight = x.offsetHeight;
		});
		tabsCtn.each(function(x) {
			var tabBody = $n.node(x, {className:'tabBody'});
			tabBody.style[heightPropertyToUse] = tabBody.offsetHeight + (maxHeight - x.offsetHeight) - $s.getV(tabBody) + "px";
			x.style.display = '';
		});
	},
	change : function(elm, iIndex, obj) {
		if(iIndex == null)
		{

			if(obj.current == obj.length - 1)
			{
			  iIndex = 0;
			}
			else
			{
				iIndex = obj.current + 1;
			}
		}
		if(elm == null)
		{
			elm = obj.theLinks[iIndex].parentNode;
		}
		elm.allTabs.each(function(tab, iIndex) {
      $(tab).removeClass('current');
      tab.allTabsContent.removeClass('tabCurrent');
		});
		elm.addClass('current');
		elm.allTabsContent[elm.indexOfTab].addClass('tabCurrent');
		obj.current = iIndex;
	},
	centerAlign: function (elm, ul){
		elm.style.paddingTop = (ul.offsetHeight-elm.offsetHeight)/2+"px";
	}
}
	var linesOfBlocks = [];
	function sizeBlocks(parentBlock, smoothResize) { //smoothResize is a boolean for only resizing blocks that are alone in a unit beceause while loading if resize is made on a lot of blocks it's not good
		if(document.body.id == 'carrefourFr') return;
		function size(block, size){
			if (size<=0) return;
			if (block){
				var body = block.className.match(/\bblock\b/) ? $n.node(block, {nodeName: "div", className: "body"}) : block; //si on a une line ou bien un block
				if (body) body.style[heightPropertyToUse] = body.offsetHeight + size - $s.getV(body) + "px";
			}
		};
		var arrayLines = linesOfBlocks;
		if (parentBlock) {
			arrayLines = $n.nodes(parentBlock, {nodeName: 'ul', className: "line"});
			arrayLines = arrayLines.concat($n.nodes(parentBlock, {nodeName: 'div', className: "line"}));
		}
		arrayLines.eachInv(function (line) {
			var units = $n.childs(line, {className: /\b(last)?unit\b/}, {className: "noresize"});
			//remove the Bspace classe on last block of each unit
			units.each(function(unit) {
				var last = $n.lastChild(unit, {className: "(block|line)"});
				if (last)	$c.remove(last, 'Bspace');
			});
			units.each(function(unit) {
				var blocks = $n.childs(unit, {className: "(block|line)"}, {className: "noresize"});
				blocks = blocks.filter(function(oBlock) {
					var body = $(oBlock).getElement('.body');
					return !body.hasClass('noresize');
				});
				unit.blocks = blocks;
				var sizeToApply = line.clientHeight-unit.clientHeight;
				var sizePerBlock = parseInt(sizeToApply/blocks.length);
				if (blocks.length > 1 &&  smoothResize) return;
				blocks.each(function(block) {
					size(block, sizePerBlock);
				});
				//sur une division on tombe parfois sur un calcul pas precis, on resize le dernier element d'un unit, afin que le calcul soit correct
				if (blocks.length > 1) size(blocks.last(), line.clientHeight-unit.clientHeight);
			});
		});
	}

/******
* resize des contenus
******/
var contentsArray = [];
function contentAdd(ContainerAttr, childNodeAttr, numberPerLines) {
	contentsArray.push({container: ContainerAttr, child: childNodeAttr, number: numberPerLines});
}
function contentSize() {
	contentsArray.each(function(attr) {
		var containers = getNodes(document, attr.container);
		containers.each(function(container) {
			var childs = getNodes(container, attr.child);
			var lineBreak = attr.number;
			var maxH = 0, count = 1, lineArray = [];
			function sizeElements() {
				lineArray.each(function(line) {
					line.style[heightPropertyToUse] = maxH - $s.getV(line) + "px";
				});
				lineArray = [];
				count=0;
				maxH=0;
			}
			childs.each(function(child) {
				if (count==1){
					child.parentNode.style.clear="left";
				}
				if (child.offsetHeight>maxH) maxH = child.offsetHeight;
				lineArray.push(child);
				if (count >= lineBreak) {
					sizeElements();
				}
				count++;
			});
			sizeElements();
		});
	});
}
contentAdd({nodeName: 'ul', className: 'list2cols '}, {nodeName: 'li', className: 'item'}, 2);
contentAdd({nodeName: 'ul', className: 'list3cols '}, {nodeName: 'li', className: 'item'}, 3);
/**
/
/ modif
/
**/
/**
/ renvoie le left et le top d'un elm
**/
function getLeft(MyObject){
    if (MyObject.offsetParent)
	return (MyObject.offsetLeft + getLeft(MyObject.offsetParent));
    else
	return (MyObject.offsetLeft);
    }
function getTop(MyObject){
    if (MyObject.offsetParent)
	return (MyObject.offsetTop + getTop(MyObject.offsetParent));
    else
	return (MyObject.offsetTop);
    }

/**
/ array index of
**/
if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length;

    var from = Number(arguments[1]) || 0;
    from = (from < 0) ? Math.ceil(from): Math.floor(from);
    if (from < 0) from += len;
    for (; from < len; from++) {
      if (from in this && this[from] === elt)
	return from;
    }
    return -1;
  };
}

function pngTrans(elm) {
   if (document.compatMode=="BackCompat" || /MSIE [56]/.test(navigator.userAgent)) {
       var imgurl = elm.currentStyle.backgroundImage.match(/url\([\"\'](.*)[\"\']\)/);
       if (imgurl) {
			alert(imgurl)
           elm.style.backgroundImage="none";
          elm.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled='true', sizingMethod='crop' src='"+imgurl[1]+"')";
       } else {
           elm.style.filter=" ";
       }
   } else {
       elm.style.filter=" ";
   }
}
function imgPngTrans(img) {
   if (document.compatMode=="BackCompat" || /MSIE [56]/.test(navigator.userAgent)) {
		var span = document.createElement('span');
		img.parentNode.insertBefore(span, img);
		span.appendChild(img);
		with(span.style) {
			display = 'inline-block';
			filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true, sizingMethod=crop src='"+img.src+"')";
		}
		img.style.visibility = 'hidden';
	}
}
/* simple trace function avoid error for forgotten trace.log in prod */
if (window.console){
	var trace = {
	  log: function(a){
			console.log(a)
		},
	  info: function(a){
			console.info(a)
		},
	  dir: function(a){
			console.dir(a)
		}
	}
}
else
{
	var trace = {
	  log: function(a){
   //
		},
	  info: function(a){
  //
		},
	  dir: function(a){
  //
		}
	}
}
// corrige un bug d'affichage pour les navigateurs trop rapide
function retraitement()
{
	for(var i = window.Retraitement.length - 1; i > -1; i--)
	{
		window.Retraitement[i].btnLeft.style.height  = window.Retraitement[i].scrollMask.offsetHeight + 'px';
		window.Retraitement[i].btnRight.style.height = window.Retraitement[i].scrollMask.offsetHeight + 'px';
	}
};
