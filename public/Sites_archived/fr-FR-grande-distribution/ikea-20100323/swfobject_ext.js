/**
 *
 * Extra functions designed to work together with SWFObject v1.4.4 and Webtrends
 *
 */
  
 //This function checks for extra flash variables in url.
 function writeFlash(swfObject) {
	try {
		FlashVarArray=getFlashVars(swfObject.getAttribute('id'));
		for (var i=0;i<FlashVarArray.length;i++) {
			thisFlashVar=FlashVarArray[i].split("=");
			swfObject.addVariable(thisFlashVar[0], thisFlashVar[1]);
		}
	} catch (e) {
	}
	try {
        var installedVersion = deconcept.SWFObjectUtil.getPlayerVersion();  // Get installed version
        var swfObjectVersion = swfObject.getAttribute('version');   // Get version from flash obj
 
	// ----------------------------------------------------
	// 080624 PAMN - Temporary workaround
	// ----------------------------------------------------
	// See: http://www.adobe.com/support/documentation/en/flashplayer/9/releasenotes.html
	// MPA2 roomsettings requires fullscreen with wmode which 
	// requires a Flashplayer of version 9.0.115 and above when 
	// using wmode. We need to force a check on the revision as well as the
	// major version.

	if( swfObjectVersion.major == 1000 )
	{
		if(installedVersion.major < 9 || (installedVersion.major == 9 && installedVersion.minor == 0 && installedVersion.rev < 115))//Greater than version 9
		{
			//Keep failing
		}
		else
		{
			swfObjectVersion.major = installedVersion.major;
		}
	}
	// End workaround

        var flashBar = $('flashBar');   // The div containing the flash error msg
        //alert('installed= ' + installedVersion.major + ' obj= ' + swfObjectVersion.major);
        if(installedVersion.major >= swfObjectVersion.major){
    		swfObject.write("flashcontent");
        }else{
            flashBar.show();    // Show the flashbar
            $('flashcontent').hide();   // Hide (collapse) the flash area
        }
        //then rename the flash container to be able to take care of the next one (if more than one exists on the page)  (IE bug)
        var flashElement = $('flashcontent');
        flashElement.id='flashcontent_used';
	
				//Check if this is the first element in the rightContent div
				//or the element after the <a name="mainContent"></a> element in the main block
        var prev = flashElement.previous();
        var flashParent = null;
        if (prev == null) {
        	//element is the first
        	flashParent = flashElement.up(0);
        } else {
        	//check if element prevoius is the <a name="mainContent"> element
        	if (prev.tagName.toLowerCase() == 'a' && prev.readAttribute('name') == 'mainContent') {
        		flashParent = prev.up(0);
        	}
        }
        if (flashParent != null && (flashParent.hasClassName('rightContent') || flashParent.id == 'main')) {
        	flashElement.addClassName('firstFlashContent');
        }
        	        
        
    } catch (e) {
        // Fallback to old solution since new one does not work...due to missing code/files...
        try {
            swfObject.write("flashcontent");
    		//rename the container  (iebug)
    		document.getElementById('flashcontent').id='flashcontent_used';
        } catch (err) {
        }
	}
 }
 
 //Function that returns an array with Flash variables
 // The flash_id is used to identify the flash_vars. We need to add a '_' to the id since that is used in the querystring.
 // The id + '_' is then removed before pushed to the flashVar-array.
function getFlashVars(flash_id ) {
	flash_id = flash_id + '_';
	currentFlashVar = 'flash_' + flash_id ;

	thisArray=new Array();
	try {
		href=document.location.href;
		startPath='none';
		urlArray=new Array();
		urlArray=href.split('?');
		if (urlArray.length==2) {
			querystringArray=new Array();
			querystringArray=urlArray[1].split('&');
			for (var i=0;i<querystringArray.length;i++) {
				thisQueryVar=querystringArray[i].split("=");
				
				// If the id contains a space ' ' we have to replace '%20' from the url to ' '
				cleanedQuerystring = querystringArray[i].replace("%20"," ");
				//document.write(cleanedQuerystring);
				if (cleanedQuerystring.substring(0,(6 + flash_id.length))==currentFlashVar) {
					thisArray.push(cleanedQuerystring.substring(0,6) + cleanedQuerystring.substring((6 + flash_id.length),cleanedQuerystring.length));
				}
			}
		}
	} catch (e) {
	}
	return thisArray;
}

//load external javascript file
function swf_loadJS(jsFile) {
	try {
		var formFunctions = document.createElement("SCRIPT");
		
		var _typeAttr = document.createAttribute("TYPE");
        _typeAttr.nodeValue = "text/javascript";
		var _langAttr = document.createAttribute("LANGUAGE");
        _langAttr.nodeValue = "Javascript";
		var _srcAttr = document.createAttribute("SRC");
        _srcAttr.nodeValue = jsFile;
		
        formFunctions.setAttributeNode(_typeAttr);
        formFunctions.setAttributeNode(_langAttr);
		formFunctions.setAttributeNode(_srcAttr);
		
		var headTag = document.getElementsByTagName('HEAD');
		headTag[0].appendChild(formFunctions);
	} catch (e) {}
}

function swf_isDefined(variable) {
	return eval('(typeof('+variable+') != "undefined");');
}

function loadIRWStats() {
	if (! (swf_isDefined('irwStatsLoaded')) || (irwStatsLoaded != true)) {
		swf_loadJS("/ms/js/irwstats/irwstat.js");
		swf_loadJS("/ms/js/irwstats/s_code.js");
	}
}

//function for for stat generation with exception handling
function IRWdcsMultiTrack() {
	try {
		var trackRia = [];
		
		trackRia.push("IRWStats.version");
		trackRia.push("1.0");
	
		for (var IRWdcsCount = 0; IRWdcsCount < arguments.length; IRWdcsCount++) {
			if (arguments[IRWdcsCount].indexOf('DCS.dcsuri') == 0) {
				trackRia.push("IRWStats.riaRequestName");
				trackRia.push(arguments[IRWdcsCount+1].replace(/\'/g, ""));
				IRWdcsCount++;
			} else if (arguments[IRWdcsCount].indexOf('WT.ti') == 0) {
				trackRia.push("IRWStats.pageName");
				trackRia.push(arguments[IRWdcsCount+1].replace(/\'/g, ""));
				IRWdcsCount++;
			} else if (arguments[IRWdcsCount].indexOf('WT.cg_n') == 0) {
				trackRia.push("IRWStats.category");
				trackRia.push(arguments[IRWdcsCount+1].replace(/\'/g, ""));
				IRWdcsCount++;
			} else if (arguments[IRWdcsCount].indexOf('WT.cg_s') == 0) {
				trackRia.push("IRWStats.subCategory");
				trackRia.push(arguments[IRWdcsCount+1].replace(/\'/g, ""));
				IRWdcsCount++;
			}
		}
		
		var riaArgs = "";
		for (var args = 0; args < trackRia.length; args++) {
			riaArgs += "'"+trackRia[args]+"',";
		}
		riaArgs = riaArgs.substr(0, (riaArgs.length-1));
		eval("irwStatTrackRia("+riaArgs+");");
	} catch (e) {}	
}

//Function to handle flash calls from older versions
function IRWflashTrack() {
	try {
		var trackRia = [];
		
		trackRia.push("IRWStats.version");
		trackRia.push("1.0");
	
		for (var IRWdcsCount = 0; IRWdcsCount < arguments.length; IRWdcsCount++) {
			if (arguments[IRWdcsCount].indexOf('DCS.dcsuri') == 0) {
				var dcsData = arguments[IRWdcsCount+1].replace(/\'/g, "");

				var dcsArray = dcsData.split("/");

				trackRia.push("IRWStats.riaAsset");
				trackRia.push(dcsArray[0]);

				trackRia.push("IRWStats.riaAssetType");
				trackRia.push("other");

				trackRia.push("IRWStats.riaAction");
				trackRia.push(dcsArray[2]);

				trackRia.push("IRWStats.riaActionType");
				trackRia.push("unknown");

				IRWdcsCount++;

				break;
			}
		}
		
		var riaArgs = "";
		for (var args = 0; args < trackRia.length; args++) {
			riaArgs += "'"+trackRia[args]+"',";
		}
		riaArgs = riaArgs.substr(0, (riaArgs.length-1));
		eval("irwStatTrackRia("+riaArgs+");");
	} catch (e) {}	
}

// Load irwstats if not loaded (but wait til whole page is loaded)
window.onDomReady = swf_DomReady;
function swf_DomReady(fn) {
	//W3C
	if(document.addEventListener) {
		document.addEventListener("DOMContentLoaded", fn, false);
	}
	//IE
	else {
		document.onreadystatechange = function(){swf_readyState(fn)}
	}
}

function swf_readyState(fn) {
	//dom is ready for interaction
	if(document.readyState == "interactive") {
		fn();
	}
}
window.onDomReady(loadIRWStats);
