/*
 * GfK NOP DHTML on-exit (c) 2007 GfK NOP Ltd
 */
	// *****  Use this var to enable/disable this script *****
    var active = true;
    
    // *****  Use this var to enable/disable this script *****
    // Check for active flag...
    if (active) {

        // The incidence rate... 100 = everyone, -1 = no one
    	var INCIDENCE = 100;

    	// --
    	var theDate=new Date();
    	month=theDate.getMonth();
    	month=month+3;
    	theDate.setMonth(month);   // this is all for the cookie expiry
    	
    	var colorscheme="#3399fd;text-decoration:none;"; // for border and link colour
    	var d=document.createElement("div"); // create the invitation layer

        var winHeight = (document.compatMode=="CSS1Compat")?document.documentElement.clientHeight:document.body.clientHeight;
        if (self.innerHeight) { // for Safari (or all but Explorer)
        	winHeight = self.innerHeight;
        }
        
    	// now style it
        d.style.borderStyle="solid";
    	d.style.borderWidth="2px";
    	d.style.borderColor="#333333";
    	d.style.width="500px";
    	//d.style.height="220px";
    	d.style.fontFamily="verdana,sans-serif";
    	d.style.fontSize="10px";
    	d.style.textAlign="center";
    	d.style.zIndex="100";
    	d.style.padding="10px";
    	d.style.position="absolute";
    	d.style.left=((document.body.clientWidth/2) - 250)+"px";
    	d.style.top=((winHeight/2) - 110)+"px";
        d.style.backgroundColor="#ffffff";
        
    	// Set valid sites
        var Sites=new Array(); 
		Sites[0]="/dk/da";
    	
    	// Set incidence rates
    	var Incidence=new Array();
		Incidence[0]=10;
		
    	var thissite = document.location.href.replace(/\/$/,"");
        var str = thissite.split(/ikea.com/);
        thissite = thissite.replace(str[0],"");     // remove everything in URL preceding ikea.com
        
        var myNum = -1;
    	var mySite = "";
        
    	for (i=0;i<Sites.length;i++) {
			mySplitSite = "/" + Sites[i].split("/")[1];
    		if (thissite == "ikea.com" + Sites[i] || thissite == "ikea.com" + mySplitSite)
			{
    			myNum=i;
    			mySite=Sites[i];
				break;
    		}
    	}
        
    	if (myNum>=0) {
    		INCIDENCE = Incidence[myNum];
    	}
    	else
		{
    		INCIDENCE = 0;
    	}
    	
    	if (INCIDENCE == undefined)
		{
    		INCIDENCE=0; // ie we just don't pop them
    	}
    	var popStart = "<p><img src=\"http://www.ikea.com/ms/img/logos/logo92x33.gif\" alt=\"Ikea logo\" /></p><p>";
        if (mySite=="/dk/da")
		{
			d.innerHTML = popStart + "<b>Hj\u00e6lp os med at blive bedre!</b><p>Deltag i vores korte unders\u00f8gelse og v\u00e6r med til at g\u00f8re IKEA.dk bedre for dig!</p><br/><input type=\"button\" onclick=\"run_survey('dk');return false;\" value=\"Deltag\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"button\" onclick=\"close_popup();return false;\" value=\"Nej tak\">";
			survey_cookie="643115_survey";
			//d.style.display="none";
        }
        else
		{
            d.style.display="none";
        }
        // check if mySite found
        if(mySite != "")
		{
            // check against incidence rate (100=everyone, -1 = no one)
			var thisCookie = parseInt(get_cookie(survey_cookie));
			var surveyDisplayCount = 2; //The number of times a user must enter a valid survey page before it display
			
			if (thisCookie)
			{
				if(mySite == "/dk/da")
				{
					if((thisCookie+1) == surveyDisplayCount) //If the user has visited the homepage the correct amount of times
					{
						window.setTimeout("document.body.appendChild(d)",1500); // pop up question after 1.5 secs
						set_cookie(survey_cookie,++thisCookie); //Sum up visits to the homepage
						//window.setTimeout("window.open('http://survey.trictrac.com/servlet/trictrac?e=GdPmqm8zdpFD8Ldm')",1500); // pop up question after 1.5 secs
						//set_cookie(survey_cookie,maxCookieCount);
					}
					else if((thisCookie+1) < surveyDisplayCount && thisCookie > 0)
					{
						set_cookie(survey_cookie,++thisCookie); //Sum up visits to the homepage
					}
				}
			}
			else
			{
				if(incidence_rate(INCIDENCE))
				{
					set_cookie(survey_cookie,"1");
					if(surveyDisplayCount == 1)
					{
						run_survey('dk');
					}
				}
				else
				{
					set_cookie(survey_cookie,"-1");
				}
			}
        }
    }
    
	function close_popup() {
		d.style.display="none";
	}

	var surveyPopupWindow;
	var looper;
	
	function run_survey(inMarket)
	{
		if(inMarket == 'dk')
		{
			thisSurvey = window.open('http://survey.trictrac.com/servlet/trictrac?e=GdPmqm8zdpFD8Ldm','mysurvey'); // pop up question after 1.5 secs
		}
		thisSurvey.focus();
		close_popup();
	}
	
	function checker() {
		try {
			myopener = surveyPopupWindow.location.href;
		}
		catch(err) {
			window.clearInterval(looper);
			close_popup();
		}
	}

	// Functions to set, get and delete cookies
	// First, to set a cookie - set a 3 month expiry date. Year is handled automatically if the month spills over
	function set_cookie(cookiename,value) {
		document.cookie = cookiename + "=" + value +";expires=" + theDate.toGMTString() +";path=/";
	}

	function get_cookie(cookiename) {
		// document.cookie gives us a list of all the cookies from this domain
		// we need to find just the cookie we are interested in
		// take a substring between "cookiename=" and ";" - this is the value of the cookie
		// this function will return either the value of the cookie, or a null value if it's not there

  		var nameStr = cookiename + "=";
  		var maxLen = document.cookie.length
  		var i = 0
  		while (i < maxLen) {
            var j = i + nameStr.length
            if (document.cookie.substring(i,j) == nameStr) {
                var cookieEnd = document.cookie.indexOf(";",j);
                if (cookieEnd == -1) {
                    cookieEnd = document.cookie.length;
                }
                return unescape(document.cookie.substring(j,cookieEnd));
            }
    		i++
  		}
  		return "";
	}

	// simply erase the cookie by making it expire before now
	function erase_cookie(cookiename) {
		document.cookie = cookiename + "=0;expires=Sun, 26 Mar-2000 12:00:00 GMT;path=/";
	}

	// do they accept cookies?
	// simply drop a test cookie and try to retrieve it. if it's not there, they don't take cookies
	function check_cookie_accept() {
		set_cookie('testaccept','1');
		var ck=get_cookie('testaccept');
		if (ck==1) {
			return 1;
		}
		else {
			return 0;
		}
		// erase it for good measure
		erase_cookie('testaccept');
	}

	function incidence_rate(incidence) {
		var percentage=incidence;
		var randNum = Math.round(Math.random() * 100);
		if (randNum<=percentage) {
			return 1; // success
		}
		else {
			return 0; // failed
		}
	}