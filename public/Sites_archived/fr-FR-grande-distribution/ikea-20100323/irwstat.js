var irwStatLocalVars = new Array();
var irwStatsLoaded = true;
var irwStatsInitialized = false;

var irwLocalFlags = new Array();
var irwLocalTrackVars = new Array();

// NB! Setting irwStatsDebug to true will make all errors in here throw alerts aswell!
var irwStatsDebug = false;

var urls = self.location.host+self.location.pathname;  //KPIA

// NB! In order to get case-insensitivity, all tMapArr keys need to be lower-case!
tMapArr = new Array();
tMapArr["pagename"] = { toVar:false, eval:"s.pageName = value; irwstatAddLocalFlag('pageName_SET')", changeCase:"lower" };
tMapArr["pagenamelocal"] = { toVar:false, eval:"s.prop1 = value; irwstatAddLocalFlag('pageNameLocal_SET')", changeCase:"lower" };
tMapArr["fallback_pagename"] = { toVar:true, varName:"s.pageName", changeCase:"lower" };
tMapArr["fallback_pagenamelocal"] = { toVar:true, varName:"s.prop1", changeCase:"lower" };
tMapArr["country"] = { toVar:true, varName:"s.prop8", changeCase:"lower" };
tMapArr["language"] = { toVar:true, varName:"s.prop17", changeCase:"lower" };
tMapArr["pagetype"] = { toVar:true, varName:"s.prop5", changeCase:"lower" };
tMapArr["event"] = { toVar:false, eval:"irwstatOmnitureAddEvent(value)" };

tMapArr["category"] = { toVar:false, eval:"s.prop2 = value; irwStatLocalVars[\"riaCategory\"] = value", changeCase:"lower" };  // omniture-name: department  KPIA
tMapArr["subcategory"] = { toVar:true, varName:"s.prop3", changeCase:"lower" };    // omniture-name: category
tMapArr["chapter"] = { toVar:true, varName:"s.prop4", changeCase:"lower" };        // omniture-name: Sub category
tMapArr["system"] = { toVar:true, varName:"s.prop21", changeCase:"lower" };        // omniture-name: Subsub Category
tMapArr["systemchapter"] = { toVar:true, varName:"s.prop22", changeCase:"lower" }; // omniture-name: Subsubsub Category

// 2.6.3 - Local names (not sent to omniture, used to build pagenamelocal)
tMapArr["categorylocal"] = { toVar:false, eval:"irwStatLocalVars[\"categoryLocal\"] = value", changeCase:"lower" };
tMapArr["subcategorylocal"] = { toVar:false, eval:"irwStatLocalVars[\"subcategoryLocal\"] = value", changeCase:"lower" };
tMapArr["chapterlocal"] = { toVar:false, eval:"irwStatLocalVars[\"chapterLocal\"] = value", changeCase:"lower" };
tMapArr["systemlocal"] = { toVar:false, eval:"irwStatLocalVars[\"systemLocal\"] = value", changeCase:"lower" };
tMapArr["systemchapterlocal"] = { toVar:false, eval:"irwStatLocalVars[\"systemChapterLocal\"] = value", changeCase:"lower" };
// End - 2.6.3 - Local names

tMapArr["nofsearchresults"] = { toVar:true, varName:"s.prop7", changeCase:"lower" };
tMapArr["products"] = { toVar:false, eval:"irwstatOmnitureAddProduct(value); irwStatLocalVars[\"products\"] = value", changeCase:"lower" };
tMapArr["productfindingmethod"] = { toVar:true, varName:"s.eVar3", changeCase:"lower" };
tMapArr["scinuse"] = { toVar:false, eval:"irwstatOmnitureAddEvent('event6')" };
tMapArr["scinstock"] = { toVar:true, conditional:[{option:"yes", varName:"s.prop11"}, {option:"no", eval:"irwstatOmnitureAddEvent('event13'); s.prop11 = \"no\""}], changeCase:"lower" };
tMapArr["scstoreno"] = { toVar:true, varName:"s.prop10", changeCase:"lower" };
tMapArr["scproduct"] = { toVar:false, eval:"s.prop12 = value; irwstatOmnitureAddProduct(value)", changeCase:"lower" };
tMapArr["sccontactmethod"] = { toVar:true, varName:"s.prop13", changeCase:"lower" };
tMapArr["ecomorderid"] = { toVar:true, varName:"s.purchaseID", changeCase:"lower" };
tMapArr["ecompaymentmethod"] = { toVar:true, varName:"s.eVar5", changeCase:"lower" };
tMapArr["ecomshippingmethod"] = { toVar:true, varName:"s.eVar6", changeCase:"lower" };
tMapArr["newslettersignup"] = { toVar:true, conditional:[{option:"yes", eval:"irwstatOmnitureAddEvent('event9')"}], changeCase:"lower" };
tMapArr["internalpagetype"] = { toVar:false, eval:"irwStatLocalVars[\"internalPageType\"] = value" };
tMapArr["addtocart"] = { toVar:true, conditional:[{option:"yes", eval:"irwstatOmnitureAddEvent('scAdd')"}], changeCase:"lower" };
tMapArr["scaddproducts"] = { toVar:false, eval:"irwstatAddLocalFlag('scAddProducts_SET'); irwStatLocalVars[\"scAddProducts\"] = value", changeCase:"lower" };
tMapArr["productalreadyincart"] = { toVar: false, conditional:[{option:"yes", eval:"irwstatAddLocalFlag('productAlreadyInCart_SET');"}], changeCase:"lower" };
tMapArr["removefromcart"] = { toVar:true, conditional:[{option:"yes", eval:"irwstatOmnitureAddEvent('scRemove')"}], changeCase:"lower" };
tMapArr["checkoutstart"] = { toVar:true, conditional:[{option:"yes", eval:"irwstatOmnitureAddEvent('scCheckout')"}], changeCase:"lower" };
tMapArr["currencycode"] = { toVar:true, varName:"s.currencyCode", changeCase:"upper" };
tMapArr["familyloginsuccessful"] = { toVar: false };
tMapArr["familyusersignedup"] = { toVar:true, conditional:[{option:"yes", eval:"irwstatOmnitureAddEvent('event8')"}], changeCase:"lower" };
tMapArr["shoppingcartview"] = { toVar:true, conditional:[{option:"yes", eval:"irwstatOmnitureAddEvent('scView')" }], changeCase:"lower" };
tMapArr["downloadurl"] = { toVar:false, eval:"s.prop9 = value; s.eVar9 = value; irwstatOmnitureAddEvent('event5')", changeCase:"lower" };

// 2.5.3+2.7.4    + s.eVar29 on prodview
tMapArr["prodview"] = { toVar:true, conditional:[{option:"yes", eval:"irwstatOmnitureAddEvent('prodView'); irwstatAddLocalFlag('prodView'); s.eVar29 = '+1'" }], changeCase:"lower" };
tMapArr["event3"] = { toVar:true, conditional:[{option:"yes", eval:"irwstatOmnitureAddEvent('event3')" }], changeCase:"lower" };
tMapArr["purchase"] = { toVar:true, conditional:[{option:"yes", eval:"irwstatOmnitureAddEvent('purchase')" }], changeCase:"lower" };
// End - 2.5.3

// 2.7.2
tMapArr["front"] = { toVar:true, conditional:[{option:"yes", eval:"irwstatAddLocalFlag('front')" }], changeCase:"lower" };
// End - 2.7.2

// Phase2 Form addition
tMapArr["hasform"] = { toVar:false, conditional:[{option:"yes", eval:"irwstatAddLocalFlag('form')" }], changeCase:"lower" };
// End - Phase2 Form addition

// newproduct fix
tMapArr["newproduct"] = { toVar:false, eval:"irwstatAddLocalFlag('newProduct')", changeCase:"lower" };

// merchandising category change
tMapArr["merchandisingcategory"] = { toVar:false, conditional:[{option:"yes", eval:"irwstatAddLocalFlag('merchandisingcategory_SET')"}], changeCase:"lower" };

// IKEA00216626
tMapArr["friendlypagename"] = { toVar:false, eval:"irwStatLocalVars[\"friendlyPageName\"] = value" };
// End - IKEA00216626

// Phase2 Filter/Sort (Page functionality property)
tMapArr["filter"] = { toVar:false, eval:"s.prop24 = \"filter>\"+value", changeCase:"lower" };
tMapArr["sort"] = { toVar:false, eval:"s.prop24 = \"sort>\"+value", changeCase:"lower" };
// End - Phase2 Filter/Sort

// Phase2 Form errors
tMapArr["formerrorfields"] = { toVar:false, eval:"irwstatAddLocalFlag('formErrorFields_SET'); irwStatLocalVars[\"formErrorFields\"] = value", changeCase:"lower" };
tMapArr["formerrortexts"] = { toVar:false, eval:"irwstatAddLocalFlag('formErrorTexts_SET'); irwStatLocalVars[\"formErrorTexts\"] = value", changeCase:"lower" };
tMapArr["formerrorname"] = { toVar:false, eval:"irwstatAddLocalFlag('formErrorName_SET'); irwStatLocalVars[\"formErrorName\"] = value", changeCase:"lower" };
// End - Phase2 Form errors

// Phase2 Search visited
tMapArr["searchvisited"] = { toVar:false, eval:"s.eVar27 = '+1'", changeCase:"lower" };
// End - Phase2 Search visited

// Phase2 Page functionality
tMapArr["pagefunctionality"] = { toVar:true, varName:"s.prop24", changeCase:"lower" };
// End - Phase2 Page functionality

// Phase2 Family Discout
tMapArr["familydiscount"] = { toVar:false, conditional:[{option:"yes", eval:"irwstatOmnitureAddEvent('event14')"}], changeCase:"lower" };
// End - Phase2 Family Discout

// Phase2 Member Signup
tMapArr["memberloggedin"] = { toVar:false, eval:"irwstatOmnitureAddEvent('event2'); s.prop28 = value; irwstatAddLocalFlag('memberType_SET'); irwstatAddLocalFlag('memberLoggedIn_SET')", changeCase:"lower" };
tMapArr["membertype"] = { toVar:false, eval:"s.prop28 = value; irwstatAddLocalFlag('memberType_SET')", changeCase:"lower" };
tMapArr["membersignupstart"] = { toVar:false, conditional:[{option:"yes", eval:"irwstatAddLocalFlag('memberSignupStart_SET'); irwstatOmnitureAddEvent('event24')"}], changeCase:"lower" };
tMapArr["membersignupstarted"] = { toVar:false, conditional:[{option:"yes", eval:"irwstatAddLocalFlag('memberSignupStarted_SET');"}], changeCase:"lower" };
// End - Phase2 Member Signup

// Phase2 Shopping List
tMapArr["emailshoppinglist"] = { toVar:false, conditional:[{option:"yes", eval:"s.eVar30 = 'email'; irwstatOmnitureAddEvent('event20')"}], changeCase:"lower" };
// End - Phase2 Shopping List

//CR #IKEA00642649
tMapArr["usertypetransition"] = { toVar:false, eval:"irwstatOmnitureAddEvent('event15'); s.eVar32 = value", changeCase:"lower" };

// Phase2 Local store number
tMapArr["localstoreno"] = { toVar:false, eval:"s.prop32 = value; s.eVar17 = value", changeCase:"lower" };
tMapArr["newsletterstoreno"] = { toVar:false, eval:"s.prop32 = value", changeCase:"lower" };
// End - Phase2 Local store number

// ## FIX ##
tMapArr["trackdownload"] = { toVar:false, conditional:[{option:"no", eval:"irwstatAddLocalFlag('disableDownloadTrack_SET');"}], changeCase:"lower" };
// ## FIX ##

// ## FIX ##
tMapArr["riaapplicationstart"] = { toVar: false };
tMapArr["riaapplicationcomplete"] = { toVar: false };
// ## FIX ##

tMapArr["supportrequest"] = { toVar: false, eval:"s.eVar31 = value; irwstatOmnitureAddEvent('event16')", changeCase:"lower" };
tMapArr["addbypartnumbererror"] = { toVar: false, conditional:[{option:"yes", eval:"irwstatAddLocalFlag('addByPartNumberError_SET');"}], changeCase:"lower" };

// Flash tracking
tMapArr["version"] = { toVar:false, eval:"irwStatLocalVars[\"version\"] = value", changeCase:"lower" };
tMapArr["riaassettype"] = { toVar:false, eval:"irwStatLocalVars[\"riaAssetType\"] = value", changeCase:"lower" }; 
tMapArr["riaasset"] = { toVar:false, eval:"irwStatLocalVars[\"riaAsset\"] = value", changeCase:"lower" }; 
tMapArr["riaaction"] = { toVar:false, eval:"irwStatLocalVars[\"riaAction\"] = value; irwstatAddLocalFlag('riaAction_SET');", changeCase:"lower" };
tMapArr["riaactiontype"] = { toVar:false, eval:"irwStatLocalVars[\"riaActionType\"] = value", changeCase:"lower" };
tMapArr["riaroomset"] = { toVar:false, eval:"irwStatLocalVars[\"riaRoomSet\"] = value", changeCase:"lower" };
tMapArr["riapageview"] = { toVar:false, conditional:[{option:"yes", eval:"irwstatAddLocalFlag('riaPageView_SET');"}], changeCase:"lower" };
tMapArr["riarequestname"] = { toVar:false, eval:"irwStatLocalVars[\"riaRequestName\"] = value; irwstatAddLocalFlag('riaRequestName_SET');", changeCase:"lower" };
//tMapArr["riaproduct"] = { toVar:false, eval:"irwstatOmnitureAddProduct(value); irwstatOmnitureAddEvent('prodView'); irwstatAddLocalFlag('prodView'); s.eVar29 = '+1'", changeCase:"lower" };

// KPIA Added for Billy planner and Besta Planner
tMapArr["riaproduct"] = { toVar:false, eval:"irwstatOmnitureAddProduct(value); irwstatOmnitureAddEvent('event12'); irwstatAddLocalFlag('event12'); s.eVar29 = '+1'", changeCase:"lower" };
//

/**
 * @desc Add a flag for local processing
 * @param flagName (String) - Name of flag to set
 */
function irwstatAddLocalFlag(flagName) {
	var __funcName__ = "irwstatAddLocalFlag";

	try {
		irwLocalFlags.push(flagName);
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

/**
 * @desc Check if a local flag has been set
 * @param flagName (String) - Name of flag to check status of
 * @return Bool - True/False if flag has been set
 */
function irwstatCheckLocalFlag(flagName) {
	var __funcName__ = "irwstatCheckLocalFlag";

	try {
		for (var flagCount = 0; flagCount < irwLocalFlags.length; flagCount++) {
			if (irwLocalFlags[flagCount] == flagName) {
				return true;
			}
		}
		
		return false;
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwstatAddTrackVar(varName) {
	var __funcName__ = "irwstatAddTrackVar";

	try {
		varName = varName.replace(/^IRWStats\.(.*)$/, "$1").toLowerCase();
		irwLocalTrackVars.push(varName);
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwstatCheckTrackVar(varName) {
	var __funcName__ = "irwstatCheckTrackVar";

	try {
		for (var varCount = 0; varCount < irwLocalTrackVars.length; varCount++) {
			if (irwLocalTrackVars[varCount] == varName) {
				return true;
			}
		}
		
		return false;
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwstatAddMetaTag(tagName, tagValue) {
	var __funcName__ = "irwstatAddMetaTag";

	try {
		var metaTags = document.getElementsByTagName('META');
		var irwTags = new Array();
		
	  	for (var metaCount = 0; metaCount < metaTags.length; metaCount++) { 
	   		var item = metaTags[metaCount];
			
			if (item.getAttribute("HTTP-EQUIV")) {
				continue;
			}
			
	 		var itemName = item.getAttribute("NAME");
			if (itemName == tagName) {
				document.getElementsByTagName('HEAD')[0].removeChild(item);
			}
	  	}
		
		// Tag wasn't found adding a new tag
		var headTags = document.getElementsByTagName('HEAD');
		var newMetaTag = document.createElement('META');
		
		var nameAttr = document.createAttribute("NAME");
        nameAttr.nodeValue = tagName;
		
        var contentAttr = document.createAttribute("CONTENT");
        contentAttr.nodeValue = tagValue;
		
        newMetaTag.setAttributeNode(nameAttr);
        newMetaTag.setAttributeNode(contentAttr);
		
		headTags[0].appendChild(newMetaTag);
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwstatReadMetaTags() {
	var __funcName__ = "irwstatReadMetaTags";

	try {
		var metaTags = document.getElementsByTagName('META');
		var irwTags = new Array();

	  	for (var metaCount = 0; metaCount < metaTags.length; metaCount++) { 
	   		var item = metaTags[metaCount];
			
			if (item.getAttribute("HTTP-EQUIV")) {
				var equiv = item.getAttribute("HTTP-EQUIV");
			
				if (equiv.toUpperCase() != "CONTENT-TYPE") {
					continue;
				}
				
				if (item.getAttribute("CONTENT")) {
					var charset = item.getAttribute("CONTENT");
					if (charset.indexOf("charset=") < 0) {
						continue;
					}
					charset = charset.substr(charset.indexOf("charset=")+8);
					irwstatOmnitureAddCharset(charset);
				}
			}
			
	 		var itemName = item.getAttribute("NAME");
			if (itemName == null) {
				continue;
			}
	  		if (itemName.substring(0, 9) == "IRWStats." ) {
	  			var tag = { };
				tag.name = itemName.substr(9, itemName.length).toLowerCase();
				tag.value = item.getAttribute("CONTENT").replace(/\'/g, '').replace(/\"/g, '');
	  			irwTags.push(tag);
	  		}
	  	}
		
		// Add pagename and pagenamelocal if data is missing
		var tag = { };
		
		if ((irwTags.length < 1) || (! irwstatCheckTag("pagename", irwTags))) {
			tag.name = "fallback_pagename";
			tag.value = location.href.replace(/\'/g, '').replace(/\"/g, '');;
			irwTags.push(tag);
		}
		
		if ((irwTags.length < 2) || (! irwstatCheckTag("pagenamelocal", irwTags))) {
			tag = { };
			tag.name = "fallback_pagenamelocal";
			tag.value = document.title.replace(/\'/g, '').replace(/\"/g, '');;
			irwTags.push(tag);
		}
		
		return irwTags;
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwstatCheckTag(tagName, irwTags) {
	var __funcName__ = "irwstatCheckTag";

	try {
		for (var tagCount = 0; tagCount < irwTags.length; tagCount++) {
			if (irwTags[tagCount].name == tagName) {
				return true;
			}
		}
		
		return false;
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwstatDisableTag(tagName, irwTags) {
	var __funcName__ = "irwstatDisableTag";
	
	try {
		for (var tagCount = 0; tagCount < irwTags.length; tagCount++) {
			if (irwTags[tagCount].name == tagName) {
				irwTags[tagCount].value = null;
				break;
			}
		}
		
		return irwTags;
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwstatGetCookie(name) {
	var __funcName__ = "irwstatGetCookie";

	try {
		// Is there any cookies at all?
		if (document.cookie.length > 0) {
			// Look for the cookie named 'name'.
			start = document.cookie.indexOf(name + "=");
			if (start != -1) {
				// Found it. Now retrieve the value of the cookie
				// and return it unescaped.
				start = start + name.length+1; 
				end = document.cookie.indexOf(";", start);
				if (end == -1) end = document.cookie.length;
				return unescape(document.cookie.substring(start, end));
			} 
		}
		
		// Cookie not found, return empty string.
		return "";
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwstatSetCookie(name, value, expiredays) {
	var __funcName__ = "irwstatSetCookie";

	try {
		// Make a new date-object to get the expiration-date.
		var exdate = new Date();
		
		// Expiration-date = now+expiredays.
		exdate.setDate(exdate.getDate() + expiredays);
		
		var domain = window.location.href.replace(/^http[s]?:\/\/([^\/]+)\/.*$/, "$1"); 
		domain = domain.replace(/^.*(\.[^\.]+\.[^\.]+)$/, "$1");
		
		// Set the cookie. (And escape the value)
		document.cookie = name + "=" + escape(value)+
		((expiredays==null) ? "" : ";expires="+exdate.toGMTString())+"; path=/; domain="+domain;
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

// ## FIX ## - Cookie persistance should be same as Commerce Login...
function irwstatSetTrailingTag(tagName, tagValue) {
	var __funcName__ = "irwstatSetTrailingTag";
	
	try {
		var trailingTag = irwstatGetCookie("IRWStats.trailingTag");		
		irwstatSetCookie("IRWStats.trailingTag", trailingTag+tagName+","+tagValue+"|", 1);
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwstatReadTrailingTag() {
	var __funcName__ = "irwstatReadTrailingTag";
	
	try {
		var trailingTag = irwstatGetCookie("IRWStats.trailingTag");
		irwstatSetCookie("IRWStats.trailingTag", "", -1);
		
		while (trailingTag.length > 0) {
			var tagName = trailingTag.substr(0, trailingTag.indexOf(","));
			var tagValue = "";
			if (trailingTag.indexOf("|") > 0) {
				tagValue = trailingTag.substr(trailingTag.indexOf(",")+1, trailingTag.indexOf("|") - (trailingTag.indexOf(",")+1));
			} else {
				tagValue = trailingTag.substr(trailingTag.indexOf(",")+1);
			}
			
			if (trailingTag.indexOf("|") > 0) {
				trailingTag = trailingTag.substr(trailingTag.indexOf("|")+1);
			} else {
				trailingTag = "";
			}
			
			
			irwstatAddMetaTag(tagName, tagValue);
		}
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwstatCheckCategories(irwTags) {
	var __funcName__ = "irwstatCheckCategories";
	
	try {
		var hasCategory = false;
		var hasSubCategory = false;
		var hasChapter = false;
		var hasSystem = false;
		var hasSystemChapter = false;
		
		if (irwstatCheckTag("category", irwTags)) hasCategory = true;
		if (irwstatCheckTag("subcategory", irwTags)) hasSubCategory = true;
		if (irwstatCheckTag("chapter", irwTags)) hasChapter = true;
		if (irwstatCheckTag("system", irwTags)) hasSystem = true;
		if (irwstatCheckTag("systemchapter", irwTags)) hasSystemChapter = true;
		
		if (! hasCategory) hasSubCategory = false;
		if (! hasSubCategory) hasChapter = false;
		if ((! hasSubCategory) && (! hasChapter)) hasSystem = false;
		if (! hasSystem) hasSystemChapter = false;
		
		// Invalidate tags that shouldn't be allowed
		if (! hasCategory) {
			irwTags = irwstatDisableTag("category", irwTags);
			irwTags = irwstatDisableTag("categorylocal", irwTags);
		} else {
			irwstatAddLocalFlag("hasCategory");
		}
		if (! hasSubCategory) {
			irwTags = irwstatDisableTag("subcategory", irwTags);
			irwTags = irwstatDisableTag("subcategorylocal", irwTags);
		} else {
			irwstatAddLocalFlag("hasSubCategory");
		}
		if (! hasChapter) {
			irwTags = irwstatDisableTag("chapter", irwTags);
			irwTags = irwstatDisableTag("chapterlocal", irwTags);
		} else {
			irwstatAddLocalFlag("hasChapter");
		}
		if (! hasSystem) {
			irwTags = irwstatDisableTag("system", irwTags);
			irwTags = irwstatDisableTag("systemlocal", irwTags);
		} else {
			irwstatAddLocalFlag("hasSystem");
		}
		if (! hasSystemChapter) {
			irwTags = irwstatDisableTag("systemchapter", irwTags);
			irwTags = irwstatDisableTag("systemchapterlocal", irwTags);
		} else {
			irwstatAddLocalFlag("hasSystemChapter");
		}
		
		return irwTags;
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwstatPostProcessVariable(varName) {
	var __funcName__ = "irwstatPostProcessVariable";

	try {
		omnitureAddLinkTrack(varName);
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwstatPostProcessEval(evalString, value) {
	var __funcName__ = "irwstatPostProcessEval";

	try {
		var matches = []
		matches.push({regexp:/^.*(s\.[^\ \=]+)[\ ]?\=.*$/g, replace:"$1"});
		matches.push({regexp:/^.*irwstatOmnitureAddEvent[\ ]?\([\ ]?value[\ ]?\).*$/g, replace:false});
		matches.push({regexp:/^.*irwstatOmnitureAddEvent[\ ]?\(\'([^\']+)\'\).*$/g, replace:"$1"});
		matches.push({regexp:/^.*irwstatOmnitureAddProduct[\ ]?\([\ ]?value[\ ]?\).*$/g, replace:"s.products"});
		matches.push({regexp:/^.*irwstatOmnitureAddProduct[\ ]?\(\'([^\']+)\'\).*$/g, replace:"s.products"});
		
		for (var matchNo = 0; matchNo < matches.length; matchNo++) {
			if (matches[matchNo].regexp.test(evalString)) {
				if (matches[matchNo].replace !== false) {
					irwstatPostProcessVariable(evalString.replace(matches[matchNo].regexp, matches[matchNo].replace));
				} else {
					irwstatPostProcessVariable(value);
				}
			}
		}
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwstatPrepareVendor(tags) {
	var __funcName__ = "irwstatPrepareVendor";

	try {
		// Check categories before processing
		tags = irwstatCheckCategories(tags);
	
		for (var metaCount = 0; metaCount < tags.length; metaCount++) {
			if (typeof(tMapArr[tags[metaCount].name]) != "object") {
				continue;
			}
		
			var tagMap = tMapArr[tags[metaCount].name];
			var value = tags[metaCount].value;
			
			// Allow other functions to invalidate a tag by setting null
			if (value == null) {
				continue;
			}
			
			if (typeof(tagMap.changeCase) != "undefined") {
				if (tagMap.changeCase.toLowerCase() == "lower") {
					value = value.toLowerCase();
				} else if (tagMap.changeCase.toLowerCase() == "upper") {
					value = value.toUpperCase();
				}
			}
			
			value = value.replace(/[\ ]*\|[\ ]*/g, '>');
				
			if ((tagMap.toVar) && (typeof(tagMap.conditional) == "undefined")) {
				if (typeof(tagMap.varName) != "undefined") {
					eval(tagMap.varName+"='"+value+"'");
					if (irwstatCheckTrackVar(tags[metaCount].name)) {
						irwstatPostProcessVariable(tagMap.varName);
					}
				}
			} else if (typeof(tagMap.conditional) != "undefined") {
				for (condCount = 0; condCount < tagMap.conditional.length; condCount++) {
					if (tags[metaCount].value.toLowerCase() == tagMap.conditional[condCount].option.toLowerCase()) {
						if (typeof(tagMap.conditional[condCount].varName) != "undefined") {
							eval(tagMap.conditional[condCount].varName+"='"+value+"'");
							if (irwstatCheckTrackVar(tags[metaCount].name)) {
								irwstatPostProcessVariable(tagMap.conditional[condCount].varName);
							}
						} else if (typeof(tagMap.conditional[condCount].eval) != "undefined") {
							eval(tagMap.conditional[condCount].eval);
							if (irwstatCheckTrackVar(tags[metaCount].name)) {
								irwstatPostProcessEval(tagMap.conditional[condCount].eval, value);
							}
						}
					}
				}
			} else if (typeof(tMapArr[tags[metaCount].name].eval) != "undefined") {
				eval(tMapArr[tags[metaCount].name].eval);
				if (irwstatCheckTrackVar(tags[metaCount].name)) {
					irwstatPostProcessEval(tMapArr[tags[metaCount].name].eval, value);
				}
			}
		}
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

// Omniture specific global-variable
var s_account = "";

// Statistics global-variable (dev or live)
var irwstats_stattype = "";

// Global-variables that will be loaded by omniture
var s_language = "";
var s_country = "";
var s_trackingServer = true;

var s_urls = "";

function irwstatPrepareConfig() {
	var __funcName__ = "irwstatPrepareConfig";

	try {
		irwstatArgs = "";
		for (var irwstat_arg = 0; irwstat_arg < arguments.length; irwstat_arg++) {
			irwstatArgs += "'"+arguments[irwstat_arg]+"', ";
		}
		
		if (irwstatArgs.length > 0) {
			irwstatArgs = irwstatArgs.substr(0, (irwstatArgs.length-2));
		}
		eval("irwstatPrepareOmnitureConfig("+irwstatArgs+");");
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwstatPrepareOmnitureConfig() {
	var __funcName__ = "irwstatPrepareOmnitureConfig";

	if (arguments.length < 2) {
		var myLocation = document.location.href;
		var serverHost = "";
		if (myLocation.indexOf('http://') == 0) {
			var serverHost = myLocation.substr(myLocation.indexOf('http://')+7, myLocation.indexOf('/', myLocation.indexOf('http://')+7)-7);
		} else if (myLocation.indexOf('https://') == 0) {
			var serverHost = myLocation.substr(myLocation.indexOf('https://')+8, myLocation.indexOf('/', myLocation.indexOf('https://')+8)-8);
		}
		
		if ((serverHost == "www.ikea.com")
		   || (serverHost == "secure.ikea.com")
		   || (serverHost == "preview.ikea.com")) {
			irwstats_stattype = "live";
		} else if (serverHost.length > 0) {
			irwstats_stattype = "dev";
		}
		
		if (arguments.length == 1) {
			var locale = arguments[0];
		} else {
			var metaTags = document.getElementsByTagName('META');
			
		  	for (var metaCount = 0; metaCount < metaTags.length; metaCount++) { 
		   		var item = metaTags[metaCount];
				
				if (item.getAttribute("HTTP-EQUIV")) {
					continue;
				}
				
		 		var itemName = item.getAttribute("NAME");
				if (itemName.toLowerCase() == "language") {
					var locale = item.getAttribute("CONTENT");
					locale.replace(/-/, "_");
				}
		  	}
		}
	} else {
		var locale = arguments[0];
		irwstats_stattype = arguments[1];
	}

	try {
		s_language = locale.substr(0, locale.indexOf("_")).toLowerCase();
		s_country = locale.substr(locale.indexOf("_")+1).toLowerCase();
		s_urls = urls;  //KPIA 
		
		if (irwstats_stattype == "dev") {
			s_account = "ikea"+s_country+irwstats_stattype;
			s_trackingServer = false;
			irwStatsInitialized = true;
		} else if (irwstats_stattype == "live") {
			s_account = "ikea"+s_country+"prod,ikeaallprod";
			s_trackingServer = true;
			irwStatsInitialized = true;
		}
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwstatOmnitureAddCharset(charset) {
	var __funcName__ = "irwstatOmnitureAddCharset";

	try {
		//s.charSet = charset.toUpperCase();
		s.charSet = "Auto";
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwstatOmnitureAddEvent(eventName) {
	var __funcName__ = "irwstatOmnitureAddEvent";

	try {
		if ((typeof(s.events) == "undefined") || (s.events.length == 0)) {
			s.events = eventName;
		} else {
			if (s.events.indexOf(eventName) < 0) {
				s.events += ","+eventName;
			}
		}
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwstatOmnitureRemoveEvent(eventName) {
	var __funcName__ = "irwstatOmnitureRemoveEvent";

	try {
		if ((typeof(s.events) == "undefined") || (s.events.length == 0)) {
			return;
		} else {
			if (s.events.indexOf(eventName) >= 0) {
				if (s.events.indexOf(eventName+",") >= 0) {
					eventName += ",";
				}
				var temp = s.events.substr(0, s.events.indexOf(eventName));
				temp += s.events.substr(s.events.indexOf(eventName)+eventName.length);
				if (temp.substr(temp.length - 1, 1) == ",") {
					temp = temp.substr(0, temp.length - 1);
				}
				s.events = temp;
			}
		}
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwStatTrackRia() {
	var __funcName__ = "irwStatTrackRia";
	
	try {
		s.products = "";
		s.events = "";
		var riaAction = "ria action";
	
		for (var irwTagCount = 0; irwTagCount < arguments.length; irwTagCount++) {
			if (arguments[irwTagCount].substr(0, 9) != "IRWStats.") {
				continue;
			}
			
			if (arguments[irwTagCount].toLowerCase() == "irwstats.riarequestname") {
				riaAction = arguments[irwTagCount+1];
			} else {
				irwstatAddTrackVar(arguments[irwTagCount]);
				irwstatAddMetaTag(arguments[irwTagCount], arguments[irwTagCount+1]);
			}
			irwTagCount++;
		}
		
		irwLocalFlags = new Array();
		irwStatLocalVars = new Array();
		irwstatAddLocalFlag("addAllPropsAndEvents");
		
		if (irwStatsInitialized == false) {
			irwstatPrepareConfig();
		}
		
		irwstatSendLink("o", riaAction);
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwStatPopupAnna() {
	var __funcName__ = "irwStatPopupAnna";
	
	try {
		omnitureRemoveVariable("prop", "eVar", "event", "pageName", "products");
		s.pageName = s.prop1 = s.prop2 = s.prop5 = s.eVar31 = "ask anna";
		irwstatOmnitureAddEvent("event16");
		
		// Clear flags and local vars
		irwLocalFlags = new Array();
		irwStatLocalVars = new Array();
		
		irwstatAddLocalFlag("pageName_SET");
		irwstatAddLocalFlag("pageNameLocal_SET");
		irwstatAddLocalFlag("categories_SET");
		
		omnitureExecute();
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwStatFormError(formName, formField, formErrorText) {
	var __funcName__ = "irwStatFormError";
	
	try {
		irwstatAddMetaTag("IRWStats.formErrorName", formName);
		irwstatAddMetaTag("IRWStats.formErrorFields", formField);
		irwstatAddMetaTag("IRWStats.formErrorTexts", formErrorText);
		
		s.linkTrackVars = "prop27";
		s.linkTrackEvents = "";
		
		irwstatSendLink("o", "form error");
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwStatCompareProducts(productArray) {
	var __funcName__ = "irwStatCompareProducts";
	
	try {
		s.eVar16 = "";
		
		if ((typeof(productArray) == "object") && (productArray.length > 0)) {
			productArray.sort();
			for (var prodCount = 0; prodCount < productArray.length; prodCount++) {
				s.eVar16 += productArray[prodCount]+">";
			}
		}
	
		s.eVar16 = s.eVar16.substr(0, s.eVar16.length-1);
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwStatShoppingList() {
	var __funcName__ = "irwStatShoppingList";
	
	try {
		if (arguments.length < 1) {
			return;
		}
		
		var listAction = arguments[0].toLowerCase();
		var actionName = "shopping list";
		var restoreProducts = "";
		
		if (listAction == "addbypartnumber") {
			if (arguments.length < 2) {
				throw new Error("addbypartnumber expects productID as second parameter.");
			}
		
			irwstatOmnitureAddEvent("event17");
			irwstatOmnitureAddProduct(arguments[1]);
			restoreProducts = s.products;
			s.products = "";
			irwstatOmnitureAddProduct(arguments[1]);
			
			s.linkTrackVars = "events,products";
			s.linkTrackEvents = "event17";
			
			actionName += " add by part number";
		} else if (listAction == "addfrompiporsc") {
			irwstatOmnitureAddEvent("event17");
			
			s.linkTrackVars = "events,products";
			s.linkTrackEvents = "event17";
			
			actionName += " add from pip or stockcheck";
		} else if (listAction == "poppopupopened") {
			s.prop24 = "shopping list popup opened";
			
			s.linkTrackVars = "prop24";
			s.linkTrackEvents = "";
			
			actionName += " popup opened";
		} else if (listAction == "emailshoppinglist") {
			s.eVar30 = "email";
			irwstatOmnitureAddEvent("event20");
			
			s.linkTrackVars = "eVar30,events,products";
			s.linkTrackEvents = "event20";
			
			actionName += " email";
		} else if (listAction == "removeproduct") {
			if (arguments.length < 2) {
				throw new Error("addbypartnumber expects productID as second parameter.");
			}
		
			s.eVar30 = "remove";
			irwstatOmnitureAddEvent("event18");
			irwstatOmnitureRemoveProduct(arguments[1]);
			restoreProducts = s.products;
			s.products = "";
			irwstatOmnitureAddProduct(arguments[1]);
			
			s.linkTrackVars = "evar30,events,products";
			s.linkTrackEvents = "event18";
		
			actionName += " remove";
		} else if (listAction == "printshoppinglist") {
			s.eVar30 = "print";
			irwstatOmnitureAddEvent("event20");
			
			s.linkTrackVars = "eVar30,events,products";
			s.linkTrackEvents = "event20";
		
			actionName += " print";
		} else if (listAction == "saveshoppinglist") {
			if (irwstatCheckLocalFlag("memberType_SET")) {
				s.eVar30 = "save";
				irwstatOmnitureAddEvent("event20");
				
				s.linkTrackVars = "eVar30,events,products";
				s.linkTrackEvents = "event20";
			
				actionName += " save";
			} else {
				return;
			}
		}
		
		omnitureExecuteLink("o", actionName);
		
		if (restoreProducts.length > 0) {
			s.products = restoreProducts;
		}
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwStatProductChanged(productID) {
	var __funcName__ = "irwStatProductChanged";

	try {
		s.products = "";
		
		if (productID.substr(0,1) != ";") {
			s.products = ";";
		}
		
		s.products += productID;
		
		omnitureExecute();
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}


function irwstatOmnitureAddProduct(productID) {
	var __funcName__ = "irwstatOmnitureAddProduct";

	try {
		if (productID.length < 2) {
			return;
		}

		if ((typeof(s.products) != "undefined") && (s.products.length > 0)) {
			if (s.products.indexOf(productID) >= 0) {
				return;
			}
		
			s.products += ",";
		} else {
			s.products = "";
		}
		
		if (productID.substr(0,1) != ";") {
			s.products += ";";
		}
		
		s.products += productID;
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwstatOmnitureRemoveProduct(productID) {
	var __funcName__ = "irwstatOmnitureRemoveProduct";

	try {
		if (productID.length < 2) {
			return;
		}
		
		if ((typeof(s.products) == "undefined") || (s.products.length < 1)) {
			return;
		}
		
		if (productID.substr(0,1) == ";") {
			productID = productID.substr(1);
		}
		
		var productSearch;
		eval("productSearch = /^(.*)\,\;"+productID+"(.*)$/");
		
		if (!productSearch.test(s.products)) {
			eval("productSearch = /^\;" + productID + "[\,]?(.*)$/");
			if (!productSearch.test(s.products)) {
				return;
			}
			
			s.products = s.products.replace(productSearch, "$1");
		} else {
			s.products = s.products.replace(productSearch, "$1$2");
		}
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwStatScAdd(prodArray) {
	var __funcName__ = "irwStatScAdd";

	try {	
		if (prodArray.constructor.toString().indexOf("Array") == -1) {
			prodArray = [ prodArray ];
		}
		
		var products = "";
		for (prodCount = 0; prodCount < prodArray.length; prodCount++) {
			products += ";"+prodArray[prodCount]+",";
		}
		products = products.substr(0, products.length-1);
		
		irwstatSetTrailingTag("IRWStats.addToCart", "yes");
		irwstatSetTrailingTag("IRWStats.scAddProducts", products);
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwStatScRemove(prodArray) {
	var __funcName__ = "irwStatScRemove";

	try {
		if (prodArray.constructor.toString().indexOf("Array") == -1) {
			prodArray = [ prodArray ];
		}
		
		var products = "";
		for (prodCount = 0; prodCount < prodArray.length; prodCount++) {
			products += ";"+prodArray[prodCount]+",";
		}
		products = products.substr(0, products.length-1);
		
		irwstatSetTrailingTag("IRWStats.removeFromCart", "yes");
		irwstatSetTrailingTag("IRWStats.products", products);
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwStatLocalStoreViewed(storeNo) {
	var __funcName__ = "irwStatLocalStoreViewed";

	try {
		s.events = "event11";
		s.eVar17 = storeNo.toLowerCase();
		
		s.linkTrackVars = 'events';
		s.linkTrackEvents = 'event11';
		
		omnitureExecuteLink("o", "local_store_viewed");
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwStatFamilyNewsletterSignup() {
	var __funcName__ = "irwStatFamilyNewsletterSignup";

	try {
		s.events = "event9";
		
		s.linkTrackVars = 'events';
		s.linkTrackEvents = 'event9';
		
		omnitureExecuteLink("o", "newsletter_signup");
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwstatSend() {
	var __funcName__ = "irwstatSend";

	try {
		irwstatReadTrailingTag();
	
		var irwTags = irwstatReadMetaTags();
		if ((irwTags == null) || (irwTags.length < 1)) {
			return;
		}
		irwstatPrepareVendor(irwTags);
		
		omnitureExecute();
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwstatSendLink() {
	var __funcName__ = "irwstatSendLink";

	try {
		var irwTags = irwstatReadMetaTags();
		if ((irwTags == null) || (irwTags.length < 1)) {
			return;
		}
		irwstatPrepareVendor(irwTags);
		
		if (irwstatCheckLocalFlag("riaPageView_SET")) {
			omnitureExecute();
			return;
		}
		
		irwstatArgs = "";
		for (var irwstat_arg = 0; irwstat_arg < arguments.length; irwstat_arg++) {
			irwstatArgs += "'"+arguments[irwstat_arg]+"', ";
		}
		
		if (irwstatArgs.length > 0) {
			irwstatArgs = irwstatArgs.substr(0, (irwstatArgs.length-2));
		}
		eval("omnitureExecuteLink("+irwstatArgs+");");
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function irwstatHandleError(err, func) {
	if (irwstats_stattype == "dev") {
		if (err.indexOf("bypass_error_functionality") >= 0) {
			return;
		}
		
		errMsg = func+": "+err+"\n";
		if (irwStatsDebug) {
			alert(errMsg);
		}
		throw new Error(errMsg);
	}
}

// ## FIX ## -- Needs documentation
function omnitureRemoveVariable() {
	var __funcName__ = "omnitureRemoveVariable";
	
	try {
		if (arguments.length < 1) {
			return;
		}
	
		var keys = [];
	    for (var property in s) {
			keys.push(property);
		}
		
		for (var arg = 0; arg < arguments.length; arg++) {	
			for (var prop = 0; prop < keys.length; prop++) {
				if (keys[prop].indexOf(arguments[arg]) == 0) {
					eval("s."+keys[prop]+" = '';");
				}
			}
		}
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function omniturePushCategory(newCat) {
	var __funcName__ = "omniturePushCategory";

	try {
		// NB! If s.prop22 is set it will get pushed out. (Shouldn't be the case though).
		if (typeof(s.prop21) != "undefined") s.prop22 = s.prop21;
		if (typeof(s.prop4) != "undefined") s.prop21 = s.prop4;
		if (typeof(s.prop3) != "undefined") s.prop4 = s.prop3;
		if (typeof(s.prop2) != "undefined") s.prop3 = s.prop2;
		s.prop2 = newCat;
		
		if (typeof(irwStatLocalVars["systemLocal"]) != "undefined") irwStatLocalVars["systemChapterLocal"] = irwStatLocalVars["systemLocal"];
		if (typeof(irwStatLocalVars["chapterLocal"]) != "undefined") irwStatLocalVars["systemLocal"] = irwStatLocalVars["chapterLocal"];
		if (typeof(irwStatLocalVars["subcategoryLocal"]) != "undefined") irwStatLocalVars["chapterLocal"] = irwStatLocalVars["subcategoryLocal"];
		if (typeof(irwStatLocalVars["categoryLocal"]) != "undefined") irwStatLocalVars["subcategoryLocal"] = irwStatLocalVars["categoryLocal"];
		irwStatLocalVars["categoryLocal"] = newCat;
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function omnitureLastChanges() {
	var __funcName__ = "omnitureLastChanges";

	try {
		if (irwStatLocalVars["internalPageType"] != "static") {
			omnitureUpdatePageName();
		}
		omnitureUpdateCategories();
		omnitureUpdateCustomTags();
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function omnitureUpdateCategories() {
	var __funcName__ = "omnitureUpdateCategories";

	try {
		if (irwstatCheckLocalFlag("categories_SET")) {
			return;
		}
		
		if (typeof(s.prop3) != "undefined") {
			// First check that we have no > (delimiter) in the category properties
			if ((s.prop3.indexOf(">") > 0)
				|| ((typeof(s.prop4) != "undefined") && (s.prop4.indexOf(">") > 0))
				|| ((typeof(s.prop21) != "undefined") && (s.prop21.indexOf(">") > 0))
				|| ((typeof(s.prop22) != "undefined") && (s.prop22.indexOf(">") > 0))) {
				irwstatAddLocalFlag("categories_SET");
				return;
			}
		}
		
		// If we're on the PIP-page, the "department" (ie. IKEA category) should be prodview and the
		// category should be "pushed" down to subcat etc.
		if (irwstatCheckLocalFlag("prodView")) {
			if (irwStatLocalVars["internalPageType"] == "pip") {
				omniturePushCategory("prodview");
			}
		}
		
		if ((typeof(s.prop2) != "undefined") && (typeof(s.prop3) != "undefined")) s.prop3 = s.prop2+">"+s.prop3;
		if ((typeof(s.prop3) != "undefined") && (typeof(s.prop4) != "undefined")) s.prop4 = s.prop3+">"+s.prop4;
		if ((typeof(s.prop4) != "undefined") && (typeof(s.prop21) != "undefined")) {
			s.prop21 = s.prop4+">"+s.prop21;
		} else if ((typeof(s.prop3) != "undefined") && (typeof(s.prop21) != "undefined")) {
			s.prop21 = s.prop3+">"+s.prop21;
		}
		if ((typeof(s.prop21) != "undefined") && (typeof(s.prop22) != "undefined")) s.prop22 = s.prop21+">"+s.prop22;
		
		irwstatAddLocalFlag("categories_SET");
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function omnitureUpdatePageName() {
	var __funcName__ = "omnitureUpdatePageName";

	try {
		if (! irwstatCheckLocalFlag("pageName_SET")) {
			if (typeof(s.prop2) != "undefined") s.pageName = s.prop2;
			if ((typeof(s.prop3) != "undefined") && (s.prop3.length > 0)) s.pageName += ">"+s.prop3;
			if ((typeof(s.prop4) != "undefined") && (s.prop4.length > 0)) s.pageName += ">"+s.prop4;
			if ((typeof(s.prop21) != "undefined") && (s.prop21.length > 0)) s.pageName += ">"+s.prop21;
			if ((typeof(s.prop22) != "undefined") && (s.prop22.length > 0)) s.pageName += ">"+s.prop22;
			if ((typeof(irwStatLocalVars["friendlyPageName"]) != "undefined") && (irwStatLocalVars["friendlyPageName"].length > 0)) {
				s.prop1 += ">"+irwStatLocalVars["friendlyPageName"];
			}
			
			irwstatAddLocalFlag("pageName_SET");
		}
		
		if (! irwstatCheckLocalFlag("pageNameLocal_SET")) {
			if ((irwStatLocalVars["categoryLocal"]) && (irwStatLocalVars["categoryLocal"].length > 0)) {
				s.prop1 = irwStatLocalVars["categoryLocal"];
			} else {
				return;
			}
			if ((typeof(irwStatLocalVars["subcategoryLocal"]) != "undefined") && (irwStatLocalVars["subcategoryLocal"].length > 0)) {
				s.prop1 += ">"+irwStatLocalVars["subcategoryLocal"];
			}
			if ((typeof(irwStatLocalVars["chapterLocal"]) != "undefined") && (irwStatLocalVars["chapterLocal"].length > 0)) {
				s.prop1 += ">"+irwStatLocalVars["chapterLocal"];
			}
			if ((typeof(irwStatLocalVars["systemLocal"]) != "undefined") && (irwStatLocalVars["systemLocal"].length > 0)) {
				s.prop1 += ">"+irwStatLocalVars["systemLocal"];
			}
			if ((typeof(irwStatLocalVars["systemChapterLocal"]) != "undefined") && (irwStatLocalVars["systemChapterLocal"].length > 0)) {
				s.prop1 += ">"+irwStatLocalVars["systemChapterLocal"];
			}		
			
			irwstatAddLocalFlag("pageNameLocal_SET");
		}
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function omnitureUpdateCustomTags() {
	var __funcName__ = "omnitureUpdateCustomTags";
	
	try {
		// Set s.prop25 (product flow) if we have prodView
		if (irwstatCheckLocalFlag("prodView")) {
			if (s.products.substr(0, 1) == ";") {
				s.prop25 = s.products.substr(1);
			} else {
				s.prop25 = s.products;
			}
		}
	
		if (irwstatCheckLocalFlag("merchandisingcategory_SET")) {
			s.eVar4 = s.pageName;
			irwstatPostProcessVariable("s.eVar4");
		}
		
		if ((irwstatCheckLocalFlag("front")) && (! irwstatCheckLocalFlag("front_SET"))) {
			irwstatAddLocalFlag("front_SET");
			s.pageName += ">front";
			s.prop1 += ">front";
		}
		if ((irwstatCheckLocalFlag("form")) && (! irwstatCheckLocalFlag("form_SET"))) {
			irwstatAddLocalFlag("form_SET");
			s.pageName += ">form";
			s.prop1 += ">form";
		}
		if ((irwstatCheckLocalFlag("prodView")) && (! irwstatCheckLocalFlag("prodView_SET"))) {
			irwstatAddLocalFlag("prodView_SET");
			s.pageName += ">prodview";
			s.prop1 += ">prodview";
		}
		
		if (irwstatCheckLocalFlag("riaAction_SET")) {
			if ((typeof(irwStatLocalVars["riaAsset"]) != "undefined") 
				&& (typeof(irwStatLocalVars["riaAssetType"]) != "undefined")
				&& (typeof(irwStatLocalVars["riaAction"]) != "undefined")
				&& (typeof(irwStatLocalVars["riaActionType"]) != "undefined")) {
				
				//IKEA00659368
				if (irwStatLocalVars["riaAssetType"] == "other") {
				irwStatLocalVars["riaAssetType"] = irwStatLocalVars["riaCategory"];
				}
				//ends IKEA00659368
								
				s.eVar23 = s.prop18 = irwStatLocalVars["riaAssetType"]+">"+irwStatLocalVars["riaAsset"];
				s.prop19 = irwStatLocalVars["riaAsset"]+">"+irwStatLocalVars["riaActionType"]+">"+irwStatLocalVars["riaAction"];
				s.prop20 = s.prop19;
				
				s.pageName += ">"+irwStatLocalVars["riaAsset"];
				s.prop1 += ">"+irwStatLocalVars["riaAsset"];
				
				irwstatPostProcessVariable("s.eVar23");
				irwstatPostProcessVariable("s.prop18");
				irwstatPostProcessVariable("s.prop19");
				irwstatPostProcessVariable("s.prop20");
			}
			if ((typeof(irwStatLocalVars["riaRoomSet"]) != "undefined") && (irwStatLocalVars["riaRoomSet"].length > 0)) {
				s.prop23 = irwStatLocalVars["riaRoomSet"];
				irwstatPostProcessVariable("s.prop23");
			}
		}
		
		if (irwstatCheckLocalFlag("memberSignupStart_SET")) {
			if ((typeof(irwStatLocalVars["internalPageType"]) != "undefined") && (irwStatLocalVars["internalPageType"].substr(0, 4) == "ecom")) {
				s.eVar18 = "ecom sign up";
				irwstatPostProcessVariable("s.eVar18");
			} else {
				irwstatSetTrailingTag("IRWStats.memberSignupStarted", "yes");
			}
		}
		// Change in fammily_code --KPIA
		if (irwstatCheckLocalFlag("memberSignupStarted_SET")) {
		   if ((typeof(irwStatLocalVars["internalPageType"]) != "undefined") && (irwStatLocalVars["internalPageType"] == "user confirmation") && 
		      (typeof(s.prop5) != "undefined") && (s.prop5 == "family")) {
			 s.eVar18 = "family sign up";
			 irwstatOmnitureAddEvent("event8");
			 irwstatPostProcessVariable("s.eVar18");
			 irwstatPostProcessVariable("event8");
		   } else if ((typeof(irwStatLocalVars["internalPageType"]) != "undefined") && (irwStatLocalVars["internalPageType"] == "user confirmation")) {
			 s.eVar18 = "sign up";
			 irwstatOmnitureAddEvent("event8");
			 irwstatPostProcessVariable("s.eVar18");
			 irwstatPostProcessVariable("event8");
		   } else if ((typeof(s.prop5) != "undefined") && (s.prop5 == "shopping list")) {
			 s.eVar18 = "sign up";
			 irwstatOmnitureAddEvent("event8");
			 irwstatPostProcessVariable("s.eVar18");
			 irwstatPostProcessVariable("event8");
		   } else if ((typeof(irwStatLocalVars["internalPageType"]) != "undefined") && (irwStatLocalVars["internalPageType"] == "ecom-step2")) {
			 s.eVar18 = "ecom sign up";
			 irwstatOmnitureAddEvent("event8");
			 irwstatPostProcessVariable("s.eVar18");
			 irwstatPostProcessVariable("event8");
		   }
		}
		// Change in fammily_code --KPIA ends
		
		if ((irwstatCheckLocalFlag("memberLoggedIn_SET")) && (typeof(s.prop5) != "undefined") && (s.prop5 == "shopping list")) {
			// Shopping list must've been saved - fire off events accordingly
			irwstatOmnitureAddEvent("event20");
			s.eVar30 = "save";
		}
			
		// Check for Form-errors
		if ((irwstatCheckLocalFlag("formErrorFields_SET")) && (irwstatCheckLocalFlag("formErrorName_SET"))) {
			var errorField = irwStatLocalVars["formErrorFields"];
			if (errorField.indexOf(";") > 0) {
				errorField = errorField.substr(0, errorField.indexOf(";"));
			}
			
			var errorText = "unknown";
			if (irwstatCheckLocalFlag("formErrorTexts_SET")) {
				errorText = irwStatLocalVars["formErrorTexts"];
				if (errorText.indexOf(";") > 0) {
					errorText = errorText.substr(0, errorText.indexOf(";"));
				}
			}
			
			var errorForm = irwStatLocalVars["formErrorName"];
			
			s.prop27 = errorForm+">"+errorField+">"+errorText;
			irwstatPostProcessVariable("s.prop27");
		}
		
		// Set page functionality property if we're on Request Password Page
		if ((typeof(irwStatLocalVars["internalPageType"]) != "undefined") && (irwStatLocalVars["internalPageType"] == "request-password")) {
			s.prop24 = "request a new password";
			irwstatPostProcessVariable("s.prop24");
		}
		
		// If we're in the ecom-flow, unset s.prop3 if it is set
		// Also, set family discount price if it is there
		if ((typeof(irwStatLocalVars["internalPageType"]) != "undefined") && (irwStatLocalVars["internalPageType"].substr(0, 4) == "ecom")) {
			s.prop3 = "";
		} else if ((typeof(irwStatLocalVars["internalPageType"]) != "undefined") && (irwStatLocalVars["internalPageType"].substr(0, 10) == "stockcheck")) {
			s.prop3 = "";
		} else if ((typeof(irwStatLocalVars["internalPageType"]) != "undefined") && (irwStatLocalVars["internalPageType"] == "search")) {
			s.prop3 = "";
		}
		
		// Set s.eVar28 to +1 if we're on the stockcheck result page
		if ((typeof(irwStatLocalVars["internalPageType"]) != "undefined") && (irwStatLocalVars["internalPageType"] == "stockcheck-result")) {
			s.eVar28 = "+1";
			irwstatPostProcessVariable("s.eVar28");
		}
		
		if ((typeof(irwStatLocalVars["internalPageType"]) != "undefined") && (irwStatLocalVars["internalPageType"].substr(0, 14) == "range-category")) {
			if ((irwstatCheckLocalFlag("hasSystemChapter")) || (irwstatCheckLocalFlag("hasSystem")) || (irwStatLocalVars["internalPageType"] == "range-category-series")) {
				s.prop5 = "series, collections and systems";
			} else if (irwstatCheckLocalFlag("hasChapter")) {
				s.prop5 = "subcategory";
			} else if (irwstatCheckLocalFlag("hasSubCategory")) {
				s.prop5 = "category";
			} else if (irwstatCheckLocalFlag("hasCategory")) {
				s.prop5 = "department";
			}
		
			if (s.prop2 != "series") {
				if ((irwstatCheckLocalFlag("hasCategory")) && (! irwstatCheckLocalFlag("hasSubCategory"))) {
					s.hier1 = "topnav";
					if (typeof(s.prop2) != "undefined") s.hier1 += ">"+s.prop2;
					irwstatPostProcessVariable("s.hier1");
				} else if ((irwstatCheckLocalFlag("hasSubCategory")) || (irwstatCheckLocalFlag("hasChapter"))) {
					s.hier1 = "leftnav";
					if (typeof(s.prop4) != "undefined") s.hier1 += ">"+s.prop4;
					else if (typeof(s.prop3) != "undefined") s.hier1 += ">"+s.prop3;
					irwstatPostProcessVariable("s.hier1");
				}
			}
		}
		
		if (irwstatCheckLocalFlag("memberType_SET")) {
			if ((typeof(s.prop24) != "undefined") && (s.prop24 == "logout")) {
				s.prop28 = "";
			} else {
				irwstatSetTrailingTag("IRWStats.memberType", s.prop28);
			}
		} else {
			s.prop28 = "";
		}
		
		if (irwstatCheckLocalFlag("addByPartNumberError_SET")) {
			irwstatOmnitureRemoveEvent("scAdd");
			s.prop24 = "";
		} else if (irwstatCheckLocalFlag("productAlreadyInCart_SET")) {
			irwstatOmnitureRemoveEvent("scAdd");
		} else if (irwstatCheckLocalFlag("scAddProducts_SET")) {
			s.products = irwStatLocalVars["scAddProducts"];
		}
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function omnitureAddLinkTrack(varName) {
	var __funcName__ = "omnitureExecute";

	try {
		if (! irwstatCheckLocalFlag("addAllPropsAndEvents")) {
			return;
		}
		
		varName = varName.replace(/s./g, "");
				
		if (((varName.substr(0, 5) == "event") && (varName.substr(0, 6) != "events")) || (varName.substr(0, 8) == "prodView")) {
			if (s.linkTrackEvents.indexOf(varName) >= 0) {
				return;
			}
			
			if (s.linkTrackEvents == "None") {
				s.linkTrackEvents = "";
			}
			
			if (s.linkTrackEvents.length >= 0) {
				s.linkTrackEvents += ",";
			}
			s.linkTrackEvents += varName;
			
			omnitureAddLinkTrack("s.events");
		} else {
			if (s.linkTrackVars.indexOf(varName) >= 0) {
				return;
			}
			
			if (s.linkTrackVars == "None") {
				s.linkTrackVars = "";
			}
		
			if (s.linkTrackVars.length > 0) {
				s.linkTrackVars += ",";
			}
			s.linkTrackVars += varName;
		}
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function omnitureExecute() {
	var __funcName__ = "omnitureExecute";

	try {
		omnitureLastChanges();
		
		if (typeof(s.charSet) == "undefined") {
			s.charSet = "Auto";
		}
		
		// Bugfix (proxy problem)
		if (irwstats_stattype == "dev") {
			s.visitorNamespace = "irwdev";
		}
		
		if (! irwstatCheckLocalFlag("revertToLink_SET")) {
			var s_code = s.t();
			if (s_code) {
				document.write(s_code)
			}
		} else {
			var s_code = s.tl(this, "o", irwStatLocalVars["linkTrackName"]);
		
			if (s_code) {
				document.write(s_code)
			}
		}
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}

function omnitureExecuteLink() {
	var __funcName__ = "omnitureExecuteLink";

	try {
		omnitureLastChanges();
		
		if (typeof(s.charSet) == "undefined") {
			s.charSet = "Auto";
		}
		
		// Bugfix (proxy problem)
		if (irwstats_stattype == "dev") {
			s.visitorNamespace = "irwdev";
		}
		
		delete(s.pageName);
		var s_code = s.tl(this, arguments[0], arguments[1]);
		if (s_code) {
			document.write(s_code)
		}
	} catch(e) {
		irwstatHandleError(e, __funcName__);
	}
}