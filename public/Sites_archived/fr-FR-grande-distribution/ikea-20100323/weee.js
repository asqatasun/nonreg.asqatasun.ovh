function openPrfPopup(id) {
	var offsetLeft = -15;
	var offsetTop = 17;

	var x = document.getElementById(
		id);
	var v = document.getElementById(
		"prfinfo");
	
	if(!v || !x){
		return;
	}
	v.style.display = "block";
	
	var xpos = getPosX(x);
	var ypos = getPosY(x);
	
	v.style.position = "absolute";
	v.style.top = (ypos - v.offsetHeight + offsetTop) + "px";
	v.style.left = (xpos + offsetLeft) + "px";
}

function closePrfPopup() {
	var v = document.getElementById(
		"prfinfo");
	v.style.display = "none";
}

function getPosX(obj) {
	var curleft = 0;
	if(obj.offsetParent) {
		while(1) {
			curleft += obj.offsetLeft;
			if(!obj.offsetParent)
				break;
			obj = obj.offsetParent;
		}
	} else if(obj.x) {
		curleft += obj.x;
	}
	return curleft;
}

function getPosY(obj) {
	var curtop = 0;
	if(obj.offsetParent) {
		while(1) {
			curtop += obj.offsetTop;
			if(!obj.offsetParent)
				break;
			obj = obj.offsetParent;
		}
	} else if(obj.y) {
		curtop += obj.y;
	}
	return curtop;
}
