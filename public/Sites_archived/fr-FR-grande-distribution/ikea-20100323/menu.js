var w3c = false;
var ie = false;
var timerID = null; 
var timerOn = false; 

if (document.getElementById) {
	var w3c = true;
}
if (document.all) {
	var ie = true;
}

//show a hidden layer
function displayLayer(strName,displayType){
    try {
		resetTimer();
		document.getElementById(strName).style.display = displayType;
	} catch (e) {}
}

//hide a shown layer
function noneLayer(strName){
	try {
		document.getElementById(strName).style.display = 'none';
	} catch(e) {}
}

function resetTimer() {
    if (timerOn) {  
        clearTimeout(timerID);  
        timerID = null;  
        timerOn = false;  
    } 
}

function showDropMenu(strName,displayType) {
    displayLayer(strName,displayType);
}

function hideDropMenu(strName) {
    if (timerOn == false) {  
        timerID = setTimeout('noneLayer("'+strName+'")',1000);
        timerOn = true;  
    } 
}

//swap image
function swapImage(imgName,imgSrc) {
	document.getElementById(imgName).src=imgSrc;
	//document.images[imgName].src=imgSrc;
}

//change the value of the search form
function logInFormValue(which,theValue) {
	if (document.getElementById(which).value == theValue) {
		document.getElementById(which).value='';
	} else if (document.getElementById(which).value == '') {
		document.getElementById(which).value=theValue;
	}
}

//change the state of the arrows in the menu
function rollArrow(chosen, objectID) {
	if(chosen == "active") {
		document.getElementById(objectID).className="arrowActive";
	}
	else {
		document.getElementById(objectID).className="arrow";
	}
}

//set style to an element
function setStyle(objId, style, styleValue) {
	document.getElementById(objId).style[style] = styleValue;
}

//change the class of an element
function changeClass(objectID, toClass) {
	document.getElementById(objectID).className=toClass;
}

//submit form
function submitIt(theFormName,parameter1,value1) { 
	document.getElementById(parameter1).value = value1; 
	temp = "document." + theFormName + ".submit()" 
	eval (temp); 
} 

// Functions for the search form in the header
function search(frmName){
    frm = document.getElementById(frmName);
    if(frm.onsubmit()){
        frm.submit();
    }
}

function checkSearch(searchForm){
    while(searchForm.query.value.charAt(searchForm.query.value.length-1)==' ') searchForm.query.value=searchForm.query.value.substring(0,searchForm.query.value.length-1);
    if (searchForm.query.value != "") {
        return true;
    } else {
        return false;
    }
}

// 070205 Functions created for Range IV by Jake
// A function to find all specified objects on a page and then show/hide them by definining the 'visibility' parameter to 'show' or 'hide'
// Function needed for IE 6 and below to hide select objects when dropdown layers are to be displayed (otherwise the select obj is showing through the layer)

// Object detection of IE7
var ie7 = (document.documentElement && typeof document.documentElement.style.maxHeight!="undefined")? true:false;

function tagVisibility(tag, visibility) {
	try {
	    if(ie && !ie7){ // Only fire if IE and not (e.g. less than)  IE7
	        var numOfForms = document.forms.length;
	        
	        for(var f=0; f<numOfForms; f++){
	            theForm = document.forms[f];

	            for(var i=0; i<theForm.length; i++){
	                var theElement = theForm.elements[i];
	                if(theElement.tagName == tag){
	                    if(visibility == 'hide'){
	                        hideElement(theElement);
	                    }else if(visibility == 'show'){
	                        showElement(theElement);
	                    }
	                }
	                
	                
	            }
	        }
	    }
	} catch (e) {}
}

function showElement(obj){
	obj.style.visibility = "visible";
}

function hideElement(obj){
	obj.style.visibility = "hidden";
}

/*  
 * DRUO 090312 - Search3
 */
 
 /*
 Removed IKEA00648492
 
function searchFieldValueUpdate(theFieldName)
{
	var this_url_path = window.location.pathname;
	var this_path_array = this_url_path.split("/");
	if(this_path_array[this_path_array.length-2] == "search")
	{
		if(document.getElementById(theFieldName).value == null || document.getElementById(theFieldName).value == '')
		{
			if (IRWreadCookie("RECENT_SEARCH_0"))
			{
				recentSearch = new String(IRWreadCookie("RECENT_SEARCH_0")).strip();
				if (recentSearch != null)
				{
					decodedStr = Url.decode(recentSearch);
				}
				document.getElementById(theFieldName).value=decodedStr;
			}
			else
			{
				document.getElementById(theFieldName).value='';
			}
		}
	}
}
*/
