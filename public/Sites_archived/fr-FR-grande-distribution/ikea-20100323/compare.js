var cookieName = "irw_compare";
var cookieValueSeparator = "**";
var cbCount = 0;
var cbDisabled = true;
var cbArray = new Array();
var cbRecArray = new Array();
var buttonArray = new Array();

/* This function is needed in order for firefox to force a reload of the page when presseing backbutton */
window.onunload = function() {};

/**
* Adds a product to the compare cookie
*/
function addProductToCompare(productNumber){
	var currentValue = getCookie(cookieName);
	var newVal = null;
	if(currentValue == null || currentValue == ""){
		newVal = productNumber;
	} else if (currentValue.indexOf(productNumber) == -1) {
		newVal = currentValue + cookieValueSeparator + productNumber;
	} else {
		return;
	}

	cbCount = cbCount + 1;
	document.cookie=cookieName+"="+escape(newVal)+";path=/";
}

/**
*Removes a product form the comparison
*cookie
*/
function removeProductToCompare(productNumber){
	var values = getCookie(cookieName);
	if(values == null || values == "" || values.indexOf(productNumber) == -1){
		return;
	}
	var newVal = values.replace(cookieValueSeparator + productNumber,'');
	//If the product is the first in the list
	newVal = newVal.replace(productNumber + cookieValueSeparator,'');
	//if the product is the only product	
	newVal = newVal.replace(productNumber,'');

	cbCount = cbCount - 1;
	document.cookie=cookieName+"="+escape(newVal)+";path=/";
}

function setComparisonCheckboxStatus(){
		
	try {
		if (navigator.appName.toLowerCase().indexOf("microsoft") != -1) {
			setTimeout("initCompareCheckboxStatus()", 0);
		} else {
			initCompareCheckboxStatus();
		}
	} catch (error) {
		initCompareCheckboxStatus();
	}
	
}

function initCompareCheckboxStatus() {
	//Unset all checkboxes in the products container and set an observer on each of them
	var prodContainer = $('productsContainer');
	if(prodContainer == null) return;
	var objects = prodContainer.select('div.cartContainer');
	objects.each(function(item) {
		//Get the checkbox
		var cb = item.down('input');
		cbArray.push(cb);
		//Show the compare checkbox and text
		cb.up(0).style.display = 'block';
		//Uncheck the checkbox
		cb.checked = false;
		cb.disabled = true;
		//Add click observer to the checkbox
		Event.observe(cb,'click',function(e) {
			updateCheckbox(e.target,false);
		});		
	});
	
	//check for product recommendation container
	var recCont = $('main').down('.rightContent').down('.prodRecsContainer');
	if (recCont != null) {
		//product recommendation container exists
		var i=0;
		var obj = null;
		while ((obj = recCont.down('input', i)) != null) {
			cbRecArray.push(obj);
			obj.up(0).style.display = 'block';
			obj.checked = false;
			obj.disabled = true;	
			Event.observe(obj, 'click', function(e) {
				updateCheckbox(e.target,true);
			});
			i = i + 1;
		}
	}	
		
	//Go through the coockie array and check the right checkboxes
	var values = getCookie(cookieName);
	if(values != null && values != ""){
		var products = values.split(cookieValueSeparator);
		var len = products.length;
		for(var i = 0; i < len;i++) {
			var cb = $('compare_'+products[i]);
			if (cb != null) {
				cb.checked = true;
				cb.disabled = false;
			}
			var cbRec = $('prodrec_compare_'+products[i]);
			if (cbRec != null) {
				cbRec.checked = true;
				cbRec.disabled = false;
			}
			cbCount = cbCount + 1;
		}
	}
	//Store the 'Show comparison' buttons in an array
	buttonArray.push($('main').down('.rightContent').down('.paginationContainer',0).down('.paginationRight').down('a'));
	buttonArray.push($('main').down('.rightContent').down('.paginationContainer',1).down('.paginationRight').down('a'));
	//If any cookies were found, try to enable the compare button
	updateComparison();
}

function updateCheckbox(cb,isProdRec) {
	var productNumber = cb.id.substring(cb.id.lastIndexOf('_')+1);
	if (cb.checked) {
		addProductToCompare(productNumber);
	} else {
		removeProductToCompare(productNumber);
	}
	var item = null;
	if (isProdRec) {
		item = $('compare_' + productNumber);
	}	else {
		item = $('prodrec_compare_' + productNumber);
	}
	if (item != null) {
		item.checked = cb.checked;
	}
	updateComparison();
}

function updateComparison() {
	try {
		buttonArray.each(function(item) {
			//Get the link in the pagination container.
			var b = (cbCount >= 2);
			//if the b (show) = false and the link is enabled, disable it.
			if (!b && !item.hasClassName('disabledButton')) {
				item.addClassName('disabledButton');
			} 
			//if the b (show) = true and the link is disbaled, enable it.
			else if (b && item.hasClassName('disabledButton')) {
				item.removeClassName('disabledButton');
			}
		});
		//allow maximum of 4 checkboxes
		if (cbCount >= 4 && !cbDisabled) {
			cbArray.each(function(item) {
				if (!item.checked) {
					item.disabled = true;
				}
				cbDisabled = true;
			});			
			cbRecArray.each(function(item) {
				if (!item.checked) {
					item.disabled = true;
				}
				cbDisabled = true;
			});			
		} else if (cbCount < 4 && cbDisabled) {
			cbArray.each(function(item) {
				item.disabled = false;
			});			
			cbRecArray.each(function(item) {
				item.disabled = false;
			});			
			cbDisabled = false;
		}		
	} catch (err) {}
}