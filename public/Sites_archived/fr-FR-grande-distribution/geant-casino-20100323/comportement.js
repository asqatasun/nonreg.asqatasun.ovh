// SQLI Agency - 02/06/2009 ------------------------------------------- */
jQuery().ready(function() {

    // lien Newsletter ------------------
    jQuery('#newsletterlink').bind("mouseenter", function() {
        jQuery("#newsletter").show();
        jQuery("#champ_newsletter").focus();
    });
    jQuery('#newsletterlink').bind("mouseleave", function() {
        jQuery("#newsletter").hide();
    });

    // lien Magasin ------------------
    jQuery('#magasinlink').bind("mouseenter", function() {
        jQuery("#magasin").show();
        jQuery("#magasin_cp").focus();
    });
    jQuery('#magasinlink').bind("mouseleave", function() {
        jQuery("#magasin").hide();
    });

    jQuery('#magasinpluglink').bind("mouseenter", function() {
        jQuery("#magasinplug").show();
    });
    jQuery('#magasinpluglink').bind("mouseleave", function() {
        jQuery("#magasinplug").hide();
    });

    // lien Mon Compte ------------------
    jQuery('#moncomptelink').bind("mouseenter", function() {
        jQuery("#moncompte").show();
        //jQuery("#moncompte_email").focus();		
    });
    jQuery('#moncomptelink').bind("mouseleave", function() {
        jQuery("#moncompte").hide();
    });

    jQuery('#moncomptepluglink').bind("mouseenter", function() {
        jQuery("#moncompteplug").show();
    });
    jQuery('#moncomptepluglink').bind("mouseleave", function() {
        jQuery("#moncompteplug").hide();
    });

    // Infos Conso ------------------
    jQuery('#conso ul li a').click(function() {
        jQuery('#conso ul li a').removeClass('selected');
        jQuery(this).addClass('selected');

        jQuery('.conso_content').css('display', 'none');
        var cible = jQuery(this).parent().attr('id');
        jQuery("#" + cible + "_content").css('display', 'block');

        return false;
    });


});