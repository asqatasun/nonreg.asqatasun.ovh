//script pour mettre la page en mode impression si la querystring contient printpage=1
var printpage = (new Querystring()).get('printpage');
if (printpage == '1') {
    window.print();
    document.write('<link rel="stylesheet" type="text/css" href="/Style%20Library/printpage.css">');
}