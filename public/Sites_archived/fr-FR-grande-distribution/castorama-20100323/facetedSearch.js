
function SearchTab(){
	this.searchTabs = new Array("produitsTab", "ideasTab", "magasinTab");
	
	this.getSearchTabHeader = function (tabId) {
		return document.getElementById(tabId + "Header");
	};
	
	this.inactiveSearchTabHeader = function (tabId) {
	
	    if (document.getElementById(tabId + "Header") != null && document.getElementById(tabId + "Header") != undefined){
		document.getElementById(tabId + "Header").className = "";
		}
	};
	
	this.inactiveAllSearchTabHeaders = function () {
		for (var i = 0; i < this.searchTabs.length; i++) {
			this.inactiveSearchTabHeader(this.searchTabs[i]);
		}
	};
	
	this.activeSearchTabHeader = function (tabId) {
	
		if (document.getElementById(tabId + "Header") != null && document.getElementById(tabId + "Header") != undefined){
		
			document.getElementById(tabId + "Header").className = "active";
			
			
		}
		
	};
	
	this.activeSearchTabContent = function (tabId) {
		if (document.getElementById(tabId) != null && document.getElementById(tabId) != undefined){
			document.getElementById(tabId).className = "searchContent_active";
		}
		
	};
	
	this.inactiveSearchTabContent = function (tabId) {
	  if (document.getElementById(tabId) != null && document.getElementById(tabId) != undefined){
		document.getElementById(tabId).className = "searchContent";
	  }
	};
	
	this.inActiveAllSearchTabContents = function () {
		for (var i = 0; i < this.searchTabs.length; i++) {
			this.inactiveSearchTabContent(this.searchTabs[i]);
		}
	};
	
	this.switchToSearchTab = function (tabId) {
		this.inactiveAllSearchTabHeaders();
		
		this.inActiveAllSearchTabContents();
		this.activeSearchTabHeader(tabId);
		this.activeSearchTabContent(tabId);
	};
	
}
function switchSearchTab(currentSearchTabId) {
	var searchTab = new SearchTab();
	
	searchTab.switchToSearchTab(currentSearchTabId);
	
	return false;
}



function activeTab(index,size)
	{	
		for(i=0;i<size;i++)
		{
			dojo.byId("title_"+i).className="";
			dojo.byId("content_"+i).className="tab_content";
		}
		dojo.byId("title_"+index).className="active";
		dojo.byId("content_"+index).className="tab_content active";
	}
