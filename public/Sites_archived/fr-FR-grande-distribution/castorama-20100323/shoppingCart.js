var conditionsAccepted = false;

function increaseAmount(elementName, isOnRemainingStock) {
	var obj = document.getElementById("quantityValue"+elementName);
	var currentVal = parseInt(obj.value);
    if (currentVal != NaN) {
        obj.value = (currentVal + 1);
    }
    checkQuantity(elementName, isOnRemainingStock);
}

function decreaseAmount(elementName, isOnRemainingStock) {
    var obj = document.getElementById("quantityValue"+elementName);
	var currentVal = parseInt(obj.value);
    if (currentVal != NaN && currentVal > 1) {
       obj.value = (currentVal - 1);
       checkQuantity(elementName, isOnRemainingStock);
    }
}

function checkQuantity(elementName, isOnRemainingStock){
	//alert(isOnRemainingStock);
	var obj = $("#quantityValue"+elementName);
	var a = obj.val();
	if(isNaN(a)){
		obj.val(1);
		a = 1;
	}
	quantity=Number(a);
	if(a <= 0){
		obj.val(1);
	}
	var stockLevel = Number(document.getElementById("stockLevel"+elementName).value);
	if(stockLevel != -1 && isOnRemainingStock != 'undefined' && isOnRemainingStock) {
		var remainsInStock = $("span#remainsInStock"+elementName);
		if(stockLevel == 0){
			remainsInStock.text("0");
			$("#quantityMessage"+elementName).css("color","red");
			$("a#iconPlus"+elementName).hide();
			return false;
		} else if((quantity > stockLevel) || (quantity == stockLevel)){
			obj.val(stockLevel);
			remainsInStock.text("0");
			$("#quantityMessage"+elementName).css("color","red");
			$("a#iconPlus"+elementName).hide();
			return false;
		} else {
			remainsInStock.text(stockLevel - quantity);
			$("#quantityMessage"+elementName).css("color","black");
			$("a#iconPlus"+elementName).show();
		}
	} else {
		$("div#quantityMessage"+elementName).hide();
	}
}

var subVar = false;
function recalculateQuantity(elementName){
	subVar = true;	
	$("#"+elementName).click();	
}

function initFields(){
	var acceptCarteLAtout = document.getElementById("acceptCarteLAtoutCheckBox");
	var acceptSalesConditions = document.getElementById("acceptSalesConditionsCheckBox");
	
	if(acceptCarteLAtout) {
		if(acceptCarteLAtout.value == "true"){
			document.getElementById("summchk").checked = true;
		}
	}
	if(acceptSalesConditions) {
		if(document.getElementById("acceptSalesConditionsCheckBox").value == "true"){
			document.getElementById("summAgree").checked = true;
		}
	}	
	var anonymousCoupon = document.getElementById("anonymousProfileUserResource");
	if(anonymousCoupon){
		$("#identifiant").show();
	} else {
		var idn = document.getElementById("identifiant");
		if(idn){
			$("#identifiant").hide();
		}
	}
		
	var showLoginSection = document.getElementById("showLoginSection");
	if(showLoginSection) {
		$("#identifiant").show();
	}
	
//	if (typeof(sessionCouponCode) != 'undefined') {
//		if(sessionCouponCode != '') {
//			document.getElementById("couponClaimCode").value = sessionCouponCode;
//		} //else {
		//	if(!showLoginSection) {
		//		document.getElementById("ENREGISTRER").click();	
		//	}
		//}
//	}
	
}


function setCarteCheckBox(element) {
	document.getElementById("acceptCarteLAtoutCheckBox").value = element.checked;
}

function  setAgreeCheckBox(element) {
	document.getElementById("acceptSalesConditionsCheckBox").value = element.checked;
	$("#unacceptedConditions").hide();
}


function submitform() {
	if(document.getElementById("summAgree").checked){
		subVar = true;
		document.getElementById("moveToPurchaseInfoByRelIdButton").click();
	} else {
		showPopup("unacceptedConditions");
	}
}

function recalcEnterPres(e, buttonName) {
	if (e.keyCode==13) {	
		subVar = false;			
		$("#"+buttonName).click();				
	} 
}




