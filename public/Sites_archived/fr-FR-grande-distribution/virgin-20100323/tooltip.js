﻿/*
 * 	Easy Tooltip 1.0 - jQuery plugin
 *	written by Alen Grakalic	
 *	http://cssglobe.com/post/4380/easy-tooltip--jquery-plugin
 *
 *	Copyright (c) 2009 Alen Grakalic (http://cssglobe.com)
 *	Dual licensed under the MIT (MIT-LICENSE.txt)
 *	and GPL (GPL-LICENSE.txt) licenses.
 *
 *	Built for jQuery library
 *	http://jquery.com
 *
 */
 
(function($) {

	$.fn.toolsTooltip = function(options){
	  
		// default configuration properties
		var defaults = {	
			xOffset: 4,		
			yOffset: 36,
			tooltipId: "tooltip-holder",
			clickRemove: false,
			content: "",
			useElement: ""
		}; 
			
		var options = $.extend(defaults, options);  
		var content;
		
		this.each(function() {  				
			var title = $(this).attr("title");				
			$(this).hover(function(e){											 							   
				content = (options.content != "") ? options.content : title;
				content = (options.useElement != "") ? $("#" + options.useElement).html() : content;
				$(this).attr("title","");									  				
				if (content != "" && content != undefined){			
					$("body").prepend("<div id='"+ options.tooltipId +"'><div class='tooltip'><div class='tooltip-lft'></div><div class='tooltip-content'><span>"+ content +"</span></div><div class='tooltip-rt'></div></div></div>");	
					$("#" + options.tooltipId)
						.css("position","absolute")
						.css("top",(e.pageY - options.yOffset) + "px")
						.css("left",(e.pageX + options.xOffset) + "px")						
						.css("display","none")
						.show();
					};
				if ($.browser.msie){
					if ($(this).parent().hasClass('btn-fan')) {
						$("#" + options.tooltipId).css("width","240px");
					}
					if ($(this).parent().hasClass('btn-bookmark, btn-comment')) {
						$("#" + options.tooltipId).css("width","100px");
					}
					if ($(this).parent().hasClass('error-msg-alert')) {
						$("#" + options.tooltipId).css("width","160px");
					}
				}
			},
			function(){	
				$("#" + options.tooltipId).remove();
				$(this).attr("title",title);
			});	
			$(this).mousemove(function(e){
				$("#" + options.tooltipId)
					.css("top",(e.pageY - options.yOffset) + "px")
					.css("left",(e.pageX + options.xOffset) + "px")					
			});	
			if(options.clickRemove){
				$(this).mousedown(function(e){
					$("#" + options.tooltipId).remove();
					$(this).attr("title",title);
				});				
			}
		});
	  
	};
	
	$.fn.easyTooltip = function(options){
	  
		// default configuration properties
		var defaults = {	
			xOffset: -10,		
			yOffset: 34,
			tooltipId: "tooltip-holder",
			clickRemove: false,
			content: "",
			useElement: ""
		}; 
			
		var options = $.extend(defaults, options);  
		var content;
		
		this.each(function() {  				
			var title = $(this).attr("title");				
			$(this).hover(function(e){											 							   
				content = (options.content != "") ? options.content : title;
				content = (options.useElement != "") ? $("#" + options.useElement).html() : content;
				$(this).attr("title","");									  				
				if (content != "" && content != undefined){			
					$("body").prepend("<div id='"+ options.tooltipId +"'><div class='tooltip'><div class='tooltip-lft'></div><div class='tooltip-content'><span>"+ content +"</span></div><div class='tooltip-rt'></div></div></div>");	
					$("#" + options.tooltipId)
						.css("position","absolute")
						.css("top",(e.pageY - options.yOffset) + "px")
						.css("left",(e.pageX + options.xOffset) + "px")						
						.css("display","none")
						.show();
					};
					
				if ($.browser.msie){
					if ($(this).parent().hasClass('btn-fan')) {
						$("#" + options.tooltipId).css("width","240px");
					}
					if ($(this).parent().hasClass('btn-bookmark, btn-comment')) {
						$("#" + options.tooltipId).css("width","100px");
					}
					if ($(this).parent().hasClass('error-msg-alert')) {
						$("#" + options.tooltipId).css("width","200px");
					}					
					if ($(this).parent().attr('id') == 'userName-errorMessage') {
						$("#" + options.tooltipId).css("width","130px");
					}
					if ($(this).parent().attr('id') == 'password-errorMessage') {
						$("#" + options.tooltipId).css("width","140px");
					}
					if ($(this).parent().attr('id') == 'first-name-errorMessage') {
						$("#" + options.tooltipId).css("width","130px");
					}
					if ($(this).parent().attr('id') == 'last-name-errorMessage') {
						$("#" + options.tooltipId).css("width","130px");
					}
					if ($(this).parent().attr('id') == 'confirmPassword-errorMessage') {
						$("#" + options.tooltipId).css("width","180px");
					}
					if ($(this).parent().attr('id') == 'email-errorMessage') {
						$("#" + options.tooltipId).css("width","150px");
					}
					if ($(this).parent().attr('id') == 'postalCode-errorMessage') {
						$("#" + options.tooltipId).css("width","180px");
					}
					if ($(this).parent().attr('id') == 'captchaResponse-errorMessage') {
						$("#" + options.tooltipId).css("width","180px");
					}
				}
			},
			function(){	
				$("#" + options.tooltipId).remove();
				$(this).attr("title",title);
			});	
			$(this).mousemove(function(e){
				$("#" + options.tooltipId)
					.css("top",(e.pageY - options.yOffset) + "px")
					.css("left",(e.pageX + options.xOffset) + "px")					
			});	
			if(options.clickRemove){
				$(this).mousedown(function(e){
					$("#" + options.tooltipId).remove();
					$(this).attr("title",title);
				});				
			}
		});
	  
	};

	$.fn.idle = function(time) {
		var o = $(this); 
	      o.queue(function() { 
	         setTimeout(function() { 
	            o.dequeue(); 
	         }, time);
	      }); 
	}
	
	//setTimeout(easyTooltip() , 1000);

})(jQuery);

$(document).ready(function(){ $("#content a.tooltip").toolsTooltip(); });
$(document).ready(function(){ $("#globalmenu a.tooltip").easyTooltip(); });