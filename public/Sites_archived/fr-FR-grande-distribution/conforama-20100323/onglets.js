var loadDetails = (function () {
   $("a.defaultLoad").parent().addClass("ongletACtive");
   $("#layerDetails").load($("a.defaultLoad").attr("href"));
   $("a.load").click(function() { 
      $("#layerDetails").load(this.href);
      $("a.load").each(function(i){
         $(this).parent().removeClass("ongletACtive");
      });
      $(this).parent().addClass("ongletACtive");
      return false;
   }); 
});


var loadPopinDetails = (function () {
   $("a.popinDefaultLoad").parent().addClass("ongletACtive");
   $("#popinLayerDetails").load($("a.popinDefaultLoad").attr("href"));
   $("a.popinLoad").click(function() { 
      $("#popinLayerDetails").load(this.href);
      $("a.popinLoad").each(function(i){
         $(this).parent().removeClass("ongletACtive");
      });
      $(this).parent().addClass("ongletACtive");
      return false;
   }); 
});
//addEvent(window, 'load',loadDetails, false);