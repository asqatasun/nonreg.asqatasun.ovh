
/*
	 multiple loads on a same windows load event.
*/

function addLoadListener(fn){
	/* Code for Mozilla-Gecko w3c Standards */
	if(typeof window.addEventListener != 'undefined'){
		window.addEventListener('load', fn, false);
	}
	/* Code For compatibility with Opera */
	else if(typeof document.addEventListener != 'undefined'){
		document.addEventListener('load', fn, false);
	}
	/* Code for IE */
	else if(window.attachEvent('onload') != 'function'){
		window.attachEvent('onload', fn);
	}
	/* Code for IE 5 Mac */
	else{
		var oldFn = window.onload;
		if(typeof window.onload != 'function'){
			window.onload=fn;
		}
		else{
			window.onload=function(){
				oldFn();
				fn();
			};
		}
	}
}

/*
	execution of the function;
	addLoadListener(FirstFunction);
	addLoadListener(secondFunction);
	addLoadListener(ThirdFunction);
*/


//fonction de gestion des évènements
function addEvent(elm, evType, fn, useCapture){
	//Cross browser event handling for IE5+, NS6+ and Mozilla/Gecko
	//By Scott Andrew
	if(elm.addEventListener){
		elm.addEventListener(evType, fn, useCapture);
		return true;
	}else if(elm.attachEvent){
		var r=elm.attachEvent('on'+evType, fn);
		return r;
	}else{
		elm['on'+ evType]=fn;
	}
}


function cancelClick(e){
	if(window.event && window.event.returnValue){
		window.event.returnValue=false;
	}
	if(e && e.preventDefault){
		e.preventDefault();
	}
	return false;
}

function getElementsByClass(searchClass,node,tag) {
	var classElements = new Array();
	if ( node == null )
		node = document;
	if ( tag == null )
		tag = '*';
	var els = node.getElementsByTagName(tag);
	var elsLen = els.length;
	var pattern = new RegExp("(^|\\s)"+searchClass+"(\\s|$)");
	for (i = 0, j = 0; i < elsLen; i++) {
		if ( pattern.test(els[i].className) ) {
			classElements[j] = els[i];
			j++;
		}
	}
	return classElements;
}

function findTarget(e){
	/* part of the DOM EVENT */
	
	var target;
	
	if(window.event && window.event.srcElement)
		target=window.event.srcElement;
	else if(e && e.target)
		target=e.target;
	if(!target)
		return null;

	return target;
}

var subShop = false;
function ShopCartSubmit(){
	if(!subShop){
		// NTO : affichage de la popin Please Wait
		if(loggedIn && isFulfillmentCenterId){
			var pleaseWaitPopin = new PleaseWaitPopin();
			pleaseWaitPopin.show();
		}
		subShop = true;
		$("#shopCartSubmit").click(function(){return false;});
	}
}
var subFinalize = false;
function finalizeOrder(){
	if(!subFinalize){
		// NTO : affichage de la popin Please Wait
		var pleaseWaitPopin = new PleaseWaitPopin();
		pleaseWaitPopin.show();
		subFinalize = true;
		$("#finalizeOrderSubmit").click(function(){return false;});
	}
}

var subShopCust = false;
function LogonSubmit(){
	var choix = document.getElementById('standardLoggon');
	if(!subShopCust && choix == null){
		$("#LogonSubmit").click(function(){return false;});
		var pleaseWaitPopin = new PleaseWaitPopin();
		pleaseWaitPopin.show();
		subShopCust = true;
	}
}

var subShopAdd = false;
function ShippingAddressSubmit(name){
	if(!subShopAdd){
		var pleaseWaitPopin = new PleaseWaitPopin();
		pleaseWaitPopin.show();
		subShopAdd = true;
		$("#ShippingAddressSubmit").click(function(){return false;});
	}
}

/*var popTest = false;
function loadPop(){
	if(!popTest){
		var pleaseWaitPopinCustom = new PleaseWaitPopinCustom();
		pleaseWaitPopinCustom.show();
		popTest = false;
	}
}*/





