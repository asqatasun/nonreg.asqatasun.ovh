/*Plugin JQuery de validation CustomValidate de formulaire.
/* Cédric Magnin.
/*
/* v1.1 03/08/2007 : ajout de la gestion des messages d'erreur suivant les erreurs
/* v1.0 01/08/2007
/*
/* IMPORTANT	
/* Les Classes de gestion d'affichage commence par cVal
/* Evitez de les utiliser pour de l'affichage
/*
/* Appel global :
/*
/* 	$(id_du_bouton_submit).bind( "click", function(event) {
/* 		event.preventDefault();
/* 		checkCreaCompte($(id_du_formulaire));		
/*	 });
/*
/* Listing des options :
/*
/* 	function checkCreaCompte(form) {
/* 		errorClasses = [tableau_des_classes_affichant_les_erreurs];
/* 		okClasses = [tableau_des_classes_affichant_les_validations];
/* 		manageErrors = 'nom_de_la_classe_qui_displayNone';
/*
/* 		$(id_du_champ_a_checker).customValidate({ options });
/*		$...
/*
/* 		$(form).customValidate({ form:true });
/* 	}
/* 
/* Les options :
/* 	
/* 	FORM:true	 			applique l'extension $.submitForm() au FORMULAIRE (Objet JQuery)
/* 	METHOD:'function'	 	temporaire, passe des fonctions pour demo
/* 	REQUIRED:true	 		vérifie un champ obligatoire sur un INPUT TEXT ou  TEXTAREA
/* 	REQUIREDIF:'id'			vérifie un champ obligatoire sur un INPUT TEXT ou  TEXTAREA ssi le champ dont l'id est passé en argument est checké (checkbox ou radio, donc)
/* 	SELECT:'x'				vérifie un champ obligatoire sur un SELECT en vérifiant si la valeur du SELECT est différente de x
/* 	SELECTIF:'id|x'			vérifie un champ obligatoire sur un SELECT ssi id est checké (checkbox ou radio, donc) en vérifiant si la valeur du SELECT est différente de x. | est le caractère de séparation des données
/* 	CHECKED:true			vérifie un champ obligatoire sur un INPUT CHECKBOX ou RADIO
/* 	DIFF:'id'  				vérifie si la valeur du champ INPUT TEXT est différente de celle du champ INPUT TEXT dont l'id est passé en argument 
/* 	CARAC:'type'			vérifie des chaines de caractères selon le type : 'abc' pour les lettres et espace, '123' pour les chiffres, 'alphanum' pour les lettres et chiffres, 'num' pour les numéros de téléphone, espace, +, () et 'email' pour les emails
/* 	MIN:'x'				vérifie si le champ comprend au moins x caractères
/* 	MAX:'x'				vérifie si le champ comprend au plus x caractères
/* 	SAME:'id'				vérifie si la valeur du champ INPUT TEXT est la même de celle du champ INPUT TEXT dont l'id est passé en argument
/* 	ATLEAST:'x|id|id|...'		vérifie si au moins x champs passés en id sont vides, unchecked, ou unselected (à tester pour unchecked et unselected)
/* 
/* Les classes utilisées dans le HTML :
/* 
/* cValIconError = permet l'affichage d'un élément indépendamment du type d'erreurs du champ.
/* cVal+type_derreur = permet de customiser ses messages d'erreur
*/

// Stocke un tableau des champs faux, pour le submit
var globalErrors = new Array;
var getFirstError = false;

jQuery.fn.extend({
	checkable: function() { // à revoir
		if($(this).attr("type") != "radio" || $(this).attr("type") != "checkbox" || $(this.nodeName.toLowerCase()) != "select") return true;
	},
	showErrors: function( errors , settings ) { // peut gérer le type d'erreur
		
		// definit si le champ est obligatoire ou non
		if( settings.required || settings.requiredIf || settings.select || settings.selectIf || settings.atleast || settings.checked ) {
			//console.log($(this).attr('id')+" obligatoire");
			var mandatory = true;
		}
		
		// Gestion de l'affichage
		if(errors.length > 0){ //si il y a au moins 1 erreur
			
			
			/* DIGITAS - 08/2008 : Ajouts */
			if($("#compteErrorTxt").length){
				$("#compteErrorTxt").removeClass("displayNone");
			}
			/* DIGITAS - 08/2008 : Fin Ajouts */
			
			// on repère la première erreur rencontrée et on va à l'ancre correspondante
			if(!getFirstError){
				getFirstError = true;
				
				/* DIGITAS - 08/2008 : Ajouts */
				if($("#compteErrorTxt").length){
					var myLabel = $(this).parent().siblings("label:first");
					// si le label "frère" est bien celui de l'objet visé
					if (myLabel.attr("for") == $(this).attr('id')){
						myLabel.attr({ id: $(this).attr('id')+"Label"});
						if(myLabel.attr("id")== "factSelCivLabel"){
							 window.location.href = "#compteErrorTxt";
						}
						else window.location.href = "#"+$(this).attr('id')+"Label";
					}
					else window.location.href = "#"+$(this).attr('id');
				}
				
				else{
					var myLabel = $(this).siblings("label:first");
					// si le label "frère" est bien celui de l'objet visé
					if (myLabel.attr("for") == $(this).attr('id')){
						myLabel.attr({ id: $(this).attr('id')+"Label"});
						window.location.href = "#"+$(this).attr('id')+"Label";
					}
					else window.location.href = "#"+$(this).attr('id');
				}
				/* DIGITAS - 08/2008 : Fin Ajouts */
			}
		
			globalErrors.push($(this).attr('id')); // on insère l'id du champ dans le tableau d'erreurs global
			if( okClasses.length > 0 ) { for(var i=0; i < okClasses.length; i++) { $(this).siblings("."+okClasses[i]+"").addClass(manageErrors); } }
			// Littéralement, l'instruction ci-dessus : pour chaque classe définie dans la variable okClasses, on ajoute la classe définie dans la variable manageErrors à tous les noeuds frères du champ qui ont l'une des classes comprises dans okClasses.
			if( errorClasses.length > 0 ) { for(var i=0; i < okClasses.length; i++) { $(this).siblings("."+errorClasses[i]).addClass(manageErrors); $(this).siblings(".cValIconError ."+errorClasses[i]).removeClass(manageErrors); } } // Ici enlève tous les messages d'erreur pour ensuite afficher les messages non-textes ou communs
			for(var i=0; i<errors.length; i++) {
				if($(this).siblings(".cVal"+errors[i])) {
					$(this).siblings(".cVal"+errors[i]).removeClass(manageErrors);
				}
			}
			$(this).addClass('cValChanged'); // On ajoute au champ la classe cValChanged, qui indique son changement de statut
		} else {
			// On initie un RegExp qui va checker si la value est différente de null, -, 0, ou espace
			var reg = new RegExp('^[-0-0 ]?$','i');
			// On gère tous les cas :
         if($(this).attr('class') != undefined) {
   			if(mandatory && $(this).attr('class').indexOf('cValChanged') != -1) { // champ obligatoire qui a changé
   				if( errorClasses.length > 0 ) { for(var i=0; i < errorClasses.length; i++) { $(this).siblings("."+errorClasses[i]+"").addClass(manageErrors); } }
   				if( okClasses.length > 0 ) { for(var i=0; i < okClasses.length; i++) { $(this).siblings("."+okClasses[i]+"").removeClass(manageErrors); } }
   			}
   			if(mandatory && $(this).attr('class').indexOf('cValChanged') != -1 && reg.test($(this).val()) ) { // champ obligatoire qui a changé mais qui est vide ou est une valeur de base d'un select
   				if( errorClasses.length > 0 ) { for(var i=0; i < errorClasses.length; i++) { $(this).siblings("."+errorClasses[i]+"").addClass(manageErrors); } }
   				if( okClasses.length > 0 ) { for(var i=0; i < okClasses.length; i++) { $(this).siblings("."+okClasses[i]+"").addClass(manageErrors); } }
   			}
   			if(!mandatory && $(this).attr('class').indexOf('cValChanged') != -1) { // champ non obligatoire qui a changé
   				if( errorClasses.length > 0 ) { for(var i=0; i < errorClasses.length; i++) { $(this).siblings("."+errorClasses[i]+"").addClass(manageErrors); } }
   				if( okClasses.length > 0 ) { for(var i=0; i < okClasses.length; i++) { $(this).siblings("."+okClasses[i]+"").removeClass(manageErrors); } }
   			}
   			if(!mandatory && $(this).attr('class').indexOf('cValChanged') != -1 && reg.test($(this).val()) ) { // champ non obligatoire qui a changé mais qui est vide ou est une valeur de base d'un select
   				if( errorClasses.length > 0 ) { for(var i=0; i < errorClasses.length; i++) { $(this).siblings("."+errorClasses[i]+"").addClass(manageErrors); } }
   				if( okClasses.length > 0 ) { for(var i=0; i < okClasses.length; i++) { $(this).siblings("."+okClasses[i]+"").addClass(manageErrors); } }
   			}
   			if(!mandatory && $(this).attr('class').indexOf('cValChanged') == -1) { // champ non obligatoire qui n'a pas changé
   				if( errorClasses.length > 0 ) { for(var i=0; i < errorClasses.length; i++) { $(this).siblings("."+errorClasses[i]+"").addClass(manageErrors); } }
   				if( okClasses.length > 0 ) { for(var i=0; i < okClasses.length; i++) { $(this).siblings("."+okClasses[i]+"").addClass(manageErrors); } }
   			}
        }
		}
	},
	submitForm: function(settings) {
	  getFirstError = false; // reset du test de la premiere erreur rencontrée
      if( globalErrors.length == 0 ) { // si le tableau global des erreurs est vide, on submit			
		if(settings.method) {
				//$("#demoForm").css("display","none");
				//$("#demoForm2").css("display","block");
            submitFormInPopin();
			} else {
				$(this)[0].submit();
			}
		}
	}
});


(function() {
	
    jQuery.fn.customValidate = function(settings) {
		
		var validElement = this; //THE jquery object
		
		// définition des options disponibles dans le plugin customValidate, on les met toutes à false pour que le check ne se fasse que sur les options déclarées par champ
        settings = jQuery.extend({
			form: false,
			method: false,
			required: false,
			requiredIf: false,
			select: false,
			selectIf: false,
			checked: false,
			diff: false,
			carac: false,
			min: false,
			max: false,
			same: false,
			atleast: false
        }, settings);
		
		// Stocke chaque erreur
		var stockErrors = new Array;
		
		// Retourne chaque élément
        return validElement.each(function(){
		
			// check de FORM
            if(settings.form) {
				$(this).submitForm(settings);
				globalErrors = [];
			}
			
			// check de REQUIRED
            if(settings.required) {				
   				if ( $(this).val() == "") {
   					stockErrors.push('required');
   				}
			}
			// check de REQUIREDIF
            if(settings.requiredIf) {
				var master = $('#'+settings.requiredIf)[0];
				if(master.checked == true) {
					if ( $(this).val() == "" ) {
						stockErrors.push('requiredIf');
					}
				}
			}
			// check de SELECT
			if(settings.select) {
				if($(this).val() == settings.select) {
					stockErrors.push('select');
				}
			}
			// check de SELECTIF - mix de RequiredIf et Select
			if(settings.selectIf) {
				var elems = settings.selectIf.split('|'); // on récupère les infos
				var id = elems[0]; // le premier element est l'id à checker
				var value = elems[1];  // le premier element est la value à checker
				var master = $('#'+id)[0];
				if(master.checked == true) {
					if($(this).val() == value) {
						stockErrors.push('selectIf');
					}
				}				
			}
			// check de CHECKED
			if(settings.checked) {
				if( !document.getElementById($(this).attr('id')).checked ) {
					stockErrors.push('checked');
				}
			}
			// check de DIFF
			if(settings.diff && $(this).val()!="") {
				if($(this).val() == document.getElementById(settings.diff).value) {
					stockErrors.push('diff');
				}
			}
			// check de CARAC
			if(settings.carac) {
				if(this.value != "") {
					switch(settings.carac) {
						case 'abc':
							var exp = new RegExp("^[a-zA-Z\ ]+$","i");
							var match = exp.test(this.value);
							if(!match) { stockErrors.push('abc'); }
						break;
						case 'num':
							var exp = new RegExp("^[0-9\ +()]+$", "i");
							var match = exp.test(this.value);
							if(!match) { stockErrors.push('num'); }
						break;
						case 'alphanum':
							var exp = new RegExp("^[a-zA-Z0-9]+$", "i");
							var match = exp.test(this.value);
							if(!match) { stockErrors.push('alphanum'); }
						break;
						case '123':
							var exp = new RegExp("^[0-9]+$", "i");
							var match = exp.test(this.value);
							if(!match) { stockErrors.push('123'); }
						break;
						case 'email':
							var exp = new RegExp("^[\\w\\-]+(\\.[\\w\\-]+)*@[\\w\\-]+(\\.[\\w\\-]+)*\\.[\\w\\-]{2,6}$", "i");
							var match = exp.test(this.value);
							if(!match) { stockErrors.push('email'); }
						break;
						default :
							return true;
						break;
					}
				}
			}
			// check de MIN
			if(settings.min && this.value!="") {
				var expMin = new RegExp("^[a-zA-Z0-9\.\,\ \']{"+settings.min+",}$","i");
				var matchMin = expMin.test(this.value);
				if(!matchMin) { stockErrors.push('min'); }
			}
			// check de MAX
			if(settings.max && this.value!="") {
				var expMax = new RegExp("^[^\*]{0,"+settings.max+"}$","i");
				var matchMax = expMax.test(this.value);
				if(!matchMax) { stockErrors.push('max'); }
			}
			// check de SAME
			if(settings.same && this.value!="") {
				var master = $('#'+settings.same)[0];
				if(master.value != this.value) {
					stockErrors.push('same'); 
				}
			}
			// check de ATLEAST
			if(settings.atleast) {
				var elems = settings.atleast.split('|'); // on récupère les infos
				var occ = elems[0]; // le premier element est le nombre d'occurences minimum
				elems.shift(); // on le retire
				var count = 0; // on initialise le compteur
				for(var i=0; i < elems.length; i++) {
					if(document.getElementById(elems[i]).type == 'text' || document.getElementById(elems[i]).nodeName == 'textarea') {
						if(document.getElementById(elems[i]).value != "") { count++ }
					}
					if(document.getElementById(elems[i]).type == 'checkbox' || document.getElementById(elems[i]).type == 'radio') {
						if(document.getElementById(elems[i]).checked) { count++ }
					}
					if(document.getElementById(elems[i]).nodeName == 'select') {
						var countSelect = 0;
						for(var op=0; op < document.getElementById(elems[i]).options.length; op++) {
							if([op].selected != "selected") { countSelect++ }
						}
						if(countSelect > 0) { count++ }
					}
				}
				if( count < occ ) { stockErrors.push('atleast');  }
			}
			// On applique l'extension showErrors à l'objet JQuery en lui passant le tableau des erreurs du champ et les options du champ.
			$(this).showErrors(stockErrors,settings);
        });		
    };

})(jQuery);