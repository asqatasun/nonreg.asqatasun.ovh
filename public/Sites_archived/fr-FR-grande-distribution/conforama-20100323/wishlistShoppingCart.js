// ****
// Taille de la wishlist
//
// Lecture du cookie "WISHLIST". Si cookie absent, ou cookies inactifs, retourne 0.
//
// @return le nombre d'articles dans la wishlist.
//
// ****
function getWishlistSize() {
	return getListItemSize ("WISHLIST");
}

// ****
// Taille du panier
//
// Lecture du cookie "SC". Si cookie absent, ou cookies inactifs, retourne 0.
//
// @return le nombre d'articles dans le panier
//
// ****
function getShoppingCartSize() {
	return getListItemSize ("SC");
}

// ****
//
// Taille de la liste d'articles pr�sents dans le cookie "cookieName"
//
// Le cookie contient une liste d'IDs : ID1|ID2|ID3| ... |IDn
//
// @param cookieName : le nom du cookie
//
// @return nombre d'articles pr�sents dans le cookie "cookieName"
//
// ****
function getListItemSize(cookieName) {
	var listItemSize = 0;
	var listItem = getCookie(cookieName);
	if(listItem != null && listItem.replace("\"\"","") != "") {
		listItemSize =	listItem.split("|").length;
	}	
	return listItemSize;
}