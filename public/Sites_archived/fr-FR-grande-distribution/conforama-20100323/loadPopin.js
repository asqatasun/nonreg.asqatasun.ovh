$(document).ready(function () {
	var agt=navigator.userAgent.toLowerCase();
		//var is_safari = ((agt.indexOf('safari')!=-1) &&(agt.indexOf('mac')!=-1))?true:false;
		//if (is_safari != true){
			initPopinDeclanche();
		//}
});

function initPopinDeclanche(){
   $("a.loadPopin").click(function(){
      masqueShow();
		initPopin();//dans scripts/popin.js
		showPopin();//dans scripts/popin.js
		
		//var maCss="../../css/general/"+getUrlVar($(this).attr("href"),"univers")+"/color.css";
		//var maLangCss="../../css/fr_FR/"+getUrlVar($(this).attr("href"),"univers")+"/color.css";
		var maCss="/wcsstore/Conforama/css/general/"+getUrlVar($(this).attr("href"),"univers")+"/color.css";
		var maLangCss="/wcsstore/Conforama/css/fr_FR/"+getUrlVar($(this).attr("href"),"univers")+"/color.css";
		var printCss="/wcsstore/Conforama/css/general/common/print_popin.css"; // Ajout Bi 7/01/08

		$("#cssColorTemp").attr("href", maCss);
		$("#cssColorLangTemp").attr("href", maLangCss);
		$("#cssPrintTemp").attr("href", printCss); // Ajout Bi 7/01/08
		
		window.location="#header";
      	myHref = this.href;
      	var pdtPopin1 = $(this).parents('.pdtDescription').is("div");
      	var lotPdtPopin = $(this).parents('.compositionElt').is("li");
      	
		$("#layerPopin").load(myHref, function(){
			loadAll();
			if(pdtPopin1 || lotPdtPopin){
				if($("#popinLayerDetails").is("div")){loadPopinDetails();}//dans scripts/onglets.js
			}
		});
      	$(this).unbind();
		return(false);
	});
}

function masqueShow(){
		var pageH = $("#contentAllWebsite").height();
		$("#masque").height(pageH+"px");
		$("#masque").show();
		$("#loading").show();
}

var reloadUnderPopin = (function(){
	// version 1 :
	// hidePopin();
	// version 2 :
	
	// pour le mdv on ne fera pas de reload...
	if ($("#CuisinePage").length > 0) return false;
	
	if(window.reloadPage){
		window.reloadPage();
		return false;
	}else{
		window.location.reload();
		return false;
	}
});

var loadAll2 = (function (){
	initClosePopin();
	if($(".qtyBloc .incLinkPopin").is("a")){initIncDecForPopin();}//dans scripts/incQuantity.js
	if($("#layerArticles").is("div")){initCaract();}//dans scripts/caractGuide.js
	if($("#tooltip").is("div")){initToolTipForPopin();}//dans scripts/initToolTip.js
   	if($(".formInPopin").is("form")){initCheckForm();}
	initPopinDeclanche();
});

var loadAll = (function (){
	initClosePopin();
	if($(".qtyBloc .incLinkPopin").is("a")){initIncDecForPopin();}//dans scripts/incQuantity.js
	if($("#layerArticles").is("div")){initCaract();}//dans scripts/caractGuide.js
	if($("#tooltip").is("div")){initToolTipForPopin();}//dans scripts/initToolTip.js
   	if($(".formInPopin").is("form")){initCheckForm();}
   	if($("div#contentPopin.reloadOnClose").is("div")){
   	$("#masque").click(reloadUnderPopin);}
	initPopinDeclanche();
	if(typeof(openZoom) == 'function')
		jQuery("#btZoomPopin a, #diapoImgPopin a").bind("click",openZoom);//slideShow.js
	try{initInfos();}catch(e){}
	try{AllFarandoles._Init();initCarousel();}catch(e){}
	try{if (initPopinForward != undefined && initPopinForward== true)initPopinForward();}catch(e){}
});

function getUrlVar(monHref, maVar){			
	var univers = monHref.substr(monHref.lastIndexOf("_") + 1);
	return univers;
	var urlVar=monHref.substr(monHref.indexOf("?"));
	urlVar=urlVar.replace("?", "");
	if(urlVar.indexOf("&")!=-1){
		var tabUrlVar=urlVar.split("&");
		for(var i=0; i<tabUrlVar.length; i++){
			if(tabUrlVar[i].indexOf(maVar)!=-1){
				var maVariable=tabUrlVar[i].replace(maVar+"=", "");
				return(maVariable);
			}
		}
	}else if((urlVar=="") && (maVar=="cuisine") && (situ=="cuisine")){
		return("bruyere");
	}else if(urlVar.indexOf(maVar)==-1){
		return("");
	}else{
		maVariable=urlVar.replace((maVar+"="), "");
		return(maVariable);
	}
}

function loadFaqPopin(url){
    masqueShow();
	initPopin();//dans scripts/popin.js
	showPopin();//dans scripts/popin.js
	window.location="#header";
	$("#layerPopin").load(url, function(){loadAll();});
    $(this).unbind();
	return(false);
}