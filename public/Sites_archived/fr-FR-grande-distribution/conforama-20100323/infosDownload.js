function initInfosDownload(){
   if(document.getElementById("infosDownloadLink") && document.getElementById("infosDownload")){
      document.getElementById("infosDownload").style.display = "none";
      var link = document.getElementById("infosDownloadLink");
      addEvent(link, 'click', displayInfosDownload, false);      
      link.href = "javascript:returnFalse();";
      var btclose = document.getElementById("closeInfosDownload");
      addEvent(btclose, 'click', hideInfosDownload, false);
      btclose.href = "javascript:returnFalse();";
   }
}
//Fonction incr&eacute;mente quantit&eacute; article panier
function displayInfosDownload(){
	if(document.getElementById("infosDownload").style.display == "none"){
      document.getElementById("infosDownload").style.display = "block";
   }
   else{
      hideInfosDownload();
   }
}

function hideInfosDownload(){
   document.getElementById("infosDownload").style.display = "none";
}

function returnFalse(){
}

//initialisation au chargement de la page
//addLoadListener(initInfosDownload);