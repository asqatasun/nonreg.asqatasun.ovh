/**
 * elements dhtml statiques du site conforama corp, uniquement les evenements
 * utilisateurs sans transfers de donn&eacute;es.
 * 
**/

var PopinHome={
	_Init:function(){
		var linkClose='<a href="#" id="linkCloseHome"><span class="displayNone"></span></a>'
		$("#footerHomeConfo").append(linkClose);
		$("#footerHomeConfo #linkCloseHome").click(function(){
			$("#footerHomeConfo").hide();
			return(false);
		});
		$("#footerHomeConfo").hide();
		$("#linkConfoHome a").click(function(){
			$("#footerHomeConfo").show();
		});
	}
}


 $(document).ready(function() {
	PopinHome._Init();//initialisation de la popîn de home
	
 });//initialisation du document