// Initialisation des check de forms - utiliser par checkForms.js

function initAutoURLRedirect() {

	if(!document.getElementById("reloadParent") && document.getElementById("reloadForm")){
		var myForm = document.forms["reloadForm"];
		myForm.submit();
	}else{
		if($("div#reloadParent").is("div")){
	   	 	window.location.reload();
			return false;
		}
	}
	
}

function checkPopAddress(form) {
	errorClasses = ['errorInputText','etatInputForm'];
	okClasses = ['okInputForm'];
	manageErrors = 'displayNone';
	$("#addressName").customValidate({ required:true , max:'255' });
	$("#adrsSurName").customValidate({ required:true , max:'255' });
	$("#adrsName").customValidate({ required:true , max:'255' });
	$("#pays").customValidate({ select:'-' });
	$("#adrsLivr").customValidate({ required:true , max:'40' });
	$("#adrsLivr2").customValidate({  max:'40' });
	$("#cp").customValidate({ required:true , carac:'123' , min:'5' , max:'5' });
	$("#ville").customValidate({ select:'-' });
	
   //D�BUT MODIFICATIONS DU 09/08/07
	//$("#adrsTelChoose").customValidate({ atleast:'1|adrsTelPtbl|adrsTelDom|adrsTelPro' });	
   $("#adrsTel").customValidate({ required:true , carac:'num' });
	//$("#adrsTelDom").customValidate({ carac:'num' });
	//$("#adrsTelPro").customValidate({ carac:'num' });
   
	//FIN MODIFICATIONS DU 09/08/07
   
	$(form).customValidate({ form:true , method:'initDemoPopin' });
}

function checkPopAddress2(form) {
	errorClasses = ['errorInputText','etatInputForm'];
	okClasses = ['okInputForm'];
	manageErrors = 'displayNone';
	$("#retirerProducts").customValidate({ atleast:'1|proximite1|proximite2' });
	
	$(form).customValidate({ form:true });
}

function checkPopinAddress(form) {
	errorClasses = ['errorInputText','etatInputForm'];
	okClasses = ['okInputForm'];
	manageErrors = 'displayNone';
	
	$("#SelCiv").customValidate({ select:'0' });
	$("#nickName").customValidate({ required:true , max:'255' });	
	$("#firstName").customValidate({ required:true , max:'255' , carac: 'name' });
	$("#lastName").customValidate({ required:true , max:'255' , carac: 'name' });
	$("#country").customValidate({ select:'-' });	
	$("#city").customValidate({select:'-' });
	$("#address1").customValidate({ required:true , max:'40' });
	$("#address2").customValidate({  max:'40' });
	if($("#country").val() == 'CH') {
		//NPA	
		$("#contactCp").customValidate({ required:true , carac:'123' , min:'4' , max:'4' });
	} else if($("#country").val() == 'FR') {
		$("#contactCp").customValidate({ required:true , carac:'123' , min:'5' , max:'5' });
	}else{
		$("#contactCp").customValidate({ required:true , carac:'123' , max:'10' });
	}
	$("#telChoose").customValidate({ atleast:'1|telPtbl|telDom|telPro' });
	$("#telPtbl").customValidate({ carac:'num' });
	$("#telDom").customValidate({ carac:'num' });
	$("#telPro").customValidate({ carac:'num' });
	
	$(form).customValidate({ form:true , method:'initDemoPopin' });
}


function checkLogin(form) {
	errorClasses = ['errorInputText'];
	okClasses = [];
	manageErrors = 'displayNone';
	$("#loginMail").customValidate({ required:true , carac:'email' });
	$("#loginPass").customValidate({ required:true });
	
	$(form).customValidate({ form:true });
}

function checkUnsubsribeNL(form) {
	errorClasses = ['errorInputText','etatInputForm'];
	okClasses = ['okInputForm'];
	manageErrors = 'displayNone';
	$("#email").customValidate({ required:true , carac:'email' });
	$("#confEmail").customValidate({ required:true , same:'email' });
	
	$(form).customValidate({ form:true });
}

function checkCreaCompte(form) {
	errorClasses = ['errorInputText','etatInputForm'];
	okClasses = ['okInputForm'];
	manageErrors = 'displayNone';	

	$("#factSelCiv").customValidate({ select:'0' });
	$("#factPrenom").customValidate({ required:true , max:'255' , carac: 'name' });
	$("#factNom").customValidate({ required:true , max:'255' , carac: 'name' });	
	$("#factPays").customValidate({ select:'-' });
	$("#factAdrs").customValidate({ required:true , max:'40' , carac: 'address' });
	$("#factAdrs2").customValidate({ max:'40' , carac: 'address' });
	if($("#factPays").val() == 'FR') {
		$("#factCp").customValidate({ required:true , carac:'123' , min:'5' , max:'5' });
	} else {
		$("#factCp").customValidate({ required:true , carac:'123' , max:'10' });
	}

	var factVilleTmp= document.getElementById("factVille");
	var factVilleTextTmp= document.getElementById("factVilleText");
	if(factVilleTextTmp.value==''){
       $("#factVille").customValidate({ select:'-' });
    }else if(factVilleTmp.value=='-'){
      $("#factVilleText").customValidate({ select:'' });
    }
    
    var autreAdrSFact_oui= document.getElementById("autreAdrSFact_oui");
	if(autreAdrSFact_oui.checked){
        $("#factIntitule").customValidate({ required:true, diff:'livrIntitule' });
	}else{
  	   $("#factIntitule").customValidate({ required:true});
	}
	
	$("#livrSelCiv").customValidate({ selectIf:'autreAdrSFact_oui|0' });
	$("#livrPrenom").customValidate({ requiredIf:'autreAdrSFact_oui' , max:'255' });
	$("#livrNom").customValidate({ requiredIf:'autreAdrSFact_oui' , max:'255' });
	$("#livrPays").customValidate({ selectIf:'autreAdrSFact_oui|-' });
	$("#livrAdrs").customValidate({ requiredIf:'autreAdrSFact_oui' , max:'40' });
	$("#livrAdrs2").customValidate({ max:'40' });
		//$("#livrCp").customValidate({ requiredIf:'autreAdrSFact_oui' , carac:'123' , min:'5' , max:'5' });
	if($("#livrPays").val() == 'FR') {
		$("#livrCp").customValidate({ requiredIf:'autreAdrSFact_oui' , carac:'123' , min:'5' , max:'5' });
	} else {
		$("#livrCp").customValidate({ requiredIf:'autreAdrSFact_oui' , carac:'123' , max:'10' });
	}	
   //$("#livrVille").customValidate({ selectIf:'autreAdrSFact_oui|-' });
	var livrVilleTmp= document.getElementById("livrVille");
	var livrVilleTextTmp= document.getElementById("livrVilleText");
	if(livrVilleTextTmp.value==''){
       $("#livrVille").customValidate({ selectIf:'autreAdrSFact_oui|-' });
    }else if(livrVilleTmp.value=='-'){
       	var valRadio= document.getElementById("autreAdrSFact_oui");
       	if(valRadio.value='Y'){
          $("#livrVilleText").customValidate({ select:'' });
      }
    }
	
	if(autreAdrSFact_oui.checked){
        $("#livrIntitule").customValidate({ required:true, diff:'factIntitule' });
	}
	
	$("#livrTel").customValidate({ requiredIf:'autreAdrSFact_oui' , carac:'num' });
	
	$("#telChoose").customValidate({ atleast:'1|telPtbl|telDom|telPro' });
	$("#telPtbl").customValidate({ carac:'num' });
	$("#telDom").customValidate({ carac:'num' });
	$("#telPro").customValidate({ carac:'num' });
	
	$("#email").customValidate({ required:true , carac:'email' });
	$("#confEmail").customValidate({ required:true , same:'email' });
	$("#pass").customValidate({ required:true, max:'255' }); // carac:'alphanum' , min:'6' , max:'12'
	$("#confPass").customValidate({ required:true , same:'pass' });
	//$("#newsLetter").customValidate({ checked:true });
	
	
	if($("#infoSMS:checked").length != 0){
		$("#telPtbl").customValidate({ required:true,carac:'num' });
	}
	
	$(form).customValidate({ form:true });
}

function checkDataPerso(form) {
	errorClasses = ['errorInputText','etatInputForm'];
	okClasses = ['okInputForm'];
	manageErrors = 'displayNone';
	$("#dataSelCiv").customValidate({ select:'0' });
	$("#dataPrenom").customValidate({ required:true , max:'255' , carac:'name' });
	$("#dataNom").customValidate({ required:true , max:'255' , carac:'name' });
   
	$("#telChoose").customValidate({ atleast:'1|telPtbl|telDom|telPro' });
	$("#telPtbl").customValidate({ carac:'num' });
	$("#telDom").customValidate({ carac:'num' });
	$("#telPro").customValidate({ carac:'num' });
	
    $("#email1").customValidate({ required:true , carac:'email' });   
    var userLogin= document.getElementById("userLogin");
    var email1= document.getElementById("email1");
    var email2= document.getElementById("email2");   
	
	if(email1.value!=email2.value) {
		email2.value=email1.value;
	}
    
    var logonPasswordDataPerso= document.getElementById("logonPasswordDataPerso");
    var logonPasswordVerify= document.getElementById("logonPasswordVerify");
    
    if(logonPasswordDataPerso.value!=''){
       $("#logonPasswordDataPerso").customValidate({ required:true, max:'255' });
	   $("#logonPasswordVerify").customValidate({ required:true , same:'logonPasswordDataPerso' });
    } else {
        if(logonPasswordVerify.value!=''){
          $("#logonPasswordDataPerso").customValidate({ required:true, max:'255' });
	      $("#logonPasswordVerify").customValidate({ required:true , same:'logonPasswordDataPerso' });
        } else {
          //test pour supprimer la non validation
          $("#logonPasswordDataPerso").customValidate({ select:'-' });
	      $("#logonPasswordVerify").customValidate({ select:'-' });
        }
    }

	
	var yourBirthDay= document.getElementById("yourBirthDay");
	var yourBirthMonth= document.getElementById("yourBirthMonth");
	var yourBirthYear= document.getElementById("yourBirthYear");
	
	if(yourBirthDay.selectedIndex>0 ||yourBirthMonth.selectedIndex>0 || yourBirthYear.selectedIndex>0){	   	 
     	$("#yourBirthDay").customValidate({ select:'00' });
     	$("#yourBirthMonth").customValidate({ select:'00' });
     	$("#yourBirthYear").customValidate({ select:'0000' });  
     	if (!isDate(yourBirthDay.value + '/' + yourBirthMonth.value + '/' + yourBirthYear.value)) {
     	 	$("#yourBirthDay").customValidate({ select:yourBirthDay.value });
     	}
	}else if(yourBirthDay.selectedIndex==0 && yourBirthMonth.selectedIndex==0 && yourBirthYear.selectedIndex==0) {	  
	    //test pour supprimer la non validation
	    $("#yourBirthDay").customValidate({ select:'-' });
     	$("#yourBirthMonth").customValidate({ select:'-' });
     	$("#yourBirthYear").customValidate({ select:'-' });     	
	}
	
	var numPerso= document.getElementById("numPerso");	
	for(var i=0;i < numPerso.value;i++){
		var persFirstnameIndex = "persFirstname" + i;
		var persBirthDayIndex = "persBirthDay" + i;
		var persBirthMonthIndex = "persBirthMonth" + i;
		var persBirthYearIndex = "persBirthYear" + i;
		$("#" + persFirstnameIndex).customValidate({ required:true , max:'255' }); 
     	$("#" + persBirthDayIndex).customValidate({ select:'00' });
     	$("#" + persBirthMonthIndex).customValidate({ select:'00' });
     	$("#" + persBirthYearIndex).customValidate({ select:'0000' });  
     	
     	var persBirthDayIndexValue = document.getElementById(persBirthDayIndex).value;
		var persBirthMonthIndexValue = document.getElementById(persBirthMonthIndex).value;
		var persBirthYearIndexValue = document.getElementById(persBirthYearIndex).value;
		
     	if (!isValidDate(persBirthDayIndexValue + '/' + persBirthMonthIndexValue + '/' + persBirthYearIndexValue)) {     	    
     	 	$("#" + persBirthDayIndex).customValidate({ select:persBirthDayIndexValue });
     	}   			
	}
	$(form).customValidate({ form:true });
}


function checkFormContact(form) {
	errorClasses = ['errorInputText','etatInputForm'];
	okClasses = ['okInputForm'];
	manageErrors = 'displayNone';
	$("#contactSelCiv").customValidate({ select:'0' });
	$("#contactPrenom").customValidate({ required:true , max:'255' });
	$("#contactNom").customValidate({ required:true , max:'255' });
	
	$("#country").customValidate({ select:'-' });   
	$("#contactAdrs").customValidate({ required:true , max:'40' });
	$("#contactAdrs2").customValidate({ max:'40' });
	
	if($("#contactPays").val() == 'CH') {
		//NPA	
		$("#contactCp").customValidate({ required:true , carac:'123' , min:'4' , max:'4' });
	} else if($("#contactPays").val() == 'FR') {
		$("#contactCp").customValidate({ required:true , carac:'123' , min:'5' , max:'5' });
	}else{
		$("#contactCp").customValidate({ required:true , carac:'123' , max:'10' });
	}
	$("#contactVille").customValidate({ select:'-' });
	
	$("#telChoose").customValidate({ atleast:'1|telPtbl|telDom|telPro' });
	$("#telPtbl").customValidate({ carac:'num' });
	$("#telDom").customValidate({ carac:'num' });
	$("#telPro").customValidate({ carac:'num' });
	
	$("#contactMagasin").customValidate({ required:true , max:'255' });
	$("#contactNomProduit").customValidate({ required:true , max:'255' });
	$("#contactMsg").customValidate({ required:true , max:'255' });
	
	$(form).customValidate({ form:true });
}

function checkFormContactFAQ(form) {
	errorClasses = ['errorInputText','etatInputForm'];
	okClasses = ['okInputForm'];
	manageErrors = 'displayNone';
	$("#contactSelCiv").customValidate({ select:'-' });
	$("#contactPrenom").customValidate({ required:true , max:'255' });
	$("#contactNom").customValidate({ required:true , max:'255' });
	
	$("#contactAdrs").customValidate({ required:true , max:'40' });
	$("#contactAdrs2").customValidate({ max:'40' });
	if($("#contactPays").val() == 'CH') {
		//NPA	
		$("#contactCp").customValidate({ required:true , carac:'123' , min:'4' , max:'4' });
	} else if($("#contactPays").val() == 'FR') {
		$("#contactCp").customValidate({ required:true , carac:'123' , min:'5' , max:'5' });
	}else{
		$("#contactCp").customValidate({ required:true , carac:'123' , max:'10' });
	}
	if(document.getElementById("contactPays").value == 'FR'){
		$("#contactVille").customValidate({ select:'-' });
	}else{
		$("#contactVilleText").customValidate({ required:true , max:'27'});
	}
	$("#contactMail").customValidate({ required:true , carac:'email' });
	
	$("#telChoose").customValidate({ atleast:'1|telPtbl|telDom|telPro' });
	$("#telPtbl").customValidate({ carac:'num' });
	$("#telDom").customValidate({ carac:'num' });
	$("#telPro").customValidate({ carac:'num' });
	
	$("#contactMagasin").customValidate({ required:true , max:'255' });
	$("#contactNomProduit").customValidate({ required:true , max:'255' });
	$("#contactMsg").customValidate({ required:true });
	$("#contactDateAchat").customValidate({ carac:'date' });
	
	$(form).customValidate({ form:true });
}

$().ready(function() {
	if($("#formCreaCompte").is("form")) {
		$("#submitCreaCompte").bind( "click", function(event) {
			event.preventDefault();
			checkCreaCompte($("#formCreaCompte"));		
		});
	}
	if($("#formLogin").is("form")) {
		$("#submitLogin").bind( "click", function(event) {
			event.preventDefault();
			checkLogin($("#formLogin"));		
		});
	}
   if($("#formDataPerso").is("form")) {
		$("#submitDataPerso").bind( "click", function(event) {
			event.preventDefault();
			checkDataPerso($("#formDataPerso"));		
		});
	}
	if($("#formContact").is("form")) {
		$("#submitContact").bind( "click", function(event) {
			event.preventDefault();
			checkFormContact($("#formContact"));		
		});
	}
	if($("#formContactFAQ").is("form")) {
		$("#submitContactFAQ").bind( "click", function(event) {
			event.preventDefault();
			checkFormContactFAQ($("#formContactFAQ"));		
		});
	}
	if($("#formUnsubscribeNL").is("form")) {
		$("#submitUnsubsribeNL").bind( "click", function(event) {
        	event.preventDefault();
			checkUnsubsribeNL($("#formUnsubscribeNL"));		
		});
	}
		
	$("#paymentForm form").submit(function(event) {
		if($("input[@name='acceptCG']:checked").length == 0){ 
			$("#cgvError").show();
			return false; 
		}
	});

});

function checkForgetPass(form) {
	errorClasses = ['errorInputText','etatInputForm'];
	okClasses = ['okInputForm'];
	manageErrors = 'displayNone';
	$("#forgetEmail").customValidate({ required:true , carac:'email' });
	$(form).customValidate({ form:true , method:'initDemoPopin' });
}

var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s)
{
	var i;
    for (i = 0; i < s.length; i++)
    {   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag)
{
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year)
{
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++)
	 {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}


function isValidDate(dtStr)
{
	var pos1=dtStr.indexOf(dtCh);
	var pos2=dtStr.indexOf(dtCh,pos1+1);
	var day=dtStr.substring(0,pos1);
	var month=dtStr.substring(pos1+1,pos2);
	var year=dtStr.substring(pos2+1);	
	var datTest_Date = new Date(year, month-1, day);
	if ((day != datTest_Date.getDate()) || (month-1 != datTest_Date.getMonth()))
	 { 
		return false;
	 }
	 if (year.length != 4 || year==0 || year<minYear || year>maxYear)
	 { 
		return false;
	 }
	 
 return true;
}


function isDate(dtStr)
{
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strDay=dtStr.substring(0,pos1)
	var strMonth=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1)
	{		
		return false
	}
	if (strMonth.length<1 || month<1 || month>12)
	{
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month])
	{
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear)
	{
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false)
	{
		return false
	}
	return true
}

function checkPopinOrderItemAddForm(form) {
	var nbCItem = document.getElementById("nbCrossItem");
	var count = 0;
	var doSubmit = false;
	var itemStr = '';
	if(nbCItem != 'undefined') {
		for(i = 1; i <= nbCItem.value; i++) {
			var qtySeq = document.getElementById("quantity_" + i);
			if(qtySeq != 'undefined') {
				if(qtySeq.value > 0) {
					doSubmit = true;
					var brandRef = document.getElementById("brandRef_" + i);
					if(brandRef != 'undefined') {
						if(count > 0) {
							itemStr += ";";
						}
						itemStr += brandRef.value;
						count++;
					}
				}
			}
		}
	}
	if ($("input.xSellServiceProposal:checked").length > 0) doSubmit = true;
	if(doSubmit) {
		document.getElementById("crossAddedItemList").value = itemStr;
		// AGR: ajout pour gld
		//submitFormInPopin();
		submitFormInAddToBagPopin();
	} else {
		document.location.href = $("#addCrossSellsAndGotoCart").href();
	    // AGR: fin ajout pour gld
	}
}

var initCheckForm = (function () {   
	$(".formInPopin .submitFormInPopin").click(function() {
		if($("#forgetPassForm").is("form")){
			checkForgetPass($("#forgetPassForm"));
		} else if($("#changeAddress").is("form")) {
			checkPopAddress($("#changeAddress"));
		} else if($("#changeAddress2").is("form")) {
			checkPopAddress2($("#changeAddress2"));
		} else if($("#formAddressUpdate").is("form")) {
			checkPopinAddress($("#formAddressUpdate"));
		} else if($("#popinOrderItemAddForm").is("form")) {
			checkPopinOrderItemAddForm($("#popinOrderItemAddForm"));
		} else {
			submitFormInPopin();
		}
     	return false;
	});
	
	// AGR: ajout pour gld
	$("#addCrossSellsAndGotoCart").click(function() {
		if($("#popinOrderItemAddForm").is("form")) {
			checkPopinOrderItemAddForm($("#popinOrderItemAddForm"));
		} else {
			//submitFormInPopin();
		}
     	return false;
	});
	// AGR: fin ajout pour gld
   
	// dsic : customize event handling for form#formSelectionRegionMagasin
	$("#formSelectionRegionMagasin select.submitFormInPopin").unbind("click");
	$("#formSelectionRegionMagasin select.submitFormInPopin").change(function() {

	if($("#formSelectionRegionMagasin select").val() == 0) {
		return false;
	}
	var myForm = $("#formSelectionRegionMagasin");
  	var result = "";
		
	$("#formSelectionRegionMagasin input[@type=hidden]").each(function() {
		result += $(this).attr("name")+"="+$(this).val()+"&";
	})		     
	$("#formSelectionRegionMagasin select").each(function() {
		result += $(this).attr("name")+"="+$(this).val();
	})

	var url = myForm.attr("action");
	$.ajax({
	      type:"POST",
	      url: url,
	      data: result,
	      success:function(msg) {
	         //affichage le contenu de la page r�sultat
	         var content = msg.responseText;
	         
	         document.getElementById("layerPopin").innerHTML = content;
	         
	         initClosePopin();
	         initCheckForm();
			 initPopinDeclanche();
    	  }
	   });       
     return false;
   });
   
   
   // dsic : customize event handling for map#Map
   $("map#Map area.linkInPopin").click(function() {
   
   		var myHref = $(this).attr("href");
   		var split = myHref.indexOf('?');

		var url = myHref.substring(0,split);
		var data = myHref.substring(split+1, myHref.length);

   		$.ajax({
			type:"POST",
			url: url,
			data: data,
			success:function(msg) {
				//affichage le contenu de la page r�sultat
				var content = msg.responseText;
				
				document.getElementById("layerPopin").innerHTML = content;

				initClosePopin();
				initCheckForm();
				initPopinDeclanche();
			}
		});	          
		return false;       
	});

   // tma : customize event handling for map#Map --> When rewritte url with url mapper
   $("map#Map area.linkInPopinWithUrlMapper").click(function() {
   		var myHref = $(this).attr("href");
		$("#layerPopin").load(myHref, function(){loadAll();});
		return false;       
	});
	
});

var clickd = false;
var submitFormInPopin = (function () {  
	if (clickd) return false;
	clickd = true;
	var myForm = $(".formInPopin");
   
	var result = "";
	$(".formInPopin input[@type=text]").each(function() {
		result += $(this).attr("name")+"="+$(this).val()+"&";
	});
	$(".formInPopin input[@type=hidden]").each(function() {
		result += $(this).attr("name")+"="+$(this).val()+"&";
	});
	$(".formInPopin textarea").each(function() {
		result += $(this).attr("name")+"="+$(this).val()+"&";
	});  
	$(".formInPopin select").each(function() {
		result += $(this).attr("name")+"="+$(this).val()+"&";
	});   
	$(".formInPopin input[@type=radio]:checked").each(function() {
		result += $(this).attr("name")+"="+$(this).val()+"&";
	});
	
	$.ajax({
		type:"POST",
		url: myForm.attr("action"),
		data: encodeURI(result),
		success:function(msg) {
			//affichage le contenu de la page r�sultat
			var content = msg.responseText;

			document.getElementById("layerPopin").innerHTML = content;

			initClosePopin();
			initCheckForm();
			initPopinDeclanche();
			initAutoURLRedirect();
			loadAll();
			clickd = false;
		}
	});  
});


// AGR: ajout pour gld
var clickdForAddToBag = false;
var submitFormInAddToBagPopin = (function () {  
	if (clickdForAddToBag) return false;
	clickdForAddToBag = true;
	var myForm = $(".formInPopin");
   
	var result = "";
	$(".formInPopin input[@type=text]").each(function() {
		result += $(this).attr("name")+"="+$(this).val()+"&";
	});
	$(".formInPopin input[@type=hidden]").each(function() {
		result += $(this).attr("name")+"="+$(this).val()+"&";
	});
	$(".formInPopin textarea").each(function() {
		result += $(this).attr("name")+"="+$(this).val()+"&";
	});  
	$(".formInPopin select").each(function() {
		result += $(this).attr("name")+"="+$(this).val()+"&";
	});   
	$(".formInPopin input[@type=radio]:checked").each(function() {
		result += $(this).attr("name")+"="+$(this).val()+"&";
	});
	$(".formInPopin input.xSellServiceProposal:checked").each(function() {
		result += $(this).attr("name")+"="+$(this).val()+"&";
	});
	
	$.ajax({
		type:"POST",
		url: myForm.attr("action"),
		data: encodeURI(result),
		success:function(msg) {
			document.location.href = $("#addCrossSellsAndGotoCart").href();
			clickdForAddToBag = false;
		}
	});  
});
// AGR: fin ajout pour gld


function clearZipCodeAndCity(){
	$("#livrCp").val("");
	$("#factVille").selectedIndex = 0;
	$("#livrVilleText").val("");
	$("#factCp").val("");
	$("#factVille").selectedIndex = 0;
	$("#factVilleText").val("");
}

function updateMaxLength(){
	if($("#livrPays").val() == 'CH') {
		//NPA	
		limitMaxLength("#livrCp",4);
	} else if($("#livrPays").val() == 'FR') {
		limitMaxLength("#livrCp",5);
	}else{
		limitMaxLength("#livrCp",10);
	}
	if($("#factPays").val() == 'CH') {
		//NPA	
		limitMaxLength("#factCp",4);
	} else if($("#factPays").val() == 'FR') {
		limitMaxLength("#factCp",5);
	}else{
		limitMaxLength("#factCp",10);
	}
}

function limitMaxLength(id,len){
	if($(id).val().length > len-1){
		var str = $(id).val().substr(0,len);
		$(id).val(str);
	}
}

function updateMaxLength2(){
	if($("#country").val() == 'CH') {
		//NPA	
		limitMaxLength("#zipCode",4);
	} else if($("#country").val() == 'FR') {
		limitMaxLength("#zipCode",5);
	}else{
		limitMaxLength("#zipCode",10);
	}
}

function updateContactMaxLength(){
	if($("#contactPays").val() == 'CH') {
		//NPA	
		limitMaxLength("#contactCp",4);
	} else if($("#contactPays").val() == 'FR') {
		limitMaxLength("#contactCp",5);
	}else{
		limitMaxLength("#contactCp",10);
	}
}
