/*	Simple Javascript RSS Reader Version 1.0
	Copyright (c) 2006 CS Truter
	Written by Christoff Truter
	email: Christoff@cstruter.com - (Please let me know if you intend to use the script) */

/* Replace all occurances of a string
  (Parameters) totalValue:'complete string' 
		oldValue:'value to be replaced' newValue:'value used for replace' */

function Replace(totalValue,oldValue,newValue)
{
	while(totalValue.indexOf(oldValue) > -1)
		totalValue=totalValue.replace(oldValue,newValue);
			return totalValue;
}

/* Get XML Node
   (Parameters) TagName:'XML Element' node:'Element row number' */

function getNode(TagName, node)
{
	var currentNode = (node == null) ? xmlDoc.getElementsByTagName(TagName) : 
					items[node].getElementsByTagName(TagName);
	if(currentNode.length > 0) {
		chaine=currentNode[0].firstChild.nodeValue;
		// Pour la date ne retourne que l'heure
		if (TagName=="pubDate") {
			ind=chaine.indexOf(" 200");
			//alert(ind);
			if (ind!=-1) {
				chaine=chaine.substring(ind+6,ind+6+5);
			}
		}
		// Retourne un maximum de 35 caracteres.
		if (TagName=="title") {
			chaine=chaine+"                                                                                ";
			chaine=chaine.substring(0,50);
		}
		return chaine;
	}
}

/* Load XML Object
   (Parameters) rssFeed:'RSS File' Body:'Layer for RSS Body' Title:'Layer for RSS Title' */

function ReadRSS(rssFeed, Body, Title) 
{
	rssTitle = document.getElementById(Title);	
	rssBody = document.getElementById(Body);

	try
	{

	  // ActiveX pour Internet Explorer
  	if (window.ActiveXObject) {
	    try {
		  	//alert("xmlDoc Creation Msxml2.XMLDOM");
        xmlDoc = new ActiveXObject('Msxml2.XMLDOM');
      } catch (e) {
		  	//alert("xmlDoc Creation Microsoft.XMLDOM");
        xmlDoc = new ActiveXObject('Microsoft.XMLDOM');
      }
      xmlDoc.async = true;
      xmlDoc.load(rssFeed);
    // � l'aide de lobjet XMLHTTPRequest
    } else if (window.XMLHttpRequest) {
	  	//alert("xmlDoc Creation XMLHttpRequest()");
      xmlDoc = new XMLHttpRequest();
      xmlDoc.overrideMimeType('text/xml'); 
      xmlDoc.open('GET', rssFeed, true);
      xmlDoc.send(null);
		// navigateur bas� sur Gecko
		} else if (document.implementation && document.implementation.createDocument) {
	  	//alert("xmlDoc Creation document.implementation.createDocument()");
      xmlDoc = document.implementation.createDocument('', '', null);
			xmlDoc.async=true;
      xmlDoc.load(rssFeed);
    }

		xmlDoc.onreadystatechange  = function()
    { 
	    	//alert("xmlDoc.readyState:"+xmlDoc.readyState);
         if(xmlDoc.readyState  == 4)
         {
 				    	//alert("xmlDoc.status:"+xmlDoc.status);
              if(xmlDoc.status  == 200) { // Cas FireFox
								items=xmlDoc.responseXML.getElementsByTagName('item');
								SetRSSTemplates();
              } else { // Cas IE 6&7
								items=xmlDoc.getElementsByTagName('item');
								SetRSSTemplates();
         			}
         }
    };     
	}
	
	catch(e)
	{
		//rssTitle.innerHTML = 'Error occured';
		//rssBody.innerHTML = 'Thrown Error:'+e.message;
		//alert('Thrown Error:'+e.message)
	}
}

/* Set HTML Template
	Did it this way to make the look and feel of the feed easy customizable, dont like mixing
	layout with code. */

function SetRSSTemplates()
{
	if (rssBody)
	{
		var buffer = "";
		//for(var i=0; i< items.length; i++) 
		var max=5;
		if (items.length<max) max=items.length;
		for(var i=0; i< 5; i++) 
		{
			var output = (document.all) ? Replace(rssBody.innerHTML,"(::Link::)",getNode('link',i)) 
									   : Replace(rssBody.innerHTML,"%28::Link::%29",getNode('link',i));
			output = Replace(output,"(::Title::)",getNode('title',i));
			output = Replace(output,"(::Pubdate::)",getNode('pubDate',i));
			output = Replace(output,"(::Description::)",getNode('description',i));
			buffer+=output;
		}
		rssBody.innerHTML = buffer;
		document.getElementById('rssBodyTemplate').style.visibility = "visible";
	}

	if (rssTitle)
	{
		var output = Replace(rssTitle.innerHTML,"(::Title::)",getNode('title'));
		output = (document.all) ? Replace(output,"(::Link::)",getNode('link'))
							   : Replace(output,"%28::Link::%29",getNode('link'));		
		output = Replace(output,"(::Description::)",getNode('description'));
		rssTitle.innerHTML = output;
	}
}
