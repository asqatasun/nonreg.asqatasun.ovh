var multiChannelTemplate = new Object();
multiChannelTemplate.campaignSkin = new Object();

multiChannelTemplate.setDefaultCampaignSkin = function(campaignSkin) {
	multiChannelTemplate.defaultCampaignSkin = campaignSkin;
}

multiChannelTemplate.addCampaignSkin = function(id, campaignSkin) {
	multiChannelTemplate.campaignSkin[id] = new Object();
	multiChannelTemplate.campaignSkin[id].campaignSkin = campaignSkin;
}

multiChannelTemplate.setNavigation = function(id) {

	if (multiChannelTemplate.campaignSkin[id] != null) {
		var campaignSkin = multiChannelTemplate.campaignSkin[id].campaignSkin;

		$$('link[title=theme]').each(function(item) {
			item.href = item.href.replace(multiChannelTemplate.defaultCampaignSkin, campaignSkin);
		});
	}

	$($('search').elements['searchCategory']).getElements('option').each(function(item, index) {

		if (item.value == id) {
			item.parentNode.selectedIndex = index;
		}
	});
	$$('#' + id + ' a')[0].addClass('selected');
}

multiChannelTemplate.setNavigationSub = function(id) {
	$$('#' + id + ' a')[0].addClass('selected');
}
