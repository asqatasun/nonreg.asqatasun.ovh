var initialiser = false ;
var timeID1 = null ;
var timeID2 = null ;
//onresize=rechargerPage
is = new BrowserCheck() ;
pf = new PlateformeCheck() ;

function init() {
	initialiser=true ;
}

function openWindowPrint(url){
	ouvrirFenetre(url,650,600);
}

function BrowserCheck() {
	var b = navigator.appName	
	if (b=="Netscape") this.b = "ns"
	else if (b=="Microsoft Internet Explorer") this.b = "ie"
	else this.b = b
	this.version = navigator.appVersion
	this.v = parseInt(this.version)
	this.ns = (this.b=="ns" && this.v>=4)
	this.ns4 = (this.b=="ns" && this.v==4)
	this.ns5 = (this.b=="ns" && this.v>=5)
	this.ie = (this.b=="ie" && this.v>=4)
	this.ie4 = (this.version.indexOf('MSIE 4')>0)
	this.ie5 = (this.version.indexOf('MSIE 5')>0)
	this.min = (this.ns || this.ie)
	this.plateforme = "plateforme" + navigator.platform
	this.pc = (this.plateforme.indexOf('Win')>0)
	this.mac = (this.plateforme.indexOf('Mac')>0)
	return this
}

function PlateformeCheck() {
	this.plateforme = "plateforme" + navigator.platform
	this.pc = (this.plateforme.indexOf('Win')>0)
	this.mac = (this.plateforme.indexOf('Mac')>0)
	return this
}

function prechargement(imgObj,imgSrc) {
	if (document.images) {
		eval(imgObj+' = new Image()') ;
		eval(imgObj+'.src = "'+imgSrc+'"') ;
	}
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function ouvrirFenetre(url,w,h) {
	ouvrirFenetre2(url, "width="+w+",height="+h+",scrollbars=1") ;
}
function ouvrirFenetre2(url, options) {
	window.open(url,"_blank",options) ;
}
function redirigerVers(lien) {
	window.open(lien, "_self") ;
}
function redirigerVers1(lien) {
	window.open(lien, "dexnew") ;
}
function imprimer(){
	window.print() ;
	//window.close() ;
}
function rechargerPage() {
	if (initialiser==true) {
		history.go(0);
	}
}

function deplacerCalque() {
	scw = (document.all) ? document.body.offsetWidth : window.innerWidth ;
	decalage = Math.ceil((scw-900)/2) ;
	sch = (document.all) ? document.body.offsetHeight : window.innerHeight ;
	//if (sch<700 && !is.ie) decalage-=15 ;
	//if (is.ns) decalage -= 10 ;
	if (is.ns) decalage += 2 ;
	deplacerCalque2(decalage) ;
	/*if (homepage==true) {
		document.write(ticker.div) ;
		idCalque = "layerIntheworld" ;
		idCalque2 = "NewsTicker0" ;
		if (decalage<=0) decalage=15 ;
		if (is.ns5) {
			if (pf.mac) {
				document.getElementById(idCalque).style.left = decalage + 50 ;
				document.getElementById(idCalque2).style.left = decalage + 255 ;
			} else {
				document.getElementById(idCalque).style.left = decalage + 40 ;
				document.getElementById(idCalque2).style.left = decalage + 245 ;
			}
		} else if (is.ns) {
			document.layers[idCalque].left = decalage + 40 ;
			document.layers[idCalque2].left = decalage + 245 ;
		} else {
			document.all[idCalque].style.left = decalage + 30 ;
			document.all[idCalque2].style.left = decalage + 230 ;
		}
		ticker.activate(true) ;
		onresize = rechargerPage ;
	} else {*/
		onresize = deplacerCalque ;
	//}
}

function deplacerCalque2(decalage) {
	//scw = (document.all) ? document.body.offsetWidth : window.innerWidth ;
	//var decalage = Math.ceil((scw-900)/2) ;
	if (is.ns && pf.mac) decalage += 20 ;
	if (decalage<0) decalage = 15 ;
	for (num=1 ; num<=nbCalques ; num++) {
		idCalque = "calqueMenuDHTML"+num ;
		if (num==numLayerDiscover) posx = decalage+posLayerDiscover ;
		else if (num==numLayerDevDurable) posx = decalage+posLayerDevDurable ;
		else if (num==numLayerNews) posx = decalage+posLayerNews ;
		else if (num==numLayerActio) posx = decalage+posLayerActio ;
		else if (num==numLayerPresse) posx = decalage+posLayerPresse ;
		else if (num==numLayerCarriere) posx = decalage+posLayerCarriere ;
		else if (num==numLayerGouv) posx = decalage+posLayerGouv ;
		/*else if (num>numLayerDiscover && num<numLayerNews) posx = decalage+posLayerDiscover+posLevel1_right ;
		else if (num>numLayerNews && num<numLayerYouare) posx = decalage+posLayerNews+posLevel1_right ;
		else if (num>numLayerYouare && num<numLayerServices) posx = decalage+posLayerYouare-posLevel1_left ;
		else if (num>numLayerServices) posx = decalage+posLayerServices-posLevel1_left ;*/
		else if (num>numLayerDiscover && num<numLayerGouv) posx = decalage+posLayerDiscover+posLevel1_right ;	
		else if (num>numLayerGouv && num<numLayerDevDurable) posx = decalage+posLayerGouv+posLevel1_right ;
		else if (num>numLayerDevDurable && num<numLayerNews) posx = decalage+posLayerDevDurable+posLevel1_right ;
		else if (num>numLayerNews && num<numLayerActio) posx = decalage+posLayerNews+posLevel1_right ;
		else if (num>numLayerActio && num<numLayerPresse) posx = decalage+posLayerActio-posLevel1_left ;
		else if (num>numLayerPresse && num<numLayerCarriere) posx = decalage+posLayerPresse-posLevel1_left ;
		else if (num>numLayerCarriere) posx = decalage+posLayerCarriere-posLevel1_left ;
		
		if(document.getElementById)
		{	
			document.getElementById(idCalque).style.left = (posx - 2000) + 'px';
			
			//alert(posx+' - document.get : '+document.getElementById(idCalque).style.left);
		}
		else
		{
			document.all[idCalque].style.left = posx - 2000 ;
			
			//alert(posx+' - document.all : '+document.all[idCalque].style.left);
		}
		/*if (is.ns5) document.getElementById(idCalque).style.left = posx - 2000 ;
		else if (is.ns) document.getElementById(idCalque).style.left = (posx - 2000) + 'px';
		else document.all[idCalque].style.left = posx - 2000 ;*/
		
		//alert(posx+' '+document.getElementById(idCalque).style.left);
	}
}

function rollOverMenuDHTML(num, flagMontreCalque) {
	if (initialiser) {
		clearTimeout(timeID2) ;
		if (flagMontreCalque) montrerCalque(num) ;
		else cacherTousCalques(0) ;
	}
}




function montrerCalque(num) {
	if (initialiser) {
		if (num!=numLayerDiscover && num!=numLayerNews && num!=numLayerDevDurable && num!=numLayerActio && num!=numLayerPresse && num!=numLayerCarriere && num!=numLayerGouv) {
			if (num>numLayerDiscover && num<numLayerGouv) cacherTousCalques(numLayerDiscover) ;
			else if (num>numLayerGouv && num<numLayerDevDurable) cacherTousCalques(numLayerGouv) ;
			else if (num>numLayerDevDurable && num<numLayerNews) cacherTousCalques(numLayerDevDurable) ;
			else if (num>numLayerNews && num<numLayerActio) cacherTousCalques(numLayerNews) ;
			else if (num>numLayerActio && num<numLayerPresse) cacherTousCalques(numLayerActio) ;
			else if (num>numLayerPresse && num<numLayerCarriere) cacherTousCalques(numLayerPresse) ;
			else if (num>numLayerCarriere) cacherTousCalques(numLayerCarriere) ;
		} else {
			cacherTousCalques(0) ;
		}	
		
		for (i=1 ; i<=nbCalques ; i++) 
		{			
			idCalque = "calqueMenuDHTML"+i ;
			if (is.ns5) objCalque = document.getElementById(idCalque).style ;
			else if (is.ns) objCalque = document.layers[idCalque] ;
			else objCalque = document.all[idCalque].style ;
			
			if (num==i) 
			{			
				if (getValPx(objCalque.left)<0) 
				{
					//alert('tt');
					/*if (is.ns5) {					
						objCalque.visibility = "visible" ;
						objCalque.left = getValPx(objCalque.left) + 2000 ;
						document.getElementById(idCalque).onmouseout = downHandler
						document.getElementById(idCalque).onmouseover = overHandler
					} else if (is.ns) {
						objCalque.visibility = "show" ;
						objCalque.left = getValPx(objCalque.left) + 2000 ;
						document.layers[idCalque].captureEvents(Event.MOUSEDOWN | Event.MOUSEUP | Event.MOUSEMOVE)
						document.layers[idCalque].onmouseout = downHandler
						document.layers[idCalque].onmouseover = overHandler
					} else {					
						objCalque.visibility = "visible" ;
						objCalque.left = getValPx(objCalque.left) + 2000 ;
						document.all[idCalque].onmouseout = downHandler
						document.all[idCalque].onmouseover = overHandler
					}*/
					
					if(document.getElementById)
					{
						objCalque.visibility = "visible" ;
						objCalque.left = (getValPx(objCalque.left) + 2000) +'px';
						document.getElementById(idCalque).onmouseout = downHandler;
						document.getElementById(idCalque).onmouseover = overHandler;
					}
					else
					{
						objCalque.visibility = "visible" ;
						objCalque.left = getValPx(objCalque.left) + 2000 ;
						document.all[idCalque].onmouseout = downHandler
						document.all[idCalque].onmouseover = overHandler
					}
				}
			}
		}
		/*if (homepage==true) {
			idCalque = "layerIntheworld" ;
			if (is.ns5) document.getElementById(idCalque).style.visibility = "hidden" ;
			else if (is.ns) document.layers[idCalque].visibility = "hide" ;
			else document.all[idCalque].style.visibility = "hidden" ;
		}*/
	}
}

function getValPx(valAsStr) {
	if (is.ns4) return valAsStr ;
	posPx = valAsStr.indexOf("px") ;
	if (posPx!=-1) {
		val = parseInt(valAsStr.substring(0,posPx));
		return val ;
	} else {
		return parseInt(valAsStr) ;
	}
}

function cacherTousCalques(sauf) {
	
	for (i=1 ; i<=nbCalques ; i++) {
		idCalque = "calqueMenuDHTML"+i ;
		/*if (is.ns5) objCalque = document.getElementById(idCalque).style ;
		else if (is.ns) objCalque = document.layers[idCalque] ;
		else objCalque = document.all[idCalque].style ;*/
		
		if(document.getElementById)
			objCalque = document.getElementById(idCalque).style ;
		else
			objCalque = document.all[idCalque].style;
			
		if (i!=sauf) {
			/*
			if (is.ns5) document.getElementById(idCalque).style.visibility = "hidden" ;
			else if (is.ns) document.layers[idCalque].visibility = "hide" ;
			else document.all[idCalque].style.visibility = "hidden" ;
			*/
			if (getValPx(objCalque.left)>0) {
				objCalque.left = getValPx(objCalque.left) - 2000 +'px';
			}
		}
	}
	/*if (homepage==true && sauf==0) {
		idCalque = "layerIntheworld" ;
		if (is.ns5) document.getElementById(idCalque).style.visibility = "visible" ;
		else if (is.ns) document.layers[idCalque].visibility = "show" ;
		else document.all[idCalque].style.visibility = "visible" ;
	}*/
}

function rollOverDefautMenuDHTML() {
	if (initialiser) timeID2 = setTimeout("cacherTousCalques(0)",125) ;
}

function downHandler() {
	rollOverDefautMenuDHTML()
}

function overHandler() {
	if (is.ns4) timeID1 = setTimeout("overHandler2()",450) ;
	else overHandler2() ;
}

function overHandler2() {
 	clearTimeout(timeID2) ;
}

function retourHome() {
	window.opener.location = "../home/home.php" ;
	setTimeout("window.close()",2000) ;
}

function AjouterFavoris(bookmarkurl,bookmarktitle,error){
  if (document.all){
        window.external.AddFavorite(bookmarkurl,bookmarktitle);
  }else{
        alert (error);
  }
}

// Menu toujours visible
var stmnLEFT = 0;
var stmnGAP1 = 0;
var stmnGAP2 = 0;
var stmnBASE = 0;
var stmnActivateSpeed = 1;
var stmnScrollSpeed = 1;

var stmnTimer;

function RefreshStaticMenu() {
	var stmnStartPoint, stmnEndPoint, stmnRefreshTimer;

	stmnStartPoint = parseInt(ssMenu.style.top, 10);
	if (document.all)
		stmnEndPoint = document.body.scrollTop + stmnGAP2;
	else
		stmnEndPoint = window.pageYOffset + stmnGAP2;
	if (stmnEndPoint < stmnGAP1) stmnEndPoint = stmnGAP1;

	stmnRefreshTimer = stmnActivateSpeed;

	if ( stmnStartPoint != stmnEndPoint ) {
		stmnScrollAmount = Math.ceil( Math.abs( stmnEndPoint - stmnStartPoint ) / 15 );
		ssMenu.style.top = parseInt(ssMenu.style.top, 10) + ( ( stmnEndPoint<stmnStartPoint ) ? -stmnScrollAmount : stmnScrollAmount );
		stmnRefreshTimer = stmnScrollSpeed;
	}	
	stmnTimer = setTimeout ("RefreshStaticMenu();", stmnRefreshTimer);
}

function InitializeStaticMenu() {
	ssMenu.style.left = stmnLEFT;
	if (document.all)
		ssMenu.style.top = document.body.scrollTop + stmnBASE;
	else
		ssMenu.style.top = window.pageYOffset + stmnBASE;
	//ssMenu.style.top = document.body.scrollTop + document.body.clientHeight - 24 + "px" ;
	RefreshStaticMenu();
}
// Fin menu toujours visible

// D�but gestion des couleurs de style
// Ajout le 10-04-2006
function setColor(idElement,colorCell){
	// TR
	//alert(idElement+':'+colorCell);
	if(document.getElementById(idElement).style.backgroundColor != colorCell){
		document.getElementById(idElement).style.backgroundColor = colorCell;
		document.getElementById(idElement).style.cursor = "pointer";
		document.getElementById(idElement).style.cursor = "hand";
	}
}
// Fin gestion des couleurs de style

// D�but gestion de l'ouverture et fermeture des menus
// Ajout le 22-05-2006
function stateMenus(idElement){
	
 	var b = document.getElementById(idElement) ;
	var c = document.getElementById('c'+idElement) ;
	var d = document.getElementById('img'+idElement) ;
	
	/*if(b){
		b.style.display="none" ;
		b.style.visibility="hidden" ;
		b.className = "sousmenu_ferme";
	}*/

	if(c.value == "") c.value = "none";
	
    if(c.value == "none")
	{
    	/*b.style.display = "block";
    	b.style.visibility = "visible";*/
		b.className = "sousmenu_ouvert";
		c.value = "block";
		d.src = "../../images/templates09/deroulanton.gif";
	}
	else
	{
		/*b.style.display = "none";
    	b.style.visibility = "hidden";*/
		b.className = "sousmenu_ferme";
		c.value = "none";
		d.src = "../../images/templates09/deroulantoff.gif";
	}
	if(document.getElementById('c'+idElement)!=c) document.getElementById('c'+idElement).value = "";
}


function getPageSize()
{				
	var xScroll, yScroll;
				
	if (window.innerHeight && window.scrollMaxY) {	
		xScroll = document.body.scrollWidth;
		yScroll = window.innerHeight + window.scrollMaxY;
	} else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
		xScroll = document.body.scrollWidth;
		yScroll = document.body.scrollHeight;
	} else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
		xScroll = document.body.offsetWidth;
		yScroll = document.body.offsetHeight;
	}
				
	var windowWidth, windowHeight;
	if (self.innerHeight) {	// all except Explorer
		windowWidth = self.innerWidth;
		windowHeight = self.innerHeight;
	} else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
		windowWidth = document.documentElement.clientWidth;
		windowHeight = document.documentElement.clientHeight;
	} else if (document.body) { // other Explorers
		windowWidth = document.body.clientWidth;
		windowHeight = document.body.clientHeight;
	}	
				
	// for small pages with total height less then height of the viewport
	if(yScroll < windowHeight){
		pageHeight = windowHeight;
	} else { 
		pageHeight = yScroll;
	}
			
	// for small pages with total width less then width of the viewport
	if(xScroll < windowWidth){	
		pageWidth = windowWidth;
	} else {
		pageWidth = xScroll;
	}
			
			
	arrayPageSize = new Array(pageWidth,pageHeight,windowWidth,windowHeight) 
	return arrayPageSize;
}

function getScrollPosition()
{
    return Array((document.documentElement && document.documentElement.scrollLeft) || window.pageXOffset || self.pageXOffset || document.body.scrollLeft,(document.documentElement && document.documentElement.scrollTop) || window.pageYOffset || self.pageYOffset || document.body.scrollTop);
}
			
			
function addEvent( obj, type, fn ) {
	 if ( obj.attachEvent ) {
		obj['e'+type+fn] = fn;
		obj[type+fn] = function(){obj['e'+type+fn]( window.event );}
		obj.attachEvent( 'on'+type, obj[type+fn] );
	  } else
		obj.addEventListener( type, fn, false );
}
function display(id){
	var bloc = document.getElementById(id);

	if(bloc.style.display == 'none')
	{
		bloc.style.display = 'block';
		size = getPageSize();			
		document.getElementById(id+'_layer').style.height = size[1]+'px';	
	}
	else
		bloc.style.display = 'none';
		
	
	
	pos = getScrollPosition();
	var top = pos[1] + 100;
	document.getElementById(id+'_inner').style.marginTop = top+'px';
	
				//	document.getElementById('overlay').style.width = size[0]+'px';	
}