<!--
// Site internet VINCI
// Frederic RENARD pour VINCI
// Derniere modification le 03/02/2009

// -----------------------------------
// Fonctions et variables PhpMyVisites
// -----------------------------------
var pmv_site = 2;
var pmv_url = "http://stats-php.vinci.com/phpmyvisites.php";
var pmv_vars = Array();

function plugMoz(pmv_pl) {
	if (pmv_tm.indexOf(pmv_pl) != -1 && (navigator.mimeTypes[pmv_pl].enabledPlugin != null)) return '1';
	else return '0';
}
	
function plugIE( pmv_plug ){
	pmv_find = false;
	document.write('<SCR' + 'IPT LANGUAGE=VBScript>\n on error resume next \n pmv_find = IsObject(CreateObject("' + pmv_plug + '"))</SCR' + 'IPT>\n');
	if (pmv_find) return '1';
	else return '0';
}

var pmv_jav='0'; if(navigator.javaEnabled()) pmv_jav='1';
var pmv_agent = navigator.userAgent.toLowerCase();
var pmv_moz = (navigator.appName.indexOf("Netscape") != -1);
var pmv_ie = (pmv_agent.indexOf("msie") != -1);
var pmv_win = ((pmv_agent.indexOf("win")!=-1) || (pmv_agent.indexOf("32bit")!=-1));
if (!pmv_win || pmv_moz){
	pmv_tm = '';
	for (var i=0; i < navigator.mimeTypes.length; i++) pmv_tm += navigator.mimeTypes[i].type.toLowerCase();
	var pmv_dir = plugMoz("application/x-director");
	var pmv_fla = plugMoz("application/x-shockwave-flash");
	var pmv_pdf = plugMoz("application/pdf");
	var pmv_qt = plugMoz("video/quicktime");
	var pmv_rea = plugMoz("audio/x-pn-realaudio-plugin");
	var pmv_wma = plugMoz("application/x-mplayer2");
} else if (pmv_win && pmv_ie){
	var pmv_dir = plugIE("SWCtl.SWCtl.1");
	var pmv_fla = plugIE("ShockwaveFlash.ShockwaveFlash.1");
	var pmv_pdf = '0'; 
	if (plugIE("PDF.PdfCtrl.1") == '1' || plugIE('PDF.PdfCtrl.5') == '1' || plugIE('PDF.PdfCtrl.6') == '1') pmv_pdf = '1';
	var pmv_qt = plugIE("QuickTimeCheckObject.QuickTimeCheck.1");
	var pmv_rea = plugIE("rmocx.RealPlayer G2 Control.1");
	var pmv_wma = plugIE("MediaPlayer.MediaPlayer.1");
}	

var getvars='';
for (var i in pmv_vars){
	getvars = getvars + '&a_vars['+ escape(i) + ']' + "=" + escape(pmv_vars[i]);
}

// --------------------------------
// Fonction de nettoyage des URLs 
// --------------------------------
function obtenirPage()
{  
  var chaine1 = " "+document.location;

  //1 cas des reecritures d'URL pour les agents : reatblir l'URL d'origine
		if (chaine1.indexOf("/pages/") != -1) {
			var reg4 = new RegExp(".htm","g")
			chaine1 = chaine1.replace(reg4,"") ;
			var reg5 = new RegExp("/pages/","g")
			chaine1 = chaine1.replace(reg5,".htm?OpenAgent&") ;
			}
			
  //2 recuperer dernier element a droite du dernier "/"			
  var reg1 = new RegExp("/","g");
  var tableau1 = chaine1.split(reg1);
  var chaine2 = tableau1[tableau1.length-1]+" ";			
		
  //3 ancres : recuperer chaine a gauche du "#"		
  var reg2 = new RegExp("#","g");
  var tableau2 = chaine2.split(reg2);		
  var chaine3 = tableau2[0]+" ";			
		
  //4 agents : recuperer chaine a gauche du "?"
  var reg3 = new RegExp("\\?","g");
  var tableau3 = chaine3.split(reg3);
  return tableau3[0];		
}

// --------------------------------------------------
// Fonctions de stats PhpMyVisites
// --------------------------------------------------
// ancienne version
function stats( langue, idpg, pmv )
{         
 if (pmv == "") pmv_pname = obtenirPage();
 else pmv_pname = pmv;
 pmv_do = document; 
 pmv_da = new Date();
 try { rtu = top.pmv_do.referrer; } catch(e) { rtu = pmv_do.referrer }
 src = pmv_url;
 src += '?url='+escape(pmv_do.location)+'&pagename='+escape(pmv_pname)+getvars;
 src += '&id='+pmv_site+'&res='+screen.width+'x'+screen.height+'&col='+screen.colorDepth;
 src += '&h='+pmv_da.getHours()+'&m='+pmv_da.getMinutes()+'&s='+pmv_da.getSeconds();
 src += '&flash='+pmv_fla+'&director='+pmv_dir+'&quicktime='+pmv_qt+'&realplayer='+pmv_rea;
 src += '&pdf='+pmv_pdf+'&windowsmedia='+pmv_wma+'&java='+pmv_jav+'&ref='+escape(rtu);
 imagePMV = new Image;
 imagePMV.src = src;
}
function pmv(page)
{         
 if (page == "") pmv_pname = obtenirPage();
 else pmv_pname = page;
 pmv_do = document; 
 pmv_da = new Date();
 try { rtu = top.pmv_do.referrer; } catch(e) { rtu = pmv_do.referrer }
 src = pmv_url;
 src += '?url='+escape(pmv_do.location)+'&pagename='+escape(pmv_pname)+getvars;
 src += '&id='+pmv_site+'&res='+screen.width+'x'+screen.height+'&col='+screen.colorDepth;
 src += '&h='+pmv_da.getHours()+'&m='+pmv_da.getMinutes()+'&s='+pmv_da.getSeconds();
 src += '&flash='+pmv_fla+'&director='+pmv_dir+'&quicktime='+pmv_qt+'&realplayer='+pmv_rea;
 src += '&pdf='+pmv_pdf+'&windowsmedia='+pmv_wma+'&java='+pmv_jav+'&ref='+escape(rtu);
 imagePMV = new Image;
 imagePMV.src = src;
}

// groupement stats PhpMyVisites & Google Analytics
function statistiques(pageHTML)
{ 
	if (pageHTML == "") pageHTML = obtenirPage();
	// stats PhpMyVisites
 pmv(pageHTML);
	// stats GoogleAnalytics
	var pageTracker = _gat._getTracker("UA-1364966-1");
 pageTracker._initData();
 pageTracker._trackPageview(pageHTML);
}

// Masquer les adresses e-mail
function malle (qui,ind) {
 var sct = "vinci" ;
 if (ind=="2" ) sct = "vincipark" ;
 var dmn = "com";
 var adr = qui+'&#64;'+sct+'&#46;'+dmn ;
 document.write ( '<a href="&#109;&#97;&#105;&#108;&#116;&#111;&#58;'+adr+'">'+adr+'</a>' );
}

// Fenetre centree dimensionnee
function FCD(url,largeur,hauteur) {
 var fn=0;
 var wP=",status=no,menubar=no,toolbar=no,resizable=yes,location=no,hotkeys=no,directories=no";
 ScrX = (screen.availWidth - largeur) / 2;
 ScrY = (screen.availHeight - hauteur) / 2;
 param = "left=" + ScrX + ",top=" + ScrY + ",width=" + largeur + ",height=" + hauteur + wP;
 fn=window.open(url,'VINCI',param);
 fn.resizeTo(largeur,hauteur);
 fn.moveTo(ScrX,ScrY);
} 

-->