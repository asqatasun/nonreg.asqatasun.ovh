	var xhr = null; 
	var zhr = null; 
	var yhr = null; 
		
	function getXhr(){
		if(window.XMLHttpRequest){ // Firefox et autres
		   xhr = new XMLHttpRequest(); 
		}
		else if(window.ActiveXObject){ // Internet Explorer 
			   try {
						xhr = new ActiveXObject("Msxml2.XMLHTTP");
					} catch (e) {
						xhr = new ActiveXObject("Microsoft.XMLHTTP");
					}
		}
		else {
			   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
			   xhr = false; 
		}
	}
	
	function getZhr(){
		if(window.XMLHttpRequest){ // Firefox et autres
		   zhr = new XMLHttpRequest(); 
		}
		else if(window.ActiveXObject){ // Internet Explorer 
			   try {
						zhr = new ActiveXObject("Msxml2.XMLHTTP");
					} catch (e) {
						zhr = new ActiveXObject("Microsoft.XMLHTTP");
					}
		}
		else {
			   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
			   zhr = false; 
		}
	}
	
	function getYhr(){
		if(window.XMLHttpRequest){ // Firefox et autres
		   yhr = new XMLHttpRequest(); 
		}
		else if(window.ActiveXObject){ // Internet Explorer 
			   try {
						yhr = new ActiveXObject("Msxml2.XMLHTTP");
					} catch (e) {
						yhr = new ActiveXObject("Microsoft.XMLHTTP");
					}
		}
		else {
			   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
			   yhr = false; 
		}
	}
	
	function displayXML(url) {
		getXhr();
		xhr.onreadystatechange = function(){
				if(xhr.readyState == 4 && xhr.status == 200){
					xmlDoc = xhr.responseText;
					constructStock(xmlDoc);
				}
		}
		xhr.open("GET",url,true);
		xhr.send(null);
	}
	
	function constructStock(xmlDoc){
		output="";
		
		startQuote = xmlDoc.indexOf('<Quote>');
		endQuote = xmlDoc.indexOf('</Quote>');
		quoteTab = (xmlDoc.substring(startQuote+7,endQuote)).split('.');
		quoteTab[1].substring(0,2);
		quoteValue = quoteTab.join('.');
		
		startChange = xmlDoc.indexOf('<Change>');
		endChange = xmlDoc.indexOf('</Change>');
		changeValue = xmlDoc.substring(startChange+8,endChange);	
		
		pctChangeStart =  xmlDoc.indexOf('<PctChange>');
		pctChangeEnd =  xmlDoc.indexOf('</PctChange>');
		pctChangeValue = xmlDoc.substring(pctChangeStart+11,pctChangeEnd);
		
		
		if (changeValue < 0){
			image = 'down';
			color = '#cc0000';
		}else{
			image = 'up';
			color = '#009900';
		}
		
		output = '&nbsp;&nbsp;&nbsp;';
		output += '&euro;<big><b>'+quoteValue+'</b></big>&nbsp;&nbsp;';
		output += '<img width="10" height="14" border="0" src="images/'+image+'_r.gif" alt="'+pctChangeValue+'%">';
		output += '&nbsp;<b style="color:'+color+';">'+changeValue+'</b>';
		output += '&nbsp;<b style="color:'+color+';"> ('+pctChangeValue+'%)</b>&nbsp;&nbsp;<br/>';
		
		document.getElementById('marketShare').innerHTML = output;
	}
	
	function displayXML1(url,count) {
		getZhr();
		zhr.onreadystatechange = function(){
				if(zhr.readyState == 4 && zhr.status == 200){
					xmlDoc = zhr.responseText;
					display_news(xmlDoc,count);
				}
		}
		zhr.open("GET",url,true);
		zhr.send(null);
	}

	function displayXML2(url,count) {
		getYhr();
		yhr.onreadystatechange = function(){
				if(yhr.readyState == 4 && yhr.status == 200){
					ymlDoc = yhr.responseText;
					display_news(ymlDoc,count);
				}
		}
		yhr.open("GET",url,true);
		yhr.send(null);
	}
	
	function display_news(xmlDoc,time){
		setTimeout(
			function(){
				out ="";
				titleStart = xmlDoc.indexOf('<Title>');
				titleEnd = xmlDoc.indexOf('</Title>');
				title = xmlDoc.substring(titleStart+7,titleEnd);
				dateStart = xmlDoc.indexOf('<Date');
				dateEnd = xmlDoc.indexOf('</Date>');
				date = xmlDoc.substring(dateStart+38,dateEnd);
				date_version = date.split('/');
				date = date_version[1]+"/"+date_version[0]+"/"+date_version[2];
				
				release = xmlDoc.split('ReleaseID="');
				id = release[1].split('"');
				releaseId = id[0];
				
				company = xmlDoc.split('CorpMasterID="');
				idcomp = company[1].split('"');
				companyId = idcomp[0];
				
				url = "http://phx.corporate-ir.net/phoenix.zhtml?c="+companyId+"&p=irol-newsArticle&ID="+releaseId;
				
				out +="<span>"+date+"</span><a href=\""+url+"\">"+title+"</a>";
				document.getElementById('news'+time).innerHTML = out;
			},1000
		);	
	}
	
	
