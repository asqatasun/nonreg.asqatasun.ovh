   var Try = {
      these: function() {
         var returnValue;
         for (var i = 0; i < arguments.length; i++) {
            var lambda = arguments[i];
            try {
               returnValue = lambda();
               break;
            } catch (e) {}
         }
         return returnValue;
      }
   }

   function getTransport() {
      return Try.these(
         function() {return new XMLHttpRequest()},
         function() {return new ActiveXObject('Msxml2.XMLHTTP')},
         function() {return new ActiveXObject('Microsoft.XMLHTTP')}
      ) || false;
   }


   function ObjectDiv( div , content ) {
      content= new String(content,"iso-8859-1");
      if ( document.getElementById )
      {
	  //document.getElementById( div ).innerHTML = "";
         //document.getElementById( div ).appendChild(document.createTextNode(content));
	  document.getElementById( div ).innerHTML = content;
         element = document.getElementById(div);
   try{
      var l=element.getElementsByTagName('script').length;
      for(var j=0;j<l;j++){
         var script = document.createElement('script');
         script.type = 'text/javascript';
         script.text = element.getElementsByTagName('script').item(j).text;
         //document.getElementsByTagName('head')[0].appendChild(script);           
		 }           
   }catch(e){
	   alert(e);
   }



      }
      else
      {
         if ( document.layers )
         {
            document.div.innerHTML = content;
         }
         else
         {
            document.all.div.innerHTML = content;
         }
      }
	  checkSizeTxt();
   }

   function Goto( FILE , METHOD , DATA , div ) {
      if( METHOD == 'GET' && DATA != null )
      {
         FILE += '?' + DATA;
         DATA = null;
      }
	  FILE += "&emailcloak=off";
      httpRequestM = getTransport();
      httpRequestM.open( METHOD , FILE , true );

      httpRequestM.onreadystatechange = function() {
         if( httpRequestM.readyState == 4 && httpRequestM.status==200)
         {
           retour=httpRequestM.responseText;
           ObjectDiv( div , retour );
           var my_lang = FILE.match('lang=fr');
	           if (my_lang){
		           jQuery('#contentWrapper p').each(function(){
		           		jQuery(this).jTruncate();
		       		});
	           }
		       else{
		        	jQuery('#contentWrapper p').each(function(){
		           		jQuery(this).jTruncate({moreText: "Read more",lessText: " Hide"});
		       		});
		       }
           if(popinVideos){
           	popinVideos.init();
           }
         }
      }

      if( METHOD  == 'POST' )
      {
         httpRequestM.setRequestHeader( "Content-type" , "application/x-www-form-urlencoded" );
      }

      httpRequestM.send( DATA );
  }

  
   function ViewContent( div , href , method , data ) {
      var wait = "<div align='center' id='contentWrapper'>" +
                   "</div>";
      ObjectDiv( div , wait );
      Goto( href , method , data , div );
      return false;
   }
