var popinVideos = {
	init: function(){
		if(jQuery("a.popinVideo").length){
			jQuery('body').prepend('<div id="conteneurPopinVideo"><div id="cntClosePopin"><a href="#" id="fermerVideo"><img src="/images/bt-fermer-popin.jpg" alt="X" /></a></div><div id="contenuVideo"></div></div>');
			jQuery('body').prepend('<div class="overlay"></div>');
			
			jQuery("a.popinVideo").click(function(){
				popinVideos.afficherPopinVideo(jQuery(this).attr('rel'), jQuery(this).attr('lang'));
				return false;
			});
			jQuery("a#fermerVideo").click(function(){
				popinVideos.fermerVideo();
				return false;
			});	
			// Centrage de la video
			jQuery(window).resize(function(){
				popinVideos.centrerVideo();
			});
		}
	},
	afficherPopinVideo: function(idPopin, isoCode){
		jQuery.get('/popinVideo.php',{id: idPopin, lang:isoCode}, popinVideos.onLoadVideo);
		jQuery(".overlay").show();
		if(jQuery('embed').length){
			jQuery('embed').hide();
		}
	},
	onLoadVideo: function(cnt){
		jQuery("#conteneurPopinVideo #contenuVideo").html(cnt);
		jQuery("#conteneurPopinVideo").show();
		jQuery("#cntClosePopin").css('width', jQuery("#conteneurPopinVideo").width());
		popinVideos.centrerVideo();
	},
	centrerVideo: function(){
		var gauche = (jQuery(document).width() - jQuery("#conteneurPopinVideo").width()) / 2;
		var haut = (jQuery(window).height() - jQuery("#conteneurPopinVideo").height()) / 2;
		var pos = popinVideos.positionAscenseur();
		
		jQuery("#conteneurPopinVideo").css({top: (haut + pos[1])+"px", left: gauche+"px"});
		jQuery(".overlay").css({height: jQuery(document).height()+"px"});
	},
	fermerVideo: function(){
		jQuery("#conteneurPopinVideo, .overlay").hide();
		jQuery("#conteneurPopinVideo #contenuVideo").html("");
		jQuery('embed').show();
	},
	positionAscenseur: function(){
		var scrOfX = 0, scrOfY = 0;
		if( typeof( window.pageYOffset ) == 'number' ) {
			//Netscape compliant
			scrOfY = window.pageYOffset;
			scrOfX = window.pageXOffset;
		} else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
			//DOM compliant
			scrOfY = document.body.scrollTop;
			scrOfX = document.body.scrollLeft;
		} else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
			//IE6 standards compliant mode
			scrOfY = document.documentElement.scrollTop;
			scrOfX = document.documentElement.scrollLeft;
		}
		return [ scrOfX, scrOfY ];
	}
};
jQuery(document).ready(popinVideos.init);


var nbOpenPopin = 0;
function afficherPopinVideoWord(videoId, lang) {
	//alert("Open video :: ("+videoId+", "+lang+")");
	if ( nbOpenPopin < 1){
		if(jQuery("#words_danone").length){
				jQuery('body').prepend('<div id="conteneurPopinVideo"><div id="cntClosePopin"><a href="#" id="fermerVideo"><img src="/images/bt-fermer-popin.jpg" alt="X" /></a></div><div id="contenuVideo"></div></div>');
				jQuery('body').prepend('<div class="overlay"></div>');
				
				jQuery("a#fermerVideo").click(function(){
					popinVideos.fermerVideo();
					return false;
				});	
				// Centrage de la video
				jQuery(window).resize(function(){
					popinVideos.centrerVideo();
				});
		}
		popinVideos.afficherPopinVideo(videoId,lang);
	}else{
		popinVideos.afficherPopinVideo(videoId,lang);
	}
}