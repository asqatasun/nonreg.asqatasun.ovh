/* Disable space bar pagedown for firefox */
var focused = false;
document.onkeypress = function process_keypress(event) {
            if (navigator.appName != "Microsoft Internet Explorer") {
                        if (!focused) {
                                   if (typeof(event.which) != 'undefined') {
                                               if (event.which == 32) {
                                                           return false;
                                               }
                                   }
                        }
            }
}


function clearSearchInput(){
	var inputSearch = $('#searchField')[0] || $('#mot')[0];
	inputSearch.onfocus = function() {
		if(this.value == this.defaultValue) {this.value = '';}
		focused = true;
	};
	
	inputSearch.onblur = function() {
		if(/^\s*$/.test(this.value)) {this.value = this.defaultValue;}
		focused=false;
	};
}

var rollOvers = {
	init: function() {
		if(document.documentElement.id != "home"){return};
		var subs = $('div.subLevel');
		for(var i = 0, sub; sub = subs[i]; i++) {
			
			var p = sub.parentNode;
			var verif = p.parentNode;
			sub.style.left = '-10000px';
			if(verif.id == 'navigationList') {
				sub.style.top = '40px';
				} else if(verif.id == 'footerNav') {
					sub.style.height = sub.offsetHeight + 3 + 'px';
					sub.style.top = - sub.offsetHeight + 'px';
					//sub.style.top = '40px';
				}
			p.onmouseover = rollOvers.open;
			p.onmouseout = rollOvers.close;
			
		}

	},
	
	open: function() {
		var sub = $('div', [this])[0];
		sub.style.left = Math.round(this.offsetWidth / 2 - 56) + 'px';
	},
	
	close: function(sub) {
		var sub = $('div', [this])[0];
		sub.style.left = '-10000px';
	}
};

var oldOpenedMenu;
function showHideMenu(ssmenu) {
	
	var insert = '<a href="#" id="close2"><img src="/templates/common/img/btnFermer2.gif" alt="fermer" /></a>';
	insert += '<span id="start"></span>';
	insert += '<ul id="profileUl" style="overflow:visible;">' + ssmenu.innerHTML + '</ul>';
	insert += '<span id="end"></span>';
	
	var newProfileNav;
	var module=document.getElementById('module');
	var profileNavOver=document.createElement('div');
	profileNavOver.setAttribute('id','profileNavOver');
	var profileName = $N.prev(ssmenu).innerHTML;
	
	if (oldOpenedMenu) {
		oldOpenedMenu.parentNode.removeChild(oldOpenedMenu);
		oldOpenedMenu = null;
	}
	profileNavOver.innerHTML='<h3>' + profileName + '</h3>' + insert;
	module.appendChild(profileNavOver);
	oldOpenedMenu = profileNavOver;
	closeProfile();
 }

function prepareMenuNav() {
	var monProfil	= (document.getElementById("profileNav"));
	if (!monProfil) {
		window.setTimeout(prepareMenuNav, 10);
	} else {
		$N.addClass(document.body,'js');
		var liLinks = monProfil.getElementsByTagName("li");
		for (var i=0; i<liLinks.length; i++ ) {
			links = liLinks[i].firstChild;
			if (links.className !== 'level1') continue;
			level2 = liLinks[i].getElementsByTagName("ul")[0];
			level2.style.display = "none";
			
			liLinks[i].firstChild.onclick = function() {
				var ssmenu = $N.next(this);
				showHideMenu(ssmenu);return false;
			}
		}
	}
}

function closeProfile(){
	var closeBtn = document.getElementById('close2');
	if (!document.getElementById('close2')) {
		return;
	} else {
		closeBtn.onclick=function(){
			var profileNavOver=document.getElementById('profileNavOver');
			profileNavOver.parentNode.removeChild(profileNavOver);
			oldOpenedMenu = null;
			return false;
		};
	}
};
$E.load(function () {window.setTimeout(clearSearchInput, 1000)});
$E.load(function () {window.setTimeout(rollOvers.init, 1000)});
$E.load(function () {window.setTimeout(prepareMenuNav, 1000)});

