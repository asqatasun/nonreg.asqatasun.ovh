function MM_showHideLayers() { //v3.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;

  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
    obj.visibility=v;
    }
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_jumpMenu(targ,selObj,restore)
{
  var val = selObj.options[selObj.selectedIndex].value;
  var modele = /implantations/;
  var page   = /actualite/;
  if (modele.test(val) || page.test(val) ) { eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'"); }
  else                                     { window.open(val); }
  if (restore) selObj.selectedIndex=0;
}


function PozMilieu(Pwidth,Pheight)
{
  var x = screen.width;
  var y = screen.height;
  var T = (x-Pwidth)/2;
  var I = (y-Pheight)/2;
  window.parent.moveTo(T,I);
}

function writeObject(StrObj, contenu){
 /* if (document.layers) {
    document.layers[StrObj].document.write(contenu);
    document.layers[StrObj].document.close();
  }
  if (document.all) {
    eval(StrObj).innerHTML=contenu;
  }
  //modif NS6 : celui-ci ne supporte plus document.layers mais document.getElementById
*/
  if (document.getElementById) {
    document.getElementById(StrObj).innerHTML=contenu;

  }
}

function validSelectListe(objSelect) {

  if (objSelect.value=='') {
    return false;
  }
  else return true;
}

function goSelectListe(objSelect) {

  if (objSelect.value!='') {
    document.location= '/go.asp?Url=' + objSelect.value;
  }
}

function goContinentListe(objSelect,goUrl) {

  if (objSelect.value!='') {
    document.location= goUrl +'?Continent=' + objSelect.value;
  }
}

function ChangeTarget(formulaire){
	if (formulaire.Url.value.substr(0,4)=="http")
		formulaire.target = "_blank";
	else 	formulaire.target = "";
}

//Utilis� pour la page des flux RSS
function changeBackground(id)
{
	object = document.getElementById(id);
	//alert(object.className);

	if (object.className=="FluxNonActif")
	{
		object.className="FluxActif";
	}
	else
	{
		object.className="FluxNonActif";
	}
	
	//document.write(object.className);	
}


//Utilis� pour la page des flux RSS
function changeBackground1(id)
{
	object = document.getElementById(id);
	//alert(object.className);

	if (object.className=="FluxNonActif1")
	{
		object.className="FluxActif1";
	}
	else
	{
		object.className="FluxNonActif1";
	}
	
	//document.write(object.className);	
}

//Utilis� pour le contr�le du formulaire de recherche
function launchSearch(lang,idInput){
  var message="";
  var valeurInterdites = new Array ("Recherche","Search");
  var valueOfReturn = true;
  switch(lang.toUpperCase()){
    case 'FR' :
      message="Entrer le(s) mot(s) que vous voulez rechercher";
      break;
    case 'EN' :
      message="Please type the word(s) you wish to search for";
      break;
  }
  if(document.getElementById(idInput).value!=""){
    for(var i=0; i < valeurInterdites.length && valueOfReturn==true ; i++)
    {
      if(valeurInterdites[i]==document.getElementById(idInput).value){
        valueOfReturn=false;
      }
    }
  } else {
    valueOfReturn=false;
  }
    
  if(valueOfReturn==false){
    alert(message);
  }
  
  return valueOfReturn;
}

