function Show(Idt,current,num,NameClass)
{
  if (num!=1)
  {
    for (i=1;i<=num;i++)
    {
      Object = document.getElementById('div_' + Idt + i);
      LnkObj = document.getElementById('lnk_' + Idt + i);
      if (current!=i)
      {
        Object.style.display = "none";
        Object.className = NameClass;
        LnkObj.className = NameClass + 'P';
      }
      else
      {
        Object.style.display = "block";
        Object.className = NameClass + '2';
        LnkObj.className = NameClass + 'P2';
      }
    }
  }
  else
  {
    Object = document.getElementById('div_' + Idt);
    if (Object.style.display=="none")
    {
      Object.style.display="block";
    }
    else
    {
      Object.style.display = "none";
    }
  }
}