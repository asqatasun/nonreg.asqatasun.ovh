/* DEBUT calendar.js */
if (sLangSite =="fr") {
	var monthInYear = new Array("janvier", "f&eacute;vrier", "mars", "avril", "mai", "juin", "juillet", "ao&ucirc;t", "septembre", "octobre", "novembre", "d&eacute;cembre");
}
else {
	var monthInYear = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
}

if (sLangSite =="fr") {
	var dayInWeek = new Array("Lu", "Ma", "Me", "Je", "Ve", "Sa","Di");
}
else {
	var dayInWeek = new Array("Mo", "Tu", "We", "Th", "Fr", "Sa", "Su");
}

//var calendar = new Calendar();

function Calendar() {
	//console.log(calendar)
//	initFirst();
}

function initFirst() {
	calendar.elementId = "";
	calendar.element = null;

	calendar.weekday = 0;
	calendar.day = 0;
	calendar.month = 0;
	calendar.year = 0;

	calendar.cday = 0;
	calendar.cmonth = 0;
	calendar.cyear = 0;
	calendar.cyear2 = 0;

	calendar.mappingArray = new Array();

	calendar.cal = document.getElementById("cal");
	calendar.cald = document.getElementById("cald");
	calendar.calm = document.getElementById("calm");
	calendar.caly = document.getElementById("caly");
}

function initCalendar(date) {
	calendar.weekday = date.getDay();
	calendar.day = date.getDate();
	calendar.month = date.getMonth() + 1;
	calendar.year = date.getFullYear();

	calendar.cmonth = calendar.month;
	calendar.cyear = calendar.year;

	fillDayChoice();
	fillMonthChoice();
	fillYearChoice();
}

function initElement(id, evt) {
	if (!evt) {
		evt = window.event;
	}

	initFirst();

	calendar.elementId = "";
	calendar.element = document.getElementById(id);

	var elementStyle = calendar.cal.style;
	var clientX = 100;
	var clientY = 100;
	elementStyle.left = document.body.scrollLeft + clientX;
	elementStyle.top = document.body.scrollTop + clientY;
	
	calendar.calm.style.left = parseInt(elementStyle.left) + 6;
	calendar.calm.style.top = parseInt(elementStyle.top) + 28;
	calendar.caly.style.left = parseInt(elementStyle.left) + 12;
	calendar.caly.style.top = parseInt(elementStyle.top) + 33;
}

function displayCalendar(id, ev) {
	calendar = new Calendar();

	var i;
	var element;
	
	element = document.getElementById("cal_y_02");
	
	initElement(id);
	
	var dateValue = calendar.element.value;
	var validDate = false;
	if (dateValue != "") {
		var s = dateValue.split("/");
		var d;
		var m;
		var y;
		if (s.length == 3) {
			d = s[0];
			m = s[1];
			y = s[2];
			if (!isNaN(d) && !isNaN(m) && !isNaN(y)) {
			  var m2 = parseNumber(m);
			  if (m2 >= 1 && m2 <= 12) {
			    validDate = true;
					var y2 = parseNumber(y);
					if (y2 < 100) {
						y2 += 2000;
					}
			    var d2 = parseNumber(d);
			    var testDate = new Date(y2, m2 - 1, 1);
			    if (d2 >= 1 && d2 <= getMaxMonthLastDay(testDate)) {
            initCalendar(new Date(y2, m2 - 1, d2));
          } else {
            initCalendar(new Date(y2, m2 - 1, 1));
          }
				}
			}
		}
	}
	if (!validDate) {
		initCalendar(new Date());
	}
	fillCalendar();
	showCalendar();
}

function showCalendar() {
	//calendar.cal.style.visibility = "visible";
	calendar.cal.style.display = "block";
	showDayChoice();
	hideMonthChoice();
	hideYearChoice();
}

function hideCalendar() {
	//calendar.cal.style.visibility = "visible";
	calendar.cal.style.display = "block";
	

}

function showDayChoice() {
	//calendar.cald.style.visibility = "visible";
	calendar.cald.style.display = "block";
}

function hideDayChoice() {
	//calendar.cald.style.visibility = "hidden";
	calendar.cald.style.display = "block";
}

function showMonthChoice() {
	//calendar.calm.style.visibility = "visible";
	calendar.calm.style.display = "block";
}

function hideMonthChoice() {
	//calendar.calm.style.visibility = "hidden";
	calendar.calm.style.display = "none";
}

function showYearChoice() {
	//calendar.caly.style.visibility = "visible";
	calendar.caly.style.display = "block";
}

function hideYearChoice() {
	//calendar.caly.style.visibility = "hidden";
	calendar.caly.style.display = "none";
}

function updateMonthChoiceVisibility() {
	//if (calendar.calm.style.visibility == "visible") {
	if (calendar.calm.style.display == "block") {
		showDayChoice();
		hideMonthChoice();
	} else {
		hideDayChoice();
		hideYearChoice();
		showMonthChoice();
	}
}

function updateYearChoiceVisibility() {
	//if (calendar.caly.style.visibility == "visible") {
	if (calendar.caly.style.display == "block") {
		showDayChoice();
		hideYearChoice();
	} else {
		calendar.cyear2 = calendar.cyear;
		hideDayChoice();
		hideMonthChoice();
		fillYearChoice();
		showYearChoice();
	}
}

function fillCalendar() {
	document.getElementById("cal_m_02").innerHTML = getStringMonth(calendar.cmonth);
	document.getElementById("cal_y_02").innerHTML = calendar.cyear;
	var element;

	var i = 0;
	var j = 0;
	for (i = 0; i < 6; i++) {
		for (j = 0; j < 7; j++) {
			element = document.getElementById("cal_d_" + i + j);
			element.innerHTML = "";
			element.onclick = "";
			element.onmouseover = "";
			element.onmouseout = "";
		}
	}
	i = 0;
	j = (new Date(calendar.cyear, calendar.cmonth-1, 1)).getDay();
	j = (j + 6) % 7;
	var count = 1;
	var days = getMonthLastDay();
	while (count <= days) {
		element = document.getElementById("cal_d_" + i + j);
		element.innerHTML = count;
		element.onclick = selectDay;
		element.onmouseover = highlightDay;
		element.onmouseout = unhighlightDay;
		calendar.mappingArray[10 * i + j] = count;
		j++;
		if (j == 7) {
			i++;
			j = 0;
		}
		count++;
	}
}

function fillMonthChoice() {
	var i;
	var element;
	for (i = 1; i <= 12; i++) {
		element = document.getElementById("calm_" + i);
		element.innerHTML = getStringMonth(i);
		element.onclick = selectMonth;
		element.onmouseover = highlightHead;
		element.onmouseout = unhighlightHead;
	}
}

function fillDayChoice() {
	var i;
	var element;
	for (i = 0; i <= 6; i++) {
		element = document.getElementById("cald_" + i);
		element.innerHTML = getStringWeekDay(i);
	}
}


function fillYearChoice() {
	var i;
	var element;
	var gap = calendar.cyear2 - 11;
	for (i = 1; i <= 25; i++) {
		element = document.getElementById("caly_" + i);
		element.innerHTML = gap + i;
		element.onclick = selectYear;
		element.onmouseover = highlightHead;
		element.onmouseout = unhighlightHead;
		//alert(calendar.month);		
	}
}

function highlightDay(evt) {
	//getEventElement(evt).className = "sel";
}

function unhighlightDay(evt) {
	//getEventElement(evt).className = "";
}

function highlightHead(evt) {
	var element = getEventElement(evt);
	if (element.tagName == "IMG") {
		element = element.parentNode;
	}
}

function unhighlightHead(evt) {
	var element = getEventElement(evt);
	if (element.tagName == "IMG") {
		element = element.parentNode;
	}
	element.className = "";
}

function selectDay(evt) {
	var id = getEventElement(evt).id;
	calendar.cday = calendar.mappingArray[parseInt(id.substr(id.length - 2))];
	hideCalendar();
	var d = calendar.cday;
	var m = calendar.cmonth;
	var y = calendar.cyear;
	window.status = d + " - " + m + " - " + y;
	calendar.element.value = (d < 10 ? "0" : "") + d + "/" + (m < 10 ? "0" : "") + m + "/" + y;
}

function selectMonth(evt) {
	var id = getEventElement(evt).id;
	var s = id.split("_");
	calendar.cmonth = parseInt(s[s.length - 1]);
	hideMonthChoice();
	showDayChoice();
	fillCalendar();
}

function selectYear(evt) {
	var id = getEventElement(evt).id;
	var s = id.split("_");
	calendar.cyear = parseInt(s[s.length - 1]) + calendar.cyear2 - 11;
	hideYearChoice();
	showDayChoice();
	fillCalendar();
}

function incrementMonth() {
	if (calendar.cmonth == 12) {
		calendar.cmonth = 1;
		calendar.cyear = calendar.cyear + 1;
	} else {
		calendar.cmonth = calendar.cmonth + 1;
	}
	fillCalendar();
}

function decrementMonth() {
	if (calendar.cmonth == 1) {
		calendar.cmonth = 12;
		calendar.cyear = calendar.cyear - 1;
	} else {
		calendar.cmonth = calendar.cmonth - 1;
	}
	fillCalendar();
}

function incrementYear() {
	calendar.cyear = calendar.cyear + 1;
	fillCalendar();
	
}

function decrementYear() {
	calendar.cyear = calendar.cyear - 1;
	fillCalendar();
}

function increaseYear() {
	calendar.cyear2 = calendar.cyear2 + 20;
	fillYearChoice();
}

function decreaseYear() {
	calendar.cyear2 = calendar.cyear2 - 20;
	fillYearChoice();
}

function getMonthLastDay() {
	var testDate = new Date(calendar.cyear, calendar.cmonth-1, 29);
  return getMaxMonthLastDay(testDate);
}

function getMaxMonthLastDay(date) {
	if (date.getDate() == 29) {
		date.setDate(30);
		if (date.getDate() == 30) {
			date.setDate(31);
			if (date.getDate() == 31) {
				return 31
			} else {
				return 30;
			}
		} else {
			return 29;
		}
	} else {
		return 28;
	}
}

function getStringWeekDay(index) {
	return dayInWeek[index];
}

function getStringMonth(index) {
	return monthInYear[index - 1];
}

var checkClickEnabled = true;
var clickedElement = null;

function checkClick(evt) {
	if ((checkClickEnabled)
			&& (calendar.cal != null)
			//&& (calendar.cal.style.visibility == "visible")) {
			&& (calendar.cal.style.display == "block")) {
		checkClickEnabled = false;
		if (evt.target != null) {
			clickedElement = evt.target;
		} else {
			clickedElement = window.event.srcElement;
		}
		setTimeout("checkClickAnalysis()", 200);
	}
}

function checkClickAnalysis() {
	var element = clickedElement;
	var calendarElement = false;
	var id;
	var tagName = element.tagName;
	// V�rification que le clic courant n'est pas relatif � une image dont le r�le est justement
	// d'afficher le calendrier.
	if (tagName == "IMG") {
		var imgOnClickFunction = "" + element.onclick;
		calendarElement = (imgOnClickFunction.indexOf("displayCalendar") != -1);
	}
	// V�rification de l'appartenance ou non de l'�l�ment ayant d�clench� l'�v�nement 'onclick' au
	// calendrier.
	if (!calendarElement) {
		while (element != null && tagName != "FORM") {
			id = element.id;
			if (id != null && (id == "cal" || id == "calm" || id == "caly") && tagName == "DIV") {
				// L'�l�ment ayant d�clench� l'�v�nement 'onclick' appartient au calendrier : on ne
				// doit donc pas masquer ce dernier.
				calendarElement = true;
			}
			element = element.parentNode;
			if (element != null) {
				tagName = element.tagName;
			}
		}
		if (!calendarElement) {
			hideCalendar();
		}
	}
	checkClickEnabled = true;
}

function getEventElement(evt) {
	if (!evt) {
		return window.event.srcElement;
	} else {
		return evt.target;
	}
}

function parseNumber(n) {
  while (n != "" && n.substring(0, 1) == "0") {
    n = n.substring(1);
  }
  return parseInt(n);
}

/* FIN calendar.js */
/* DEBUT Observer.js */
/**
 * Observer - Observe formelements for changes
 *
 * @version		1.0rc1
 *
 * @license		MIT-style license
 * @author		Harald Kirschner <mail [at] digitarald.de>
 * @copyright	Author
 */
var Observer = new Class({

	options: {
		periodical: false,
		delay: 1000
	},

	initialize: function(el, onFired, options){
		this.setOptions(options);
		this.addEvent('onFired', onFired);
		this.element = $(el);
		this.listener = this.fired.bind(this);
		this.value = this.element.getValue();
		if (this.options.periodical) this.timer = this.listener.periodical(this.options.periodical);
		else this.element.addEvent('keyup', this.listener);
	},

	fired: function() {
		var value = this.element.getValue();
		if (this.value == value) return;
		this.clear();
		this.value = value;
		this.timeout = this.fireEvent.delay(this.options.delay, this, ['onFired', [value]]);
	},

	clear: function() {
		$clear(this.timeout);
		return this;
	}
});

Observer.implement(new Options);
Observer.implement(new Events);
/* Fin Observer.js */

/* Debut Autocompleter.js */
/**
 * Autocompleter
 *
 * @version		1.0rc4
 *
 * @license		MIT-style license
 * @author		Harald Kirschner <mail [at] digitarald.de>
 * @copyright	Author
 */
var Autocompleter = {};

Autocompleter.Base = new Class({

	options: {
		minLength: 1,
		useSelection: true,
		markQuery: true,
		inheritWidth: true,
		maxChoices: 10,
		injectChoice: null,
		onSelect: Class.empty,
		onShow: Class.empty,
		onHide: Class.empty,
		customTarget: null,
		className: 'autocompleter-choices',
		observerOptions: {},
		fxOptions: {},
		overflown: []
	},

	initialize: function(el, options) {
		this.setOptions(options);
		this.element = $(el);
		this.build();
		this.observer = new Observer(this.element, this.prefetch.bind(this), $merge({
			delay: 400
		}, this.options.observerOptions));
		this.value = this.observer.value;
		this.queryValue = null;
		
	},

	/**
	 * build - Initialize DOM
	 *
	 * Builds the html structure for choices and appends the events to the element.
	 * Override this function to modify the html generation.
	 */
	build: function() {
		if ($(this.options.customTarget)) this.choices = this.options.customTarget;
		else {
			this.choices = new Element('ul', {
				'class': this.options.className,
				'id': 'theAutoCompleter',
				styles: {zIndex: this.options.zIndex}
			}).injectInside(document.body);
			//}).injectInside(this.element.getParent().getParent().getElement('DIV'));
			this.fix = new OverlayFix(this.choices);
			
		}
		this.fx = this.choices.effect('opacity', $merge({
			wait: false,
			duration: 200
		}, this.options.fxOptions))
			.addEvent('onStart', function() {
				if (this.fx.now) return;
				this.choices.setStyle('display', '');
				this.fix.show();
			}.bind(this))
			.addEvent('onComplete', function() {
				if (this.fx.now) return;
				this.choices.setStyle('display', 'none');
				this.fix.hide();
			}.bind(this)).set(0);
		this.element.setProperty('autocomplete', 'off')
			.addEvent(window.ie ? 'keydown' : 'keypress', this.onCommand.bindWithEvent(this))
			//.addEvent('mousedown', this.onCommand.bindWithEvent(this, [false]))
			//.addEvent('focus', this.toggleFocus.bind(this, [false]))
			//.addEvent('blur', this.toggleFocus.bind(this, [false]))
			.addEvent('trash', this.destroy.bind(this));
	},

	destroy: function() {
		this.choices.remove();
	},

	toggleFocus: function(state) {
		this.focussed = state;
		if (!state) this.hideChoices();
	},

	onCommand: function(e, mouse) {
		if (mouse && this.focussed) this.prefetch();
		
		if (e.key && !e.shift) switch (e.key) {
			case 'enter':
				if (this.selected && this.visible) {
					this.choiceSelect(this.selected);
					e.stop();
				} return;
			/*
			case 'up': case 'down':
				if (this.observer.value != (this.value || this.queryValue)) this.prefetch();
				else if (this.queryValue === null) break;
				else if (!this.visible) this.showChoices();
				else {
					this.choiceOver((e.key == 'up')
						? this.selected.getPrevious() || this.choices.getLast()
						: this.selected.getNext() || this.choices.getFirst() );
					this.setSelection();
				}
				e.stop(); return;
			*/
			case 'esc': this.hideChoices(); return;
		}
		this.value = false;
	},

	setSelection: function() {
		if (!this.options.useSelection) return;
		var startLength = this.queryValue.length;
		if (this.element.value.indexOf(this.queryValue) != 0) return;
		var insert = this.selected.inputValue.substr(startLength);
		if (document.getSelection) {
			this.element.value = this.queryValue + insert;
			this.element.selectionStart = startLength;
			this.element.selectionEnd = this.element.value.length;
		} else if (document.selection) {
			var sel = document.selection.createRange();
			sel.text = insert;
			sel.move("character", - insert.length);
			sel.findText(insert);
			sel.select();
		}
		this.value = this.observer.value = this.element.value;
	},

	hideChoices: function() {
		if (!this.visible) return;
		this.visible = this.value = false;
		this.observer.clear();
		this.fx.start(0);
		this.fireEvent('onHide', [this.element, this.choices]);
	},

	showChoices: function() {
		if (this.element.value.length > 0) {
			if (this.visible || !this.choices.getFirst()) return;
			this.visible = true;
			var pos = this.element.getCoordinates(this.options.overflown);
			this.choices.setStyles({
				left: pos.left,
				top: pos.bottom
			});
			if (this.options.inheritWidth) this.choices.setStyle('width', pos.width);
			this.fx.start(1);
			this.choiceOver(this.choices.getFirst());
			this.fireEvent('onShow', [this.element, this.choices]);
		}
	},

	prefetch: function() {
		if (this.element.value.length < this.options.minLength) this.hideChoices();
		else if (this.element.value == this.queryValue) this.showChoices();
		else this.query();
	},

	updateChoices: function(choices) {
		this.choices.empty();
		this.selected = null;
		if (!choices || !choices.length) return;
		if (this.options.maxChoices < choices.length) choices.length = this.options.maxChoices;
		choices.each(this.options.injectChoice || function(choice, i){
			var el = new Element('li').setHTML(this.markQueryValue(choice));
			el.inputValue = choice;
			this.addChoiceEvents(el).injectInside(this.choices);
		}, this);
		//alert(this.element.value.length);
		
		if (this.element.value.length > 0) {
			//alert(this.element.value.length+'0');
			this.showChoices();
		}
	},

	choiceOver: function(el) {
		if (this.selected) this.selected.removeClass('autocompleter-selected');
		this.selected = el.addClass('autocompleter-selected');
	},

	choiceSelect: function(el) {
		this.observer.value = this.element.value = el.inputValue;
		this.hideChoices();
		this.fireEvent('onSelect', [this.element], 20);
	},

	/**
	 * markQueryValue
	 *
	 * Marks the queried word in the given string with <span class="autocompleter-queried">*</span>
	 * Call this i.e. from your custom parseChoices, same for addChoiceEvents
	 *
	 * @param		{String} Text
	 * @return		{String} Text
	 */
	markQueryValue: function(txt) {
		return (this.options.markQuery && this.queryValue) ? txt.replace(new RegExp('^(' + this.queryValue.escapeRegExp() + ')', 'i'), '<span class="autocompleter-queried">$1</span>') : txt;
	},

	/**
	 * addChoiceEvents
	 *
	 * Appends the needed event handlers for a choice-entry to the given element.
	 *
	 * @param		{Element} Choice entry
	 * @return		{Element} Choice entry
	 */
	addChoiceEvents: function(el) {
		return el.addEvents({
			mouseover: this.choiceOver.bind(this, [el]),
			mousedown: this.choiceSelect.bind(this, [el])
		});
	}
});

Autocompleter.Base.implement(new Events);
Autocompleter.Base.implement(new Options);

Autocompleter.Local = Autocompleter.Base.extend({

	options: {
		minLength: 0,
		filterTokens : null
	},

	initialize: function(el, tokens, options) {
		this.parent(el, options);
		this.tokens = tokens;
		if (this.options.filterTokens) this.filterTokens = this.options.filterTokens.bind(this);
	},

	query: function() {
		this.hideChoices();
		this.queryValue = this.element.value;
		this.updateChoices(this.filterTokens());
	},

	filterTokens: function(token) {
		var regex = new RegExp('^' + this.queryValue.escapeRegExp(), 'i');
		return this.tokens.filter(function(token) {
			return regex.test(token);
		});
	}

});

Autocompleter.Ajax = {};

Autocompleter.Ajax.Base = Autocompleter.Base.extend({

	options: {
		postVar: 'value',
		postData: {},
		ajaxOptions: {},
		onRequest: Class.empty,
		onComplete: Class.empty
	},

	initialize: function(el, url, options) {
		this.parent(el, options);
		this.ajax = new Ajax(url, $merge({
			autoCancel: true
		}, this.options.ajaxOptions));
		this.ajax.addEvent('onComplete', this.queryResponse.bind(this));
		this.ajax.addEvent('onFailure', this.queryResponse.bind(this, [false]));
	},

	query: function(){
		var data = $extend({}, this.options.postData);
		data[this.options.postVar] = this.element.value;
		this.fireEvent('onRequest', [this.element, this.ajax]);
		this.ajax.request(data);
	},

	/**
	 * queryResponse - abstract
	 *
	 * Inherated classes have to extend this function and use this.parent(resp)
	 *
	 * @param		{String} Response
	 */
	queryResponse: function(resp) {
		this.value = this.queryValue = this.element.value;
		this.selected = false;
		this.hideChoices();
		this.fireEvent(resp ? 'onComplete' : 'onFailure', [this.element, this.ajax], 20);
	}

});

Autocompleter.Ajax.Json = Autocompleter.Ajax.Base.extend({

	queryResponse: function(resp) {
		this.parent(resp);
		var choices = Json.evaluate(resp || false);
		if (!choices || !choices.length) return;
		this.updateChoices(choices);
	}

});

Autocompleter.Ajax.Xhtml = Autocompleter.Ajax.Base.extend({

	options: {
		parseChoices: null
	},

	queryResponse: function(resp) {
		this.parent(resp);
		if (!resp) return;
		this.choices.setHTML(resp).getChildren().each(this.options.parseChoices || this.parseChoices, this);
		if (this.element.value.length > 0) {
			this.showChoices();
		}
	},

	parseChoices: function(el) {
		var value = el.innerHTML;
		el.inputValue = value;
		el.setHTML(this.markQueryValue(value));
	}

});


var OverlayFix = new Class({

	initialize: function(el) {
		this.element = $(el);
		if (window.ie){
			this.element.addEvent('trash', this.destroy.bind(this));
			this.fix = new Element('iframe', {
				properties: {
					frameborder: '0',
					scrolling: 'no',
					src: 'javascript:false;'
				},
				styles: {
					position: 'absolute',
					border: 'none',
					display: 'none',
					filter: 'progid:DXImageTransform.Microsoft.Alpha(opacity=0)'
				}
			}).injectAfter(this.element);
		}
	},

	show: function() {
		if (this.fix) this.fix.setStyles($extend(
			this.element.getCoordinates(), {
				display: '',
				zIndex: (this.element.getStyle('zIndex') || 1) - 1,
				top: '0px',
				left: '0px'
			}));
		return this;
	},

	hide: function() {
		if (this.fix) this.fix.setStyle('display', 'none');
		return this;
	},

	destroy: function() {
		this.fix.remove();
	}

});
/* Fin Autocompleter.js */

/* Debut textsizer.js */
//Specify affected tags. Add or remove from list:
var tgs = new Array( 'span','a','h1','div');

//Specify spectrum of different font sizes:
var szs = new Array( 'xx-small','x-small','small','medium','large','x-large','xx-large' );


var startSz = 3;

function ts( trgt,inc ) {
	if (!document.getElementById) return
	var d = document,cEl = null,sz = startSz,i,j,cTags;
	
	sz += inc;
	if ( sz < 0 ) sz = 0;
	if ( sz > 6 ) sz = 6;
	startSz = sz;
		
	if ( !( cEl = d.getElementById( trgt ) ) ) cEl = d.getElementsByTagName( trgt )[ 0 ];

	cEl.style.fontSize = szs[ sz ];

	for ( i = 0 ; i < tgs.length ; i++ ) {
		cTags = cEl.getElementsByTagName( tgs[ i ] );
		for ( j = 0 ; j < cTags.length ; j++ ) cTags[ j ].style.fontSize = szs[ sz ];
	}
}
/* Fin textsizer.js */




var PopIn = new Class({
	options: {
		height : false,
		width : false,
		container : false,
		content : false,
		evaluation : false,
		mode : false
	},
	initialize : function(options){
		this.container = this.options.container ? this.options.container : $E('body');
		this.height = this.options.height ? this.options.height : 500;
		this.width = this.options.width ? this.options.width : 500;
		this.content = this.options.content ? this.options.content : 'pas de contenu'
		this.mode = this.options.mode ? this.options.mode : 'default';
		this.evaluation = this.options && this.options.evaluation ? this.options.evaluation : false;
		this.makePop();
	},
	makePop : function(){
		var _this = this;
		this.pop = new Element('div', {'class':'popIn'});
		this.pop.container = new Element('div', {'class':'popInContent'});
		if (!window.ie6){
			this.popBorders = new Element('img', {'class' : 'popBorders', 'src' : '/images/fondPopin.png'});
		}else{
			this.popBorders = new Element('div', {'class' : 'popBordersIE'});
		}
		this.closeButton = new Element('a', {'class':'popInCloser'});
		this.closeButton.href = '#';
		this.closeButton.innerHTML = '<img alt="fermer" src="/images/croix.gif"/>';

		//insertion elemnt
		this.container.adopt(this.popBorders);
		this.container.adopt(this.pop);
		this.pop.adopt(this.closeButton);
		this.pop.adopt(this.pop.container);

		this.mask = new Element('div', {'class':'popInMask'});
		this.mask.fxOppac = new Fx.Style(this.mask, 'opacity',{duration:200});

		this.container.adopt(this.mask);
		this.mask.addEvent('click', function(e){
			var e = new Event(e).stop();
			_this.closePop();
		})
		this.closeButton.addEvent('click', function(e){
			var e = new Event(e).stop();
			_this.closePop();
		})
	},
	setPop :function(options){
		this.height = options && options.height ? options.height : this.height;
		this.width = options && options.width ? options.width : this.width;
		var _leftToSet = (this.container.getCoordinates().width/2) - (this.width/2);
		//var _topToSet = (this.container.getCoordinates().height/2) - (this.height/2);
		var _topToSet = 25;
		var separateur = (window.ie6) ? 13 : 3;

		this.pop.setStyles({
			'left' : _leftToSet - separateur, //-20 correspond au padding de la bordure
			//'top' : _topToSet - separateur,//-20 correspond au padding de la bordure
			'top' : _topToSet,//-20 correspond au padding de la bordure
			'width' : parseInt(this.width),
			'height' : parseInt(this.height)
		})
		this.pop.container.setStyle('height', this.height);
		this.pop.container.setStyle('width', this.width);

		this.popBorders.setStyles({
			'left' : _leftToSet - 35, //-35 = -20 correspond au padding de la bordure + - 15 decalage
			'top' : _topToSet - 25, //-35 = -20 correspond au padding de la bordure + - 15 decalage
			'width' : parseInt(this.width) + 70,
			'height' : parseInt(this.height) + 90
		});
		//console.log(this.popBorders.offsetWidth, this.width, this.height + 10)
	},
	openPop : function(options){
		this.content = options && options.content ? options.content : this.content;
		this.container = options && options.container ? options.container : this.container;
		this.mode = options && options.mode ? options.mode : 'default';
		this.evaluation = options && options.evaluation ? options.evaluation : true;

		this.pop.container.empty();
		this.setPop(options);
		this.innerPop(options);
		this.maskPop('on');

		this.pop.setStyle('display','block');
	},
	closePop : function(){
		this.pop.setStyles('display','none');
		this.popBorders.setStyles({'left':-1000, 'top':-1000})
		this.pop.container.empty();
		this.maskPop('off');
	},
	maskPop : function(door){
		var _this = this;
		switch (door) {
		    case 'on': //Mode mask On
				if (window.ie6) $$('select').setOpacity(0);
				_this.mask.setStyles({
					'height':window.getHeight(),
					'width':window.getWidth(),
					'display':'block'
				});
				_this.mask.fxOppac.start(0.5);
			break;
		    case 'off': //Mode Mask Off
				if (window.ie6) $$('select').setOpacity(0);
				_this.mask.fxOppac.start(0);
		    default:
			return false;
		}
	},
	innerPop : function(options){
		this.mode = options && options.mode ? options.mode : this.mode;
		this.evaluation = options && options.evaluation ? options.evaluation : false;

		var _this = this;
		var bool = window.ie ? false : this.evaluation;

		switch (this.mode) {
		    case 'remote': //Mode ajax
				var __width = 0;
				new Ajax(_this.content + '?mode=popin', {
					method: 'get',
					evalScripts : bool,
					onComplete : function(response){
						_this.pop.container.setHTML(response);
					}
				}).request();
			break;
		    case 'iframe': //Mode iframe
				//alert('to do iframe')
				var ifrLay = new Element('iframe', {'src':_this.content, 'scrolling' :'no', 'frameborder' : 0});
				ifrLay.width = this.width;
				ifrLay.height = this.height;
				_this.pop.container.adopt(ifrLay);
			break;
			case 'default': //Mode default
				if (typeof _this.content == 'object' && $(this.content)){
					_this.pop.container.adopt(_this.content.clone());
				}else if(typeof _this.content == 'string'){
					_this.pop.container.setText(_this.content);
				}
			break;
		}
	}
});
window.addEvent('load', function(){
	pop = new PopIn();
});
function openBubblePlanet(mode, url, eval){
	var mode = mode ? mode : 'popdown';
	var yval = (eval == undefined) ? false : true;

	switch (mode) {
	    case 'popin': //Mode ajax
			pop.openPop({content:url, container:$(document.documentElement), evaluation:'eval', height:592, width:997, mode:'remote'});
		break;
	    case 'popup': //Mode popup
			window.open(url, '' , 'width=1000,height=592,left=100,top=100')
		break;
		default : // Mode normal
			window.location.href = url;
		break;
	}
}
