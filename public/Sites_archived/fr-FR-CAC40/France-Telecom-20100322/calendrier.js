//alert("load calendar");
/* DEBUT calendar.js */
if (sLangSite =="fr") {
	var monthInYear = new Array("janvier", "f&eacute;vrier", "mars", "avril", "mai", "juin", "juillet", "ao&ucirc;t", "septembre", "octobre", "novembre", "d&eacute;cembre");
}
else {
	var monthInYear = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
}

if (sLangSite =="fr") {
	var dayInWeek = new Array("Lu", "Ma", "Me", "Je", "Ve", "Sa","Di");
}
else {
	var dayInWeek = new Array("Mo", "Tu", "We", "Th", "Fr", "Sa", "Su");
}
var Calendrier = new Class({
	
	initialize : function(){
		this.elementId = "";
		this.element = null;
	
		this.weekday = 0;
		this.day = 0;
		this.month = 0;
		this.year = 0;
	
		this.cday = 0;
		this.cmonth = 0;
		this.cyear = 0;
		this.cyear2 = 0;
	
		this.mappingArray = new Array();
	
		this.cal = document.getElementById("cal");
		this.cald = document.getElementById("cald");
		this.calm = document.getElementById("calm");
		this.caly = document.getElementById("caly");
	},
	
	initCalendar : function(date) {
		this.weekday = date.getDay();
		this.day = date.getDate();
		this.month = date.getMonth() + 1;
		this.year = date.getFullYear();
	
		this.cmonth = this.month;
		this.cyear = this.year;
	
		this.fillDayChoice();
		this.fillMonthChoice();
		this.fillYearChoice();
	},
	
	initElement : function (id, evt) {
		if (!evt) {
			evt = window.event;
		}
		this.elementId = "";
		this.element = document.getElementById(id);
	
		var elementStyle = this.cal.style;
		var clientX = 100;
		var clientY = 100;
		elementStyle.left = document.body.scrollLeft + clientX;
		elementStyle.top = document.body.scrollTop + clientY;
	
		this.calm.style.left = parseInt(elementStyle.left) + 6;
		this.calm.style.top = parseInt(elementStyle.top) + 28;
		this.caly.style.left = parseInt(elementStyle.left) + 12;
		this.caly.style.top = parseInt(elementStyle.top) + 33;
	},
	
	displayCalendar : function(id, ev) {
	
		var i;
		var element;
		
		element = document.getElementById("cal_y_02");
		
		this.initElement(id);
		
		var dateValue = this.element.value;
		var validDate = false;
		if (dateValue != "") {
			var s = dateValue.split("/");
			var d;
			var m;
			var y;
			if (s.length == 3) {
				d = s[0];
				m = s[1];
				y = s[2];
				if (!isNaN(d) && !isNaN(m) && !isNaN(y)) {
				  var m2 = parseNumber(m);
				  if (m2 >= 1 && m2 <= 12) {
				    validDate = true;
						var y2 = parseNumber(y);
						if (y2 < 100) {
							y2 += 2000;
						}
				    var d2 = parseNumber(d);
				    var testDate = new Date(y2, m2 - 1, 1);
						if (d2 >= 1 && d2 <= getMaxMonthLastDay(testDate)) {
			            this.initCalendar(new Date(y2, m2 - 1, d2));
			          } else {
			            this.initCalendar(new Date(y2, m2 - 1, 1));
			          }
					}
				}
			}
		}
		if (!validDate) {
			this.initCalendar(new Date());
		}
		this.fillCalendar();
		this.showCalendar();
	},
	
	showCalendar : function() {
		//calendar.cal.style.visibility = "visible";
		$('loader_calendar').style.display = 'none';
		this.cal.style.display = "block";
		this.showDayChoice();
		this.hideMonthChoice();
		this.hideYearChoice();
	},
	
	hideCalendar : function () {
		//this.cal.style.visibility = "visible";
		this.cal.style.display = "block";
		
	
	},
	
	showDayChoice : function () {
		//this.cald.style.visibility = "visible";
		this.cald.style.display = "block";
	},
	
	hideDayChoice : function () {
		//this.cald.style.visibility = "hidden";
		this.cald.style.display = "block";
	},
	
	showMonthChoice : function () {
		//this.calm.style.visibility = "visible";
		this.calm.style.display = "block";
	},
	
	hideMonthChoice : function () {
		//this.calm.style.visibility = "hidden";
		this.calm.style.display = "none";
	},
	
	showYearChoice : function () {
		//this.caly.style.visibility = "visible";
		this.caly.style.display = "block";
	},
	
	hideYearChoice : function () {
		//this.caly.style.visibility = "hidden";
		this.caly.style.display = "none";
	},
	
	updateMonthChoiceVisibility : function () {
		//if (this.calm.style.visibility == "visible") {
		if (this.calm.style.display == "block") {
			this.showDayChoice();
			this.hideMonthChoice();
		} else {
			this.hideDayChoice();
			this.hideYearChoice();
			this.showMonthChoice();
		}
	},
	
	updateYearChoiceVisibility : function () {
		//if (this.caly.style.visibility == "visible") {
		if (this.caly.style.display == "block") {
			this.showDayChoice();
			this.hideYearChoice();
		} else {
			this.cyear2 = this.cyear;
			this.hideDayChoice();
			this.hideMonthChoice();
			this.fillYearChoice();
			this.showYearChoice();
		}
	},
	
	fillCalendar : function () {
		
		document.getElementById("cal_m_02").innerHTML = this.getStringMonth(this.cmonth);
		
		document.getElementById("cal_y_02").innerHTML = this.cyear;
		
		var element;
	
		var i = 0;
		var j = 0;
		for (i = 0; i < 6; i++) {
			for (j = 0; j < 7; j++) {
				element = document.getElementById("cal_d_" + i + j);
				element.innerHTML = "";
				element.onclick = "";
				element.onmouseover = "";
				element.onmouseout = "";
			}
		}
		i = 0;
		j = (new Date(this.cyear, this.cmonth-1, 1)).getDay();
		j = (j + 6) % 7;
		var count = 1;
		var days = this.getMonthLastDay();
		
		var case_num;
		
		while (count <= days) {
			element = document.getElementById("cal_d_" + i + j);
		
			case_num = new Element("div", {'class':'case_Date'});
			case_num.innerHTML = count;
			case_num.id=count+"_"+this.cmonth+"_"+this.cyear;
			element.appendChild(case_num);
			
			//element.onclick = this.selectDay;
			//element.onmouseover = this.highlightDay;
			//element.onmouseout = this.unhighlightDay;
			this.mappingArray[10 * i + j] = count;
			j++;
			if (j == 7) {
				i++;
				j = 0;
			}
			count++;
		}
	},
	
	fillMonthChoice : function() {
		var i;
		var element;
		for (i = 1; i <= 12; i++) {
			element = document.getElementById("calm_" + i);
			element.innerHTML = this.getStringMonth(i);
			element.onclick = this.selectMonth;
			element.onmouseover = this.highlightHead;
			element.onmouseout = this.unhighlightHead;
		}
	},
	
	fillDayChoice : function () {
		var i;
		var element;
		for (i = 0; i <= 6; i++) {
			element = document.getElementById("cald_" + i);
			element.innerHTML = this.getStringWeekDay(i);
		}
	},
	fillYearChoice : function () {
		var i;
		var element;
		var gap = this.cyear2 - 11;
		for (i = 1; i <= 25; i++) {
			element = document.getElementById("caly_" + i);
			element.innerHTML = gap + i;
			element.onclick = this.selectYear;
			element.onmouseover = this.highlightHead;
			element.onmouseout = this.unhighlightHead;
			//alert(this.month);		
		}
	},
	
	highlightDay : function(evt) {
		//getEventElement(evt).className = "sel";
	},
	
	unhighlightDay : function(evt) {
		//getEventElement(evt).className = "";
	},
	
	highlightHead : function(evt) {
		var element = this.getEventElement(evt);
		if (element.tagName == "IMG") {
			element = element.parentNode;
		}
	},
	
	unhighlightHead : function(evt) {
		var element = this.getEventElement(evt);
		if (element.tagName == "IMG") {
			element = element.parentNode;
		}
		element.className = "";
		/*if (element.id == "cal_m_02") {
			document.getElementById("cal_m_03").className = "";
		} else if (element.id == "cal_m_03") {
			document.getElementById("cal_m_02").className = "";
		} 
		else if (element.id == "cal_y_02") {
			document.getElementById("cal_y_03").className = "";
		} else if (element.id == "cal_y_03") {
			document.getElementById("cal_y_02").className = "";
		} */
	},
	
	selectDay : function (evt) {
		var id = this.getEventElement(evt).id;
		this.cday = this.mappingArray[parseInt(id.substr(id.length - 2))];
		this.hideCalendar();
		var d = this.cday;
		var m = this.cmonth;
		var y = this.cyear;
		window.status = d + " - " + m + " - " + y;
		this.element.value = (d < 10 ? "0" : "") + d + "/" + (m < 10 ? "0" : "") + m + "/" + y;
	},
	
	selectMonth : function (evt) {
		var id = this.getEventElement(evt).id;
		var s = id.split("_");
		this.cmonth = parseInt(s[s.length - 1]);
		this.hideMonthChoice();
		this.showDayChoice();
		this.fillCalendar();
	},
	
	selectYear : function (evt) {
		var id = this.getEventElement(evt).id;
		var s = id.split("_");
		this.cyear = parseInt(s[s.length - 1]) + this.cyear2 - 11;
		this.hideYearChoice();
		this.showDayChoice();
		this.fillCalendar();
	},
	
	incrementMonth : function() {
		if (this.cmonth == 12) {
			this.cmonth = 1;
			this.cyear = this.cyear + 1;
		} else {
			this.cmonth = this.cmonth + 1;
		}
		this.fillCalendar();
	},
	
	decrementMonth : function() {
		if (this.cmonth == 1) {
			this.cmonth = 12;
			this.cyear = this.cyear - 1;
		} else {
			this.cmonth = this.cmonth - 1;
		}
		this.fillCalendar();
	},
	
	incrementYear : function() {
		this.cyear = this.cyear + 1;
		this.fillCalendar();
	},
	
	decrementYear : function () {
		this.cyear = this.cyear - 1;
		this.fillCalendar();
	},
	
	increaseYear : function () {
		this.cyear2 = this.cyear2 + 20;
		this.fillYearChoice();
	},
	
	decreaseYear : function () {
		this.cyear2 = this.cyear2 - 20;
		this.fillYearChoice();
	},
	
	getMonthLastDay : function () {
		var testDate = new Date(this.cyear, this.cmonth-1, 29);
	  	return this.getMaxMonthLastDay(testDate);
	},
	
	getMaxMonthLastDay : function(date) {
		if (date.getDate() == 29) {
			date.setDate(30);
			if (date.getDate() == 30) {
				date.setDate(31);
				if (date.getDate() == 31) {
					return 31
				} else {
					return 30;
				}
			} else {
				return 29;
			}
		} else {
			return 28;
		}
	},
	
	getStringWeekDay : function(index) {
		return dayInWeek[index];
	},
	
	getStringMonth : function(index) {
		return monthInYear[index - 1];
	}
	
});