
function correctPNG()
{var arVersion=navigator.appVersion.split("MSIE")
var version=parseFloat(arVersion[1])
if((version>=5.5)&&(document.body.filters))
for(var i=0;i<document.images.length;i++)
{var img=document.images[i]
var imgName=img.src.toUpperCase()
if(imgName.substring(imgName.length-3,imgName.length)=="PNG")
{var imgID=(img.id)?"id='"+img.id+"' ":""
var imgClass=(img.className)?"class='"+img.className+"' ":""
var imgTitle=(img.title)?"title='"+img.title+"' ":"title='"+img.alt+"' "
var imgStyle="display:inline-block;"+img.style.cssText
var imgAttribs=img.attributes;for(var j=0;j<imgAttribs.length;j++)
{var imgAttrib=imgAttribs[j];if(imgAttrib.nodeName=="align")
{if(imgAttrib.nodeValue=="left")imgStyle="float:left;"+imgStyle
if(imgAttrib.nodeValue=="right")imgStyle="float:right;"+imgStyle
break}}
var strNewHTML="<span "+imgID+imgClass+imgTitle
strNewHTML+=" style=\""+"width:"+img.width+"px; height:"+img.height+"px;"+imgStyle+";"
strNewHTML+="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader"
strNewHTML+="(src='"+img.src+"', sizingMethod='scale');\""
strNewHTML+=" onmouseover=\"PNGswap('"+img.id+"');\" onmouseout=\"PNGswap('"+img.id+"');\""
strNewHTML+="></span>"
img.outerHTML=strNewHTML
i=i-1}}}
function correct_png_background(){
if(!window.ie6){
	return;
}
if(document.all){
	var images_list=document.getElementsByTagName('*');
	for(var i=0;i<images_list.length;i++){
		var image_name=images_list[i].currentStyle.backgroundImage.replace(/url[s]*()+/,'');
		image_name=image_name.replace(/(")+/g,'');
		image_name=image_name.substr(1,image_name.length-1);
		image_name=image_name.substr(0,image_name.length-1);
		if(!images_list[i].className.match(/\bnofix\b/) && image_name.substring(image_name.length-3,image_name.length)=='png'){
			images_list[i].runtimeStyle.backgroundImage="url( '"+base+"/images/blank.gif' )";
			images_list[i].runtimeStyle.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader( src='"+image_name+"', sizingMethod='scale' )";}
		}
	}
return true;
}
if(window.attachEvent)
 window.attachEvent("onload", correct_png_background);



function pngFix(elm, noOverflow) {
	elm.style.filter = ' ';
	if (!(document.all && window.print && /MSIE [56]/.test(navigator.userAgent))) return;
	var exec = (function(elm, noOverflow, scale) {
		return function() {


			var options = { noOverflow:noOverflow};
			var repeat = elm.currentStyle.backgroundRepeat.toLowerCase()=='repeat';
			elm.style.filter = ' ';
				// si l'élément est un tag img, on va en faire creer une balise qui encadrera cette image et ensuite traiter la balise comme si c'etait un élément qui avait une image de fond
				if (elm.nodeName.match(/^(IMG|INPUT)$/)) {
					if (!elm.src.match(/.*\.png$/)) return;

					elm.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true, sizingMethod='image', src='"+ elm.src + "')";

					elm.width = elm.offsetWidth;
					elm.height = elm.offsetHeight;

					/* recuperation de l'url du pixel transparent */
					var url = elm.currentStyle.backgroundImage.match(/^url\(["'](.*\.gif)["']\)$/); //seulement les .png
					elm.src = url[1];
					elm.className = elm.className.replace(/pngFix/g,'');

				}
				else {
					if (elm.currentStyle.backgroundImage == "" || elm.currentStyle.backgroundImage == "url()") return;
					var url = elm.currentStyle.backgroundImage.match(/^url\(["'](.*\.png)["']\)$/); //seulement les .png
					if (!url || url.length<2) return;
					var pngLayer = document.createElement('i'); // on genere un <i> en position:absolute (layer), qui viendra se placer sous le contenu du div  qui avait besoin du style.
					with(pngLayer.style) {
						if (options.noOverflow) {
							width = elm.offsetWidth + 'px';
							height = elm.offsetHeight + 'px';
						} else {
						 	width = '32000px';
							height = '32000px'; 
						}
						position = 'absolute';
						zIndex = -1;
						fontSize = '1%';
						filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true, sizingMethod='" + (options.noOverflow ? 'crop' : 'image') + "', src='"+url[1]+"')";
						background = 'none'; //forcing car parfois il peut arriver qu'on ai une CSS qui vienne rajouter des images / couleurs de fond
						/* positionnement de l'image en fonction du background-position sur l'element */
						if (!repeat) {
							switch((elm.currentStyle.backgroundPositionX+'').toLowerCase()) {
								case 'left' : left=0; break; 
								case 'right' : right = 0; break;
								case 'center' : 
									left='50%'; 
									setTimeout(function(pngLayer) {
										return function() {
											pngLayer.style.marginLeft = -(pngLayer.offsetWidth/2)+'px'; 
										}
									}(pngLayer), 50);
									break;
								default : 
									left = elm.currentStyle.backgroundPositionX; 
							}

							switch((elm.currentStyle.backgroundPositionY+'').toLowerCase()) {
								case 'top' : top = 0; break;
								case 'bottom' : bottom = 0; break;
								case 'center' : 
									top='50%'; 
									setTimeout(function(pngLayer) {
										return function() {
											pngLayer.style.marginTop=-(pngLayer.offsetHeight/2)+'px'; 
										}
									}(pngLayer), 100);
									break;
								default : 
									top = elm.currentStyle.backgroundPositionY || 0; 
							}
						} else {
							left = 0; //elm.currentStyle.backgroundPositionX +'';
							top = 0; //elm.currentStyle.backgroundPositionY +'';
						}
					} 

						/* gestion automatique du sizingMethod='scale' ou sizingMethod='image', ne pouvant pas tester le backgroundRepeat correctement, on passe par une methode un peu plus tricky */
						setTimeout(function(elmN, pngLayerN, repeatN) {
							return function() {
								if (!elmN || elmN.parentNode || !pngLayerN || !pngLayerN.parentNode) return;
								if (pngLayerN.filters['DXImageTransform.Microsoft.AlphaImageLoader'].sizingMethod=='image') {
									if (pngLayerN.offsetWidth<elmN.offsetWidth && repeatN) {
										pngLayerN.filters['DXImageTransform.Microsoft.AlphaImageLoader'].sizingMethod='scale';
									} else if (pngLayerN.offsetWidth>elmN.offsetWidth && elm.currentStyle.backgroundPositionX.match(/^(left|0%|0px|0)$/) || elm.currentStyle.backgroundPositionY.match(/^(top|0%|0px|0)$/)){
										pngLayerN.filters['DXImageTransform.Microsoft.AlphaImageLoader'].sizingMethod='crop';
									}
								} else {
									pngLayerN.sizingMethod = 'image';
								}
								if (elm.currentStyle.width.match(/^(0|[12](%|px)?)$/)) {
									pngLayerN.filters['DXImageTransform.Microsoft.AlphaImageLoader'].sizingMethod='image';
								}
								if (pngLayerN.style.right != 'auto' && pngLayerN.style.right !='')
									setTimeout(function() {
										pngLayerN.style.right = parseInt(pngLayerN.style.right) - (elm.offsetWidth%2 ? 1 : 0) + 'px';
									}, 50)
							}
						}(elm, pngLayer, repeat), 200);

					with (elm.style) {
						position = elm.currentStyle.position=="static" || elm.currentStyle.position=="" ? 'relative' : position;
						if (elm.currentStyle.overflow!='auto' && elm.currentStyle.overflow!='hidden') overflow = options.noOverflow ? 'visible' : (elm.currentStyle.width.match(/^(0|[12](%|px)?)$/) ? 'visible' : 'hidden');
						backgroundImage = 'none';
					}
					elm.appendChild(pngLayer);

			}
		}
	})(elm, noOverflow);
	try{
		pngFixLoader.useOnload ? pngFixLoader.addFunc(exec) : exec();
	} catch(e) {};
}

/* pngFixLoader 
	@unction 		:	objet pour permet le lancement des modifications des png via pngFix en décalé sur le onload de la page.
							cela permet de contourner un bug d'internet explorer qui affiche un message d'erreur si le DOM est modifié pendant le chargement de la page.
*/
var pngFixLoader = {
	useOnload : true, // true : active l'execution du fixPng sur le load, et false, execute le fixPng dès qu'il est appelé par la CSS
	functions : [], // toutes les fonctions à éxécuter sur le onload de la page
	addFunc : function(func) {
		pngFixLoader.functions.push(func);
	},
	launch : function() {
			pngFixLoader.useOnload = false; //une fois la page chargée, il faut laisser s'executer automatiquement la fonction pour d'autres actions (ex : ouverture layer)
			var counter = 1;
			while(pngFixLoader.functions.length>0) {
				//setTimeout(pngFixLoader.functions.pop(), 20*counter);
				pngFixLoader.functions.pop()();
				counter++;
			}
	},
	init : function() {
		if (pngFixLoader.useOnload && window.attachEvent && document.all) {
			window.attachEvent('onload', function() {
				setTimeout(pngFixLoader.launch, 100);
			});
		}
	}
}
pngFixLoader.init();