/* fonctions NAVG */
var LayerNavg = new Class({
	initialize : function(){
		//mode par defaut
		this.mode = 1;
		this.containerCenter = $('conteneur_centrer');
		this.container = $(document.body);
		this.gauche = $('gauche');
		this.gauche.coord = this.gauche.getCoordinates();
		this.lastItem = null;
		this.createLay();
		this.timer;
	},
	createLay : function(){
		//make elements
		this.layerCtn = new Element('div', {'class' : 'navgLay nofix'});
		this.layerSubCtn = this.layerCtn.clone();
		this.layerCtnN3 = this.layerCtn.clone();
		this.lay = new Element('div', {'class' : 'ctt nofix'});
		this.laySub = this.lay.clone();
		this.layN3 = this.lay.clone();
		this.span = new Element('span');
		this.spanSub = this.span.clone();
		this.spanN3 = this.span.clone();
		//make effects
		this.fxLayer  = new Fx.Styles(this.layerCtn, {duration:250});
		this.fxLayerN3  = new Fx.Styles(this.layerCtnN3, {duration:250});
		this.fxArrow = new Fx.Styles(this.span, {duration:300});
		this.fxArrowSub = new Fx.Styles(this.spanSub, {duration:350});
		//include elements in dom
		this.container.adopt([this.layerCtn.adopt(this.lay),this.layerSubCtn.adopt(this.laySub),this.layerCtnN3.adopt(this.layN3)]);
		ifrlayer.make(this.layerCtn); //genere ou affiche l'iframe
		ifrlayer.make(this.layerSubCtn); //genere ou affiche l'iframe
		ifrlayer.make(this.layerCtnN3); //genere ou affiche l'iframe

		this.layerCtn.adopt(this.span);
		this.layerSubCtn.adopt(this.spanSub);
		this.layerCtnN3.adopt(this.spanN3);

		this.layerOn = false;
	},
	openLay : function(oItem, mode, twoLines){
		var _this = this;
		var topLay = this.gauche.coord.top;
		if (mode) this.mode = mode;
		switch(this.mode){
			case 1:
				this.layerCtn.className = 'navgLay nofix layerMode1';
				this.layerCtn.setStyles({
					'top' : topLay,
					'left' : this.containerCenter.getLeft() + 147
				})
				//fix ie6
				if(window.ie6){
					pngFix(this.span);
				}
				var diff = 0;
				if (!this.layerOn){
					ifrlayer.make(this.layerSubCtn); //genere ou affiche l'iframe
					this.fxLayer.start({
						'opacity': 1
					})
					this.span.setStyle('top', oItem.getTop() - topLay +5 +diff);
				}else{
					this.fxArrow.start({
						'top' : oItem.getTop() - topLay + 4 -diff
					})
				}
				this.layerCtn.removeEvents();
				this.layerCtn.addEvent('mouseenter', function(){
					oItem.fireEvent('mouseenter');
				})
				this.layerCtn.addEvent('mouseleave', function(){
					oItem.fireEvent('mouseleave');
				})
			break;
			case 2:
				this.layerSubCtn.className = 'navgLay layerMode2';
				if (leftNavLayer.layerSubCtn.layerOn){
					this.fxArrowSub.start({
						'top' : oItem.getTop() - topLay + 18
					})
				}else{
					this.layerSubCtn.setStyles({
						'top' : topLay,
						'left' : this.containerCenter.getLeft() + 280
					})
					//fix ie6
					if(window.ie6){
						//this.layerSubCtn.style.backgroundImage = 'url(/images/fondLayerNav2.png)';
						pngFix(this.layerSubCtn);
						pngFix(this.spanSub);
						ifrlayer.make(this.layerSubCtn); //genere ou affiche l'iframe
					}
					this.spanSub.setStyles({
						'top' : oItem.getTop() - topLay + 18
					})
				}
				this.layerSubCtn.removeEvents();
				this.layerSubCtn.addEvent('mouseenter', function(){
					$clear(persoMenu.timeLeave);
					$clear(persoMenu.timesubLeave);
				})
				this.layerSubCtn.addEvent('mouseleave', function(){
					_this.layerCtn.fireEvent('mouseleave');
					oItem.fireEvent('mouseleave');
					//oItem.fireEvent('mouseleave');
				})
			break;
			case 3:
				if (!this.layerOn){
					ifrlayer.make(this.layerCtnN3); //genere ou affiche l'iframe
					this.fxLayerN3.start({
						'opacity': 1
					})
				}
				
				this.layerCtnN3.className = (twoLines) ? 'navgLay layerMode3 twoLines' : 'navgLay layerMode3';
				
				if (this.lastItem) this.lastItem.removeClass('titleLayerMode3');
				oItem.className += ' titleLayerMode3';
				this.lastItem = oItem;
				this.layerCtnN3.setStyles({
					'top' : oItem.getTop(),
					'left' : this.containerCenter.getLeft() + 156
				})
				this.span.setStyle('top', oItem.getTop() - topLay + 5);
				this.layerCtnN3.removeEvents();
				this.layerCtnN3.addEvent('mouseenter', function(e){
					oItem.fireEvent('mouseenter');
					oItem.removeEvents();
				})
				this.layerCtnN3.addEvent('mouseleave', function(e){
					oItem.fireEvent('mouseleave');
					oItem.removeEvents();
				})
			break;
		}
	},
	closeLay : function(){
		ifrlayer.hide(this.layerCtn);
		this.fxLayer.start({
			'opacity' : 0
		})
		this.layerOn = false;
	},
	closeSubLay : function(){
		ifrlayer.hide(this.layerSubCtn);
		this.layerSubCtn.setStyles({
			'top' : -1000
		})
	},
	closeLayN3 : function(){
		ifrlayer.hide(this.layerCtnN3);
		if (this.lastItem) this.lastItem.removeClass('titleLayerMode3');
		
		this.fxLayerN3.start({
			'opacity': 0
		})
		this.layerOn = false;
	}
});

var PersoMenu = new Class({
	initialize : function(){
		var _this = this;
		this.timeLeave;
		this.timeEnter;
		this.timesubEnter;
		this.timesubLeave;
		this.oldItem;

		//set initial profil in a cookie
		this.myProfile;
		if (Cookie.get('navLprofile')){
			this.myProfile = this.getPairFromCook('navLprofile');
		}else{
			this.myProfile = aLeftMenuProfil[0];
		}

//creation cookies :
//this.setCookFromPair(aLeftMenuProfil[0], 'navLprofile');
//Not used anymore, created by java
//Cookie.set('menuItemBookMarks','histo2$/images/histo2.png$http://www.tesst.fr@histo1$/images/histo1.png$http://www.google.fr@histo3$/images/histo3.png$http://www.lol.fr', {duration:365,path:"/"});
//Cookie.set('menuItemHistory','histo1$/images/histo1.png$http://www.google.fr@histo2$/images/histo2.png$http://www.tesst.fr@histo3$/images/histo3.png$http://www.lol.fr@histo2$/images/histo2.png$http://www.tesst.fr@histo3$/images/histo3.png$http://www.lol.fr', {duration:365,path:"/"});

		this.leftProfiles = aLeftMenuProfil;

		if ($('navg_personnal_menu') == null) return;
		this.menu = $('navg_personnal_menu');
		var itemss = this.menu.getElements('li');
		this.menu.items = itemss;
		this.menu.items.each(function(item){
			_this.setItem(item);
		})
	},
	getArrFromCook : function(sId){
		if (Cookie.get(sId)){
			var arrGlobal = [];
			var sCookie = Cookie.get(sId);
			sCookie = sCookie.replace('"', '');
			var aCookies = sCookie.split('@');
			for (var i=0; i < aCookies.length; i++) {
				var oCookie = {};
				var a = aCookies[i];
				a = a.replace('"', '');
				var b = a.split('$');
				oCookie.libelle = b[0];
				oCookie.src = b[1];
				oCookie.url = b[2];
				arrGlobal.push(oCookie);
			};
			return arrGlobal;
		}
	},
	getPairFromCook : function(sCook){
		sCook = sCook ? sCook : 'navLprofile';
		var aCook = Cookie.get(sCook, {duration:365,path:"/"}).split('@');
		var oCook = {
			'label' : aCook[0],
			'url': aCook[1],
			'position' : aCook[2]
		}
		
		/* Test de la langue si elle est differente, on prends le nouveau label avec la position enregistree */
		var lgCookie = aCook[1].split("/")[1];
		var lgURL = document.location.href;
		var flag = "/"+ lgCookie +"/"
		if( !lgURL.match(flag) )
			oCook.label = aLeftMenuProfil[oCook.position].label;
		/* //Test de la langue */
		
		return oCook;
	},
	setCookFromObj : function(oElm, sCook){
		sCook = sCook ? sCook : 'navLprofile';
		var sLab = oElm.innerHTML;
		var _string = '';
		for (var i=0; i < aLeftMenuProfil.length; i++) {
			if (aLeftMenuProfil[i].label == sLab){
				_string = aLeftMenuProfil[i].label + '@' + aLeftMenuProfil[i].url + '@' + i;
			}
		};
		Cookie.set(sCook,_string, {duration:365,path:"/"});
	},
	setItem : function(oItem){
		var _this = this;
		var arrToUse = null;
		oItem.layerOn = false;
		//getCook ou recup JS array
		if (oItem.id == 'menuItemHome')return;

		//setting profile mode
		if (oItem.id == 'menuItemProfil'){
			var a = oItem.getElement('a');
			a.href = _this.myProfile.url;
			a.innerHTML += '&nbsp;<span>' +_this.myProfile.label + '</span>';
		}
		if (oItem.id == 'menuItemHistory') oItem.className += 'noLink';

		//events
		oItem.addEvent('mouseenter', function(){
			if (_this.timeLeave) $clear(_this.timeLeave);
			_this.timeEnter = (function(){
				if (_this.oldItem && !oItem.layerOn) _this.oldItem.layerOn = false;

				var pathImage = document.location.pathname;
				//var langImage = 'es';

				//var regexp = "^/[a-z]{2}_[A-Z]{2}/";
				//resReg = pathImage.match(regexp);
				//langImage = (resReg == "/en_EN/") ? 'en' : 'fr';
				// Il y avait + simple =)
				var langImage = pathImage.substring(1, 3); // ce qui r�cup�re une sous-chaine commen�ant � l'index 1 jusqu'� l'index 3

				//get array
				switch(oItem.id){
					case 'menuItemProfil':
						arrToUse = _this.leftProfiles;
					break;
					case 'menuItemBookMarks':
						arrToUse = _this.getArrFromCook('menuItemBookMarks');

						if (!arrToUse){
							arrToUse = [{
								'libelle' : (langImage == 'en') ? 'no bookmark' : 'pas de bookmarks',
								'src' : '/images/bookMarksEmpty_' + langImage + '.png',
								'url' : '/'
							}]
						}
						arrToUse.thumbMode = true;
					break;
					case 'menuItemHistory':
						arrToUse = _this.getArrFromCook('menuItemHistory');

						if (!arrToUse){
							arrToUse = [{
								'libelle' : (langImage == 'en') ? 'no history' : 'pas d\'historiques',
								'src' : '/images/historyEmpty_' + langImage + '.png',
								'url' : '/'
							}]
						}
						arrToUse.thumbMode = true;
					break;
				}
				//do layer
				_this.oldItem = oItem;
				oItem.layerOn = true;
				_this.popLay(oItem, arrToUse);
				leftNavLayer.openLay(oItem, 1);
			}).delay(100)
			if (oItem.layerOn && (oItem.layerOn == (_this.oldItem && oItem.id == _this.oldItem.id))) $clear(_this.timeEnter);
		})
		oItem.addEvent('mouseleave', function(){
			if (_this.timeEnter) $clear(_this.timeEnter);
			_this.timeLeave = (function(){
				leftNavLayer.closeLay();
				oItem.layerOn = false;
			}).delay(1000);
		})
	},
	popLay : function(oItem, arrToUse){
		var _this = this;
		var _string = '';
		var aElmss = [];
		oItem.aElms = aElmss;
		leftNavLayer.lay.empty();
		if (arrToUse.thumbMode){
			var ul = new Element('ul');
			/*var aElms = [];*/
			var aImgs = [];
			var aEls = [];
			arrToUse.each(function(item){
				aImgs.push(item.src);
				aEls.push(item);
			});
			if (arrToUse.length == 1){
				ul.style.paddingBottom = '17px';
			}else{
				ul.style.paddingBottom = '0';
			}
			if(oItem.aElms.length == 0){
				var pathImage = document.location.pathname;
				var langImage = 'es';
				switch(pathImage) {
					case '/en_EN/':
						langImage = 'en';
						break;
					case '/fr_FR/':
					default:
						langImage = 'fr';
						break;
				}

				var j = 0;
				new Asset.images(aImgs, {
					onProgress: function(i) {
						// EXECUTED WHEN ONE IMAGE IS LOADED
						if (!aEls[i]) return;
						var li = new Element('li', {'class':'imgCase'});
						var a = new Element('a', {'href':aEls[i].url});
						this.alt = aEls[i].libelle;
						li.adopt(a.adopt(this));
						oItem.aElms.push(li);
						ul.adopt(li);

						if (i == aImgs.length - 1){
							//complete
							leftNavLayer.lay.adopt(ul);
							_this.subNav(oItem.aElms);
						}
					},
					onError: function(i) {
						// EXECUTED WHEN ONE IMAGE IS FAILED
						if (!aEls[i]) return;
						var li = new Element('li', {'class':'imgCase'});
						var a = new Element('a', {'href':aEls[i].url});
						this.src = '/images/imageEmpty_' + langImage + '.png';
						this.alt = aEls[i].libelle;
						li.adopt(a.adopt(this));
						oItem.aElms.push(li);
						ul.adopt(li);

						if (i == aImgs.length - 1){
							//complete
							leftNavLayer.lay.adopt(ul);
							_this.subNav(oItem.aElms);
						}
					}
				});
				/*aImgs.each(function(oImg, i){
					var img = new Asset.image(oImg, {
						onload: function(){
							if (!aEls[i]) return;
							var li = new Element('li', {'class':'imgCase'});
							var a = new Element('a', {'href':aEls[i].url});
							//a.href = aEls[i].url;
							this.alt = aEls[i].libelle;
							li.adopt(a.adopt(this));
							oItem.aElms.push(li);
							ul.adopt(li);

							if (i == aImgs.length - 1){
								//complete
								leftNavLayer.lay.adopt(ul);
								_this.subNav(oItem.aElms);
							}
						},
						onabort: function(){
							this.src = '/images/imageEmpty_' + langImage + '.png';
						}
					});
				});*/
				_this.subNav(oItem.aElms);
			} else {
				oItem.aElms.each(function(oElm){
					ul.adopt(oElm);
				/*	_this.subNav();*/
				});

				_this.subNav(oItem.aElms);
				leftNavLayer.lay.adopt(ul);
			}
		}else{
			for (var i=0; i < arrToUse.length; i++) {
				var sClass = this.myProfile.label.toLowerCase() == arrToUse[i].label.toLowerCase() ? 'used' : '';
				_string += '<li class="modeList"><a class="' + sClass + '" href="'+ arrToUse[i].url +'" onclick="persoMenu.setCookFromObj(this);">' + arrToUse[i].label + '</a></li>';
			};
			var sList = '<ul>' + _string + '</ul>';
			leftNavLayer.lay.innerHTML = sList;
		}
	},
	subNav : function(aElms){
		var _this = this;
		leftNavLayer.layerSubCtn.layerOn = false;
		oldOne = null;
		aElms.each(function(oElm, i){
			oElm.layerOn = false;
			oElm.removeEvents();
			oElm.img = oElm.getElement('img');
			oElm.addEvent('mouseenter', function(){
				if (_this.timesubLeave) $clear(_this.timesubLeave);
				_this.timesubEnter = (function(){
					if(oElm.img) {
						$(oElm).addClass('current');
						var extension = oElm.img.src.substring(oElm.img.src.length, oElm.img.src.length - 3);
						var bigImg = oElm.img.src.substring(0, oElm.img.src.length - 4) + '_big.' + extension;
						new Asset.image(bigImg, {
							onload: function(){
								if (this){
									leftNavLayer.laySub.empty();
									var titre = new Element('h5');
									var link = new Element('a');
									link.href = oElm.getElement('a').href;
									link.adopt(this);
									titre.innerHTML = oElm.img.alt;
									leftNavLayer.laySub.adopt([link, titre]);
									leftNavLayer.openLay(oElm, 2);
									leftNavLayer.layerSubCtn.layerOn = true;
								}
							}
						});
						oElm.oldOne = oElm;
						oElm.oldOne.layerOn  = false;
						oElm.layerOn = true;
					}
				}).delay(300);
			})

			oElm.addEvent('mouseleave', function(e){
				if (_this.timesubEnter) $clear(_this.timesubEnter);
				_this.timesubLeave = (function(){
					$(oElm).removeClass('current');
					leftNavLayer.layerSubCtn.layerOn = false;
					leftNavLayer.closeSubLay();
					//leftNavLayer.laySub.empty();
					oElm.layerOn = false;
				}).delay(200)
			})
		})
	}
});

var MenuN3 = new Class({
	initialize : function(){
		var _this = this;
		if($$('#gauche .menugauche')[0] == null) return;
		this.menu = $$('#gauche .menugauche')[0];
		this.menu.items = this.menu.getElements('.sousmenu');
		this.menu.items.merge(this.menu.getElements('.sousmenuactive'));
		this.menu.items.each(function(item){
			_this.setItem(item);
		})
	},
	setItem :function(oItem){
		var _this = this;
		oItem.contenu = oItem.getElement('ul');
		if (!oItem.contenu) return;
		oItem.addEvent('mouseenter', function(){
			
			var clonedContent = oItem.contenu.clone();
			/* patch border du sous menu */
			var twoLines = (oItem.getSize().size.y > 25 ) ? true : false;
			
			if (_this.timeLeave) $clear(_this.timeLeave);
			_this.timeEnter = (function(){
				oItem.layerOn = true;
				leftNavLayer.layN3.empty();
				leftNavLayer.layN3.adopt(clonedContent);
				leftNavLayer.openLay(oItem, 3, twoLines);
			}).delay(100)
		})
		oItem.addEvent('mouseleave', function(){
			if (_this.timeEnter) $clear(_this.timeEnter);
			_this.timeLeave = (function(){
				leftNavLayer.closeLayN3();
				oItem.layerOn = false;
			}).delay(600);
		})
	}
});

/*$(window).addEvent('load', function(){
	leftNavLayer = new LayerNavg();
});*/

$(window).addEvent('domready', function(){
	leftNavLayer = new LayerNavg();
	persoMenu = new PersoMenu();
	menuSubN3 = new MenuN3();
});
