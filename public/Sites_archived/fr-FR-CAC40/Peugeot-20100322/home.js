function hideInfo() {
	if (!document.getElementsByTagName) return false;
	var divs = document.getElementsByTagName("div");
	for (var i=0; i<divs.length; i++) {
		if(divs[i].className.indexOf("home_info") == -1) continue;
		if(divs[i].className.indexOf("home_info") != -1) {
				divs[i].style.display = "none";
		}
	}
}
function turnOff() {
  var expandlnk = document.getElementById("content").getElementsByTagName("a");
  for (var i=0; i<expandlnk.length; i++) {
    if (!expandlnk[i].className) continue;
		if (expandlnk[i].className == "on") {
			expandlnk[i].className = "expander";
		}
  }
}
function turnOn() {
  var expandlnk = document.getElementById("content").getElementsByTagName("a");
	for (var i=0; i<expandlnk.length; i++ ) {
		if (!expandlnk[i].className) continue;
		if (expandlnk[i].className == "expander") {
			expandlnk[i].className = "on";
		}
	}
}
function showInfo() {
	if (!document.getElementsByTagName) return false;
	var dropdown = document.getElementById("content");
	var lnks = dropdown.getElementsByTagName("a");
	for (var i=0; i<lnks.length; i++) {
		var expandlnk = lnks[i].className.indexOf("expander");
		if(lnks[i].className.indexOf("expander") == -1) continue;
		if(lnks[i].className.indexOf("expander") != -1) {
			lnks[i].onclick = function() {
				var divs = document.getElementsByTagName("div");
				for (var i=0; i<divs.length; i++) {
					if(divs[i].className.indexOf("home_info") == -1) continue;
					if(divs[i].className.indexOf("home_info") != -1) {
							if(divs[i].style.display == "none") {
								turnOn();
								divs[i].style.display = "block";	
							} else if(divs[i].style.display == "block") {
								turnOff();
								divs[i].style.display = "none";							
							} else {
								turnOff();
								divs[i].style.display = "none";
							}
							
						}
					} 
				return true;
			}
		}
	}
}

addLoadEvent(showInfo);
//addLoadEvent(hideInfo);
