function updateStarRatingBar(id,value,voteCount,textshown,errortext,thanktext)
    {
    
    if (readStarRatingCookie(id) == null)
    {
    
    
        url = "/base/StarRating/update/"+id+"/"+value+".aspx";
       
        
       
       try{
            req = new XMLHttpRequest();
            }catch (e)
            {
            req = new ActiveXObject("Microsoft.XMLHTTP");
            
            }
            req.open("GET", url, true);
            req.send(null);                
           
           //rateBar = document.getElementById(id);
           //rateBar.style.width = value*20 +"%";
           
           
           createStarRatingCookie(id,value,100);
           if(textshown == 1)
           {
           rateText = document.getElementById('ratingtext'); 
           
           rateText.innerHTML = thanktext;
           //Calculates the new average . The function is located in m012-news-article-tools.js
           
           CalculateRating(value,voteCount)
           }
           
           }
		   else
    
		{
			if(textshown == 1)
			{
				rateText = document.getElementById('ratingtext');
				rateText.innerHTML = errortext;
			}
		}
    }
    	
    
    function createStarRatingCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readStarRatingCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

