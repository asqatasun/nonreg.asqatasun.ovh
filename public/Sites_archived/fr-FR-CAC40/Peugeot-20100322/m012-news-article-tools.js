//	FUNCTION:		To control the mouse over functionality of the ratings panel
//	ARGUMENTS:	The number of the element that the mouse is currently over
function fRatings(nActiveElement) {
	if (nTimer) {
		clearTimeout(nTimer);
	}
	for (nCount = 1; nCount <= nScoreOutOf; nCount++) {
		if (nCount <= nActiveElement) {
			document.getElementById('rating_' + nCount).className="vote";
		} else {
			document.getElementById('rating_' + nCount).className="";
		}
	}
}
//	FUNCTION: 	To begin a timout once the mouse leaves the list (in order to prevent flickering)
//	ARGUMENTS:	None
function fResetRatings(l_nCurrentRating) 
{   
    if (nCurrentRating==0)
    {
		nCurrentRating=l_nCurrentRating;
    }
	nTimer = setTimeout("fRedrawRatings()", 500);	
	
}
// FUNCTION:		To redraw the ratings panel as it was before mouse over if no vote is made
// ARGUMENTS:		None though uses two global variables (nScoreOutOf & nCurrentRating).
var nCurrentRating=0;
var nScoreOutOf=5;
var nTimer;
var nCount = 0;
//Variables are used for calculating the new average rating when the user submits the rating.
//var NewUserRating=0;
var voteCount=0;

function CalculateRating(NewUserRating,voteCount)
{    
    nCurrentRating = ((nCurrentRating * voteCount) + NewUserRating)/ (voteCount + 1); 
    //fRedrawRatings();
}
function fRedrawRatings() {
	
	
	var bIsHalf = false;	
	var nVotes = Math.round(nCurrentRating);	
	if (nVotes > nCurrentRating) {
		bIsHalf = true;
	}
	
	for (nCount = 1; nCount <= nScoreOutOf; nCount++) {
		if (nCount <= nVotes) {
			if (bIsHalf && (nCount == nVotes)) {
				document.getElementById('rating_' + nCount).className="half-vote";

			} else {
					document.getElementById('rating_' + nCount).className="vote";
				}
		} else {
				document.getElementById('rating_' + nCount).className="";
			}
	}
}
//	FUNCTION:		To open a pop-up window of specified size
//	ARGUMENTS:	url of window to be opened, width of window, height of window
function fPopUp (sUrl, nWidth, nHeight) {
	window.open(sUrl, 'NewWindow', 'width='+nWidth+', height='+nHeight+', toolbar=0, location=0, resizable=0, status=0');
}
 
