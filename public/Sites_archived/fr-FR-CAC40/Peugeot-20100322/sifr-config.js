//<[CDATA[
var gillsanslight = {
    src: '/sifr/gillsanslight.swf'
    ,ratios: [7,1.32,11,1.31,13,1.24,14,1.25,19,1.23,27,1.2,34,1.19,42,1.18,47,1.17,48,1.18,69,1.17,74,1.16,75,1.17,1.16]
  };

  sIFR.delayCSS  = true;
	sIFR.fitExactly = true;
	sIFR.forceTextTransform = true;

  // sIFR.domains = ['peugeot.com'] // Don't check for domains in this demo
  sIFR.activate(gillsanslight);
	
	sIFR.replace(gillsanslight, {
    selector: 'h2.sifred'
    ,css: {
      '.sIFR-root': { 'color': '#b6d1d8' }
    }
    ,selectable: true
		,tuneHeight: -14
  });
	sIFR.replace(gillsanslight, {
    selector: 'h1.sifredhome'
    ,css: {
      '.sIFR-root': { 'color': '#000000' }
    }
    ,selectable: true
		,tuneHeight: 0
  });
	sIFR.replace(gillsanslight, {
    selector: 'h2.sifredhome'
    ,css: {
      '.sIFR-root': { 'color': '#000000' }
    }
    ,selectable: true
		,tuneHeight: 0
  });
	sIFR.replace(gillsanslight, {
    selector: 'h3.sifredhome'
    ,css: {
      '.sIFR-root': { 'color': '#000000' }
    }
    ,selectable: true
		,tuneHeight: 0
  });
//]]>