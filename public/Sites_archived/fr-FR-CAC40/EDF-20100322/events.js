/*
	 multiple loads on a same windows load event.
*/

function addLoadListener(fn){
	/* Code for Mozilla-Gecko w3c Standards */
	if(typeof window.addEventListener != 'undefined'){
		window.addEventListener('load', fn, false);
	}
	/* Code For compatibility with Opera */
	else if(typeof document.addEventListener != 'undefined'){
		document.addEventListener('load', fn, false);
	}
	/* Code for IE */
	else if(window.attachEvent('onload') != 'function'){
		window.attachEvent('onload', fn);
	}
	/* Code for IE 5 Mac */
	else{
		var oldFn = window.onload;
		if(typeof window.onload != 'function'){
			window.onload=fn;
		}
		else{
			window.onload=function(){
				oldFn();
				fn();
			};
		}
	}
}

/*
	execution of the function;
	addLoadListener(FirstFunction);
	addLoadListener(secondFunction);
	addLoadListener(ThirdFunction);
*/



//fonction de gestion des évènements
function addEvent(elm, evType, fn, useCapture){
	//Cross browser event handling for IE5+, NS6+ and Mozilla/Gecko
	//By Scott Andrew
	if(elm.addEventListener){
		elm.addEventListener(evType, fn, useCapture);
		return true;
	}else if(elm.attachEvent){
		var r=elm.attachEvent('on'+evType, fn);
		return r;
	}else{
		elm['on'+ evType]=fn;
	}
}

