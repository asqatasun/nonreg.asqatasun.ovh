/* -------------------------------------------------------------------------------------- */
/*	=Interactif
-------------------------------------------------------------------------------------- */
/* Indiquer ici le nombre de theme */
var nombreDeTheme = 3;


//Gestion des rollOvers
objs = null;
function mainNavEvents(){
	
	if(!document.getElementsByTagName)
		return;
		
	var elements = document.getElementsByTagName("a");	
	for(var i=0; i<elements.length;i++){
		var link = elements[i];		
		if(link.className.indexOf("links") != -1){
			link.onclick = function(e){//click
				var el=(typeof event!=='undefined')? event.srcElement : e.target
				var e=(typeof event!=='undefined')? event : e
				clickNav(e,this);
				return false;
			}
			
			link.onmouseover = function(e){//over
				var el=(typeof event!=='undefined')? event.srcElement : e.target
				var e=(typeof event!=='undefined')? event : e
				mouseOverNav(e,this);
				$this = this;
				document.onmousemove = function(e){
					var el=(typeof event!=='undefined')? event.srcElement : e.target
					var e=(typeof event!=='undefined')? event : e
					mouseOutNav2(e,el, $this);				
					return false;
				}
				return false;
			}
		}
	}
	
	/****************/
	
		var all_Inputs=document.getElementsByTagName("input");
		for(var i=0; i<all_Inputs.length;i++){
			var MyInput=all_Inputs[i];
			if(MyInput.className.indexOf("rollOver") != -1 && MyInput.type=="submit"){
				MyInput.onmouseover = function(e){//over input ok
					mouseoverInput(e,this);
					return false;
				}
				MyInput.onmouseout = function(e){//over input ok
					mouseoutInput(e,this);
					return false;
				}
			}
		}
		
	/******************/
	
}

function hideLayer(){
	/*hider les layers */
	
	var elmts = document.getElementsByTagName("ul");
	for(var i=0; i<elmts.length;i++){
		var objs = elmts[i];
		if(objs.className.indexOf("noScript") != -1){
			objs.className = "";
		}
	}
	
	/*var elmts = document.getElementsByTagName("div");
	for(var i=0; i<elmts.length;i++){
		var objs = elmts[i];
		
		if(objs.className.indexOf("menuList") != -1){
			console.log(objs);
			objs.style.display = "none";
		}
	}*/
	
}

function getElementsByClass(tag, className, objet){	
	if(!objet) objet = document;	
	var elements = objet.getElementsByTagName(tag);
	var results = new Array();	
	for(var i=0; i<elements.length; i++){
		if(elements[i].className == className){
			results[results.length] = elements[i];
		}
	}
	return results;
}

function getElementsByObj(tag, obj, objet){	
	if(!objet) objet = document;	
	var elements = objet.getElementsByTagName(tag);
	for(var i=0; i<elements.length; i++){
		if(elements[i] == obj){
			return true
		}
	}
	return false;
}

function mouseOutNav2(e,el, ele1){
	if(e.originalTarget) {
		targetNode = e.originalTarget;
	} else if(e.srcElement) {
		targetNode = e.srcElement;
	}
	
	var elem = getElementsByObj(el.tagName,targetNode,ele1.parentNode);	
	
	if(!elem){		
		var ele = getElementsByClass("div","menuList",ele1.parentNode);
		ele[0].style.display = "none";
		removeClassOn(ele1);
		document.onmousemove = function(e){	return false;}
	}

	
}

function removeClassOn(obj){
	var objet = obj.parentNode;
	if(objet.className.indexOf("on") != -1) {	
		objet.className = "";
	}
}

function clickNav(e,t){		
	hideAll("menuList"); //hider les autres claque+enlever class on
	var elem = getElementsByClass("div","menuList",t.parentNode);
	elem[0].style.display = "block";
	
	var objet = t.parentNode.className;
	if(objet.indexOf("on") != -1) {	
		t.parentNode.className = "";
	}else{
		t.parentNode.className = "on";
	}
}

function mouseOverNav(e, t){
	hideAll("menuList"); //hider les autres claque+enlever class on
	var elem = getElementsByClass("div","menuList",t.parentNode);
	elem[0].style.display = "block";
	
	var objet = t.parentNode.className;
	if(objet.indexOf("on") != -1) {	
		t.parentNode.className = "";
	}else{
		t.parentNode.className = "on";
	}	
}

function hideAll(className){
	var tabs = document.getElementsByTagName("li");
	for(var i=0; i<tabs.length; i++){
		if(tabs[i].className == "on"){
			tabs[i].className="";
		}
	}	
	
	var elements = document.getElementsByTagName("div");
	for(var i=0; i<elements.length; i++){
		if(elements[i].className == className){
			elements[i].style.display = "none";
		}
	}
	return false;
}

/*** over sur input ****/

function mouseoverInput(e, ele){
	var input_tag=ele;
	if(ele.className.indexOf("btnOk") != -1) {	
		input_tag.className=input_tag.className.replace('btnOk', 'btnOkhover');
	}
}

function mouseoutInput(e, ele){
	var input_tag=ele;	
	if(ele.className.indexOf("btnOkhover") != -1){		
		input_tag.className=input_tag.className.replace('btnOkhover', 'btnOk');
	}
}

/*********************************/
/*********************************/

var d = document;

function matchAll(oElm, strTagName, strClassName){
	var arrElements = (strTagName == "*" && oElm.all)? oElm.all : oElm.getElementsByTagName(strTagName);
	var arrReturnElements = new Array();
	strClassName = strClassName.replace(/\-/g, "\\-");
	var oRegExp = new RegExp("(^|\\s)" + strClassName + "(\\s|$)");
	var oElement;
	for(var i=0; i<arrElements.length; i++){
		oElement = arrElements[i];
		if(oRegExp.test(oElement.className)){
			arrReturnElements.push(oElement);
		}
	}
	return (arrReturnElements);
}

function setUpArrows() {
	
	var global = d.getElementById('homeContent');
	var ttl = d.getElementById('ttl-level-2');
	
	var arrows = d.getElementById('arrows');
	var span = d.createElement('span');
		span.id = 'textzone';
	var ul = d.createElement('ul');
	arrows.appendChild(span);
	arrows.appendChild(ul);
	
	var textes = [];
	if (d.body.className == 'fr') {
		textes[0] = 'engagement suivant';
		textes[1] = 'engagement précédent';
	}
	else if (d.body.className == 'en') {
		textes[0] = 'next commitment';
		textes[1] = 'last commitment';
	}
		
	var src = [];
		src[0] = '/FRONT/ACCUEIL_FR/img/common/arr_r_0.png';
		src[1] = '/FRONT/ACCUEIL_FR/img/common/arr_l_0.png';
		
	for (var i = 0; i < 2; ++i) {
		var li = d.createElement('li');
		var link = d.createElement('a');
		var span = d.createElement('span');
		var text = d.createTextNode(textes[i]);
		var img = new Image();
			img.src = src[i];
			img.className = i;
		ul.appendChild(li).appendChild(span).appendChild(text);
		ul.appendChild(li).appendChild(link).appendChild(img);
		
		img.onmouseover = function() {
			if (this.src.match(/_0.png$/)) {
				this.src = this.src.replace(/_0.png$/,"_1.png");
				d.getElementById('textzone').innerHTML = this.parentNode.parentNode.getElementsByTagName('span')[0].innerHTML;
			}
			else if (this.pngsrc.match(/_0.png$/)) {
				this.src = this.pngsrc.replace(/_0.png$/,"_1.png");
				ph.hack({elm:this});
				d.getElementById('textzone').innerHTML = this.parentNode.parentNode.getElementsByTagName('span')[0].innerHTML;
			}
		};
		
		img.onmouseout = function() {
			if (this.src.match(/_1.png$/)) {
				this.src = this.src.replace(/_1.png$/,"_0.png");
				d.getElementById('textzone').innerHTML = '';
			}
			else if (this.pngsrc.match(/_1.png$/)) {
				this.src = this.pngsrc.replace(/_1.png$/,"_0.png");
				ph.hack({elm:this});
				d.getElementById('textzone').innerHTML = '';
			}
		};
		
		img.onclick = function() {
			var j = parseInt(global.className.split('-')[1], 10);
			var iter = 0;
						
			if (this.className == 1) {
				while (true)
				{
					j = (j-1 > 0 ? j-1 : nombreDeTheme);
					if (d.getElementById('theme-' + j))
					{
						break;
					}
					
					iter++;
					if(iter > nombreDeTheme)
					{
						j = -1;
						break;
					}
				}
			}
			else if (this.className == 0) {
				while (true)
				{
					j = (j+1 <= nombreDeTheme ? j+1 : 1);
					if (d.getElementById('theme-' + j))
					{
						break;
					}
					
					iter++;
					if(iter > nombreDeTheme)
					{
						j = -1;
						break;
					}
				}
			}
			
			if (j > 0) {
				global.className = 'theme-' + j;
				ttl.getElementsByTagName('div')[0].className = 'theme-' +  j;
			}
			
			return false;
		};
	}
}


function changeSrc(x) {
	var menu = d.getElementById('ttl-level-2');
	
	var item = menu.getElementsByTagName('img');
	for (var i = 0; i < item.length; i++) {
		item[i].onmouseover = function() {
			if (this.src.match(/_0.png$/)) {
				this.src = this.src.replace(/_0.png$/,"_1.png");
			}
			else if (this.pngsrc.match(/_0.png$/)) {
				this.src = this.pngsrc.replace(/_0.png$/,"_1.png");
				ph.hack({elm:this});
			}
		};
		item[i].onmouseout = function() {
			if (this.src.match(/_1.png$/)) {
				this.src = this.src.replace(/_1.png$/,"_0.png");
			}
			else if (this.pngsrc.match(/_1.png$/)) {
				this.src = this.pngsrc.replace(/_1.png$/,"_0.png");
				ph.hack({elm:this});
			}
		};
	}
}



/*********************************/
/*********************************/

function initAll(){
	hideLayer();
	mainNavEvents();
	changeSrc('ttl'); // over image title
	setUpArrows();
	if (!isBICM)
	{
		/*
		so = new SWFObject("/FRONT/ACCUEIL_FR/swf/loader.swf", "flashID", "1000", "600", "8", "#FFFFFF");
		so.useExpressInstall('/FRONT/ACCUEIL_FR/swf/expressinstall.swf');
		so.addVariable("confFile", "/FRONT/ACCUEIL_FR/xml/"+langue+"/conf.xml");
		so.addParam("allowScriptAccess", "always");
		so.addParam("scale","noscale");
		so.addParam("salign", "t");
		so.write("homeContent");
		*/
		
		var DateRandom = new Date();

		so = new SWFObject("/FRONT/ACCUEIL_FR/swf/loader.swf", "flashID", "1000", "600", "8", "#FFFFFF");
		so.useExpressInstall('/FRONT/ACCUEIL_FR/swf/expressinstall.swf');
		so.addVariable("confFile", "/FRONT/ACCUEIL_FR/xml/"+langue+"/conf.xml?random="+DateRandom.getTime());
		so.addParam("allowScriptAccess", "always");
		so.addParam("scale","noscale");
		so.addParam("salign", "t");
		so.addVariable("random", DateRandom.getTime());
		so.write("homeContent");
	}
	// handle IE PNG Hack
	if (false /*@cc_on || @_jscript_version < 5.7 @*/)
	{
		try {
			ph = PNGHack('/FRONT/ACCUEIL_FR/img/spacer.gif');
			ph.remotehost = false;
			ph.crush();
		}catch(e){}
	}
}



// Dean Edwards/Matthias Miller/John Resig

function init() {
  // quit if this function has already been called
  if (arguments.callee.done) return;

  // flag this function so we don't do the same thing twice
  arguments.callee.done = true;

  // kill the timer
  if (_timer) clearInterval(_timer);

  // do stuff
  initAll();
  
  
};

/* for Mozilla/Opera9 */
if (document.addEventListener) {
  document.addEventListener("DOMContentLoaded", init, false);
}

/* for Internet Explorer */
/*@cc_on @*/
/*@if (@_win32)
  document.write("<script id=__ie_onload defer src=javascript:void(0)><\/script>");
  var script = document.getElementById("__ie_onload");
  script.onreadystatechange = function() {
    if (this.readyState == "complete") {
      init(); // call the onload handler
    }
  };
/*@end @*/

/* for Safari */
if (/WebKit/i.test(navigator.userAgent)) { // sniff
  var _timer = setInterval(function() {
    if (/loaded|complete/.test(document.readyState)) {
      init(); // call the onload handler
    }
  }, 10);
}

/* for other browsers */
addLoadListener(initAll);



