var curSelectedLink = 0
var linkToPicIndexPhase = 0
var linkNumber = 7
var curentPicIndex = 0
var urlPictureTab = []
var descPictureTab = []

var rotate_delay = 2000; // delay in milliseconds (5000 = 5 secs)
var started = 0;

function nextPicture()
{
	if(curentPicIndex < (urlPictureTab.length - 1))
	{
		if(curSelectedLink < (linkNumber - 1) )
		{
			changeSelectedIndex(curSelectedLink + 1)
		}
		else
		{
			moveButtons(1)
			SwitchToPicture(curentPicIndex + 1)
		}
	}
	var nextLink = document.getElementById("next");
	nextLink.href = "#";
	return 0;
}

function previousPicture()
{
	if(curentPicIndex > 0)
	{
		if(curSelectedLink > 0 )
		{
			changeSelectedIndex(curSelectedLink - 1)
		}
		else
		{
			moveButtons(-1)
			SwitchToPicture(curentPicIndex - 1)
		}
	}
	var previousLink = document.getElementById("previous");
	previousLink.href = "#";
	return 0;
}

function changeSelectedIndex(newSelectedLink)
 {
 	var oldLinkId = "link".concat(curSelectedLink)
	var oldLink = document.getElementById(oldLinkId)
	var newLinkId = "link".concat(newSelectedLink)
	var newLink = document.getElementById(newLinkId)
	newLink.className = "box-diaporama-center-left-img-button-selected"	
	oldLink.className = "box-diaporama-center-left-img-button-unselected"
	newLink.href = "#"
	curSelectedLink = newSelectedLink
	SwitchToPicture(newSelectedLink + linkToPicIndexPhase)
	return 0;
 }

function SwitchToPicture(picIndex)
{
	if(document.getElementById("diapo"))
	{
		var diapo
		diapo = document.getElementById("diapo")
		diapo.src = urlPictureTab[picIndex]
		diapo.alt = descPictureTab[picIndex]
		curentPicIndex = picIndex
	}
	return 0;
}
	

function moveButtons(direction)
{	
	
	if(direction > 0)
	{	
		linkToPicIndexPhase = linkToPicIndexPhase + 1
		for(var i = 0; i < linkNumber; i++){
			var tmp_imput_id
			tmp_imput_id = "link".concat(i)
			var tmpInput
			tmpInput = document.getElementById(tmp_imput_id)
			tmpInput.innerHTML = linkToPicIndexPhase + i + 1
			tmpInput.title= title + ( linkToPicIndexPhase + i + 1 )
		}
	}
	if(direction < 0)
	{	
		linkToPicIndexPhase = linkToPicIndexPhase - 1
		for(var i = 0; i < linkNumber; i++){
			var tmp_imput_id
			tmp_imput_id = "link".concat(i)
			var tmpInput
			tmpInput = document.getElementById(tmp_imput_id)
			tmpInput.innerHTML = linkToPicIndexPhase + i + 1
			tmpInput.title= title + ( linkToPicIndexPhase + i + 1 )
		}
	}
	return 0;
}	


var b;
var isDiaporamSarted = false;

function startDiaporama()
{
	if( !isDiaporamSarted )
	{
		isDiaporamSarted = true;
		slideNext();
		b = setInterval("slideNext()",rotate_delay);	
	}
	return 0;
}

function stopDiaporama()
{
	if( isDiaporamSarted )
	{
		clearTimeout(b);
		isDiaporamSarted = false;
	}
	return 0;
}

function slideNext() {
	//alert("slideNext");
	if (curentPicIndex < urlPictureTab.length -1) {
		SwitchToPicture(curentPicIndex + 1)
	}
	else slideFirst();
	return 0;
}

function slideFirst() {
	//alert("slideFirst");
	curentPicIndex = 0;
	SwitchToPicture(curentPicIndex)
	return 0;
}

