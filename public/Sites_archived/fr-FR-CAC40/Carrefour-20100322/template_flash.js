function write_open_object_tag (src,id, width, eight, altContentUrl,altContentTitle) {
	var ooTag = '<object data="'+src+'" ' 
		+ 'width="'+width+'" height="'+eight+'"'
		+ ' name="flashmovie" id="'+id+'"'
		+ 'type="application/x-shockwave-flash">'
		+ '<param name="movie" value="'+src+'">'
		+ '<param name="play" value="true">'   	
		+ '<param name="quality" value="high">'
		+ '<param name="menu" value="false">'   
		+ '<param name="bgcolor" value="#ffffff" />	' 
		+ '<a class="alt_flash"  href="'+altContentUrl+'" >'+altContentTitle+'</a>'
		+ '</object>'  
		+ '<br><a id="' + id + '_alt" class="alt_flash"  href="'+altContentUrl+'" >'+altContentTitle+'</a>';
	document.write(ooTag);
	showFlash(id);
}

function write_open_object_tag_footer (src,id, width, eight, altContentUrl,altContentTitle) {
	var ooTag = '<object data="'+src+'" ' 
		+ 'width="'+width+'" height="'+eight+'"'
		+ ' name="flashmovie" id="'+id+'"'
		+ 'type="application/x-shockwave-flash">'
		+ '<param name="movie" value="'+src+'">'
		+ '<param name="play" value="true">'   	
		+ '<param name="quality" value="high">'
		+ '<param name="menu" value="false">'   
		+ '<param name="bgcolor" value="#ffffff" />	' 
		+ '</object>'  
		+ '<br><a id="' + id + '_alt" class="alt_flash"  href="'+altContentUrl+'" >'+altContentTitle+'</a>';
	document.write(ooTag);
	showFlash(id);
}

function hydeFlash(id) {
	var flashmovie = window.document.getElementById(id);
	var altFlash = window.document.getElementById(id + "_alt");
	flashmovie.style.display = "none";
	altFlash.style.display = "";
}

function showFlash(id) {
	var flashmovie = window.document.getElementById(id);
	var altFlash = window.document.getElementById(id + "_alt");
	flashmovie.style.display = "";
}

