//Code de collecte XiTi pour Carrefour.com
//M.MORENO 19/02/2010
//Version 1.1d

// Plan de marquage --> Niveau 2 --------------------------------------------------------------------------

	// 1 - Accueil
	// 2 - Groupe
	// 3 - Finance
	// 4 - Emploi
	// 5 - Commerce_Responsable
	// 6 - Fondation
	// 7 - Presse
	// 8 - Client
	// 9 - Resultats_moteur
	// 10 - Pages_annexes
	// 11 - Pages_erreurs


// Variables Xiti ------------------------------------------------------------------------------------------

	var xtnv = document; //parent.document or top.document or document 
	var xtsdi = "" // pixel transparent 1x1 
	var xtsd = "http://logc174";
	var xtsite = "";
	var xtdi = "0"; //implication degree

// Autres variables -----------------------------------------------------------------------------------------

	var vxtSiteName = "";
	var vxtLevel2Id = "0";
	var vxtLevel2Name = "";
	var vxtChapter = "";
	var vxtSubChapters = "";
	var vxtPageTitle = "" ;
	var vxtLang = "en";
	var vxtPageName = "";
	var vxtPageTitle = document.title;
	var vxtMaxIndex = "";
	var vxtdomain = "";
	
	
// Nettoyage des chaines de caractères ----------------------------------------------------------------------
	
	function CleanString(vsString)
	{	
	eval(unescape("%76%73%53%74%72%69%6E%67%3D%76%73%53%74%72%69%6E%67%2E%72%65%70%6C%61%63%65%28%2F%5B%E0%E1%E2%E3%E4%E5%E6%5D%2F%67%69%2C%22%61%22%29%3B%20%76%73%53%74%72%69%6E%67%3D%76%73%53%74%72%69%6E%67%2E%72%65%70%6C%61%63%65%28%2F%5B%E8%E9%EA%EB%5D%2F%67%69%2C%22%65%22%29%3B%20%76%73%53%74%72%69%6E%67%3D%76%73%53%74%72%69%6E%67%2E%72%65%70%6C%61%63%65%28%2F%5B%EC%ED%EE%EF%5D%2F%67%69%2C%22%69%22%29%3B%20%76%73%53%74%72%69%6E%67%3D%76%73%53%74%72%69%6E%67%2E%72%65%70%6C%61%63%65%28%2F%5B%F2%F3%F4%F5%F6%F8%5D%2F%67%69%2C%22%6F%22%29%3B%20%76%73%53%74%72%69%6E%67%3D%76%73%53%74%72%69%6E%67%2E%72%65%70%6C%61%63%65%28%2F%5B%F9%FA%FB%FC%5D%2F%67%69%2C%22%75%22%29%3B%20%76%73%53%74%72%69%6E%67%3D%76%73%53%74%72%69%6E%67%2E%72%65%70%6C%61%63%65%28%2F%5B%E7%5D%2F%67%69%2C%22%63%22%29%3B%20%76%73%53%74%72%69%6E%67%3D%76%73%53%74%72%69%6E%67%2E%72%65%70%6C%61%63%65%28%2F%5B%FF%FD%FE%5D%2F%67%69%2C%22%79%22%29%3B%20%76%73%53%74%72%69%6E%67%3D%76%73%53%74%72%69%6E%67%2E%72%65%70%6C%61%63%65%28%2F%5B%F1%5D%2F%67%69%2C%22%6E%22%29%3B%20%76%73%53%74%72%69%6E%67%3D%76%73%53%74%72%69%6E%67%2E%72%65%70%6C%61%63%65%28%2F%20%2F%67%69%2C%22%5F%22%29%3B"));
	vsString = vsString.toLowerCase();
	return vsString;

	}


//Recuperation niveau 2 et des sous niveaux (chapitres, sous-chapitres ...) ----------------------------------
	
	function GetSiteNiveau2(pageTitle)
	{
		
		try
		{
			var vxtArray1 = document.URL.split("/");
			vxtMaxIndex = vxtArray1.length - 1;
			if (vxtArray1[4]=="cdc") 
			{ 
				var valx = 1;
			}
			else
			{ 	
				var valx = 0;
			}

			var val0 = 0 + valx;
			var val1 = 1 + valx;
			var val2 = 2 + valx;
			var val3 = 3 + valx;
			var val4 = 4 + valx;
			var val5 = 5 + valx;
			var val6 = 6 + valx;
			var val7 = 7 + valx;
			var val8 = 8 + valx;
			var val9 = 9 + valx;
			var val10 = 10 + valx;
			
			// Initialisation de xtsite en fonction du nom de domaine.
			vxtdomain=vxtArray1[val2];
			
			// Site de Prod Carrefour.com
			if (vxtdomain == "www.carrefour.com" || vxtdomain == "carrefour.com")
			{
				xtsite="431132"; // env. Prod XiTi
				vxtSiteName="C4Prod";
			}
			else
			{
				// Site de prod Fondation
				if (vxtdomain == "www.fondation-internationale-carrefour.org" || vxtdomain == "fondation-internationale-carrefour.org")
				{
					xtsite="447239"; // env. Prod XiTi
					vxtSiteName="FondProd";
				}
				else
				{
					//Site de Dev ou Pre-Prod Fondation
					if (vxtArray1[val3] == "fic")
					{
						xtsite="447240"; // env. Prod XiTi
						vxtSiteName="FondTest";
					}
					// Site de Dev ou Pre-Prod Carrefour.com
					else
					{
						xtsite = "430256"; // env. Test XiTi
						vxtSiteName="C4Test";
					}
				}
			}
			
			// Recuperation de la rubrique principale
			vxtLevel2Name = vxtArray1[val4];
			if (vxtLevel2Name == null) {vxtLevel2Name = "home";}
			
			if (vxtMaxIndex > val5 ) { vxtChapter = vxtArray1[val5];}
			vxtPageName = vxtArray1[vxtMaxIndex];
					
			var vxtArray2 = vxtPageTitle.split(" - ");
			vxtPageTitle = vxtArray2[val0];
			vxtPageTitle = CleanString(vxtPageTitle);

			
			for (i=val6;i<vxtMaxIndex;i++)
			{
				if (i==val6) 
				{ 
					vxtSubChapters = vxtArray1[i];
				}
				else
				{
					vxtSubChapters = vxtSubChapters + "::" + vxtArray1[i];
				}
			}
			
			// Sites de Niveau2 de Carrefour.com ----------------------------------------------------------------------
			if (vxtSiteName == "C4Prod" || vxtSiteName == "C4Test")
			{
			
				if(vxtLevel2Name == "accueil" || vxtLevel2Name == "home" )
				{
					vxtLevel2Id = "1";
				}
				
				if(vxtLevel2Name == "groupe" || vxtLevel2Name == "group" )
				{
					vxtLevel2Id = "2";
					if(vxtLang == "en")
					{
						if(vxtChapter == "current-news") { vxtChapter = "notre-actualite";}
						if(vxtChapter == "our-group") { vxtChapter = "notre-groupe";}
						if(vxtChapter == "point-of-view") { vxtChapter = "point-de-vue";}
						if(vxtChapter == "our-business") { vxtChapter = "nos-activites";}
						if(vxtChapter == "our-values") { vxtChapter = "nos-valeurs";}
						if(vxtChapter == "our-strategy") { vxtChapter = "notre-strategie";}
						if(vxtChapter == "governance-structure") { vxtChapter = "gouvernance";}
						if(vxtChapter == "history") { vxtChapter = "historique";}
					}
				}
				else
				if(vxtLevel2Name == "finance-fr" || vxtLevel2Name == "finance")
				{
					vxtLevel2Id = "3";
					if(vxtLang == "en")
					{
						if(vxtChapter == "key-figures") { vxtChapter = "chiffres-cles";}
						if(vxtChapter == "sales-and-results") { vxtChapter = "chiffres-d-affaires-et-resultats";}
						if(vxtChapter == "publications-and-presentations") { vxtChapter = "publications-et-presentations";}
						if(vxtChapter == "carrefour-share") { vxtChapter = "action-carrefour";}
						if(vxtChapter == "calendar") { vxtChapter = "agenda";}
						if(vxtChapter == "shareholder-s-corner") { vxtChapter = "espace-actionnaire";} 
					}
				}
				else
				if(vxtLevel2Name == "emploi" || vxtLevel2Name == "careers")
				{
					vxtLevel2Id = "4";
					if(vxtLang == "en")
					{
						if(vxtChapter == "our-project") { vxtChapter = "metiers-et-carrieres";}
						if(vxtChapter == "employment-opportunities") { vxtChapter = "nos-offres";} 
					}
				}
				else
				if(vxtLevel2Name == "commerce-responsable" || vxtLevel2Name == "responsible-commerce")
				{
					vxtLevel2Id = "5";
					if(vxtLang == "en")
					{
						if(vxtChapter == "our-approach") { vxtChapter = "notre-demarche";}
						if(vxtChapter == "our-organization-and-policy") { vxtChapter = "notre-organisation-et-notre-politique";}
						if(vxtChapter == "product-safety-and-quality") { vxtChapter = "securite-et-qualite-des-produits";}
						if(vxtChapter == "our-nutrition-policy") { vxtChapter = "notre-politique-nutrition";}
						if(vxtChapter == "our-commitment-to-the-environment") { vxtChapter = "notre-engagement-pour-l-environnement";}
						if(vxtChapter == "our-social-and-ethical-approach") { vxtChapter = "nos-demarches-ethiques-et-sociales";}
						if(vxtChapter == "solidarity-actions") { vxtChapter = "nos-actions-solidaires";}
						if(vxtChapter == "evaluation-of-our-overall-performance") { vxtChapter = "evaluation-de-notre-performance-globale";}
						if(vxtChapter == "sustainibility-report") { vxtChapter = "rapports-developpement-durable";}
						if(vxtChapter == "sri-community") { vxtChapter = "espace-isr-_investissement-socialement-responsable_";} 
					}
				}
				else
				if(vxtLevel2Name == "fondation" || vxtLevel2Name == "foundation")
				{
					vxtLevel2Id = "6";
					if(vxtLang == "en")
					{
						if(vxtChapter == "the-carrefour-corporate-international-foundation") { vxtChapter = "la-fondation-d-entreprise-internationale-carrefour";}
					}
				}
				else
				if(vxtLevel2Name == "presse" || vxtLevel2Name == "press")
				{
					vxtLevel2Id = "7";
					if(vxtLang == "en")
					{
						if(vxtChapter == "releases") { vxtChapter = "les-communiques";}
						if(vxtChapter == "press-contact") { vxtChapter = "contact-presse";}
						if(vxtChapter == "group-presentation") { vxtChapter = "presentation-groupe";}
						if(vxtChapter == "photograph-library") { vxtChapter = "phototheque"} 
						else { vxtChapter = "non-classees" ;} 
					}
				}
				else
				if(vxtLevel2Name == "client" || vxtLevel2Name == "customer")
				{
					vxtLevel2Id = "8";
					vxtChapter = "client";
				}
				else
				if(vxtLevel2Name == "aide" 
				   	|| vxtLevel2Name == "help" 
				   	|| vxtLevel2Name == "contacts-fr" 
				   	|| vxtLevel2Name == "contacts"
					|| vxtLevel2Name == "infos-legales"
					|| vxtLevel2Name == "legal-infos"
					|| vxtLevel2Name == "panier-d-impression"
					|| vxtLevel2Name == "print-basket"
					|| vxtLevel2Name == "rss-fr"
					|| vxtLevel2Name == "rss"
					|| vxtLevel2Name == "plan-du-site"
					|| vxtLevel2Name == "site-map"			   
				   )
				{
					vxtLevel2Id = "9";
					vxtChapter = "pages-annexes";
				}
				else
				if(vxtLevel2Name == "moteur-de-recherche" || vxtLevel2Name == "search-engine")
				{
					vxtLevel2Id = "10";
					vxtChapter = "moteur-de-recherche";
				}
				else
				if(this.title=="Erreur - page introuvable")
				{
					vxtLevel2Id = "11";
					vxtChapter = "erreur";
				}
				
			}
			// Sites de Niveau 2 de Fondation. ----------------------------------------------------------------------
			else
			{
			
				if(vxtLevel2Name == "accueil" || vxtLevel2Name == "home" || vxtLevel2Name == "inicio")
				{
					vxtLevel2Id = "1";
				}
				
				if(vxtLevel2Name == "fondation" || vxtLevel2Name == "foundation" || vxtLevel2Name == "fundacion")
				{
					vxtLevel2Id = "2";
				}
				
				if(vxtLevel2Name == "actions-fr" || vxtLevel2Name == "actions" || vxtLevel2Name == "acciones")
				{
					vxtLevel2Id = "3";
				}
				
				if(vxtLevel2Name == "partenaires" || vxtLevel2Name == "partners" || vxtLevel2Name == "socios")
				{
					vxtLevel2Id = "4";
				}
				
				if(vxtLevel2Name == "benevolat" || vxtLevel2Name == "volunteering" || vxtLevel2Name == "voluntariado")
				{
					vxtLevel2Id = "5";
				}
				
				if(vxtLevel2Name == "actions-solidarite-groupe" || vxtLevel2Name == "action-solidarity-group" || vxtLevel2Name == "acciones-de-solidaridad-grupo")
				{
					vxtLevel2Id = "6";
				}	
				
			}
	
		}
		catch(e)
		{
			//alert("Error : " + e.description);
			vxtLevel2Id = "0";
			vxtChapter = "";
			vxtSubChapters = "";
		;}

		return vxtLevel2Id;
	}

// Fin initialisation ---------------------------------------------------------------------------------------

	var xtn2 = GetSiteNiveau2(); // level 2 site 
	var xtpage = "";
	
	
	if(vxtChapter != "") 
	{
		if (vxtSubChapters != "")
		{
			xtpage = vxtChapter + "::" + vxtSubChapters + "::" + vxtPageTitle;
		}
		else
		{
			xtpage = vxtChapter + "::" + vxtPageTitle;
		}
		
	}
	else
	{
		xtpage = vxtPageTitle;
	}
