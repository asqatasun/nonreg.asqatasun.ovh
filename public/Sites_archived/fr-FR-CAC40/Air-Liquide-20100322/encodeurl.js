function AsciiToHexa(sCaractere){
	var sDecCode;
	var firstcode;
	var secondcode;
	var sHexacode='';

	sDecCode = sCaractere.charCodeAt(0);
	firstcode = (Number(sDecCode) - Number(Number(sDecCode) % 16))/16;
	secondcode = Number(Number(sDecCode) % 16)

	if (firstcode<=9){
		sHexacode = ''+firstcode;
 	} else{
		if (firstcode == 10){
			sHexacode = 'A';
		} else if (firstcode == 11){
			sHexacode = 'B';
		} else if (firstcode == 12){
			sHexacode = 'C';
		} else if (firstcode == 13){
			sHexacode = 'D';
		} else if (firstcode == 14){
			sHexacode = 'E';
		} else if (firstcode == 15){
			sHexacode = 'F';
		}
	}
	if (secondcode<=9){
		sHexacode += ''+secondcode;
 	} else{
		if (secondcode == 10){
			sHexacode += 'A';
		} else if (secondcode == 11){
			sHexacode += 'B';
		} else if (secondcode == 12){
			sHexacode += 'C';
		} else if (secondcode == 13){
			sHexacode += 'D';
		} else if (secondcode == 14){
			sHexacode += 'E';
		} else if (secondcode == 15){
			sHexacode += 'F';
		}
	}
	return(sHexacode);	
}

function encodeUrl(sUrl){
	var i;
	var newUrl='';
	var charcode;

	for (i = 0; i < sUrl.length; i++) 
	{
		charcode = Number(sUrl.charCodeAt(i));

		if (((charcode>=97) && (charcode<=122)) || ((charcode>=65) && (charcode<=90)) || (charcode==46) ){
			newUrl = newUrl + sUrl.charAt(i);
		}
		else{
			if (sUrl.charAt(i) == ' '){
				newUrl = newUrl + '+';
			}
			else{
				newUrl = newUrl + '%' + AsciiToHexa(sUrl.charAt(i));			
			}
		}
	}
	return(newUrl);
}
function replaceUrl(sUrl,scarIn,schaineOut){
	var i;
	var newUrl='';
	for (i = 0; i < sUrl.length; i++) 
	{
		if (sUrl.charAt(i) == scarIn){
			newUrl = newUrl + schaineOut;
		}
		else{
			newUrl = newUrl + sUrl.charAt(i);
		}
	}
	return(newUrl);
}
