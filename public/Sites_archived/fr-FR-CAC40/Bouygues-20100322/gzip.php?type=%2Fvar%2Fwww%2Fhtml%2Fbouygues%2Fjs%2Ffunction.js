function getMenuContainerHeight()
{
	var intElemOffsetHeight = document.getElementById("MenuGaucheTaille").offsetHeight;
	var elem 				= document.getElementById("col-centre");
	elem.style.height 		= intElemOffsetHeight+'px' ;
}
/* AUGMENTER LA TAILLE DE LA POLICE */
var t_taille;			
t_taille = 1;
function changerTaille(modif) {
	t_taille = t_taille + modif;
	document.getElementsByTagName("body")[0].style.fontSize = t_taille + "em";
}

function m_opacity (id_menu, classe) {
	document.getElementById(id_menu).className = classe;

}
function no_opacity (id_menu, classe) {
	document.getElementById(id_menu).className = classe;
}

var doBeforeSoumettre = new Array();
function soumettre (lang) {
	
	// Execution de fonctions avant de soumettre un formulaire
	if (typeof doBeforeSoumettre !== undefined ) {
		try  {
			for (var i=0; i<doBeforeSoumettre.length; i++) {
				doBeforeSoumettre[i].apply();
			}
		}		
		catch( e ){
			alert('Une fonction qui devrait s executer avant la soummission du formulaire a leve une exeption : ' + e.message);
		}
	}
	
	
	if( validateForm( 'main_form',lang ) ){
		var oSelect = document.getElementById('main_form').getElementsByTagName("SELECT");
		for(var i=0;i<oSelect.length;i++){
			if(oSelect[i].multiple==true){
				oSelect[i].name = oSelect[i].name+'[]';
				if(oSelect[i].className=='selector'){
					NbCol1 = oSelect[i].length;
					// On lance une boucle pour selectionner tous les items
					for(a=0; a<NbCol1; a++){
						oSelect[i].options[a].selected = true;
					}
				}
			}
		}
		var oInput = document.getElementById('main_form').getElementsByTagName("INPUT");
		for(var i=0;i<oInput.length;i++){
			if(oInput[i].type=="checkbox"){
				oInput[i].name = oInput[i].name+'[]';
			}
		}
		document.getElementById('main_form').submit();
	}
}
function popupWindow(url, largeur, hauteur)
{
	var top=(screen.height-hauteur)/2;
	var left=(screen.width-largeur)/2;
	window.open(url, "popup","top="+top+",left="+left+",width="+largeur+",height="+hauteur+",toolbar=no,location=no,scrollbar=no,status=no,menubar=no'");
}

function PopupCentrer(page,largeur,hauteur,options) {
	var top=(screen.height-hauteur)/2;
	var left=(screen.width-largeur)/2;
	window.open(page,"","top="+top+",left="+left+",width="+largeur+",height="+hauteur+","+options);
}
function popFlash(up){
	new Effect.Fade(document.getElementById(up));
}
function appendOptionLast(selectName,text,value)
{
	var elOptNew = document.createElement('option');
	elOptNew.text = text;
	elOptNew.value = value;
	var elSel = document.getElementById(selectName);

	try {
		elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
	}
	catch(ex) {
		elSel.add(elOptNew); // IE only
	}
}

function removeOptionLast(selectName)
{
	var elSel = document.getElementById(selectName);
	if (elSel.length > 0)
	{
		elSel.remove(elSel.length - 1);
	}
}