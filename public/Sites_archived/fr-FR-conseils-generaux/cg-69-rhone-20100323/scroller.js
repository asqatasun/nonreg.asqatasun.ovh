function infos_flash() {

	infos= $A($('pscroller1').getElementsByTagName('div'));
	
	pausecontent=[];
	compteur = 0;
	
	infos.each(function(infos) {
		
	if(compteur >= 2) {

		Element.toggle(infos) ;
			
	}
		
		pausecontent[compteur] = infos.innerHTML ;
		compteur +=1 ;
				
	});

	$("pscroller1").setStyle({
		height:'70px',
		overflow: 'hidden'
	});

	new pausescroller(pausecontent,"pscroller1", "someclass", 3000);

}

function pausescroller(pausecontent,divId, divClass, delay){

	this.tickerid=divId ; //ID of ticker div to display information =====> pscroller
	this.delay=delay ; //Delay between msg change, in miliseconds.
	this.mouseoverBol=0 ; //Boolean to indicate whether mouse is currently over scroller (and pause it if it is)
	this.hiddendivpointer=1 ;//index of message array for hidden div
	this.content=pausecontent ; //message array content

	var scrollerinstance=this;
	
	scrollerinstance.initialize();
	
}

// -------------------------------------------------------------------
// initialize()- Initialize scroller method.
// -Get div objects, set initial positions, start up down animation
// -------------------------------------------------------------------

pausescroller.prototype.initialize=function(){

	this.tickerdiv=$(this.tickerid) ;
	this.visiblediv=$(this.tickerid+"1") ;	
	this.hiddendiv=$(this.tickerid+"2") ;
	
	this.visibledivtop=parseInt(pausescroller.getCSSpadding(this.tickerdiv));

	//set width of inner DIVs to outer DIV's width minus padding (padding assumed to be top padding x 2)
	this.visiblediv.style.width=this.hiddendiv.style.width;
	this.hiddendiv.style.width=this.tickerdiv.offsetWidth-(this.visibledivtop*2)+"px";
	this.getinline(this.visiblediv, this.hiddendiv);
	
	this.hiddendiv.style.visibility="visible" ;

	var scrollerinstance=this ;

	$(this.tickerid).onmouseover=function(){scrollerinstance.mouseoverBol=1}
	$(this.tickerid).onmouseout=function(){scrollerinstance.mouseoverBol=0}

	if (window.attachEvent) //Clean up loose references in IE
	window.attachEvent("onunload", function(){scrollerinstance.tickerdiv.onmouseover=scrollerinstance.tickerdiv.onmouseout=null})
	
	setTimeout(function(){scrollerinstance.animateup()}, this.delay);
	
}


// -------------------------------------------------------------------
// animateup()- Move the two inner divs of the scroller up and in sync
// -------------------------------------------------------------------

pausescroller.prototype.animateup=function(){

	var scrollerinstance=this;

	if (parseInt(this.hiddendiv.style.top)>(this.visibledivtop)){
		this.visiblediv.style.top=parseInt(this.visiblediv.style.top) - 5 + "px";
		this.hiddendiv.style.top=parseInt(this.hiddendiv.style.top)-5+"px";
		setTimeout(function(){scrollerinstance.animateup()}, 50);
	}else{
	
		this.getinline(this.hiddendiv, this.visiblediv);
		this.swapdivs();
		setTimeout(function(){scrollerinstance.setmessage()}, this.delay);
	
	}
}

// -------------------------------------------------------------------
// swapdivs()- Swap between which is the visible and which is the hidden div
// -------------------------------------------------------------------

pausescroller.prototype.swapdivs=function(){

	var tempcontainer=this.visiblediv;
	
	this.visiblediv=this.hiddendiv;
	this.hiddendiv=tempcontainer;

}

	pausescroller.prototype.getinline=function(div1, div2){
	
		div1.style.top=this.visibledivtop+"px" ;
		div2.style.top=Math.max(div1.parentNode.offsetHeight, div1.offsetHeight)+"px" ;

}

// -------------------------------------------------------------------
// setmessage()- Populate the hidden div with the next message before it's visible
// -------------------------------------------------------------------

pausescroller.prototype.setmessage=function(){

	var scrollerinstance=this

	if (this.mouseoverBol==1) //if mouse is currently over scoller, do nothing (pause it)

		setTimeout(function(){scrollerinstance.setmessage()}, 100)

	else{

		var i=this.hiddendivpointer
		var ceiling=this.content.length

		this.hiddendivpointer=(i+1>ceiling-1)? 0 : i+1
		this.hiddendiv.innerHTML=this.content[this.hiddendivpointer]
		this.animateup()

	}

}

pausescroller.getCSSpadding=function(tickerobj){ //get CSS padding value, if any

	if (tickerobj.currentStyle){
	
		return tickerobj.currentStyle["paddingTop"] ;
	
	} else if (window.getComputedStyle) { //if DOM2
	
		return window.getComputedStyle(tickerobj, "").getPropertyValue("padding-top") ; 
	
	} else {
	
		return 0 ;
	
	}
}
