/**
 * Initialise le script
 */
function dTab_init()
{
	var i = 1;
	// pour chaque dTabs
	while ( dtab = document.getElementById("dTab" + i) ) {
		var n_actif = 1;
		var liste = document.getElementById("dTab" + i + "-0");
		if ( ! liste )
			continue;
		// rendre cliquable
		var li = liste.getElementsByTagName("LI");
		for ( var j = 0; j < li.length; j++ ) {
			if ( li[j].className.indexOf("actif") >= 0 )
				n_actif = j + 1;
				
			li[j].firstChild.onclick = function() {
				if ( this.className != "actif" ) {
					var div = getParent(this,"DIV");
					var id = div.id.substr(0,div.id.indexOf("-"));
					
					var n = dTab_getPos(id, this);
					
					dTab_hideAll(id);
					dTab_show(id, n);
				}
				return false;
			}
			
		}
		dTab_hideAll("dTab" + i);
		dTab_show("dTab" + i, n_actif); // par d�faut on affiche le premier
		
		i++;
	}
}

function dTab_getPos(id, elt)
{
	var theLi = elt.parentNode;
	var liste = document.getElementById(id + "-0");
	var li = liste.getElementsByTagName("LI");
	for ( var n = 0; n < li.length; n++ ) {
		if ( li[n] == theLi )
			return n + 1;
	}
	return -1;
}

function dTab_show(id, n)
{
	var dtab = document.getElementById(id);
	if ( ! dtab ) return;
	
	// r�cuperer la liste
	var liste = document.getElementById(id+ "-0");
	// r�cuperer le tab
	var tab = document.getElementById(id + "-" + n);
	
	if ( tab && liste ) {
		var li = liste.getElementsByTagName("LI");
		tab.style.display = "block";
		li[n-1].className = "actif";
	}
}

function dTab_hideAll(id)
{
	var dtab = document.getElementById(id);
	if ( ! dtab ) return;
	
	// récuperer la collection des LI représentant la liste des tabs
	var tab = document.getElementById(id + "-0");
	var li = tab.getElementsByTagName("LI");
	// traiter les divs (les masquer tous)
	var j = 1;
	while ( tab = document.getElementById(id + "-" + j) ) {
		tab.style.display = "none"; 
		li[j-1].className = "";
		j++;
	}
}