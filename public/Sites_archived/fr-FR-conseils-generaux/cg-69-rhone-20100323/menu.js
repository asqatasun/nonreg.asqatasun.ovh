/**
 * Masque toutes les entrées du menu.
 * Retire la classe "actif" à tous les LI si elle est présente
 * @param string id l'id du menu
 */
 
 
 function menu_onload() {
	menu_init('menu');
}
 
function menu_hideall(id)
{
	var navroot = $(id);
	if ( ! navroot ) return; // erreur
	var li = navroot.getElementsByTagName("LI");
	for ( var i = 0; i < li.length; i++ ) {
		var actif = li[i].className.indexOf("actif");
		if ( actif >= 0 ) 
			li[i].className = li[i].className.substr(0,actif);
	}
}

/**
 * Affiche l'entrée "elt" du menu.
 * Ajoute la classe "actif".
 * @param elt l'élément.
 */
function menu_show(elt)
{
	if ( ! elt )
		return;

	if ( elt.className.indexOf("actif") < 0 )
		elt.className += " actif";
}

/**
 * Initialise le script.
 * Trouve le menu identifié par un id et l'active.
 * @param id l'id du menu
 */
function menu_init(id)
{
	var navroot = document.getElementById(id);
	if ( ! navroot ) return; // erreur
	
	var li = navroot.getElementsByTagName("LI");
	for ( var i = 0; i < li.length; i++ ) {
		var ul = li[i].getElementsByTagName("UL");
		if ( ul.length > 0 ) {
			li[i].onmouseover = function() {
				// ne pas refaire si c'est déjà l'élément actif
				// évite les problèmes de scintillement
				if ( this.className.indexOf("actif") < 0 ) {
					menu_hideall(id);
					menu_show(this);
				}
			}
		}
	}
	
}

