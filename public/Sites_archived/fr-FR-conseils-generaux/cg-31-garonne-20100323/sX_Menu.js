		if (!window.Node) {
			var Node = {
				ELEMENT_NODE: 1,
				ATTRIBUTE_NODE: 2,
				TEXT_NODE: 3,
				COMMENT_NODE: 8,
				DOCUMENT_NODE: 9,
				DOCUMENT_FRAGMENT_NODE: 11
				}
			}

		var currentItem = null
		var sX_Menu_selectedID = null
		var hideTimer = new Array()
		var sX_Menu_parameters = new Array()

		function getID(current)
			{
			// Retourne l'ID d'un element, ex : current.id=item_m1_AE423FGB => m1_AE423FGB
			var tab = current.id.split("_")
			return tab[1] + (tab[2]!=null ? '_' + tab[2] : '')
			}

		function getMenuID(current)
			{
			// Retourne l'ID du menu d'un element, ex : current.id=item_m1_AE423FGB => m1
			var tab = current.id.split("_")
			return tab[1]
			}

		function getName(current)
			{
			// Retourne le type d'un element, ex : current.id=item_m1_AE423FGB => item
			var tab = current.id.split("_")
			return tab[0]
			}

		function setItemClassToOver(current)
			{
			// Change la classe d'un element, ajoute OVER, ex : current.className=item_m1_n1 => item_m1_n1_over
			// Si le menu est compose d'une image, on modifie egalement la source
			var currentImg = document.getElementById('img_' + getID(current))

			if (current)
				if (current.className.lastIndexOf('_over')==-1)
					current.className = current.className + '_over'

			regExp = /\.gif/gi
			if (currentImg)
				if (currentImg.src.lastIndexOf('_over')==-1)
					currentImg.src = currentImg.src.replace(regExp, '_over.gif')
			}

		function setItemClassToOut(current)
			{
			// Change la classe d'un element, enleve OVER, ex : current.className=item_m1_n1_over => item_m1_n1
			// Si le menu est compose d'une image, on modifie egalement la source
			var currentImg = document.getElementById('img_' + getID(current))

			regExp = /_over/gi
			if (current)
				if (current.className.lastIndexOf('_over')!=-1)
					current.className = current.className.replace(regExp, '')

			if (currentImg)
				if (currentImg.src.lastIndexOf('_over')!=-1)
					currentImg.src = currentImg.src.replace(regExp, '')
			}

		function setItemClassToOpen(current)
			{
			// Change la classe d'un element, ajoute OPEN, ex : current.className=item_m1_n1_over => item_m1_n1_open_over
			// Si le menu est compose d'une image, on modifie egalement la source
			var currentImg = document.getElementById('img_' + getID(current))

			regExp = /_over/gi
			if (current)
				if (current.className.lastIndexOf('_open')==-1)
					current.className = (current.className.lastIndexOf('_over')==-1) ? current.className + '_open' : current.className.replace(regExp, '_open_over')

			regExp = /\.gif/gi
			regExpB= /_over\.gif/gi
			if (currentImg)
				if (currentImg.src.lastIndexOf('_open')==-1)
					currentImg.src = (currentImg.src.lastIndexOf('_over')==-1) ? currentImg.src.replace(regExp, '_open.gif') : currentImg.src.replace(regExpB, '_open_over.gif')
			}

		function setItemClassToClose(current)
			{
			// Change la classe d'un element, enleve OPEN, ex : current.className=item_m1_n1_open_over => item_m1_n1_over
			// Si le menu est compose d'une image, on modifie egalement la source
			var currentImg = document.getElementById('img_' + getID(current))

			regExp = /_open/gi
			if (current)
				if (current.className.lastIndexOf('_open')!=-1)
					current.className = current.className.replace(regExp, '')

			if (currentImg)
				if (currentImg.src.lastIndexOf('_open')!=-1)
					currentImg.src = currentImg.src.replace(regExp, '')
			}

		function showItemsContent(current)
			{
			// Ouvre le DIV afin d'afficher les sous-menus
			if (current)
				{
				var isItem = (getName(current)!='content')
				var content
				if (isItem)
					content = document.getElementById('content_' + getID(current))
				else
					content = current
				if (content && isItem)
					content.style.display = 'block'
				if (isItem)
					{
					current.status = 'open'
					setItemClassToOpen(current)
					}
				}
			}

		function hideItemsContent(current)
			{
			// Cache le DIV afin d'effacer les sous-menus
			if (current)
				{
				var isItem = (getName(current)!='content')
				var content
				if (isItem)
					content = document.getElementById('content_' + getID(current))
				else
					content = current
				if (content)
					{
					var child = content.childNodes
					for (var i = 0; i<child.length; i++)
						if (child[i].nodeType==Node.ELEMENT_NODE)
							hideItemsContent(child[i])
					if (isItem)
						content.style.display = 'none'
					}
				if (isItem)
					{
					current.status = 'close'
					setItemClassToClose(current)
					}
				}
			}

		function hideAllItems(id)
			{
			// Cache tous les sous-menus a partir du DIV selectionne
			var content = document.getElementById('content_' + id)
			if (content)
				{
				changeItemsClassToOut(content)
				hideItemsContent(content)
				currentItem = content
				}
			}

		function changeItemsClassToOut(current)
			{
			if (current)
				{
				var isItem = (getName(current)!='content')
				var content
				if (isItem)
					content = document.getElementById('content_' + getID(current))
				else
					content = current
				if (content)
					{
					var child = content.childNodes
					for (var i = 0; i<child.length; i++)
						if (child[i].nodeType==Node.ELEMENT_NODE)
							changeItemsClassToOut(child[i])
					}
				if (isItem)
					setItemClassToOut(current)
				}
			}

		function contains(a, b)
			{
			// remonte par les parents de b jusqu'a ce que nous en trouvions un
			while (b && (a!=b) && (b!=null)) b = b.parentNode
			return a == b
			}

		function sX_Menu_mouseOver(e, newItem, id)
			{
			//var id = getMenuID(newItem)
			clearTimeout(hideTimer[id])

			var eSrc
			if (e.srcElement) { eSrc = e.srcElement; e.cancelBubble = true } else { eSrc = e.target; e.stopPropagation() }
			if (eSrc)
				{
				var newItemID = getID(newItem)
				var newItemContent = document.getElementById('content_' + newItemID)
	
				if (newItem!=currentItem && eSrc!=newItemContent)
					{
					//direction : 1 si vers un fils ; -1 si vers le pere ; 0 sinon
					var direction = (contains(currentItem, newItem)) ? 1 : ((contains(newItem, currentItem)) ? -1 : 0)
					if (direction==-1)
						if (sX_Menu_parameters[id]['mouseOverGenerateClick']) sX_Menu_click(e, currentItem, null, null)
					currentItem = newItem
					if (direction==1 || direction==0)
						if (sX_Menu_parameters[id]['mouseOverGenerateClick']) sX_Menu_click(e, currentItem, null, null)
					setItemClassToOver(currentItem)
					}
				}
			}

		function sX_Menu_mouseOut(e, item, id)
			{
			//var id = getMenuID(item)
			clearTimeout(hideTimer[id])

			var eTo
			if (e.srcElement) { eTo = e.toElement; e.cancelBubble = true } else { eTo = e.relatedTarget; e.stopPropagation() }
			if (eTo)
				{
				var newItemID = getID(eTo)
				var newItem = document.getElementById('item_' + newItemID)
				var newItemContent = document.getElementById('content_' + newItemID)
	
				if (newItem!=currentItem && (eTo!=newItemContent || eTo==document.getElementById('content_' + id)))
					{
					changeItemsClassToOut(currentItem)
					if (sX_Menu_parameters[id]['mouseOverGenerateClick'])
						hideTimer[id] = setTimeout('hideAllItems("' + id + '")', 200)
					else
						currentItem = null
					}
				}
			}

		function sX_Menu_click(e, item, href, target)
			{
			if (e.srcElement) { e.cancelBubble = true } else { e.stopPropagation() }
			if (href)
				{
				var reg1=new RegExp("sX_Menu_selectedID");
				if (!href.match(reg1))
				{
					//alert(getID(item))
					href = setUrl(href, 'sX_Menu_selectedID', getID(item))
				}
				if(target=='_blank')
					{
						var newb = window.open(href,"","toolbar=yes,location=yes,resizable=yes,menubar=yes, status=yes, scrollbars=yes, menubar=yes, width=780, height=500");
					}
				else if (target && target!='' && top.frames[target])
					top.frames[target].location = href
				else
					location = href
				}
			else
				{
				if (item.status=='open')
					hideItemsContent(item)
				else
					{
					hideItemsContent(item.parentNode)
					showItemsContent(item)
					}
				}
			}

		function sX_Menu_openSelectedAndAncestors(current)
			{
			if (current && (getName(current)=='content' || getName(current)=='item') && sX_Menu_parameters[getMenuID(current)]['openSelectedIfRefresh'])
				{
				showItemsContent(current)
				sX_Menu_openSelectedAndAncestors(current.parentNode)
				}
			}

		function setUrl(url, param, value) { //v1.0
			var r = getUrlString(url)
			//var a = getUrlAnchor(url)
			if (arguments[3]!=null) r = arguments[3]
			var query = getQueryString(url)
			var sep = "?"
			for(var i=0;i<query.length;i++) {
				if (query[i].indexOf(param + "=")==-1) {	r += sep + query[i]; sep = "&" }
			}
			if (value!='') r += sep + param + "=" + value
			return r
		}

		function getUrlString(url) { //v1.0
			var pos = url.indexOf("?")
			if (pos!=-1) {
				var r = url.substr(0,pos)
			} else {
				var r = url
			}
			return r
		}

		function getQueryString(url) { //v1.0
			var pos = url.indexOf("?")
			if (pos!=-1) {
				var query = url.substr(pos + 1)
				var apos = query.indexOf("#")
				if (apos!=-1) {
					var epos = query.substr(apos + 1).indexOf("&")
					query = query.substr(0, apos) + (epos!=-1 ? query.substr(apos + 1 + epos) : "")
				}
				var tquery = query.split("&")
				query = "?" + query
			} else {
				var query = ""
				var tquery = []
			}
			if (arguments[1]==true) return query
			else return tquery
		}