// JavaScript Document



	// Emule le focus sous IE

	function focusfix(selector, className) {

		$(selector).focus(function() {

			$(this).addClass(className);

		});

		$(selector).blur(function() {

			$(this).removeClass(className);

		});

	}



	$(document).ready(function() {

		

		// Emule le focus sous IE

		focusfix('a', 'focus');

		focusfix('input', 'focus');

		focusfix('textarea', 'focus');

		focusfix('select', 'focus');

		

		// Carousel

		$(".carousel").jCarouselLite({

			btnNext: ".next",

			btnPrev: ".prev",

			visible: 1,

			speed: 800

		});

		

		// Onglets

		$('ul.onglets').tabs({selected:1, fx:{opacity:"toggle"}});



		// Lightbox			   

		$('a.lightbox').lightBox();

		

		// Tooltip

		$("div.bloc_gris.top p").hide();

		$("div.bloc_gris.top img").mouseover(function(){

			$("div.bloc_gris.top p").addClass('infobulle');

			$("div.bloc_gris.top p").show();

		});

		$("div.bloc_gris.top img").mouseout(function(){

			$("div.bloc_gris.top p").removeClass('infobulle');

			$("div.bloc_gris.top p").hide();

		});

		$("div.bloc_gris.bottom p").hide();

		$("div.bloc_gris.bottom img").mouseover(function(){

			$("div.bloc_gris.bottom p").addClass('infobulle');

			$("div.bloc_gris.bottom p").show();

		});

		$("div.bloc_gris.bottom img").mouseout(function(){

			$("div.bloc_gris.bottom p").removeClass('infobulle');

			$("div.bloc_gris.bottom p").hide();

		});



		// Identifie les liens pointant vers des sites externes (ajout d'un pictogramme � droite du lien)

		$("#main a[@href^=\"http\"]").addClass("externe");

		

		// Aspect des lignes (<tr>) des tableaux de donn�es au survol et altern�

		$('table.tableau_donnees tr').mouseover(function(){$(this).addClass('survol');}).mouseout(function(){$(this).removeClass('survol');});

		$("table.tableau_donnees tr:even").addClass("alterne");

		

		// autocompletion

		function findValueCallback(event, data, formatted) {

		$("<li>").html( !data ? "No match!" : "Selected: " + formatted).appendTo("#result");

		}

	

		$("#communes_immo").autocomplete("/suggest/communes_immo", {

				width: 150,

				selectFirst: false

			});

		$("#communes_immo").result(function(event, data, formatted) {

			if (data)

				$(this).parent().next().find("input").val(data[1]);

		});

		

		$("#communes").autocomplete("/suggest/communes", {

				width: 150,

				selectFirst: false

			});

		$("#communes").result(function(event, data, formatted) {

			if (data)

				$(this).parent().next().find("input").val(data[1]);

		});

		

		// autocompletion par pays

//		function findValueCallback(event, data, formatted) {

//		$("<li>").html( !data ? "No match!" : "Selected: " + formatted).appendTo("#result");

//		}

	

//		$("#communes_pays").autocomplete("/suggest/communes_pays", {

//				width: 150,

//				selectFirst: false

//			});

//		$("#communes_pays").result(function(event, data, formatted) {

//			if (data)

//				$(this).parent().next().find("input").val(data[1]);

//		});

//		 $("#communes_pays").click(function() 

//		  {      			

//        		window.location

//      		});

//		

	});