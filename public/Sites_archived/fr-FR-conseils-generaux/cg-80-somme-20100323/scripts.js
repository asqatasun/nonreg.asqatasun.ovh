$(document).ready(function(){
    //clear inputs on focus:
    clearOnFocus();
    
    //switch news on news bar:
    //switchNews();
    
    //top "info" carousel:
    if($('#info-carousel'))
        $('#info-carousel').jcarousel({
            initCallback: function() {
                 $('#info-carousel .jcarousel-item').corner('4px').find('strong').corner('bottom 4px'); 
            }
        }); 

    //main navigation rollover menu:
    mainNav();
    
    //home slider
    homeSlider();

    //tooltip with large image:
    $('.magasin-box img').tooltip({
        track: true, 
        delay: 0, 
        showURL: false,
        fade: 200,
        bodyHandler: function() { 
            return $("<img/>").attr("src", $(this).attr('data-largeimage')); 
        }
    });
    
    //bold first letters of headers:
    boldFirstLetters();
    
    //agenda carousel:
    if($('#agenda-carousel'))
        $('#agenda-carousel').jcarousel({scroll: 1}); 
    if($('#agenda-carousel2'))
        $('#agenda-carousel2').jcarousel({scroll: 1}); 

    // menu
    $('#main-nav li').click(function() {
    	$(this).addClass("hover").mouseleave(function() {
    		$(this).removeClass("hover");
    	});
    	$('#main-nav li li a').click(function(){window.location.href=$(this).attr('href');});
    	return false;
    });

    
    //tabs mechanism:
    tabs(); 

	imgHover();


    $("ul#ticket").liScroll({travelocity: 0.04}); 

	
	
  
    
    //sites carousel:
    if($('#sites-carousel')) {
        $('#sites-carousel').jcarousel({
            vertical: true,
            scroll: 1,
            autoScroll: 1,
            itemScroll: 1,
            animation:'slow',
            auto:1,
            wrap: 'circular',
            initCallback: carousel_1_initCallback,
            itemVisibleInCallback: {
  			onBeforeAnimation: carousel_1_beforeAnimation,
  			onAfterAnimation: carousel_1_afterAnimation
						},
			itemVisibleOutCallback:carousel_1_itemVisibleOutCallback


        });
    }
    
    
    
    

    //add comment button rounded corners:
    $('.comments .add-comment .btn-line span').corner('3px');   
        
    //side nav header rounded corners:
    if($('.side-nav h3'))
        $('.side-nav h3').corner('top 4px');
        
    //side search rounded corners:
    if($('.side-search'))
        $('.side-search').corner('bottom 6px');

    // external links
    $("a.external").click(function() {
    	window.open($(this).href);
    	return false;
    });

});


var carousel_1;
function carousel_1_initCallback(carousel) {
		carousel.selected=1
    jQuery('.carousel_1_control a').bind('click', function() {
    	//alert(this.className)
        carousel.startAuto(0);
        carousel.scroll( this.className);
        return false; 
        
    });

    jQuery('#box_carousel_next').bind('click', function() {
        carousel.next();
        carousel.startAuto(0);

        return false;
    });

    jQuery('#box_carousel_prev').bind('click', function() {
        carousel.prev();
        carousel.startAuto(0);

        return false;
    });
    
        // Pause autoscrolling if the user moves with the cursor over the clip.
    carousel.clip.hover(function() {
        carousel.stopAuto();
    }, function() {
        carousel.startAuto();
    });

};



function carousel_1_beforeAnimation(carousel,element,i,status){
		$('#carousel_1_slide_'+carousel.selected).attr("src","img/slide_off.gif");    
    var idx = carousel.index(i, carousel.options.size);
		carousel.add(i,carousel.get(idx).html())
  	}
        	
function carousel_1_afterAnimation(carousel,element,index,status){
    var idx = carousel.index(index, carousel.options.size);
		carousel.selected=idx
		$('#carousel_1_slide_'+idx).attr("src","img/slide_on.gif");
		}

function carousel_1_itemVisibleOutCallback(carousel, item, i, state, evt){
   	if (i>carousel.options.size || i<0){
    	carousel.remove(i);
    	}
  	carousel.startAuto();
		};



jQuery.fn.liScroll = function(settings) {
		settings = jQuery.extend({
		travelocity: 0.07
		}, settings);		
		return this.each(function(){
				var $strip = jQuery(this);
				$strip.addClass("newsticker")
				var stripWidth = 0;
				var $mask = $strip.wrap("<div class='mask'></div>");
				var $tickercontainer = $strip.parent().wrap("<div class='tickercontainer'></div>");								
				var containerWidth = $strip.parent().parent().width();	//a.k.a. 'mask' width 	
				$strip.find("li").each(function(i){
				stripWidth += jQuery(this, i).width();
				});
				$strip.width(stripWidth);			
				var totalTravel = stripWidth+containerWidth;
				var defTiming = totalTravel/settings.travelocity;	// thanks to Scott Waye		
				function scrollnews(spazio, tempo){
				$strip.animate({left: '-='+ spazio}, tempo, "linear", function(){$strip.css("left", containerWidth); scrollnews(totalTravel, defTiming);});
				}
				scrollnews(totalTravel, defTiming);				
				$strip.hover(function(){
				jQuery(this).stop();
				},
				function(){
				var offset = jQuery(this).offset();
				var residualSpace = offset.left + stripWidth;
				var residualTime = residualSpace/settings.travelocity;
				scrollnews(residualSpace, residualTime);
				});			
		});	
};


/*
 * --------------------------------------------------------------------
 *  clear inputs on focus
 * --------------------------------------------------------------------
 */
function clearOnFocus() {
    var initial = new Array();
    $('input.clear-on-focus').each(function(index) {
        initial[index] = $(this).val();
        $(this)
            .focus(function() {
                if($(this).val() == initial[index])
                $(this).val('');
            })
            .blur(function() {
                if(!$(this).val() || $(this).val()=='') 
                $(this).val(initial[index]);
            });
    });
}


/*
 * --------------------------------------------------------------------
 *  switch news on news bar
 * --------------------------------------------------------------------
 */
function switchNews() {
    $('#news-bar .nav img').click(function() {
        var currentActive = $('#news-bar li.active').removeClass('active');
        var nextActive;
        switch(this.className) {
            case 'prev': {
                nextActive = currentActive.prev('li');
                if(!nextActive.length)
                    nextActive = $('#news-bar li:last-child');
                break;
            }
            case 'next': {
                nextActive = currentActive.next('li');
                if(!nextActive.length)
                    nextActive = $('#news-bar li:first-child');             
                break;
            }
        }
        nextActive.addClass('active');
    });
}


/*
 * --------------------------------------------------------------------
 *  main navigation rollover menu
 * --------------------------------------------------------------------
 */
function mainNav() {
    var activeLi = $('#main-nav > li.active');
    if (activeLi.length)
        mainNavActive(activeLi.addClass('current'));
    $('#main-nav > li').hover(function() {
            mainNavActive($(this));
        }, function() {
            if(activeLi.length) {
                mainNavActive(activeLi.addClass('current'));
            }
            else
                $('#main-nav').css('backgroundPosition', '0 0');
        }    
    ); 
}

function imgHover() {
    $('#main-nav > li a').mouseover(function(){
        $('#header-banner img').attr('src', $(this).attr('img'));
        });
        
    $('#main-nav > li').mouseleave(function(){
    
    	if($(this).parent().find('li.active a').attr('img'))
    	{
        $('#header-banner img').attr('src', $(this).parent().find('li.active a').attr('img'));
    	}
        });    
        
    }

function mainNavActive(elem) {
    var moveBackground;
    if(elem.hasClass('op1'))
            moveBackground = '-87px';
    else if(elem.hasClass('op2'))
            moveBackground = '-174px';
    else if(elem.hasClass('op3'))
            moveBackground = '-261px';
    else if(elem.hasClass('op4'))
            moveBackground = '-348px';
    else if(elem.hasClass('op5'))
            moveBackground = '-435px';
    else if(elem.hasClass('op6'))
            moveBackground = '-522px';
    $('#main-nav').css('backgroundPosition', '0 ' + moveBackground);
    if(!elem.hasClass('current')) 
        $('#main-nav > li.current').removeClass('current'); 
}

/*
 * --------------------------------------------------------------------
 *  home slider
 * --------------------------------------------------------------------
 */
function homeSlider() {
    var sliderLinks = $('.news-slider .slider-links li');
    var sliderSlides = $('.news-slider .slider-slides li');
    sliderLinks.each(function(index){
        $(this).height(Math.floor($('.news-slider .slider-links').height()/sliderLinks.length) );
        $('#news-slider').PiSlider({
            timeOut: 4000
         });
    });
}


/*
 * --------------------------------------------------------------------
 *  bold first letters of headers:
 * --------------------------------------------------------------------
 */
function boldFirstLetters() {
    $('.first-letters').html(function(index, html){
        var wordsArray = html.split(' ');
        var resultTxt = '';
        for(i=0; i<wordsArray.length; i++) {
            resultTxt += '<span>' + wordsArray[i] + '</span>';
            if(i!=wordsArray.length-1)
                resultTxt += ' ';
        }
        return resultTxt;
    });
}
    

/*
 * --------------------------------------------------------------------
 *  tabs mechanism:
 * --------------------------------------------------------------------
 */

function tabs() {
    if($('.tabs-nav').length) {
        $('.tabs-nav li').click(function(event){
            $(this).addClass('active').siblings('li').removeClass('active');
            var activeDivHref = $(this).find('a').attr('href');
            $(this).parent().nextAll('.tab-body').removeClass('active').filter(activeDivHref.substr(activeDivHref.indexOf('#'))).addClass('active');
            event.preventDefault();
        });
    }
}




/*
 * --------------------------------------------------------------------
 *  prototypes:
 * --------------------------------------------------------------------
 */
String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g,"");
}

function ajaxload(div,image){
$(div).html('<p style="text-align:center;"><img src='+image+' /></p>');
}