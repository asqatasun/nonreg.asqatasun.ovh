/******************************************************************************
* cg50WdWidgetPhoto.js
*******************************************************************************

*******************************************************************************
*                                                                             *
* Copyright 2006									                          *
*                                                                             *
******************************************************************************/

function CG50WidgetPhoto(pDiv)
{
	this.photos = new Array();
	this.currentPhoto = 0;
	this.objDiv = pDiv;
}

CG50WidgetPhoto.prototype.addPhoto =  function(pTitle, pPhoto)
{
	var photo = new Array();
	photo["title"] = pTitle;
	photo["photo"] = pPhoto;
	
	this.photos[this.photos.length] = photo;
}

CG50WidgetPhoto.prototype.nextPhoto =  function()
{
	if(this.currentPhoto + 1 > (this.photos.length - 1)) this.currentPhoto = 0;
	else this.currentPhoto = this.currentPhoto + 1;
	this.showPhoto();
}

CG50WidgetPhoto.prototype.previousPhoto =  function()
{
	if(this.currentPhoto == 0) this.currentPhoto = this.photos.length - 1;
	else this.currentPhoto = this.currentPhoto - 1;
	this.showPhoto();
}

CG50WidgetPhoto.prototype.showPhoto =  function()
{
	if(this.photos.length > 0)
	{
		document.getElementById(this.objDiv).src = this.photos[this.currentPhoto]["photo"];
		document.getElementById(this.objDiv).alt = this.photos[this.currentPhoto]["title"];
	}
	else document.getElementById(this.objDiv).style.display = 'none';
}

