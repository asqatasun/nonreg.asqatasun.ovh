/******************************************************************************
* cg50WdWidgetMeteo.js
*******************************************************************************

*******************************************************************************
*                                                                             *
* Copyright 2006									                          *
*                                                                             *
******************************************************************************/
function CG50WidgetMeteo(pNormalClassName, pSelectedClassName)
{
	this.result = new Array();
	this.listeVille = new Array();
	this.normalClassName = pNormalClassName;
	this.selectedClassName = pSelectedClassName;
	this.listeVilleDetailled = new Array();
}

CG50WidgetMeteo.prototype.addVille = function(code, urlImageMatin, altMatin, urlImageMidi, altMidi, urlImageSoir, altSoir)
{
	var tabVille = new Array();
	tabVille["code"] = code;
	tabVille["matin"] = urlImageMatin;
	tabVille["altMatin"] = altMatin;
	tabVille["midi"] = urlImageMidi;
	tabVille["altMidi"] = altMidi;
	tabVille["soir"] = urlImageSoir;
	tabVille["altSoir"] = altSoir;
	
	this.result[code] = tabVille;
	
	this.listeVille[this.listeVille.length] = code;
}

CG50WidgetMeteo.prototype.addVilleLibelle = function(code, libelle)
{
	this.listeVilleDetailled[code] = libelle;
}

CG50WidgetMeteo.prototype.swapVille = function(code, idMatin, idMidi)
{
	var urlMatin = this.result[code]["matin"];
	var urlMidi = this.result[code]["midi"];
	var altMatin = this.result[code]["altMatin"];
	var altMidi = this.result[code]["altMidi"];
	
	document.getElementById(idMatin).src = urlMatin;
	document.getElementById(idMidi).src = urlMidi;
	document.getElementById(idMatin).alt = altMatin;
	document.getElementById(idMidi).alt = altMidi;
	
	for(var i=0; i<this.listeVille.length; i++)
	{
		if(this.listeVille[i] == code) document.getElementById("cg50WdMeteo" + this.listeVille[i]).className = this.selectedClassName;
		else  document.getElementById("cg50WdMeteo" + this.listeVille[i]).className = this.normalClassName;
	}
}

CG50WidgetMeteo.prototype.isObjectExist = function(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=this.isObjectExist(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

CG50WidgetMeteo.prototype.addVilleDetailled = function(code, horaire, jour, date, img, temp, tempMin, tempMax, ventDirection, ventForce, libelle)
{
	var tabVille = new Array();
	tabVille["code"] = code;
	tabVille["horaire"] = horaire;
	tabVille["jour"] = jour;
	tabVille["date"] = date;
	tabVille["img"] = img;
	tabVille["temp"] = temp;
	tabVille["tempMin"] = tempMin;
	tabVille["tempMax"] = tempMax;
	tabVille["ventDirection"] = ventDirection;
	tabVille["ventForce"] = ventForce;
	tabVille["libelle"] = libelle;
	
	if(this.result[code] == null) this.result[code] = new Array();
	if(this.result[code][horaire] == null) this.result[code][horaire] = new Array();
	this.result[code][horaire][jour] = tabVille;
}

CG50WidgetMeteo.prototype.swapVilleDetailled = function(code)
{
	var jourNow = new Date();
	var start = jourNow.getDate() ;
	var stop = start + 4;
	var tabHoraire = new Array();
	tabHoraire[tabHoraire.length] = "matin";
	tabHoraire[tabHoraire.length] = "midi";
	tabHoraire[tabHoraire.length] = "soir";
	
	for(var i=start; i<stop; i++)
	{	
		for(var j=0; j<tabHoraire.length; j++)
		{
			var prefixeObj = "cg50WdMeteo" + tabHoraire[j] + i;
			
			if(this.isObjectExist("cg50WdMeteoTitle") != null) document.getElementById("cg50WdMeteoTitle").innerHTML = this.listeVilleDetailled[code];
			if(this.isObjectExist(prefixeObj + "Img") != null)
			{
				document.getElementById(prefixeObj + "Img").src = this.result[code][tabHoraire[j]][i]["img"];
				document.getElementById(prefixeObj + "Img").alt = this.result[code][tabHoraire[j]][i]["libelle"];
			}
			if(this.isObjectExist(prefixeObj + "TT") != null) document.getElementById(prefixeObj + "TT").innerHTML = this.result[code][tabHoraire[j]][i]["temp"];
			if(this.isObjectExist(prefixeObj + "TTMIN") != null) document.getElementById(prefixeObj + "TTMIN").innerHTML = this.result[code][tabHoraire[j]][i]["tempMin"];
			if(this.isObjectExist(prefixeObj + "TTMAX") != null) document.getElementById(prefixeObj + "TTMAX").innerHTML = this.result[code][tabHoraire[j]][i]["tempMax"];
			if(this.isObjectExist(prefixeObj + "DD") != null) document.getElementById(prefixeObj + "DD").innerHTML = this.result[code][tabHoraire[j]][i]["ventDirection"];
			if(this.isObjectExist(prefixeObj + "FF") != null) document.getElementById(prefixeObj + "FF").innerHTML = this.result[code][tabHoraire[j]][i]["ventForce"];
		}
	}
}