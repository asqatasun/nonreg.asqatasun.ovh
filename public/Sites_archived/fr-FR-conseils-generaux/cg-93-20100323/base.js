//Egaliser la hauteur des colonnes (deja ecrit)
function addLoadEvent(func) { // Manage Load Event
    var oldonload = window.onload;
    if (typeof window.onload != 'function') { window.onload = func; }
    else {
        window.onload = function() {
            oldonload();
            func();
}    }    }

function addResizeEvent(func) { // Manage Resize Event
    var oldonresize = window.onresize;
    if (typeof window.onresize != 'function') { window.onresize = func; }
	else {
        window.onresize = function() {
            oldonresize();
            func();
}    }    }

//un nouvel et
function equalHeightColumnRetard(){
		setTimeout('equalHeightColumn()',500);
}

//adaptation pour IE7
function equalHeightColumn() {
	var elm = document.getElementById("content");
	var x = elm.offsetHeight;
	
	divs = new Array("siteNav","startContent","startContent2","schedule","sidebar");
	for (i=0; i<divs.length; i++) {
		if (div=document.getElementById(divs[i])) {
			if (navigator.userAgent.toLowerCase().indexOf('msie') < 0 || 
			    navigator.userAgent.toLowerCase().indexOf('msie 7') > 0) 
			  div.style.minHeight = x + "px";
			else div.style.height = x + "px";
}	}	}

//Visualiser le menu (style fleche de la puce est code dans le html)
var rideau = new Array('none','block');
var fleche = new Array('pas.gif','act.gif');
var etat = 1;
function lever(){
	if (document.getElementById('themes')){
		etat = Math.abs(etat - 1);
		document.getElementById('sub').style.display = rideau[etat];
		var chemin = document.getElementById('themes').style.listStyleImage;
		chemin = chemin.substring(4,chemin.indexOf('.gif') - 3);
		document.getElementById('themes').style.listStyleImage = 'url(' + chemin + fleche[etat] + ')';
}	}
var limage = new Image();;
function prelever(){
	if (document.getElementById('themes')){
		var chemin = document.getElementById('themes').style.listStyleImage;
		chemin = chemin.substring(4,chemin.indexOf('.gif') - 3);
		limage.src = chemin + fleche[0];
}	}

/* IMPORTANT addLoadEvent : laisser la fonction equalHeightColumn() s'executer avant lever()
que la hauteur de #content ait au minimum la hauteur du menu deplie 
(ne pas relancer equalHeightColumn comme dans fonction d'apparition des formulaires ci-dessous)
- egalement : timing pour prise en compte d'ie7 en conjonction avec une page comme article ou il y a un fomulaire en display none/block
*/

//au chargement de la page :
addLoadEvent(prelever);
/*addLoadEvent(equalHeightColumn);*/
if (navigator.userAgent.toLowerCase().indexOf('msie 7') < 0 && navigator.userAgent.toLowerCase().indexOf('safari') < 0){
	addLoadEvent(equalHeightColumn);
} else {
	addLoadEvent(equalHeightColumnRetard);
}
//pour gerer le deploeiment ou non du menu -> on check les cookies
if (getCookie('theme') != 1) { 
  addLoadEvent(lever);
}


//Attendre que le formulaire a visualiser soit charge dans le DOM
function charger(a){
	while (! document.getElementById(a)) return setTimeout('charger("' + a + '")',5);
	return reagir(document.getElementById(a));
}

//Visualisations du formulaire 
var dori = false;
function reagir(a,b,c){
	//si le div a est masqu�, on le fait apparaitre sinon le contraire
	a.style.display = a.style.display == 'block' ? a.style.display = 'none' : a.style.display = 'block';
	// si on a pass� un b
	if (b) {
		//on masque son parent
		b.parentNode.style.display = 'none';
		//
		if (c) dori = b.parentNode;
		setTimeout('equalHeightColumn()',30);
	} else if (dori){
		
		dori.style.display = dori.style.display == 'block' ? dori.style.display = 'none' : dori.style.display = 'block';
	}
	return false;
}

/* Firefox : dans le menu parfois petite secousse du lien "tous les themes" entre l'affichage des deux puces flechees */
if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1){
	document.write('<style type="text/css">#siteNav li#themes>a	{ padding-left: 21px; float: left; margin-top: -15px; }</style>');
}
/* Safari : 1) dans le menu la couleur marquant l'item actif parait au-dessus 
2) menu detaille du theme actif */
if (navigator.userAgent.toLowerCase().indexOf('safari') > -1){
	var stylesafari = '<style type="text/css">#siteNav li span	{ padding-top: 2px; }';
	stylesafari += '#siteNav .theme	{ list-style-image: none; list-style-type: none; }';
	stylesafari += 'html>body #siteNav .theme p	{ top: 0; left: 0; margin-bottom: 0; padding-left: 16px;';
	stylesafari += 'background: url(img/pu_theme.gif) top left no-repeat; }';
	stylesafari += 'html>body #siteNav .maitriseflot	{ float: none; } </style>';
	document.write(stylesafari);
}

//ajout julien

function setCookieTheme()
{
  if (document.getElementById('sub').style.display == 'none') {
     setCookie('theme',1);
  } else {
     deleteCookie('theme'); 
  }
}



function setCookie(sName, sValue, oExpires, sPath, sDomain, bSecure) {
  var sCookie = sName + "=" + encodeURIComponent(sValue);
  if (oExpires) {
    sCookie += "; expires=" + oExpires.toGMTString();
  }
  if (sPath) {
    sCookie += "; path=" + sPath;
  }
  if (sDomain) {
    sCookie += "; domain=" + sDomain;
  }
  if (bSecure) {
    sCookie += "; secure";
  }
  document.cookie = sCookie;
}

function getCookie(sName) {
  var sRE = "(?:; )?" + sName + "=([^;]*);?";
  var oRE = new RegExp(sRE);
  if (oRE.test(document.cookie)) {
    return decodeURIComponent(RegExp["$1"]);
  } else {
    return null;
  }
}
function deleteCookie(sName, sPath, sDomain) {
  setCookie(sName, "", new Date(0), sPath, sDomain);
}


function removeChilds(oEl)
{
   while (oEl.childNodes.length>0) {
      oEl.removeChild(oEl.firstChild);
    }
 
}
