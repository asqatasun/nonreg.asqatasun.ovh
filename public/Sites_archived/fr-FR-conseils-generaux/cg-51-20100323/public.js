var fontSizeModify={
	currentSelectedSize : '',
	load: function(){
		new Insertion.Top('usertool_container', "<ul id=\"usertool\"><li class='fontSizeModify first'><a href='#' class='firstFontSize' title='Taille de police par d\351faut'><span>A</span></a></li><li class='fontSizeModify'><a href='#' class='secondFontSize' title='Augmenter l\351g\350rement la taille de la police'><span>A</span></a></li><li class='fontSizeModify'><a href='#' class='thirdFontSize' title='Augmenter la taille de la police'><span>A</span></a></li></ul>");
		buttonFontList=$$('#usermenu li.fontSizeModify a');
		//fontSizeModify.selectButton(buttonFontList[0]);
		Event.observe(buttonFontList[0], 'click', function(){
			//fontSizeModify.selectButton(buttonFontList[0]);
			fontSizeModify.setFontSize("75%");
		});
		Event.observe(buttonFontList[1], 'click', function(){
			//fontSizeModify.selectButton(buttonFontList[1]);
			fontSizeModify.setFontSize("82%");
		});
		Event.observe(buttonFontList[2], 'click', function(){
			//fontSizeModify.selectButton(buttonFontList[2]);
			fontSizeModify.setFontSize("90%");
		});
	},
	selectButton: function(selectedButton){
		if(fontSizeModify.currentSelectedSize != ''){
			strEndFile=fontSizeModify.currentSelectedSize.src.split('/user-fontsize')[1];
			fontSizeModify.currentSelectedSize.src='style/site/interface/public/user-fontsize'+strEndFile.split('.')[0]+'.gif';
		}
		strEndFile=selectedButton.src.split('/user-fontsize')[1];
		selectedButton.src='style/site/interface/public/user-fontsize'+strEndFile.split('.')[0]+'.select.gif';
		fontSizeModify.currentSelectedSize=selectedButton;
	},
	setFontSize: function(intfontSize){
		document.body.style.fontSize=intfontSize;
	}
};

Event.observe(window, 'load', fontSizeModify.load);
