	/*
// +----------------------------------------------------------------------+
// | Copyright (c) 2004 Bitflux GmbH                                      |
// +----------------------------------------------------------------------+
// | Licensed under the Apache License, Version 2.0 (the "License");      |
// | you may not use this file except in compliance with the License.     |
// | You may obtain a copy of the License at                              |
// | http://www.apache.org/licenses/LICENSE-2.0                           |
// | Unless required by applicable law or agreed to in writing, software  |
// | distributed under the License is distributed on an "AS IS" BASIS,    |
// | WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or      |
// | implied. See the License for the specific language governing         |
// | permissions and limitations under the License.                       |
// +----------------------------------------------------------------------+
// | Author: Bitflux GmbH <devel@bitflux.ch>                              |
// +----------------------------------------------------------------------+

*/
var liveSearchReq2 = false;
var t2 = null;
var liveSearchLast2 = "";
	
var isIE = false;
// on !IE we only have to initialize it once
if (window.XMLHttpRequest) {
	liveSearchReq2 = new XMLHttpRequest();
}

function liveSearchInit() {
	if (navigator.userAgent.indexOf("Safari") > 0) {
		document.getElementById('livesearch2').addEventListener("keydown",liveSearchKeyPress2,false);
//		document.getElementById('livesearch').addEventListener("blur",liveSearchHide,false);
	} else if (navigator.product == "Gecko") {
		
		document.getElementById('livesearch2').addEventListener("keypress",liveSearchKeyPress2,false);
		document.getElementById('livesearch2').addEventListener("blur",liveSearchHideDelayed2,false);
		
	} else {
		document.getElementById('livesearch2').attachEvent('onkeydown',liveSearchKeyPress2);
//		document.getElementById('livesearch').attachEvent("onblur",liveSearchHide,false);
		isIE = true;
	}
	
	document.getElementById('livesearch2').setAttribute("autocomplete","off");

}

function liveSearchHideDelayed2() {
	window.setTimeout("liveSearchHide2()",400);
}
	
function liveSearchHide2() {
	document.getElementById("LSResult2").style.display = "none";
	var highlight = document.getElementById("LSHighlight2");
	/*if (highlight) {
		highlight.removeAttribute("id2");
	}*/
}

function liveSearchKeyPress2(event) {
	
	if (event.keyCode == 40 )
	//KEY DOWN
	{
		highlight = document.getElementById("LSHighlight2");
		if (!highlight) {
			highlight = document.getElementById("LSShadow2").firstChild.firstChild;
		} else {
			highlight.removeAttribute("id2");
			highlight = highlight.nextSibling;
		}
		if (highlight) {
			highlight.setAttribute("id2","LSHighlight2");
		} 
		if (!isIE) { event.preventDefault(); }
	} 
	//KEY UP
	else if (event.keyCode == 38 ) {
		highlight = document.getElementById("LSHighlight2");
		if (!highlight) {
			highlight = document.getElementById("LSResult2").firstChild.firstChild.lastChild;
		} 
		else {
			highlight.removeAttribute("id2");
			highlight = highlight.previousSibling;
		}
		if (highlight) {
				highlight.setAttribute("id2","LSHighlight2");
		}
		if (!isIE) { event.preventDefault(); }
	} 
	//ESC
	else if (event.keyCode == 27) {
		highlight = document.getElementById("LSHighlight2");
		if (highlight) {
			highlight.removeAttribute("id2");
		}
		document.getElementById("LSResult2").style.display = "none";
	} 
}
function liveSearchStart2(nomFormulaire) {
	if (t2) {
		window.clearTimeout(t2);
	}
	t2 = window.setTimeout("liveSearchDoSearch2('"+nomFormulaire+"')",200);
}

function liveSearchDoSearch2(nomFormulaire) {

	if (typeof liveSearchRoot == "undefined") {
		liveSearchRoot = "";
	}
	if (typeof liveSearchRootSubDir == "undefined") {
		liveSearchRootSubDir = "";
	}
	if (typeof liveSearchParams == "undefined") {
		liveSearchParams = "";
	}
	if (liveSearchLast2 != eval('document.'+nomFormulaire+'.c1.value')) {
	if (liveSearchReq2 && liveSearchReq2.readyState < 4) {
		liveSearchReq2.abort();
	}
	if ( eval('document.'+nomFormulaire+'.c1.value') == "") {
		liveSearchHide();
		return false;
	}
	if (window.XMLHttpRequest) {
	// branch for IE/Windows ActiveX version
	} else if (window.ActiveXObject) {
		liveSearchReq2 = new ActiveXObject("Microsoft.XMLHTTP");
	}
	liveSearchReq2.onreadystatechange= liveSearchProcessReqChange2;
	liveSearchReq2.open("GET", "liveSearchCommunesMich.php?nomFormu="+nomFormulaire+"&c1=" + eval('document.'+nomFormulaire+'.c1.value') + liveSearchParams);
	liveSearchLast2 = eval('document.'+nomFormulaire+'.c1.value');
	liveSearchReq2.send(null);
	}
}

function liveSearchProcessReqChange2() {
	
	if (liveSearchReq2.readyState == 4) {
		var  res1 = document.getElementById("LSResult2");
		res1.style.display = "block";
		var  sh1 = document.getElementById("LSShadow2");
	//	alert(document.getElementById("LSShadow2"));
		sh1.innerHTML = liveSearchReq2.responseText;		 
	//	alert(sh2.innerHTML);
	}
}

function liveSearchSubmit2() {
	var highlight = document.getElementById("LSHighlight2");
	if (highlight && highlight.firstChild) {
		window.location = liveSearchRoot + liveSearchRootSubDir + highlight.firstChild.nextSibling.getAttribute("href");
		return false;
	} 
	else {
		return true;
	}
}

function myLiveSearchStart2(objet, nomFormulaire){
	var reg=new RegExp(" ");
	var valeur= objet.value.replace(reg,"");
	if(valeur.length>=1){
		liveSearchStart2(nomFormulaire);
	}else{
		liveSearchHide2();
	}
}


