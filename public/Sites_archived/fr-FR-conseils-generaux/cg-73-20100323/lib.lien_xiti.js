// +----------------------------------------------------------------------+
// | CMS V1.5.2
// | Auteur: jeremy@eolas.fr
// | Action: Statistiques XiTi - Marqueur de lien externe et document
// +----------------------------------------------------------------------+

function xt_addEventLst(EventTarget,type,listener,useCapture) {
	useCapture = typeof(useCapture)=="boolean"?useCapture:false;
	if (EventTarget.addEventListener) {
		EventTarget.addEventListener(type, listener, useCapture);
	} else if ((EventTarget==window) && document.addEventListener) {
		document.addEventListener(type, listener, useCapture);
	} else if (EventTarget.attachEvent) {
		EventTarget["e"+type+listener] = listener;
		EventTarget[type+listener] = function() {EventTarget["e"+type+listener]( window.event );}
		EventTarget.attachEvent("on"+type, EventTarget[type+listener]);
	} else {
		EventTarget["on"+type] = listener;
	}
}

function xt_getInnerText(oNode) {
	var _innerText ="";
	if (!(oNode=oNode.firstChild)) {return "";}
	while (oNode) {
		if (oNode.tagName == 'IMG') _innerText += oNode.getAttributeNode("ALT").value;
		else _innerText += getOuterText(oNode);
		oNode = oNode.nextSibling;
	}
	return _innerText;
}

xt_addEventLst(window,'load',xt_initialisation);

function xt_initialisation() {
  	try {
		if (xt_doc_path == '' || xt_loc_path == '') {
			window.status = "Les chemins xt ne sont pas renseignés.";
		} else {
			var oAs = document.getElementsByTagName("A");
			for (var i = 0; i < oAs.length; i++) xt_addEventLst(oAs[i], 'click', xt_aclick); 
		}
	} catch(e) {}
}

function xt_trim(str) {
	try {
		str = str.replace(/(^[\s:*]+)|([\s:*]+$)/g,'');
	} catch (e) {
		str = '';
	}
	
	return str;
}

function xt_aclick() {
	try {
		ahref = this.href;
		if (ahref.substr(0,11)!= 'javascript:' && ahref.substr(0,7)!= 'mailto:') {
			if (this.getAttributeNode("TITLE")) aText = this.getAttributeNode("TITLE").value;
			if (xt_trim(aText) == '') aText = xt_getInnerText(this);
			aText = xt_trim(aText).replace(/[^A-Za-z0-9_~\\\/\-]+/g, '_');
			if (ahref.substr(0, xt_doc_path.length) == xt_doc_path) {
				xt_med('C', SIT_XITI_ID, aText,'T');
			} else if (ahref.substr(0, xt_loc_path.length) != xt_loc_path) {
				xt_med('C', SIT_XITI_ID, ahref,'S');
			}
			return false;
		}
	} catch (e) {}
}
