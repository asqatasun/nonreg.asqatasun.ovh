/****** BEGIN LICENSE BLOCK *****
 * Copyright (c) 2005 Harmen Christophe and contributors. All rights reserved.
 * 
 * FormControl script is free software; you can redistribute it and/or
 *   modify under the terms of the Creative Commons - Attribution-ShareAlike 2.0
 * <http://creativecommons.org/licenses/by-sa/2.0/>
 * You are free:
 *     * to copy, distribute, display, and perform the work
 *     * to make derivative works
 *     * to make commercial use of the work
 * 
 * Under the following conditions:
 * _Attribution_. You must attribute the work in the manner specified by the
 *   author or licensor.
 * _Share Alike_. If you alter, transform, or build upon this work, you may
 *   distribute the resulting work only under a license identical to this one.
 *     * For any reuse or distribution, you must make clear to others 
 *      the license terms of this work.
 *     * Any of these conditions can be waived if you get permission from 
 *      the copyright holder.
 * 
 * Your fair use and other rights are in no way affected by the above.
 * 
 * This is a human-readable summary of the Legal Code (the full license). 
 * <http://creativecommons.org/licenses/by-sa/2.0/legalcode>
 ***** END LICENSE BLOCK ******/
/******
 ** 2005/08/29 V0.24a Non finalis�e
 * [BUG MacOS9 MSIE5 : ne semble pas supporter le nodeValue sur un textNode]
 ******/
function addFormsControlListener() {
	var cFormsToControl, nLabel;
	cFormsToControl = getElementsByClass("formToControl");
	for (var i=0; i < cFormsToControl.length; i++) {
		if (cFormsToControl[i].nodeName.toLowerCase()=="form") {
			addEventLst(cFormsToControl[i],"submit",formControlListener);
		}
	}
}
addEventLst(window,"load",addFormsControlListener);
function getClearedTextLabel(nLabel) {
	return getInnerText(nLabel).replace(/\s+/g," ").replace(/(^[\s:*]+)|([\s:*]+$)/g,"");
}
function formControlListener(e) {
	var bIsValide, nForm, cLabels;
	var aFormCtrlSchemes = [["isNotNull","Le champ \"%s\" doit �tre renseign�."],
			["isDate","Le champ \"%s\" n'est pas une date valide.\nFormat : jj/mm/aaaa."],
			["isEmail","Le champ \"%s\" n'est pas un email valide."],
			["isInt","Le champ \"%s\" n'est pas un entier valide."],
			["isFloat","Le champ \"%s\" n'est pas un r�el valide."]];
	bIsValide = true;
	if (document.addEventListener)
		nForm = e.currentTarget;
	else if (window.event)
		nForm = window.event.srcElement;
	else
		nForm = this;
	while (nForm.nodeType==1) {
		if (nForm.nodeName.toLowerCase()=="form") break;
		nForm = nForm.parentNode;
	}
	try {
		if (bIsValide && (typeof(eval("preControl_"+nForm.id))=="function"))
			bIsValide = eval("preControl_"+nForm.id+"(nForm);");
	} catch(err) {}
	cLabels = nForm.getElementsByTagName("label");
	for (var i=0; bIsValide && i<cLabels.length; i++) {
		if ((cLabels[i].htmlFor=="") || !(nField=document.getElementById(cLabels[i].htmlFor))) continue;
		for (var j=0; bIsValide && aFormCtrlSchemes[j]; j++) {
			if (hasClassName(cLabels[i],aFormCtrlSchemes[j][0])) {
				if (aFormCtrlSchemes[j][0]=="isFloat") nField.value = nField.value.replace(",",".");
				if (!eval(aFormCtrlSchemes[j][0]+"(nField.value)")) {
					bIsValide = false;
					alert(aFormCtrlSchemes[j][1].replace(/[^\W]*%s[^\W]*/g,getClearedTextLabel(cLabels[i])));
				}
			}
		}
		if (bIsValide && hasClassName(cLabels[i],"extendedControl"))
			bIsValide = eval("extendedControl_"+cLabels[i].htmlFor+"(nField);");
		if (!bIsValide) {
			if (nField.focus)
				nField.focus();
			else if (nField.selected)
				nField.selected();
		}
	}
	try {
		if (bIsValide && (typeof(eval("postControl_"+nForm.id))=="function"))
			bIsValide = eval("postControl_"+nForm.id+"(nForm);");
	} catch(err) {}
	if (!bIsValide) {
		if (e && e.preventDefault) {
			e.preventDefault();
		} else if (e && e.returnValue) {
			e.returnValue = false;
		} else {
			return false;
		}
	}
}
	function getElementsByClass(sClass,oEl) {
		var outArray = new Array();
		function _getElementsByClass(outArray, oEl, sClass) {
			while (oEl) {
				if (oEl.nodeType == 1) {
	       			if (hasClassName(oEl,sClass)) outArray[outArray.length] = oEl;
	    			_getElementsByClass(outArray, oEl.firstChild, sClass);
				}
				oEl = oEl.nextSibling;
			}
		}
		oEl = (oEl && (oEl.nodeType==1))?oEl:document.documentElement;
		_getElementsByClass(outArray, oEl, sClass);
		return outArray;
	}
	function getOuterText(oNode) {
		var _outerText = "";
		if (!oNode) {return "";}
		switch (oNode.nodeType) {
			case 1: _outerText += getInnerText(oNode); break;
			case 3: try {_outerText += oNode.nodeValue;} catch (err) {} break;// try temporaire pour MSIE5 MacOS9
		}
		return _outerText;
	}
	function getInnerText(oNode) {
		var _innerText ="";
		if (!(oNode=oNode.firstChild)) {return "";}
		while (oNode) {
			_innerText += getOuterText(oNode);
			oNode = oNode.nextSibling;
		}
		return _innerText;
	}
	function addEventLst(EventTarget,type,listener,useCapture) {
		useCapture = typeof(useCapture)=="boolean"?useCapture:false;
		if (EventTarget.addEventListener) {
			EventTarget.addEventListener(type,listener, useCapture);
		} else if ((EventTarget==window) && window.document.addEventListener) {
			window.document.addEventListener(type,listener, useCapture);
		} else if (EventTarget.attachEvent) {
			EventTarget.attachEvent("on"+type,listener);
		} else {
			eval("EventTarget.on"+type+" = "+listener);
		}
	}
	function hasClassName(oEl,sHasClass) {
	    return ((" " + oEl.className + " ").indexOf(" " + sHasClass + " ")!=-1);
	}
	function trim(s) {
		return s.replace(/(^\s+)|(\s+$)/g,"");
	}
function isNotNull(s) {return trim(s)!="";}
function isEmail(s) {
 if (isNotNull(s)) return /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})+$/.test(s); else return true;
}
function isDate(s) {
	var bIsDate, d, m, y;
	bIsDate = true;
	if (isNotNull(s)) {
		if ((s.length != 10) || (s.substring(2,3) != "/") || (s.substring(5,6) != "/")) bIsDate = false;
		var d = s.substring(0,2);
		var m = s.substring(3,5);
		var y = s.substring(6,10);	
		if (m==1 || m==3 || m==5 || m==7 | m==8 || m==10 || m==12) {
			if (d > 31) bIsDate = false;
		} else if (m==4 || m==6 || m==9 || m==11) {
			if (d > 30) bIsDate = false;	
		} else if (m==2) {
			if (y % 4 == 0) {
				if (d > 29) bIsDate = false;	
			} else {
				if (d > 28) bIsDate = false;	
			}
		} else {
			bIsDate = false;	
		}
	}
	return bIsDate;
}
function isInt(s) {
 if (isNotNull(s)) return (parseInt(s)==s); else return true;
}
function isFloat(s) {
 if (isNotNull(s)) return (parseFloat(s)==s); else return true;
}