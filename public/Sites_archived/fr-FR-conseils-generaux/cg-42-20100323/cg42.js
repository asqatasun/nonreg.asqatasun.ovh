// Au chargement de la page on doit ex�cuter
window.onload = function() {
	// Ouvrir les liens externes dans de nouvelle fen�tre
	externalLinks();
}
// On R�cup�re l'ensemble des liens et ajoute l'attribut target="_blank" � l'ensemble des liens externes 
// (avec la cha�ne "http://" au sein de la propri�t� href)
function externalLinks() {
	if (!document.getElementsByTagName) return;
	var cA = document.getElementsByTagName("A");
	var oA;
	for(var i=0; oA = cA[i]; i++) {
		if  ( (oA.className!="notExternal")
     && (oA.getAttribute("HREF"))
     && (!/(^mailto:)|(^javascript:)|(logout.jsp)|(admin.jsp)|(pubBrowser.jsp)|(index.jsp)|(display.jsp)|(^#)/i.test(oA.getAttribute("HREF"))) ) {
			oA.target = "_blank";
		}
	}
}
