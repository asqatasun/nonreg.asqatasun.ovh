// ---------------------------------------
// Events Functions ||||||||||||||||||||||
// ---------------------------------------

function addEvent(el, evname, func) {
	if (el.attachEvent) { // IE
		el.attachEvent("on" + evname, func);
	} else if (el.addEventListener) { // Gecko / W3C
		el.addEventListener(evname, func, true);
	} else {
		el["on" + evname] = func;
	}
};

function removeEvent(el, evname, func) {
	if (el.detachEvent) { // IE
		el.detachEvent("on" + evname, func);
	} else if (el.removeEventListener) { // Gecko / W3C
		el.removeEventListener(evname, func, true);
	} else {
		el["on" + evname] = null;
	}
};

var mouseX;
var mouseY;
var i = 1;
function getMousePos(e)
{
	mouseX = 0;
	mouseY = 0;
	if (!e) var e = window.event;
	if (e.pageX || e.pageY)
	{
		mouseX = e.pageX;
		mouseY = e.pageY;
	}
	else if (e.clientX || e.clientY)
	{
		mouseX = e.clientX + document.body.scrollLeft;
		mouseY = e.clientY + document.body.scrollTop;
	}
	if (navigator.userAgent.indexOf('MSIE 6') != -1 &&
			document.documentElement &&
			document.documentElement.scrollTop)	// Explorer 6 Strict
	{
		mouseX = mouseX + document.documentElement.scrollLeft;
		mouseY = mouseY + document.documentElement.scrollTop;
	}
}
// ---------------------------------------
// Calendar Detail Event Function ||||||||
// ---------------------------------------

var lastDivShown = null;
var lastDivX1;
var lastDivX2;
var lastDivY1;
var lastDivY2;

// show the given div (detailed event)
// @param eventDivId the id of the div do display
// @param event the mouse event (onMouseOver) used to position the div
function showCalendarDetailedEvent(eventDivId, event) 
{
  var detailedEventDiv = document.getElementById('PortletCalendar_DetailedEvent');
  var eventDiv = document.getElementById(eventDivId);
  
	if (lastDivShown != eventDiv) {
		hideCalendarDetailedEvent();
	}
	
  getMousePos(event);
	document.body.appendChild(eventDiv);
  eventDiv.style.left = mouseX + 10 + "px";
  eventDiv.style.top = mouseY + 10 + "px";
	eventDiv.style.visibility = "visible";
	eventDiv.style.zIndex = 999999;

	// variable used in event callback (processMouseMove)
  lastDivShown = eventDiv; 
	lastDivX1 = parseInt(lastDivShown.style.left, 10);
	lastDivX2 = parseInt(lastDivShown.style.left, 10) + parseInt(lastDivShown.clientWidth, 10);
	lastDivY1 = parseInt(lastDivShown.style.top, 10);
	lastDivY2 = parseInt(lastDivShown.style.top, 10) + parseInt(lastDivShown.clientHeight, 10);

	addEvent(document, "mousemove", processMouseMove);
}

function hideCalendarDetailedEvent()
{
	if (lastDivShown) {
		lastDivShown.style.visibility = "hidden";
		lastDivShown = null;
		activateMouseOutCheck = false;
		removeEvent(document, "mousemove", processMouseMove);
	}
}

var activateMouseOutCheck = false;
function processMouseMove(event)
{
	if (lastDivShown) {
  	getMousePos(event);
  	if ((mouseX >= lastDivX1) && (mouseX <= lastDivX2) &&
  			(mouseY >= lastDivY1) && (mouseY <= lastDivY2)) {
	  	activateMouseOutCheck = true;
  	}
  	if (activateMouseOutCheck == true &&
  			((mouseX < lastDivX1) || (mouseX > lastDivX2) ||
  			(mouseY < lastDivY1) || (mouseY > lastDivY2))) {
			hideCalendarDetailedEvent();
  	}
	}
}
