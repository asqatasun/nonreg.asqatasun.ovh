// controle.js
// ARPEGE 2004 - LB
// Cr�ation de demandes de documents d'Etat Civil � destination
// d'ActesWeb.
// Controles de surface ( cot� client ) de la saisie utilisateur.
// 20040805:LB: V1


function ControlerCommun(TypeD,Acte)
{
	var oForm = document.getElementById('formDemande');
	var	vcheckpereinconnu = false ;
	if (oForm.checkpereinconnu!=undefined)
		vcheckpereinconnu = oForm.checkpereinconnu.checked ;

	var	vcheckmereinconnu = false ;
	if (oForm.checkmereinconnu!=undefined)
		vcheckmereinconnu = oForm.checkmereinconnu.checked ;
		
	var	vcheckpereconjointinconnu = false ;
	if (oForm.checkpereconjointinconnu!=undefined)
		vcheckpereconjointinconnu = oForm.checkpereconjointinconnu.checked ;

	var	vcheckmereconjointinconnu = false ;
	if (oForm.checkmereconjointinconnu!=undefined)
		vcheckmereconjointinconnu = oForm.checkmereconjointinconnu.checked ;

	// En fonction du type de document demand� ( copie int�grale, extrait avec filiation ) la filiation est obligatoire
	if ((TypeD == "extrait avec filiation") || (TypeD == "copie int�grale"))
	{
		if (!vcheckpereinconnu)
		{
			if (oForm.NomPere.value == "")
			{
				alert("Le nom du p�re de la personne demand�e est obligatoire ( Lors d'une demande de copie int�grale ou d'un extrait avec filiation, la filiation est obligatoire )");
				return false;	
			}						
			if (oForm.PrenomsPere.value == "")
			{
				alert("Les pr�noms du p�re de la personne demand�e sont obligatoires ( Lors d'une demande de copie int�grale ou d'un extrait avec filiation, la filiation est obligatoire )");
				return false;	
			}						
		}
		if (!vcheckmereinconnu)
		{
			if (oForm.NomMere.value == "")
			{
				alert("Le nom de la m�re de la personne demand�e est obligatoire ( Lors d'une demande de copie int�grale ou d'un extrait avec filiation, la filiation est obligatoire )");
				return false;	
			}						
			if (oForm.PrenomsMere.value == "")
			{
				alert("Les pr�noms de la m�re de la personne demand�e sont obligatoires ( Lors d'une demande de copie int�grale ou d'un extrait avec filiation, la filiation est obligatoire )");
				return false;	
			}						
		}
	}
	// En fonction du type de l'acte demand� ( mariage ) les renseignements sur le conjoint sont obligatoires
	if (Acte == "MA")
	{
		if (oForm.NomConjoint.value == "")
		{
			alert("Le nom du conjoint est obligatoire ( Lors d'une demande d'acte de mariage, l'identit� du conjoint est obligatoire )");
			return false;	
		}						
		if (oForm.PrenomsConjoint.value == "")
		{
			alert("Les pr�noms du conjoint sont obligatoires ( Lors d'une demande d'acte de mariage, l'identit� du conjoint est obligatoire )");
			return false;	
		}
		
		if ((TypeD == "extrait avec filiation") || (TypeD == "copie int�grale"))
		{
			if (!vcheckpereconjointinconnu)
			{
				if (oForm.NomPereConjoint.value == "")
				{
					alert("Le nom du p�re du conjoint de la personne demand�e est obligatoire ( Lors d'une demande de copie int�grale ou d'un extrait avec filiation, la filiation est obligatoire )");
					return false;	
				}						
				if (oForm.PrenomsPereConjoint.value == "")
				{
					alert("Les pr�noms du p�re du conjoint de la personne demand�e sont obligatoires ( Lors d'une demande de copie int�grale ou d'un extrait avec filiation, la filiation est obligatoire )");
					return false;	
				}						
			}
			if (!vcheckmereconjointinconnu)
			{
				if (oForm.NomMereConjoint.value == "")
				{
					alert("Le nom de la m�re du conjoint de la personne demand�e est obligatoire ( Lors d'une demande de copie int�grale ou d'un extrait avec filiation, la filiation est obligatoire )");
					return false;	
				}						
				if (oForm.PrenomsMereConjoint.value == "")
				{
					alert("Les pr�noms de la m�re du conjoint de la personne demand�e sont obligatoires ( Lors d'une demande de copie int�grale ou d'un extrait avec filiation, la filiation est obligatoire )");
					return false;	
				}						
			}
		}								
	}
			
	return true;
}

// Controle dans le cas d'une demande d'une administration ( au sens large : notaire etc ... )
// retourne true si aucune erreur
// sinon affiche le message d'erreur et retourne false
function ControlerDemandeA()
{
	var oForm = document.getElementById('formDemande');
	var TypeD = "" ;
	var Acte = "" ;
	var Demandeur = "" ;
	var Mandant = "" ;
	
	
	
	// Merci IE ... ( qui retourne 'undefined' pour la valeur d'un groupe de radio bouton )
	for (var i=0; i < oForm.TypeD.length; i++)
	{
		if (oForm.TypeD[i].checked)
		{
			TypeD = oForm.TypeD[i].value ;
		}
	}	
	for (i=0; i < oForm.Acte.length; i++)
	{
		if (oForm.Acte[i].checked)
		{
			Acte = oForm.Acte[i].value ;
		}
	}	

	Demandeur = oForm.Demandeur.value ;

	Mandant = oForm.Mandant.value ;
	

	
		
	if (oForm.Nom.value == "")
	{
		alert("Le nom est obligatoire.");
		return false;	
	}
	
	if (oForm.Prenom.value == "")
	{
		alert("Le pr�nom est obligatoire.");
		return false;	
	}		
	if (oForm.Adresse1.value == "")
	{
		alert("L'adresse est obligatoire.");
		return false;	
	}	
	if (oForm.CP.value == "")
	{
		alert("Le code postal est obligatoire.");
		return false;	
	}	
	if (oForm.Ville.value == "")
	{
		alert("La nom de la ville est obligatoire.");
		return false;	
	}	
	if (oForm.Pays.value == "")
	{
		alert("Le nom du pays est obligatoire.");
		return false;	
	}		

	if (Demandeur == "")
	{
		alert("Le type de demandeur est obligatoire.");
		return false;	
	}		
	if ((Demandeur == "administration") && (oForm.DemandeurAdministration.value == ""))
	{
		alert ("Le nom de l'administration est obligatoire");
		return false;
	}
	if ((Mandant == "autrelien") && (oForm.MandantPrecision.value == ""))
	{
		alert ("La pr�cision du lien entre le mandant et le demand� est olibigatoire");
		return false;
	}
	if ((Mandant == "autrecas") && (oForm.MandantPrecision.value == ""))
	{
		alert ("La pr�cision du lien entre le mandant et le demand� est olibigatoire");
		return false;
	}


	if (oForm.NomD.value == "")
	{
		alert("Le nom de la personne demand�e est obligatoire.");
		return false;	
	}						
	if (oForm.PrenomsD.value == "")
	{
		alert("Les pr�noms de la personne demand�e sont obligatoires.");
		return false;	
	}	
	if (oForm.DateD.value == "")
	{
		alert("La date de l'�v�nement est obligatoire.");
		return false;	
	}	
	
	var ret = ControlerDate(oForm.DateD.value);
	if (ret.length)
	{
		alert("Date d'�v�nement ("+ oForm.DateD.value+ ") incorrecte.\n"+ ret);
		return false;	
	}				

	if (oForm.LieuD.value == "")
	{
		alert("Le lieu de l'�v�nement est obligatoire.");
		return false;	
	}	

	if (!ControlerCommun(TypeD,Acte))
		return false ;
	
	if (oForm.motif_obligatoire.value=="true")
	{
		if (oForm.Motif.value == "")
		{
			alert("Le motif est obligatoire.");
			return false;	
		}		
	}
			
	return true;
}






// Controle dans le cas d'une demande d'un particulier
// retourne true si aucune erreur
// sinon affiche le message d'erreur et retourne false
function ControlerDemandeP()
{
	var oForm = document.getElementById('formDemande');
	var TypeD = "" ;
	var Acte = "" ;
	var Motif = "" ;
	var Demandeur = "" ;
	
	
	
	// Merci IE ... ( qui retourne 'undefined' pour la valeur d'un groupe de radio bouton )
	for (var i=0; i < oForm.TypeD.length; i++)
	{
		if (oForm.TypeD[i].checked)
		{
			TypeD = oForm.TypeD[i].value ;
		}
	}	
	for (i=0; i < oForm.Acte.length; i++)
	{
		if (oForm.Acte[i].checked)
		{
			Acte = oForm.Acte[i].value ;
		}
	}	
	for (i=0; i < oForm.Demandeur.length; i++)
	{
		if (oForm.Demandeur[i].checked)
		{
			Demandeur = oForm.Demandeur[i].value ;
		}
	}	
	for (i=0; i < oForm.Motif.length; i++)
	{
		if (oForm.Motif[i].checked)
		{
			Motif = oForm.Motif[i].value ;
		}
	}	
	
	if (oForm.Nom.value == "")
	{
		alert("Le nom est obligatoire.");
		return false;	
	}
	
	if (oForm.Prenom.value == "")
	{
		alert("Le pr�nom est obligatoire.");
		return false;	
	}		
	if (oForm.Adresse1.value == "")
	{
		alert("L'adresse est obligatoire.");
		return false;	
	}	
	if (oForm.CP.value == "")
	{
		alert("Le code postal est obligatoire.");
		return false;	
	}	
	if (oForm.Ville.value == "")
	{
		alert("La nom de la ville est obligatoire.");
		return false;	
	}	
	if (oForm.Pays.value == "")
	{
		alert("Le nom du pays est obligatoire.");
		return false;	
	}		
	if ((Demandeur == "autrelien") && (oForm.DemandeurParent.value == ""))
	{
		alert ("La parent� est obligatoire");
		return false;
	}
	if ((Demandeur == "autrecas") && (oForm.DemandeurAutre.value == ""))
	{
		alert ("La qualit� du mandant est obligatoire");
		return false;
	}


	if (oForm.NomD.value == "")
	{
		alert("Le nom de la personne demand�e est obligatoire.");
		return false;	
	}						
	if (oForm.PrenomsD.value == "")
	{
		alert("Les pr�noms de la personne demand�e sont obligatoires.");
		return false;	
	}	
	if (oForm.DateD.value == "")
	{
		alert("La date de l'�v�nement est obligatoire.");
		return false;	
	}
	
	var ret = ControlerDate(oForm.DateD.value);
	if (ret.length)
	{
		alert("Date d'�v�nement ("+ oForm.DateD.value+ ") incorrecte.\n"+ ret);
		return false;	
	}				

	if (oForm.LieuD.value == "")
	{
		alert("Le lieu de l'�v�nement est obligatoire.");
		return false;	
	}	

	if ((Motif == "autremotif") && (oForm.MotifAutre.value == ""))
	{
		alert ("Le motif est obligatoire");
		return false;
	}


	if (!ControlerCommun(TypeD,Acte))
		return false ;
			
	return true;
}





// controle format d'une date
// renvoie un message d'erreur si il y'a lieu
// sinon retourne une chaine vide
function ControlerDate(d)
{
	var oForm = document.getElementById('formDemande');
	if (d == "")
	{
		return  "Date vide. Le format attendu est JJ/MM/AAAA.";
	}
	if (d.length < 8 || d.length > 10)
	{
		return  "Format incorrect ( mauvaise longueur : "+d.length+" ). Le format attendu est JJ/MM/AAAA.";
	}
	var date = d;
	var dateArray = date.split("/");
	if (dateArray.length != 3)
	{
		return  "Format incorrect ( manque s�parateurs ). Le format attendu est JJ/MM/AAAA.";
	}
	var jour = dateArray[0];
	var mois = dateArray[1];
	var annee = dateArray[2];
	
	if (!ControlerNombre(jour))
	{
		return  "Format incorrect. Le format attendu est JJ/MM/AAAA.";
	}
	if (!ControlerNombre(mois))
	{
		return  "Format incorrect. Le format attendu est JJ/MM/AAAA.";
	}		
	if (!ControlerNombre(annee))
	{
		return  "Format incorrect. Le format attendu est JJ/MM/AAAA.";
	}		
	if (jour < 1 || jour > 31)
	{
		return  "Format incorrect. Le jour doit �tre compris entre 1 et 31.";			
	}
	if (mois < 1 || mois > 12)
	{
		return  "Format incorrect. Le mois doit �tre compris entre 1 et 12.";
	}		
	var cd = new Date();
	var cy = cd.getYear();
	if (cy < 1000)
		cy = cy + 1900;
	if (annee < 1500 || annee > cy )
	{
		return  "Format incorrect. L'ann�e doit �tre comprise entre 1500 et "+cy+".";
	}
	if (mois == 2 )
	{
		var nb = 28;
		if (annee / 4 == Math.floor(annee / 4))
		{
			nb = 29;
			if (annee / 100 == Math.floor(annee / 100))
			{
				nb = 28;
				if (annee / 400 == Math.floor(annee / 400))
				{
					nb = 29;
				}
			}
		}
		if (jour >	nb) 
		{
			return "Le mois concern� ne contient que "+ nb +" jours.";
		}
	}					
	if (mois ==  4 && jour > 30) return "Le mois concern� ne contient que 30 jours.";
	if (mois ==  6 && jour > 30) return "Le mois concern� ne contient que 30 jours.";
	if (mois ==  9 && jour > 30) return "Le mois concern� ne contient que 30 jours.";
	if (mois == 11 && jour > 30) return "Le mois concern� ne contient que 30 jours.";
				
	return "";
}

// retourne true si la chaine pass�e est compos� uniquement de chiffres
function ControlerNombre(str)
{
	var oForm = document.getElementById('formDemande');
	for (i = 0; i<str.length; i++)
	{
		if (str.charAt(i) < '0' || str.charAt(i) >'9')
			return false;
	}
	return true;
}

// retourne true si la chaine pass�e est un code de d�partement francais
function ControlerDepartement(str)
{
var oForm = document.getElementById('formDemande');
	switch(str)
	{
	case "01":
	case "02":
	case "03":
	case "04":
	case "05":
	case "06":
	case "07":
	case "08":
	case "09":
	case "10":
	case "11":
	case "12":
	case "13":
	case "14":
	case "15":
	case "16":
	case "17":
	case "18":
	case "19":
	case "20":
	case "21":
	case "22":
	case "23":
	case "24":
	case "25":
	case "26":
	case "27":
	case "28":
	case "29":
	case "2A":
	case "2B":
	case "30":
	case "31":
	case "32":
	case "33":
	case "34":
	case "35":
	case "36":
	case "37":
	case "38":
	case "39":
	case "40":
	case "41":
	case "42":
	case "43":
	case "44":
	case "45":
	case "46":
	case "47":
	case "48":
	case "49":
	case "50":
	case "51":
	case "52":
	case "53":
	case "54":
	case "55":
	case "56":
	case "57":
	case "58":
	case "59":
	case "60":
	case "61":
	case "62":
	case "63":
	case "64":
	case "65":
	case "66":
	case "67":
	case "68":
	case "69":
	case "70":
	case "71":
	case "72":
	case "73":
	case "74":
	case "75":
	case "76":
	case "77":
	case "78":
	case "79":
	case "80":	
	case "81":
	case "82":
	case "83":
	case "84":
	case "85":
	case "86":
	case "87":
	case "88":
	case "89":
	case "90":	
	case "91":
	case "92":
	case "93":
	case "94":
	case "95":
	case "971":
	case "972":
	case "973":
	case "974":
	case "975":
	case "984":
	case "985":
	case "986":
	case "987":
	case "988":
		return false;
	
	};
	return true;
}


