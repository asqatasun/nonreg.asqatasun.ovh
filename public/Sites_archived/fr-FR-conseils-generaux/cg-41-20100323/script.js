function replaceflashbyimg(){
	replaceflash('header');
	replaceflash('menu');
}
function replaceflash(id){
	var flashelement = null;
	var imgelement = null;
	if( document.getElementById){
		flashelement = document.getElementById(id+'flash');
		imgelement = document.getElementById(id+'flashreplace');
	}else if( document.all){
		flashelement = document.all[id+'flash'];
		imgelement = document.all[id+'flashreplace'];
	}
	if (flashelement!=null && imgelement!=null) {
		flashelement.innerHTML = imgelement.innerHTML;
	}
}

function switchflash(objectid, buttonid) {
	if( document.getElementById){
		flashelement = document.getElementById(objectid+'flash');
		imgelement = document.getElementById(objectid+'flashreplace');
		buttonelement = document.getElementById(buttonid);
	}else if( document.all){
		flashelement = document.all[objectid+'flash'];
		imgelement = document.all[objectid+'flashreplace'];
		buttonelement = document.all[buttonid];
	}
	if(flashelement!=null && imgelement!=null) {
		if(flashelement.className =="hidden") {
			flashelement.className="";
			imgelement.className="hidden";	
		} else {
			flashelement.className="hidden";
			imgelement.className="";	
		}
	}
	if(buttonelement.innerHTML == "Arr\u00EAter l'animation") {
		buttonelement.innerHTML = "D&eacute;marrer l'animation";
	}else{
		buttonelement.innerHTML = "Arr&ecirc;ter l'animation";
	}	
}
function textePlus(){
	var main_element = null;
	if( document.getElementById){
		main_element = document.getElementById("wrapper");
	}else if( document.all){
		main_element = document.all["wrapper"];
	}
	if (main_element!=null) {
		var sizestr=main_element.style.fontSize.substring(0,main_element.style.fontSize.length-1);
		sizeInt = sizestr!=""?parseInt(sizestr):100;
		sizeInt += 10;
		main_element.style.fontSize=""+sizeInt+"%";
	}
}
function texteMoins(){
	var main_element = null;
	if( document.getElementById){
		main_element = document.getElementById("wrapper");
	}else if( document.all){
		main_element = document.all["wrapper"];
	}
	if (main_element!=null) {
		var sizestr=main_element.style.fontSize.substring(0,main_element.style.fontSize.length-1);
		sizeInt = sizestr!=""?parseInt(sizestr):100;
		sizeInt -= 10;
		main_element.style.fontSize=""+sizeInt+"%";
	}
}
function directPrint(){
	if (window.print) self.print();
}
function add2Bookmark() {
	if (document.all&&navigator.userAgent.indexOf("Opera")==-1) {
		window.external.AddFavorite(document.location,document.title);
	} else if (window.sidebar) {
		window.sidebar.addPanel(document.title,document.location.href,"");
	} else if (navigator.userAgent.indexOf("Opera")!=-1) {
		alert('Utilisez la fonction d\'impression de votre navigateur! (Ctrl+P)');
	}
}
function invertGroup(titregroup){
	if( titregroup != null && titregroup.substring(0,6) == "titre_"){
		var newid = titregroup.replace("titre_","group_");
		var element = null;
		if( document.getElementById){
			element = document.getElementById(newid);
		}else if( document.all){
			element = document.all[newid];
		}
		if( element!=null){
			var classname = element.className
			if( classname.indexOf("hidden") == -1 ){
				element.className = classname+' hidden';
			}else{
				element.className = classname.replace("hidden","");
			}
		}
	}
}

function writeTextePlus(icone){
	document.write('<li id="textPlus">');
	document.write('<a class="tool" href="javascript:textePlus()">Agrandir le texte <img src="'+icone+'" width="18" alt=""/></a>');
	document.write('</li>');
}

function writeTexteMoins(icone){
	document.write('<li id="textMoins">');
	document.write('<a class="tool" href="javascript:texteMoins()">R&eacute;duire le texte <img src="'+icone+'" width="18" alt=""/></a>');
	document.write('</li>');
}
function writePrint(icone){
	document.write('<li id="printPage">');
	document.write('<a class="tool" href="javascript:directPrint()">Imprimer la page <img src="'+icone+'" width="18" alt=""/></a>');
	document.write('</li>');
}
function writeBookmark(icone){
	document.write('<li id="add2Bookmark">');
	document.write('<a class="tool" href="javascript:add2Bookmark()">Ajouter aux favoris <img src="'+icone+'" width="18"  alt=""/></a>');
	document.write('</li>');
}
function writeHeaderStopFlash(){
	document.write('<a id="stopheaderflash" class="stopflash" ');
	document.write('href="javascript:switchflash(\'header\',\'stopheaderflash\')">');
	document.write('Arr&ecirc;ter l\'animation</a>');
}
function writeMenuStopFlash(){
	document.write('<a id="stopmenuflash" href="javascript:switchflash(\'menu\',\'stopmenuflash\')">');
	document.write('Arr&ecirc;ter l\'animation</a>');
}

function printToolsList(contexturl, envoyerurl){
	document.write('<ul id="tools">');
	writeTextePlus(contexturl+'/img/ico_agrandir.gif');
	writeTexteMoins(contexturl+'/img/ico_diminuer.gif');
	writePrint(contexturl+'/img/ico_imprimer.gif');
	writeBookmark(contexturl+'/img/ico_favoris.gif');
	document.write('<li><a class="tool" href="'+envoyerurl+'">Envoyer &agrave; un ami <img src="'+contexturl+'/img/ico_envoyer.gif" width="18" alt=""/></a></li>');
	document.write('</ul>');
}

function printDate(){
	var now = new Date();
	var jours = ['Dimanche', 'Lundi','Mardi','Mercredi','Jeudi','Vendredi', 'Samedi'];
	var mois = ['Janvier','F\351vrier','Mars','Avril','Mai','Juin','Juillet','Ao\373t','Septembre','Octobre','Novembre','D\351cembre'];
	//alert('jour : ' + now.getDay() + ', mois : ' + now.getMonth());
	//for( var i = 0; i< mois.length; i++){
	//	alert('mois['+i+'] = '+mois[i]);
	//}
	var formatedDate = jours[now.getDay()]+' '+now.getDate()+' '+mois[now.getMonth()]+' '+now.getFullYear();
	document.write(formatedDate);
}