/*============================================
JS :: CG67
============================================*/
/* pour AddInto */
var ai2_bkmks = "Sphinn,Delicious,Bookmark,Google,Ask,Bookmarks,Diigo,Blogmarks,Yahoo,Email,StumbleUpon,FriendFeed,Yoolink ,BlinkList,Slashdot,Newsvine,Spurl,Netlog,Technorati,MySpace,Live Favorites,Faves,Furl,reddit,Mikiwi,Mister Wong,Simpy,meneame,Propeller,YiGG,Webnews,CiteULike,Identica,Viadeo,oneview,Folkd,Multiply,LinkedIn,Fuzz,Twitter,Pioche,TapeMoi,Mixx,Blogasty,Facebook,Bebo,symbaloo,DZone,Sphere,Plaxo,Squidoo,Care2,Xanga,Digg,Yahoo Buzz,Bluegger,Scoopeo,Nuouz";

    Shadowbox.init({
        language: 'fr',
        players: ['img', 'html', 'iframe', 'swf', 'flv']
    });


function initRollMenu(elSelectBis, elSelect)
{
	//Cache tous les subMenu
	$(".submenu").css('display','none');

	//Pour le rollOver
	if ($(".li-submenu"))
	{
	    contentSubmenu = $(".li-submenu").children("ul");
	    contentSubmenu.each(function(i) {
	        $(this).attr('class', 'submenu countSubMenu_' + (i + 1).toString());
	    });
		$(".li-submenu").mouseover(function(){
			$(this).children(".submenu").css('display', 'block');
			//Pour IE6
				if(elSelectBis)
				{
					$("."+elSelectBis).css('visibility', 'hidden');
				}
				if(elSelectBis)
				{
					$("."+elSelect).css('visibility', 'hidden');
				}
		});
		
		$(".li-submenu").mouseout(function(){
			$(this).children(".submenu").css('display', 'none');
			//Pour IE6
				if(elSelect)
				{
					$("."+elSelectBis).css('visibility', 'visible');
				}
				if(elSelectBis)
				{
					$("."+elSelect).css('visibility', 'visible');
				}
		});
	}
}

//Focus + Blur sur les fields
function initInput(class_name)
{
	$("."+class_name).focus(function(){
		if ($(this).attr("value")==$(this).attr("title")) {$(this).attr("value", "")}
	});
	$("."+class_name).blur(function(){
		if ($(this).attr("value")=="")
		{
			$(this).attr("value", $(this).attr("title"));
		}
	});
}


/*==== Scroll dossiers home ====*/
var dureeDossier = 5000;

var totalSlides = 0;
var currentSlide = 1;
var contentSlides = "";

function scrollDossiers(cible)
{
	var nbCible = parseInt(cible);
  var scrollAmount = 0;
  
  contentSlides = $(".contener-dossier");
  contentSlides.each(function(i)
	  {
	    scrollAmount = parseInt(this.clientWidth);
	  }
  );

	if(nbCible > 0)
  	scrollAmount = scrollAmount*nbCible;
  else
  	scrollAmount = 0;
  
  $(".viewer-dossiers").animate({scrollLeft: scrollAmount}, 600, function() {
  		
  		$(".current_dossier").attr('value', cible);
  		
			clearInterval(timerDossier);
			timerDossier = setInterval("initScrollAutoDossiers()", dureeDossier);
  		
  		$(".li-dossier").children("a").attr('class', '');
  		btnSlides = $(".li-dossier");
  		btnSlides.each(function(i)
	  		{
	  			if( $(this).children("a").attr('rel')==cible )
	  			{
	  				$(this).children("a").attr('class', 'selected');
	  			}
	  		}
  		);
  		
  	});
}

function initScrollDossiers()
{
	var cible = 0;
	if( $(".li-dossier") )
	{	
		$(".li-dossier").click(function(){
			cible = $(this).children("a").attr('rel');
			scrollDossiers(cible);
		});
	}
}
function initScrollAutoDossiers()
{
	var cible = 0;
	var current_dossier = parseInt($(".current_dossier").attr('value'));
	
  btnSlides = $(".li-dossier");
  btnSlides.each(function(i)
	  {
			if(i==current_dossier)
			{
				if(i < btnSlides.length-1)
				{
					scrollDossiers(i+1);	
				}
				if(i == btnSlides.length-1)
				{
					scrollDossiers(0);	
				}
			}
	  }
  );	
}


/*==== Pad vote ====*/
function initPadVote()
{
	$(".vote").mouseover(function(){
		$(this).children(".btn-vote").attr("class", "btn-vote-over");
		$(this).children(".pad-vote").css("display", "block");
	});
	$(".vote").mouseout(function(){
		$(this).children(".btn-vote-over").attr("class", "btn-vote");
		$(this).children(".pad-vote").css("display", "none");
	});
}

/*==== Pad commentaire ====*/
function initPadCommentaire()
{
	$(".commentaire-form").css("display", "none");
	
	$(".btn-commentaire").click(function(){
		$(".commentaire-form").css("display", "block");
	});
}

/*==== Hide/Show Pad ====*/
function initPad(padParent, padEnfant, elSelect)
{
	$("."+padParent).children("."+padEnfant).css("display", "none");
	
	$("."+padParent).mouseover(function(){
		$(this).children("."+padEnfant).css("display", "block");
		//Pour IE6
		if(elSelect)
		{
			$("."+elSelect).css('visibility', 'hidden');
		}
	});
	$("."+padParent).mouseout(function(){
		$(this).children("."+padEnfant).css("display", "none");
		//Pour IE6
		if(elSelect)
		{
			$("."+elSelect).css('visibility', 'visible');
		}
	});
}

/*==== Limite les caractères d'un champ textarea ====*/
function limitChars(Nomclass, limit)
{
	var text = $('.'+Nomclass).val();
	var textlength = text.length;
	
	if(textlength > limit)
	{
		$('.'+Nomclass).val(text.substr(0,limit));
		return false;
	}
	else
	{
		//Zone info
		$('.NbCarsRestant').html('Encore '+ (limit - textlength) +' caract&egrave;res.');
		return true;
	}
}

function initLimitChars(Nomclass, limit)
{
	$('.'+Nomclass).keyup(function() {
		limitChars(Nomclass, limit);
	});
}



// Work Around to fix Combobox width issue of IE
function initDropdownMenu(Nomclass)
{
	$('.'+Nomclass).focus(function() {
		window.dropdown_menu_hack(this);
	});
}
function foo(px,py,pw,ph,baseElement,fid)
{
var win = document.getElementById(this.fid);
}

function dropdown_menu_hack(el)
{
if(navigator.appName.substring(0,3) != "Net")
{
if(el.runtimeStyle.behavior.toLowerCase()=="none"){return;}
el.runtimeStyle.behavior="none";

var ie5 = (document.namespaces==null);
el.ondblclick = function(e)
{
window.event.returnValue=false;
return false;
}

if(window.createPopup==null)
{

var fid = "dropdown_menu_hack_" + Date.parse(new Date());

window.createPopup = function()
{
if(window.createPopup.frameWindow==null)
{
el.insertAdjacentHTML("MyFrame","<iframe id='"+fid+"' name='"+fid+"' src='about:blank' frameborder='1' scrolling='no'></></iframe>");
var f = document.frames[fid];
f.document.open();
f.document.write("<html><body></body></html>");
f.document.close();
f.fid = fid;


var fwin = document.getElementById(fid);
fwin.style.cssText="position:absolute;top:0;left:0;display:none;z-index:99999;";


f.show = function(px,py,pw,ph,baseElement)
{
py = py + baseElement.getBoundingClientRect().top + Math.max( document.body.scrollTop, document.documentElement.scrollTop) ;
px = px + baseElement.getBoundingClientRect().left + Math.max( document.body.scrollLeft, document.documentElement.scrollLeft) ;
fwin.style.width = pw + "px";
fwin.style.height = ph + "px";
fwin.style.posLeft =px ;
fwin.style.posTop = py ;
fwin.style.display="block";
}


f_hide = function(e)
{
if(window.event && window.event.srcElement && window.event.srcElement.tagName && window.event.srcElement.tagName.toLowerCase()=="select"){return true;}
fwin.style.display="none";
}
f.hide = f_hide;
document.attachEvent("onclick",f_hide);
document.attachEvent("onkeydown",f_hide);

}
return f;
}
}

function showMenu()
{

function selectMenu(obj)
{
var o = document.createElement("option");
o.value = obj.value;
//alert("val"+o.value)
o.innerHTML = obj.innerHTML;
while(el.options.length>0){el.options[0].removeNode(true);}
el.appendChild(o);
el.title = o.innerHTML;
el.contentIndex = obj.selectedIndex ;
el.menu.hide();
if(el.onchange)
{
el.onchange();
}
}


el.menu.show(0 , el.offsetHeight , 10, 10, el);
var mb = el.menu.document.body;

mb.style.cssText ="border:solid 1px black;margin:0;padding:0;overflow-y:auto;overflow-x:auto;background:white;font:12px Tahoma, sans-serif;";
var t = el.contentHTML;
//alert("1"+t);
t = t.replace(/<select/gi,'<div');
//alert("2"+t);
t = t.replace(/<option/gi,'<span');
//alert("3"+t);
t = t.replace(/<\/option/gi,'</span');
//alert("4"+t);
t = t.replace(/<\/select/gi,'</div');
mb.innerHTML = t;
//mb.innerHTML = "<div><span value='dd:ff'>gfgfg</span></div>";

el.select = mb.all.tags("div")[0];
el.select.style.cssText="list-style:none;margin:0;padding:0;";
mb.options = el.select.getElementsByTagName("span");

for(var i=0;i<mb.options.length;i++)
{

mb.options[i].selectedIndex = i;
mb.options[i].style.cssText = "list-style:none;margin:0;padding:1px 2px;width/**/:100%;cursor:hand;cursorointer;white-space:nowrap;"
mb.options[i].title =mb.options[i].innerHTML;
mb.options[i].innerHTML ="<nobr>" + mb.options[i].innerHTML + "</nobr>";
mb.options[i].onmouseover = function()
{
if( mb.options.selected )
{mb.options.selected.style.background="white";mb.options.selected.style.color="black";}
mb.options.selected = this;
this.style.background="#333366";this.style.color="white";
}
mb.options[i].onmouseout = function(){this.style.background="white";this.style.color="black";}
mb.options[i].onmousedown = function(){selectMenu(this); }
mb.options[i].onkeydown = function(){selectMenu(this); }
if(i == el.contentIndex)
{
mb.options[i].style.background="#333366";
mb.options[i].style.color="white";
mb.options.selected = mb.options[i];

}
}
var mw = Math.max( ( el.select.offsetWidth + 22 ), el.offsetWidth + 22 );
mw = Math.max( mw, ( mb.scrollWidth+22) );
var mh = mb.options.length * 15 + 8 ;
var mx = (ie5)?-3:0;
var my = el.offsetHeight -2;
my=my+5;
var docH = document.documentElement.offsetHeight ;
var bottomH = docH - el.getBoundingClientRect().bottom ;
mh = Math.min(mh, Math.max(( docH - el.getBoundingClientRect().top - 50),100) );
if(( bottomH < mh) )
{
mh = Math.max( (bottomH - 12),10);
if( mh <100 )
{
my = -100 ;
}
mh = Math.max(mh,100);
}
self.focus();
el.menu.show( mx , my , mw, mh , el);
sync=null;
if(mb.options.selected)
{
mb.scrollTop = mb.options.selected.offsetTop;
}
window.onresize = function(){el.menu.hide()};
}
function switchMenu()
{
if(event.keyCode)
{
if(event.keyCode==40){ el.contentIndex++ ;}
else if(event.keyCode==38){ el.contentIndex--; }
}
else if(event.wheelDelta )
{
if (event.wheelDelta >= 120)
el.contentIndex++ ;
else if (event.wheelDelta <= -120)
el.contentIndex-- ;
}
else{return true;}
if( el.contentIndex > (el.contentOptions.length-1) ){ el.contentIndex =0;}
else if (el.contentIndex<0){el.contentIndex = el.contentOptions.length-1 ;}
var o = document.createElement("option");
o.value = el.contentOptions[el.contentIndex].value;
o.innerHTML = el.contentOptions[el.contentIndex].text;
while(el.options.length>0){el.options[0].removeNode(true);}
el.appendChild(o);
el.title = o.innerHTML;
}
if(dropdown_menu_hack.menu ==null)
{
dropdown_menu_hack.menu = window.createPopup();
document.attachEvent("onkeydown",dropdown_menu_hack.menu.hide);
}
el.menu = dropdown_menu_hack.menu ;
el.contentOptions = new Array();
el.contentIndex = el.selectedIndex;
el.contentHTML = el.outerHTML;

for(var i=0;i<el.options.length;i++)
{

el.contentOptions [el.contentOptions.length] =
{
"value": el.options[i].value,"text": el.options[i].innerHTML
}
if(!el.options[i].selected){el.options[i].removeNode(true);i--;};
}
el.onkeydown = switchMenu;
el.onclick = showMenu;
el.onmousewheel= switchMenu;
}
}

function initExternalLinks() {
    $("a.lien-externe").each(function() {
        var ext_link_title = $(this).attr('title');
        if (ext_link_title != "") {
            ext_link_title += " (s\'ouvre dans une nouvelle fenêtre)";
        }
        else {
            ext_link_title = 'Lien externe qui s\'ouvre dans une nouvelle fenêtre';
        }
        $(this).attr('title', ext_link_title);
        $(this).click(function() {window.open(this.href);return false;});
    });
}

function randomWallpaper() {
    var x = Math.random();
    if (x < 0.083) {
        document.body.className = "cycliste";
    }
    if (x >= 0.083 && x < 0.166) {
        document.body.className = "pieton";
    }
    if (x >= 0.166 && x < 0.249) {
        document.body.className = "chat";
    }
    if (x >= 0.249 && x < 0.332) {
        document.body.className = "passage";
    }
    if (x >= 0.332 && x < 0.415) {
        document.body.className = "bus";
    }
    if (x >= 0.415 && x < 0.498) {
        document.body.className = "personnes";
    }
    if (x >= 0.498 && x < 0.581) {
        document.body.className = "stop";
    }
    if (x >= 0.581 && x < 0.664) {
        document.body.className = "fleches";
    }
    if (x >= 0.664 && x < 0.747) {
        document.body.className = "coeur";
    }
    if (x >= 0.747 && x < 0.83) {
        document.body.className = "bulles";
    }
    if (x >= 0.83 && x < 0.913) {
        document.body.className = "fleurs";
    }
    if (x >= 0.913 && x < 1) {
        document.body.className = "fleur";
    }
}


/*============================================
JS :: Airsoft
============================================*/
/* Modified to support Opera */
function bookmarksite(title, url) {
    if (window.sidebar) // firefox
        window.sidebar.addPanel(title, url, "");
    else if (window.opera && window.print) { // opera
        var elem = document.createElement('a');
        elem.setAttribute('href', url);
        elem.setAttribute('title', title);
        elem.setAttribute('rel', 'sidebar');
        elem.click();
    }
    else if (document.all)// ie
        window.external.AddFavorite(url, title);
}
