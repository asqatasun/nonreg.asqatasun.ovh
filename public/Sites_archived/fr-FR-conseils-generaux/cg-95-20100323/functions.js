// WebCT95
//
// Copyright (C) 2003 Conseil G?n?ral du Val d'Oise. All Rights
// Reserved.
//
// Developed by IDEALX S.A.S. and Bull S.A.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
// 02111-1307, USA.



// Example :
// showMessage('You should login first !');
function showMessage(message) {
    alert(message);
}


// Example :
// resetTheFormTarget('form1')
function resetTheFormTarget(targetElementId) {

    // If the browser doesn't even support
    // document.getElementById, let's give up now.
    if (!document.getElementById) {
      return true;
    }

    // Static example :
    //document.ContentManagementForm.target='';

    document.getElementById(targetElementId).target='';
}


// Example :
// openFormInWindow('previewWindow', 'form1', 'large')
function openFormInWindow(newWindowName, targetElementId, sizeName) {

    // If the browser doesn't even support
    // document.getElementById, let's give up now.
    if (!document.getElementById) {
      return true;
    }

    // Static examples :
    //document.ContentManagementForm.target='preview'; var PreviewWindow=window.open('','preview','width=500,height=200'); PreviewWindow.focus();
    //var previewWindow = open('','preview','width=800,height=400'); previewWindow.focus(); window.open('',target='preview');

    // Change the form target
    document.getElementById(targetElementId).target = newWindowName;

    // Create the popup window

    //
    // General form of the function
    // variable = open(url, newWindowName, 'parameters', hist);
    // url        : the url to load in the new window
    // newWindowName : the name of the target to use (existing or not)
    //
    // Static size version
    //var newWindow = open('', newWindowName, 'width=800,height=500');
    //
    // Parameterize size version
    var newWindow = open('', newWindowName, getSizeParameters(sizeName));

    // Set the focus on the popup newWindow and bring it on top of all the other windows
    newWindow.focus();
}


// Example :
// openUrlInWindow('/struts/c39_PageManagement.do', 'associationWindow', 'large')
function openUrlInWindow(url, windowName, sizeName) {
    var newWindow = window.open(url, windowName, getSizeParameters(sizeName));
    newWindow.focus();
    return newWindow;
}


// Example :
// openFormInParentWindow('/struts/c23_ContentManagement.do', 'form1')
function openFormInParentWindow(url, targetElementId) {

    // First submit the form
    document.getElementById(targetElementId).submit();

    // Then load the url in the parent window
    window.opener.location = url;

    // And finally close the current window
    window.close();
}


// Example :
// openLinkInWindow('/struts/c11_ContentManagement.do', 'linkWindow', 'link1', 'small')
//
// WARNING :
// It is very important to have the 'id' of the <a> element or the 'styleId' of
// the <html:link> element given as the hrefElementId parameter of the function.
//
// <html:link styleClass="liencontenuon" styleId="link3"
// href="/struts/c12_ContentManagement.do"
// onclick="openLinkInWindow('/struts/c12_ContentManagement.do', 'linkWindow',
// 'link3', 'small')">&gt; Associer</html:link>
function openLinkInWindow(url, newWindowName, hrefElementId, sizeName) {

    // If the browser doesn't even support
    // document.getElementById, let's give up now.
    if (!document.getElementById) {
      return true;
    }

    // Disable the href of the link by making it return javascript:void(0)
    document.getElementById(hrefElementId).href = 'javascript:void(0)';

    // Create the popup window
    //
    // Static size version
    //var newWindow = open(url, newWindowName, 'width=800,height=500');
    //
    // Parameterize size version
    var newWindow = open(url, newWindowName, getSizeParameters(sizeName));

    // Set the focus on the popup window and bring it on top of all the other windows
    newWindow.focus();
}


// See absolute-size at http://www.w3.org/TR/REC-CSS2/fonts.html#font-size-props
// to know how the size names have been chosen
function getSizeParameters(sizeName) {
    var sizeParameters = '';
    var width,height;

    switch (sizeName) {
	// This size can be redefined because it is not used yet
        case 'xx-small':
	    sizeParameters = 'width=300,height=150,location=0,resizable=yes,scrollbars=yes';
	    width = 300;
	    height = 150;
	    break;

	// typical use : confirmation window
        case 'x-small':
	    sizeParameters = 'width=300,height=150,location=0,resizable=yes,scrollbars=yes';
	    width = 300;
	    height = 150;
	    break;

	case 'small':
	    sizeParameters = 'width=400,height=200,location=0,resizable=yes,scrollbars=yes';
	    width = 400;
	    height = 200;
	    break;

	// typical use : an association window (eg associate a media to a content)
	case 'medium':
	    sizeParameters = 'width=600,height=400,location=0,resizable=yes,scrollbars=yes';
	    width = 600;
	    height = 400;
	    break;

	// typical use : an association window (eg associate a media to a content)
        case 'mediumasso':
	    sizeParameters = 'width=600,height=400,location=0,resizable=yes,scrollbars=yes';
	    width = 600;
	    height = 400;
	    break;

	case 'large':
	    sizeParameters = 'width=800,height=600,location=0,resizable=yes,scrollbars=yes';
	    width = 800;
	    height = 600;
	    break;

	case 'geoloc':
	    sizeParameters = 'width=970,height=700,location=0,resizable=yes,scrollbars=yes,status=yes';
	    width = 870;
	    height = 570;
	    break;

	// a special window size asked by CG95
	case 'sendContent':
	    sizeParameters = 'width=590,height=580,location=0,resizable=yes,scrollbars=no';
	    width = 590;
	    height = 430;
	    break;

	case 'x-large':
	    sizeParameters = 'width=860,height=700,location=0,resizable=yes,scrollbars=yes';
	    width = 860;
	    height = 780;
	    break;

	// This size can be redefined because it is not used yet
	case 'xx-large':
	    sizeParameters = 'width=860,height=740,location=0,resizable=yes,scrollbars=yes';
	    width = 860;
	    height = 780;
	    break;

	case 'widescreen':
	    sizeParameters = 'width=830,height=500,location=0,resizable=no,scrollbars=no';
	    width = 830;
	    height = 500;
	    break;

	default:
	    sizeParameters = 'width=400,height=200,location=0,resizable=yes,scrollbars=yes';
	    width = 400;
	    height = 200;
	    break;
    }

    // center the popups onto the screen
    var screenX, screenY;
    if (navigator.appName == "Microsoft Internet Explorer") {
	screenY = document.body.offsetHeight
	screenX = window.screen.availWidth
    } else {
	screenY = screen.height;
	screenX = screen.width;
    }

    var leftvar, rightvar, leftprop, topprop;
    leftvar = (screenX - width) / 2
    rightvar = (screenY - height) / 2
    if (navigator.appName == "Microsoft Internet Explorer") {
	leftprop = leftvar
	topprop = rightvar
    } else {
	// adjust Netscape coordinates for scrolling
	leftprop = (leftvar - pageXOffset)
	topprop = (rightvar - pageYOffset)
    }
    if(leftprop<0) leftprop=0;
    if(topprop<0) topprop=0;
    sizeParameters = sizeParameters + ",left=" + leftprop;
    sizeParameters = sizeParameters + ",top=" + topprop;

    return sizeParameters;
}


// Function to validate the PageManagementForm's titles of bloc and title of page
function validPageManagementForm(formId) {
    var obj = document.getElementById(formId);
    // Number of elements in form
    var max = obj.length;
    var msg = "";

    for (var i = 0; i < max; i++) {
	// Retrieves the element i of the form
	field = obj.elements[i];

	// Retrieves the name of the element i
	fieldName = field.name;

	// Tests if the field title is empty
	if (fieldName == "title") {
	    // Retrieves the value of the field
	    fieldValue=field.value;

	    if ((fieldValue == "") || (fieldValue == null)) {
		msg=msg+"Le titre de la page doit �tre renseign�.\n";
	    }
	}

	// Association to an heading has been removed in heading pages screen

	// else if (fieldName == "heading") {
	// fieldValue = field.value;
	//  if ((fieldValue == "") || (fieldValue == null)) {
	//msg = msg + "La rubrique associ?e ? la page doit ?tre renseign?e.\n";
	//  }
	//}

	// Bloc titles are now optional

	// else {
	    // Test if the field is a "blocTitle" field
	//	    if (fieldName.length == 10) {
	//if (fieldName.substr(0,9) == "blocTitle") {
		    // Retrieves the value of the field
	//    fieldValue = field.value;

		    // Tests if the field is empty
	//    if ((fieldValue == "") || (fieldValue == null)) {
	//	j = fieldName.substr(9,1);
	//	j = eval(j)+1;
	//	msg = msg + "Le titre du bloc " + j + " doit ?tre renseign?.\n";
	//    }
	//}
	//  }
	//   }
    }

    if (msg != "") {
	alert(msg);
	return false;
    }

    // When there is no empty fields
    return true;
}


function reloadListWithFilterHidden(val) {
	document.listFilterForm.categoryId.value = val;
	reloadListWithFilter();
}

// Reloads a list with a new filter
// @param this function doesn't use any param but needs that an
//     input 'baseUrl' to be present
function reloadListWithFilter() {
   var target = new String(window.location);
    var newTarget;

    if (target.search(/glue/) != -1) {
        var arrayStr = target.split("&categoryId");
        newTarget = arrayStr[0] + "&categoryId=" + document.listFilterForm.categoryId.value;
    } else {
        var encoded = encodeURI(target);

        // Add categoryId filter
        var arrayStr = target.split("\.html");
        var arrayStr2 = arrayStr[0].split("/categoryId");
        newTarget = arrayStr2[0] + "/categoryId" + document.listFilterForm.categoryId.value +".html";
        //      newTarget = target.replace(/\.html$/, "/categoryId" + document.listFilterForm.categoryId.value + ".html");
    }

    // Reloading the navigator at the new location
    window.location.replace(newTarget);
}
// Filter for list in pdr
function reloadListWithFilterPdr(assertion,idCat) {
   var target = new String(window.location);
    var newTarget;

 //var recherche = assertion.indexOf(".html");
 var recherche = assertion.split(".");
 newTarget = recherche[0] + "/categoryId" + idCat + "." + recherche[1];

    // Reloading the navigator at the new location
   window.location.replace(newTarget);
   //window.location.replace(assertion);
}

// Update the target of the external data filter form
// with filters parameters and reload the page
function extDataReloadFilter() {
    var target = document.extDataFilterForm.baseUrl.value;

    // Add locality filter
    target += "/locality/"
	+document.extDataFilterForm.localityId.value;

    // Add extra filter
    target += "/"+document.extDataFilterForm.filterName.value+"/"
	+document.extDataFilterForm.filterValue.value;

    //alert(target);

    // reload with new location
    window.location.replace(target);
}


// Inits hidden fields to know if the checkbox have been checked
// (enables to call the "set" method for the checkbox attributes
// that struts does not call when the checkbox are not checked.)
function controlIfChecked(form) {
    if (form.isHeadingHomePage.checked) {
	form.isHeadingHomePageChecked.value = "true";
    } else {
	form.isHeadingHomePageChecked.value = "false";
    }

    if (form.isSiteHomePage.checked) {
	form.isSiteHomePageChecked.value = "true";
    } else {
	form.isSiteHomePageChecked.value = "false";
    }
}


// Functions used in contents pages to detect changes
// and inform users it has to save them
function setDocumentHasChanged(form, val) {
   document.getElementById(form).documentHasChanged.value = val;
}

// used by content edition screen
function doTabMapping(form, zone) {
   var doTabMapping = false;
   if (document.getElementById(form).documentHasChanged.value == 'true') {
     if (confirm('Abandonner les modifications en cours ?')) {
        doTabMapping = true;
     }
   } else {
     doTabMapping = true;
   }

   if (doTabMapping) {
      document.getElementById(form).tabChange.value = 'yes';
      document.getElementById(form).tabMapping.value = zone;
      document.getElementById(form).documentHasChanged.value = 'false';
      getMSIEEditorContent();
      document.getElementById(form).submit();
   }
}

// used by position screen
function doPositionTabMapping(form, zone) {
   var doTabMapping = false;
   if (document.getElementById(form).documentHasChanged.value == 'true') {
     if (confirm('Abandonner les modifications en cours ?')) {
        doTabMapping = true;
     }
   } else {
     doTabMapping = true;
   }

   if (doTabMapping) {
      document.getElementById(form).tabChange.value = 'yes';
      document.getElementById(form).tabMapping.value = zone;
      document.getElementById(form).documentHasChanged.value = 'false';
      Enregistrer();
   }
}

// used by workflow screen and list edition screen
function doSimpleTabMapping(form, zone) {
   var doTabMapping = false;
   if (document.getElementById(form).documentHasChanged.value == 'true') {
     if (confirm('Abandonner les modifications en cours ?')) {
        doTabMapping = true;
     }
   } else {
     doTabMapping = true;
   }

   if (doTabMapping) {
      document.getElementById(form).tabChange.value = 'yes';
      document.getElementById(form).tabMapping.value = zone;
      document.getElementById(form).documentHasChanged.value = 'false';
      document.getElementById(form).submit();
   }
}

// used by search box to toogle between plain text search style and keywords search style
function toggleBox(szDivID, iState) // 1 visible, 0 hidden
{
    if(document.layers)	   //NN4+
    {
       document.layers[szDivID].visibility = iState ? "show" : "hide";
    }
    else if(document.getElementById)	  //gecko(NN6) + IE 5+
    {
        var obj = document.getElementById(szDivID);
        obj.style.visibility = iState ? "visible" : "hidden";
    }
    else if(document.all)	// IE 4
    {
        document.all[szDivID].style.visibility = iState ? "visible" : "hidden";
    }
}


// escape caractere '<','>','\n','"' with the entities values. only for '\n' which is remplaced by '<br/>'
function myEscape(s){
	s=s.replace(/</g,'&lt;');
	s=s.replace(/>/g,'&gt;');
	s=s.replace(/\n/g,'<br/>');
	s=s.replace(/"/g,'&quot;');
	return s;
}

// indicate if the value s is found in the obj
function contains(obj,s){
	for(var i in obj){
		if(obj[i]==s) return true;
	}
	return false;
}



// This block is to automatically set the right mode or filetype respectively
// for both Emacs and VIM.
//
// Local Variables:
// mode: java
// ispell-local-dictionary: "american"
// End:
//
// vim: set tw=80 filetype=javascript:
