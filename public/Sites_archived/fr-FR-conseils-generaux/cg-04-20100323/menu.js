window.addEvent('domready', function(){
	$$('.noclick a').each(function(elem,i){
		elem.addEvent('click', function(e){
			if (elem.getParent().hasClass('noclick'))
			{
				e = new Event(e).stop();
				$$('.noclick a').each(function(elem2,i){
					if (elem2.getParent().hasClass('noclick'))
					{
						var type_class2 = elem2.getParent().getProperty('class').substr(0,9);
						if (type_class2.substr(0,2) == 'on')
						{
							elem2.getNext().setStyle('display','none');
							elem2.getParent().removeClass(type_class2);
							elem2.getParent().addClass('off'+type_class2.substr(2,9));
							elem2.getParent().removeClass('noclick');
							elem2.getParent().addClass('noclick');
						}
						
					
					}
								
				});
				var type_class = elem.getParent().getProperty('class').substr(0,10);
				if (type_class.substr(0,3) == 'off')
				{
					elem.getNext().setStyle('display','block');
					elem.getParent().removeClass(type_class);
					elem.getParent().removeClass('noclick');
					elem.getParent().addClass('on'+type_class.substr(3,10));
					elem.getParent().addClass('noclick');
				}
			}
		});
	});
});