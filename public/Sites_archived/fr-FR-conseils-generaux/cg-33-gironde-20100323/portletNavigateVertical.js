// ---------------------------------------
// Vertical Menu Functions |||||||||||||||
// ---------------------------------------

var expandMenu = false;

function vMenuOver(obj, iconOpen, iconClosed) {
  expandMenu = true;
  icon = obj.className=="open" ? iconClosed : iconOpen;
  obj.src = icon;
}

function vMenuOut(obj, iconOpen, iconClosed) {
  expandMenu = false;
  icon = obj.className=="open" ? iconOpen : iconClosed;
  obj.src = icon;
}

// Expand menu only if the mouse is over an expand icon
function vMenuLink(obj, iconOpen, iconClosed) {
  if (expandMenu == true) {
    toggleMenu(obj, iconOpen, iconClosed);
    return false;
  }
}

// Toggle the menu visibility and change the expand icon
function toggleMenu(obj, iconOpen, iconClosed) {
  if (obj.parentNode.lastChild.className=="open") {
    obj.firstChild.className = "close";
    obj.parentNode.lastChild.className = "close";
  } else {
    obj.firstChild.className = "open";
    obj.parentNode.lastChild.className = "open";
  }
}

// Deroule les sous categories de la categorie selectionnee
function deplie(obj, iconOpen, iconClosed, nbCat) {
	replie(nbCat);	
	toggleMenu(obj,iconOpen,iconClosed);
	if (obj.parentNode.className == "level0 open") {
		obj.parentNode.firstChild.nextSibling.firstChild.src = iconOpen;
	} else {
		obj.parentNode.firstChild.nextSibling.firstChild.src = iconClosed;
	}
	//alert(obj.firstChild.src);
}

// Replie les sous categories deroulees
function replie(nbCat) {
	for(i=0;i<nbCat;i++){
		str = 'lien'+i;
		if(document.getElementById(str) != null){
			if(document.getElementById(str).className == "level0 " || document.getElementById(str).className == "level0"){	
				document.getElementById(str).parentNode.lastChild.className = "close";
			}
		}
	}	
}
