// ie = 1 si internet explorer ou mozilla
var ie = document.layers?0:1;
function changePortlet(portlet, couleur) {
	if (ie==1) {
		//var _div=document.all? document.all["show_" + portlet] : document.getElementById("show_" + portlet);
		var _div= document.getElementById("show_" + portlet);
		if (_div.style.display=="none") {
			_div.style.display="block";
			document.images["img_" + portlet].src="front/images/portlet/fermer"+couleur+".gif";
			}
		else {
			_div.style.display="none";
			document.images["img_" + portlet].src="front/images/portlet/ouvrir"+couleur+".gif";
			}
	}
	else {
		if (document.layers["show_" + portlet].visibility=="hide") {
			document.layers["show_" + portlet].visibility="show";
			document.images["img_" + portlet].src="front/images/portlet/fermer"+couleur+".gif";
			}
		else {
			document.layers["show_" + portlet].visibility="hide";
			document.images["img_" + portlet].src="front/images/portlet/ouvrir"+couleur+".gif";
			}
	}
}

function agrandir(portlet) {
	if (ie==1) {
		document.getElementById("hide_" + portlet).style.display="none";
		document.getElementById("show_" + portlet).style.display="block";
	}
	
	// Enregistrement de l'�tat de la portlet dans un cookie : Etat agrandis
 setCookie(portlet, 'agrandis');
}
function reduire(portlet) {
	if (ie==1) {
		document.getElementById("show_" + portlet).style.display="none";
		document.getElementById("hide_" + portlet).style.display="block";
	}
	
 // Enregistrement de l'�tat de la portlet dans un cookie : Etat r�duis
 setCookie(portlet, 'reduis');
}

/*
Gestion des cookies pour les PortLets permettant de conserver l'�tat ouvert/ferm�
*/
function getCookieContextValue(contextId) {
				var leCookie = document.cookie;
				
    var index = leCookie.indexOf(contextId + "=");
    if (index == -1) return null;
    index = leCookie.indexOf("=", index) + 1; // first character
    var endstr = leCookie.indexOf(";", index);
    if (endstr == -1) endstr = leCookie.length; // last character
    return unescape(leCookie.substring(index, endstr));
}

function setCookie(name, value) {
  var today = new Date();
  var expiry = new Date(today.getTime() + 28 * 24 * 60 * 60 * 1000); // plus 28 days
	
  if (value != null && value != "")
    document.cookie=name + "=" + escape(value) + "; expires=" + expiry.toGMTString();
}


