/*
Javascript two_columns.js

Permet de g�rer l'affichage des boutons pr�c�dent, suivant
pour l'affichage de plusieurs colonnes
*/


// ie = 1 si internet explorer ou mozilla
var ie = document.layers? 0 : 1;


// HashMap des pages courantes par contexte
// Par l'ID de contexte, on r�cup�re la page actuelle
var ht_pagesByContexte = {};


// retourne le num�ro de page en fonction du contexte
function getCurrentPage( contexte_id )
{
	var page = ht_pagesByContexte[contexte_id];
	
	if ( page == null )
		return 1;
		
	return parseInt(page);
}

// Modifie le num�ro de page courante
function setCurrentPage( contexte_id, current )
{
	if ( contexte_id != null && contexte_id != "" )
		ht_pagesByContexte[contexte_id] = current;
}

/*
Lance l'affichage des DIV entre deb et fin exclue et le 
masquage les autres DIV entre 1 et deb, et entre fin et max

deb : borne inf�rieure des DIV � afficher
fin : borne sup�rieure des DIV � afficher (borne exclue)
max : nombre total de DIV
*/
function showDiv( id, deb, fin, max)
{
	// R�duction pages inf�rieures
	for( i = 1; i < deb; i++)
	{
		hide( id +'_'+ i);
	}

	// Affichage des pages entre deb et fin-1 (borne sup�rieure exclue)
	for( i = deb; i < fin; i++)
	{
		show( id +'_'+ i);
	}

	// R�duction pages sup�rieures
	for( i = fin; i <= max; i++)
	{
		hide( id +'_'+ i);
	}

}


/*
Affiche les blocs DIV dont le nom est pass� sous la forme 
Identifiant_Numero
- contexte_id : Identifiant communs � toutes les pages
- nb_pages : nombre de pages � affich�s (affichage des pages
  de current_page � current_page + nb_pages
- nb_pages_total : nombre de pages total
*/
function afficher_pages_precedentes( contexte_id, nb_pages, nb_pages_total )
{
	// Premi�re page � afficher	
	var current_page = getCurrentPage(contexte_id) - nb_pages;
	setCurrentPage(contexte_id, current_page);

	if ( current_page <= 1 )
	{
		current_page = 1;
		setCurrentPage(contexte_id, current_page);
		
		// Masquage du bouton pr�c�dent s'il n'y a plus d'�lement
		if ( current_page - nb_pages < 1 )
		{
			show( contexte_id +'_btPred_inactif');
			hide( contexte_id +'_btPred_actif');
		}

	}

	showDiv( contexte_id, current_page, current_page + nb_pages, nb_pages_total);
	
	// Affichage du bouton suivant
	show( contexte_id +'_btSuiv_actif');
	hide( contexte_id +'_btSuiv_inactif');
}

/*
Affiche les blocs DIV dont le nom est pass� sous la forme Identifiant_Numero
- contexte_id : Identifiant communs � toutes les pages
- nb_pages : nombre de pages � affich�s (affichage des pages de current_page � current_page + nb_pages
- nb_pages_total : nombre de pages total
*/
function afficher_pages_suivantes( contexte_id, nb_pages, nb_pages_total )
{
	// Premi�re page � afficher	
	var current_page = getCurrentPage(contexte_id) + nb_pages;
	setCurrentPage(contexte_id, current_page);

	if ( current_page >= nb_pages_total - nb_pages + 1 )
	{
		current_page = nb_pages_total - nb_pages + 1;
		setCurrentPage(contexte_id, current_page);
		
		// Masquage du bouton suivant s'il n'y a plus d'�lement
			show( contexte_id +'_btSuiv_inactif');
			hide( contexte_id +'_btSuiv_actif');
	}
	
	showDiv( contexte_id, current_page, current_page + nb_pages, nb_pages_total);
	
	// Affichage du bouton pr�cedent
	show( contexte_id +'_btPred_actif');
	hide( contexte_id +'_btPred_inactif');
}

