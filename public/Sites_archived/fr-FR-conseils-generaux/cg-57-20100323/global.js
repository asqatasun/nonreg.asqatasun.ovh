function getInputValue(inputId) {
	var input = document.getElementById(inputId);
	if (input) {
		if (input.value) {
			var value = input.value;
			return value;
		}
	}
	return "";
}

document.write('<div class="popDiv" id="popDiv"></div>');
document.write('<input id="popElt" type="hidden" value=""/>');
document.write('<style type="text/css">.popDiv { position: absolute; visibility: hidden }</style>');

function showOrHidePopDivById(origDivId, elt, top, left, width) {
	var stylePopDiv=document.getElementById("popDiv").style;
	if (stylePopDiv.visibility == "visible"){
		hidePopDiv();
	} else {
		showPopDivById(origDivId, elt, top, left, width);
	}
}

function showPopDivById(origDivId, elt, top, left, width) {
	var origDiv = document.getElementById(origDivId);
	if (origDiv) {
		showPopDiv(origDiv.innerHTML, elt, top, left, width);	
	}
}

function showOrHidePopDiv(message, elt, top, left, width) {
	var stylePopDiv=document.getElementById("popDiv").style;	
	if (stylePopDiv.visibility == "visible"){
		hidePopDiv();
	} else {
		showPopDiv(message, elt, top, left, width);
	}
}

function showPopDiv(message, elt, top, left, width) {
	if (!top) {
		var top = 40;
	}
	if (!left) {
		var left = 40;
	}
	if (!width) {
		var widthCol = 100;
	}
   var stylePopDiv=document.getElementById("popDiv").style;
   stylePopDiv.visibility="hidden";
   var posY = findPosY(elt);
   var posX = findPosX(elt);
   stylePopDiv.top=posY + top;
   stylePopDiv.left= posX + left;
   var content = '<table width="'+widthCol+'" cellpadding="0" cellspacing="0"><tr><td width="100%">' + message + '</td></tr></table>';
   document.getElementById("popDiv").innerHTML=content;
   stylePopDiv.visibility="visible";
} 

function hidePopDiv() {
	var stylePopDiv=document.getElementById("popDiv").style; 
     stylePopDiv.visibility="hidden";
} 

function findPosY(obj) {
   var curtop = 0;
   if (obj.offsetParent) {
       while (obj.offsetParent) {
           curtop += obj.offsetTop
           obj = obj.offsetParent;
       }
   }
   else if (obj.y)
       curtop += obj.y;
   return curtop;
}

function findPosX(obj) {
   var curleft = 0;
   if (obj.offsetParent) {
       while (obj.offsetParent) {
           curleft += obj.offsetLeft;
           obj = obj.offsetParent;
       }
   }
   else if (obj.x)
       curleft += obj.x;
   return curleft;
} 

	/*
	* Renvoi un objet en fonction de son id
	*/
	function getIdObject( name ) {
		var divID = name;
		if ( document.getElementById && document.getElementById( divID ) ) {// Pour les navigateurs r�cents
			Pdiv = document.getElementById( divID );
			PcH = true;
 		} else if ( document.all && document.all[ divID ] ) {// Pour les veilles versions
			Pdiv = document.all[ divID ];
			PcH = true;
		} else if ( document.layers && document.layers[ divID ] ) { // Pour les tr�s veilles versions
			Pdiv = document.layers[ divID ];
			PcH = true;
		} else {
			PcH = false;
		}
		if ( PcH ) {
			return Pdiv;
		}else{	
			return false;
		}
	}

	function showHideFilter( nameZone, nbCol ){
		var objet = getIdObject(nameZone);
		if(objet){
			var linkLabel = getIdObject(nameZone + '-label-lien');
			var linkAction = getIdObject(nameZone + '-action');
			if(objet.className == 'cachediv'){
				objet.className = '';
				linkLabel.innerHTML = 'Cacher les crit�res de filtre sur les colones';
				linkAction.className = '';
			}else{
				objet.className = 'cachediv';
				linkLabel.innerHTML = 'Afficher les crit�res de filtre sur les colones';
				linkAction.className = 'cachediv';
			}	
		}	
		for(i = 0; i < nbCol; i++){
			objet = getIdObject(nameZone + '-col' + i);
			if(objet){
				if(objet.className == 'cachediv'){
					objet.className = '';
				}else{
					objet.className = 'cachediv';
				}	
			}
		}
	}
	
	/*
	* Effacement de toutes les zones de filtre d'un tableau
	*/
	function clearFilter( nbCol ){
		for(i = 0; i < nbCol; i++){
			objet = getIdObject('filter_' + i);
			if(objet){
				objet.value = '';
			}
		}
	}
	
