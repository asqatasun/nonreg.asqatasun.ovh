var overlay_conf = {target: '#gallery', expose: '#f1f1f1'};
var gallery_conf = {speed: 500, template: '<div><strong>${title}</strong></div><div>${description}</div><span>Image ${index}/${total}</span>'};
var phototeque_conf = $.extend({preload:false}, gallery_conf); 

$(document).ready(function(){
	
	if( $("#portlet_4444_tabs").length ){
		// Tabulations Flash infos 
		$("#portlet_4444_tabs").tabs("#portlet_4444 > .portlet_4444_value");
	}
	
    if( $(".gallery a").length ){
    	// Gallery remplace l'url accessible par l'url Jquery Tools pour le slideshow
        $(".gallery a").each(function(){
        	$(this).removeAttr('href');
            $(this).attr('href', $(this).attr('img_url'));
            $(this).removeAttr('img_url');
        });
    	// Gallery phototheque
    	$(".gallery a").overlay(overlay_conf).gallery(gallery_conf);
    }
    
    if( $(".scrollable").length ){
    	//pagination a consulter/phototheque
    	$(".scrollable").scrollable({size:3}).navigator().mousewheel();
    }
    

    // Phototèque
    if( $("#phototeque").length ){
    	// Gallery remplace l'url accessible par l'url Jquery Tools pour le slideshow
        $("#phototeque a").each(function(){
        	$(this).removeAttr('href');
            $(this).attr('href', $(this).attr('img_url'));
            $(this).removeAttr('img_url');
        });
	    $("#phototeque").scrollable().circular().autoscroll({
	        autoplay: true, 
	        interval: 5000
	    }).mousewheel();
	    $("#phototeque a:not(.cloned)").overlay(overlay_conf).gallery(phototeque_conf);		// a:not(.cloned) to fix the 'circular' plugin behaviour (add 10 elements)
    }
    
});

/*$("#portlet_38794255 img").each(function(){
      w = $(this).width();
      maxwidth = 160;
      if (w > maxwidth){
          $(this).attr('width', maxwidth);
      }
  });*/