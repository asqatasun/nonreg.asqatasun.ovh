var msie5 = (navigator.userAgent.indexOf('MSIE 5') != -1);

//***********************************************************
// FCKEditor

var popup_editor_form = 'popup_fckeditor_form';
var width = 670;
var height = 580;

function popup_rte(input_id, label_edit) {
    var args, value;
    value = document.getElementById(input_id).value;
    args = '?input_id='+input_id+'&amp;label_edit='+escape(label_edit);
    str_window_features = 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=1,dependent=1,width=' + width + ',height=' + height;
    popup = window.open(popup_editor_form + args, input_id, str_window_features);
    if (!popup.opener) {
        popup.opener = window;
    }
    return false;
}


//************************************************************
// Folder content
var isSelected = false;

function toggleSelect(toggleSelectButton, selectAllText, deselectAllText) {
  formElements = toggleSelectButton.form.elements;

  if (isSelected) {
    for (i = 0; i < formElements.length; i++) {
      formElements[i].checked = false;
    }
    isSelected = false;
    toggleSelectButton.value = selectAllText;
  } else {
    for (i = 0; i < formElements.length; i++) {
      formElements[i].checked = true;
    }
    isSelected = true;
    toggleSelectButton.value = deselectAllText;
  }
}

//************************************************************
/**
 * Toggles an element's visibility.
 * Function to show tooltips.
 * If your element is a span, you must use inline display instead of block
 * XXX recognize div and span then automaticly choose the best display rule
 */
function toggleElementVisibility(id) {
  element = document.getElementById(id);
  if (element) {
    //window.alert(element.tagName)
    if (element.style.visibility == 'hidden') {
      element.style.visibility = 'visible';
      if (element.tagName == 'DIV') {
        element.style.display = 'block';
      } else if (element.tagName == 'SPAN') {
        element.style.display = 'inline';
      } else {
        element.style.display = 'block';
      }
    } else {
      element.style.visibility = 'hidden';
      element.style.display = 'none';
    }
  }
}

function showElement(show, id) {
  element = document.getElementById(id);
  if (element) {
    //window.alert(element.tagName)
    if (show) {
      element.style.visibility = 'visible';
      if (element.tagName in ['DIV', 'P']) {
        element.style.display = 'block';
      } else {
        element.style.display = 'inline';
      }
    } else {
      element.style.visibility = 'hidden';
      element.style.display = 'none';
    }
  }
}

//************************************************************
function trim(s) {
  if (s) {
    return s.replace(/^\s*|\s*$/g, "");
  }
  return "";
}

//************************************************************
function checkEmptySearch(formElem) {
  var query = trim(formElem.SearchableText.value);
  if (query != '') {
    formElem.SearchableText.value = query;
    return true;
  }
  formElem.SearchableText.value = query;
  formElem.SearchableText.focus();
  return false;
}

//************************************************************
/**
 * Sets focus on <input> elements that have a class attribute
 * containing the class 'focus'.
 * Examples:
 * <input type="text" id="username" name="__ac_name" class="focus"/>
 * <input type="text" id="searchableText" class="standalone giant focus"/>
 *
 * This function does not work on crappy MSIE5.0 and MSIE5.5.
 */
function setFocus() {
  if (msie5) {
    return false;
  }
  var elements = document.getElementsByTagName('input');
  for (var i = 0; i < elements.length; i++) {
    var nodeClass = elements[i].getAttributeNode('class');
    //alert("nodeClass = " + nodeClass);
    if (nodeClass) {
      var classes = nodeClass.value.split(' ');
      for (var j = 0; j < classes.length; j++) {
        if (classes[j] == 'focus') {
          elements[i].focus();
          return true;
        }
      }
    }
  }
}

/**
 * Validates that the input fields designated by the given ids are not empty.
 * It returns true if all the input fields are not empty, it returns false
 * otherwise.
 * Example:
 * <form onsubmit="return validateRequiredFields(['field1', 'field2'],
 * ['Title', 'Comments'], 'are empty while they are required fields.')">
 */
function validateRequiredFields(fieldIds, fieldLabels, informationText) {
  for (i = 0; i < fieldIds.length; i++) {
    element = document.getElementById(fieldIds[i]);
    if (element && !element.value) {
      window.alert("'" + fieldLabels[i] + "' " + informationText);
      return false;
    }
  }
  return true;
}

//************************************************************
function getSelectedRadio(buttonGroup) {
  if (buttonGroup[0]) {
    for (var i=0; i<buttonGroup.length; i++) {
      if (buttonGroup[i].checked) {
        return i
      }
    }
  } else {
    if (buttonGroup.checked) { return 0; }
  }
  return -1;
}

function getSelectedRadioValue(buttonGroup) {
  var i = getSelectedRadio(buttonGroup);
  if (i == -1) {
    return "";
  } else {
    if (buttonGroup[i]) {
    return buttonGroup[i].value;
    } else {
    return buttonGroup.value;
    }
  }
}

function getSelectedRadioId(buttonGroup) {
  var i = getSelectedRadio(buttonGroup);
  if (i == -1) {
    return "";
  } else {
    if (buttonGroup[i]) {
      return buttonGroup[i].id;
    } else {
      return buttonGroup.id;
    }
  }
}

/**
 * Return the label content corresponding to a radio selection
 */
function getSelectedRadioLabel(buttonGroup) {
  var id = getSelectedRadioId(buttonGroup);
  if (id == "") {
    return "";
  } else {
    for (var i=0; i<document.getElementsByTagName("label").length; i++) {
      var element_label = document.getElementsByTagName("label")[i];
      if (element_label.htmlFor == id) {
        return element_label.firstChild.nodeValue;
      }
    }
  }
}


//************************************************************
/*
    Used to highlight search terms
    from Geir B�kholt, adapted for CPS

 */
function highlightSearchTerm() {
  var query_elem = document.getElementById('searchGadget')
  if (! query_elem){
    return false
  }
  var query = query_elem.value
  // _robert_ ie 5 does not have decodeURI
  if (typeof decodeURI != 'undefined'){
    query = unescape(decodeURI(query)) // thanks, Casper
  }
  else {
    return false
  }
  if (query){
    queries = query.replace(/\+/g,' ').split(/\s+/)
    // make sure we start the right place and not higlight menuitems or breadcrumb
    searchresultnode = document.getElementById('searchResults')
    if (searchresultnode) {
      for (q=0;q<queries.length;q++) {
        // don't highlight reserved catalog search terms
        if (queries[q].toLowerCase() != 'not'
          && queries[q].toLowerCase() != 'and'
          && queries[q].toLowerCase() != 'or') {
          climb(searchresultnode,queries[q]);
        }
      }
    }
  }
}

function climb(node, word){
  // traverse childnodes
  if (! node){
    return false
  }
  if (node.hasChildNodes) {
    var i;
    for (i=0;i<node.childNodes.length;i++) {
      climb(node.childNodes[i],word);
    }
    if (node.nodeType == 3) {
      checkforhighlight(node, word);
      // check all textnodes. Feels inefficient, but works
    }
  }
}

function checkforhighlight(node,word) {
  ind = node.nodeValue.toLowerCase().indexOf(word.toLowerCase())
  if (ind != -1) {
    if (node.parentNode.className != "highlightedSearchTerm"){
      par = node.parentNode;
      contents = node.nodeValue;
      // make 3 shiny new nodes
      hiword = document.createElement("span");
      hiword.className = "highlightedSearchTerm";
      hiword.appendChild(document.createTextNode(contents.substr(ind,word.length)));
      par.insertBefore(document.createTextNode(contents.substr(0,ind)),node);
      par.insertBefore(hiword,node);
      par.insertBefore(document.createTextNode( contents.substr(ind+word.length)),node);
      par.removeChild(node);
    }
  }
}

/************************************************************
/**
 * searchLanguage widget functions
 * used to auto select languages checkbox/radio
 * cf CPSSchemas/skins/cps_schemas/widget_searchlanguage_render.pt
 */
function searchLanguageCheckSelected(languages, no_language, language) {
  var count=0;
  for (var i=0; i<languages.length; i++) {
    if (languages[i].checked) {
      count++;
    }
  }
  no_language.checked = (count <= 0);
  language.checked = (count > 0);
}

function searchLanguageClearSelected(languages) {
  for (i=0; i<languages.length; i++) {
    languages[i].checked = 0;
  }
}

function toggleLayers(more_block, more_items) {
  var objMoreBlock = document.getElementById(more_block).style;
  var objMoreItems = document.getElementById(more_items).style;
  if(objMoreBlock.display == "block")
    objMoreBlock.display = "none";
  if(objMoreItems.display == "none")
    objMoreItems.display = "block";
}

//-----------------------------------------------------------------------------
//Funtion previewPrint
//-----------------------------------------------------------------------------
function previewPrint()
{
if (navigator.appName  != 'Microsoft Internet Explorer') {
window.print();
}
else
{
document.body.insertAdjacentHTML('beforeEnd', '<OBJECT ID="nav" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>');
nav.ExecWB(7,1);
nav.outerHTML = "";
}
}


//-----------------------------------------------------------------------------
//Function rolloverMap
//-----------------------------------------------------------------------------
function rollOverMap(mapName, mapOver){
	document[mapName].src=mapOver;
}

//-----------------------------------------------------------------------------
//Function font control
//-----------------------------------------------------------------------------
function fontCtrl(action){

	if (!document.body.style.fontSize)  document.body.style.fontSize = '100%';

	if (action=='increase'){
		document.body.style.fontSize = parseInt(document.body.style.fontSize)+10+'%';
	}
	else if (action =='decrease'){
		document.body.style.fontSize = parseInt(document.body.style.fontSize)-10+'%';
	}
	else {
		alert("unknown action !");
	}
	return false
}

//-----------------------------------------------------------------------------
//Function utils
//-----------------------------------------------------------------------------

function openWindow(title, url, width, height){
    popup_title = 'popup'; // Avoid crashes in IE when title contains localized caracters or punctuation
    var w = window.open(url, popup_title, 'scrollbars=no, toolbar=no, location=no, menubar=no, statusbar=no, alwaysRaised=no, resizable=no, width='+width+', height='+height);
    w.focus();
    return false;
}

function openDocumentWindow(title, url, width, height){
    popup_title = 'popup'; // Avoid crashes in IE when title contains localized caracters or punctuation
    var w = window.open(url, popup_title, 'scrollbars=yes, resizable=yes, toolbar=no, location=no, menubar=no, statusbar=no, alwaysRaised=no, width='+width+', height='+height);
    w.focus();
    return false;
}

function updateDisplayedImage(target, image, album){
    var html = '<a href="'+image.url+'" onclick="return openWindow(\''+image.name+'\', \''+image.url+'\', '+width+', '+height+')"> <img src="'+image.src+'" alt="'+image.name+'" title="'+image.description+'"/></a><div class="legende">'+album+'</div>';
    target.innerHTML = html;
}

function addToFavorites(anchor){
    if (window.sidebar){
        window.sidebar.addPanel(anchor.getAttribute('title'), anchor.getAttribute('href'), "");
    }else if (window.external){
        window.external.AddFavorite(anchor.getAttribute('href'), anchor.getAttribute('title'));
    }
}

//-----------------------------------------------------------------------------
//Function rollover
//-----------------------------------------------------------------------------

var curseur = false;

/*function move(e){
    if (document.getElementById("curseur") && curseur){
        var elem = document.getElementById("curseur");
        if (navigator.appName!="Microsoft Internet Explorer") {
            elem.style.left = e.pageX+5+"px";
            elem.style.top = e.pageY+10+"px";
        }else{
            if(document.documentElement.clientWidth>0) {
                elem.style.left = 10+event.x+document.documentElement.scrollLeft+"px";
                elem.style.top = 5+event.y+document.documentElement.scrollTop+"px";
            }else{
                elem.style.left = 10+event.x+document.body.scrollLeft+"px";
                elem.style.top = 5+event.y+document.body.scrollTop+"px";
            }
        }
    }
}*/

function move(e){
    if (document.getElementById("curseur") && curseur){
        var elem = document.getElementById("curseur");
        if (navigator.appName!="Microsoft Internet Explorer") {
            elem.style.left = e.pageX+5+"px";
            elem.style.top = e.pageY+10+"px";
        }
	else{
               elem.style.left = 10+event.x+document.body.scrollLeft+"px";
               elem.style.top = 480+event.y+document.body.scrollTop+"px";
        }
    }   
}


function display_infos(text){
    if(curseur==false){
        var elem = document.getElementById("curseur");
        elem.style.visibility = "visible";
        elem.innerHTML = text;
        curseur = true;
    }
}

function hide_infos(){
    if(curseur==true){
        document.getElementById("curseur").style.visibility = "hidden";
        curseur = false;
    }
}

document.onmousemove=move;

function display_block(id){
    var e = document.getElementById(id);
    var  f = document.getElementById(id+'_li');
    e.style.display = "block";
    if (f.className == "last"){
	f.className = "open last";
    }
    else if (f.className == ""){
	f.className="open";    
    }

}

function hide_block(id){
    var e = document.getElementById(id);
    var  f = document.getElementById(id+'_li');    
    e.style.display = "none";
    if (f.className == "open last"){
	f.className = "last";
    }
    else if (f.className == "open"){
	f.className="";    
    }
}

/* Ou Sortir */
function openManif(x) {
	x=window.open(x.href, 'popupmanif', 'height=500, width=400, top=20, left=20, scrollbars=yes, resizable=yes, status=yes');
	x.focus();
	return false;
}


function openLinkPopup(field_id){
    window.document.getElementById("hidden_link_widget").value = field_id;
    var w = window.open('browse_server.html');
    w.opener = window;
    return false;
}


//-----------------------------------------------------------------------------
// Show or hide a collapsible pane
//-----------------------------------------------------------------------------
function toggleCollapsiblePane(pane_id){
	var contents = $(pane_id);
	var icon = $("icon_"+pane_id);
	if( contents.visible() ){
		contents.hide();
		icon.removeClassName('collapsible_pane_icon_close');
		icon.addClassName('collapsible_pane_icon_open');
	}else{
		contents.show();
		icon.removeClassName('collapsible_pane_icon_open');
		icon.addClassName('collapsible_pane_icon_close');
	}
}
