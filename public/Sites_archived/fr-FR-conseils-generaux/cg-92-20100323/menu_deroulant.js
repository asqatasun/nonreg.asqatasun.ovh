// fonction qui ferme tous les niveaux 0 (sauf le sous menu de la page en cours ?)
function menu_nav_fermer_niveaux0(is_onload)
{		

	// on recupere tous les noeuds fils du menu de naviguation (= les <li> de premier niveau)
	var list_li = document.getElementById('menu_naviguation').childNodes;
	
	// si le menu n'est pas vide
	if (list_li != null)
	{		
		// on parcours chaque <li> pour fermer le <ul> qu'il contient			
		for (var i = 0 ; i < list_li.length ; i++)
		{			
			// firefox/gecko comptabilise les tab d'intendation comme des noeuds donc attention au type de noeud
			if (list_li[i].nodeType == 1)
			{
				ul_to_hide = list_li[i].getElementsByTagName('ul')[0];
				
				if (ul_to_hide != null)
				{
					span_titre_niv0 = list_li[i].firstChild;														
					while (span_titre_niv0.nodeType != 1)
						{	span_titre_niv0 = span_titre_niv0.nextSibling;	}
				
					a_titre_niv0 = span_titre_niv0.firstChild;
					while (a_titre_niv0.nodeType != 1)
						{	a_titre_niv0 = a_titre_niv0.nextSibling;	}
				
					// on ne ferme pas le menu de la page affichee au chargement de la page
					if (! is_onload || span_titre_niv0.getAttribute('displaylocked') == null)					
					{
						// 1) on ferme le sous menu de niveau 1 correspondant
						ul_to_hide.style.display='none';						
						// 2) on change la puce par celle dirigee vers la droite (puce de menu ferme)
						//a_titre_niv0.style.backgroundImage = 'url('+imagepath+'flechebleue1.gif)';
						a_titre_niv0.parentNode.parentNode.className = "";							
					}				
				}
			}
		}	
	}					
}


function menu_nav_fermer_niveaux0_onload()
{
	menu_nav_fermer_niveaux0(true);
}


function menu_nav_ouvrir_niveau0(titre_niv0)
{
	// si on essaye d'agir sur le menu correspondant a la page en cours, on ne fait RIEN
	//if (titre_niv0.getAttribute('displaylocked') == null)
	//{	
		menu_nav_fermer_niveaux0(false);
			
		a_node = titre_niv0.firstChild;
		while(a_node.nodeType != 1)
			{a_node = a_node.nextSibling;}
		
		text_node = a_node.firstChild;	
	
		// si le sous menu est deja ouvert, on le referme
		if (fenetre_ouverte == text_node.nodeValue)
		{
			//alert("Fenetre deja ouverte: on la referme");
			li_titre_niv0 = titre_niv0.parentNode;
			ul_to_hide = li_titre_niv0.getElementsByTagName("ul")[0];
			ul_to_hide.style.display='none';
			fenetre_ouverte = '';
		}
			// sinon on l'ouvre	
		else	
		{
			//alert("Fenetre fermee: on l'ouvre");
			li_titre_niv0 = titre_niv0.parentNode;
			ul_to_hide = li_titre_niv0.getElementsByTagName("ul")[0];
			ul_to_hide.style.display='block';		
			fenetre_ouverte = text_node.nodeValue;
			a_node.parentNode.parentNode.className = "actif";		
		}
	
	//}
	
}