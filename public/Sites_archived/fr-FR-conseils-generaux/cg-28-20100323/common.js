//*********** START FUNCTIONS FOR DIAPORAMA ***********

function fullOrNormal() {
    images = new Array;
    pics_loaded=0;
    if (fullsized)
    {
        normal();
    }
    else
    {
        full();
    }
    next_location = current_location;
    preload_photo(next_location);
    go_to_next_photo();
}

function toggleLoop() {
    if (loop)
    {
        loop = 0;
    }
    else
    {
        loop = 1;
    }
}

function changeElementText(id, newText) {
    element = document.getElementById(id);
    element.innerHTML = newText;
}

function stop() {
    changeElementText("stopOrStartText", "");
    onoff = 0;
    status = "Le diaporama est arrété, Cliquez sur [jouer] pour continuer.";
    clearTimeout(timer);
}

function play() {
    changeElementText("stopOrStartText", "");

    onoff = 1;
    status = "Le diaporama est lancé...";
    go_to_next_photo();
}

function full() {
    changeElementText("fullOrNormalText", "taille normale");
    fullsized = 1;
    status = "Le diaporama affiche des images en pleine taille, cliquez [taille normale] pour voir les images redimensionnées.";
}

function normal() {
    changeElementText("fullOrNormalText", "taille maximale");

    fullsized = 0;
    status = "Le diaporama affiche des images de taille normale, cliquez sur [taille maximale] pour voir les images en pleine taille.";
}

function preload_complete() {

}

function reset_timer() {
    clearTimeout(timer);
    if (onoff) {
        timeout_value = 3 * 1000;
        timer = setTimeout('go_to_next_photo()', timeout_value);
    }
}

function wait_for_current_photo() {
    /* Show the current photo */
    if (!show_current_photo())
    {
        /*
        * The current photo isn't loaded yet.  Set a short timer just to wait
        * until the current photo is loaded.
        */
        status = "Chargement de l'image...(" + current_location + " de" + photo_count + ").  " + "Patientez ..." ;
        clearTimeout(timer);
        timer = setTimeout('wait_for_current_photo()', 500);
        return 0;
    }
    else
    {
        status = "Le diaporama est lancé..." ;
        preload_next_photo();
        reset_timer();
    }
}

/* foto precedente */
function go_to_prev_photo()
{
    /* Go to the next location */
    current_location = next_location - 2;
    
    if ( current_location == 0 )
    {
        current_location = photo_count
    }
    
    if ( current_location == - 1 )
    {
        current_location = photo_count - 1
    }
    
    /* Show the current photo */
    if (!show_current_photo())
    {
        wait_for_current_photo();
        return 0;
    }
    
    preload_prev_photo();
    reset_timer();
}
    
function preload_prev_photo()
{
    /* Calculate the new next location */
    next_location = (parseInt(current_location) + parseInt(direction));
    if (prev_location > photo_count)
    {
        prev_location = 1;
        if (!loop) { stop(); }
    }
    
    if (prev_location == 0)
    {
        prev_location = photo_count;
        if (!loop)
        {
            stop();
        }
    }
    
    /* Preload the prev photo */
    preload_photo(prev_location);
}
/* foto precedente END */

function go_to_next_photo()
{
    /* Go to the next location */
    current_location = next_location;
    
    /* Show the current photo */
    if (!show_current_photo())
    {
        wait_for_current_photo();
        return 0;
    }
    
    preload_next_photo();
    reset_timer();
}

function preload_next_photo()
{
    /* Calculate the new next location */
    next_location = (parseInt(current_location) + parseInt(direction));
    if (next_location > photo_count)
    {
        next_location = 1;
        if (!loop) { stop(); }
    }
    if (next_location == 0)
    {
        next_location = photo_count;
        if (!loop)
        {
            stop();
        }
    }

    /* Preload the next photo */
    preload_photo(next_location);
}

function show_current_photo()
{
    if (!images[current_location] || !images[current_location].complete)
    {
        preload_photo(current_location);
        return 0;
    }
    var innerHTML;
    var imageHTML;

    /* transistion effects */
    imageHTML = '<img id="photo" src="' + images[current_location].src + '" name="slide" title="" alt="' + photo_captions[current_location] + '" />';
    if( photo_links[current_location] != '' )
    {
        innerHTML = '<a href="' + photo_links[current_location] + '"';
        if( photo_links[current_location] != '' )
        {
            innerHTML += 'class="' + photo_links_class[current_location] + '"';
        }
        innerHTML += '>';
        innerHTML += imageHTML;
        innerHTML += '</a>';
    }
    else
    {
       innerHTML = imageHTML;
    }
    changeElementText( 'image', innerHTML );
    changeCopyrightCaptionText( 'copyright_caption', current_location );
    tb_init('#image a.thickbox');
    
    return 1;
}

function preload_photo(index)
{
    /* Load the next picture */
    if (pics_loaded < photo_count) 
    {
        /* not all the pics are loaded.  Is the next one loaded? */
        if (!images[index])
        {
            images[index] = new Image;
            images[index].onLoad = preload_complete();
            
            if (fullsized)
            {
                images[index].src = document.getElementById("full_photo_urls_" + index).href;
            }
            else
            {
                images[index].src = document.getElementById("full_photo_urls_" + index).href;
            }
            pics_loaded++;
        }
    }
}


function setCaption(text)
{
    changeElementText("caption", text);
}

function changeCopyrightCaptionText(id, numElement) {
    element = document.getElementById(id);
    newHTML = '';
    if( photo_copyrights[numElement] != '')
    {
        newHTML += '<div class="copyright">' + photo_copyrights[numElement] + '</div>';
    }
    if( photo_legends[numElement] != '')
    {
        newHTML += '<div class="caption">' + photo_legends[numElement] + '</div>';
    }
    element.innerHTML = newHTML;
}
//*********** END FUNCTIONS FOR DIAPORAMA ***********