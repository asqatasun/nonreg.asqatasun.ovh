/*******************************************************************************
	F O N C T I O N S   D E   V A L I D A T I O N   D E   F O R M U L A I R E
********************************************************************************/	
/**
* tab: must contain:
* 0 form element
* 1 name of element to print in message
* 2 function to apply
* 3 size or arg to pass in function param (optionnal)
* 4 message to show instead of default error message (optionnal)
* !! Function to valid the elements take as param the form object, not its value !!
*/
var tab=new Array();
var tabOr=new Array();

function postwizard(form,comp_id,valid)
{
    var text="";
	var focusObj=null;
	var type="";
	
	//form test
	if( ! form)
	{
		alert("Please give a form name in postwizard()");
		return;
	}	
	
	if(comp_id)
	{
		eval("tab=tab"+comp_id);
	}
    for(var i=0;i<tab.length;i++)
    {
        if(tab[i][0])
		{
			var size = ( tab[i][3] )? tab[i][3]:'';
			
			if(tab[i][3])
			{
           		var mess=tab[i][3];
			}else
			{
				var mess="est invalide";
			}
			
			if(isArray(tab[i][0]))
			{
				str=''
				flag=false
				for(var j=0;j<tab[i][0].length;j++)
				{
					eval("var obj=document."+form+".elements[tab[i][0][j]]");
					
					if( ! eval(tab[i][2])(obj))
		    	    {
		    	        str=str+" "+tab[i][1][j];
		        	}else flag=true
				}
				if(str!="" && !flag)	text=text+"Un des champs suivants \""+str+"\" "+mess+"\n";
			}else
			{
				eval("var obj=document."+form+".elements[tab[i][0]]");
				
				if( ! eval(tab[i][2])(obj,size))
	    	    {
					if(tab[i][4])
					{
	            		var mess=tab[i][4];
					}else
					{
						var mess="est invalide";
					}
	    	        text=text+"Le champ \""+tab[i][1]+"\" "+mess+"\n";
					
					//Focus the first element not valid
					if(obj)
					{
						if( ! focusObj && (obj.type=="text" || obj.type=="textarea" || obj.type=="select"))
						{	
							focusObj=obj;
						}
					}
	        	}
			}
		}
    }

	if (text!="")
	//error
    {	
		alert(text);
		if(focusObj)
		{
			focusObj.focus();
		}
		if(valid=='no') return false;
    }else if(valid=='no')
	//empty text => no error => no submit => return true
	{
		return true
	}else	
	//empty text => no error => submit
    {
        eval("document."+form+".submit()");
    }	
}

function stringValid(obj,size)
{
	if(size=="")
	{
		size=1;
	}
    if(obj.value.length<size || obj.value=="")
    {
        return false;
    }else
    {
        return true;
    }
}

function passwdValid(obj,PassField)
{
	if(document.wizard.elements[PassField].value == obj.value)
	{
		return true;
	}else
	{
		return false;	
	}
}

function textareaValid(obj,size)
{
    if(obj.value.length>size)
    {
        return false;
    }else
    {
        return true;
    }
}

function emailValid(obj)
{
	if ( obj.value != 0 )
	{
		serie = obj.value.split('@');
		if ( serie.length == 2 )
		{
			virgule_deb = serie[0].indexOf(".");
			virgule_fin = serie[0].lastIndexOf(".");
			if ( virgule_deb != 0 && virgule_fin != serie[0].length-1 )
			{
				virgule_deb = serie[1].indexOf(".");
				virgule_fin = serie[1].lastIndexOf(".");
				lg = serie[1].length
				serie2 = serie[1].split('.')
				if ( virgule_deb != 0 && virgule_fin != lg-1 && serie2.length > 1 )
				{
					return 1;
				}
			}
		}
		return 0;
	} else {
		return 1;
	}
}

//check if the field is numeric
function numericValid(obj)
{
	var StrValidChars = "0123456789.-,";
	var StrString;
	var StrChar;
	var error;
	var Inti;
	var blnResult = true;
	error = 10;

	StrString = obj.value;

	if (StrString == ''){
		error = 0;
	}else{
			for (i = 0; i < StrString.length && blnResult == true; i++){
			      StrChar = StrString.charAt(i);
			      if (StrValidChars.indexOf(StrChar) == -1){
			               blnResult = false;
			               error = 1;
			      }
			}
	}

	switch(error)
	{
	case 0: return false;
			break;
	case 1: return false;
			break;
	default: return true;
	}
}

function radioValid(obj)
{		
	//Multi choice
	if(obj.length)
	{
		for(i=0; i<obj.length;i++)
   		{
    		if(obj[i].checked)
			{
	 			return true;
			}
   		}
		return false;
	}
	//One choice
	else	
	{
		return obj.checked;
	}
}

function checkboxValid(obj,multi) {
	if (obj != null && obj.length > 0) {
		var nb=0;
	   	   	   	
		for(i=0; i<obj.length;i++) {
	    	if(obj[i].checked)
			{
		 		nb++;
			}
	   	}
	   	
		if(multi!="") {
			if(nb==multi) {
				return true;
			}
			else {
				return false;
			}	
		}
		else {
			if(nb >= 1) {
				return true;
			}
			else {
				return false;
			}
		}
	}
	else {
		if (obj != null) {
			return obj.checked
		}
		else {
			return false;
		}
	}
}

function selectValid(obj)
{
	if(obj)
	{
		if ((obj.options[obj.selectedIndex].value != "") && (obj.options[obj.selectedIndex].value !=-1))
   		{    	
			return true;
	   	}
		return false;
	}
	return true;
}


//check if the field is integer (the is optional)
function integerValidOpt(obj)
{
	var StrValidChars = "0123456789";
	var StrString;
	var StrChar;
	var error;
	var Inti;
	var blnResult = true;
	error = 0;

	StrString = obj.value;

	for (i = 0; i < StrString.length && blnResult == true; i++){
	      StrChar = StrString.charAt(i);
	      if (StrValidChars.indexOf(StrChar) == -1){
	               blnResult = false;
	               error = 1;
	      }
	}

	if (error == 1)
		return false;
	else
		return true;
}

function dateValid(obj)
{
	if(obj.value)
	{
		date = obj.value.split("/");
	
		if ( isNaN(date[0]) || date[0] < 1 || date[0] > 31 )
		{
			return 0;
		}
		if ( isNaN(date[1]) || date[1] < 1 || date[1] > 12 )
		{
			return 0;
		}
		if ( isNaN(date[2]) )
		{
			return 0;
		}
	}
	return 1;
}

function neverValid()
{
	return false;
}

function selectAddValid(obj)
{
	eval("var text_field=document.wizard."+obj.name+"_text");

	if(text_field.value != '')
	{
		return true;
	}
	else if(obj.value)
	{
		if ((obj.options[obj.selectedIndex].value != "") && (obj.options[obj.selectedIndex].value !=-1))
		{
			return true;
		}
		return false;
	}
	return false;
}

function selectMultipleValid(field)
{
	if(field)
	{
		var nb = field.options.length;

		if (nb > 0)
		{
			return true;
		}
		return false;
	}
	return true;
}

function twoFieldsValid(field1,field2)
{
	eval("var field2=wizard."+field2);

	if ((field1.value=="") && (field2.value==""))
	{
		return true;
	}
	else
	{
		if ((field1.value=="") || (field2.value==""))
		{
			return false;
		}
		else
		{
			return true;
		}
	}
}

function returnToBr(obj)
{
	var	text1 = obj.value;
	var text2 = "";
	
	for (var i=0; i<text1.length; i++)
	{
		switch (text1.charCodeAt(i))
		{
			case 10:
				break;
			case 13:
				text2+="<br>";
				break;
			default:
				text2+=text1.charAt(i);
				break;
		}		
	}
	obj.value=text2;
	return true;
}

function returnToQuot(obj)
{
	var	text1 = obj.value;
	var text2 = "";
	
	for (var i=0; i<text1.length; i++)
	{
		text2+=text1.charAt(i).replace('"',"&quot;");
	}
	obj.value=text2;
	return true;
}

function stepaction(action,i)
{
	document.wizard.step_action.value = action;
	document.wizard.step_nb.value = i;
	document.wizard.submit();
}