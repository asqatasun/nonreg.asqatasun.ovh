/*************************************************************************
	F O N C T I O N S   U T L I S E E S   P A R   L A   G A L L E R I E
**************************************************************************/	
function returnGallery(IdNomCache, IdNomImg, ImgPath, typeGallery, width, height)
{
	var Choix = self.opener.document.getElementById(IdNomImg);
	var cacheChoix = self.opener.document.getElementById(IdNomCache);
	
	if (typeGallery == js_constant["C_GAL_IMG"])
	{
		if(self.opener.document.getElementById(IdNomCache+"_width"))
		{
			self.opener.document.getElementById(IdNomCache+"_width").value = width;
		}
		if(self.opener.document.getElementById(IdNomCache+"_height"))
		{
			self.opener.document.getElementById(IdNomCache+"_height").value = height;
		}
		Choix.src = ImgPath;
		Choix.alt = ImgPath;
	}else
	{
		a = ImgPath.split("/");
		Choix.value = a[a.length-1];
	}
	cacheChoix.value = ImgPath;
}

function changeGallery(IdNomImg,IdNomCache,type,dossier)
{
	var newwin = popupscroll("index.php?type=mediatheque&bo_action=list&type_gallery="+type+"&dir_origine="+dossier+"&view=popup&mode=view&returngalhid="+IdNomCache+"&returngalvis="+IdNomImg,500,700);
}

function supprimeImage(NomImg,NomCache)
{
	var ObjImg = document.getElementById(NomImg);
	var ObjHidden = document.getElementById(NomCache);
	if (ObjImg.type == "text")
	{
		ObjImg.value = "";
	}else
	{
		ObjImg.src = images_path+"generiques/pix_transp.gif";
		ObjImg.alt = "Choisissez une image";
	}
	ObjHidden.value = "";
}

function defaultImage(NomImg,NomCache)
{
	var ObjImg = document.getElementById(NomImg);
	var ObjHidden = document.getElementById(NomCache);
	
	var replaceValue = "{" + ObjHidden.name + "}";
	
	if( ObjHidden.value == "" || ObjHidden.value == replaceValue)
	{
    	supprimeImage(NomImg,NomCache)	
	}else
	{
		if (ObjImg.type == "text")
		{
			a = ObjHidden.value.split("/");
			ObjImg.value = a[a.length-1];
		}else
		{
	    	ObjImg.src = ObjHidden.value;
			ObjImg.alt = ObjHidden.value;
		}
	}
}

/*************************************************************************
	FONCTIONS utiles
**************************************************************************/	
function setFormGeneriqueValue(obj,value)
{
	obj.value=value;
}

/*************************************************************************
	FONCTIONS BLOC COLOR
**************************************************************************/	
function setColorFormElement(name)
{	
	eval("var textInput=document.wizard."+name);
	eval("var colorInput=document.wizard.color_"+name);
	var myColor=webColor(textInput.value);
	setFormGeneriqueValue(textInput,myColor);
	colorInput.style.backgroundColor=myColor;	
}

function resetColorFormElement(name)
{	
	eval("var textInput=document.wizard."+name);
	eval("var colorInput=document.wizard.color_"+name);
	setFormGeneriqueValue(textInput,'');
	colorInput.style.backgroundColor='white';	
}

function setDivColor(obj,value)
{
	obj.innerText='<font color="'+value+'">&nbsp;&nbsp;</font>';
}

/*************************************************************************
	FONCTIONS LINK
**************************************************************************/	
function setFreeUrl(name)
{
	eval("var obj=document.wizard." + name);
	obj.value='';
	obj.focus();
	document.execCommand("CreateLink");
}

function setTreeUrl(name,page)
{
	var newwin = popupscroll("index.php?type=publication&bo_action=link_rubrik&restrict=all&view=popup&name="+name,500,700);
}

function setTreeUrlOnlyRub(name,level)
{
	var newwin = popupscroll("index.php?type=publication&bo_action=link_rubrik&restrict=only_rub&view=popup&name="+name+"&level="+level,500,700);
}

function setTreeUrlOnlyArt(name)
{
	var newwin = popupscroll("index.php?type=publication&bo_action=link_rubrik&restrict=only_art&view=popup&name="+name,500,700);
}

function setTreeUrlOnlyPrivate(name)
{
	var newwin = popupscroll("index.php?type=publication&bo_action=link_rubrik&restrict=only_private&view=popup&name="+name,500,700);
}

function removeUrl(name)
{
	eval("var obj=document.wizard."+name);
	setFormGeneriqueValue(obj,"");
}