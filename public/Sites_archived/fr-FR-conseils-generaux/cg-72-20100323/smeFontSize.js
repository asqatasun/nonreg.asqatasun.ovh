/******************************************************************************
* smeFontSize.asp.asp                                                                
*******************************************************************************

*******************************************************************************
*                                                                             *
* Copyright 2000-2006                                                         *
*                                                                             *
******************************************************************************/
function fontSizeChange(strChange)
{
	// on recupere les parametres de l'url
	var strparams = window.location.search;

	// on supprime le '?'
	var qmarkIndex = strparams.indexOf('?');
	if ( qmarkIndex >= 0 )
		strparams = strparams.substr(1);
	
	// si le parametre 'fontSize' existe deja on le remplace
	var bExists = false;
	var params = strparams.split('&');
	var i;
	for (i=0 ; i<params.length ; i++)
	{
		var param = params[i].split('=');
		if (param[0] == "fontSize")
		{
			bExists = true;
			params[i] = "fontSize="+strChange;
		}
	}

	// s'il n'existe pas on l'ajoute
	if (!bExists)
	{
		params[params.length] = "fontSize="+strChange;
	}

	var url = "";
	if ( qmarkIndex >= 0 )
		url = window.location.href.substring(0, qmarkIndex);
	else
		url = window.location.href;

	url += '?' + params.join('&');
	window.location.assign(url);
	
}

function initFontSize()
{
	// on recupere les parametres de l'url
	var strparams = window.location.search;

	// on supprime le '?'
	var qmarkIndex = strparams.indexOf('?');
	if ( qmarkIndex >= 0 )
		strparams = strparams.substr(1);
	
	// on cherche le param 'fontSize'
	var strChange = "";
	var params = strparams.split('&');
	var i;
	for (i=0 ; i<params.length ; i++)
	{
		var param = params[i].split('=');
		if (param[0] == "fontSize")
		{
			strChange = param[1];
			break;
		}
	}

	// on regarde si on a deja un cookie :
	var fontSize = parseFloat(getFontSizeCookie());
	if (strChange == "plus")
		fontSize += 12.5;
	else if (strChange == "minus")
		fontSize -= 12.5;

	// on met a jour le cookie
	var expires = new Date();
	expires.setTime(expires.getTime() + (86400 * 1000 * 30));
	document.cookie = "currentFontSize=" + fontSize + "; expires=" + expires.toGMTString() +  "; path=/";
	// on mets le font-size sur le body
	document.body.style.fontSize = fontSize+"%";
}

addLoadAction(initFontSize);

function getFontSizeCookie()
{
	var offset = document.cookie.indexOf("currentFontSize=");
	if (offset != -1)
	{
		offset += 16;
		var end = document.cookie.indexOf(";", offset);
		if (end == -1)
			end = document.cookie.length;
		return document.cookie.substring(offset, end);
	}
	else
	{
		// retourne la taille par defaut si pas de cookie
		return 62.5;
	}
}
