var oldonload=window.onload;
if(typeof(oldonload)=='function') window.onload=function(){oldonload(); init()};
else window.onload=function(){init()};

function init()
{
	if (leselect=document.getElementById("class_selector"))
	{
		var conteneur=document.getElementById('conteneur');
		leselect.onchange=function() {
			
			conteneur.innerHTML="";
			for(var i=0;i<attributs[this.value].length;i++)
			{
				var temp;
				var identifier=attributs[this.value][i][0];
				var name=attributs[this.value][i][1];
				
				temp="<div class='lig'><input type='checkbox' class='check' name='attr_want[]' value='"+identifier+"' /><label for=''>"+name+"</label><input type='text' name='"+identifier+"' value='"+identifier+"'/><br class='clear' /></div><br class='clear' />";
				conteneur.innerHTML=conteneur.innerHTML+temp;
				
			}
		}
		document.getElementById('formFluxmlEdit').onsubmit=function() {
			if (this.flux_name.value.length<=0) { alert("Veuillez remplir le Nom du flux"); return false;}
			if (this.flux_description.value.length<=0) { alert("Veuillez remplir la Description du flux"); return false;}
			if (this.flux_url_site.value.length<=0) { alert("Veuillez remplir l'url du site"); return false;}
//			if (this.flux_url.value.length<=0) { alert("Veuillez remplir l'url du flux"); return false;}
			if (this.flux_source_node.value.length<=0) { alert("Veuillez saisir l'id du noeud source"); return false;}
			if (this.flux_class.value<=0) { alert("Veuillez renseigner une classe a exporter"); return false;}
			return true;		
		}
	}
}