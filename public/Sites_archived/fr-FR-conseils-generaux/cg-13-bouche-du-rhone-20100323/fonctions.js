jQuery.noConflict();
jQuery(function($) {
	
	MENU_Gestion('vertical');
	
	// Gére l'impression de la page
	$('#imprimer').click(function () {
		window.print();
		return false;
	});
	
	// Gere l'affichage des boutons + -  de elementMontre
/*	
	$('div.bt-more-hide').show();
	$("ul.montre li").hide();	
	$("div.elementMontre")
	.each(function () {
		$('ul.montre li:lt(3)', this).show();	
		var valMini = 3;
		var valActu = 3;
		var valMax = $('li', this).length;	
		
		if(valActu == valMini) {
			var imgSRC = $('span.bt-hide a img', this).attr('src');
			$('span.bt-hide a img', this).attr('src', imgSRC.replace(/bt-hide.pngj/ig,'bt-hide-on.png'));
		}
		
		$(this).find('span.bt-more a').bind('click', function(){
			if(valActu == valMini) {
				var imgSRC = $(this).parents('div.elementMontre').find('span.bt-hide a img').attr('src');
				$(this).parents('div.elementMontre').find('span.bt-hide a img').attr('src', imgSRC.replace(/bt-hide-on.pngj/ig,'bt-hide.png'));
			}
			if(valActu < valMax) {
				//valActu++;
				$(this).parents('div.elementMontre').find('ul.montre li').hide();
				$(this).parents('div.elementMontre').find("ul.montre li:lt("+valMax+")").show();
				//alert(valActu);
			}		
			if(valActu != valMax) {
				var imgSRC = $('img', this).attr('src');
				$('img', this).attr('src', imgSRC.replace(/bt-more.pngj/ig,'bt-more-on.png'));
			}
			return false;
		});
		
		$(this).find('span.bt-hide a').bind('click', function(){
			if(valActu == valMini) {
				var imgSRC = $(this).parents('div.elementMontre').find('span.bt-more a img').attr('src');
				$(this).parents('div.elementMontre').find('span.bt-more a img').attr('src', imgSRC.replace(/bt-more-on.pngj/ig,'bt-more.png'));
			}
			if(valActu < valMax) {
				//valActu++;
				$(this).parents('div.elementMontre').find('ul.montre li').hide();
				$(this).parents('div.elementMontre').find("ul.montre li:lt("+valMini+")").show();
				//alert(valActu);
			}		
			if(valActu == valMini) {
				var imgSRC = $('img', this).attr('src');
				$('img', this).attr('src', imgSRC.replace(/bt-hide.pngj/ig,'bt-hide-on.png'));
			}
			return false;
		});
		
	});		
*/

// Gestion des carousels
    $('.newCarousel').each(function(){
        var animationActive = false;
        var idCarousel = '#'+$(this).attr('id')+' ';

        var elementWidth = $(idCarousel + '.carousel li').width();
        var numberElements = $(idCarousel + '.carousel li').length;
		var c;
		var d;
        $(idCarousel + '.carousel').css('width', (numberElements*2)*elementWidth).wrap(document.createElement('div')).parent().addClass('jcarousel-clip');
        
        // Gestion de defilement arriere
        $(idCarousel + '.carouselPrev a').bind('click', function(){
            if(animationActive != true) {
                animationActive = true;
                
                $(idCarousel + '.carousel li:last').clone().insertBefore(idCarousel + '.carousel li:last');
                $(idCarousel + '.carousel li:last').insertBefore(idCarousel + '.carousel li:first');
                
                var marginEnd = ($(idCarousel + '.carousel').css('marginLeft'));
                var marginBegin = marginEnd.replace(/px/, '');
                marginBegin = marginBegin-elementWidth;
                marginBegin = marginBegin+'px';
                
                $(idCarousel + '.carousel').css('marginLeft', marginBegin);
                $(idCarousel + '.carousel').stop().animate({'marginLeft': marginEnd}, 'slow', function() {
                    $(idCarousel + '.carousel li:last').remove();
                    animationActive = false
                });
            }
			//c = $('.newCarousel ul li:eq(4)').attr('id');
			$('.navCarousel li.hidden').removeClass('hidden');
			$('.navCarousel ul li:last]').clone().insertBefore('.navCarousel ul li:first');
			$('.navCarousel ul li:last]').insertBefore('.navCarousel ul li:first');			
			//$('.navCarousel ul li:last').remove();
			$('.navCarousel ul li:last]').addClass('hidden');
            return false;
        });
        
        // Gestion de defilement avant
        $(idCarousel + '.carouselNext a').bind('click', function(){
            if(animationActive != true) {
                animationActive = true;
                
                $(idCarousel + '.carousel li:first').clone().insertAfter(idCarousel + '.carousel li:first');
				$(idCarousel + '.carousel li:first').insertAfter(idCarousel + '.carousel li:last');
                
                var marginEnd = ($(idCarousel + '.carousel').css('marginLeft'));
                var marginBegin = marginEnd.replace(/px/, '');
                marginBegin = marginBegin-elementWidth;
                marginBegin = marginBegin+'px';
                
                $(idCarousel + '.carousel').stop().animate({'marginLeft': marginBegin}, 'slow', function() {
                    $(idCarousel + '.carousel').css('marginLeft', marginEnd);
                    $(idCarousel + '.carousel li:first').remove();
                    animationActive = false
                });
            }
			
            return false;
        });
		
		
		// Gère l'affichage des item de la navigation du carousel
		$('.navCarousel ul li a').click(function(){
			var idNav = $(this).attr('href');
			if($('.navCarousel ul li').hasClass('hidden')){
				$('.navCarousel ul li').removeClass('hidden');
			}
			$(this).parent().addClass('hidden');
			$(this).parent().insertAfter('.navCarousel ul li:last');
			$(idCarousel + ".carousel " + idNav).insertBefore(idCarousel + '.carousel li:first');
			
			return false;
		});	
		
        
        // Autoslide
        if ($(this).is('.autoSlide')){
			var a=0;
			var b;
			var c;
            var interval = setInterval(function() {
                $(idCarousel + '.carouselNext a').click();
				b = $('.newCarousel ul li:eq(1)').attr('id');
				$('.navCarousel li').removeClass('hidden');
				$('.navCarousel ul li a[href$="'+b+'"]').parent().addClass('hidden').insertAfter('.navCarousel ul li:last');
            }, 5000);
			$(idCarousel + '.carousel').bind('mouseover', function(){ clearInterval(interval); });
			$('.navCarousel').bind('mouseover', function(){ clearInterval(interval); });		  
        };		
	});

	function navCarouselUpdate(){
		b = $('.newCarousel ul li:eq(0)').attr('id');
		$('.navCarousel li').removeClass('hidden');
		$('.navCarousel ul li a[href$="'+b+'"]').parent().addClass('hidden').insertAfter('.navCarousel ul li:last');
	}
		
	
	
	// Gère l'affichage des rubriques - Page d'accueil showHide
	
	$('.bloc-article .actu h3 a').bind('click', function() {
		$(this).addClass('ici');
		$('.bloc-article .show').removeClass('show');
		$(this).parents('.actu').addClass('show');
		$('.actu p').slideUp('fast');
		$('.bloc-article .show p').slideDown('fast');
		return false;
	});
	
	$('.bloc-article .agenda h3 a').bind('click', function() {
    		$('.bloc-article .show').removeClass('show');
    		$(this).parents('.agenda ').addClass('show');
    		$('.agenda p').slideUp('fast');
    		$('.bloc-article .show p').slideDown('fast');
    		return false;
	});
	
// Gestion visage 2013

	 if ($("#CG13Visages").length) {
   var flashparams = {
    bgcolor: "#ffffff"
   };
   var flashvars = {
    xmlPath: "/fileadmin/templates/flash/culture-visages/assets/visages.xml",
    nbLines: "3",
    functionJS: "selectCategory2013"
   };
   swfobject.embedSWF("/fileadmin/templates/flash/culture-visages/CG13Visages.swf", "CG13Visages", "536", "330", "9.0.0", "/fileadmin/templates/flash/culture-visages/expressInstall.swf", flashvars, flashparams);
  }
  
  if ($("#CG13VisagesMenu").length) {
   var flashparams = {
    bgcolor: "#ffffff"
   };
   var flashvars = {
    xmlPath: "/fileadmin/templates/flash/culture-visages/assets/visages.xml",
    nbLines: "1",
    functionJS: "selectCategory2013"
   };
   swfobject.embedSWF("/fileadmin/templates/flash/culture-visages/CG13Visages.swf", "CG13VisagesMenu", "536", "110", "9.0.0", "/fileadmin/templates/flash/culture-visages/expressInstall.swf", flashvars, flashparams);
  }

});


function selectCategory2013(cssId) {
$('#menuMarseille2013 li.hovered').removeClass('hovered');
if(cssId!=''){  
 $('#'+cssId).addClass('hovered');
}
}