/**
 * Gestion du menu déroulant
 *
 * @author      Eric Giovannetti <eric@bleuroy.com>
 * @copyright   BleuRoy.com
 * @version     1.0.4
 */
 
/**
 * Gère l'affichage dynamique du menu
 * @param {string}    _typeDeMenu          Valeurs : horizontal, vertical
 * @var {object}      menu                 Récupère l'élément possèdant l'id "menu"
 * @var {array}       niveau               Récupère les listes <ul>
 * @var {string}      classElement         Récupère la classe des listes <ul> ayant une classe possèdant la chaine de caractère  'menuNiveau'
 * @var {array}       liNiveau             Récupère les éléments de liste <li> ayant comme parent
 * @var {object}      ancre                Contient un élément lien <a>
 * @var {number}      menuTimer            Contient le timer de gestion d'affichage du menu
 */
 
 
 
function MENU_Gestion(_typeDeMenu) {
    // Vérifie si le navigateur est compatible avec le DOM et s'il possède un élément ayant l'id 'menu'
    if(!document.getElementById || !document.createTextNode) {return;}
    if(!document.getElementById('menu')) {return;}
 
    var menu = document.getElementById('menu');
    var niveau = menu.getElementsByTagName('UL');
    var classElement;
    var liNiveau;
    var ancre;
    var menuTimer = null;
 
    if (document.getElementById('conteneur') && _typeDeMenu == 'horizontal') {
        document.getElementById('conteneur').id = 'conteneurJS'
    }
 
    // Boucle et exécute les listes <ul> possédant une classe du type  'menuNiveauX'  où X est le nombre correspondant au niveau de la profondeur du menu
    for (var i=0; i<niveau.length; i++) {
        if (niveau[i].className.indexOf('menuNiveau') >= 0) {
            // Transforme les   <ul class="menuNiveauX">   en   <ul class="menuNiveau menuNiveauXJS"> où X est un chiffre
            classElement = niveau[i].className;
            niveau[i].className = niveau[i].className + ' ' + niveau[i].className+'JS';
            if ((i+1) == niveau.length) {
                niveau[i].id = 'lastNiveau2JS';
            }
 
            liNiveau = niveau[i].getElementsByTagName('LI');
 
            // Boucle et exécute les éléments de liste <li> ne possédant pas de noeud enfant lien <a> mais ayant un noeud frère liste <ul>
            for (var j=0; j<liNiveau.length; j++){
                if (liNiveau[j].parentNode.className == niveau[i].className && liNiveau[j].firstChild.tagName != 'A' && liNiveau[j].firstChild.nextSibling) {
                    // Encapsule d'un lien <a href="#" id="ancreMenuX"> les données des éléments de liste ayant un sous-niveau
                    ancre  = document.createElement('A');
                    ancre.href = '#';
                    ancre.id= 'ancreMenu'+j;
                    ancre.appendChild(document.createTextNode(liNiveau[j].firstChild.data));
                    liNiveau[j].replaceChild(ancre, liNiveau[j].firstChild);
 
                    // Gestion d'évènements
                    // Menu horizontal de type  'Survol'
                    if(_typeDeMenu == 'horizontal') {
                        ancre.onmouseover = ancre.onfocus = function(event) {
                            clearInterval(menuTimer);
                            menuTimer = null;
                            MENU_affichage('horizontal', this, event);
                        };
 
                        sousNiveau = ancre.nextSibling.getElementsByTagName('A');
 
                        ancre.nextSibling.onmouseover = sousNiveau[0].onfocus = function() {
                            clearInterval(menuTimer);
                            menuTimer = null;
                        };
                        if(ancre.nextSibling.lastChild.firstChild != null && ancre.nextSibling.lastChild.firstChild.tagName == 'A') {
                            lastItem = ancre.nextSibling.lastChild.firstChild;
                        } else {
                            lastItem = ancre.nextSibling.lastChild.previousSibling.firstChild;
                        }
                        ancre.onmouseout = ancre.onblur = ancre.nextSibling.onmouseout = lastItem.onblur = function() {
                            if(menuTimer == null) {
                               menuTimer = setInterval('MENU_affichage(\'horizontal\', null)',200);
                            }
                       };
                    }
                    // Menu vertical de type  'Clic'
                    else if(_typeDeMenu == 'vertical') {
                        ancre.onclick = function(event) {
                            MENU_affichage('vertical', this, event);
                            return false;
                        };
                    }
 
                }
            }
        }
    }
}
 
 
/**
 * Gère l'affichage et masquage du menu
 * @param {string}    _typeDeMenu         Valeurs : horizontal, vertical
 * @param {object}    [_ancre]            Ancre qui subit l'action
 * @var {object}      menu                Récupère l'élément possèdant l'id "menu"
 * @var {array}       sousNiveau          Récupère les sous-listes <ul>
 * @var {boolean}     actif               Vérifie si le menu est actif
 */
function MENU_affichage(_typeDeMenu, _ancre) {
 
    var actif = false;
    var menu = document.getElementById('menu');
    var sousNiveau;
 
    if(_typeDeMenu == 'horizontal')
        var sousNiveau = menu.getElementsByTagName('UL');
    else if(_typeDeMenu == 'vertical')
        var sousNiveau = _ancre.parentNode.parentNode.getElementsByTagName('UL');
 
    if(_typeDeMenu == 'vertical' && _ancre.nextSibling.className.indexOf('JSactif') >= 0)
        actif = true;
 
    // Désactive tous les sous-menus et éléments parents actifs
    for (var i=0; i<sousNiveau.length; i++) {
        if (sousNiveau[i].className.indexOf('JSactif') >= 0) {
            sousNiveau[i].className = sousNiveau[i].className.replace('JSactif',"JS");
            sousNiveau[i].parentNode.className = '';
        }
    }
 
    if(_typeDeMenu == 'horizontal' && _ancre) {
        _ancre.nextSibling.className = _ancre.nextSibling.className.replace('JS',"JSactif");
        _ancre.parentNode.className = 'parentActif';
    }
 
    if(_typeDeMenu == 'vertical' && actif == false) {
        _ancre.nextSibling.className = _ancre.nextSibling.className.replace('JS',"JSactif");
        _ancre.parentNode.className = 'parentActif';
    }
 }