jQuery(document).ready(function(){
    jQuery("#form_newsletter_inscription").submit(function() {
        var civ = jQuery("#form_newsletter_inscription .blocRadio input:checked").value;     
        pageTracker._setVar(civ);            
    });

    jQuery("#form_presse_inscription").submit(function() {          
        pageTracker._trackEvent('Newsletter', 'Inscription Presse Hebdo');
    });
    //rubrique accent
    if(jQuery('#c2114').length){
        jQuery('.download').click(function () { 
            pageTracker._trackEvent('Magazine Accent', 'Telechargement PDF', jQuery(this).attr('href'));
        }); 
        jQuery('.lien-accent').click(function () { 
            var chaine = jQuery(this).attr('href')
            var reg=new RegExp('["]+', 'g');
            var tableau=chaine.split(reg);
            pageTracker._trackEvent('Magazine Accent', 'Lecture en ligne', tableau[1] );
        });
    }
    //Rubrique Robinson :
    if(jQuery('#c2132').length){
        jQuery('.download').click(function () { 
            pageTracker._trackEvent('Magazine Robinson', 'Telechargement PDF', jQuery(this).attr('href'));
        });         
    }
    //Rubrique Guides :
    if(jQuery('#c2150').length){
        jQuery('.download').click(function () { 
            pageTracker._trackEvent('Guide en ligne', 'Telechargement PDF', jQuery(this).attr('href'));
        });         
    }
    //Rubrique Newsletter :
    if(jQuery('#c2204').length){
        jQuery('.download').click(function () { 
            pageTracker._trackEvent('Newsletter', 'Telechargement PDF', jQuery(this).attr('href'));
        });         
    }
     //Rubrique Associa'Treize :
    if(jQuery('#c2182').length){
        jQuery('.download').click(function () { 
            pageTracker._trackEvent('Associa Treize', 'Telechargement PDF', jQuery(this).attr('href'));
        });         
    }
    //form contact    
    jQuery(".tx-powermail-pi1_formwrap_2994 form").submit(function() {
       pageTracker._trackEvent('Contact', 'Demande de contact');  
    });
    //form envoi ami    
    jQuery(".formFriend form").submit(function() {
       pageTracker._trackEvent('Envoi à un ami', 'Envoi à un ami');
    });
    //profil
    jQuery('.profil option').click(function () { 
        pageTracker._setVar(jQuery(this).text());
    });  
});

/*Formulaire inscription à la newsletter :
- sur le bouton de validation du formulaire d'inscription à la newsletter : pageTracker._trackEvent('Newsletter', 'Inscription Newsletter'); ->TS

- dans le formulaire d'inscription à la newsletter :
    <form onSubmit="pageTracker._setVar(this.mymenu.options[this.mymenu.selectedIndex].value);">
    <select name=mymenu>
    <option value="Monsieur">Homme</option>
    <option value="Madame">Femme<option>
    <option value="Mademoiselle">Femme<option>

- sur la page de confirmation d'inscription à la newsletter : <script type="text/javascript">pageTracker._setVar('Inscrit Newsletter');</script>


Formulaire inscription à l'espace presse :
- sur le bouton de validation du formulaire d'inscription à la presse : pageTracker._trackEvent('Newsletter', 'Inscription Presse Hebdo');

- sur la page de confirmation d'inscription à la presse : <script type="text/javascript">pageTracker._setVar('Espace Presse');</script>


Rubrique Accent :
- sur le bouton "Téléchargez" : pageTracker._trackEvent('Magazine Accent', 'Telechargement PDF', 'Nom du fichier');
- sur le bouton "Lire en ligne" : pageTracker._trackEvent('Magazine Accent', 'Lecture en ligne', 'Nom du fichier');

Rubrique Robinson :
- sur le bouton "Téléchargez" : pageTracker._trackEvent('Magazine Robinson', 'Telechargement PDF', 'Nom du fichier');

Rubrique Guides :
- sur le bouton "Téléchargez" : pageTracker._trackEvent('Guide en ligne', 'Telechargement PDF', 'Nom du fichier');

Rubrique Newsletter :
- sur le bouton "Téléchargez" : pageTracker._trackEvent('Newsletter', 'Telechargement PDF', 'Nom du fichier');

Rubrique Associa'Treize :
- sur le bouton "Téléchargez" : pageTracker._trackEvent('Associa Treize', 'Telechargement PDF', 'Nom du fichier');

Page Contact :
- sur le bouton d'envoi de formulaire de contact : pageTracker._trackEvent('Contact', 'Demande de contact');

Page Envoi à un ami :
- sur le bouton de validation de validation du formulaire d'envoi à un ami : pageTracker._trackEvent('Envoi à un ami', 'Envoi à un ami');

Pages par profil :
- sur les différentes pages de choix de profil :
    <script type="text/javascript">pageTracker._setVar('Chef d'entreprise');</script>
    <script type="text/javascript">pageTracker._setVar('Parents');</script>
    <script type="text/javascript">pageTracker._setVar('Collégien');</script>
    <script type="text/javascript">pageTracker._setVar('Bénéficiaire du RMI');</script>
    <script type="text/javascript">pageTracker._setVar('Personne Handicapées');</script>
    <script type="text/javascript">pageTracker._setVar('Sportif');</script>
    <script type="text/javascript">pageTracker._setVar('Jeune');</script>
    <script type="text/javascript">pageTracker._setVar('Senior');</script>
    <script type="text/javascript">pageTracker._setVar('Journaliste');</script>
*/