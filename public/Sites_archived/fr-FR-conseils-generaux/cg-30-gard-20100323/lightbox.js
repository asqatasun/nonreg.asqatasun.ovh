$(document).ready(function(){
	
	$("a.lightbox_link").click(function(){
		$("#fond_lightbox").show();
		$("#lightbox").hide();
		document.getElementById('lightbox').style.visibility="visible";
		$("#lightbox").show('slow');
		// on est oblig� d'utiliser les visiblity car si la google map se trouve en un div en display none, la carte ne se charge pas 
	});
	
	var cpt = 0;
	$("a.lightbox_link_lieu").each(function(){
		cpt = 1;
	});
	if(cpt > 0){
		$("#fond_lightbox").show();
		$("#lightbox").hide();
		document.getElementById('lightbox').style.visibility="visible";
		$("#lightbox").show('slow');
		// on est oblig� d'utiliser les visiblity car si la google map se trouve en un div en display none, la carte ne se charge pas 
	}
 
	$("#lightbox span.valid").click(function () {
		$("#fond_lightbox").hide();
		$("#lightbox").hide('slow');
		document.getElementById('lightbox').style.visibility="hidden";
	});
	
	
	// onglet photo / vid�o en page d'accueil
	$("#phototheque ul li.link_video a").click(function () {
		$("#phototheque div.photo").hide();
		$("#phototheque div.video").show();
	});
	$("#phototheque ul li.link_photo a").click(function () {
		$("#phototheque div.video").hide();
		$("#phototheque div.photo").show();
	});
	
	
	// afficher la video
	var recherche= /.*.html*/;
	var recherche2= /.*accueil2.html*/;
	if(document.getElementById('fond_lightbox_video') 
		&& (!recherche.test(location.href)
			|| recherche2.test(location.href)
			)
		){
		$("#fond_lightbox").show();
		$("#fond_lightbox_video").show('slow');
	}else{
		if(!document.getElementById('open_lightbox_video'))
			$("#fond_lightbox_video").remove();
	}
	
	$("#open_lightbox_video").click(function() {
		if(document.getElementById('fond_lightbox_video')){
			$("#fond_lightbox").show();
			$("#fond_lightbox_video").show('slow');
		}
		return false;
	});
	
	if(document.getElementById('google_map_inforoute')){
		makeMap();
	}
	
	$("#fond_lightbox_video span.valid").click(function () {
		$("#fond_lightbox").hide();
		$("#fond_lightbox_video").hide('slow');
		/*$("#fond_lightbox_video").remove();*/
	});
	$("div.autrePublication h4").click(function () {
		$("div.autrePublication .contentPublication").hide();
		$(this).parent().children(".contentPublication").show("slow");
	});
});