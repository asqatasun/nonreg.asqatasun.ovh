$(document).ready(function(){
	$("div.ttnews_galerie div.news-latest-img img").hide();
	$("div.ttnews_galerie div.news-single-img a").hide();
	
	$("div.ttnews_galerie div.news-latest-img img:first").show();
	$("div.ttnews_galerie div.news-single-img a:first").show();
	
	$("div.ttnews_galerie div.news-single-img-miniature div.contenu_defil a").click(function(){
		var value = this.href;
		$("div.ttnews_galerie div.news-latest-img img").hide();
		$("div.ttnews_galerie div.news-single-img a").hide();
		$("div.ttnews_galerie div.news-latest-img img").each(function(){
			if(this.src == value){
				$(this).show();
			}
		});
		$("div.ttnews_galerie div.news-single-img img").each(function(){
			if(this.src == value){
				$(this).parent().show();
			}
		});
		return false;
	});
	
	
	
	$("div.ttnews_galerie span.droit").mouseover(function(){
		$(this).everyTime(100, "adroite", function() {
			var left = $("div.ttnews_galerie div.news-single-img-miniature div.contenu_defil").css("left");
			left = parseInt(left);
			left -= 2;
			// calcul du nombre d'images 
			var nb = $("div.ttnews_galerie div.news-single-img-miniature div.contenu_defil a img").length;
			nb = parseInt(nb);
			var max = (nb*50)+(nb*4);
			//taille de la barre de défilement 315px environ
			var taille = $("div.ttnews_galerie div.news-single-img-miniature").css("width");
			taille = parseInt(taille);
			taille -= 10;
			max = taille - max;
			if(left > max)
				$("div.ttnews_galerie div.news-single-img-miniature div.contenu_defil").css("left", left+"px");
		});
	});
	$("div.ttnews_galerie span.droit").mouseout(function(){
		$("div.ttnews_galerie span.droit").stopTime("adroite");
	});
	
	$("div.ttnews_galerie span.gauche").mouseover(function(){
		$(this).everyTime(100, "agauche", function() {
			var left = $("div.ttnews_galerie div.contenu_defil").css("left");
			left = parseInt(left);
			left += 2;
			if(left < 0)
				$("div.ttnews_galerie div.news-single-img-miniature div.contenu_defil").css("left", left+"px");
		});
	});
	$("div.ttnews_galerie span.gauche").mouseout(function(){
		$("div.ttnews_galerie span.gauche").stopTime("agauche");
	});
	
});