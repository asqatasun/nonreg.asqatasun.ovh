var TUSFOTOS = {};

TUSFOTOS.fotoGaleria = function(fichInclude, edicion) {
	
	categoriaAux = fichInclude.replace('c-', '');
	categoria = fichInclude.replace('c-', '');
	datos = categoria.split("-");
	var categoria = '';
	for(i=0; i<datos.length; i++) {
		if(isNaN(datos[i]) || datos[i] > 10) {
			if(categoria != '') 
				categoria = categoria + "-" + datos[i];
			else
				categoria = datos[i];
		}
	}
	if(categoria == '') {
		categoria = categoriaAux.substr(1);
	}
	if(edicion != null && edicion != '') {
		$("#rd-TUSFOTOS-" + categoria).load("/includes_ee/participacion/" + edicion + "/tus-fotos/" + fichInclude + ".html");
	} else {
		$("#rd-TUSFOTOS-" + categoria).load("/includes_ee/participacion/tus-fotos/" + fichInclude + ".html");
	}
}

TUSFOTOS.videoGaleria = function(fichInclude, edicion) {
	
	categoriaAux = fichInclude.replace('c-tus-videos', '');
	categoria = fichInclude.replace('c-tus-videos', '');
	datos = categoria.split("-");
	categoria = '';
	for(i=0; i<datos.length; i++) {
		if(isNaN(datos[i]) || datos[i] > 10) {
			if(categoria != '') 
				categoria = categoria + "-" + datos[i];
			else
				categoria = datos[i];
		}
	}
	if(categoria == '') {
		categoria = categoriaAux.substr(1);
	}
	if(edicion != null && edicion != '') {
		$("#rd-TUSVIDEOS-" + categoria).load("/includes_ee/participacion/" + edicion + "/videos/" + fichInclude + ".html");
	} else {
		$("#rd-TUSVIDEOS-" + categoria).load("/includes_ee/participacion/videos/" + fichInclude + ".html");
	}
}