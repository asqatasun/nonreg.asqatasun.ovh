var DTS = {};
DTS.cambiarModulo = function (modulo, actual, nuevo) {
	var aNavegacion = modulo.split('.');
	var navegacion = $('#'+aNavegacion[0]+'\\.'+aNavegacion[1]+'\\.'+aNavegacion[2]+'-1').parent().siblings('.modnav');
	
	$('#' + aNavegacion[0]+'\\.'+aNavegacion[1]+'\\.'+aNavegacion[2] + '-' + nuevo).find('img[rel*="/"]').attr('src', $('#' + aNavegacion[0]+'\\.'+aNavegacion[1]+'\\.'+aNavegacion[2] + '-' + nuevo).find('img[rel*="/"]').attr('rel'));
	
	$('#' + aNavegacion[0]+'\\.'+aNavegacion[1]+'\\.'+aNavegacion[2] + '-' + actual).hide();
	$('#' + aNavegacion[0]+'\\.'+aNavegacion[1]+'\\.'+aNavegacion[2] + '-' + nuevo).show();
	
	$(navegacion).find('a.ng_numero:eq(' + (actual - 1) + ')').removeClass('activo');
	$(navegacion).find('a.ng_numero:eq(' + (nuevo - 1) + ')').addClass('activo');
	
};

DTS.paginacionModulos = function (modulo) {

		var aNavegacion = modulo.split('.');
		var navegacion = $('#'+aNavegacion[0]+'\\.'+aNavegacion[1]+'\\.'+aNavegacion[2]+'-1').parent().siblings('.modnav');
		var num_fotonoticias = $(navegacion).find('a.ng_numero ').length;
	
		if(num_fotonoticias == '') {
			num_fotonoticias = 1;
		}
		
		
		var actual = 1;
	
		if(num_fotonoticias == 1) {
			actual = 1;
		}
	
		if($('#' + aNavegacion[0]+'\\.'+aNavegacion[1]+'\\.'+aNavegacion[2] + '-' + actual).find('img[rel*="/"]').attr('rel')) {
			$('#' + aNavegacion[0]+'\\.'+aNavegacion[1]+'\\.'+aNavegacion[2] + '-' + actual).find('img[rel*="/"]').attr('src', $('#' + aNavegacion[0]+'\\.'+aNavegacion[1]+'\\.'+aNavegacion[2] + '-' + actual).find('img[rel*="/"]').attr('rel'));
		} else {
			if(actual == 1 && $('#' + aNavegacion[0]+'\\.'+aNavegacion[1]+'\\.'+aNavegacion[2] + '-' + actual).find('img[rel*="/"]').css('display') == 'none') {
				$('#' + aNavegacion[0]+'\\.'+aNavegacion[1]+'\\.'+aNavegacion[2] + '-' + actual).find('img[rel*="/"]').css('display', 'block');
			}
		}
	
	
		$(navegacion).find('a.ng_numero:eq(' + (actual - 1) + ')').addClass('activo');
		
	
		// Ocultamos todas menos la actual
		
		$('[id^=' +aNavegacion[0]+'\.'+aNavegacion[1]+'\.'+aNavegacion[2] + '-]').each(function (i) {
				if (i != (actual - 1)) {
					$(this).hide();
				} 
			}
		);
		
		// Numeros
		
		$(navegacion).find('a.ng_numero').each(function() {
				$(this).attr('href', 'javascript:void(0)');
				
				var posicion = $(this).text();
				
				$(this).click(function() {
	
						DTS.cambiarModulo(modulo, actual, posicion);
						
						actual = posicion;
					}
				);
			}
		);
		
		
		// Siguiente
		
		$(navegacion).find('.ngas_sig').attr('href', 'javascript:void(0)');
		$(navegacion).find('.ngas_sig').click(function(){
				
				var siguiente = ((actual + 1) > num_fotonoticias) ? 1 : (actual + 1);
					
				DTS.cambiarModulo(modulo, actual, siguiente);
				
				actual = siguiente;
			}
		);
		
		// Anterior
		
		$(navegacion).find('.ngas_ant').attr('href', 'javascript:void(0)');
		$(navegacion).find('.ngas_ant').click(function(){
				
				var anterior = ((actual - 1) < 1) ? num_fotonoticias : (actual - 1);
				
				DTS.cambiarModulo(modulo, actual, anterior);
				
				actual = anterior;
			}
		);

};