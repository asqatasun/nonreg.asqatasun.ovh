// manejo de elementos de la maquetaci�n
var MQT = {};
MQT.fotoArticuloActual = '';
MQT.variableGlobalServ = 0;
MQT.variableGlobalCamb = 0;
MQT.variableGlobalPers = 0;
MQT.variableGlobalMeteo = 0

MQT.modulosLomas = function () {
	$("div.mod_lomas > div.bloque_lomas > div.blm_header").click( function() {
				var $origen = $(this);
				$origen.find("a.boton_despliega").attr({ href: "javascript:void(0)"});
				// movimiento del contenido
				$("div.blm_content").slideUp("slow");
				$origen.parent().find("div.blm_content").slideDown("slow");
			
				// colores activos/no activos encabezado
				$("div.blm_header").removeClass("blm_header_activa");
				$origen.addClass("blm_header_activa");
			
				// colores activos/no activos encabezado
				$("a.boton_despliega").removeClass("btn_deson");
				$origen.find("a.boton_despliega").addClass("btn_deson");
	});
};

MQT.activa_lomas = function(idparaactivar) {
	// opciones idparaactivar: masleido, mascomentado, masultimos
	$("#" + idparaactivar).find("div.blm_content").slideDown("slow");
	$("#" + idparaactivar).find("div.blm_header").addClass("blm_header_activa");
	$("#" + idparaactivar).find("a.boton_despliega").addClass("btn_deson");
};

MQT.abrirFormularioEnviar = function() {
	$(".ah_enviar").click( function() {
		$(".frm_art_rectifica").slideUp("slow");
		$("#form-articulo-top").find(".ah_enviar").attr({ href: "javascript:void(0)"});
		$(".art_herramientas a").removeClass("activo");
		$(this).addClass("activo");
		$(".frm_art_envia").slideDown("slow");
	});
};

MQT.cerrarFormularioEnviar = function() {
	$(".frm_art_envia .cerrar").click( function() {
		$(this).attr({ href: "javascript:void(0)"});
		$(".art_herramientas a").removeClass("activo");
		$(".frm_art_envia").slideUp("slow");
	});
};

MQT.abrirFormularioRectificar = function() {
	$(".ah_rectificar").click( function() {
		$(".frm_art_envia").slideUp("slow");
		$("#form-articulo-top").find(".ah_rectificar").attr({ href: "javascript:void(0)"});
		$(".art_herramientas a").removeClass("activo");
		$(this).addClass("activo");
		$(".frm_art_rectifica").slideDown("slow");
	});
};

MQT.cerrarFormularioRectificar = function() {
	$(".frm_art_rectifica .cerrar").click( function() {
		$(this).attr({ href: "javascript:void(0)"});
		$(".frm_art_envia").slideUp("slow");
		$(".art_herramientas a").removeClass("activo");
		$(".frm_art_rectifica").slideUp("slow");
	});
};

MQT.moduloscabecera1 = function() {

$HTMLcaja = '<span class="cj"><span class="cjtl"><span class="cjtr"><span class="cjt"></span></span></span><span class="cjl"><span class="cjr"><span class="cjc"><span class="clear"></span></span></span></span><span class="cjbl"><span class="cjbr"><span class="cjb"></span></span></span><span class="clear"></span></span>';
	// menu de servicios
	$("#mi-servicios").mouseover( function() {
			if(MQT.variableGlobalServ == 0) {
				MQT.variableGlobalServ = 1;
				$("#mi-servicios").prepend($HTMLcaja);
				$("#mi-servicios .cjc").prepend($("div.menunav ul.submn_servicios"));
				MQT.moduloscabecera2();
			}
			$("#mi-servicios .mn_item_a").addClass("activo");							   
			$("#mi-servicios .cj").show();
			$("div.menunav ul.submn_servicios").show();
	});
	$("#mi-servicios").mouseout( function() {
			$("#mi-servicios .mn_item_a").removeClass("activo");
			$("#mi-servicios .cj").hide();
			$("#mi-servicios .submn_servicios").hide();
	});
	$("#mi-servicios .cj").mouseover( function() {
			$("#mi-servicios .mn_item_a").addClass("activo");
			$("#mi-servicios .cj").show();
			$("div.menunav ul.submn_servicios").show();
	});

	// cambios
	$("div.cef_cambiar > a.cef_cambiar_seleccion").click( function() {
		if(MQT.variableGlobalCamb == 0) {
			MQT.variableGlobalCamb = 1;
			$("span.cef_cambio_lst").prepend($HTMLcaja);
			$("span.cef_cambio_lst  span.cjc").prepend($("span.cef_cambio_lst > .lstcab"));
		}										
		
		$("div.cef_cambiar > a.cef_cambiar_seleccion").addClass("cef_perso_activa");
		$("span.cef_cambio_lst  span.cj").show();
		$("span.cef_cambio_lst  ul.lstcab").show();
	});
	$("span.cef_cambio_lst").mouseout( function() {	
		$("div.cef_cambiar a.cef_cambiar_seleccion").removeClass("cef_perso_activa");
		$("span.cef_cambio_lst span.cj").hide();
		$("span.cef_cambio_lst ul.lstcab").hide();
	});
	$("span.cef_cambio_lst").mouseover( function() {
		$("div.cef_cambiar a.cef_cambiar_seleccion").addClass("cef_perso_activa");
		$("span.cef_cambio_lst span.cj").show();
		$("span.cef_cambio_lst ul.lstcab").show();
	});
	// perso
	$("a.cef_perso_seleccion").click( function() {
		if(MQT.variableGlobalPers == 0) {
			MQT.variableGlobalPers = 1;
			$("span.cef_perso_lst").prepend($HTMLcaja);
			$("span.cef_perso_lst span.cjc").prepend($("span.cef_perso_lst > .lstcab"));
		}	
											  
		$("a.cef_perso_seleccion").addClass("cef_perso_activa");		  
		$("span.cef_perso_lst span.cj").show();
		$("span.cef_perso_lst ul.lstcab").show();
	});
	$("span.cef_perso_lst").mouseout( function() {	
		$("a.cef_perso_seleccion").removeClass("cef_perso_activa");
		$("span.cef_perso_lst span.cj").hide();
		$("span.cef_perso_lst ul.lstcab").hide();
	});
	$("span.cef_perso_lst").mouseover( function() {
		$("a.cef_perso_seleccion").addClass("cef_perso_activa");
		$("span.cef_perso_lst span.cj").show();
		$("span.cef_perso_lst ul.lstcab").show();
	});
};


MQT.moduloscabecera2 = function () {
	/*pagina de inicio*/
	var userAgent = navigator.userAgent.toLowerCase();
	jQuery.browser = {
	version: (userAgent.match( /.+(?:rv|it|ra|ie|me)[\/: ]([\d.]+)/ ) || [])[1],
	chrome: /chrome/.test( userAgent ),
	safari: /webkit/.test( userAgent ) && !/chrome/.test( userAgent ),
	opera: /opera/.test( userAgent ),
	msie: /msie/.test( userAgent ) && !/opera/.test( userAgent ),
	mozilla: /mozilla/.test( userAgent ) && !/(compatible|webkit)/.test( userAgent )
	};
	var urlieactual = window.document.location.href.split('/');
	urlie = urlieactual[2];
	urliefinal = "http://" + urlie;
	$HTMLinicio = '<li id="paginainicio" class="li"><a href="#" class="a">P�gina de inicio</a></li>';
	$("ul.submn_servicios").append($HTMLinicio);
	//A. safari
	if( $.browser.safari ) $("#paginainicio a").attr({ href: "/interactivo/comun/pagina_inicio_safari.html"});
	//B. Explorer
	if ($.browser.msie )$("#paginainicio a").attr({ href: "javascript:void();"});$("#paginainicio a").click( function() {
		this.style.behavior='url(#default#homepage)';this.setHomePage(urliefinal);
	});
	//C. Firefox
	if ($.browser.mozilla && $.browser.version >= "1.8" ) $("#paginainicio a").attr({ href: "/interactivo/comun/pagina_inicio_firefox.html"});
	//D. Chrome
	if ($.browser.chrome ) $("#paginainicio a").attr({ href: "/interactivo/comun/pagina_inicio_chrome.html"});
	//D. Opera
	if ($.browser.opera ) $("#paginainicio a").attr({ href: "/interactivo/comun/pagina_inicio_opera.html"});
	};
	
	MQT.moduloscabecera5 = function () {
	//el tiempo
	$("a.mcmet_mas").click( function() {
									
		if(MQT.variableGlobalMeteo == 0) {
			MQT.variableGlobalMeteo = 1;
			$("form.mcmet_localidades").before($HTMLcaja);
			$("div.modcab_meteo span.cjc").prepend($("form.mcmet_localidades"));
		}	
		$(this).attr({ href: "javascript:void(0)"});
		if($(this).hasClass("mcmet_mas_pulsado")) {
			$(this).removeClass("mcmet_mas_activo");
			$(this).removeClass("mcmet_mas_pulsado");
			$(this).addClass("mcmet_mas_no_pulsado");
			$("div.modcab_meteo span.cj").hide();
			$("form.mcmet_localidades").hide();
		} else {
			$(this).removeClass("mcmet_mas_no_pulsado");
			$(this).removeClass("mcmet_mas_activo");
			$(this).addClass("mcmet_mas_pulsado");
			$("div.modcab_meteo span.cj").show();
			$("form.mcmet_localidades").show();
		}
	});
	
	$("div.modcab_meteo > .cj").mouseover( function() {
		$(this).attr({ href: "javascript:void(0)"});
		$("a.mcmet_mas").addClass("mcmet_mas_activo");
		$("div.modcab_meteo span.cj").show();
		$("form.mcmet_localidades").show();
	});
	$("div.modcab_meteo span.cj select").mouseover( function() {
		$(this).attr({ href: "javascript:void(0)"});
		$("a.mcmet_mas").addClass("mcmet_mas_activo");
		$("div.modcab_meteo span.cj").show();
		$("form.mcmet_localidades").show();
	});
	$("div.modcab_meteo span.cj input").mouseover( function() {
		$(this).attr({ href: "javascript:void(0)"});
		$("a.mcmet_mas").addClass("mcmet_mas_activo");
		$("div.modcab_meteo span.cj").show();
		$("form.mcmet_localidades").show();
	});
	
};

MQT.moduloCabeceraVideochat = function() {
		$("div.modcab_videochat").hide();
		var indice = Math.floor(Math.random() * $("div.modcab_videochat").length);
		$("div.modcab_videochat:eq(" + indice + ")").show();	
};

//ARTICULOS LEAD
MQT.articulosLead = function () {
	// apoyos sobre foto
	$("div.lead .link-app2").each( function() {
		var $origen = $(this);
		if(!$origen.parent().parent().find("div.photo").hasClass("bloquerelativo")) {
			$origen.parent().parent().find("div.photo").addClass("bloquerelativo");
		}
		if($origen.parent().find("div.photo").html() != null && $origen.parent().find("div.photo").html().indexOf($origen.html()) == -1) {
			$origen.parent().find("div.photo").append($origen);
		}
		}); 
	$("div.lead .link-app2 .related-link").append('<span class="rltrans"></span>');
	// apoyos especiales con foto para  bomba AB
	$("div.bomba-AB div.photo:last").after($(".link-app3"));
};

//PARTICIPACION
MQT.zonaparticipacion = function () {
	$("div.todoparticipacion").append('<div class="tp_sombra"></div>');
//	$("div.modPARTheader").wrapInner('<span class="modph_izq"><span class="modph_der"></span></span>');
	$(".zp_ico_nuevo").each( function() {
		$(this).parent("span.modph_tit").addClass("zp_icon");
	}); 
};

MQT.clscode = '<div class="clear"></div>';
MQT.separacode = '<div class="clear"></div>';
MQT.cls = function () {
	// Ajuste de bloques
	$("div#publicidades_top").addClass('clearfix');	
	//$("div#todoportal").addClass('clearfix');
	$("div.zonaABC").addClass('clearfix');
	$("div.contenidoABC").addClass('clearfix');
	$("div.contenidoAB").addClass('clearfix');
	$("div.colAB").addClass('clearfix');
	$("div.pie").addClass('clearfix');
	$("div.pieIN").addClass('clearfix');
	
	$("div.zonaPARTICIPACION").addClass('clearfix');
	$("div.todoparticipacion").addClass('clearfix');
	$("div.ZP_contenido").addClass('clearfix');
	$("div.ZPC").addClass('clearfix');
	$("div.ZPC_contenidoABC").addClass('clearfix');
	$("div.modPART").addClass('clearfix');
	$("div.modPARTheader").addClass('clearfix');
	$("div.modPARTcontent").addClass('clearfix');
	$("div.modulo-consultorio").addClass('clearfix');
	$("div.moduloparticipacion2").addClass('clearfix');
	
	$("div.articulo div.related-link").addClass('clearfix');
	$("div.bomba-AB div.link-app3 div.related-link").addClass('clearfix');
	$("div.una-vez-ano div.link-app1").addClass('clearfix');
		
};

MQT.randomXToY = function (minVal,maxVal,floatVal) {
  var randVal = minVal+(Math.random()*(maxVal-minVal));
  return typeof floatVal=='undefined'?Math.round(randVal):randVal.toFixed(floatVal);
};

MQT.cambiarFotonoticia = function (modulo, actual, nuevo) {
	
	$('#' + modulo + '-' + nuevo).find('img[rel*="/"]').attr('src', $('#' + modulo + '-' + nuevo).find('img[rel*="/"]').attr('rel'));

	$('#' + modulo + '-' + actual).hide();
	$('#' + modulo + '-' + nuevo).show();
	
	$('.' + modulo + ' .navegacion_galeria .ng_numeros a:eq(' + (actual - 1) + ')').removeClass('ng_numero_activo');
	$('.' + modulo + ' .navegacion_galeria .ng_numeros a:eq(' + (nuevo - 1) + ')').addClass('ng_numero_activo');
	
};

MQT.paginacion = function (modulo, aleatorio) {
	
	var num_fotonoticias = $('.' + modulo + ' .ng_numeros a').length;

	if(num_fotonoticias == '') {
		num_fotonoticias = 1;
	}
	
	
	var actual = aleatorio ? MQT.randomXToY(1, num_fotonoticias) : 1;

	if(num_fotonoticias == 1) {
		actual = 1;
		aleatorio = false;
	}


	if($('#' + modulo + '-' + actual).find('img[rel*="/"]').attr('rel')) {
		$('#' + modulo + '-' + actual).find('img[rel*="/"]').attr('src', $('#' + modulo + '-' + actual).find('img[rel*="/"]').attr('rel'));
	} else {
		if(actual == 1 && $('#' + modulo + '-' + actual).find('img[rel*="/"]').css('display') == 'none') {
			$('#' + modulo + '-' + actual).find('img[rel*="/"]').css('display', 'block');
		}
	}
	
	$('.' + modulo + ' .navegacion_galeria .ng_numeros a:eq(' + (actual - 1) + ')').addClass('ng_numero_activo');
	

	// Ocultamos todas menos la actual
	
	$('[id*=' + modulo + '-]').each(function (i) {
			if (i != (actual - 1)) {
				$(this).hide();
			} 
		}
	);
	
	var urlGaleria = $('.' + modulo + ' .photo').find('a').attr('href');

	// Numeros
	
	$('.' + modulo + ' .navegacion_galeria .ng_numeros a').each(function() {
			$(this).attr('href', 'javascript:void(0)');
			
			var posicion = $(this).text();
			
			$(this).click(function() {

					MQT.cambiarFotonoticia(modulo, actual, posicion);
					
					actual = posicion;
				}
			);
		}
	);
	
	
	// Siguiente
	$('.' + modulo + ' .navegacion_galeria [class="ngas ngas_sig"]').attr('href', 'javascript:void(0)');
	$('.' + modulo + ' .navegacion_galeria [class="ngas ngas_sig"]').click(function(){
			//var siguiente = ((actual + 1) > num_fotonoticias) ? 1 : (actual + 1);
			var siguiente;
			$('.' + modulo).find('.ng_numeros').find('a').each(function(index){
				if($(this).hasClass('ng_numero_activo')) {
					var ind = parseInt(index);
					ind++;
					siguiente = ind + 1;
				}
			});
			if(siguiente > num_fotonoticias) {
				siguiente = 1;
			}
			
			
			/*if(urlGaleria != null && urlGaleria.indexOf('http://') >= 0 && siguiente == 1) {
				document.location.href = urlGaleria;
				return;
			} else {
				MQT.cambiarFotonoticia(modulo, actual, siguiente);
			}*/
			MQT.cambiarFotonoticia(modulo, actual, siguiente);			
			actual = siguiente;
		}
	);
	
	// Anterior
	
	$('.' + modulo + ' .navegacion_galeria [class="ngas ngas_ant"]').attr('href', 'javascript:void(0)');
	$('.' + modulo + ' .navegacion_galeria [class="ngas ngas_ant"]').click(function(){
			
			var anterior = ((actual - 1) < 1) ? num_fotonoticias : (actual - 1);
			
			MQT.cambiarFotonoticia(modulo, actual, anterior);
			
			actual = anterior;
		}
	);

	// Foto

	if(num_fotonoticias > 1) {
		if($('.' + modulo + ' .photo').find('a').find("img") && $('.' + modulo + ' .photo').find('a').attr('href').indexOf('/') == -1) {
			$('.' + modulo + ' .photo').find('a').attr('href', 'javascript:void(0)');
			$('.' + modulo + ' .photo').find('a').click(function(){
					
					var siguiente = ((actual + 1) > num_fotonoticias) ? 1 : (actual + 1);
					if(urlGaleria != null && urlGaleria.indexOf('http://') >= 0 && siguiente == 1) {
						document.location.href = urlGaleria;
						return;
					} else {
						MQT.cambiarFotonoticia(modulo, actual, siguiente);
					}
					
					actual = siguiente;
				}
			);
		}
	}
};

MQT.galeriaH = function () {
	
	$(document).ready(function() {

			num_fotonoticias = 0;
			
			$(".fotogaleriasH .photo").each( function(indice) {
				$(this).prepend($('<span class="fgHl"></span><span class="fgHr"></span>'));
				$(this).find(".fgHl").height($(this).height());
				$(this).find(".fgHr").height($(this).height());
				$(this).parent().find(".nav_fgH_ant").height($(".fotogaleriasH .photo").height());
				$(this).parent().find(".nav_fgH_sig").height($(".fotogaleriasH .photo").height());
				if(MQT.fotoArticuloActual == '' && ($(this).is(':hidden') || !$(this).find("img").attr('rel'))) {
					datos_actual = $(this).attr('id').split('-');
					MQT.fotoArticuloActual = datos_actual[1];
				}

				if(indice > 0) {
					$(this).hide();
					return false;
				}
			}); 

			$(".fotogaleriasH .photo").each( function(indice) {
				num_fotonoticias++;
				if(indice > 0) {
					$(this).hide();
				}
			});
			
			// Siguiente
			$('.fotogaleriasH .nav_fgH_sig').attr('href', 'javascript:void(0)');
			$('.fotogaleriasH .nav_fgH_sig').click(function() {
					var siguiente = MQT.fotoArticuloActual >= num_fotonoticias ? 1 : parseInt(MQT.fotoArticuloActual) + 1;
					
					MQT.cambiarFotonoticia('foto', MQT.fotoArticuloActual, siguiente);
					
					$(document).ready(function() {
						$(this).parent().find(".nav_fgH_ant").height($("#foto-" + siguiente).height());
						$(this).parent().find(".nav_fgH_sig").height($("#foto-" + siguiente).height());
					});
					MQT.fotoArticuloActual = siguiente;
				}
			);
			
			// Anterior
				$('.fotogaleriasH .nav_fgH_ant').attr('href', 'javascript:void(0)');
				$('.fotogaleriasH .nav_fgH_ant').click(function(){
					var anterior = MQT.fotoArticuloActual <= 1 ? num_fotonoticias : parseInt(MQT.fotoArticuloActual) - 1;

					MQT.cambiarFotonoticia('foto', MQT.fotoArticuloActual, anterior);
					$(document).ready(function() {
						$(this).parent().find(".nav_fgH_ant").height($("#foto-" + anterior).height());
						$(this).parent().find(".nav_fgH_sig").height($("#foto-" + anterior).height());
					});
					MQT.fotoArticuloActual = anterior;
				}
			);
		}
	);
	
};


MQT.moduloscabecera4 = function () {
	//buscador
	$("div.menunav").append($(".cab_buscar"));
	$('.cab_inputxt').keyup(function(e) {if(e.keyCode == 13) {busqueda();}});
	$("div.cabecera").append($("div.modulo_cabecera"));
	$("div.cabecera").append($("div.modcab_tv"));
	$("div.cabecera").append($("div.modcab_meteo"));
	MQT.moduloscabecera1();
	MQT.moduloscabecera5();
	MQT.zonaparticipacion();
	MQT.modulosLomas();
	MQT.articulosLead();
	MQT.abrirFormularioEnviar();
	MQT.cerrarFormularioEnviar();
	MQT.abrirFormularioRectificar();
	MQT.cerrarFormularioRectificar();
	MQT.zonaparticipacion();
	MQT.cls();
};

$(window).resize(function() {
	//alert($("body").width());
	var $calculoanchodelapagina = $("body").width() % 2;
	//alert($calculoanchodelapagina);
	if($calculoanchodelapagina == 0) {
		//alert("par");
		$("body").removeClass("bodyimpar");	
		$("body").addClass("bodypar");		
	} else {
		//alert("impar");
		$("body").removeClass("bodypar");
		$("body").addClass("bodyimpar");
	}
});

function MM_openBrWindow(theURL,winName,features) {
                window.name='principal';
                window.open(theURL,winName,features);
        }

