var memory_string="";

/***********************************************/
//Se llamar� desde el flash para saber si tiene que recordar una posici�n memorizada
function has_memory() {
	if(memory_string){
		var flashObj = getFlashMovieObject("video");
		flashObj.SetVariable("main.memorize", memory_string);
		memory_string = "";
	}
}

//Funciones pasarela hacia el top
function minimizar(id, args) {
	memory_string = args;
	top.minimizar(id, args);
}
function maximizar(id,args) {
	memory_string = args;
	top.maximizar(id, args);
}

function getFlashMovieObject(movieName)
{
  if (window.document[movieName]){
      return window.document[movieName];
  }
  if (navigator.appName.indexOf("Microsoft Internet")==-1){
    if (document.embeds && document.embeds[movieName])
      return document.embeds[movieName]; 
  }
  else{
    return document.getElementById(movieName);
  }
}
