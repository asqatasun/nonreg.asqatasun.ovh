<!-- funciones para el buscador -->

var super_node = null;
var timeoutid = 0;
var elem_ini = "galfoto_1"
var temps_slide = 2500;

function gotoPlana(node, page) {
	while(node.id.indexOf("galfoto_")==-1) {
		node = node.parentNode
	}
	node.style.display="none";
	
	var id = page.substring("galfoto_".length);

	// IE 6+ hack with synchronization
	setTimeout("document.getElementById('img_" + id + "').src='" + medias[id] + "';",100);
	
	document.getElementById(page).style.display = "block";
	super_node = document.getElementById(page);
	
	recargar();
}

function inici(node) {
	while(node.id.indexOf("galfoto_")==-1) {
		node = node.parentNode
	}
	node.style.display="none";
	
	// IE 6+ hack with synchronization
	setTimeout("document.getElementById('img_1').src='" + medias[1] + "';",100);
	
	document.getElementById(elem_ini).style.display = "block";
	super_node = document.getElementById(elem_ini);

	recargar();
}

function goto(node) {
	var id = node.firstChild.nodeValue;
	
	while(node.id.indexOf("galfoto_")==-1) {
		node = node.parentNode
	}
	node.style.display="none";
	
	// IE 6+ hack with synchronization
	setTimeout("document.getElementById('img_" + id + "').src='" + medias[id] + "';",100);
	
	var elem = "galfoto_" + id;
	document.getElementById(elem).style.display = "block";
	super_node = document.getElementById(elem);

	recargar();
}

function playPause() {
	if (timeoutid !=0) {
		var anchors = document.getElementsByName('playpause');
		// loop through all anchor tags
		for (var i=0; i < anchors.length; i++){
			var anchor = anchors[i];
			anchor.setAttribute("src", "/img/ico_boton_play.gif");
			anchor.nextSibling.nodeValue = ' play';
			}
		stop();
	} else {
		var anchors = document.getElementsByName('playpause');
		// loop through all anchor tags
		for (var i=0; i < anchors.length; i++){
			var anchor = anchors[i];
			anchor.setAttribute("src", "/img/ico_boton_pause.gif");
			anchor.nextSibling.nodeValue = ' pause';
			}
		play();
	}
}

function play() {
	stop();
	timeoutid = setTimeout("loop()",temps_slide);
}

function stop() {
    if (timeoutid != 0) {
      clearTimeout(timeoutid);
      timeoutid = 0;
    }
}

function loop() {
	nextFotogal(super_node);
	play();
}

function nextFotogal(node) {
	if (node == undefined) {
		node=document.getElementById("galfoto_1");
	}
	while(node.id.indexOf("galfoto_")==-1) {
		node = node.parentNode
	}
	node.style.display="none";
	var sibling=node;
	do {
		sibling = sibling.nextSibling;
		if (!sibling) sibling=node.parentNode.firstChild;
	} while (sibling.nodeType!=1)

	var id = sibling.id.substring("galfoto_".length);
	
	// IE 6+ hack with synchronization
	setTimeout("document.getElementById('img_" + id + "').src='" + medias[id] + "';",100);
	
	sibling.style.display="block";
	super_node = sibling;

	recargar();

	return false;
}

function prevFotogal(node) {
	while(node.id.indexOf("galfoto_")==-1){
		node = node.parentNode
	}
	node.style.display="none";
	var sibling=node;
	do {
		sibling = sibling.previousSibling;
		if (!sibling) sibling=node.parentNode.lastChild;
	} while (sibling.nodeType!=1)

	var id = sibling.id.substring("galfoto_".length);

	// IE 6+ hack with synchronization
	setTimeout("document.getElementById('img_" + id + "').src='" + medias[id] + "';",100);
	
	sibling.style.display="block";
	super_node = sibling;

	recargar();

	return false;
}

<!-- fin funciones buscador -->

function loadVideo(div, id, w, h, file, preview) {
	var so = new SWFObject('/swf/CTVMediaPlayer.swf','mpv'+id, w, h,'8');
	so.addParam("allowfullscreen","true");
	so.addVariable("displayheight", h);
	so.addParam("menu","false");
	so.addVariable("file", file);
	so.addVariable("image", preview);
	so.addVariable('enablejs','true');
	so.addVariable("javascriptid","mpv" + id);
	so.addVariable("width", w);
	so.addVariable("height", h);
	so.addVariable("wmode", "transparent");
	so.write(div);
}

function loadAudio(div, id, w, h, file, preview) {
	var so = new SWFObject('/swf/CTVAudioPlayer.swf','mpa'+id, w, h, '7');
	so.addParam("allowfullscreen", "false");
	so.addVariable('file', file);
	so.addVariable('width', w);
	so.addVariable("height", h);
	so.addVariable("wmode", "transparent");
	if (preview) {
		so.addVariable("image", preview);
	}
	so.write(div);
}

function loadSWF(div, id, w, h, file) {
	var so = new SWFObject(file,'mpsw'+id, w, h,'8');
	so.addParam("allowfullscreen","true");
	so.addParam("menu","false");
	so.addVariable('enablejs','true');
	so.addVariable("javascriptid","mpsw" + id);
	so.addVariable("width", w);
	so.addVariable("height", h);
	so.addVariable("wmode", "transparent");
	so.write(div);
}

// -----------------------------------------------------------------------
var paginaActual = 0;

function bloqueSiguiente(numPaginasTmp)
{
	var numPaginas = numPaginasTmp;
	if(numPaginasTmp==0){
		var numPaginas=1;
	}
	
	var nombreIDAOcultar = 'pag_ultimos_' + (paginaActual + 1);
	var nombreIDDesmarcar = 'pagina_' + (paginaActual + 1);
	var obj = document.getElementById (nombreIDAOcultar);
	var obj2 = document.getElementById (nombreIDDesmarcar);
	if (obj != null && obj2 != null) {
		obj.style.display = 'none'; 
		obj2.className = '';		
	}
	
	paginaActual = (paginaActual + 1) % Math.ceil(numPaginas);
		
	var nombreIDAMostrar = 'pag_ultimos_' + (paginaActual + 1);
	var nombreIDMarcar = 'pagina_' + (paginaActual + 1);
	obj = document.getElementById (nombreIDAMostrar);
	obj2 = document.getElementById (nombreIDMarcar);
	if (obj != null && obj2 != null) {
		obj.style.display = 'block'; 
		obj2.className = 'activo';		
	}
	
	return false;
}

function bloqueAnterior(numPaginasTmp)
{
	var numPaginas = numPaginasTmp;
	if(numPaginasTmp==0){
		numPaginas=1;
	}
	//var numPaginas = numPaginastmp - 1;
	//alert(Math.ceil(numPaginas));
	var nombreIDAOcultar = 'pag_ultimos_' + (paginaActual + 1);
	var nombreIDDesmarcar = 'pagina_' + (paginaActual + 1);
	var obj = document.getElementById (nombreIDAOcultar);
	var obj2 = document.getElementById (nombreIDDesmarcar);
	if (obj != null && obj2 != null) {
		obj.style.display = 'none'; 
		obj2.className = '';		
	}
	
	if(paginaActual != 0){
		paginaActual = (paginaActual - 1) % Math.ceil(numPaginas);
	}else{
		paginaActual = Math.ceil(numPaginas) - 1;
	}
	var nombreIDAMostrar = 'pag_ultimos_' + (paginaActual + 1);
	var nombreIDMarcar = 'pagina_' + (paginaActual + 1);
	obj = document.getElementById (nombreIDAMostrar);
	obj2 = document.getElementById (nombreIDMarcar);
	if (obj != null && obj2 != null) {
		obj.style.display = 'block'; 
		obj2.className = 'activo';		
	}
	
	return false;
}

function navegarBloqueUltimos(numPag)
{
	var nombreIDAOcultar = 'pag_ultimos_' + (paginaActual + 1);
	var nombreIDDesmarcar = 'pagina_' + (paginaActual + 1);
	var obj = document.getElementById (nombreIDAOcultar);
	var obj2 = document.getElementById (nombreIDDesmarcar);
	if (obj != null && obj2 != null) {
		obj.style.display = 'none'; 
		obj2.className = '';		
	}
	
	var nombreIDAMostrar = 'pag_ultimos_' + (numPag);
	var nombreIDMarcar = 'pagina_' + (numPag);
	obj = document.getElementById (nombreIDAMostrar);
	obj2 = document.getElementById (nombreIDMarcar);
	if (obj != null && obj2 != null) {
		obj.style.display = 'block'; 
		obj2.className = 'activo';		
	}
	paginaActual = numPag-1;
	
	return false;
}

var paginaActualRelacionados = 0;

function bloqueSiguienteRel(numPaginasTmp)
{
	var numPaginas = numPaginasTmp;
	if(numPaginasTmp==0){
		numPaginas=1;
	}
	//alert(Math.ceil(numPaginas));
	var nombreIDAOcultar = 'pag_relacionados_' + (paginaActualRelacionados + 1);
	var nombreIDDesmarcar = 'paginaRel_' + (paginaActualRelacionados + 1);
	var obj = document.getElementById (nombreIDAOcultar);
	var obj2 = document.getElementById (nombreIDDesmarcar);
	if (obj != null && obj2 != null) {
		obj.style.display = 'none'; 
		obj2.className = '';		
	}
	
	paginaActualRelacionados = (paginaActualRelacionados + 1) % Math.ceil(numPaginas);
	
	var nombreIDAMostrar = 'pag_relacionados_' + (paginaActualRelacionados + 1);
	var nombreIDMarcar = 'paginaRel_' + (paginaActualRelacionados + 1);
	obj = document.getElementById (nombreIDAMostrar);
	obj2 = document.getElementById (nombreIDMarcar);
	if (obj != null && obj2 != null) {
		obj.style.display = 'block'; 
		obj2.className = 'activo';		
	}
	
	return false;
}

function bloqueAnteriorRel(numPaginasTmp)
{
	var numPaginas = numPaginasTmp;
	if(numPaginasTmp==0){
		numPaginas=1;
	}
	//alert(Math.ceil(numPaginas));
	var nombreIDAOcultar = 'pag_relacionados_' + (paginaActualRelacionados + 1);
	var nombreIDDesmarcar = 'paginaRel_' + (paginaActualRelacionados + 1);
	var obj = document.getElementById (nombreIDAOcultar);
	var obj2 = document.getElementById (nombreIDDesmarcar);
	if (obj != null && obj2 != null) {
		obj.style.display = 'none'; 
		obj2.className = '';		
	}
	
	if(paginaActualRelacionados != 0){
		paginaActualRelacionados = (paginaActualRelacionados - 1) % Math.ceil(numPaginas);
	}else{
		paginaActualRelacionados = Math.ceil(numPaginas) - 1;
	}
	var nombreIDAMostrar = 'pag_relacionados_' + (paginaActualRelacionados + 1);
	var nombreIDMarcar = 'paginaRel_' + (paginaActualRelacionados + 1);
	obj = document.getElementById (nombreIDAMostrar);
	obj2 = document.getElementById (nombreIDMarcar);
	if (obj != null && obj2 != null) {
		obj.style.display = 'block'; 
		obj2.className = 'activo';		
	}
	
	return false;
}

function navegarBloqueRelacionados(numPag)
{
	var nombreIDAOcultar = 'pag_relacionados_' + (paginaActual + 1);
	var nombreIDDesmarcar = 'paginaRel_' + (paginaActual + 1);
	var obj = document.getElementById (nombreIDAOcultar);
	var obj2 = document.getElementById (nombreIDDesmarcar);
	if (obj != null && obj2 != null) {
		obj.style.display = 'none'; 
		obj2.className = '';		
	}
	
	var nombreIDAMostrar = 'pag_relacionados_' + (numPag);
	var nombreIDMarcar = 'paginaRel_' + (numPag);
	obj = document.getElementById (nombreIDAMostrar);
	obj2 = document.getElementById (nombreIDMarcar);
	if (obj != null && obj2 != null) {
		obj.style.display = 'block'; 
		obj2.className = 'activo';		
	}
	paginaActualRelacionados = numPag-1;
	
	return false;
}


function swapTabs(node,sel,tot) 
{
	var arrel = node.id.substring(0,node.id.indexOf("_"));
	for(var i=1;i<=tot;i++){
		var id1 = arrel+'_'+i;
		var id2 = 'cont_'+arrel+'_'+i;
		document.getElementById(id1).className = '';
		document.getElementById(id2).style.display="none";
			if(i == sel){
 			document.getElementById(id1).className = 'activo';
 			document.getElementById(id2).style.display="block";
			}
	}
	return false;
}

function marcaBoto(arrel,sel,tot) {
	for(var i=1;i<=tot;i++){
		var id1 = arrel+'_'+i;
		document.getElementById(id1).className = '';
		if(i == sel){document.getElementById(id1).className = 'selected';}
	}
}

function sendEvent(typ,prm,idVisor) { thisMovie(idVisor).sendEvent(typ,prm); };
function loadFile(obj,idVisor) { thisMovie(idVisor).loadFile(obj); };
function thisMovie(movieName) {
	if(navigator.appName.indexOf("Microsoft") != -1) {
		return window[movieName];
	} else {
		return document[movieName];
	}
};

// ------------------------------------------------------------------------------------
function cambioFoto(pag) {
	document.getElementById("div_foto").innerHTML = arr[0];
}

function play() {
	enplay = 1;
	var interval = document.frmFoto.intervalo.value;
	var numpag = parseInt(document.frmFoto.pag.value);
	numpag++;
	if(numpag > parseInt(document.frmFoto.num_fotos_categoria.value)) numpag = 1;
		cambioFoto(numpag);
		timeout = setTimeout("play()", interval*1000);
	}

function pause() {
	clearTimeout(timeout);
	document.getElementById("div_play1").innerHTML = "<a href=\"javascript:play();\" class=\"boton\" title=\"Reproducir\"><img src=\"/img/ico_boton_play.gif\" alt=\"Reproducir\" />&nbsp;play</a>";
	document.getElementById("div_play2").innerHTML = "<a href=\"javascript:play();\" class=\"boton\" title=\"Reproducir\"><img src=\"/img/ico_boton_play.gif\" alt=\"Reproducir\" />&nbsp;play</a>";
	enplay = 0;
}

// --------------- Funcion para cargar el player de video ----------------

function cargaPlayerVideo(nombreDiv, ancho, alto, rutaVideo, rutaImagen, sitio, titulo){
	var NOMBRE_DE_DIV = nombreDiv;
    var ANCHURA = ancho;
    var ALTURA = alto;
    var RUTA_VIDEO = rutaVideo;
    var RUTA_IMAGEN = rutaImagen;
    var SITIO = sitio;
    var PUBLI_TITULO = titulo;
    var CATEGORY  = "methode";
    return loadMultimedia(NOMBRE_DE_DIV, ANCHURA, ALTURA, RUTA_VIDEO, RUTA_IMAGEN, SITIO, PUBLI_TITULO, CATEGORY);		
}