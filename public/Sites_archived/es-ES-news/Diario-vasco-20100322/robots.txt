User-agent: *
Disallow: /prensa/
Allow: /prensa/2006*/
Allow: /prensa/200701*/
Allow: /prensa/200702*/
Allow: /prensa/200703*/
Allow: /prensa/200704*/
Allow: /prensa/200705*/
Allow: /prensa/200706*/
Allow: /prensa/200707*/
Allow: /prensa/noticias
Allow: /prensa/fotos/
Allow: /prensa/opinion
Allow: /prensa/*/opinion
Allow: /prensa/vinnetas

Allow: /prensa/altodeba/altodeba.html
Allow: /prensa/bajodeba/bajodeba.html
Allow: /prensa/sansebastian/sansebastian.html
Allow: /prensa/altourola/altourola.html
Allow: /prensa/bidasoa/bidasoa.html
Allow: /prensa/comarca/comarca.html
Allow: /prensa/costaurola/costaurola.html
Allow: /prensa/pasaia/pasaia.html
Allow: /prensa/tolosa/tolosa.html
Allow: /prensa/politica/politica.html
Allow: /prensa/mundo/mundo.html

Allow: /prensa/gente/gente.html
Allow: /prensa/television/television.html
Allow: /prensa/efemerides/efemerides.html
Allow: /prensa/deportes/
Allow: /prensa/cultura/
Allow: /prensa/especiales/especiales.html
Disallow: /RC/
Allow: /RC/*/Media/
Disallow: /registro/
Disallow: /interactivo/comun/contactar.html
Disallow: /interactivo/comun/condiciones.html
Disallow: /interactivo/comun/privacidad.html
Disallow: /interactivo/comun/publicidad.html
Disallow: /popups/
User-agent: Mediapartners-Google
Disallow: 
User-agent: GoogleBot-Image
Disallow:
