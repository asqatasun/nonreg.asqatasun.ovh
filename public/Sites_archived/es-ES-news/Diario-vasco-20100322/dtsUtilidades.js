/*
Devuelve el portal en el que estamos navegando o null si no estamos en ninguno o hay algun error
*/
function getPortal() {
	var portal = null;
	var url = location.href;
	
	if (url.indexOf ('diariosur.es') != -1) {
		portal = 'diariosur';
	}
	else if (url.indexOf ('diariovasco.com') != -1) {
		portal = 'diariovasco';
	}
	else if (url.indexOf ('elcomerciodigital.com') != -1) {
		portal = 'elcomerciodigital';
	}
	else if (url.indexOf ('elcorreo.com') != -1) {
		portal = 'elcorreo';
	}
	else if (url.indexOf ('elcorreodigital.com') != -1) {
		portal = 'elcorreodigital';
	}
	else if (url.indexOf ('eldiariomontanes.es') != -1) {
		portal = 'eldiariomontanes';
	}
	else if (url.indexOf ('hoysport.com') != -1) {
		portal = 'hoysport';
	}
	else if (url.indexOf ('hoy.es') != -1) {
		portal = 'hoy';
	}
	else if (url.indexOf ('ideal.es') != -1) {
		portal = 'ideal';
	}
	else if (url.indexOf ('larioja.com') != -1) {
		portal = 'larioja';
	}
	else if (url.indexOf ('lasprovincias.es') != -1) {
		portal = 'lasprovincias';
	}
	else if (url.indexOf ('laverdad.es') != -1) {
		portal = 'laverdad';
	}
	else if (url.indexOf ('lavozdigital.es') != -1) {
		portal = 'lavozdigital';
	}
	else if (url.indexOf ('nortecastilla.es') != -1) {
		portal = 'nortecastilla';
	}
	else if (url.indexOf ('abcdesevilla.es') != -1) {
		portal = 'abcdesevilla';
	}
	else if (url.indexOf ('abc.es') != -1) {
		portal = 'abc';
	}
	else if (url.indexOf ('surinenglish.com') != -1) {
		portal = 'surinenglish';
	}
	
	return portal;
}

/*
Devuelve la edicion local del portal en el que estamos navegando, o null si es monoedicion
*/
function getEdicionLocal() {
	var portal = getPortal();
	var edicionLocal = null;
	var url = location.href;
	
	if ((portal != null) && ((portal == 'elcorreo') || (portal == 'elcorreodigital'))) {
		if (url.indexOf ('/alava') != -1) {
			edicionLocal = 'alava';
		}
		else {
			edicionLocal = 'vizcaya';
		}
	}else if ((portal != null) && (portal == 'ideal')) {
		if (url.indexOf ('/jaen') != -1) {
			edicionLocal = 'jaen';
		}
		else if (url.indexOf ('/almeria') != -1) {
			edicionLocal = 'almeria';
		}
		else {
			edicionLocal = 'granada';
		}
	}
	else if ((portal != null) && (portal == 'lasprovincias')) {
		if (url.indexOf ('/alicante') != -1) {
			edicionLocal = 'alicante';
		}
		else if (url.indexOf ('/castellon') != -1) {
			edicionLocal = 'castellon';
		}
		else {
			edicionLocal = 'valencia';
		}
	}
	else if ((portal != null) && (portal == 'laverdad')) {
		if (url.indexOf ('/albacete') != -1) {
			edicionLocal = 'albacete';
		}
		else if (url.indexOf ('/alicante') != -1) {
			edicionLocal = 'alicante';
		}
		else {
			edicionLocal = 'murcia';
		}
	}
	else if ((portal != null) && (portal == 'lavozdigital')) {
		if (url.indexOf ('/jerez') != -1) {
			edicionLocal = 'jerez';
		}
		else {
			edicionLocal = 'cadiz';
		}
	}
	
	return edicionLocal;
}

/*
Devuelve true si estamos en la Home del portal, o false en caso contrario
*/
function esHomePortal() {
	var estamosEnHome = false;

	var url = location.href;
	if (url != null) {
		var partesURL = url.split('/');
		if (partesURL != null) {
			// Comprobamos si estamos en un medio monoedicion o multiedicion
			var edicionLocal = getEdicionLocal();
		
			// Medios monoedicion
			if (edicionLocal == null) {
				if (partesURL.length <= 4) estamosEnHome = true;
			}
			else {
				if (partesURL.length <= 5) estamosEnHome = true;
			}
		}
	}
	
	return estamosEnHome;
}


function llamadaGoogleAnalytics(){
	var portal = getPortal();
	var edicionLocal = getEdicionLocal();
	
	if(portal == "" || portal == null){
		portal = "localhost";
	}
	/* Para el caso de abc */
	if(edicionLocal == "" || edicionLocal == null){
		edicionLocal = "";
		if(location.href.indexOf("sevilla") >= 0){
			edicionLocal = "desevilla";
		}
	}
	
	/* Crear hashtable con informacion requerida por portal */
	var portalesDatosCodigoGoogle = new Object();
	var portalesDatosDominioGoogle= new Object();
	var portalesDatosTrackPageView = new Object();
	
	portalesDatosCodigoGoogle["abc"]              = "UA-67773806-1";
	portalesDatosDominioGoogle["abc"]			  = "";
	portalesDatosTrackPageView["abc"]             = "";
	
	portalesDatosCodigoGoogle["abcdesevilla"]     = "UA-67773806-1";
	portalesDatosDominioGoogle["abcdesevilla"]    = "";
	portalesDatosTrackPageView["abcdesevilla"]    = "/sevilla/";
	
	portalesDatosCodigoGoogle["diariosur"]        = "UA-3128303-21";
	portalesDatosDominioGoogle["diariosur"]       = "";
	portalesDatosTrackPageView["diariosur"]       = "";
	
	portalesDatosCodigoGoogle["nortecastilla"]    = "UA-7123523-1";
	portalesDatosDominioGoogle["nortecastilla"]   = "";
	portalesDatosTrackPageView["nortecastilla"]   = "";
	
	portalesDatosCodigoGoogle["lavozdigital"]     = "UA-3047322-6";
	portalesDatosDominioGoogle["lavozdigital"]    = "";
	portalesDatosTrackPageView["lavozdigital"]    = "";
	portalesDatosCodigoGoogle["lavozdigitaljerez"]     = "UA-3047322-6";
	portalesDatosDominioGoogle["lavozdigitaljerez"]    = "";
	portalesDatosTrackPageView["lavozdigitaljerez"]    = "";
	portalesDatosCodigoGoogle["lavozdigitalcadiz"]     = "UA-3047322-6";
	portalesDatosDominioGoogle["lavozdigitalcadiz"]    = "";
	portalesDatosTrackPageView["lavozdigitalcadiz"]    = "";
	
	portalesDatosCodigoGoogle["laverdad"]         = "UA-6052672-2";
	portalesDatosDominioGoogle["laverdad"]        = "";
	portalesDatosTrackPageView["laverdad"]        = "";
	portalesDatosCodigoGoogle["laverdadalbacete"]         = "UA-6052672-2";
	portalesDatosDominioGoogle["laverdadalbacete"]        = "";
	portalesDatosTrackPageView["laverdadalbacete"]        = "";
	portalesDatosCodigoGoogle["laverdadalicante"]         = "UA-6052672-2";
	portalesDatosDominioGoogle["laverdadalicante"]        = "";
	portalesDatosTrackPageView["laverdadalicante"]        = "";
	portalesDatosCodigoGoogle["laverdadmurcia"]         = "UA-6052672-2";
	portalesDatosDominioGoogle["laverdadmurcia"]        = "";
	portalesDatosTrackPageView["laverdadmurcia"]        = "";
	
	portalesDatosCodigoGoogle["lasprovincias"]    = "UA-7008806-1";
	portalesDatosDominioGoogle["lasprovincias"]   = "";
	portalesDatosTrackPageView["lasprovincias"]   = "";
	portalesDatosCodigoGoogle["lasprovinciascastellon"]    = "UA-7008806-1";
	portalesDatosDominioGoogle["lasprovinciascastellon"]   = "";
	portalesDatosTrackPageView["lasprovinciascastellon"]   = "";
	portalesDatosCodigoGoogle["lasprovinciasalicante"]    = "UA-7008806-1";
	portalesDatosDominioGoogle["lasprovinciasalicante"]   = "";
	portalesDatosTrackPageView["lasprovinciasalicante"]   = "";
	portalesDatosCodigoGoogle["lasprovinciasvalencia"]    = "UA-7008806-1";
	portalesDatosDominioGoogle["lasprovinciasvalencia"]   = "";
	portalesDatosTrackPageView["lasprovinciasvalencia"]   = "";
	
	portalesDatosCodigoGoogle["larioja"]          = "UA-7124171-1";
	portalesDatosDominioGoogle["larioja"]         = "";
	portalesDatosTrackPageView["larioja"]         = "";
	
	portalesDatosCodigoGoogle["idealgranada"]     = "UA-4272508-12";
	portalesDatosDominioGoogle["idealgranada"]	  = "";
	portalesDatosTrackPageView["idealgranada"]    = "";
	portalesDatosCodigoGoogle["idealalmeria"]     = "UA-4272508-13";
	portalesDatosDominioGoogle["idealalmeria"]	  = "";
	portalesDatosTrackPageView["idealalmeria"]	  = "";	
	portalesDatosCodigoGoogle["idealjaen"]        = "UA-4272508-14";
	portalesDatosDominioGoogle["idealjaen"]		  = "";
	portalesDatosTrackPageView["idealjaen"]		  = "";
	
	portalesDatosCodigoGoogle["hoy"]              = "UA-7648528-1";
	portalesDatosDominioGoogle["hoy"]             = "";
	portalesDatosTrackPageView["hoy"]			  = "";
	
	portalesDatosCodigoGoogle["eldiariomontanes"] = "UA-7123523-1";
	portalesDatosDominioGoogle["eldiariomontanes"]= "";
	portalesDatosTrackPageView["eldiariomontanes"]= "";
	
	portalesDatosCodigoGoogle["elcorreo"]  = "UA-7212485-3";
	portalesDatosDominioGoogle["elcorreo"] = "";
	portalesDatosTrackPageView["elcorreo"] = "";
	portalesDatosCodigoGoogle["elcorreoalava"]  = "UA-7212485-3";
	portalesDatosDominioGoogle["elcorreoalava"] = "";
	portalesDatosTrackPageView["elcorreoalava"] = "";
	portalesDatosCodigoGoogle["elcorreovizcaya"]  = "UA-7212485-3";
	portalesDatosDominioGoogle["elcorreovizcaya"] = "";
	portalesDatosTrackPageView["elcorreovizcaya"] = "";
	
	portalesDatosCodigoGoogle["elcomerciodigital"]= "UA-3458093-1";
	portalesDatosDominioGoogle["elcomerciodigital"]="";
	portalesDatosTrackPageView["elcomerciodigital"]="";
	portalesDatosCodigoGoogle["elcomerciodigitalaviles"]= "UA-3458093-1";
	portalesDatosDominioGoogle["elcomerciodigitalaviles"]="";
	portalesDatosTrackPageView["elcomerciodigitalaviles"]="";
	portalesDatosCodigoGoogle["elcomerciodigitaloviedo"]= "UA-3458093-1";
	portalesDatosDominioGoogle["elcomerciodigitaloviedo"]="";
	portalesDatosTrackPageView["elcomerciodigitaloviedo"]="";
	portalesDatosCodigoGoogle["elcomerciodigitalgijon"]= "UA-3458093-1";
	portalesDatosDominioGoogle["elcomerciodigitalgijon"]="";
	portalesDatosTrackPageView["elcomerciodigitalgijon"]="";
	
	portalesDatosCodigoGoogle["diariovasco"]      = "UA-59232-1";
	portalesDatosDominioGoogle["diariovasco"]     = "diariovasco.com";
	portalesDatosTrackPageView["diariovasco"]     = "S";
		
	/* Generar llamada Google Analytics */
	
	
		
	var scriptGoogle = unescape("%3Cscript type=\"text/javascript\">");
	scriptGoogle += 'try {';
	scriptGoogle +=	'var pageTracker = _gat._getTracker("'+portalesDatosCodigoGoogle[portal+edicionLocal]+'");';
	scriptGoogle += 'pageTracker._setDomainName("'+portalesDatosDominioGoogle[portal+edicionLocal]+'");';
	scriptGoogle += 'if("'+portalesDatosTrackPageView[portal+edicionLocal]+'" != null \&\&';
	scriptGoogle += '"'+portalesDatosTrackPageView[portal+edicionLocal]+'" != "" \&\&';
	scriptGoogle += '"'+portalesDatosTrackPageView[portal+edicionLocal]+'" != "S")'
	scriptGoogle += '{';
	scriptGoogle += 'pageTracker._trackPageview("'+portalesDatosTrackPageView[portal+edicionLocal]+'");';	
	scriptGoogle += '}else{';
	scriptGoogle += 'pageTracker._trackPageview();';
	scriptGoogle += '}'
	scriptGoogle += '} catch(err) ';
	scriptGoogle += '{ alert(err.description) }';
	scriptGoogle += unescape("%3C/script>");

	return unescape(scriptGoogle);
	
}