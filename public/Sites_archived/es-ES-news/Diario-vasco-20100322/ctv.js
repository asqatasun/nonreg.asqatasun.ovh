function loadVideo(div, id, w, h, file, preview) {
	var so = new SWFObject('/swf/CTVMediaPlayer.swf','mpv'+id, w, h,'8');
	so.addParam("allowfullscreen","true");
	so.addVariable("displayheight", h);
	so.addParam("menu","false");
	so.addVariable("file", file);
	so.addVariable("image", preview);
	so.addVariable('enablejs','true');
	so.addVariable("javascriptid","mpv" + id);
	so.addVariable("width", w);
	so.addVariable("height", h);
	so.addParam("wmode", "transparent");
	so.write(div);
}

function loadAudio(div, id, w, h, file, preview) {
	var so = new SWFObject('/swf/CTVAudioPlayer.swf','mpa'+id, w, h, '7');
	so.addParam("allowfullscreen", "false");
	so.addVariable('file', file);
	so.addVariable('width', w);
	so.addVariable("height", h);
	so.addParam("wmode", "transparent");
	if (preview) {
		so.addVariable("image", preview);
	}
	so.write(div);
}

function loadSWF(div, id, w, h, file) {
	var so = new SWFObject(file,'mpsw'+id, w, h,'8');
	so.addParam("allowfullscreen","true");
	so.addParam("menu","false");
	so.addVariable('enablejs','true');
	so.addVariable("javascriptid","mpsw" + id);
	so.addVariable("width", w);
	so.addVariable("height", h);
	//so.addParam("wmode", "transparent");
	so.write(div);
}

// -----------------------------------------------------------------------
function nextModule(node)
{
	while(node.id.indexOf("pag_")==-1){
		node = node.parentNode
	}
	node.style.display="none";

	var sibling=node;
	do {
		sibling = sibling.nextSibling;
		if (!sibling) sibling=node.parentNode.firstChild;
	} while (sibling.nodeType!=1)
			
	sibling.style.display="block";

	return false;
}

function prevModule(node)
{
	while(node.id.indexOf("pag_")==-1){
		node = node.parentNode
	}
	node.style.display="none";

	var sibling=node;
	do {
		sibling = sibling.previousSibling;
		if (!sibling) sibling=node.parentNode.lastChild;
	} while (sibling.nodeType!=1)
	
	sibling.style.display="block";

	return false;
}

function swapTabs(node,sel,tot) 
{
	var arrel = node.id.substring(0,node.id.indexOf("_"));
	for(var i=1;i<=tot;i++){
		var id1 = arrel+'_'+i;
		var id2 = 'cont_'+arrel+'_'+i;
		document.getElementById(id1).className = '';
		document.getElementById(id2).style.display="none";
			if(i == sel){
 			document.getElementById(id1).className = 'activo';
 			document.getElementById(id2).style.display="block";
			}
	}
	return false;
}

function marcaBoto(arrel,sel,tot) {
	for(var i=1;i<=tot;i++){
		var id1 = arrel+'_'+i;
		document.getElementById(id1).className = '';
		if(i == sel){document.getElementById(id1).className = 'selected';}
	}
}

function sendEvent(typ,prm,idVisor) { thisMovie(idVisor).sendEvent(typ,prm); };
function loadFile(obj,idVisor) { thisMovie(idVisor).loadFile(obj); };
function thisMovie(movieName) {
	if(navigator.appName.indexOf("Microsoft") != -1) {
		return window[movieName];
	} else {
		return document[movieName];
	}
};

// ------------------------------------------------------------------------------------
function cambioFoto(pag) {
	document.getElementById("div_foto").innerHTML = arr[0];
}

function play() {
	enplay = 1;
	var interval = document.frmFoto.intervalo.value;
	var numpag = parseInt(document.frmFoto.pag.value);
	numpag++;
	if(numpag > parseInt(document.frmFoto.num_fotos_categoria.value)) numpag = 1;
		cambioFoto(numpag);
		timeout = setTimeout("play()", interval*1000);
	}

function pause() {
	clearTimeout(timeout);
	document.getElementById("div_play1").innerHTML = "<a href=\"javascript:play();\" class=\"boton\" title=\"Reproducir\"><img src=\"/img/ico_boton_play.gif\" alt=\"Reproducir\" />&nbsp;play</a>";
	document.getElementById("div_play2").innerHTML = "<a href=\"javascript:play();\" class=\"boton\" title=\"Reproducir\"><img src=\"/img/ico_boton_play.gif\" alt=\"Reproducir\" />&nbsp;play</a>";
	enplay = 0;
}
