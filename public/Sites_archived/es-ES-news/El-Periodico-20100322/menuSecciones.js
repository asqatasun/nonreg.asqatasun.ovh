function XsltTransform(elIdSeccion, elIdSeccionPadre, elIdioma)
{ 
 var xml = Sarissa.getDomDocument();  
 var xslt = Sarissa.getDomDocument(); 
 
 xml.async = false;
 xslt.async = false;
  
 xml.load("/vivo/secciones/090907/" + elIdioma + "/noticies" + elIdioma + ".xml"); 
 //xml.load("/menu_v2/menu_horizontal_pestanas.xml");
 xslt.load("/dissenys/xsl/menuSecciones.xsl");
 
 var processor = new XSLTProcessor();
 processor.importStylesheet(xslt);
 
 //processor.setParameter(null, "idSeccion", elIdSeccion);  
 //processor.setParameter(null, "idSeccionPadre", elIdSeccionPadre);  
 
 var atr=xml.createAttribute("idSeccion");
 atr.nodeValue=elIdSeccion;
 xml.documentElement.attributes.setNamedItem(atr)
 
 var atr2=xml.createAttribute("idSeccionPadre");
 atr2.nodeValue=elIdSeccionPadre; 
 xml.documentElement.attributes.setNamedItem(atr2)

 var atr3=xml.createAttribute("idioma");
 atr3.nodeValue=elIdioma; 
 xml.documentElement.attributes.setNamedItem(atr3)


//alert(xml.documentElement.attributes["idSeccion"].value);
//alert(xml.documentElement.attributes["idSeccionPadre"].value);
 var XmlDom = processor.transformToDocument(xml)
 
 var serializer = new XMLSerializer(); 
 var output = serializer.serializeToString(XmlDom.documentElement);
 
 var outputDiv = document.getElementById("Output");
 outputDiv.innerHTML = output;
}

function XsltTransformCAT(elIdSeccion, elIdSeccionPadre, elIdioma)
{ 
 var xml = Sarissa.getDomDocument();  
 var xslt = Sarissa.getDomDocument(); 
 
 xml.async = false;
 xslt.async = false;
  
 xml.load("/vivo/secciones/091214/" + elIdioma + "/noticies" + elIdioma + ".xml"); 
 //xml.load("/menu_v2/menu_horizontal_pestanas.xml");
 xslt.load("/dissenys/xsl/menuSeccionesCAT.xsl");
 
 var processor = new XSLTProcessor();
 processor.importStylesheet(xslt);
 
 //processor.setParameter(null, "idSeccion", elIdSeccion);  
 //processor.setParameter(null, "idSeccionPadre", elIdSeccionPadre);  
 
 var atr=xml.createAttribute("idSeccion");
 atr.nodeValue=elIdSeccion;
 xml.documentElement.attributes.setNamedItem(atr)
 
 var atr2=xml.createAttribute("idSeccionPadre");
 atr2.nodeValue=elIdSeccionPadre; 
 xml.documentElement.attributes.setNamedItem(atr2)

 var atr3=xml.createAttribute("idioma");
 atr3.nodeValue=elIdioma; 
 xml.documentElement.attributes.setNamedItem(atr3)


//alert(xml.documentElement.attributes["idSeccion"].value);
//alert(xml.documentElement.attributes["idSeccionPadre"].value);
 var XmlDom = processor.transformToDocument(xml)
 
 var serializer = new XMLSerializer(); 
 var output = serializer.serializeToString(XmlDom.documentElement);
 
 var outputDiv = document.getElementById("Output");
 outputDiv.innerHTML = output;
}