var GENERAL = {};

GENERAL.menu = function(id) {
	if((id != null) && (id != "")) {
		var selector = "#que-navegacion #" + id;
		$(selector).addClass("activo");
	}
	$(selector).addClass("activo");
};

GENERAL.submenu = function(id) {
	if((id != null) && (id != "")) {
		var selector = "#que-navegacion-sub #" + id;
		$(selector).parent("ul").find(".navegasub0").after($(selector));
		$(selector).addClass("navegasub1");
	}
};

GENERAL.str_replace = function (search, replace, subject) {
	var f = search, r = replace, s = subject;
	var ra = r instanceof Array, sa = s instanceof Array, f = [].concat(f), r = [].concat(r), i = (s = [].concat(s)).length;
	while (j = 0, i--) {
		if (s[i]) {
			while (s[i] = (s[i]+'').split(f[j]).join(ra ? r[j] || "" : r[0]), ++j in f){};
		}
	};
	return sa ? s : s[0];
};

GENERAL.jq = function (myid) {
  return '#' + myid.replace(/(:|\.)/g,'\\$1');
};

GENERAL.checkEmail = function(email) {
	var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
	return filter.test(email) ? true : false;
};

GENERAL.ajaxPost = function(frm,funRespuesta,respuesta) {
	funRespuesta = ($.isFunction(funRespuesta)) ? funRespuesta : function() {void(0)};
	var target = frm.target;
	$("#ifr_ajax_post").remove();
	var iframeObj = $('<iframe name="ifr_ajax_post" id="ifr_ajax_post" src="about:blank" width="0" height="0" frameborder="0" scrolling="yes"/>');
	$("body").append(iframeObj);
	$(iframeObj).load(
		function() {
			respuesta = (respuesta != null) ? respuesta : $(this).get(0).contentWindow.document.body.innerHTML;
			frm.target = target;
			funRespuesta(respuesta);
			$("#ifr_ajax_post").remove();
		}
	);
	frm.target = "ifr_ajax_post";
	frm.submit();
};

GENERAL.timeHace = function(timestamp) {
	var salida = "";
	timestamp = parseInt(timestamp);
	if(!isNaN(timestamp)) {
		var ahora = new Date();
		var segundos = Math.floor(ahora.getTime()/1000) - timestamp;
		
		if (segundos > 60) {
			if(segundos < 3600) salida = "hace " + Math.floor(segundos/60) + " minutos";
			else if(segundos < 3600*2) salida = "hace una hora y " + Math.floor((segundos-3600)/60) + " minutos";
			else if(segundos < 3600*24) salida = "hace " + Math.floor(segundos/3600) + " horas";
			else if(segundos < 3600*48) salida = "hace un d�a y " + Math.floor((segundos-(3600*24))/3600) + " horas";
			else salida = "hace " + Math.floor(segundos/(3600*24)) + " d�as";
		}
		else {
			salida = 'Hace menos de un minuto';
		}
	}
	return salida;
};

GENERAL.console = function(texto) {
	if(typeof(console) != "undefined") console.log(texto);
};

GENERAL.in_array = function(needle, haystack, argStrict) {

    var key = '', strict = !!argStrict;

    if (strict) {
        for (key in haystack) {
            if (haystack[key] === needle) {
                return true;
            }
        }
    } else {
        for (key in haystack) {
            if (haystack[key] == needle) {
                return true;
            }
        }
    }

    return false;
};

GENERAL.reload = function() {
	
	var pagina = document.location.href;
	var v = 1;
	
	if (pagina.indexOf('anker_') != -1) {
		
		var campos = document.location.href.split('?');
		var campos2 = campos[1].split('_');
		
		v = parseInt(campos2[1]) + 1;
		pagina = campos[0];
	}
	
	urlreload = pagina + "?anker_" + v;
	
	window.location = urlreload;
}
