var USUARIOS = {};
USUARIOS.sesion = false;
USUARIOS.url_comunidad = "http://gente.que.es";
USUARIOS.preferencias = {};

USUARIOS.login = function() {
	$.ajax({type: 'GET', url: '/backend/USUARIOS.sesion.php', dataType: "json", async: false, success: function(datos) {if(typeof(datos.usuario) != "undefined") USUARIOS.sesion = datos}});
}
USUARIOS.login();

USUARIOS.relaciones = function(usuario,opciones) {
	var salida = null;
	opciones = (opciones != null) ? opciones : {};
	opciones.async = ((opciones.async != null) && (opciones.async == true)) ? true : false;
	opciones.success = ((opciones.success != null) && $.isFunction(opciones.success)) ? opciones.success : (function(xml) {salida = xml});
	$.ajax({type: "GET", url: "/backend/USUARIOS.relaciones.php", data: {usuario: usuario}, async: opciones.async, dataType: "xml", success: opciones.success});
	return salida;
}

USUARIOS.datos = function(usuario,opciones) {
	var salida = null;
	opciones = (opciones != null) ? opciones : {};
	opciones.async = ((opciones.async != null) && (opciones.async == true)) ? true : false;
	opciones.success = ((opciones.success != null) && $.isFunction(opciones.success)) ? opciones.success : (function(xml) {salida = xml});
	$.ajax({type: "GET", url: "/backend/USUARIOS.datos.php", data: {usuario: usuario}, async: opciones.async, dataType: "xml", success: opciones.success});
	return salida;
}

USUARIOS.datosHtml = function(usuario,destino,tamano,xml) {
	tamano = ((tamano == null) || ((tamano != "90x90") && (tamano != "56x56") && (tamano != "30x30"))) ? "30x30" : tamano;
	if($(destino).length == 1) {
		if(xml == null) {
			USUARIOS.datos(
				usuario,
				{
					async: true,
					success: function(xml) {
						if($("usuario avatar",xml).length == 1) {
							var nombre = ($("usuario nombre",xml).text() != "") ? $("usuario nombre",xml).text() : $("usuario id_usuario",xml).text();
							var avatar = ($("usuario avatar",xml).text() != "") ? $("usuario avatar",xml).text() : "generico.jpg";
							$("img.usuario_avatar",destino).attr("src",USUARIOS.url_comunidad + '/avatares/que/resize/' + tamano + '/' + avatar).attr("alt",nombre);
							$(".usuario_nombre",destino).html(nombre);
						}
					}
				}
			);
		}
		else {
			if($("usuario avatar",xml).length == 1) {
				var nombre = ($("usuario nombre",xml).text() != "") ? $("usuario nombre",xml).text() : $("usuario id_usuario",xml).text();
				var avatar = ($("usuario avatar",xml).text() != "") ? $("usuario avatar",xml).text() : "generico.jpg";
				$("img.usuario_avatar",destino).attr("src",USUARIOS.url_comunidad + '/avatares/que/resize/' + tamano + '/' + avatar).attr("alt",nombre);
				$(".usuario_nombre",destino).html(nombre);
			}
		}
	}
}

USUARIOS.personalizar = function(preferencias,anadir) {
	anadir = ((anadir != null) && (anadir == true)) ? 1 : 0;
	$.ajax(
		{
			type: 'GET', url: "/backend/USUARIOS.personalizar.php", async: false, data: {preferencias: $.toJSON(preferencias), anadir: anadir}, dataType: "json",
			success: function(data) {USUARIOS.preferencias = data;}
		}
	);
}

USUARIOS.perfilUsuario = function(usuario,destino,opciones) {
	usuario = (usuario != null) ? usuario : "";
	opciones = (opciones != null) ? opciones : {};
	opciones.info = (opciones.info != null) ? opciones.info : "";
	opciones.titulo = (opciones.titulo != null) ? opciones.titulo : "_usuario_";
	if((usuario != "") && ($(destino).length == 1)) {
		USUARIOS.datos(
			usuario,
			{
				async: true,
				success: function(xml) {
					if($("usuario ",xml).length == 1) {
						var nombre = ($("usuario nombre",xml).text() != "") ? $("usuario nombre",xml).text() : $("usuario id_usuario",xml).text();
						var avatar = ($("usuario avatar",xml).text() != "") ? $("usuario avatar",xml).text() : "generico.jpg";
						var salida_html =	'<div class="avatar M"><img src="' + USUARIOS.url_comunidad + '/avatares/que/resize/90x90/' + avatar + '" width="65" height="65" alt="' + nombre + '" class="usuario_avatar"/></div>' +
										 			'<p>' + opciones.titulo.replace(/_usuario_/g,'<a href="' + USUARIOS.url_comunidad + '/perfil.php?user=' + usuario + '" class="user-info usuario_nombre">' + usuario + '</a>') + '</p>';
						if(opciones.info != '')
							salida_html +=	'<span class="stats-user">' + opciones.info + '</span>';
						salida_html +=		'<a class="pulso" href="' + USUARIOS.url_comunidad + '/perfil.php?user=' + usuario + '&filtro=36-40">Pulso</a>';
										 			// '<span id="follow"><button type="button" id="seguir-usuario" class="btn"><span class="ico"></span> Seguir a este usuario</button></span>';
						$(destino).html(salida_html);
					}
				}
			}
		);
	}
}

$(document).ready(function() {
	USUARIOS.personalizar();
});
