var HOROSCOPO = {};
HOROSCOPO.signos = Array('aries', 'tauro', 'geminis', 'cancer', 'leo', 'virgo', 'libra', 'escorpio', 'sagitario', 'capricornio', 'acuario', 'piscis');
HOROSCOPO.signo = '';
HOROSCOPO.mes_activo = '';
HOROSCOPO.meses_calendario = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

HOROSCOPO.calendario = function(accion) {
	var calendario_html = '';
	var diaHoy = new Date();
	var mesHoy = diaHoy.getFullYear() + ((diaHoy.getMonth() < 9) ?  "0" + ((diaHoy.getMonth() + 1).toString()) : (diaHoy.getMonth() + 1).toString());
	if(((parseInt(HOROSCOPO.mes_activo) < parseInt(mesHoy)) && (accion == "+")) || ((parseInt(HOROSCOPO.mes_activo) > 200902) && (accion == "-"))) {

		var mes = (accion == "+") ? parseInt(HOROSCOPO.mes_activo.substring(4,6),10) + 1 : parseInt(HOROSCOPO.mes_activo.substring(4,6),10) - 1;
		var ano = parseInt(HOROSCOPO.mes_activo.substring(0,4));
		if(mes == 0) {
			mes = 12;
			ano = ano - 1;
		}
		if(mes == 13) {
			mes = 1;
			ano = ano + 1;
		}
		HOROSCOPO.mes_activo = ano.toString() + ((mes < 10) ? "0" + mes.toString() : mes.toString());
		var semanas = new Array();
		var diaInicio = new Date(ano,(mes - 1),1);
		var diaFin = new Date(ano,mes,0);
		var diaLimite = new Date(2009,3,23);
		for(var dia = diaInicio; dia <= diaFin; dia = new Date(dia.getFullYear(), dia.getMonth(), (dia.getDate() + 1))) {
			indiceSemana = ((dia.getDate() == 1) || (dia.getDay() == 1)) ? semanas.length : indiceSemana;
			if(typeof(semanas[indiceSemana]) != "undefined") {
				indiceDia = semanas[indiceSemana].length;
				semanas[indiceSemana][indiceDia] = dia;
			}
			else {
				semanas[indiceSemana] = new Array();
				semanas[indiceSemana][0] = dia;
			}
		}
		for(indiceSemana = 0; indiceSemana < semanas.length; indiceSemana++) {
			if(indiceSemana == 0) {
				for(indiceDia = 0; indiceDia < (7 - semanas[indiceSemana].length); indiceDia++) {
					dia = semanas[indiceSemana][indiceDia];
					calendario_html += '<span></span>';
				}
			}
			for(indiceDia = 0; indiceDia < semanas[indiceSemana].length; indiceDia++) {
				dia = semanas[indiceSemana][indiceDia];
				var fechaDia = dia.getFullYear().toString() + ((dia.getMonth() < 9) ? "0" + ((dia.getMonth() + 1).toString()) : (dia.getMonth() + 1).toString()) + ((dia.getDate() < 10) ? "0" + dia.getDate().toString() : dia.getDate().toString());
				calendario_html += ((dia >= diaLimite) && (dia < diaHoy)) ? '<span><a href="/horoscopo/' + fechaDia + '-'+HOROSCOPO.signo+'.html">' + dia.getDate() + '</a></span>' : '<span>' + dia.getDate() + '</span>';
			}
			if(indiceSemana == (semanas.length - 1)) {
				for(indiceDia = 0; indiceDia < (7 - semanas[indiceSemana].length); indiceDia++) {
					dia = semanas[indiceSemana][indiceDia];
					calendario_html += '<span></span>';
				}
			}
		}

		HOROSCOPO.signo += '';
		var signoVer = HOROSCOPO.signo.charAt(0).toUpperCase();
		signoVer = signoVer + HOROSCOPO.signo.substr(1);

		$("#mod_horoscopo").find("#contenido-calendario").find(".mes-anno").html(signoVer + ' ' + HOROSCOPO.meses_calendario[diaInicio.getMonth()] + " " + diaInicio.getFullYear());
		$("#mod_horoscopo").find("#contenido-calendario").find(".mes-anno .calendario_select").selectedIndex = diaInicio.getMonth();
		$("#mod_horoscopo .contenido-modulo-calendario .mod_calendario .cont_calendario").each(function(index) {
			if(index > 0) {
				$(this).html(calendario_html);
			}
		});
	}
};

$(document).ready(function() {
	for(var i=0; i<HOROSCOPO.signos.length; i++) {
		if(document.location.href.indexOf(HOROSCOPO.signos[i]) >= 0) {
			HOROSCOPO.signo = HOROSCOPO.signos[i];
			break;
		}
	}

	if ($("#mod_horoscopo").length) {
		if(HOROSCOPO.signo != '') {
			$("#mod_horoscopo").html($("#mod_horoscopo").html().replace('##signo##', HOROSCOPO.signo));
			HOROSCOPO.signo += '';
			var signoVer = HOROSCOPO.signo.charAt(0).toUpperCase();
			signoVer = signoVer + HOROSCOPO.signo.substr(1);
			$("#mod_horoscopo").html($("#mod_horoscopo").html().replace('##signo_ver##', signoVer));
			$("#mod_horoscopo").show();
		} else {
			$("#mod_horoscopo").hide();
		}
	}
});

