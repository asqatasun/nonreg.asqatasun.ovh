var COMENTARIOSPORTADA = {};

COMENTARIOSPORTADA.plantillas = new Array();

COMENTARIOSPORTADA.templates = new Array();

COMENTARIOSPORTADA.cargarPlantillas = function() {

	$.ajax({
	    type: 'GET',
        url: '/js/plantillas/comentarios-articulo.html',
        async: false,
        success: function(data) {
            html_comentarios = data;
        }
    });

	var plantillas = html_comentarios.split('###');

	COMENTARIOSPORTADA.templates['comentario'] = plantillas[0];
	COMENTARIOSPORTADA.templates['herramientas'] = plantillas[1];
	COMENTARIOSPORTADA.templates['form-denunciar'] = plantillas[2];
	COMENTARIOSPORTADA.templates['form-responder'] = plantillas[3];
	COMENTARIOSPORTADA.templates['comentario-respuesta'] = plantillas[4];
	COMENTARIOSPORTADA.templates['compartir'] = plantillas[5];

};

COMENTARIOSPORTADA.setNumComentariosPortada = function() {

		//GENERAL.console('COMENTARIOSPORTADA.setNumComentariosPortada');
		
		var html_comentarios = '';

		$.ajax({
		    type: 'GET',
            url: '/js/plantillas/comentarios-portada.html',
            async: false,
            success: function(data) {
                html_comentarios = data;
            }
        });

		var plantillas = html_comentarios.split('###');
		var destacados = new Array();

		var texto_comentar;
		var actividad;

		// Sacamos primero todos los destacados de una vez

		var datos = '';

		$('.qn_comentar').each(function(){

			var $padre = $(this);
			var id = GENERAL.str_replace('comentarios-', '', $(this).attr('id'));

			switch (true) {
				case $(this).hasClass('comentario_video'):
					actividad = 0;
					break;
				case $(this).hasClass('comentario_foto'):
					actividad = 40;
					break;
				case $(this).hasClass('comentario_galeria'):
					actividad = 0;
					break;
				case $(this).hasClass('comentario_blog'):
					actividad = 0;
					break;
				case $(this).hasClass('comentario_multimarcador'):
					actividad = 0;
					break;
				case $(this).hasClass('comentario_noticia'):
					actividad = 35;
					break;
				default:
					actividad = 35;
					break;
			}

			datos += datos ? ',' + id + '@' + actividad : id + '@' + actividad;
		});

		var destacado = $('<div></div>');

		$.ajax({
		    type: 'GET',
            url: '/backend/COMENTARIOS.getDestacadoTodos.php',
			data: {datos: datos},
            async: false,
			timeout: 100,
			dataType: 'json',
            success: function(resp) {

				if (resp.mensaje == 'OK') {

					for (var i = resp.destacados.length - 1; i >= 0; i--) {

						var t = $.template(plantillas[2]);
						var otros_array = [];
						if($.isArray(resp.destacados[i].otros) && (resp.destacados[i].otros.length > 0)) {
							for(var i_otros=0;i_otros<resp.destacados[i].otros.length;i_otros++) {
								otros_array.push('<a href="' + resp.destacados[i].url + '#comentarios' + resp.destacados[i].otros[i_otros].id_comentario + '">' + resp.destacados[i].otros[i_otros].nick + '</a>');
							}
						}

						destacado.html(t, {
							avatar: resp.destacados[i].avatar,
							autor: resp.destacados[i].autor,
							comentario: resp.destacados[i].comentario,
							tiempo: GENERAL.timeHace(resp.destacados[i].timestamp),
							display: 'block',
							id: resp.destacados[i].id_item,
							autor_enlace: USUARIOS.url_comunidad + '/perfil.php?user=' + resp.destacados[i].nick + '&do=yo',
							comentario_enlace: '/comentarios.html?id_item=' + resp.destacados[i].id_item + '&id_comentario=' + resp.destacados[i].comentario_id + '&user=' + resp.destacados[i].nick,
							imagen: (resp.destacados[i].imagen != "") ? '<img src="' + resp.destacados[i].imagen + '" height="50" style="float: right; margin-left: 4px;"/>' : "",
							otros: (otros_array.length > 0) ? '<p class="qn_mas_comentarios">M�s comentarios: ' + otros_array.join(", ") + '</p>' : ""
						});

						destacados[resp.destacados[i].id_item] = destacado.html();
					}
				}
            }
        });

		var texto_comentar_arr = new Array();

		$('.qn_comentar').each(function(){

			var $padre = $(this);
			var id = GENERAL.str_replace('comentarios-', '', $(this).attr('id'));

			switch (true) {
				case $(this).hasClass('comentario_video'):
					texto_comentar = 'Comenta este v�deo';
					break;
				case $(this).hasClass('comentario_foto'):
					texto_comentar = 'Comenta';
					break;
				case $(this).hasClass('comentario_galeria'):
					texto_comentar = 'Comenta esta galer�a';
					break;
				case $(this).hasClass('comentario_blog'):
					texto_comentar = 'Comenta este blog';
					break;
				case $(this).hasClass('comentario_multimarcador'):
					texto_comentar = 'Comenta este partido';
					break;
				case $(this).hasClass('comentario_noticia'):
					texto_comentar = 'Comenta esta noticia';
					break;
				default:
					texto_comentar = 'Comenta esta noticia';
					break;
			}

			var t = $.template(plantillas[0]);

			$(this).html(t, {
					enlace: ($(this).find('a').length > 0) ? $(this).find('a').attr('href') : $(this).parent().find('a').attr('href') + '#comentarios',
					id: id,
					texto_comentar: texto_comentar
				}
			);

			var destacado_html = '';

			if ($(this).hasClass('comentario_destacado')) {

				destacado_html = typeof(destacados[id]) != 'undefined' ? destacados[id] : '';
			}

			if ($(this).hasClass('comentario_formulario')) {

				var t = $.template(plantillas[1]);

				$padre.parent().append(t, {
					id: id,
					avatar: USUARIOS.sesion ? USUARIOS.sesion.avatar_30 : USUARIOS.url_comunidad + '/avatares/que/resize/30x30/generico.jpg',
					destacado: destacado_html
				});

				// Guardamos el formulario para utilizarlo despu�s

				COMENTARIOSPORTADA.plantillas[id] = $padre.parent().find('.qnf_adjuntar').html();
				$padre.parent().find('.qnf_adjuntar').data("adjuntar",false).data("tipo","").data("html","").data("datos",{});
			}

			texto_comentar_arr[id] = texto_comentar;
		});

		var id = '';
		// Los ids de las noticias los saco de todos los class=qn_comentar que haya en la pagina
		var arr = $('.qn_comentar');

		for(var i=arr.length - 1; i>=0; i--) {
			num_id = arr[i].id;

			if(num_id.indexOf('comentarios-') > -1) {
				arrAux = num_id.split('-');
				id = id + '|' + arrAux[1];
			}
		}

	 	if (id != '') {

			$.get("/backend/getComentariosPortada.php",
				{id: id, by: 'date', r: Math.random()},
				function(data) {
					if (data != '') {
						var campos = data.split('|');
						var limite = campos.length;
						var i = limite - 1;
						do {
							campos2 = campos[i].split('@');
							var num = campos2[1];

							var $num_comentarios = $(GENERAL.jq('num-comentarios-' + campos2[0]));
							var $comentarios = $(GENERAL.jq('comentarios-' + campos2[0]));

							if($num_comentarios.length) {

								if(num == 0 || num == '') {
									$num_comentarios.html('');
								} else if(num == 1) {
									$num_comentarios.html('1');
									$comentarios.html(GENERAL.str_replace(texto_comentar_arr[campos2[0]], 'Comentario', $comentarios.html()));
								} else {
									$num_comentarios.html(num);
									$comentarios.html(GENERAL.str_replace(texto_comentar_arr[campos2[0]], 'Comentarios', $comentarios.html()));
								}
							}
						}
					    while (i--);
					}
				}
			);
	   }

	setInterval('COMENTARIOSPORTADA.nuevos()', 120*1000);
};

COMENTARIOSPORTADA.nuevos = function() {

	var id = '';
	var arr = $('.qn_comentar');
	var num_comentarios_ant = new Array();

	for(var i = arr.length - 1; i>=0; i--) {

		num_id = arr[i].id;

		if(num_id.indexOf('comentarios-') > -1) {
			arrAux = num_id.split('-');
			id = id + '|' + arrAux[1];

			var $num = $(GENERAL.jq('num-' + arr[i].id));
			var $comentario = $(GENERAL.jq(arr[i].id));

			num_comentarios_ant[arr[i].id] = ($num.html() != '') ? $num.html() : 0;

			if ($comentario.find('.qn_nuevos').length == 0) {
				$comentario.html($comentario.html() + '<a class="qn_nuevos qnn_on" href="' + $comentario.find('a.num_comentarios').attr('href') + '"><span>2 Nuevos</span></a>');
			}
			else {
				$('.qn_nuevos').show();
				$('.qn_nuevos').addClass('qnn_on');
			}
		}
	}

	if (id != '') {

		$.get("/backend/getComentariosPortada.php",
			{id: id, by: 'date', r: Math.random()},
			function(data) {

				var campos = data.split('|');
				var limite = campos.length;
				var i = limite - 1;
				var campos2;
				do {
					campos2 = campos[i].split('@');
					var num = campos2[1];

					if (typeof(num_comentarios_ant['comentarios-' + campos2[0]]) != 'undefined') {

						if (campos2[1] > num_comentarios_ant['comentarios-' + campos2[0]]) {

							var nuevos = campos2[1] - num_comentarios_ant['comentarios-' + campos2[0]];

							$(GENERAL.jq('comentarios-' + campos2[0]) + ' > a.qn_nuevos.qnn_on').removeClass('qnn_on');
							$(GENERAL.jq('comentarios-' + campos2[0]) + ' > a.qn_nuevos > span').html(nuevos + ' Nuevos');

							$(GENERAL.jq('comentarios-' + campos2[0]) + ' > a.qn_nuevos > span').data('nuevos', nuevos);

							if (typeof(CONTENIDO) != 'undefined') {

								var nuevos = $(GENERAL.jq('comentarios-' + campos2[0]) + ' > a.qn_nuevos');

								nuevos.attr('href', 'javascript:;');
								nuevos.unbind('click');

								nuevos.data('idc', campos2[0]);

								$(GENERAL.jq('comentarios-' + campos2[0]) + ' > a.qn_nuevos').click(function(){

										var idc = $(this).data('idc');
										var n_comentarios = $(GENERAL.jq('num-comentarios-' + idc)).html() ? parseInt($(GENERAL.jq('num-comentarios-' + idc)).html()) : 0;

										$(GENERAL.jq('num-comentarios-' + idc)).html(n_comentarios + $('span', this).data('nuevos'));
										COMENTARIOSPORTADA.verMasComentarios(0, true);
										$(GENERAL.jq('comentarios-' + idc) + ' > a.qn_nuevos.qnn_on').hide();
									}
								);
							}
						}
						else {

							var nuevos = $(GENERAL.jq('comentarios-' + campos2[0]) + ' > a.qn_nuevos.qnn_on');

							nuevos.hide();
							nuevos.removeClass('qnn_on');
						}
					}
					else {

						var nuevos = $(GENERAL.jq('comentarios-' + campos2[0]) + ' > a.qn_nuevos');

						nuevos.hide();
						nuevos.removeClass('qnn_on');
					}
				}
			    while (i--);

				$('a.qn_nuevos.qnn_on').hide();
			}
		);
	}
};

COMENTARIOSPORTADA.elastic = function(id) {
	ta = $(GENERAL.jq('message-' + id))
	if(ta.attr('pulsado') == undefined || ta.attr('pulsado') == 0) {
		ta.attr('pulsado', 1);
		ta.val('');
		/*
		if (ta.data('recogido') != 1) {
			ta.val('');
		}
		else {
			ta.data('recogido', 0);
		}
		*/
		ta.css('height', '80px');
		//$(GENERAL.jq('comentarios-span-' + id)).find('.qnf_botones').show();
		$(GENERAL.jq('bot-publicar-' + id)).parent('.qnf_botones').show();
	}
	ta.elastic();
};

COMENTARIOSPORTADA.delastic = function(id) {

	var bot_publicar = GENERAL.jq('bot-publicar-' + id);
	var ta = $(GENERAL.jq('message-' + id));

	if(ta.data('recogido') == undefined || ta.data('recogido') == 0) {
		ta.data('recogido', 1);
		ta.attr('pulsado', 0);
		ta.css('height', '20px');
		$(bot_publicar).addClass('qnf_inactivo');
	}
}

COMENTARIOSPORTADA.elasticDenuncia = function(ta, id) {

	if (ta.attr('pulsado') == undefined || ta.attr('pulsado') == 0) {

		ta.attr('pulsado', 1);
		ta.val('');
		ta.css('height', '80px');

		var btn_cancelar = $('#bot-cancelar-' + id);

		btn_cancelar.click(function() {

			ta.attr('pulsado', 0);
			ta.val('Explica aqu� tu motivo para denunciar este comentario');
			ta.css('height', '40px');
			$('#ko-denunciar-' + id).hide();

		});
	}

	ta.elastic();

};

COMENTARIOSPORTADA.activaPublicar = function(id,tipo) {

	tipo = (typeof(tipo) == 'undefined' || tipo == '') ? 'portada' : tipo;

	var bot_publicar = GENERAL.jq('bot-publicar-' + id);

	$(bot_publicar).unbind('click');
	$(bot_publicar).attr('onclick', '');

	if(tipo == "portada") {

		if(1 == 1) {
			$(bot_publicar).removeClass('qnf_inactivo');
			$(bot_publicar).click(
				function() {
					
					var campos = id.toString().split('.');
					var noticia = 'http://' + window.location.host + $(GENERAL.jq('tension-' + campos[1] + '_' + campos[0]) + ' h1 a').attr('href');
					
					pageTracker._trackEvent('Proceso_comentario', 'Publicar', 'portada-' + noticia);
					
					COMENTARIOSPORTADA.guardar_comentario(id,"nuevo");
				}
			);
		}
		else {
			$(bot_publicar).addClass('qnf_inactivo');
		}
	}
	else {

		if(1 == 1) {
			$(bot_publicar).removeClass('qnf_inactivo');
			$(bot_publicar).click(
				function() {

					// Desactivamos el bot�n al pulsar para que evitar la multipublicaci�n

					$(this).addClass('qnf_inactivo');
					$(this).unbind('click');
					$(this).attr('onclick', '');
					
					pageTracker._trackEvent('Proceso_comentario', 'Publicar', 'articulo-' + document.location.href);
					
					if (USUARIOS.sesion) {
						COMENTARIOSPORTADA.enviar_comentario(id,tipo);
					}
					else {
						var enviarComentario = function() {COMENTARIOSPORTADA.enviar_comentario(id,"nuevo")}
						LOGIN.login(enviarComentario);
					}
				}
			);
		}
		else {
			$(bot_publicar).addClass('qnf_inactivo');
		}
	}
};

COMENTARIOSPORTADA.guardar_comentario = function(id) {

	var campos = id.toString().split('.');
	var adjuntar_jq = $(GENERAL.jq('adjuntar-' + id));

	$.post('/backend/COMENTARIOS.guardar_comentario.php',
		{
			message: $(GENERAL.jq('comentarios-span-' + id) + ' textarea').val(),
			entradilla: $(GENERAL.jq('tension-' + campos[1] + '_' + campos[0]) + ' .qn_entradilla').html(),
			titulo: $(GENERAL.jq('tension-' + campos[1] + '_' + campos[0]) + ' :header a').html(),
			foto: $(GENERAL.jq('tension-' + campos[1] + '_' + campos[0]) + ' .qn_media img').length ? $(GENERAL.jq('tension-' + campos[1] + '_' + campos[0]) + ' .qn_media img').attr('src') : '',
			adjuntar: adjuntar_jq.data("adjuntar") ? 1 : 0,
			adjuntar_tipo: adjuntar_jq.data("tipo"),
			adjuntar_html: adjuntar_jq.data("html"),
			adjuntar_foto_foto: (typeof(adjuntar_jq.data("datos").foto) != "undefined") ? adjuntar_jq.data("datos").foto : "",
			adjuntar_foto_nombre: (typeof(adjuntar_jq.data("datos").nombre) != "undefined") ? adjuntar_jq.data("datos").nombre : "",
			adjuntar_foto_contento: (typeof(adjuntar_jq.data("datos").contento) != "undefined") ? adjuntar_jq.data("datos").contento : ""

		},
		function(data) {
			if (data.mensaje == 'OK') {
				window.location = $(GENERAL.jq('comentarios-' + id) + ' a.num_comentarios').attr('href');
			}
		},
		'json'
	);

};

COMENTARIOSPORTADA.enviar_comentario_portada = function() {

	$.post('/backend/COMENTARIOS.session.php',
		{},
		function(resp){

			$('#message-' + CONTENIDO.id_contenido).val(resp.message);

			var adjuntar = $('#adjuntar-' + CONTENIDO.id_contenido);

			adjuntar.data('adjuntar', resp.adjuntar);
			adjuntar.data('tipo', resp.adjuntar_tipo);

			if (resp.adjuntar) {
				if (resp.adjuntar_tipo == 'foto') {
					adjuntar.data('datos', {"nombre": resp.adjuntar_foto_nombre , "foto": resp.adjuntar_foto_foto , "contento": resp.adjuntar_foto_contento });
				}
			}

			COMENTARIOSPORTADA.enviar_comentario(CONTENIDO.id_contenido, 'articulo');

		},
		'json'
	);
};

COMENTARIOSPORTADA.enviar_comentario = function(id,tipo) {
	tipo = ((tipo == null) || (tipo != "respuesta")) ? "nuevo" : "respuesta";

	var theDate = new Date();
	var submit_time = theDate.valueOf();

	var message_id = 'message-' + id;
	var message = $('#' + message_id).val();
	if(message == "") {
		alert("No has insertado el comentario.");
		return;
	}
	var adjuntar_id = 'adjuntar-' + id;
	var adjuntar = $(GENERAL.jq(adjuntar_id));

	if(adjuntar.data("adjuntar")) {
		if(adjuntar.data("tipo") == "foto") {
			var datos = {};
			var datos_foto = adjuntar.data("datos");
			datos.foto = datos_foto.foto;
			datos.nombre = datos_foto.nombre;
			datos.titulo = CONTENIDO.titulo;
			datos.texto = $('#message' + id).val();
			datos.id_seccion = CONTENIDO.id_seccion;
			datos.usuario = USUARIOS.sesion ? USUARIOS.sesion.usuario : 'anonimo';
			datos.contento = datos_foto.contento;

			foto = COMENTARIOSPORTADA.subir_foto(datos);
			adjuntar.data("html",'<img src="' + foto + '"/>');
		}

		if(typeof(adjuntar.data('html')) != 'undefined' && adjuntar.data("html") != "") message = adjuntar.data("html") + '<br>' + message;
	}
	
	var entradilla = (typeof(CONTENIDO.entradilla) != 'undefined') ? CONTENIDO.entradilla : '';
	entradilla = (entradilla.length > 100) ? entradilla.substring(0, 100) : entradilla;

	$.post('/backend/COMENTARIOS.enviar_comentario.php',
		{
			titulo: CONTENIDO.titulo,
			url: (typeof(CONTENIDO.url_noticia) != 'undefined') ? CONTENIDO.url_noticia : document.location.href,
			message: message,
			id_item: (tipo == "nuevo") ? CONTENIDO.id_comentarios : 'c.' + id,
			load_time: CONTENIDO.load_time,
			submit_time: submit_time,
			entradilla: entradilla,
			timestamp_noticia: (typeof(CONTENIDO.timestamp) != 'undefined') ? CONTENIDO.timestamp_noticia : CONTENIDO.load_time,
			/*author_email: USUARIOS.sesion ? USUARIOS.sesion.email : 'An�nimo',*/
			tipo: (typeof(CONTENIDO.tipo) != 'undefined') ? CONTENIDO.tipo : '',
			foto: ($('#media-contenedor img').length ? $('#media-contenedor img').attr('src') : ''),
			ajax: 1,
			debug: 0
		},
		function (xml) {

			if ($(xml).find('msg').attr('id') == 200) {

				var comentario = {};

				comentario.id = $(xml).find('log').attr('id');
				comentario.avatar = USUARIOS.sesion ? USUARIOS.sesion.avatar_30 : USUARIOS.url_comunidad + '/avatares/que/resize/30x30/generico.jpg';
				comentario.enlace = '/comentarios.html?id_item=' + CONTENIDO.id_comentarios + '&id_comentario=' + $(xml).find('log').attr('id') + '&user=' + USUARIOS.sesion.usuario;
				comentario.message = message;
				comentario.id_comentario = $(xml).find('comentario_id').text();
				
				if (tipo == "respuesta") {
					COMENTARIOSPORTADA.respuestaComentario(comentario, id);
					$('#form-responder-' + id).toggle('slow');
				}
				else {
					
					var n_comentarios = $(GENERAL.jq('num-comentarios-' + CONTENIDO.id_comentarios)).html() ? parseInt($(GENERAL.jq('num-comentarios-' + CONTENIDO.id_comentarios)).html()) : 0;
					$(GENERAL.jq('num-comentarios-' + CONTENIDO.id_comentarios)).html(n_comentarios + 1);

					COMENTARIOSPORTADA.nuevoComentario(comentario);
				}

				// Facebook

				var datos = {};

				datos.msg = message;
				datos.attach_name = CONTENIDO.titulo;
				datos.attach_url = (typeof(CONTENIDO.url_noticia) != 'undefined') ? CONTENIDO.url_noticia : document.location.href;
				datos.attach_body = CONTENIDO.entradilla;

				var foto = $('#articulo-pbox > #articulo-media > img');

				if (foto.length) {
					datos.attach_img_src = foto.attr('src');
					datos.attach_img_url = window.location.href;
				}

				if ($('.qnf_enviaFB input[type=checkbox]').attr('checked')) {
					COMENTARIOSPORTADA.fbcomentarios(datos);
				}

				// Limpiamos el formulario
				
				$('.qnf_enviaFB input[type=checkbox]').attr('checked', '');
				
				var ta = $('#' + message_id);
				
				ta.val('');
				ta.css('height', '20px');
				
				COMENTARIOSPORTADA.adjuntar_cancelar(id);

			}
			else {
				alert('Error al enviar el comentario');
			}
		}
		,'xml'
	);
};

COMENTARIOSPORTADA.nuevoComentario = function(comentario) {

	var contenido = $('<div id="comentario_' + comentario.id + '" class="que-articulo-comentarios"></div>');
	var t;

	t = $.template(COMENTARIOSPORTADA.templates['comentario']);

	contenido.html(t, {
		id: 'comentario-comp-' + comentario.id,
		avatar: comentario.avatar,
		enlace_usuario: USUARIOS.url_comunidad + '/perfil.php?user=' + USUARIOS.sesion.usuario + '&amp;do=yo',
		usuario: USUARIOS.sesion.usuario,
		nombre: USUARIOS.sesion.nombre ? USUARIOS.sesion.nombre : USUARIOS.sesion.usuario,
		timestamp: 'Hace menos de un minuto',
		enlace_comentario: comentario.enlace,
		comentario: comentario.message
	});

	t = $.template(COMENTARIOSPORTADA.templates['herramientas']);

	contenido.append(t, {
		id: comentario.id,
		responder: ''
	});

	t = $.template(COMENTARIOSPORTADA.templates['form-denunciar']);

	contenido.append(t, {
		id: comentario.id,
		avatar: USUARIOS.sesion.avatar_30,
		load_time: CONTENIDO.load_time,
		id_comentario: comentario.id_comentario
	});

	t = $.template(COMENTARIOSPORTADA.templates['form-responder']);

	contenido.append(t, {
		id: comentario.id,
		avatar: USUARIOS.sesion.avatar_30,
		tipo: 'respuesta'
	});

	$('#nuevo_comentario').after(contenido);

	HERRAMIENTAS.gustaComentarios('', comentario.id);
	COMENTARIOSPORTADA.compartir(comentario.id);

};

COMENTARIOSPORTADA.respuestaComentario = function(comentario, id) {

	var contenido = $('<div class="qnf_respondiendo"></div>');
	var t;

	t = $.template(COMENTARIOSPORTADA.templates['comentario-respuesta']);

	contenido.html(t, {
		avatar: comentario.avatar,
		enlace_usuario: USUARIOS.url_comunidad + '/perfil.php?user=' + USUARIOS.sesion.usuario + '&amp;do=yo',
		usuario: USUARIOS.sesion.usuario,
		nombre: USUARIOS.sesion.nombre ? USUARIOS.sesion.nombre : USUARIOS.sesion.usuario,
		timestamp: 'Hace menos de un minuto',
		enlace_comentario: comentario.enlace,
		comentario: comentario.message
	});

	t = $.template(COMENTARIOSPORTADA.templates['herramientas']);

	contenido.append(t, {
		id: comentario.id,
		responder: 'style="display:none"'
	});

	t = $.template(COMENTARIOSPORTADA.templates['form-denunciar']);

	contenido.append(t, {
		id: comentario.id,
		avatar: USUARIOS.sesion.avatar_30,
		load_time: CONTENIDO.load_time,
		id_comentario: comentario.id_comentario
	});

	$('#comentario_' + id).append(contenido);
};

COMENTARIOSPORTADA.adjuntar_pestana = function(tipo) {
	var campos = tipo.split('-');
	var id = (typeof(campos[1]) != 'undefined') ? campos[1] : CONTENIDO.id_contenido;
	var plantilla = (typeof(campos[1]) != 'undefined') ? COMENTARIOSPORTADA.plantillas[id] : CONTENIDO.plantilla;
	$(".bloque-enlaces",$(GENERAL.jq('adjuntar-' + id))).hide();
	$(GENERAL.jq('adjuntar-' + id) + ' .adjuntar_contenido').empty().hide();
	$(GENERAL.jq('adjuntar-' + id) + ' .adjuntar_formulario').show();
	$(GENERAL.jq('op-' + tipo)).css("display","block");
};

COMENTARIOSPORTADA.adjuntar_cancelar = function(id) {
	id = (typeof(id) != 'undefined') ? id : CONTENIDO.id_contenido;
	var plantilla = (typeof(id) != 'undefined') ? COMENTARIOSPORTADA.plantillas[id] : CONTENIDO.plantilla;
	$(GENERAL.jq('adjuntar-' + id) + ' .qnf_caja').hide();
	$(GENERAL.jq('adjuntar-' + id) + ' .adjuntar_contenido').empty().hide();
	$(GENERAL.jq('adjuntar-' + id) + ' .adjuntar_formulario').show();
	$(GENERAL.jq('foto-' + id)).val("");
	$(GENERAL.jq('video-' + id)).val("");
	$(GENERAL.jq('enlace-' + id)).val("");
	$(".bloque-enlaces",$(GENERAL.jq('adjuntar-' + id))).show();
	$(GENERAL.jq('adjuntar-' + id)).data("adjuntar",false).data("tipo","").data("html","").data("datos",{});
};

COMENTARIOSPORTADA.adjuntar_foto = function(id, resp) {

	if ($(GENERAL.jq('foto-' + id)).val() == '') {
		alert('Por favor, selecciona una foto de tu ordenador');
		return;
	}

	var formulario = $(GENERAL.jq('comentarios-span-' + id)).length ? $(GENERAL.jq('comentarios-span-' + id) + ' .qn_form') : $('div.qn_form form.qn_form');
	var adjuntar = $(GENERAL.jq('adjuntar-' + id));
	var pestana = $(GENERAL.jq('op-foto-' + id));
	var ko = $(GENERAL.jq('comentarios-span-' + id)).length ? $(GENERAL.jq('comentarios-span-' + id) + ' #foto-ko') : $('div.qn_form #foto-ko');

	formulario = (typeof(resp) != 'undefined') ? $('#form-responder-' + id + ' form.qn_form') : formulario;

	formulario.attr('method', 'post');
	formulario.attr('enctype', 'multipart/form-data');
	formulario.attr('action', '/backend/COMENTARIOS.adjuntar_foto.php');

	GENERAL.ajaxPost(formulario.get(0), function (data) {
			eval('var resp = ' + data);
			if (resp.exito == 'OK') {
				ko.hide();
				var html = '<img src="' + resp.thumb + '"/><br>';
				$(".adjuntar_formulario",pestana).hide();
				$(".adjuntar_contenido",pestana).html(html).show();
				adjuntar.data("adjuntar",true);
				adjuntar.data("tipo","foto");
				adjuntar.data("html","");
				adjuntar.data("datos",{"nombre": resp.nombre, "foto": resp.foto, "contento": resp.contento});
				formulario.attr('method', '');
				formulario.attr('enctype', '');
				formulario.attr('action', '');
			}
			else {
				ko.html(resp.msg);
				ko.show();
			}
		}
	);
};

COMENTARIOSPORTADA.subir_foto = function(datos) {
	var foto = false;
	$.ajax({
		type: 'POST',
		url: '/backend/COMENTARIOS.subir_foto.php',
		data: {
			foto: datos.foto,
			nombre: datos.nombre,
			titulo: datos.titulo,
			id_seccion: datos.id_seccion,
			texto: datos.texto,
			usuario: datos.usuario,
			contento: datos.contento
		},
        async: false,
		dataType: 'json',
        success: function(data) {
			if(data.exito == 'OK') foto = data.foto;
		}
	});
	return foto;
};

COMENTARIOSPORTADA.adjuntar_video = function(id) {

	var video_id = (typeof(id) == 'undefined') ? 'video' : 'video-' + id;
	var adjuntar = $(GENERAL.jq('adjuntar-' + id));

	if ($(GENERAL.jq(video_id)).val() == '') {
		alert('Por favor, copia un v�deo o una URL de un v�deo');
		return;
	}

	$.post('/backend/COMENTARIOS.adjuntar_video.php',
		{
			datos: $(GENERAL.jq(video_id)).val()
		},
		function(data) {
			if (data.mensaje == 'OK') {

				$(".adjuntar_formulario",$(GENERAL.jq('op-' + video_id))).hide();
				$(".adjuntar_contenido",$(GENERAL.jq('op-' + video_id))).html(data.objeto).show();
				adjuntar.data("adjuntar",true);
				adjuntar.data("tipo","video");
				adjuntar.data("html",data.objeto);
				adjuntar.data("datos",{});

				if (typeof(CONTENIDO) != 'undefined') CONTENIDO.video = 1;
			}
		},
		'json'
	);

};

COMENTARIOSPORTADA.adjuntar_enlace = function(id) {

	var enlace_id = (typeof(id) == 'undefined') ? 'enlace' : 'enlace-' + id;
	var adjuntar = $(GENERAL.jq('adjuntar-' + id));

	if ($(GENERAL.jq(enlace_id)).val() == '') {
		alert('Por favor, introduce la URL del enlace');
		return;
	}

	$.post('/backend/COMENTARIOS.adjuntar_enlace.php',
		{
			url: $(GENERAL.jq(enlace_id)).val()
		},
		function(data) {

			if(data.titulo == "") {
				alert("No se ha podido recoger informaci�n del enlace.");
				return;
			}

			var html = '<a href="' + $(GENERAL.jq(enlace_id)).val() + '">' + data.titulo + '</a>' + ((data.imagen != "") ? '<img src="' + data.imagen + '" style="width:120px"/>' : '');
			var html_pulso = '<a href="' + $(GENERAL.jq(enlace_id)).val() + '">' + data.titulo + '</a>' + ((data.imagen != "") ? '<img src="' + data.imagen + '" style="width:272px"/>' : '');

			$(".adjuntar_formulario",$(GENERAL.jq('op-' + enlace_id))).hide();
			$(".adjuntar_contenido",$(GENERAL.jq('op-' + enlace_id))).html(html).show();
			adjuntar.data("adjuntar",true);
			adjuntar.data("tipo","enlace");
			adjuntar.data("html",html_pulso);
			adjuntar.data("datos",{});
		},
		'json'
	);

};

COMENTARIOSPORTADA.denunciarComentario = function(id, id_l) {

	if ($('#denuncia-' + id).val() == '') {
		alert('Por favor, escribe el motivo de la denuncia');
		return;
	}

	var theDate = new Date();
	var submit_time = theDate.valueOf();

	$.post('/backend/COMENTARIOS.denunciar_comentario.php',
		{
			load_time: $('#denuncia-load-time-' + id).val()
			,submit_time: submit_time
		    ,id_comentario: id
	        ,denuncia: $('#denuncia-' + id).val()
		},
		function(data) {

			if (data.mensaje == 'OK') {
				$('#form-denunciar-' + id_l).toggle('slow');
				$('#denuncia-' + id).val('Explica aqu� tu motivo para denunciar este comentario');
			}
			else {
				$('#ko-denunciar-' + id_l).html('No se ha podido enviar la denuncia int�ntelo m�s tarde.');
				$('#ko-denunciar-' + id_l).show();
			}
		},
		'json'
	);

};


COMENTARIOSPORTADA.compartir = function(id) {

	if (typeof(id) != 'undefined') {

		var enlace = $('#comentario-comp-' + id + ' a.pagina_final');

		var contenido = $('<div></div>');
		var t;

		t = $.template(COMENTARIOSPORTADA.templates['compartir']);

		contenido.html(t, {
			id: id,
			email: USUARIOS.sesion ? USUARIOS.sesion.email : 'Mi correo electr�nico',
			url: 'http://' + window.location.host + enlace.attr('href'),
			texto: enlace.text()
		});

		$('#compartir-' + id).html(contenido.html());
	}

};

COMENTARIOSPORTADA.verMasComentarios = function(inicio, nuevos) {

	// deshabilitamos el bot�n para evitar varias pulsaciones
	
	$('.qac_vermascomentarios').find('a').attr('onclick', '');
	$('.qac_vermascomentarios').find('a').unbind('click');
	
	$.getJSON('http://comentarios.que.es/xml/get.php?id_item=' + CONTENIDO.id_comentarios + '&by=date&formato=json&callback=?',
		function(data) {

			var comentarios = new Array();

			$.each(data.comments, function(i, item) {
					comentarios[item.id] = 1;
				}
			);
			
			switch(CONTENIDO.id_tipo) {
				case 12: 
					var actividad = 35;
					break;
				case 26:
					var actividad = 40;
					break;
				default:
					var actividad = 35
					break;
			}

			$.get('/backend/COMENTARIOS.getComentariosPulso.php',
				{
					id_item: CONTENIDO.id_comentarios,
					start: inicio,
					actividad: actividad,
					limit: 1000,
					r: Math.random()
				},
				function(xml) {

					var html = '';
					var i = 0;
					var desp = 0;

					$(xml).find('log').each(function() {

							desp++;

							if (comentarios[$(this).find('comentario_id').text()]) {

								var datos = {};

								datos.id = $(this).attr('id');
								datos.avatar = $(this).find('avatar').text();
								datos.nick_usuario = $(this).find('nick_usuario').text();
								datos.nombre = $(this).find('nombre').text();
								datos.id_usuario_txt = $(this).find('id_usuario_txt').text();
								datos.comentario_id = $(this).find('comentario_id').text();
								datos.id_item = CONTENIDO.id_comentarios;
								datos.descripcion = $(this).find('descripcion').text();
								datos.timestamp = GENERAL.timeHace($(this).find('timestamp').text());

								html += COMENTARIOSPORTADA.comentarios(datos);

								i++;

								if (i == 5) return false;
							}
						}
					);
						
					if (html != '') {

						if (typeof(nuevos) != 'undefined') {
							$('.que-articulo-comentarios').remove();
							$('.qnf_responder').remove();
							$('#articulo-sidebar script').remove();
							$(GENERAL.jq('comentarios-' + CONTENIDO.id_comentarios) + ' .qn_nuevos').hide();
							$('#nuevo_comentario').after(html);
						}

						if (desp > 0) {
							$('.qac_vermascomentarios').before(html);

							$('.qach_compartir').each(function(){

									var id = GENERAL.str_replace('compartir-', '', $(this).attr('id'));

									HERRAMIENTAS.gustaComentarios('', id);
									COMENTARIOSPORTADA.compartir(id);
								}
							);

							$('.qac_vermascomentarios').find('a').attr('onclick', '');
							$('.qac_vermascomentarios').find('a').unbind('click');
							$('.qac_vermascomentarios').find('a').click(function() {
									COMENTARIOSPORTADA.verMasComentarios(inicio + desp);
								}
							);
							
							COMENTARIOSPORTADA.quitarEnlaceVideosComentarios();
						}
					
						// Ocultamos el bot�n de Ver m�s comentarios
						if (desp < 5 || ( inicio + 5) == data.total_comments){
							$('.qac_vermascomentarios').hide();
						}
					}
					else {
						
						// Mostramos error al actualizar
						
						if (typeof(nuevos) != 'undefined') {
							$('#ko-vermas').fadeIn('slow', function() {
									$('#ko-vermas').fadeOut('slow');
								}
							);
						}
						else {
							$('#ko-nuevos').fadeIn('slow', function() {
									$('#ko-nuevos').fadeOut('slow');
								}
							);
						}
					}
				}
				,'xml'
			);
		}
	);
};

COMENTARIOSPORTADA.getRespuestas = function(id) {

	var comentarios = new Array();
	var html = '';

	return html;

	$.ajax({
		url: 'http://comentarios.que.es/xml/get.php?id_item=' + id + '&by=date&formato=json&callback=?',
	  	dataType: 'json',
	  	async: false,
	  	success: function(data) {
			$.each(data.comments, function(i, item) {
					comentarios[item.id] = 1;
				}
			);
		}
	});

	$.ajax({
	 	url: '/backend/COMENTARIOS.getComentariosPulso.php?id_item=' + id,
	  	async: false,
		dataType: 'xml',
		success: function(xml) {
			$(xml).find('log').each(function() {
					if (comentarios[$(this).find('comentario_id').text()]) {

						var datos = {};

						datos.id_padre = id;
						datos.id = $(this).attr('id');
						datos.avatar = $(this).find('avatar').text();
						datos.nick_usuario = $(this).find('id_usuario_txt').text();
						datos.nombre = $(this).find('nombre').text() ? $(this).find('nombre').text() : $(this).find('nick_usuario').text();
						datos.comentario_id = $(this).find('comentario_id').text();
						datos.id_item = CONTENIDO.id_comentarios;
						datos.descripcion = $(this).find('descripcion').text();
						datos.timestamp = GENERAL.timeHace($(this).find('timestamp').text());

						html += COMENTARIOSPORTADA.respuestas(datos);
					}
				}
			);
		}
	});

	return html;
};

COMENTARIOSPORTADA.respuestas = function(datos) {

	var theDate = new Date();
	var load_time = theDate.valueOf();

	var contenido = $('<div class="qnf_respondiendo"></div>');
	var t;

	t = $.template(COMENTARIOSPORTADA.templates['comentario-respuesta']);

	contenido.html(t, {
		avatar: datos.avatar,
		enlace_usuario: USUARIOS.url_comunidad + '/perfil.php?user=' + datos.id_usuario_txt + '&amp;do=yo',
		usuario: datos.nick_usuario,
		nombre: datos.nombre ? datos.nombre : datos.nick_usuario,
		timestamp: datos.timestamp,
		enlace_comentario: '/comentarios.html?id_item=' + CONTENIDO.id_comentarios + '&id_comentario=' + datos.id_padre + '&user=' + datos.nick_usuario,
		comentario: datos.descripcion
	});

	t = $.template(COMENTARIOSPORTADA.templates['herramientas']);

	contenido.append(t, {
		id: datos.id,
		responder: 'style="display:none"'
	});

	t = $.template(COMENTARIOSPORTADA.templates['form-denunciar']);

	contenido.append(t, {
		id: datos.id,
		avatar: USUARIOS.sesion ? USUARIOS.sesion.avatar_30 : '/img/avatars/avatar-defecto.png',
		load_time: load_time,
		id_comentario: datos.comentario_id
	});

	var html = $('<div><div>');
	html.append(contenido);

	return html.html();
};

COMENTARIOSPORTADA.comentarios = function(datos) {

	var theDate = new Date();
	var load_time = theDate.valueOf();

	var contenido = $('<div id="comentario_' + datos.id + '" class="que-articulo-comentarios"></div>');
	var t;

	t = $.template(COMENTARIOSPORTADA.templates['comentario']);

	contenido.html(t, {
		id: 'comentario-comp-' + datos.id,
		avatar: datos.avatar,
		enlace_usuario: USUARIOS.url_comunidad + '/perfil.php?user=' + datos.id_usuario_txt + '&amp;do=yo',
		usuario: datos.nick_usuario,
		nombre: datos.nombre ? datos.nombre : datos.nick_usuario,
		timestamp: datos.timestamp,
		enlace_comentario: '/comentarios.html?id_item=' + CONTENIDO.id_comentarios + '&id_comentario=' + datos.id + '&user=' + datos.nick_usuario,
		comentario: datos.descripcion
	});

	t = $.template(COMENTARIOSPORTADA.templates['herramientas']);

	contenido.append(t, {
		id: datos.id,
		responder: ''
	});

	t = $.template(COMENTARIOSPORTADA.templates['form-denunciar']);

	contenido.append(t, {
		id: datos.id,
		avatar: USUARIOS.sesion ? USUARIOS.sesion.avatar_30 : '/img/avatars/avatar-defecto.png',
		load_time: load_time,
		id_comentario: datos.comentario_id
	});

	// Sacamos las respuestas al comentario

	contenido.append(COMENTARIOSPORTADA.getRespuestas('c.' + datos.id));

	t = $.template(COMENTARIOSPORTADA.templates['form-responder']);

	contenido.append(t, {
		id: datos.id,
		avatar: USUARIOS.sesion ? USUARIOS.sesion.avatar_30 : '/img/avatars/avatar-defecto.png'
	});

	var html = $('<div><div>');
	html.append(contenido);

	return html.html();

};


COMENTARIOSPORTADA.fbcomentarios = function(datos) {

		var msg = datos.msg;
		var attach_name = datos.attach_name;
		var attach_url = datos.attach_url;
		var attach_body = datos.attach_body;

		if ($('.pbox > .photo > img').length) {
			var attach_img_src = $('.pbox > .photo > img').attr('src');
			var attach_img_url = window.location.href;
		}

		var attachment = {
			'name': attach_name ,
			'href': attach_url,
			'description': attach_body,
			'media': (typeof attach_img_src == undefined) ? null : [{
				'type':'image',
				'src':attach_img_src,
				'href':attach_img_url
			}]
		};

		var action_link = [{
			'text': 'Leer noticia',
			'href': window.location.href
		}];

		var user_message_prompt = 'Tu comentario:';
		var auto_publish = 'true';

		FB.ensureInit(function () {
			//FB.Connect.streamPublish(msg, attachment, action_link,null,user_message_prompt,commentCallback,auto_publish);
			FB.Connect.streamPublish(msg, attachment, action_link, null, user_message_prompt);
		});
};


COMENTARIOSPORTADA.enviar = function(frm, id) {

	var enlace = $('#comentario-comp-' + id + ' a.pagina_final');

	var email_remitente = frm.email_remitente.value;
	var email_destinatario = frm.email_destinatario.value;
	var comentario = frm.comentario.value;
	if (!email_remitente || !email_destinatario) {
		alert('No ha rellenado todos los campos');
		return false;
	}
	if (!GENERAL.checkEmail(email_remitente)) {
		alert('El email del remitente no es correcto');
		frm.email_remitente.focus();
		return false;
	}
	if (!GENERAL.checkEmail(email_destinatario)) {
		alert('El email del destinatario no es correcto');
		frm.email_destinatario.focus();
		return false;
	}
	$.getJSON(
		"/backend/HERRAMIENTAS.enviar.php",
		{email_remitente: email_remitente, email_destinatario: email_destinatario, comentario: comentario, url: 'http://' + window.location.host + enlace.attr('href')},
		function(datos) {
			if(datos.msg != "") alert(datos.msg);
			if(datos.exito == "OK") $('#compartir-' + id).removeClass("activo");
		}
	);
}

COMENTARIOSPORTADA.desarrollo = function() {
	alert('Funcionalidad a�n en desarrollo');
};

COMENTARIOSPORTADA.quitarEnlaceVideosComentarios = function () {
	
	$('.pagina_final').each(function() {
			
			var pagina_final = $(this);
			//GENERAL.console(pagina_final);
			pagina_final.before(pagina_final.find('object').get(0));
			//GENERAL.console(pagina_final.find('object').get(0));
			pagina_final.find('object').remove();
		}
	);
};

COMENTARIOSPORTADA.cargarPlantillas();

$(document).ready(function() {
		
		$('li.qach_responder').hide();
		
		// Quitamos los enlaces de los v�deos
		
		COMENTARIOSPORTADA.quitarEnlaceVideosComentarios();
	}
);
