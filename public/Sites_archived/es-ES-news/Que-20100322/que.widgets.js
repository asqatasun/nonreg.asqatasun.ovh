var WIDGETS = {};
WIDGETS.pagActual = 1;
WIDGETS.paginacion = 8; // De 8 en 8

////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// FUNCIONES COMUNES WIDGETS ////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
WIDGETS.pagina = function (numpag) {
	WIDGETS.pagActual = numpag;
	var inicio = (numpag - 1)*WIDGETS.paginacion + 1;
	var numPaginas = Math.ceil($('.widget').length / WIDGETS.paginacion);

	/* paginas 1, 2, 3, ..... */
	$('[id*=widgets_]').each(function(index) {
		if(index + 1 == numpag) {
			$(this).addClass("activo");
		} else {
			$(this).removeClass("activo");
		}
	});


	/* iconos de los widgets */
	$(".widget").each(function(index) {
		indice = index + 1;
		if(indice >= inicio && indice < (inicio + WIDGETS.paginacion)) {
			$(this).show();
		} else {
			$(this).hide();
		}
	});
	/* botones */
	if(numpag > 1) {
		$("#widgets #caja .pagination .pag-menos").show();
	} else {
		$("#widgets #caja .pagination .pag-menos").hide();
	}

	if(numpag < numPaginas) {
		$("#widgets #caja .pagination .pag-mas").show();
	} else {
		$("#widgets #caja .pagination .pag-mas").hide();
	}
};

WIDGETS.botones = function (masmenos) {
	if(masmenos == '+') {
		WIDGETS.pagActual = WIDGETS.pagActual + 1;
	} else {
		WIDGETS.pagActual = WIDGETS.pagActual - 1;
	}
	WIDGETS.pagina(WIDGETS.pagActual);
};

WIDGETS.aniadir = function(idwidget, personalizacion, columna) {
	if(personalizacion == null) {
		personalizacion = true;
	}
	if(columna == null) {
		columna = 'A';
	}
	$("#wid_" + idwidget).addClass("activo");
	// Cargamos el contenido del modulo correspondiente y lo aniadimos a la zona arrastrable
	$.ajax({
		url: "/includes/modulos/widgets/widgets-" + idwidget + ".html",
		cache: false,
		success: function(html) {
			var $contenido = $(html);
			if(idwidget != 138 && $contenido.find(".mw_precontenido").html() == null) { // Quito el boton de edicion del modulo
				$contenido.find(".mw_enlacestop").remove();
			} else {
				$contenido.find(".mw_enlacestop").html('Finalizar edici�n');
			}
			// lo aniadimos a la zona arrastrable si no est� ya aniadido
			if($("#modwid_" + idwidget).html() == null) {
				$("#zona_arrastrable_" + columna).prepend($contenido.parent().html());
				WIDGETS.accionModulo(idwidget);
			} else {
				$("#modwid_" + idwidget).show();
				WIDGETS.accionModulo(idwidget);
			}
			if(personalizacion) {
				WIDGETS.personalizacion('aniadir');
			}
		}
	});
};

WIDGETS.accionModulo = function(idwidget) { // Accion que se lleva a cabo sobre el modulo antes de terminar de ser arrastrado
	if($("#modwid_" + idwidget).find("#clasificaciones-futbol").html() != null) { // CLASIFICACIONES FUTBOL
		$.getScript("http://www.hoysport.com/js/geca/futbol-include-clasificaciones.js", function() {
			WIDGETS.HScargaCla('futbol', 'primera');
		});
	}
	if($("#modwid_" + idwidget).find("#tablasresultados-futbol").html() != null) { // RESULTADOS FUTBOL
		WIDGETS.HScarga('futbol', 'primera');
	}
	if($("#modwid_" + idwidget).find("#mod_tu_cine").html() != null) { // TU CINE
		WIDGETS.tuCineInicializa(1);
	}
	if($("#modwid_" + idwidget).find("#mod_laguiatv").html() != null) { // LAGUIATV
		WIDGETS.parrilla();
	}
	if($("#modwid_" + idwidget).find("#mod_meteo").html() != null) { // METEO
		WIDGETS.meteoLocalidades();
		if((USUARIOS.preferencias.widgets != null) && (USUARIOS.preferencias.widgets.preferencias != null) && (USUARIOS.preferencias.widgets.preferencias.mod_meteo != null)) WIDGETS.meteoTiempo(USUARIOS.preferencias.widgets.preferencias.mod_meteo);
	}
	if($("#modwid_" + idwidget).find("#mod_horoscopo").html() != null) { // HOROSCOPO
		WIDGETS.horoscopo();
	}
	if($("#modwid_" + idwidget).find("#mw-empleo").html() != null) { // INFOEMPLEO
		WIDGETS.infoempleo();
	}
	if(USUARIOS.preferencias.widgets != null && USUARIOS.preferencias.widgets.preferencias != null) {
		if($("#modwid_" + idwidget).find("[name=selNumUltimas]").html() != null) { // ULTIMAS NOTICIAS
			var idModulo = $("#modwid_" + idwidget + " .mw_contenedor").attr("id");
			if(idModulo == 'mod_ultimas_noticias_mujerhoy') {
				WIDGETS.ultimasNoticias($("#modwid_" + idwidget).find(".mw_precontenido"), 'seleccionar', USUARIOS.preferencias.widgets.preferencias.mod_ultimas_noticias_mujerhoy);
			} else {
				WIDGETS.ultimasNoticias($("#modwid_" + idwidget).find(".mw_precontenido"), 'seleccionar', USUARIOS.preferencias.widgets.preferencias.mod_ultimas_noticias);
			}
		}
	}
};

WIDGETS.cancelar = function(obj) {
	var objId = $(obj).parents(".mw_contenedor").attr("id");
	if(typeof(objId) == 'undefined') {
		objId = $(obj).parents(".mw_precontenido").attr("id");
	}
	
	switch(objId) {
		case 'mod_ultimas_noticias_mujerhoy':
			USUARIOS.personalizar({widgets:{preferencias:{mod_ultimas_noticias_mujerhoy: 'null'}}});
			var frm = document.forms["frm_mod_ultimas_noticias_mujerhoy"];
			frm.selNumUltimas.value = 10;
			$("#mod_ultimas_noticias_mujerhoy .mw_contenido ul li").each(function(index) {
				$(this).slideDown('slow');
			});
			break;
		case 'mod_ultimas_noticias':
			USUARIOS.personalizar({widgets:{preferencias:{mod_ultimas_noticias: null}}});
			var frm = document.forms["frm_mod_ultimas_noticias"];
			frm.selNumUltimas.value = 10;
			$("#mod_ultimas_noticias .mw_contenido ul li").each(function(index) {
				$(this).slideDown('slow');
			});
			break;
		case 'mod_laguiatv':
			USUARIOS.personalizar({widgets:{preferencias:{mod_laguiatv: null}}});
			break;
		case 'mod_meteo':
			USUARIOS.personalizar({widgets:{preferencias:{mod_meteo: null}}});
			$('#mod_meteo').parents(".modWidget").find(".mw_enlacestop").html('Editar');
			$('#mod_meteo').parents(".modWidget").find(".mw_contenedor .mw_precontenido").slideUp('slow');
			$('#mod_meteo').parents(".modWidget").find(".mw_precontenido").slideUp('slow');
			break;
		case 'mod_tu_cine':
			USUARIOS.personalizar({widgets:{preferencias:{tucine: {provincia: null}}}});
			USUARIOS.personalizar({widgets:{preferencias:{tucine: {localidad: null}}}});
			USUARIOS.personalizar({widgets:{preferencias:{tucine: {cine: null}}}});
			break;
		case 'mod_horoscopo':
			USUARIOS.personalizar({widgets:{preferencias:{horoscopo: {signo: null}}}});
			USUARIOS.personalizar({widgets:{preferencias:{horoscopo: {prevision: null}}}});
			break;
	}


};


WIDGETS.cargando = function(cargando) {
	if(cargando == 1) {
		if($("#mw-cargando") == null) {
			$("#zona_arrastrable_A").prepend('<span id="mw-cargando"><span></span></span>');
		} else if($("#zona_arrastrable_A #mw-cargando").is(':hidden')) {
			$("#mw-cargando").show();
		}
	} else {
		if($("#mw-cargando") != null) {
			$("#mw-cargando").remove();
		}
	}
};

WIDGETS.quitar = function(idwidget) {
	$("#modwid_" + idwidget).remove();
	$("#wid_" + idwidget).removeClass("activo");
	WIDGETS.personalizacion('guardar_columnas');
};


WIDGETS.editar = function(obj) {
	if($(obj).parents(".modWidget").find(".mw_enlacestop").html() == 'Editar') { // slideDown + cambio texto
		$(obj).parents(".modWidget").find(".mw_enlacestop").html('Finalizar edici�n');
		$(obj).parents(".modWidget").find(".mw_contenedor .mw_precontenido").slideDown('slow');
		$(obj).parents(".modWidget").find(".mw_precontenido").slideDown('slow');
	} else { // slideUp + cambio texto
		if($(obj).html() == 'Cancelar') {
			WIDGETS.cancelar(obj);
		}
		$(obj).parents(".modWidget").find(".mw_enlacestop").html('Editar');
		$(obj).parents(".modWidget").find(".mw_contenedor .mw_precontenido").slideUp('slow');
		$(obj).parents(".modWidget").find(".mw_precontenido").slideUp('slow');
	}
};

WIDGETS.modulosColumna = function(columna) { // Devuelve un array con los ids de los modulos arrastrados a la columna "columna"
	var widgets = new Array();
	$("#zona_arrastrable_" + columna + " .modWidget").each(function() {
		datos = $(this).attr("id").split("_");
		if(datos[1] != null) {
			widgets[widgets.length] = datos[1];
		}
	});
	return widgets;
};

WIDGETS.personalizacion = function(accion) {
	if(accion == 'aniadir') { // Guardar los modulos en cookie o bbdd
		USUARIOS.personalizar({widgets:{columnas:{columnaA: WIDGETS.modulosColumna('A')}}});
	} else if(accion == 'guardar_columnas') { // Guardar los modulos en cookie o bbdd
		USUARIOS.personalizar({widgets:{columnas:{columnaA:[]}}});
		USUARIOS.personalizar({widgets:{columnas:{columnaB:[]}}});
		USUARIOS.personalizar({widgets:{columnas:{columnaC:[]}}});
		USUARIOS.personalizar({widgets:{columnas:{columnaA: WIDGETS.modulosColumna('A')}}});
		USUARIOS.personalizar({widgets:{columnas:{columnaB: WIDGETS.modulosColumna('B')}}});
		USUARIOS.personalizar({widgets:{columnas:{columnaC: WIDGETS.modulosColumna('C')}}});
	} else { // Mostrar los m�dulos que tenga personalizados
		if(USUARIOS.preferencias.widgets == null) {
			return;
		}
		if(USUARIOS.preferencias.widgets.columnas == null) {
			return;
		}

		if(USUARIOS.preferencias.widgets.columnas.columnaA != null) {
			var preferenciasA = USUARIOS.preferencias.widgets.columnas.columnaA;
			if(preferenciasA.length > 0) {
				for(i=preferenciasA.length - 1; i>=0;i--) {
					if(preferenciasA[i] != '') {
						WIDGETS.aniadir(preferenciasA[i], false, 'A');
					}
				}
			}
		}
		if(USUARIOS.preferencias.widgets.columnas.columnaB != null) {
			var preferenciasB = USUARIOS.preferencias.widgets.columnas.columnaB;
			if(preferenciasB.length > 0) {
				for(i=preferenciasB.length - 1; i>=0;i--) {
					if(preferenciasB[i] != '') {
						WIDGETS.aniadir(preferenciasB[i], false, 'B');
					}
				}
			}
		}
		if(USUARIOS.preferencias.widgets.columnas.columnaC != null) {
			var preferenciasC = USUARIOS.preferencias.widgets.columnas.columnaC;
			if(preferenciasC.length > 0) {
				for(i=preferenciasC.length - 1; i>=0;i--) {
					if(preferenciasC[i] != '') {
						WIDGETS.aniadir(preferenciasC[i], false, 'C');
					}
				}
			}
		}

	}
};
////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// FIN FUNCIONES COMUNES WIDGETS ////////////////////////
////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// MODULO ULTIMAS NOTICIAS //////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
WIDGETS.ultimasNoticias = function(obj, accion, numNoticias) {
	var idModulo = $(obj).parents(".modWidget").find(".mw_contenedor").attr("id");
	var frm = document.forms[idModulo.replace("mod_ultimas_noticias", "frm_mod_ultimas_noticias")];
	if(numNoticias == null)
		numNoticias = frm.selNumUltimas.value;
	else
		frm.selNumUltimas.selectedIndex = numNoticias - 1;
	if(accion == 'seleccionar') {
		$("#" + idModulo + " .mw_contenido ul li").each(function(index) {
			if(index + 1 > numNoticias) {
				$(this).slideUp('slow');
			} else if($(this).is(':hidden')) {
				$(this).slideDown('slow');
			}
		});
	} else if(accion == 'guardar') {
		if(idModulo == 'mod_ultimas_noticias_mujerhoy') {
			USUARIOS.personalizar({widgets:{preferencias:{mod_ultimas_noticias_mujerhoy: frm.selNumUltimas.value}}});
			$('#mod_ultimas_noticias_mujerhoy').parents(".modWidget").find(".mw_enlacestop").html('Editar');
			$('#mod_ultimas_noticias_mujerhoy').parents(".modWidget").find(".mw_contenedor .mw_precontenido").slideUp('slow');
			$('#mod_ultimas_noticias_mujerhoy').parents(".modWidget").find(".mw_precontenido").slideUp('slow');
		} else {
			USUARIOS.personalizar({widgets:{preferencias:{mod_ultimas_noticias: frm.selNumUltimas.value}}});
			$('#mod_ultimas_noticias').parents(".modWidget").find(".mw_enlacestop").html('Editar');
			$('#mod_ultimas_noticias').parents(".modWidget").find(".mw_contenedor .mw_precontenido").slideUp('slow');
			$('#mod_ultimas_noticias').parents(".modWidget").find(".mw_precontenido").slideUp('slow');
		}
	}
};
////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// FIN MODULO ULTIMAS NOTICIAS //////////////////////////
////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// TU CINE //////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
WIDGETS.tuCineInicializa = function (pag) { // Carga los selects de Tu Cine
	if(USUARIOS.preferencias != null && USUARIOS.preferencias.widgets != null && USUARIOS.preferencias.widgets.preferencias != null && USUARIOS.preferencias.widgets.preferencias.tucine != null) {
		provincia = USUARIOS.preferencias.widgets.preferencias.tucine.provincia;
		localidad = USUARIOS.preferencias.widgets.preferencias.tucine.localidad;
		cine = USUARIOS.preferencias.widgets.preferencias.tucine.cine;
	} else {
		provincia = 0;
		localidad = 0;
		cine = 0;
	}
	var frm = document.frmCartelera;

	if(typeof(provinciasHC) == 'undefined') {
		$.getScript("/js/que.provinciasHC.js", function(data, status) {
			if(data != '' && status == 'success' && typeof(provinciasHC) != 'undefined') cargarSel(frm.selprovinciasHC, provinciasHC, provincia, 0);
		});
	} else {
		cargarSel(frm.selprovinciasHC, provinciasHC, provincia, 0);
	}
	if(typeof(localidadesHC) == 'undefined') {
		$.getScript("/js/que.localidadesHC.js", function(data, status) {
			if(data != '' && status == 'success' && typeof(localidadesHC[provincia]) != 'undefined') cargarSel(frm.sellocalidadesHC, localidadesHC[provincia], localidad, 0);
		});
	} else {
		cargarSel(frm.sellocalidadesHC, localidadesHC[provincia], localidad, 0);
	}
	if(typeof(cinesHC) == 'undefined') {
		$.getScript("/js/que.cinesHC.js");
	}
	if(typeof(cinesLocalidadHC) == 'undefined') {
		$.getScript("/js/que.cinesLocalidadHC.js", function(data, status) {
			if(provincia != 0) {
				if(data != '' && status == 'success') cargarSel(frm.selcinesHC, cinesLocalidadHC[provincia + '###' + frm.sellocalidadesHC.options[frm.sellocalidadesHC.selectedIndex].text], cine, 0);
			} else {
				if(data != '' && status == 'success') cargarSel(frm.selcinesHC, cinesLocalidadHC[0], cine, 0);
			}
		});
	} else {
		if(provincia != 0) {
			cargarSel(frm.selcinesHC, cinesLocalidadHC[provincia + '###' + frm.sellocalidadesHC.options[frm.sellocalidadesHC.selectedIndex].text], cine, 0);
		} else {
			cargarSel(frm.selcinesHC, cinesLocalidadHC[0], cine, 0);
		}
	}

	if(cine != 0)
		WIDGETS.tuCine(1);

	$("select[name=selprovinciasHC]").change(function() {
		cargarSel(frm.sellocalidadesHC, localidadesHC[this.value], '', 0);
		cargarSel(frm.selcinesHC, cinesHC[this.value], '', 0);
	});
	$("select[name=sellocalidadesHC]").change(function() {
		cargarSel(frm.selcinesHC, cinesLocalidadHC[frm.selprovinciasHC.value + '###' + frm.sellocalidadesHC.options[frm.sellocalidadesHC.selectedIndex].text], 0, 0);
	});
	$("select[name=selcinesHC]").change(function() {
		WIDGETS.tuCine(1);
	});


};

WIDGETS.tuCine = function (pag) { // Carga los datos del cine seleccionado
	var frm = document.frmCartelera;
	if(frm.selprovinciasHC.value == 0) {
		return;
	}
	if(frm.sellocalidadesHC.options[frm.sellocalidadesHC.options.selectedIndex].text == '--Localidad--') {
		return;
	}
	if(frm.selcinesHC.options[frm.selcinesHC.options.selectedIndex].text == '--Cine--') {
		return;
	}

	var localidadEnvio = frm.sellocalidadesHC.selectedIndex > 0 ? frm.sellocalidadesHC.options[frm.sellocalidadesHC.selectedIndex].text : '';
	var cineEnvio = frm.selcinesHC.selectedIndex > 0 ? frm.selcinesHC.options[frm.selcinesHC.selectedIndex].value : '';

	$.getJSON(
		"/backend/WIDGETS.modulos.php",
		{modulo: 'tucine', codprovincia: frm.selprovinciasHC.value, localidad: localidadEnvio, codcine: cineEnvio, pag: pag, r: Math.random()},
		function(data) {
			$("#cine_datos").html();
			$("#cine_datos .mw_cinedatos h3").html(data.cine);
			if(data.direccion != null) {
				$("#cine_datos .mw_cinedatos h3").append("<span>" + data.direccion + "</span>");
			}
			if(data.precio != null) {
				$("#cine_datos .mw_cinedatos h3").append("<span>" + data.precio + "</span>");
			}
			if(data.telefono != null) {
				$("#cine_datos .mw_cinedatos h3").append("<span>" + data.telefono + "</span>");
			}
			$("#cine_datos .mw_cinedatos h6").html("Programa para el " + data.fecha);
			$("#cine_datos .mw_listado").html('');
			for(i=0; i<data.peliculas.length; i++) {
				$("#cine_datos .mw_listado").append("<li>" + data.peliculas[i][0] + "</li>");
			}
		}
	);
};

WIDGETS.tuCineGuardar = function () { // Guarda en las preferencias del usuario el cine seleccionado
	var frm = document.frmCartelera;
	USUARIOS.personalizar({widgets:{preferencias:{tucine: {provincia: frm.selprovinciasHC.value}}}});
	USUARIOS.personalizar({widgets:{preferencias:{tucine: {localidad: frm.sellocalidadesHC.value}}}});
	USUARIOS.personalizar({widgets:{preferencias:{tucine: {cine: frm.selcinesHC.value}}}});
	WIDGETS.tuCine(1);
};
////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// FIN TU CINE //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// MODULOS HOYSPORT /////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

var estados_futbol = Array('hstddia##No iniciado','hstdparte##1� Parte','hstdparte##Descanso','hstdparte##2� Parte','hstdparte##1� Parte Prorroga','hstdparte##2� Parte Prorroga','hstdparte##Penalties','hstdfin##Finalizado','hstdparte##Suspendido','hstdparte##Directo');
var patrocinador_futbol = '';
var gif_futbol = '<img src="/HS_hoysport/hsimages/hs_balonfutbol.gif"/>';
var defecto_futbol = 'primera';
var competiciones_futbol = new RegExp("^(primera|segunda|segundab_I|segundab|ligacampeones|uefa|coparey)$");

WIDGETS.HScarga = function(deporte,competicion,grupo) {
	var grupoComp = "";
	if((typeof(grupo) != "undefined") && (grupo != '') && ((window.document.getElementById('comp2-' + competicion + '_' + grupo) != null) || (window.document.getElementById('comp-' + competicion + '_' + grupo) != null)))
		grupoComp = "_" + grupo;
	eval('competicion_' + deporte + ' = competicion');
	eval('competiciones = competiciones_' + deporte);
	eval('defecto = defecto_' + deporte);
	if((competiciones.test(competicion) == false) || ((window.document.getElementById('comp-' + competicion) == null) && (window.document.getElementById('comp-' + competicion + grupoComp) == null))) competicion = defecto;

	$('[id*=comp-]').each(function() {
		$(this).removeClass("activo");
	});
	$("#comp-" + competicion).addClass("activo");
	if(competicion == 'segundab') {
		$("#hs-pestannas2-segundab").show();
	} else {
		$("#hs-pestannas2-segundab").hide();
	}
	WIDGETS.HSresultados(deporte,competicion,grupoComp);
};

WIDGETS.HScargaCla = function(deporte,competicion,grupo) {
	var grupoComp = "";
	if((typeof(grupo) != "undefined") && (grupo != '') && ((window.document.getElementById('comp2-cla-' + competicion + '_' + grupo) != null) || (window.document.getElementById('comp-cla-' + competicion + '_' + grupo) != null)))
		grupoComp = "_" + grupo;
	eval('competiciones = competiciones_' + deporte);
	eval('defecto = defecto_' + deporte);
	if((competiciones.test(competicion) == false) || ((window.document.getElementById('comp-cla-' + competicion + grupoComp) == null) && (window.document.getElementById('comp-cla-' + competicion + grupoComp) == null))) competicion = defecto;

	$('[id*=comp-cla-]').each(function() {
		$(this).removeClass("activo");
	});
	if(grupo != null && typeof(grupo) != "undefined" && grupo != '') {
		$("#comp-cla-" + competicion + "_" + grupo).addClass("activo");
	} else {
		$("#comp-cla-" + competicion).addClass("activo");
	}
	WIDGETS.HSclasificaciones(deporte,competicion,grupoComp);
};

WIDGETS.HSresultados = function(deporte,competicion,grupoComp) {

	var url_deporte = Array();
	url_deporte['ligacampeones'] = 'liga-campeones';
	url_deporte['coparey'] = 'copa-rey';
	url_deporte['mundialrallies'] = 'mundial-rallies';


	var salida = '';
	resArray = new Array();
	estPartidos = new Array();

	eval('if(typeof(HSres_' + competicion + grupoComp + ') != "undefined") resArray = HSres_' + competicion + grupoComp + ';');
	var titulo = window.document.getElementById('hs-titulo-' + deporte);



	if(resArray.length > 1) {
		$('#tablasresultados-' + deporte).html('');
		eval('estados = estados_' + deporte);
		for(i=2;i<resArray.length;i++) {
			parDatos = resArray[i].split('#');

			if(parDatos.length == 4) {
				estado = parDatos[0];
				equipos = parDatos[1];
				pob = parDatos[3];

				if((estado > 0) && (estado < 8)) {
					resultado = parDatos[2];
				} else {
					resultado = '&nbsp;-&nbsp;';
				}

				estDatos = Array('manita.gif','No iniciado');
				if(typeof(estados[parDatos[0]]) != "undefined")
					estDatos = estados[parDatos[0]].split('##');
				var enlace = '';
				if(parDatos[3] != '') {
					if((typeof(estados[estados.length-1]) != "undefined") && (parDatos[0] > 0) && (parDatos[0] < 7))
						estDatos = estados[estados.length-1].split('##');
					competicion_url = competicion;
					if(typeof(url_deporte[competicion]) != 'undefined')
						competicion_url = url_deporte[competicion];
					if(competicion == 'primera' || competicion == 'segunda' )
						enlace = '/deportes/futbol/liga-directo.html?' + parDatos[3] ;
					else if(competicion == 'ligacampeones')
						enlace = '/deportes/futbol/champions-directo.html?' + parDatos[3] ;
					else if(competicion == 'uefa')
						enlace = '/deportes/futbol/uefa-directo.html?' + parDatos[3] ;
					else if(competicion == 'coparey')
						enlace = '/deportes/futbol/copa-del-rey-directo.html?' + parDatos[3] ;
					else
					enlace = 'http://www.hoysport.com/' + deporte + '/' + competicion_url + '/partido-en-directo.html'+ '?' + parDatos[3] ;
				}

				if (enlace == '')
					html_estado = salida + '<div class="estado">' + estDatos[1]+'</div>';
				else
					html_estado = salida + '<div class="estado"><a href="' + enlace +'">' + estDatos[1]+'</a></div>';


				if(typeof(equipos)!= "undefined")
					estPartidos = equipos.split('-');

				className = i%2 == 0 ? "inpar clearfix" : "par clearfix";
				html = '<div class="' + className + '"><div class="equipos"><span class="eq1">' + estPartidos[0] + '</span><span class="res">' + resultado + ' </span><span class="eq2">' + estPartidos[1] + '</span></div>' + html_estado + '</div>';
				$('#tablasresultados-' + deporte).append(html);
			}
		}
	}
};

WIDGETS.HSclasificaciones = function(deporte,competicion,grupoComp) {

	var salida = '';
	var filaTabla = '';
	resArray = new Array();
	eval('if(typeof(HScla_' + competicion + grupoComp + ') != "undefined") resArray = HScla_' + competicion + grupoComp + ';');

	$('#clasificaciones-futbol table tbody tr').each(function(index) {
		if(index > 0) {
			$(this).remove();
		}
	});

	if(resArray.length > 2) {
		for(i=2;i<resArray.length;i++) {
			filaTabla = '';
			parDatos = resArray[i].split('#');
			if(parDatos.length == 3) {
				// al reves de lo logico
				if(i % 2 == 0)
					filaTabla = '<tr class="impar">\n';
				else
					filaTabla = '<tr>\n';
				if(competicion == "primera") {
					if(i<6)
						filaTabla = filaTabla + '<td><div class="ico champion"></div></td>\n';
					else if(i<8)
						filaTabla = filaTabla + '<td><div class="ico uefa"></div></td>\n';
					else if(i>18)
						filaTabla = filaTabla + '<td><div class="ico descenso"></div></td>\n';
					else
						filaTabla = filaTabla + '<td></td>\n';
				}
				else if(competicion == "segunda") {
					if(i<5)
						filaTabla = filaTabla + '<td><div class="ico ascenso"></div></td>\n';
					else if(i>19)
						filaTabla = filaTabla + '<td><div class="ico descenso"></div></td>\n';
					else
						filaTabla = filaTabla + '<td></td>\n';
				}
				else if(competicion.indexOf("segundab") == 0) {
					if(i<6)
						filaTabla = filaTabla + '<td class="hst_ico"><div class="ico promoascenso"></div></td>\n';
					else if(i == 17)
						filaTabla = filaTabla + '<td class="hst_ico"><div class="ico promodescenso"></div></td>\n';
					else if(i>17)
						filaTabla = filaTabla + '<td><div id="descenso" class="ico"></div></td>\n';
					else
						filaTabla = filaTabla + '<td></td>\n';
				}
				filaTabla = filaTabla + '<td class="orden">' + parDatos[0] + '</td>\n';
				filaTabla = filaTabla + '<td class="equipo">' + parDatos[1] + '</td>\n';
				filaTabla = filaTabla + '<td class="puntos">' + parDatos[2] + '</td>\n';
				filaTabla = filaTabla + '</tr>\n';
				$('#clasificaciones-futbol table tbody').append(filaTabla);
			}
			else if((parDatos.length == 2) && (parDatos[0] == "GRUPO")) {
				filaTabla = filaTabla + '<tr>\n';
				filaTabla = filaTabla + '<td class="pos" colspan="3">' + parDatos[1] + '</td>\n';
				filaTabla = filaTabla + '</tr>\n';
				$('#clasificaciones-futbol table tbody').append(filaTabla);
			}
		}
	}
};
////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// FIN MODULOS HOYSPORT /////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// PARRILLA LAGUIATV ////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
WIDGETS.parrilla = function(dia, hora, cambiofranja) { // Carga de la parrilla

	if(cambiofranja == 'anterior') {
		if($("[name=selFranjas]").get(0).selectedIndex > 0) {
			$("[name=selFranjas]").get(0).selectedIndex = $("[name=selFranjas]").get(0).selectedIndex - 1;
		} else {
			return;
		}
	} else if(cambiofranja == 'siguiente') {
		if($("[name=selFranjas]").get(0).selectedIndex < $("[name=selFranjas]").get(0).length - 1) {
			$("[name=selFranjas]").get(0).selectedIndex = $("[name=selFranjas]").get(0).selectedIndex + 1;
		} else {
			return;
		}
	}

	if(hora == null) {
		if(USUARIOS.preferencias.widgets != null && USUARIOS.preferencias.widgets.preferencias != null && USUARIOS.preferencias.widgets.preferencias.mod_laguiatv != null) {
			hora = USUARIOS.preferencias.widgets.preferencias.mod_laguiatv;
		} else {
			hora = '';
		}
	} else {
		$('#mod_laguiatv .mw_precontenido ul li').each(function() {
			if($(this).hasClass('activo')) {
				dia = $(this).find('a').html();
			}
		});
	}
	if(dia == null) {
		dia = '';
	} else {
		hora = $('[name=selFranjas]').val();
	}
	$.getJSON(
		"/backend/WIDGETS.modulos.php",
		{modulo: 'parrilla-laguiatv', dia: dia, hora: hora, r: Math.random()},
		function(data) {
			var frm = document.forms['frmParrilla'];


			$('#mod_laguiatv .mw_contenido .mwtv_listado').html('');
			if(data.TVE1 != null) {
				$('#mod_laguiatv .mw_contenido .mwtv_listado').append('<li class="first"><span class="ico mwtv-1"></span><span class="mwtv_cadena">Tve1</span> ' + data.TVE1.programa + ' <span class="mwtv_hora">(' + data.TVE1.inicio + ' - ' + data.TVE1.fin + ')</span></a></li>');
			}
			if(data.La2 != null) {
				$('#mod_laguiatv .mw_contenido .mwtv_listado').append('<li><span class="ico mwtv-2"></span><span class="mwtv_cadena">Tve2</span> ' + data.La2.programa + ' <span class="mwtv_hora">(' + data.La2.inicio + ' - ' + data.La2.fin + ')</span></a></li>');
			}
			if(data.Antena3 != null) {
				$('#mod_laguiatv .mw_contenido .mwtv_listado').append('<li><span class="ico mwtv-3"></span><span class="mwtv_cadena">Antena3</span> ' + data.Antena3.programa + ' <span class="mwtv_hora">(' + data.Antena3.inicio + ' - ' + data.Antena3.fin + ')</span></a></li>');
			}
			if(data.Cuatro != null) {
				$('#mod_laguiatv .mw_contenido .mwtv_listado').append('<li><span class="ico mwtv-4"></span><span class="mwtv_cadena">Cuatro</span> ' + data.Cuatro.programa + ' <span class="mwtv_hora">(' + data.Cuatro.inicio + ' - ' + data.Cuatro.fin + ')</span></a></li>');
			}
			if(data.Tele5 != null) {
				$('#mod_laguiatv .mw_contenido .mwtv_listado').append('<li><span class="ico mwtv-5"></span><span class="mwtv_cadena">Tele5</span> ' + data.Tele5.programa + ' <span class="mwtv_hora">(' + data.Tele5.inicio + ' - ' + data.Tele5.fin + ')</span></a></li>');
			}
			if(data.LaSexta != null) {
				$('#mod_laguiatv .mw_contenido .mwtv_listado').append('<li><span class="ico mwtv-6"></span><span class="mwtv_cadena">LaSexta</span> ' + data.LaSexta.programa + ' <span class="mwtv_hora">(' + data.LaSexta.inicio + ' - ' + data.LaSexta.fin + ')</span></a></li>');
			}
			$('#mod_laguiatv .mw_contenido .mwtv_listado').append('<li><a class="mwtv_mas" href="http://que.laguiatv.com/programacion.php">Ver todas las cadenas</a></li>');

			$('#mod_laguiatv .mw_precontenido ul li').each(function() {
				if($(this).find('a').html() == data.dia) {
					$(this).addClass('activo');
				} else {
					$(this).removeClass('activo');
				}
				if(data.hora != '') {
					frm.selFranjas.value = data.hora;
				}
			});
		}
	);
};

WIDGETS.parrillaGuardar = function() { // Guarda en las preferencias del usuario la hora seleccionada
	hora = $('[name=selFranjas]').val();
	USUARIOS.personalizar({widgets:{preferencias:{mod_laguiatv: hora}}});
	$('#mod_laguiatv').parents(".modWidget").find(".mw_enlacestop").html('Editar');
	$('#mod_laguiatv').parents(".modWidget").find(".mw_contenedor .mw_precontenido").slideUp('slow');
	$('#mod_laguiatv').parents(".modWidget").find(".mw_precontenido").slideUp('slow');

};
////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// FIN PARRILLA LAGUIATV ////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// MODULO METEO //////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

WIDGETS.meteoLocalidades = function() {
	var frm = window.document.mw_frm_meteo;
	$.getJSON(
		"/backend/MODULOS.meteo.php",
		{accion: "localidades"},
		function(datos) {
			if(datos.comunidades != null) {
				cargarSel(frm.comunidad,datos.comunidades);
				$(frm.comunidad).change(
					function() {
						eval("var localidades = datos.localidades_" + this.value + ";");
						cargarSel(frm.localidad,localidades);
					}
				);
				$(frm.localidad).change(
					function() {WIDGETS.meteoTiempo(this.value)}
				);
			}
		}
	);
};

WIDGETS.meteoTiempo = function(id_localidad) {
	if((id_localidad != null) && (id_localidad != "") && (!isNaN(id_localidad))) {
		$.getJSON(
			"/backend/MODULOS.meteo.php",
			{accion: "tiempo", id_localidad: id_localidad},
			function(datos) {
				var salida_obj = $('<table></table>');
				$(salida_obj).append('<tr id="mw-meteo-thcol"><th scope="col">&nbsp;</th><th scope="col">Estado</th><th scope="col">Temperatura m�nima </th><th scope="col">Temperatura m�xima </th></tr>');
				if($.isArray(datos.prevision)) {
					for(i=0;i<datos.prevision.length;i++) {
						if(i == 0) {
							$(salida_obj).append('<tr id="mw-meteo-hoy"><th colspan="2" scope="row"><span class="mw-meteo-estado met-' + datos.prevision[i].manana + '"></span><h3 class="mw-meteo-localidad">' + datos.localidad + '</h3><span id="mw-meteo-dia">' + datos.prevision[i].l + '</span> <span id="mw-meteo-dia2">' + datos.prevision[i].d + ' de ' + datos.prevision[i].F.toLowerCase() + '</span></th><td class="mw-meteo-min">' + datos.prevision[i].minima + '<br />min </td><td class="mw-meteo-max">' + datos.prevision[i].maxima + ' <br />max</td></tr>');
						}
						else {
							var prevision = (i == 1) ? datos.prevision[i].manana : datos.prevision[i].dia;
							$(salida_obj).append('<tr><th scope="row" class="mw-meteo-dia">' + datos.prevision[i].l + '</th><td><span class="mw-meteo-estado met-' + prevision + '"></span></td><td class="mw-meteo-min">' + datos.prevision[i].minima + ' min</td><td class="mw-meteo-max">' + datos.prevision[i].maxima + ' max</td></tr>');
						}
					}
				}
				$("#mw-meteo table").replaceWith(salida_obj);
			}
		);
	}
}

WIDGETS.meteoGuardar = function() { // Guarda en las preferencias del usuario la localidad seleccionada
	var frm = window.document.mw_frm_meteo;
	var id_localidad = frm.localidad.value;
	if(id_localidad != "") USUARIOS.personalizar({widgets:{preferencias:{mod_meteo: id_localidad}}});
	$('#mod_meteo').parents(".modWidget").find(".mw_enlacestop").html('Editar');
	$('#mod_meteo').parents(".modWidget").find(".mw_contenedor .mw_precontenido").slideUp('slow');
	$('#mod_meteo').parents(".modWidget").find(".mw_precontenido").slideUp('slow');
};

////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// FIN MODULO METEO //////////////////////////
////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// HOROSCOPO ////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
WIDGETS.horoscopo = function() {
	var frm = window.document.mw_frm_horoscopo;

	$(frm.selSigno).change(
		function() {WIDGETS.verHoroscopo()}
	);
	$(frm.selPrevision).change(
		function() {WIDGETS.verHoroscopo()}
	);

	if(USUARIOS.preferencias.widgets != null && USUARIOS.preferencias.widgets.preferencias != null && USUARIOS.preferencias.widgets.preferencias.horoscopo != null) {
		$(frm.selSigno).val(USUARIOS.preferencias.widgets.preferencias.horoscopo.signo);
		$(frm.selPrevision).val(USUARIOS.preferencias.widgets.preferencias.horoscopo.prevision);
		WIDGETS.verHoroscopo();
	}
};

WIDGETS.verHoroscopo = function() {
	var frm = window.document.mw_frm_horoscopo;
	$.ajax({
		url: "/includes/modulos/widgets/horoscopo-" + frm.selPrevision.value + "-" + frm.selSigno.value + ".html",
		cache: false,
		success: function(html) {
			//var $contenido = $(html);
			$("#horoscopo_datos").html(html);
		}
	});
};

WIDGETS.horoscopoGuardar = function () { // Guarda en las preferencias del usuario el signo + prevision
	var frm = document.mw_frm_horoscopo;
	USUARIOS.personalizar({widgets:{preferencias:{horoscopo: {signo: frm.selSigno.value}}}});
	USUARIOS.personalizar({widgets:{preferencias:{horoscopo: {prevision: frm.selPrevision.value}}}});

	$('#mod_horoscopo').parents(".modWidget").find(".mw_enlacestop").html('Editar');
	$('#mod_horoscopo').parents(".modWidget").find(".mw_contenedor .mw_precontenido").slideUp('slow');
	$('#mod_horoscopo').parents(".modWidget").find(".mw_precontenido").slideUp('slow');
};
////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// FIN HOROSCOPO ////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// INFOEMPLEO ///////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
WIDGETS.infoempleo = function() {
	$("#mw-empleo #pest-infoempleo-trabajo").click( function() {
		$("#mw-empleo #pest-infoempleo-formacion").removeClass('activo');
		$(this).attr({ href: "javascript:void(0)"});
		$(this).addClass('activo');
		$("#mw-empleo #cont-infoempleo-trabajo").removeClass('ocultar');
		$("#mw-empleo #cont-infoempleo-formacion").addClass('ocultar');
	});


	$("#mw-empleo #pest-infoempleo-formacion").click( function() {
		$("#mw-empleo #pest-infoempleo-trabajo").removeClass('activo');
		$(this).attr({ href: "javascript:void(0)"});
		$(this).addClass('activo');
		$("#mw-empleo #cont-infoempleo-formacion").removeClass('ocultar');
		$("#mw-empleo #cont-infoempleo-trabajo").addClass('ocultar');
	});

	$("#mw-empleo #F_PClaveTrabajo").click(function() {
		if($(this).val() == 'Palabra clave') {
			$(this).val('');
		}
	});
};

WIDGETS.infoempleoBuscar = function(cadena) {
	var a, cad, cad_aux;

    if (cadena == 'trabajo')
    {
        cad = 'http://www.infoempleo.com/trabajo';
        cad_aux = cad;

        if (document.getElementById('id_tipo_oferta').checked == true)
        {
			cad += '/puesto-de-trabajo_Sin-especificar';
        }

        if(document.getElementById('F_PClaveTrabajo').value != 'Palabra clave' && document.getElementById('F_PClaveTrabajo').value != '')
        {
            cad += '/palabra_' + WIDGETS.InfoempleoEscapeUtf8(document.getElementById('F_PClaveTrabajo').value);
        }
        if (document.getElementById('F_AGeografica').selectedIndex > 0)
        {
            cad += '/en_' + WIDGETS.InfoempleoEscapeUtf8(document.getElementById('F_AGeografica')[document.getElementById('F_AGeografica').selectedIndex].text);
            cad += '/selAGeografica_' + document.getElementById('F_AGeografica').selectedIndex;
        }
        if (document.getElementById('F_AFuncional').selectedIndex > 0)
        {
            cad += '/area-de-empresa_' + WIDGETS.InfoempleoEscapeUtf8(document.getElementById('F_AFuncional')[document.getElementById('F_AFuncional').selectedIndex].text);
            cad += '/selAFuncional_' + document.getElementById('F_AFuncional').selectedIndex;
        }
    }
    else
    {
        cad = 'http://www.infoempleo.com/cursos/';
        cad_aux = cad;

        if(document.getElementById('F_PClaveFormacion').value != 'Palabra clave' && document.getElementById('F_PClaveFormacion').value != '')
        {
                    cad += '/palabra_' + WIDGETS.InfoempleoEscapeUtf8(document.getElementById('F_PClaveFormacion').value) + '/';
        }
    }

    if (cad == cad_aux)
    {
        cad += '/';
    }
    a=window.open(cad);
};

WIDGETS.InfoempleoEscapeUtf8 = function(cad)
{
    var contaTextoSeo = 0;
    var cadAux = cad.toString().toLowerCase();

    cadAux = cadAux.replace(/\xe1/g,"a");
    cadAux = cadAux.replace(/\xe0/g,"a");
    cadAux = cadAux.replace(/\xe9/g,"e");
    cadAux = cadAux.replace(/\xe8/g,"e");
    cadAux = cadAux.replace(/\xed/g,"i");
    cadAux = cadAux.replace(/\xec/g,"i");
    cadAux = cadAux.replace(/\xf3/g,"o");
    cadAux = cadAux.replace(/\xf2/g,"o");
    cadAux = cadAux.replace(/\xfa/g,"u");
    cadAux = cadAux.replace(/\xfc/g,"u");
    cadAux = cadAux.replace(/\xf9/g,"u");
    cadAux = cadAux.replace(/\//g,"-");
    cadAux = cadAux.replace(/ /g,"-");
    cadAux = cadAux.replace(/,/g,"-");
    cadAux = cadAux.replace(/\|/g,"-");
    cadAux = cadAux.replace(/\xf1/g,"nn");
    cadAux = cadAux.replace(/--/g,"-");

    while (contaTextoSeo < cadAux.length)
    {
        if (cadAux.substring(contaTextoSeo, contaTextoSeo + 1) != '-' && cadAux.substring(contaTextoSeo, contaTextoSeo + 1) != '\xe7' && ! (cadAux.substring(contaTextoSeo, contaTextoSeo + 1).charCodeAt() >= 97 && cadAux.substring(contaTextoSeo, contaTextoSeo +1).charCodeAt() <= 122) && ! (cadAux.substring(contaTextoSeo, contaTextoSeo + 1).charCodeAt() >= 48 && cadAux.substring(contaTextoSeo, contaTextoSeo + 1).charCodeAt() <= 57))        {
            cadAux = cadAux.replace(cadAux.substring(contaTextoSeo, contaTextoSeo + 1), '');
            contaTextoSeo -= 1;
        }
        contaTextoSeo += 1;
    }
    return cadAux;
};

////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// FIN INFOEMPLEO ///////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////


$(document).ready(function() {
	// Genero las zonas ARRASTRABLES
	$('#zona_arrastrable_A').sortable({
		placeholder : 'sortableHelper',
	    forcePlaceholderSize: 'true',
		handle: '.mw_header',
		cursor: 'move',
        items: '.modWidget',
        zIndex: 100,
		connectWith: ['#zona_arrastrable_B', '#zona_arrastrable_C'],
		stop: function (ev, ui) {
		},
		remove: function(ev, ui) {
		},
		receive: function(ev, ui) {
			WIDGETS.personalizacion('guardar_columnas');
		}
	});
	$('#zona_arrastrable_B').sortable({
		placeholder : 'sortableHelper',
	    forcePlaceholderSize: 'true',
		handle: '.mw_header',
		cursor: 'move',
		items: '.modWidget',
        zIndex: 100,
		connectWith: ['#zona_arrastrable_A', '#zona_arrastrable_C'],
		stop: function (ev, ui) {
		},
		remove: function(ev, ui) {
		},
		receive: function(ev, ui) {
			WIDGETS.personalizacion('guardar_columnas');		}
	});
	$('#zona_arrastrable_C').sortable({
		placeholder : 'sortableHelper',
	    forcePlaceholderSize: 'true',
		handle: '.mw_header',
		cursor: 'move',
		items: '.modWidget',
        zIndex: 100,
		connectWith: ['#zona_arrastrable_A', '#zona_arrastrable_B'],
		stop: function (ev, ui) {
		},
		remove: function(ev, ui) {
		},
		receive: function(ev, ui) {
			WIDGETS.personalizacion('guardar_columnas');
		}
	});

	// Muestro en la zona arrastrable los modulos que tenga personalizados
	WIDGETS.personalizacion('personalizar');
});

