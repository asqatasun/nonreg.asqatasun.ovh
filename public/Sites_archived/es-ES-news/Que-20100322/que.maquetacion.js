// manejo de elementos de la maquetación
var MQT = {};

MQT.modulometeo = function () {
	$("#cab-meteo-editar").click(MODULOS.meteoAbrir);
	$("#cab-meteo-desplegable .btn_cerrar").click(MODULOS.meteoCerrar);
	$("#cab-meteo-localidad").keyup(
		function() {MODULOS.meteoLocalidades($("#cab-meteo-localidad").val())}
	);
}

MQT.menunavegacion = function () {
	$(".submenu").hover(
		function () {$(this).addClass('subm_activar')},
		function () {$(this).removeClass('subm_activar')}
	);
}

MQT.submenumas = function () {
	$(".qns_mas").hover(
		function () {$(this).addClass('qns_mas_activo')},
		function () {$(this).removeClass('qns_mas_activo')}
	);
}

MQT.navegacionsecciones = function () {
	$("#navs-fotos,#navs-videos,#navs-blogs").hover(
		function () {$(this).addClass('activo')},
		function () {$(this).removeClass('activo')}
	);
};

MQT.fotothumbs = function () {
	$("#modFotoThumbs li").hover(
		function () {$(this).addClass('activo')},
		function () {$(this).removeClass('activo')}
	);
};

MQT.publicidades = function () {
// control de visualización de publicidades
$("#cintillo,.robapaginas").each(
	function() {
		if ($(this).height() < 5 ){
		$(this).css('display', 'none');
	} 
	}
);
};

$(document).ready(function() {
	MQT.modulometeo();
	MQT.menunavegacion();
	MQT.navegacionsecciones();
	MQT.submenumas();
	MQT.fotothumbs();
	MQT.publicidades();
});


