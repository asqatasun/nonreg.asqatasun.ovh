var MODULOS = {};

MODULOS.slide = function(id,opciones) {
	opciones = (opciones != null) ? opciones : {};
	opciones.visibles = ((opciones != null) && !isNaN(opciones.visibles)) ? opciones.visibles : 5;
	opciones.pasos = ((opciones != null) && !isNaN(opciones.pasos)) ? opciones.pasos : opciones.visibles;
	if($(id).length == 1) {
		var total = $(".slide-contenidos .slide-contenido",$(id)).length;
		var total_visibles = (total > opciones.visibles) ? (total - opciones.visibles) : 0;
		if(total > 0) {
			var posicion = $(".slide-contenidos").position().left;
			var unidad = $(".slide-contenidos .slide-contenido:first",$(id)).outerWidth(true);
			var desplazamiento = unidad * opciones.pasos;
			var posicion_inicial = posicion;
			if(posicion > 0) posicion_inicial = 0;
			if(posicion < -((total-1)*unidad)) posicion_inicial = -(total_visibles*unidad);
			$(".slide-contenidos",$(id)).animate({"left": posicion_inicial + "px"},500);
			$(".slide-izquierda",$(id)).unbind('click').click(
				function() {
					posicion = $(".slide-contenidos").position().left;
					posicion = (posicion > -unidad) ? -(total_visibles*unidad) : posicion+desplazamiento;
					$(".slide-contenidos",$(id)).animate({"left": posicion + "px"},500);
				}
			);
			$(".slide-derecha",$(id)).unbind('click').click(
				function() {
					posicion = $(".slide-contenidos").position().left;
					posicion = (posicion <= -(total_visibles*unidad)) ? 0 : posicion-desplazamiento;
					$(".slide-contenidos",$(id)).animate({"left": posicion + "px"},500);
				}
			);
		}
	}
}

MODULOS.slideShow = function(numero) {
	var desplazamiento = 196;
	var items = 5;
	var total = $("#slideShowItems .item").length;
	var indice_origen = Math.round(-$("#slideShowItems").position().left / desplazamiento);
	var indice_destino = 0;

	if(total > 0) {
		// Recogemos el numero a desplazar
		if(!isNaN(numero)) indice_destino = numero;
		else if(numero == "+") indice_destino = (indice_origen == (total - items)) ? 0 : indice_origen + 1;
		else if(numero == "-") indice_destino = indice_origen - 1;

		// Chequeamos que el valor de numero es correcto
		if(indice_destino > (total - items)) indice_destino = total - items; // Chequeamos si estamos al final del carrusel
		if(indice_destino < 0) indice_destino = total - items;
		else if(indice_destino >= total) indice_destino = 0;

		// Dibujamos los items de navegacion
		$("#slideShowNews .navi a").removeClass("active");
		$("#slideShowNews .navi a").slice(indice_destino,(indice_destino + items)).addClass("active");
		// Desplazamos al Slide
		$("#slideShowItems").animate({"left": -indice_destino*desplazamiento + "px"}, 1000);
	}
}

MODULOS.slideDownUp = function(obj) {
	if($(obj).parent().parent().parent().find("#encuesta_formulario").is(":hidden")) {
		$(obj).parent().parent().parent().find("#encuesta_formulario").slideDown("slow");
	} else {
		$(obj).parent().parent().parent().find("#encuesta_formulario").slideUp("slow");
	}
}

MODULOS.miniportada = function(miniportada,numero) {
	numero = ((numero != null) && !isNaN(numero) && (numero >= 0)) ? numero : 5;
	if(($(miniportada).length == 1) && (typeof($(miniportada).attr("id")) != "undefined")) {
		idArr = $(miniportada).attr("id").split("-");
		var id = idArr[1];
		// Paginaci�n
		$(".pagination-more-less a",miniportada).removeClass("active");
		$(".pagination-more-less a.pagina_" + numero,miniportada).addClass("active");
		// Noticias
		var tension = new Array();
		// Saco los ids de las noticias de tensi�n y las marco
		$("*[id^='tension-']").each(
			function() {
				idArr = this.id.split("-");
				tension.push(idArr[1]);
			}
		);
		// Sacamos las noticias
		var indice = 1;
		$(".que-noticiaSecundarias[id^='miniportada-" + id + "-']",miniportada).each(
			function() {
				idArr = this.id.split("-");
				if($.inArray(idArr[2],tension) > -1) $(this).addClass("ocultar");
				else {
					if(indice > numero) $(this).addClass("ocultar"); else $(this).removeClass("ocultar");
					indice++;
				}
			}
		);
	}
}

MODULOS.lomas = function(contenedor,id) {
	if($(contenedor).length == 1) {
		$(".menus .menus2 ul li a",contenedor).removeClass("activo");
		$(".menus .menus2 ul li a#lomas_pestana_" + id,contenedor).addClass("activo");
		$(".locontenido .loultimo",contenedor).addClass("oculto");
		$(".locontenido .loultimo#lomas_contenido_" + id,contenedor).removeClass("oculto");
	}
}

/* Twitter */

MODULOS.twitter_comienzo = 1;
MODULOS.twitter_num_desplazamientos = 0; // Posici�n del array que vamos mostrando
MODULOS.twitter_array_li = new Array(); // Array con los id de los elementos
MODULOS.twitter_num_entradas_inicial = 5; // N�mero de entradas inicialmente

MODULOS.twitterObtenerDatos = function() {
	search = MODULOS.twitterFindWords();
	$.getJSON(
		'/backend/MODULOS.twitter.php?search='+search+'&nbLigne='+MODULOS.twitter_num_entradas_inicial,
		function(data) {
			MODULOS.twitterPintarDatos(data);
		}
	);
}

MODULOS.twitterPintarDatos = function(data) {
	//La primera vez ponemos comienzo a 0
	MODULOS.twitter_comienzo = 0;
	//Recorremos el array de datos y pintamos los resultados
	$('#titulo-twitter').text(data.request + ' en Twitter');
	items = data.items.reverse();
	$.each(items, function(i, item) {
		if($('#li-twitter-' + item.id).length == 0) {
			//Guardamos el id en el array de elementos
			MODULOS.twitter_array_li[MODULOS.twitter_array_li.length] = item.id;
			var item_html = '<li id="li-twitter-' + item.id + '" class="clearfix">';
			item_html+= '<div class="detalle">';
			item_html+= '<a class="trackerTwitter titulo" href="'+ item.author_url + '"><strong>@' + item.author_name + '</strong></a>';
			item_html+= item.author_text + '<p><span class="marcaTiempo">' + item.time + '</span> <a class="trackerTwitter" href="' + item.url + '">Ver tweed</a></p>';
			item_html+= '</div>';
			item_html+= '</li>';
			if(MODULOS.twitter_comienzo == 1) {
				$('#entradas-twitter').prepend(item_html);
				if(i >= MODULOS.twitter_num_entradas_inicial) {
					$('#li-twitter-' + item.id).hide(); // Ocultamos el elemento para mostrarlo cuando corresponda
				}
				else {
					MODULOS.twitter_num_desplazamientos++; // Al comienzo las entradas cuentan como desplazamientos
				}
			}
			else {
				$('#entradas-twitter').prepend(item_html);
				//Ocultamos el elemento para mostrarlo cuando corresponda
				$('#li-twitter-' + item.id).hide();
			}
		}
	});
	//La primera vez ponemos comienzo a 0
	MODULOS.twitter_comienzo = 0;
}

MODULOS.twitterDesplazar = function() {
	if(MODULOS.twitter_array_li.length > MODULOS.twitter_num_desplazamientos) {
		$('#li-twitter-' + MODULOS.twitter_array_li[MODULOS.twitter_num_desplazamientos]).show('normal');
		$('#li-twitter-' + MODULOS.twitter_array_li[MODULOS.twitter_num_desplazamientos - MODULOS.twitter_num_entradas_inicial]).hide();
		MODULOS.twitter_num_desplazamientos++;
	}
}

MODULOS.twitterFindWords = function() {
	var MonReg = new RegExp("^[a-z]{1,4}$", "i");
	var titulo = "";
	if($('#twitterid').html() != null) {
		titulo = $('#twitterid').html();
	}
	else if($('h1').html() != null) {
		titulo = $('h1').html(); //aqui necesitamos de replasar el buen id
	}

	palabras = titulo.split(' ');
	titulo='';
	for(i_palabra=0;i_palabra<palabras.length;i_palabra++)
	{
		palabra = palabras[i_palabra].replace(MonReg,"");
		if(palabra != ''){
			titulo += palabra+' ';
		}
	}
	return titulo;
}

/***********/

/* GenteQue */

MODULOS.genteque_comienzo = 1; // Indica si es la primera llamada
MODULOS.genteque_num_desplazamientos = 0; // Posici�n del array que vamos mostrando
MODULOS.genteque_array_li = new Array(); // Array con los id de los elementos
MODULOS.genteque_num_entradas_inicial = 5; // N�mero de entradas inicialmente

MODULOS.gentequeObtenerDatos = function(tipos) {
	tipos = (tipos != null) ? tipos : "";
	$.getJSON('/backend/MODULOS.genteque.php?tipos=' + tipos, function(data) {
		$("#entradas-genteque").empty();
		MODULOS.gentequePintarDatos(data);
	});
}

MODULOS.gentequePintarDatos = function(data) {
	//Recorremos el array de datos y pintamos los resultados
	$.each(data.items.reverse(), function(i, item) {
		if($('#li-genteque-' + item.id).length == 0) {
			//Guardamos el id en el array de elementos
			MODULOS.genteque_array_li[MODULOS.genteque_array_li.length] = item.id;
			//Montamos el html de la entrada
			var item_html = '<li id="li-genteque-' + item.id + '" class="noticia clearfix">';
			item_html+= '<div class="avatar S"><a href="http://gente.que.es/perfil.php?user=' + item.id_usuario_txt + '" class="titulo"><img src="' + item.avatar + '" alt="avatar - ' + item.id_usuario_txt + '" /></a></div>';
			item_html+= '<div class="conten"><a href="http://gente.que.es/perfil.php?user=' + item.id_usuario_txt + '" class="titulo">' + item.id_usuario_txt + '</a> <a class="' + item.actividad_class + '" href="' + item.link + '">' + item.actividad_texto + '</a><span class="marcaTiempo">Hace ' + item.actividad_minutos + ' min. </span></div>';
			//Comprobamos si tenemos que mostrar m�s info
			if(item.actividad_masinfo == 1) {
				item_html+= '<div class="btn-masInfo"><a id="a-' + item.id + '" href="javascript:;" rel="nofollow" onclick="MODULOS.gentequeMasInfo(' + item.id + ')">info</a></div>';
				//Comprobamos si tenemos elemento multimedia
				if(item.actividad_masinfo_media == 1) {
					var item_html_media = '<div class="media"><span></span><a href="' + item.link + '"><img src="'+ item.thumbnail +'" alt="thumbnail" border="0" /></a></div>';
				}
				else { var item_html_media = ''; }
				item_html+= '<div id="desplegable_masInfo"><ul id="ul-' + item.id + '" class="masInfo no-activo"><li class="clearfix"><div class="conten '+ item.class_content +'">' + item_html_media + '<h4><a href="' + item.link + '">' + item.titulo + '</a></h4><p>' + item.descripcion + '</p></div><div class="qn_comentar"><a class="num_comentarios" href="' + item.link + '#' + item.ancla_comenta + '"><span><span></span></span> ' + item.texto_comenta + '</a></div></li></ul></div>';
			}
			item_html+= '</li>';
			//Si es la primera vez que ejecutamos pintarDatos
			if(MODULOS.genteque_comienzo == 1) {
				$('#entradas-genteque').prepend(item_html);
				if(i >= MODULOS.genteque_num_entradas_inicial) {
					$('#li-genteque-' + item.id).hide(); // Ocultamos el elemento para mostrarlo cuando corresponda
				}
				else {
					MODULOS.genteque_num_desplazamientos++; // Al comienzo las entradas cuentan como desplazamientos
				}
			}
			else {
				$('#entradas-genteque').prepend(item_html);
				$('#li-genteque-' + item.id).hide(); // Ocultamos el elemento para mostrarlo cuando corresponda
			}
		}
	});
	//La primera vez ponemos comienzo a 0
	MODULOS.genteque_comienzo = 0;
}

MODULOS.gentequeDesplazar = function() {
	if(MODULOS.genteque_array_li.length > MODULOS.genteque_num_desplazamientos) {
		$('#li-genteque-' + MODULOS.genteque_array_li[MODULOS.genteque_num_desplazamientos - MODULOS.genteque_num_entradas_inicial]).hide("fast");
		$('#li-genteque-' + MODULOS.genteque_array_li[MODULOS.genteque_num_desplazamientos]).show("normal");
		MODULOS.genteque_num_desplazamientos++;
	}
	setTimeout("MODULOS.gentequeDesplazar()", MODULOS.gentequeAleatorio(5000, 15000));
}

MODULOS.gentequeMasInfo = function(id) {
	if($('#ul-' + id).hasClass('no-activo')) {
		$('#ul-' + id).show();
		$('#ul-' + id).removeClass("no-activo");
		$('#a-' + id).addClass('activo');
	}
	else {
		$('#ul-' + id).hide();
		$('#ul-' + id).addClass("no-activo");
		$('#a-' + id).removeClass('activo');
	}
}

MODULOS.gentequeAleatorio = function(inferior, superior) {
	numPosibilidades = superior - inferior
	aleat = Math.random() * numPosibilidades
	aleat = Math.floor(aleat)
	return parseInt(inferior) + aleat
}

/**********/

/* Meneame */

MODULOS.meneame_comienzo = 1;
MODULOS.meneame_num_desplazamientos = 0; // Posici�n del array que vamos mostrando
MODULOS.meneame_array_li = new Array(); // Array con los id de los elementos
MODULOS.meneame_num_entradas_inicial = 5; // N�mero de entradas inicialmente

MODULOS.meneameObtenerDatos = function() {
	$.getJSON(
		'/backend/MODULOS.meneame.php?nbLigne='+MODULOS.meneame_num_entradas_inicial,
		function(data) {
			MODULOS.meneamePintarDatos(data);
		}
	);
}

MODULOS.meneamePintarDatos = function(data) {
	//Recorremos el array de datos y pintamos los resultados
	items = data.items.reverse();
	$.each(items, function(i, item) {
		if($('#li-meneame-' + item.id).length == 0){
			//Guardamos el id en el array de elementos
			MODULOS.meneame_array_li[MODULOS.meneame_array_li.length] = item.id;
			var item_html = '<li id="li-meneame-' + item.id + '" class="yui-gf clearfix">';
			item_html+= '<div class="yui-u first">';
			item_html+= '<div id="meneame" class="votacion">';
			item_html+= '<span class="votos">'+item.vote+'</span>';
			item_html+= '</div>';
			item_html+= '</div>';
			item_html+= '<div class="yui-u details">';
			item_html+= '<a class="trackerMeneame" href="'+item.url+'"><h4>'+item.title+'</h4></a>';
			item_html+= '</div>';
			item_html+= '</li>';
			if(MODULOS.meneame_comienzo == 1) {
				$('#entradas-meneame').prepend(item_html);
				if(i >= MODULOS.meneame_num_entradas_inicial) {
					$('#li-meneame-' + item.id).hide(); // Ocultamos el elemento para mostrarlo cuando corresponda
				}
				else {
					MODULOS.meneame_num_desplazamientos++; // Al comienzo las entradas cuentan como desplazamientos
				}
			}else {
				$('#entradas-meneame').prepend(item_html);
				$('#li-meneame-' + item.id).hide(); // Ocultamos el elemento para mostrarlo cuando corresponda
			}
		}
	});
	MODULOS.meneame_comienzo = 0; // La primera vez ponemos comienzo a 0
}

MODULOS.meneameDesplazar = function() {
	if(MODULOS.meneame_array_li.length > MODULOS.meneame_num_desplazamientos) {
		$('#li-meneame-' + MODULOS.meneame_array_li[MODULOS.meneame_num_desplazamientos]).show('normal');
		$('#li-meneame-' + MODULOS.meneame_array_li[MODULOS.meneame_num_desplazamientos - MODULOS.meneame_num_entradas_inicial]).hide();
		MODULOS.meneame_num_desplazamientos++;
	}
}

/**********/

/* Blogs */

MODULOS.blogsPulso = function() {
	$.get(
		"/backend/USUARIOS.actividades.php",
		{id_actividad: "31,32,33", limite: 5},
		function(xml) {
			var obj_pulso = $("<ul></ul>");
			$("resp log",xml).each(
				function() {
					var usuario = $("nick_usuario",this).text();
					var actividad = "";
					var url = "";
					var apoyo = "";
					var mas_info = false;
					switch($("id_actividad",this).text()) {
						case "31":
							actividad = "Ha publicado un blog";
							url = $("url_blog",this).text();
							apoyo = "apoyo-nwBlog";
							break;
						case "32":
							actividad = "Ha comentado un post";
							url = $("url_post",this).text();
							apoyo = "apoyo-comentario";
							mas_info = true;
							break;
						case "33":
							actividad = "Ha publicado un post";
							url = $("url_post",this).text();
							apoyo = "apoyo-nwBlog";
							mas_info = true;
							break;
					}
					if(actividad != "") {
						var minutos = Math.round(((new Date().getTime())/1000 - parseInt($("timestamp",this).text()))/60);
						var obj_log = $('<li class="noticia clearfix"></li>');
						$(obj_log).append('<div class="avatar S"><img src="' + $("avatar",this).text() + '" alt="' + usuario + '" /></div>');
						$(obj_log).append('<div class="conten"> <a href="' + USUARIOS.url_comunidad + '/perfil.php?user=' + usuario + '" class="titulo">' + usuario + '</a><a href="' + url + '" class="' + apoyo + '">' + actividad + '</a> <span class="marcaTiempo">Hace ' + minutos + ' min.</span> </div>');
						if(mas_info) {
							$(obj_log).append('<div class="btn-masInfo"><a href="javascript:;" rel="nofollow">info</a></div>');
							$(obj_log).append('<div id="desplegable_masInfo"><ul class="masInfo no-activo"><li class="clearfix"><div class="conten"><h4><a href="' + url + '">' + $("titulo_post",this).text() + '</a></h4><p>' + $("texto",this).text() + '</p></div><div class="qn_comentar"><a class="num_comentarios" href="' + url + '#formulario-comentarios"><span><span></span></span> Comenta este post</a></div></li></ul></div>');
							$(".btn-masInfo a",obj_log).click(
								function() {
									if($(this).hasClass("activo")) {
										$(this).removeClass("activo");
										$("#desplegable_masInfo ul.masInfo",obj_log).addClass("no-activo");
									}
									else {
										$(this).addClass("activo");
										$("#desplegable_masInfo ul.masInfo",obj_log).removeClass("no-activo");
									}
								}
							);
						}
						$(obj_pulso).append(obj_log);
					}
				}
			);
			$("#blogs-pulso").empty().append(obj_pulso);
		},
		"xml"
	);
}

/**********/

/* Edici�n impresa */

MODULOS.impresaRefreshSelect = function(mes,destino) {
	if($(destino).length == 1) $("#mes_agenda",$(destino)).val(mes);
}

MODULOS.impresaCambioMes = function(mes,annio,destino) {
	if($(destino).length == 1)  {
		if($("#div_list",$(destino)).is(':visible') ){
			$("#contenidos",$(destino)).slideUp("slow",function(){
				$("#intro",$(destino)).show("fast");
				$("#div_list",$(destino)).hide("fast",function(){
					$("#contenidos",$(destino)).slideDown("slow");
				});
			});
		}
		//var annio = document.frmCalendario.annio.value;
		if(mes == 0) {
			annio--;
			mes = 12;
		}
		if(mes == 13) {
			annio++;
			mes = 1;
		}
		$.get("/backend/MODULOS.edicionImpresa.php?function=getCalendario",
			{mes: mes, annio: annio, destino: destino, r: Math.random()},
			function(data) {
				$("div#div_calendario",$(destino)).html(data);
			}
		);
	}
}

MODULOS.impresaListarEdicion = function(dia,mes,annio,destino) {
	if($(destino).length == 1)  {
		$.get("/backend/MODULOS.edicionImpresa.php?function=getListEdition",
			{mes: mes, annio: annio, dia: dia, r: Math.random()},
			function(data) {
				
				$("#div_list",$(destino)).html(data);
				$("#contenidos_in",$(destino)).slideUp("slow",function() {
					$("#div_list",$(destino)).html(data);
					$("#intro",$(destino)).hide("fast");
					$("#div_list",$(destino)).show("fast",function() {
						$("#contenidos_in",$(destino)).slideDown("slow");
						
						// EventTrack
					
						$('.listado_pdfs ul li a').each(function() {
								$(this).data('url', $(this).attr('href'));
								$(this).attr('href', 'javascript:;');
								$(this).click(function(){
										var campos = $(this).data('url').split('/');
										pageTracker._trackEvent('PDF', $(this).text(), campos[5].substring(0, 6));
										window.open($(this).data('url'), 'Hemeroteca');
									}
								);
							}
						);
						
					});
				});
				
				
				
			}
		);
	}
}

MODULOS.impresaGoTo = function(anno,destino) {
	MODULOS.impresaCambioMes($("#mes_agenda option:selected").val(),anno,destino);
}

/**********/

/* Meteo */

MODULOS.meteoTiempo = function(id_localidad) {
	id_localidad = ((id_localidad == null) && (typeof(USUARIOS.preferencias.meteo) != "undefined")) ? USUARIOS.preferencias.meteo : id_localidad;
	$.getJSON(
		"/backend/MODULOS.meteo.php",
		{accion: "tiempo", id_localidad: id_localidad},
		function(datos) {
			$("#cab-meteo-info").html(((datos.prevision[0] != "") ? '<img src="/meteo/iconos/cab_' + datos.prevision[0].manana + '.gif" /> ' : '') + datos.localidad + ' ' + datos.prevision[0].maxima + '/' + datos.prevision[0].minima + '&ordm;');
			if(!isNaN(id_localidad)) USUARIOS.personalizar({meteo: id_localidad});
			MODULOS.meteoCerrar();
		}
	);
}

MODULOS.meteoAbrir = function() {
	$("#cab-meteo-editar").addClass("activo");
	$("#cab-meteo-desplegable").show();
}

MODULOS.meteoCerrar = function() {
	$("#cab-meteo-editar").removeClass("activo");
	$("#cab-meteo-desplegable").hide();
	$("#cab-meteo-localidad").val("");
	$("#cab-meteo-localidades").empty();
	MODULOS.meteo_busqueda = 0;
}

MODULOS.meteo_busqueda = 0;
MODULOS.meteoLocalidades = function(q) {
	var meteo_busqueda = ++MODULOS.meteo_busqueda;
	$("#cab-meteo-localidades").empty();
	$.getJSON(
		"/backend/MODULOS.meteo.php",
		{accion: "buscar", q: q},
		function(datos) {
			if(meteo_busqueda == MODULOS.meteo_busqueda) {
				$(datos).each(
					function() {
						$("#cab-meteo-localidades").append('<li><a href="javascript:;" onclick="MODULOS.meteoTiempo(' + this.id_localidad + ')">' + this.nombre + '</a></li>')
					}
				);
			}
		}
	);
}

/**********/

/* Galer�as */

MODULOS.galeriaSlide = function(id,opciones) {
	opciones = (opciones != null) ? opciones : {};
	if($(id).length == 1) {
		$.getJSON(
			"/backend/FOTOS.json.php",
			{json: "galeria." + opciones.id_contenido + ".fotos.orden.asc.json"},
			function(galeria) {
				if((typeof(galeria.listado) != "undefined") && (galeria.listado.length > 0)) {
					var contenidos_jq = $(".slide-contenidos",$(id));
					$(contenidos_jq).empty();
					$(galeria.listado).each(
						function() {
							var foto = this;
							var item_jq = $('<div class="item slide-contenido"><img src="' + foto.fotos.thumb50 + '" width="48" /></div>');
							$(item_jq).click(
								function() {
									var url = (typeof(galeria.url) != "undefined") ? galeria.url : foto.url;
									$(".item",$(id)).removeClass("active");
									$(this).addClass("active");
									$(".media",$(id)).html('<span></span><a href="' + url + '"><img src="' + foto.fotos.modulo + '" /></a>');
								}
							);
							$(contenidos_jq).append(item_jq);
						}
					);
					MODULOS.slide(id);
					$(".item:first",contenidos_jq).addClass("active");
				}
			}
		);
	}
}

/**********/


/* Cabecera */

MODULOS.cabeceraFotos = function(destino,id_seccion) {
	var fotos = false;
	if($(destino).length == 1) {
		$(destino).unbind("hover");
		$(destino).hover(
			function () {
				if(fotos == false) {
					fotos = {};
					if($(".dsnoticias",destino).length == 1) {
						$.getJSON(
							"/backend/FOTOS.json.php",
							{json: "seccion." + id_seccion + ".fotos.fecha.desc.1.json"},
							function(fotos) {
								var fotos_html = "";
								if(typeof(fotos.listado) != "undefined") {
									for(i_foto=0;(i_foto<4) && (i_foto < fotos.listado.length);i_foto++) {
										fotos_html +=	'<div class="yui-u' + ((i_foto == 0) ? " first" : "") + '">' +
																'	<div class="photo"><span class="media-foto"></span><a href="javascript:;" onclick="window.location=\'' + fotos.listado[i_foto].url + '\'"><img src="' + fotos.listado[i_foto].fotos.thumb + '" alt="' + fotos.listado[i_foto].titulo + '" /></a></div>' +
																'	<a href="' + USUARIOS.url_comunidad + '/perfil.php?user=' + fotos.listado[i_foto].usuario + '" class="autor">' + fotos.listado[i_foto].usuario + '</a>' +
																'	<h4><a href="' + fotos.listado[i_foto].url + '">' + fotos.listado[i_foto].titulo + '</a></h4>' +
																'</div>';
									}
								}
								$(".dsnoticias",destino).html(fotos_html);
							}
						);
					}
				}
				$(this).addClass('activo')
			},
			function () {$(this).removeClass('activo')}
		);
	}
}

MODULOS.cabeceraVideos = function(destino,id_seccion) {
	var videos = false;
	if($(destino).length == 1) {
		$(destino).unbind("hover");
		$(destino).hover(
			function () {
				if(videos == false) {
					videos = {};
					if($(".dsnoticias",destino).length == 1) {
						$.getJSON(
							"/backend/EXTERNOS.json.php",
							{id_tipo: 24, id_seccion: id_seccion, limite: 4},
							function(videos) {
								var videos_html = "";
								if(typeof(videos) != "undefined") {
									for(i_video=0;(i_video<4) && (i_video < videos.length);i_video++) {
										videos_html +=		'<div class="yui-u' + ((i_video == 0) ? " first" : "") + '">';
										if(videos[i_video].thumb != '')
											videos_html +=	'	<div class="photo"><span class="media-video"></span><img src="' + videos[i_video].thumb + '" alt="' + videos[i_video].titulo + '" /></div>';
										videos_html +=		'	<a href="' + USUARIOS.url_comunidad + '/perfil.php?user=' + videos[i_video].usuario + '" class="autor">' + videos[i_video].usuario + '</a>' +
																	'	<h4><a href="' + videos[i_video].url + '">' + videos[i_video].titulo + '</a></h4>' +
																	'</div>';
									}
								}
								$(".dsnoticias",destino).html(videos_html);
							}
						);
					}
				}
				$(this).addClass('activo')
			},
			function () {$(this).removeClass('activo')}
		);
	}
}

MODULOS.cabeceraBlogs = function(destino,id_seccion) {
	var blogs = false;
	if($(destino).length == 1) {
		$(destino).unbind("hover");
		$(destino).hover(
			function () {
				if(blogs == false) {
					blogs = {};
					$(".cabecera_blogs",destino).empty();
					if($(".cabecera_blogs",destino).length == 1) {
						$.getJSON(
							"/backend/EXTERNOS.json.php",
							{id_tipo: 23, id_seccion: id_seccion, limite: 4},
							function(blogs) {
								var blogs_html = "";
								if(typeof(blogs) != "undefined") {
									for(i_blog=0;(i_blog<4) && (i_blog < blogs.length);i_blog++) {
										var blog_jq = '<div class="yui-u dsblogs"><div class="avatar"><a href="' + USUARIOS.url_comunidad + '/perfil.php?user=' + blogs[i_blog].usuario + '" class="autor"><img src="' + USUARIOS.url_comunidad + '/avatares/que/resize/30x30/generico.jpg"/></a></div><h4><a href="' + blogs[i_blog].url + '">' + blogs[i_blog].titulo + '</a></h4><p style="height: 18px; overflow: hidden;">' + blogs[i_blog].entradilla + '</p></div>';
										USUARIOS.datos(blogs[i_blog].usuario,{async: true, success: function(xml) {if($("usuario avatar",xml).text() != "") $(".avatar a img",blog_jq).attr("src",USUARIOS.url_comunidad + "/avatares/que/resize/30x30/" + $("usuario avatar",xml).text());}});
										$(".cabecera_blogs",destino).append(blog_jq);
									}
								}
							}
						);
					}
				}
				$(this).addClass('activo')
			},
			function () {$(this).removeClass('activo')}
		);
	}
}

/**********/
