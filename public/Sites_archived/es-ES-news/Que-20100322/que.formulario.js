<!--
	var formatoFecha = 'dd/mm/yyyy';	// Formato de fecha que vamos a utilizar
	var formatoHora = 'HH:mm';	// Formato de fecha que vamos a utilizar

	// Funci�n de validaci�n de email
	function emailValido(valor) {
		var EmailOk = true;
		var AtSym = valor.indexOf('@');
		var Period = valor.lastIndexOf('.');
		var Space = valor.indexOf(' ');
		var Length = valor.length - 1;
		if ((AtSym < 1) || (Period <= AtSym+1) || (Period == Length ) || (Space  != -1)) {
			  EmailOk = false;
		}
		return EmailOk;
	}

	// Funci�n de validaci�n de fecha
	function fechaValida(valor) {
		var formatoFechaReg = formatoFecha;
		formatoFechaReg = formatoFechaReg.replace('dd','([0-2][0-9]|3[0-1])');
		formatoFechaReg = formatoFechaReg.replace('mm','(0[0-9]|1[0-2])');
		formatoFechaReg = formatoFechaReg.replace(/y/g,'[0-9]');
		formatoFechaReg = "^" + formatoFechaReg + "$";
		var ExpReg = new RegExp(formatoFechaReg);
		var fechaOK = ExpReg.test(valor);
		return fechaOK;
	}

	// Funci�n de validaci�n de fecha
	function horaValida(valor) {
		var formatoHoraReg = formatoHora;
		formatoHoraReg = formatoHoraReg.replace('HH','([0-1][0-9]|2[0-3])');
		formatoHoraReg = formatoHoraReg.replace('mm','([0-5][0-9])');
		formatoHoraReg = "^" + formatoHoraReg + "$";
		var ExpReg = new RegExp(formatoHoraReg);
		var horaOK = ExpReg.test(valor);
		return horaOK;
	}

	// Funci�n de validaci�n de fotos
	function fileValido(valor,extension) {
		var formatoFile = "^(.*)(\\\\|/)[\\w:\\s/_-]+\\.(" + extension + ")$";
		var ExpRegFile = new RegExp(formatoFile,"i");
		var fileOK = ExpRegFile.test(valor);
		return fileOK;
	}

	// Funci�n de validaci�n de precios
	function precioValido(valor) {
		var formatoPrecio = "^[0-9]+(,[0-9]{1,2})?$";
		var ExpRegPrecio = new RegExp(formatoPrecio,"i");
		var precioOK = ExpRegPrecio.test(valor);
		return precioOK;
	}

	function checkForm(frm) {
		for(i=0;i<frm.length;i++) {
			campoForm = frm.elements[i];
			if(campoForm.disabled == false) {
				var datosCheck = campoForm.id.split('#');
				texto_explicativo = "";
				if(datosCheck[2])
					texto_explicativo = datosCheck[2];
				if(datosCheck[0].indexOf('*') != -1) {
					if(campoForm.value == "") {
						alert('No has rellenado el campo ' + datosCheck[1]);
						campoForm.focus();
						return false;
					}
				}
				if(datosCheck[0].indexOf('numero') != -1) {
					if(isNaN(campoForm.value)) {
						alert('El campo ' + datosCheck[1] + ' tiene que ser num�rico.' + texto_explicativo);
						campoForm.focus();
						return false;
					}
				}
				if((datosCheck[0].indexOf('email') != -1) && (campoForm.value != "")) {
					if(!emailValido(campoForm.value)) {
						alert('El campo ' + datosCheck[1] + ' tiene que ser un email');
						campoForm.focus();
						return false;
					}
				}
				if((datosCheck[0].indexOf('fecha') != -1) && (campoForm.value != "")) {
					if(!fechaValida(campoForm.value)) {
						alert('El formato del campo ' + datosCheck[1] + ' no es v�lido');
						campoForm.focus();
						return false;
					}
				}
				if((datosCheck[0].indexOf('hora') != -1) && (campoForm.value != "")) {
					if(!horaValida(campoForm.value)) {
						alert('El formato del campo ' + datosCheck[1] + ' no es v�lido');
						campoForm.focus();
						return false;
					}
				}
				if(datosCheck[0].indexOf('checkbox') != -1) {
					if(campoForm.checked == false) {
						alert('No has rellenado el campo ' + datosCheck[1]);
						return false;
					}
				}
				if(datosCheck[0].indexOf('radio') != -1) {
					var radioCheck = 0;
					eval('var radioElemento = frm.' + campoForm.name);
					for(j=0;j<radioElemento.length;j++) {
						if(radioElemento[j].checked) {
							radioCheck = 1;
							break;
						}
					}
					if(radioCheck == 0) {
						alert('No has rellenado el campo ' + datosCheck[1]);
						return false;
					}
				}
				if(datosCheck[0].indexOf('select') != -1) {
					if(campoForm.selectedIndex == 0) {
						alert('No has rellenado el campo ' + datosCheck[1]);
						return false;
					}
				}
				if(datosCheck[0].indexOf('selectMultiple') != -1) {
					seleccion = 0
					for(j=1;j<campoForm.length;j++) {
						if(campoForm.options[j].selected == true)
							seleccion = 1
					}
					if(seleccion == 0) {
						alert('No has rellenado el campo ' + datosCheck[1]);
						return false;
					}
				}
				var expReg = /[^A-Za-z0-9��������������_\s\�\?\�\!\<\>\.\,\:\;\(\)\@\#\$\�\%\&\\\/\*\=\+\-\{\}\[\]\�\�\�]/i;
				if(datosCheck[0].indexOf('parsear') != -1) {
					if(expReg.test(campoForm.value)) {
						alert('El campo ' + datosCheck[1] + ' no es v�lido\n');
						campoForm.focus();
						return false;
					}
				}
				if((datosCheck[0].indexOf('min') != -1) && (campoForm.value != "")) {
					posicion = datosCheck[0].indexOf('min') + 3;
					limite = "0";
					while((!isNaN(datosCheck[0].substring(posicion,posicion + 1))) && (posicion < datosCheck[0].length)) {
						limite = limite + datosCheck[0].substring(posicion,posicion + 1);
						posicion = posicion + 1;
					}
					limite = parseInt(limite,10);
					if(campoForm.value.length < limite) {
						alert('El campo ' + datosCheck[1] + ' tiene que tener al menos ' + limite + ' caracteres\n');
						campoForm.focus();
						return false;
					}
				}
				if((datosCheck[0].indexOf('max') != -1) && (campoForm.value != "")) {
					posicion = datosCheck[0].indexOf('max') + 3;
					limite = "0";
					while((!isNaN(datosCheck[0].substring(posicion,posicion + 1))) && (posicion < datosCheck[0].length)) {
						limite = limite + datosCheck[0].substring(posicion,posicion + 1);
						posicion = posicion + 1;
					}
					limite = parseInt(limite,10);
					if(campoForm.value.length > limite) {
						alert('El campo ' + datosCheck[1] + ' tiene que como m�ximo ' + limite + ' caracteres\n');
						campoForm.focus();
						return false;
					}
				}
				if(datosCheck[0].indexOf('foto') != -1) {
					if((campoForm.value != "") && !fileValido(campoForm.value,'jpeg|jpg|gif')) {
						alert('El formato de la foto no es correcta. Verifique que es un JPEG o un GIF o que el nombre no tiene acentos, espacios o caracteres extra�os.');
						campoForm.focus();
						return false;
					}
				}
				if(datosCheck[0].indexOf('mp3') != -1) {
					if((campoForm.value != "") && !fileValido(campoForm.value,'mp3')) {
						alert('El formato del mp3 no es correcto. Verifique que es un MP3 o que el nombre no tiene acentos, espacios o caracteres extra�os.');
						campoForm.focus();
						return false;
					}
				}
				if(datosCheck[0].indexOf('acepto') != -1) {
					if(campoForm.checked == false) {
						alert('Tienes que aceptar las condiciones');
						return false;
					}
				}
				if(datosCheck[0].indexOf('precio') != -1) {
					if((campoForm.value != "") && !precioValido(campoForm.value)) {
						alert('El formato del precio no es correcto. El formato correcto es 1000,11.');
						campoForm.focus();
						return false;
					}
				}
				if(datosCheck[0].indexOf('repetir') != -1) {
					if(campoForm.value != frm.elements[i-1].value) {
						alert('El campo ' + datosCheck[1] + ' no coincide.');
						campoForm.focus();
						return false;
					}
				}
			}
		}
		return true;
	}

	function envForm(frm,action) {
		if(checkForm(frm)) {
			if(typeof(action) != "undefined") {
				frm.action = action;
				frm.target = "";
			}

			if(typeof(frm.onsubmit) == 'function') frm.onsubmit();
			frm.submit();
		}
	}

	function irPag(frm,pag) {
		frm.pag.value = pag;
		frm.submit();
	}

	Array.prototype.in_array = function ( obj ) {
		var len = this.length;
		for ( var x = 0 ; x <= len ; x++ ) {
			if ( this[x] == obj ) return true;
		}
		return false;
	}


	function cargarSel(campo,datos_array,valor_seleccionado,inicio,annadir) {
		var j = 0;
		var arrDatos = new Array();

		if(valor_seleccionado == null) valor_seleccionado = "";
		if(inicio == null) inicio = 1;
		if(annadir == null) annadir = 0;
		if(campo.type.indexOf("select") != -1) {
			if(annadir != 1) borrarSel(campo,inicio);
			if(datos_array && (datos_array.constructor.toString().indexOf("Array") != -1)) {
				for(i=0;i<datos_array.length;i++) {
					valor = datos_array[i];
					texto = datos_array[i];
					if(datos_array[i].indexOf("###") != -1) {
						datos_opcion = datos_array[i].split("###");
						valor = datos_opcion[0];
						texto = datos_opcion[1];
					}
					indice = campo.length;
					campo.options[indice] = new Option(texto,valor);
					if(valor_seleccionado == valor) campo.options[indice].selected = true;
				}
			}
		}

		// LLamo a los onchange en cascada para recargar los select
		if(typeof(campo.fireEvent) != "undefined")
			campo.fireEvent("onchange");
		else {
			var evt = window.document.createEvent("HTMLEvents");
			evt.initEvent("change",true,true);
			campo.dispatchEvent(evt);
		}
	}

	function borrarSel(campo,inicio,selected) {
		if(inicio == null)
			inicio = 1;
		if(campo.type.indexOf("select") != -1) {
			for(i=campo.length-1;i>=inicio;i--) {
				if((selected == null) || (campo[i].selected == selected))
					campo.options[i] = null;
			}
		}
	}

	function selSel(campo,sel,inicio) {
		if(sel != false)
			sel = true;
		if(inicio == null)
			inicio = 1;
		if(campo.type.indexOf("select") != -1) {
			for(i=inicio;i<campo.length;i++)
				campo.options[i].selected = sel;
		}
	}

	function in_array(array,valor) {
	    var i;
	    for(i=0;i<array.length;i++) {
	        if(array[i] == valor)
	            return true;
	    }
	    return false;
	};
//-->
