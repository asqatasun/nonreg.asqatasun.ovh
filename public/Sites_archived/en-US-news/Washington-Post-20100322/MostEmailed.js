function MostEmailed(text,url,source)
{
  this.text   = text;
  this.url    = url;
 this.source = source;
}
var most_emailed = new Array(
new MostEmailed('The Republicans who stirred the tea','http://www.washingtonpost.com/wp-dyn/content/article/2010/03/21/AR2010032103484.html','Post'),
new MostEmailed('Lawmakers move to cut out the middleman','http://www.washingtonpost.com/wp-dyn/content/article/2010/03/21/AR2010032103548.html','Post'),
new MostEmailed('Yes, they made history','http://www.washingtonpost.com/wp-dyn/content/article/2010/03/21/AR2010032102642.html','Post'),
new MostEmailed('Divorce: Waiting out the downturn','http://www.washingtonpost.com/wp-dyn/content/article/2010/03/21/AR2010032103139.html','Post'),
new MostEmailed('Divided House passes health bill','http://www.washingtonpost.com/wp-dyn/content/article/2010/03/21/AR2010032100943.html','Post'),
new MostEmailed('Guam\'s support for military has its limits','http://www.washingtonpost.com/wp-dyn/content/article/2010/03/21/AR2010032101025.html','Post'),
new MostEmailed('When profit outweighs penalties','http://www.washingtonpost.com/wp-dyn/content/article/2010/03/19/AR2010031905578.html','Post'),
new MostEmailed('Can the Constitution stop health-care reform?','http://www.washingtonpost.com/wp-dyn/content/article/2010/03/19/AR2010031901470.html','Post'),
new MostEmailed('What the health-care bill means for you','http://www.washingtonpost.com/wp-srv/special/politics/what-health-bill-means-for-you/','Link'),
new MostEmailed('Chinese official: U.S. would lose trade war','http://www.washingtonpost.com/wp-dyn/content/article/2010/03/21/AR2010032101111.html','Post'),
new MostEmailed('A look at the health care overhaul bill','http://www.washingtonpost.com/wp-dyn/content/article/2010/03/21/AR2010032101637.html','AP Online'),
new MostEmailed('Balancing babies and books','http://www.washingtonpost.com/wp-dyn/content/article/2010/03/21/AR2010032102620.html','Post'),
new MostEmailed('A look at the health care overhaul bill','http://www.washingtonpost.com/wp-dyn/content/article/2010/03/22/AR2010032200410.html','AP Online'),
new MostEmailed('Michael Steele\'s disgraceful comments on Tea Party epithets','http://voices.washingtonpost.com/postpartisan/2010/03/michael_steeles_disgraceful_co.html','Link'),
new MostEmailed('A Mideast obstacle, ignored','http://www.washingtonpost.com/wp-dyn/content/article/2010/03/21/AR2010032101708.html','Post'),
new MostEmailed('The maestro\'s misconceptions','http://www.washingtonpost.com/wp-dyn/content/article/2010/03/21/AR2010032101707.html','Post'),
new MostEmailed('\'Tea party\' protesters accused of spitting on lawmaker, using slurs','http://www.washingtonpost.com/wp-dyn/content/article/2010/03/20/AR2010032002556.html','Post'),
new MostEmailed('Under Panetta, a more aggressive CIA','http://www.washingtonpost.com/wp-dyn/content/article/2010/03/20/AR2010032003343.html','Post'),
new MostEmailed('Low achieving? The label stings.','http://www.washingtonpost.com/wp-dyn/content/article/2010/03/19/AR2010031901362.html','Post'),
new MostEmailed('For senators, a long week lies ahead','http://www.washingtonpost.com/wp-dyn/content/article/2010/03/21/AR2010032103445.html','Post'),
new MostEmailed('Who does health-care reform help?','http://voices.washingtonpost.com/ezra-klein/2010/03/who_does_health-care_reform_he.html','Link'),
new MostEmailed('What a way to go','http://www.washingtonpost.com/wp-dyn/content/article/2010/03/21/AR2010032103129.html','Post'),
new MostEmailed('Peak blooms, peak crowding','http://www.washingtonpost.com/wp-dyn/content/article/2010/03/20/AR2010032002854.html','Post'),
new MostEmailed('Democrats\' education blockade','http://www.washingtonpost.com/wp-dyn/content/article/2010/03/19/AR2010031903679.html','Post'),
new MostEmailed('Out of the ashes to a lush, sustainable Wales','http://www.washingtonpost.com/wp-dyn/content/article/2010/03/18/AR2010031804977.html','Post')
);
