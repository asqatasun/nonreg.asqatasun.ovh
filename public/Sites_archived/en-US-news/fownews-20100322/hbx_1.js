function _hbxStrip(a){
	a = a.split("|").join("");
	a = a.split("&").join("");
	a = a.split("'").join("");
	a = a.split("#").join("");
	a = a.split("$").join("");
	a = a.split("%").join("");
	a = a.split("^").join("");
	a = a.split("*").join("");
	a = a.split(":").join("");
	a = a.split("!").join("");
	a = a.split("<").join("");
	a = a.split(">").join("");
	a = a.split("~").join("");
	a = a.split(";").join("");
	a = a.split(" ").join("+");
	return a;
}

var this_segment = "";
var date = new Date();
var is_dst = true;
var t_offst = is_dst?240:300;
var hours = date.getHours()+(date.getTimezoneOffset()-t_offst)/60;
if (5 <= hours && hours <= 23) { 
	this_segment_arr = [-1,-2,-3,-4,-5,-6,-7,-8,-9,-10,-11,-12,-13,-14,-15,-16,-17,-18,-19,-20]; 
	this_segment_arr[hours-5] = this_segment_arr[hours-5]*-1; 
	this_segment = this_segment_arr.join();
} 
var _hbEC=0,_hbE=[];function _hbEvent(a,b){b=_hbE[_hbEC++]={};b._N=a;b._C=0;return b;}
var hbx=_hbEvent("pv");hbx.vpc="HBX0100u";hbx.gn="tracking.foxnews.com";
hbx.fv="";hbx.lt="auto";hbx.dlf="n";hbx.dft="n";hbx.elf="n";hbx.seg=this_segment;hbx.acct="DM550608D8WV01EN3";hbx.pndef="title";hbx.ctdef="full";