/* Global Variables*/
var hideTimer=0, showTimer=0;
var time = 0;
var date = 0;



function convertTime(number) {
		number = Math.abs( number );
		var val = new Array( 5 );
		val[ 0 ] = Math.floor( number / 86400 / 7 );//weeks  
		val[ 1 ] = Math.floor( number / 86400 % 7 );//days  
		val[ 2 ] = Math.floor( number / 3600 % 24 );//hours  
		val[ 3 ] = Math.floor( number / 60 % 60 );//mins  
		val[ 4 ] = Math.floor( number % 60 );//secs  
		var stopage = false;
		var cutIndex = -1;
		for (var i = 0; i < val.length; i++) {
			if ( val[ i ] < 10 ) {
				val[ i ] = "0" + val[ i ];
			}
			if ( val[ i ] == "00" && i < ( val.length - 2 ) && !stopage ) {
				cutIndex = i;
			} else {
				stopage = true;
			}
		}
		val.splice( 0, cutIndex + 1 );
		return val.join(":");
	}
	
	function convertXmlTime(xmlDate) {
		
		
		 
		if (xmlDate == undefined || xmlDate == null || xmlDate == "") {
			    	
			    return "";
		   }
		   
	        var dt = new Date();
	        var time;
	        var day;
	        var when = "am";
		    var dtS = xmlDate.slice(xmlDate.indexOf('T')+1, xmlDate.indexOf('.'));
		    var TimeArray = dtS.split(":");
		    dt.setUTCHours(TimeArray[0],TimeArray[1],TimeArray[2]);
		    if (TimeArray[0] > 11) {
			    if (TimeArray[0] == 12) { 
				   TimeArray[0] = TimeArray[0];
				} else {
					TimeArray[0] = TimeArray[0] - 12;
				}
		        
		        when = "pm";
		    }
		   if (TimeArray[0] == 0) {
			  TimeArray[0] = 12;
		   }
		
		  
		
		
	        
	        time = TimeArray[0] + ":" + TimeArray[1] + when;
		
		    dtS = xmlDate.slice(0, xmlDate.indexOf('T'));
		    TimeArray = dtS.split("-");
		    dt.setUTCFullYear(TimeArray[0],TimeArray[1],TimeArray[2]);
		    day = TimeArray[1] + "/" + TimeArray[2] + "/" + TimeArray[0] + "";
		    
			if (TimeArray[1] == undefined || TimeArray[1] == "undefined" || TimeArray[1] == null || TimeArray[1] == "" ) {
				
				  return "";
			   }
		   
		
		    return day;
		    
		
	  }
	
	function showTividMenu(id) {
	
	 var subMenu = document.getElementById("tivid-submenu" + id);

	 if(subMenu) {

	    subMenu.className = "show-subMenu";
	 }
	}
	
	function hideTividMenu(id) {

	 var subMenu = document.getElementById("tivid-submenu" + id);
	
	 if(subMenu) {
	 subMenu.className = "hide-subMenu";
      }

	}

