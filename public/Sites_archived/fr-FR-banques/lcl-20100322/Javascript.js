function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i < a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i < d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i < d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i < (a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i < a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function changeImagesArray(array) {
	if (preloadFlag == true) {
		var d = document; var img;
		for (i=0;i<array.length;i+=2) {
			img = null; var n = array[i];
			if (d.images) {
				if (d.layers) {img = findElement(n,0);}
				else {img = d.images[n];}
			}
			if (!img && d.getElementById) {img = d.getElementById(n);}
			if (!img && d.getElementsByName) {
				var elms = d.getElementsByName(n);
				if (elms) {
					for (j=0;j<elms.length;j++) {
						if (elms[j].src) {img = elms[j]; break;}
					}
				}
			}
			if (img) {img.src = array[i+1];}
		}
	}
}

function changeImages() {
	changeImagesArray(changeImages.arguments);
}

function opentarget(page,target)
{
	window.open(page,target).focus();
}

function openpopup(page,largeur,hauteur,options)
{
	var top=(screen.height-hauteur)/2;
	var left=(screen.width-largeur)/2;
	window.open(page,"popup","top="+top+",left="+left+",width="+largeur+",height="+hauteur+","+options).focus();
}

function openparent(url)
{
	window.opener.navigate(url);
	window.opener.focus();
}
/* Pour le menu deroulant des simulateurs RV 11/10/2005 */
function apOpenSel(strId) {
	if (fe = document.getElementById(strId) ) {
		var fel = fe.options[fe.selectedIndex].value;
		openpopup(fel,620,600,'menubar=no,scrollbars=1,statusbar=no,resizable=yes');
	}
}
/* Pour les simulateurs Epargne 31/07/2006 */
function apOpenSim(strUrl) {
openpopup(strUrl, 620,600,'menubar=no,scrollbars=1,statusbar=no,resizable=yes');
	
}