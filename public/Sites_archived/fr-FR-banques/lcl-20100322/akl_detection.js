//version du 13/05/2009 -- modif pour tag Xiti au passage au vert et au click sur Activer

//D�claration des variables concernant les cookies.
//Cette page affiche des liens sur des pages utilisant la commande "innerHTML". C'est fou ce que l'on peut faire avec. Mais voil�, seules les derni�res versions des navigateurs savent l'utiliser : IE 5 et 6, Netscape 6 et 7, Mozilla, Firefox, Icab 2.9, Safari ou Camino
//status de la protection
var status = 0;
var objet;
//Gestion de la temporisation
var tempoBlocActif;
var compteurTempo=0;
//p�riode de la v�rification
var MSG_AKLNOTLOADED ='LCL - Avertissement :\r\n-------------------------- \r\n\nEn raison d\'un incident ou des param�tres de s�curit� de votre  \r\nordinateur, le dispositif de s�curisation des saisies n\'a pu �tre\r\nactiv�.\r\n\nPour conna�tre la configuration ad�quate, veuillez consulter\r\nle site \'\'S�curit� sur Internet\'\'.\r\n\n';
var MSG_POSTENONELIGIBLE='LCL - Avertissement :\r\n-------------------------- \r\n\nLa configuration ou la version de navigateur de votre ordinateur\r\nn\'ont pas permis d\'activer le dispositif de s�curisation des saisies.\r\n\nPour conna�tre la configuration ad�quate, veuillez consulter\r\nle site \'\'S�curit� sur Internet\'\'.\r\n\n';
var MSG_CHANGEMENT_ELIGIBILITE='Une modification a �t� apport� � votre poste, celle-ci a conduit � sa non �ligibilit�';

//P�riode de rafraichissement dans la t�ti�re.
checkPeriod = 3000;

//P�riode de rafraichissement dans la page d'accueil.
checkPeriodPa = 10000;

//R�cup�ration des param�tres contenus dans la page appelante.
var topPageAccueil = (page_akl=='Accueil');
var topTetiere = (page_akl=='Tetiere');
function mon_erreur(nouvelle,fichier,ligne)
{
	erreur = "L\'erreur suivante s\'est produite :\n"+ nouvelle+"\n"+fichier+"\n"+ligne;
 	affiche_erreur();
 	return true;
}
function gestionErreurNonChargement(nouvelle,fichier,ligne)
{
	status = -1;
 	composantNonCharge();
 	afficherBlocInactif();
 	return true;
}
function affiche_erreur()
{
 	alert(window.erreur)
}

//Top �ligibilit� du poste client du composant ActiveX
var alkEligibiliteActiveX = (module=='ActiveX'?true:false);
//Param�tre contenant le code qui permet d'ins�rer l'objet ActiveX si le poste est �ligible.
var insertionObjetActiveX ='';
//Param�tres li�s aux cookies akl.
var K_aklPath = '/';
var K_aklSecure ='false'; //'true';
var K_aklDomain ='.lcl.fr';

//Nom de cookie permettant d'indiquer si on est en mont�e en charge ou en g�n�ralisation
var K_aklGeneralisationCook = 'aklGeneralisation';

//Nom du cookie permettant d'indiquer que le client a activ� le dispositif.
var K_aklActivationCook = 'aklActivation';

//Nom du cookie permettant d'indiquer que le dispositif est bloqu�.
var K_aklBlocageCook = 'aklBlocage';

//Nom du cookie permettant de comptabiliser le nombre de fois que l'utilitisateur refuse de charger l'akl
var K_aklNonChargementCompteurCook = 'aklNonChargementCompteur';
//Nom du cookie permettant de conserver le version de l'active X
var K_aklVersionActivexCook = "aklVersionActivex";

//Valeurs des cookies
var V_NON = 'NON';
var V_OUI = 'OUI';
var V_REFRACTAIRE = 'REFRACTAIRE';
var V_AKLNONCHARGE ='AKLNONCHARGE';
var V_ACTIF = 'AKLACTIF';
var premAccesEnGen = false;
var changementEligibilitePoste = false;
var topRefractaire = false;
var topNonChargementAkl = false;
//On v�rifie si le composant est charg� ou non.
topNonChargementAkl=(lireCookie(K_aklActivationCook)==V_AKLNONCHARGE?true:false);

//Top chargement de la t�ti�re
var topChargementTetiere = false;

//On positionne les cookies pour les indicateurs de blocage du dispositif et de g�n�ralisation 
//lorsque la page appelante est la page d'accueil.
if(topPageAccueil)
{
	//On v�rifie s'il s'agit du premier acc�s en mode g�n�ralisation
	if((GeneralisationAkl && !lireCookie(K_aklGeneralisationCook))||(!GeneralisationAkl && !lireCookie(K_aklGeneralisationCook)))
	premAccesEnGen =true; 

	//On v�rifie si l'�ligibilit� du poste du client n'a pas �t� modifi�e. 
	if(alkEligibiliteActiveX && !lireCookie(K_aklGeneralisationCook))
	changementEligibilitePoste=true;
	//On positionne le cookie d'indication du bloacge du dispositif AKL.
	poserAklCookie(K_aklBlocageCook,(BlocageAkl?V_OUI:V_NON),getDateExpiration(),K_aklPath,K_aklDomain,K_aklSecure);
	//On positionne le cookie d'indication de la g�n�ralsation ou de mont�e en charge 
	poserAklCookie(K_aklGeneralisationCook,(GeneralisationAkl?V_OUI:V_NON),getDateExpiration(),K_aklPath,K_aklDomain,K_aklSecure);
	//On positionne le cookie d'indication du num�ro de version du composant ActiveX.
	poserAklCookie(K_aklVersionActivexCook,transformValueVtoP(versionActiveX),getDateExpiration(),K_aklPath,K_aklDomain,K_aklSecure);
}
if(topTetiere)
{
	var versionActiveXValue = lireCookie(K_aklVersionActivexCook);
	var versionActiveX = transformValuePtoV(versionActiveXValue);
}

//R�cup�ration de l'indicateur de la g�n�ralisation du dispositif AKL.
var topGeneralisation=(lireCookie(K_aklGeneralisationCook)=='OUI'?true:false);
//On v�rifie s'il s'agit de l'activation volontaire du client dans le processus de mont�e en charge.
var topJustActive = lireCookie(K_aklActivationCook)=='newActivation';

if(topJustActive||(topGeneralisation&&lireCookie(K_aklActivationCook)==null))
{
	poserAklCookie(K_aklActivationCook,V_ACTIF,getDateExpiration(),K_aklPath,K_aklDomain,K_aklSecure);
}
//On v�rifie si le client est r�fractaire
var topRefractaire = lireCookie(K_aklActivationCook)==V_REFRACTAIRE;

//R�cup�ration du top activation de l'AKL par le client 
var topActivationAkl =(lireCookie(K_aklActivationCook)==V_ACTIF||lireCookie(K_aklActivationCook)=='OUI'||topJustActive?true:false);
var topActivite		 =(lireCookie(K_aklActivationCook)==V_ACTIF?true:false);
var topBlocage    	 =(lireCookie(K_aklBlocageCook)=='OUI'?true:false);

function afficherForm()
{
	 //  var Objet = navigator;
 //  for( var i in Objet)
 //    alert( i + ' = '+ Objet[i]);
var Objet = document.FormProtectWeb;
   for( var i in Objet)
     alert( i + ' = '+ Objet[i]);
}
//Test Activit� globale du dispositif
if(!topBlocage)
{
	if(!topRefractaire)
	{
		//Test �ligibilit� composant
		if(alkEligibiliteActiveX)
		{
			//Test si on est en g�n�ralisation
			if(topGeneralisation || (!topGeneralisation && topActivationAkl))
			{
				//Test si le client a bien charg� le composant Active X
				if(!topNonChargementAkl )
				{
					if(topPageAccueil)
					insertionObjetActiveX = "<OBJECT classid=\"CLSID:4EFE4BE8-8771-4649-B3EF-D97374C8D2C2\" codebase=\"/v_1.0/img/akl/FormProtect.cab#version="+versionActiveX+"\" id=\"FormProtectWeb\"></OBJECT>";
					if(topTetiere)
					insertionObjetActiveX = "<OBJECT classid=\"CLSID:4EFE4BE8-8771-4649-B3EF-D97374C8D2C2\" codebase=\"/v_1.0/img/akl/FormProtect.cab#version="+versionActiveX+"\" id=\"FormProtectWeb\"></OBJECT>";
				}
				document.write(insertionObjetActiveX);
			}
			else
				nonActivationDuDispositif();
		}
	}
}
else
	 blocageDuDispositif();

//Cette fonction permet de v�rifier l'activit� de l'AKL dans les pages d'accueil.
function verifieActivitePa()
{
	verifieActivitePa2();
	if(alkEligibiliteActiveX)
	{
		var interval = setInterval("verifieActiviteRefreshPa()", checkPeriodPa);
	}
}	 
//Cette fonction permet de v�rifier l'activit� de l'AKL dans les pages d'accueil.
function verifieActivitePa2()
{
	//Test si le dispositif est bloqu� et si oui on retourne
	if(topBlocage)
	return true;
	
	//R�cup�ration du topactivit�
	topActivite		 =(lireCookie(K_aklActivationCook)==V_ACTIF?true:false);
		
	//Cas o� le poste est in�ligible et que l'on acc�de tente une activation volontaire
	if(!alkEligibiliteActiveX && topJustActive)
	{
		nonEligibiliteDuPoste();
		return true;
	}
	if(!alkEligibiliteActiveX)
	return true;
	
	//On v�rifie si le client est r�fractaire � l'AKL
	if(topRefractaire||topNonChargementAkl)
	{
		afficherBlocInactif();
		return true;
	}
	//Test activation globale du dispositif
	if(!topGeneralisation && !topActivationAkl)
	return true;
	//R�cup�ration du cookie d'�ligibilit�
	if(alkEligibiliteActiveX)
	{
		//Test si le client a activ� la protection
		if(topActivite)
				chargerBlocActif();
		else
				chargerBlocInactif()
	}
	else
	{
		if(changementEligibilitePoste)
		{
			//alert(MSG_CHANGEMENT_ELIGIBILITE);
			return true;
		}
		if(topJustActive||premAccesEnGen)
		nonEligibiliteDuPoste();
	}
}
//Cette fonction permet de v�rifier l'activit� de l'AKL dans les pages d'accueil.
function verifieActiviteRefreshPa()
{
	//Test si le dispositif est bloqu� et si oui on retourne
	if(topBlocage || !alkEligibiliteActiveX || (!topGeneralisation && !topActivationAkl))
	return true;
	
	//R�cup�ration du cookie d'�ligibilit�
	if(alkEligibiliteActiveX)
	{
		//R�cup�ration du topactivit�
		topActivite =(lireCookie(K_aklActivationCook)==V_ACTIF?true:false);
		//Test si le client a activ� la protection
		if(topActivite)
				chargerBlocActifRefreshPa();
		else
				chargerBlocInactif()
	}
}
//Cette fonction permet de v�rifier l'�tat d'activation du dispositif dans les t�ti�res.
function verifieActivite()
{
	//Si le poste du client est in�ligible.
	if(!alkEligibiliteActiveX)
	return true;
	
	//Test si le dispositif n'est pas bloqu� et si oui on retourne.
	if(topBlocage)
	return true;
		
	//On r�cup�re l'�tat de l'activit� de l'akl
	topActivite =(lireCookie(K_aklActivationCook)==V_ACTIF?true:false);
	//On v�rifie si le client est r�fractaire � l'AKL
	if(topRefractaire||topNonChargementAkl)
	{
		afficherBlocInactif();
		return true;
	}
	//Test activation globale du dispositif
	if((!topGeneralisation && !topActivationAkl))
	return true;
	
	//R�cup�ration du cookie d'�ligibilit�
	if(alkEligibiliteActiveX)
	{
		topChargementTetiere = true;
		detecterActive();
		var interval = setInterval("detecterActive()", checkPeriod);
	}
}

function detecterActive()
{
		topActivite =(lireCookie(K_aklActivationCook)==V_ACTIF?true:false);
		//Test si le client a activ� la protection
		if(topActivite)
				chargerBlocActifTetiere();
		else
				chargerBlocInactif()
}
function activerProtection()
{
	//On v�rifie si le client est r�fractaire � l'AKL, on pose le cookie d'activation et on recharge la page
	if(topRefractaire)
	{
		poserAklCookie(K_aklActivationCook,V_ACTIF,getDateExpiration(),K_aklPath,K_aklDomain,K_aklSecure);
		rechargerPage();
		return true;
	}
	if(lireCookie(K_aklActivationCook)==V_AKLNONCHARGE)
	{
		poserAklCookie(K_aklActivationCook,V_ACTIF,getDateExpiration(),K_aklPath,K_aklDomain,K_aklSecure);
		rechargerPage();
		return true;
	}
	
	poserAklCookie(K_aklActivationCook,V_ACTIF,getDateExpiration(),K_aklPath,K_aklDomain,K_aklSecure);
	chargerBlocActif();
	
}



function desactiverProtection()
{
	poserAklCookie(K_aklActivationCook,V_OUI,getDateExpiration(),K_aklPath,K_aklDomain,K_aklSecure);
	chargerBlocInactif();
}
function changementStatut()
{
	checkFormProtectStatus();
	tempoBlocActif= setTimeout("changementStatut()",500);
	if(status==1 || compteurTempo>=5000)
	{
		if(status==1)
			afficherBlocActif();
			else
				afficherBlocInactif();
		clearTimeout(tempoBlocActif);
	}
	compteurTempo=compteurTempo+500;
}
function  chargerBlocActif()
{
	checkFormProtectStatus();
	if(status==1)
		afficherBlocActif();
	else
	{
		if(status==0)
		{
			document.FormProtectWeb.StartProtect();
			compteurTempo=0;
			changementStatut();
		}
	}
		
}
function chargerBlocInactif()
{
	checkFormProtectStatus();
	if(status!=0 && status!=-1 )
	document.FormProtectWeb.StopProtect();
	checkFormProtectStatus();
	
	if(status==0||status==-1)
	afficherBlocInactif();
}
function  chargerBlocActifTetiere()
{
	checkFormProtectStatus();
	if(topChargementTetiere && status==0)
	{
		document.FormProtectWeb.StartProtect();
		topChargementTetiere=false;
	}
		
	if(status==1)
		afficherBlocActif();
	else
	{
		if(status==0)
		{
			afficherBlocInactif();
		}
	}
		
}
function  chargerBlocActifRefreshPa()
{
	checkFormProtectStatus();
	if(status==1)
		afficherBlocActif();
	else
	if(status==0)
		afficherBlocInactif();
}


//***Gestion du cookie de l'activation du dispositif par le client**********//
function lireCookie (name) {  
  var arg = name + "=";  
  var alen = arg.length;  
  var clen = document.cookie.length;  
  var i = 0;  
  while (i < clen) {    
    var j = i + alen;    
    if (document.cookie.substring(i, j) == arg)      
      return getCookieVal (j);    
    i = document.cookie.indexOf(" ", i) + 1;    
    if (i == 0) break;   
  }
  return null;
}
function poserAklCookie (name, value) {  
  var argv = poserAklCookie.arguments;  
  var argc = poserAklCookie.arguments.length;  
  var expires = (argc > 2) ? argv[2] : null;  
  var path = (argc > 3) ? argv[3] : null;  
  var domain = (argc > 4) ? argv[4] : null;  
  var secure = (argc > 5) ? argv[5] : false;  
  document.cookie = name + "=" + escape (value) + 
  ((expires == null) ? "" : ("; expires=" + expires.toGMTString())) + 
  ((path == null) ? "" : ("; path=" + path)) +  
  ((domain == null) ? "" : ("; domain=" + domain)) +    
  ((secure == true) ? "; secure" : "");
}

function retirerCookie(name) {  
  var exp = new Date();  
  exp.setTime (exp.getTime() - 1);  
  var cval = '' //GetCookie (name);  
  document.cookie = name + "=" + cval + "; expires=" + exp.toGMTString()+"; path=/";
}

function getCookieVal (offset) {
	var endstr = document.cookie.indexOf (";", offset);
  if (endstr == -1)
  endstr = document.cookie.length;
  return unescape(document.cookie.substring(offset, endstr));
}
//Cette fonction permet de v�rifier le coposant est charg� et son statut.
function checkFormProtectStatus() {
if(document.getElementById("FormProtectWeb")==null )
{
	status = -1;
	poserAklCookie(K_aklActivationCook,V_OUI,getDateExpiration(),K_aklPath,K_aklDomain,K_aklSecure);
	composantNonInsere();
}
else
{	
	objet = document.getElementById("FormProtectWeb");
	if(objet.value==undefined)
	{
		onerror=gestionErreurNonChargement;
		status = objet.GetProtectStatus();
		
	}else
		status = objet.GetProtectStatus();
}	
}	
function afficherBlocActif()
{
	document.getElementById("affichage").innerHTML=document.getElementById("_actif").innerHTML;
}
function afficherBlocInactif()
{
	document.getElementById("affichage").innerHTML=document.getElementById("_inactif").innerHTML;
}
//Cette m�tode permet de retourner une date pour l'expiration des cookies � j+100 ans
function getDateExpiration()
{
    var now = new Date();
    isIE = (document.all);
   return new Date(now.getYear() + (isIE ? 1 : 1901), now.getMonth(), now.getDate());
 }	
//Cette m�thode permet de g�rer le cas o� le composant n'est pas charg�
function composantNonCharge()
{
	poserAklCookie(K_aklActivationCook,V_AKLNONCHARGE,getDateExpiration(),K_aklPath,K_aklDomain,K_aklSecure);
	alertNonCharge();
}

//Cette m�thode permet de g�rer le cas des postes non �ligibles
function nonEligibiliteDuPoste()
{
	if(topPageAccueil)
	alertIneligible();
}
//Cette m�thode permet de g�rer le cas o� du cas ou on est en mont�e en charge et non activation du dispositif.
function nonActivationDuDispositif(){}
//Cette m�thode permet de g�rer le cas o� le dispositif est bloqu�
function blocageDuDispositif(){}
//Cette m�thode permet de g�rer le cas o� l'insertion n'a pas �t� faite
function composantNonInsere()
{
	poserAklCookie(K_aklActivationCook,V_AKLNONCHARGE,getDateExpiration(),K_aklPath,K_aklDomain,K_aklSecure);
}
function rechargerPage()
{
	document.location.reload()
}
//Cette fonction permet d'arreter le dispositif anti-keylogger s'il est actif
function stopAkl()
{
	if(alkEligibiliteActiveX)
	{
		var objet =document.getElementById("FormProtectWeb");
		if(objet=='[object]')
		{
			if(!(lireCookie(K_aklActivationCook)==V_AKLNONCHARGE?true:false) )
			{
				onerror=gestionErreurSansActionAkl;
				status = objet.GetProtectStatus();
				if(status==1)
				objet.StopProtect();
			}
		}
	}
}
//Cette fonction permet de g�rer les erreurs lors de l'arr�t de l'AKL
function gestionErreurSansActionAkl()
{
	return true;
}
//Cette fonction permet de faire une pause dans l'ex�cution du script
function pause(millis)
{
date = new Date();
var curDate = null;

do { var curDate = new Date(); }
while(curDate-date < millis);
}
//Cette fonction permet d'afficher les popups
function alertIneligible() {
alert(MSG_POSTENONELIGIBLE);
}
function alertNonCharge() {
alert(MSG_AKLNOTLOADED);
}
function transformValuePtoV(cookieValue){
  var finalValue = '';
  var reg=new RegExp("\\.", "g");
  finalValue = cookieValue.replace(reg,",");
  return finalValue;
}
function transformValueVtoP(cookieValue){
  var finalValue = '';
  var reg=new RegExp(",", "g");
  finalValue = cookieValue.replace(reg,".");
  return finalValue;
}


// XITI


function xitiDefini() {
	return (typeof xtsite=='string' && typeof xtn2 =='string'  && typeof xtpage == 'string');
}

function xitiActiver() {
	if (xitiDefini()) {
		var myd = document.getElementById('aklimg'); 
		if (typeof myd =='object') {
			myd.src='https://logs3.xiti.com/hit.xiti?s='+xtsite+xtn2+'&p='+xtpage+'::AKL_Activer';
			if (typeof xitiDebug == 'boolean') alert('xiti AKL activ�');
		}
		else if (typeof xitiDebug == 'boolean') alert('Element aklimg non d�fini:'+typeof myd);
	}
	else if (typeof xitiDebug == 'boolean') alert("Tag Xiti non d�fini");
}
var aklimgdone=false;
function xitiVert() {
	var s="";
	// placer dans ziid les diff�rent noms d'id parent du div 'affichage' contenant l'akl inscrust�
	var ziid=",zoneIdentification,";
	if (xitiDefini()) {
		var myd=document.getElementById('aklimg');
		var e = window.event;
		var targ=null;
		if (e!=null) {
			if (e.target) targ = e.target;
			else if (e.srcElement) targ = e.srcElement;
		}
		if (targ!=null) {
			if (targ.nodeType == 3)
			targ = targ.parentNode;
		}
		if (typeof myd == 'object') {
		   if (targ && aklimgdone != true) {
			for (var p=targ;p != null;p=p.parentNode) {
				s=((p!=null && typeof p.id == 'string') ? p.id : '')+'<'+s;
				if (ziid.indexOf(","+p.id+",")>-1) {
					myd.src='https://logs3.xiti.com/hit.xiti?s='+xtsite+xtn2+'&p='+xtpage+'::AKL_Vert';
					aklimgdone=true;
					if (typeof xitiDebug == 'boolean') alert('Xiti AKL vert');
					break;
				}
			}
		    }
		}
		else if (typeof xitiDebug == 'boolean') alert('Element aklimg non d�fini:'+typeof myd);
	}
	else if (typeof xitiDebug == 'boolean') alert("Tag Xiti non d�fini");
}

