// MOM Engine v1.2
// Cree par Nicolas LEVIEIL - Studio SQLI 05/2003

// -- Parametres -- //
 var iswit = new Array('off','on');
 var _lft = 6;
 var _rgt = 6;
 var _pimg = new Array("", "img/p.gif", "img/p.gif", "img/p.gif", "img/p.gif", "img/p.gif", "img/p.gif", "img/p.gif");
 var _onload = "";
 var agt=navigator.userAgent.toLowerCase();
 var mac = (agt.indexOf("mac")!=-1);
 var filetpart = "";

 var contarr = new Array()
 var _cts = new Array();
 var _lgth = new Array();
 var pref = new Array();
 var cref = new Array();
 var urls = new Array();
 var lvl = -1;
 var nbc = 0;
 var _stor = new Array('','','','','','','','','','');
 var _cind = new Array();
 var rel = new Array();
 var theid = new Array();
 var _st = "<style type=text/css>";
 for(i=1;i<_pimg.length;i++){
  tmpi = new Image();
  tmpi.src = _pimg[i];
 }
 var _hp = _lft+_rgt;
 var lo = null;
 var tp = 50;
 var inc = 0;
 var TM = 1;
 var ie5 = (document.all && navigator.appVersion.indexOf("5.") != -1);
 var ismac = (navigator.appVersion.indexOf("Mac") != -1);
 var macie5 = (ie5 && ismac);
 var lastback = "";

 function def(){ if(def.arguments[0]){ l = def.arguments[0];} else{ l = _lgth[nbc-1];}; lvl++; theid[lvl] = _cts[nbc] = 'ct'+nbc; prevlvl = _max(0, lvl-1); pref[nbc] = theid[prevlvl]; cref[nbc] = _cind[prevlvl]-inc; rel[nbc] = new Array(); rel[nbc] = def.arguments; if(rel[nbc][5]!=null){ lastback = rel[nbc][5]; filet = "<div class="+filetpart+"filet"+lastback+"><img src=\"img/p.gif\" height=1 width=1></div>";} else{ filet="";}; _lgth[nbc] = l; _cind[lvl] = 0; _stor[lvl] += "<div class=abs"+lastback+" id="+theid[lvl]+" style=\"width:"+(l-2*1)+"\">"+filet; nbc++; if(CM.e){ CM.e(l);};}
 function add(l, u){ _stor[lvl] += CM.b(l, u); _cind[lvl]+=inc;}
 function clo(){ _stor[lvl] += "</div>"; lvl--;}
 function pos(o, l, t){ o.left = l; o.top = t;}
 function showtm(){ TM=0; }
 function show(){
  TM=0;
  if(lo){
   if(lo==this)
    return;
   if(this.bloc.prnt!=lo){
    if(lo.chld)
     lo.chld.hide();
    lo.hide();
    if(this.bloc!=lo.bloc){
     while((lo)&&(this.bloc!=lo.bloc)){
      lo = lo.bloc.prnt;
      if(lo){
       if(lo.chld)
        lo.chld.hide();
       lo.hide();
      }
     }      
    }
   }
  }
  this.show();
  if(this.chld)
   this.chld.show();
  lo = this;
 }

 function hide(){ TM=1; setTimeout('tmhide()', tp);}
 
 function tmhide(){
  if(TM){
   while(lo){
    if(lo.chld)
     lo.chld.hide();
    lo.hide();
    lo.bloc.hide();
    lo = lo.bloc.prnt;
   }
  }      
 }

 function show1c(){ this.b.visibility = 'show';}
 function show1b(){ this.visibility = 'show';}
 function show1i(){ this.i.visibility = 'show';}
 function hide1c(){ this.b.visibility = 'hide';}
 function hide1b(){ this.visibility = 'hide';}
 function hide1i(){ this.i.visibility = 'hide';}

 function show2c(){ this.b.style.visibility = 'visible';}
 function show2b(){ this.style.visibility = 'visible';}
// function show2bis(){ if(this.first==lo){ this.first.hide(); lo = this.l;} else{ alert(this.bloc.prnt);show();}}
 function show2bis(){ show(this.l);}
 function show2i(){ this.i.src = this.ion;}
 function hide2c(){ this.b.style.visibility = 'hidden';}
 function hide2b(){ this.style.visibility = 'hidden';}
 function hide2i(){ this.i.src = this.ioff;}

 function show3c(){ this.className = this.con;}
 function show3b(){ this.className = 'v6on'+this.lastback;}
 function show3i(){ this.ion.className = 'v6ion';}
 function hide3c(){ this.className = this.coff;}
 function hide3b(){ this.className = 'v6off'+this.lastback;}
 function hide3i(){ this.ion.className = 'v6ioff';}

 function _max(a, b){ return ((a>b)?a:b);}
 function _go(u){ window.location.href = u;}
 function _go2(){ window.location.href = this.u;}
 function _extract(s){ return s.substring(s.lastIndexOf("/")+1, s.length);}
 function _takeoff(off, on, s){ theoff = s.lastIndexOf(off); return (s.substring(0, theoff)+on+s.substring(theoff+off.length, s.length));}

 function _b(l, u){ urls[theid[lvl]][_cind[lvl]] = u; return "<div class=v4off"+lastback+">"+l+"</div><div class=v4on"+lastback+">"+l+"</div><div class=v4img><img src='"+_pimg[lastback]+"'></div>";} 
 function _e(l){ urls[theid[lvl]] = new Array(); _st+="#"+theid[lvl]+"{width:"+(l-2*1)+"px;}";}
 function _s(){ _b = ''; _st+="</style>"; for(s=0;s<_stor.length;s++) _b+=_stor[s]; document.write(_st); document.write(_b);}
 function _l(o){ return new Array(o.pageX, o.pageY);}
 function _reload(){ window.location.reload();}
 function launch(){ CM._launch();}
 function _launch(){ return true;}
 function _load(){ CM.s(); _onload+="CM.o();"; eval("window.onload = function(){ "+_onload+"};");}
 
 function _cm(){
 }
 
 if(document.layers){
  filetpart = "v4";
  _cm.prototype.o = _org1;
  inc = 3;
  _cm.prototype.b = _b;
  _cm.prototype.e = _e;
  _cm.prototype.s = _s;
  _cm.prototype.l = _l;
  _cm.prototype.p = pos1;
  _cm.prototype.r = _reload;  
  _cm.prototype._launch = _launch;  
  _cm.prototype._load = _load;  
  tp = 1;
 }
 if(document.all){
  _cm.prototype.o = _org2;
  _cm.prototype.b = function(l, u){ return "<div class=off4"+lastback+"><div class=nochild"+lastback+">"+l+"</div></div><div class=on4"+lastback+" onmousedown=\"_go('"+u+"')\"><div class=nochild"+lastback+">"+l+"</div></div>";}
  inc = 2;
  _cm.prototype.s = function(){ _b = ''; for(s=0;s<_stor.length;s++) _b+=_stor[s]; document.write(_b);}
  _cm.prototype.l = function(o){  x = o.offsetLeft; y = o.offsetTop; while(o.offsetParent){ x += o.offsetParent.offsetLeft; y += o.offsetParent.offsetTop; o = o.offsetParent;} return new Array(x, y);}
  _cm.prototype.p = _cm.prototype.r = pos2;
//  _cm.prototype.r = _reload;
  _cm.prototype._launch =  function(){ CM.s(); _onload+="CM.o();"; eval("window.onload = function(){ "+_onload+"};");}
  _cm.prototype._load =  function(){ return true;}
 }
else if(document.getElementById){
  _cm.prototype.o = _org3;
  _cm.prototype.b = function(l, u){ return "<div class=offv6"+lastback+" onmousedown=\"_go('"+u+"')\"><div class=nochild>"+l+"</div></div>";}
  inc = 1;
  _cm.prototype.s = function(){ _b = ''; for(s=0;s<_stor.length;s++) _b+=_stor[s];document.write(_b);}
  _cm.prototype.l = function(o){  x = o.offsetLeft; y = o.offsetTop; while(o.offsetParent){ x += o.offsetParent.offsetLeft; y += o.offsetParent.offsetTop; o = o.offsetParent;} return new Array(x, y);}
  _cm.prototype.p = pos3;
  _cm.prototype.r = _reload;
  _cm.prototype._launch = _launch;  
  _cm.prototype._load = _load;  
  tp = 1;
 }   

 var CM = new _cm();


 function _org1(){
  for(c=0;c<nbc;c++){
  cont = eval("document."+_cts[c]);
  cont.show = show1b;
  cont.hide = hide1b;
  contarr[c] = cont;
  tmpcontcol = cont.layers;
  cont.contcol = new Array();
  if(tmpcontcol.length%3){
   for(o=0;o<(tmpcontcol.length-1);o++)
    cont.contcol[o] = tmpcontcol[o+1];
   tmpcontcol[0].onmouseover = showtm;
   reftop = 1;
  }
  else{ 
   cont.contcol = tmpcontcol;
   reftop = 1;
  }
  u = cont.contcol.length;
  l = _lgth[c];
  _rl = l-2;
  cont.clip.width = l;
  for(i=0;i<u;i+=3){
   thee = new Layer(l, cont);
   thel = cont.contcol[i];
   thel.b = thee.b = theb = cont.contcol[i+1];
   theb.l = thel;
   thee.il = cont.contcol[i+2];
   thel.clip.width = theb.clip.width = _rl;
   thee.clip.width = l;
   thel.clip.height = theb.clip.height = _max(thel.clip.height+2, thee.il.clip.height);
   thel.left = theb.left = thee.il.left = 1;
   thel.e = thee;
   thee.left = 0;
   thee.show = show1c;
   thee.hide = hide1c;
   thee.u = urls[_cts[c]][i];
   thee.captureEvents(Event.MOUSEOVER|Event.MOUSEOUT|Event.MOUSEDOWN);
   thee.onmouseover = show;
   thee.onmouseout = hide;
   thee.onmousedown = _go2;
   thee.bloc = cont;
   thee.visibility = 'inherit';
   if(i>0){
    thee.il.top = thel.top = theb.top = theprev;
    thee.top = thel.top;
    thee.clip.height = thel.clip.height;
    theprev = thel.top + thel.clip.height;
   }
   else{
    thee.top = reftop;
    thee.clip.height = thel.clip.height;
    thel.top = theb.top = thee.il.top = reftop;
    theprev = thel.top + thel.clip.height;
   }
   thee.il.visibility = 'inherit';
   thee.il.clip.height = 0;
  }
  cont.clip.height = cont.contcol[u-3].top + cont.contcol[u-3].clip.height + 1;
  
  if(pref[c]!=_cts[c]){
   pref[c] = thepref = eval("document."+pref[c]);
   cref[c] = thecref = thepref.contcol[cref[c]];
   thecref.e.chld = cont;
   cont.prnt = thecref.e;
   thecref.e.il.clip.height = thecref.b.clip.height;
  }
  else{
   pref[c] = thedec = eval("document."+rel[c][1]);
   cref[c] = null;
   theldec = new Layer(pref[c].clip.width, pref[c]);
   theldec.clip.height = pref[c].clip.height;
   thelink = thedec.document.links[0];
   ioffsrc = rel[c][4];
   ionsrc = _takeoff(iswit[0], iswit[1], ioffsrc);
   theldec.document.write("<layer left=0 top=0><a href='"+thelink+"'><img src='"+ionsrc+"' border=0></a></layer>");
   theldec.document.close();
   theldec.i = theldec.document.layers[0];
   theldec.i.visibility = 'show';
   theldec.i.visibility = 'hide';
   theldec.visibility = 'show';
   theldec.chld = cont;
   theldec.bloc = theldec;
   theldec.show = show1i;
   theldec.hide = hide1i;
   pref[c] = theldec;
   cont.prnt = theldec;   
  }
 }
 this.p();
 setTimeout('window.onresize = function(){ CM.r();}', 1000);
 }

 function _org2(){
  for(c=0;c<nbc;c++){
  contarr[c] = cont = document.all[_cts[c]];
  tmpcontcol = cont.children;
  cont.contcol = new Array();
  if(tmpcontcol.length%2){
   for(o=0;o<(tmpcontcol.length-1);o++)
    cont.contcol[o] = tmpcontcol[o+1];
  }
  else{ 
   cont.contcol = tmpcontcol;
   cont.contcol[0].style.marginTop = 1;
  }
  u = cont.contcol.length;
  l = _lgth[c];
  _rl = l-2*1;
  cont.style.width = l;
  for(i=0;i<u;i+=2){
   thel = cont.contcol[i];
   theb = cont.contcol[i+1];
   theb.style.top = thel.offsetTop;
   theb.style.width = thel.offsetWidth;
   thel.b = theb;
   thel.onmouseover = show;
   thel.show = show2c;
   thel.hide = hide2c;
   thel.bloc = theb.bloc = cont;
  }
  cont.style.height = cont.contcol[u-2].offsetTop + cont.contcol[u-2].offsetHeight + 1;
  cont.show = show2b;
  cont.hide = hide2b;
  cont.onmouseover = showtm;
  cont.onmouseout = hide;
  
  if(pref[c]!=_cts[c]){
   pref[c] = thepref = document.all[pref[c]];
   cref[c] = thecref = thepref.contcol[cref[c]];
   cont.prnt = thecref;
   thecref.chld = cont;
//   thecref.b.first = cont.children[0];
   lastback = thecref.className.substring(thecref.className.length-1, thecref.className.length);
   thecref.b.children[0].className = 'childon'+lastback;
   thecref.children[0].className = 'childoff'+lastback;
  }
  else{
   pref[c] = thedec = document.all[rel[c][1]];
   cref[c] = null;
   thelink = thedec.children[1];
   thedec.i = thedec.children[1].children[0];
   thedec.ioff = thedec.i.src;
   thedec.ion = _takeoff(iswit[0], iswit[1], rel[c][4]);
   thedec.chld = cont;
   thedec.bloc = thedec;
   thedec.show = show2i;
   thedec.hide = hide2i;
   cont.prnt = thedec;
  }
  }
  this.p();
  window.onresize = function(){ CM.r();}
 }

 function _org3(){
  for(c=0;c<nbc;c++){
   cont = document.getElementById(_cts[c]);
   cont.onmouseover = showtm;
   cont.onmouseout = hide;
   cont.show = show3b;
   cont.hide = hide3b;
   contarr[c] = cont;
   tmpcontcol = cont.childNodes;
   cont.contcol = new Array();
   if(tmpcontcol[0].className.indexOf("filet")==0){
    for(o=0;o<(tmpcontcol.length-1);o++)
     cont.contcol[o] = tmpcontcol[o+1];
   }
   else{ 
    cont.contcol = tmpcontcol;
    cont.contcol[0].style.marginTop = 1;
   }
   u = cont.contcol.length;
   l = _lgth[c];
   _rl = l-2*1-_hp;
   cont.style.width = l;
   cont.lastback = cont.className.substring(cont.className.length-1, cont.className.length);
   for(i=0;i<u;i++){
    thel = cont.contcol[i];
    thel.lastback = cont.lastback;
    thel.show = show3c;
    thel.hide = hide3c;
    thel.onmouseover = show;
    thel.onmouseout = hide;
    thel.con = 'onv6'+thel.lastback;
    thel.coff = 'offv6'+thel.lastback;
    thel.bloc = cont;
   }
   cont.sh = cont.style.height = cont.contcol[u-1].offsetTop + cont.contcol[u-1].offsetHeight + 1;
   if(pref[c]!=_cts[c]){
    pref[c] = thepref = document.getElementById(pref[c]);
/*    if(macie5)
     cref[c] = thecref = thepref.children[cref[c]];
    else*/
     cref[c] = thecref = thepref.contcol[cref[c]];
    thecref.childNodes[0].className = 'child';
    thecref.chld = cont;
    cont.prnt = thecref;
   }
   else{
    pref[c] = thedec = document.getElementById(rel[c][1]);
    cref[c] = null;
/*    if(macie5){
     thelink = thedec.children[1];
     thedec.i = thedec.children[1].children[0];
     thedec.ioff = thedec.i.src;
     thedec.ion = _takeoff(iswit[0], iswit[1], rel[c][4]);
     thedec.chld = cont;
     thedec.bloc = thedec;
     thedec.show = show2i;
     thedec.hide = hide2i;
     cont.prnt = thedec;   
    }
    else{*/
     thelink = thedec.childNodes[0].childNodes[0];
     ionsrc = _takeoff(iswit[0], iswit[1], rel[c][4]);
     thedec.ion = ion = document.createElement('IMG');
     locc = this.l(thedec);
     ion.style.left = locc[0];
     ion.style.top = locc[1];
     ion.src = ionsrc;
     ion.className = 'v6ioff';
     thelink.appendChild(ion);
     thedec.u = thelink;
     thedec.chld = cont;
     thedec.bloc = thedec;
     thedec.show = show3i;
     thedec.hide = hide3i;
     cont.prnt = thedec;
    //}
   }
  }
  this.p();
  window.onresize = function(){ CM.r();}
 }

 function pos1(){
  IW = _max(document.width, window.innerWidth-20*(window.pageYOffset>0));
  IH = window.innerHeight+window.pageYOffset-20*(window.pageXOffset>0);
  for(c=0;c<nbc;c++){
   if(contarr[c].layers.length%4)
    contarr[c].layers[0].clip.width = contarr[c].clip.width-2;
   if(cref[c]){
    nloc0 = pref[c].left+pref[c].clip.width-1;
    nloct = nloc0+contarr[c].clip.width;
    nloc0=(nloct>IW)?(pref[c].left-contarr[c].clip.width+1):nloc0;
    nloc1 = pref[c].top+cref[c].top-1;
    nloct = nloc1+contarr[c].clip.height;
    nloc1=(nloct>IH)?(IH-contarr[c].clip.height):nloc1;
    pos(contarr[c], nloc0, nloc1);
   }
   else{
    loc = this.l(pref[c]);
    nloc0 = loc[0]+rel[c][2];
    nloct = nloc0+contarr[c].clip.width;
    nloc0=(nloct>IW)?(nloc0-(nloct-IW)):nloc0;
    nloc1 = loc[1]+rel[c][3];
    pos(contarr[c], nloc0, nloc1);
    pref[c].onmouseover = show;
    pref[c].onmouseout = hide;
   }
  }
 }

 function pos2(){
  IW = _max(document.body.scrollWidth, document.body.clientWidth);
  IH = document.body.clientHeight;
  for(c=0;c<nbc;c++){
   if(cref[c]){
    nloc0 = pref[c].offsetLeft+pref[c].offsetWidth-1;
    nloct = nloc0+contarr[c].offsetWidth;
    nloc0=(nloct>IW)?(pref[c].offsetLeft-contarr[c].offsetWidth+1):nloc0;
    nloc1 = pref[c].offsetTop+cref[c].offsetTop-1;
    nloct = nloc1+contarr[c].offsetHeight;
    nloc1=(nloct>IH)?(IH-contarr[c].offsetHeight):nloc1;
    pos(contarr[c].style, nloc0, nloc1);
   }
   else{
    loc = this.l(pref[c]);
    nloc0 = loc[0]+rel[c][2];
    nloct = nloc0+contarr[c].offsetWidth;
    nloc0=(nloct>IW)?(nloc0-(nloct-IW)):nloc0;
    nloc1 = loc[1]+rel[c][3];
    pos(contarr[c].style, nloc0, nloc1);
    pref[c].onmouseover = show;
    pref[c].onmouseout = hide;
   }
  }
 }

 function pos3(){
  for(c=0;c<nbc;c++)
   contarr[c].className = 'abs'+contarr[c].lastback;
  if(macie5)
   sint = 20;
  else
   sint = 15;
  IW = window.innerWidth+window.pageXOffset-sint*(window.innerHeight<document.body.scrollHeight);
  IH = window.innerHeight+window.pageYOffset-sint*(window.innerWidth<document.body.scrollWidth);
  for(c=0;c<nbc;c++){
   contarr[c].className = 'v6on'+contarr[c].lastback;
   if(cref[c]){
    locc = this.l(cref[c]);
    locp = this.l(pref[c]);
    nloc0 = locp[0]+pref[c].offsetWidth-1;
    nloct = nloc0+contarr[c].offsetWidth;
    nloc0=(nloct>IW)?(locp[0]-contarr[c].offsetWidth+1):nloc0;
    nloct = locc[1]+contarr[c].sh;
    nloc1 = (nloct>IH)?(IH-contarr[c].sh):locc[1];
    pos(contarr[c].style, nloc0, nloc1-1);
   }
   else{
    loc = this.l(pref[c]);
    nloc0 = loc[0]+rel[c][2];
    nloct = nloc0+contarr[c].offsetWidth;
    nloc0=(nloct>IW)?(nloc0-(nloct-IW)):nloc0;
    nloc1 = loc[1]+rel[c][3];
    pos(contarr[c].style, nloc0, nloc1);
    pref[c].onmouseover = show;
    pref[c].onmouseout = hide;
    pref[c].onclick = function(){ _go(this.u);};
   }
  }
  for(c=0;c<nbc;c++){
   contarr[c].className = 'v6off'+contarr[c].lastback;
  }
 }

 fill();
 CM._load();
