// Fonctions pour le positionnement sous la souris et l'affichage/masquage d'un
// calque de bulle d'aide ou de texte d'info ou de menu contextuel.
// EID - Patrick PRUDENT - 07/06/2005

var isDHTML = 0;
var isID = 0;
var isAll = 0;
var isLayers = 0;
var idBulleOn = "";
var TextPoped = "";
var IEWinNav = (/MSIE (\d+\.\d+); [^Mac]/.test(navigator.userAgent));
var IEMacNav = (/MSIE (\d+\.\d+); Mac/.test(navigator.userAgent));
var IEWin6 =  (/msie|MSIE 6/.test(navigator.userAgent));
var IEWin6Plus = 0;

if (/MSIE (\d+\.\d+);/.test(navigator.userAgent))
{
	var ieversion=new Number(RegExp.$1)
 	if (ieversion>6) IEWin6Plus = 1;
}

if (document.getElementById)
{	isID = 1;
	isDHTML = 1;
}
else
{	if (document.all)
	{	isAll = 1;
		isDHTML = 1;
	}
	else
	{	browserVersion = parseInt(navigator.appVersion);
		if ((navigator.appName.indexOf('Netscape') != -1) && (browserVersion == 4))
		{	isLayers = 1;
			isDHTML = 1;
		}
	}
}
	
// Ref multinavigateur d'un objet ou de son style
function findDOM(objectID,withStyle) {
	if (withStyle == 1) {
		if (isID) { return (document.getElementById(objectID).style) ; }
		else {
			if (isAll) { return (document.all[objectID].style); }
			else {
				if (isLayers) { return (document.layers[objectID]); }
			};
		}
	}
	else {
		if (isID) { return (document.getElementById(objectID)) ; }
		else {
			if (isAll) { return (document.all[objectID]); }
			else {
				if (isLayers) { return (document.layers[objectID]); }
			};
		}
	}
}

// Affichage bulle d'aide automatique
function popUp(evt,objectID){
	popUpWithAll(evt,objectID,-10,15,-1,-1,true, '');
}
// Affichage bulle d'aide en relatif / souris
function popUpRelative(evt,objectID,offsetX,offsetY,autom){
	popUpWithAll(evt,objectID,offsetX,offsetY,-1,-1,autom, '');
}
// Affichage bulle d'aide en coordonn�es absolues
function popUpAbsolute(evt,objectID,Xabs,Yabs){
	popUpWithAll(evt,objectID,0,0,Xabs,Yabs,false, '');
}

// Affichage bulle d'aide avec texte dynamique
function popUpText(evt,o){
	popUpTextRelative(evt,o,10,15,true);
}
// Affichage bulle d'aide avec texte dynamique, en relatif
function popUpTextRelative(evt,o,offsetX,offsetY,autom){
	if (o.getAttribute('alt') != "") TextPoped = o.getAttribute('alt');
	o.setAttribute('alt','');
	o.setAttribute('title','');
	popUpWithAll(evt,'BullePourAlt',offsetX,offsetY,-1,-1,autom,TextPoped);
}
// Affichage bulle d'info (anc. information survol�e) avec texte dynamique, en relatif
function popUpTextRelativeInfo(evt,o,offsetX,offsetY,autom){
	for (var i=0;i< o.childNodes.length;i++)
	{
		if (o.childNodes[i].tagName)
		{
			if (o.childNodes[i].tagName.toLowerCase() == "img")
				if (o.childNodes[i].getAttribute('alt') != "") TextPoped = o.childNodes[i].getAttribute('alt');
		}
	}
	popUpWithAll(evt,'BullePourInfo',offsetX,offsetY,-1,-1,autom,TextPoped);
}
// Affichage menu contextuel
function popMenuCtx(evt,objectID,offsetX){
	popUpWithAll(evt,objectID,offsetX,-12,-1,-1,true, 'mnuctx');
}

function popUpWithAll(evt,objectID,offsetX,offsetY,Xabs,Yabs,autom,texte){

	if (isDHTML) {
		if (IEWinNav)
		{
			if (findDOM("iframeBulleAide",0) == null)
			{
				oIFrame = document.createElement("iframe");
				document.body.appendChild(oIFrame);
				oIFrame.setAttribute("id","iframeBulleAide");
				oIFrame.setAttribute("frameborder","0");
				oIFrame.setAttribute("scrolling","no");
				oIFrame.setAttribute("src","javascript:;");
				oIFrame.className = "ifrBulleAide";
			}
		}
		if (findDOM(objectID,0) == null)
		{	oDiv = document.createElement("div");
			oDiv = document.body.appendChild(oDiv);
			oDiv.setAttribute("id",objectID);
			oDiv.className = "bulleinit bulle blocbulle"; // bulleinit bulle : pour compatibilit� V1
		}
		domStyle = findDOM(objectID,1);
		dom = findDOM(objectID,0);
		if (Xabs != -1 && Yabs != -1) {
			topVal = Yabs;
			leftVal = Xabs;
		}
		else {
			scrollLeft = 0;
			elemWidth = (dom.offsetWidth)? dom.offsetWidth: ((dom.clip)? dom.clip.width: 0);
			if (IEWin6)
				scrollLeft = (evt.x && !isLayers)? document.body.scrollLeft: 0;
			else if (IEWin6Plus)
				scrollLeft = (evt.x && !isLayers)? ((document.body.scrollLeft==0)?document.documentElement.scrollLeft:document.body.scrollLeft): 0;
				
			x = (evt.pageX)? evt.pageX: evt.clientX;
			dx = offsetX;

			if (Xabs != -1) {
				leftVal = Xabs;
			}
			else {
				livePageWidth = (window.innerWidth)? window.innerWidth: document.body.clientWidth;
				leftVal = x + dx + scrollLeft;
				if (autom) {
					/* Si l'�l�ment sort de la page � gauche ou � droite, ceci le repositionne */
					if ((leftVal + elemWidth - scrollLeft) > livePageWidth)
					{	leftVal2 = x + scrollLeft - elemWidth + 10;
						if (leftVal2 >= 2)
						{ leftVal = leftVal2 }
						else
						{ leftVal = livePageWidth - elemWidth - 2; }
					}
				}
				if (leftVal < 2)
				{ leftVal = 2; }
			}
			if (Yabs != -1) {
				topVal = Yabs;
			}
			else {
				scrollTop = 0;
				livePageHeight = (window.innerHeight)? window.innerHeight: document.body.clientHeight;
				elemHeight = (dom.offsetHeight)? dom.offsetHeight: ((dom.clip)? dom.clip.height: 0);
				y = (evt.pageY)? evt.pageY: evt.clientY;
				dy = offsetY;
				if (IEWin6)
					scrollTop = (evt.y && !isLayers)? document.body.scrollTop: 0;
				else if(IEWin6Plus)
					scrollTop = (evt.y && !isLayers)? ((document.body.scrollTop==0)?document.documentElement.scrollTop:document.body.scrollTop): 0;
					
				topVal = y + dy + scrollTop;
				if (autom) {
					/* Si l'�l�ment sort de la page en bas ou en haut, ceci le repositionne */
					if ((topVal + elemHeight - scrollTop) > livePageHeight)
					{	if (texte == 'mnuctx') { topVal2 = livePageHeight - elemHeight + scrollTop; }
						else { topVal2 = y - 5 + scrollTop - elemHeight; }
						if (topVal2 >= 2)
						{ topVal = topVal2 }
						else
						{ topVal = livePageHeight - elemHeight - 2;
						  /* Pour �viter que le calque soit sous le curseur de souris */
						  if (texte != 'mnuctx' && leftVal-scrollLeft <= x && leftVal-scrollLeft+elemWidth >= x)
						  {	leftVal = x - dx + scrollLeft; }
						}
					}
				}
				if (topVal < 2)
				{ topVal = 2; }
			}
		}
		domStyle.top = topVal + "px";
		domStyle.left = leftVal + "px";
		//alert ("top:"+ domStyle.top + "|left:" + domStyle.left);
		domStyle.zIndex="2";
		if (texte != '' && texte != 'mnuctx')
		{ dom.innerHTML = "<p>"+texte.replace(/\. /g,".<br />")+"</p>";
		}
		domStyle.visibility = "visible";
		if (texte == 'mnuctx') { idBulleOn = objectID; }
		
		if (IEWinNav)
		{
			domIFrameStyle = findDOM("iframeBulleAide",1);
			domIFrameStyle.top = topVal + "px";
			domIFrameStyle.left = leftVal + "px";
			domIFrameStyle.height = (dom.offsetHeight)? dom.offsetHeight: ((dom.clip)? dom.clip.height: 0);
			domIFrameStyle.width = (dom.offsetWidth)? dom.offsetWidth: ((dom.clip)? dom.clip.width: 0);
			domIFrameStyle.visibility = "visible";
			domIFrameStyle.zIndex="1";
		}
	}
}

function popDown(objectID){
	if (isDHTML) {
		if (findDOM(objectID,0) != null)
		{
			domStyle = findDOM(objectID,1);
			domStyle.visibility = "hidden";
		}
		if (IEWinNav)
			popDownFrame('iframeBulleAide');
	}
}
function popDownFrame(objectID){
	if (isDHTML) {
		if (findDOM(objectID,0) != null)
		{
			domIFrameStyle = findDOM(objectID,1);
			domIFrameStyle.visibility = "hidden";
		}
	}
}
function popDownText(o) {
	o.setAttribute('alt',TextPoped);
	popDown('BullePourAlt');
}
function popDownInfo(){
	popDown('BullePourInfo');
}

function popDownMenuCtx(e)
{	var o;
	var x, y, xmin, xmax, ymin, ymax;
	if (idBulleOn == "") return;
	
	o = document.getElementById(idBulleOn);
	x = ((e.pageX)? e.pageX: e.x) + document.body.scrollLeft;
	y = ((e.pageY)? e.pageY: e.y) + ((document.all)? document.body.scrollTop: 0);
	xmin = o.offsetLeft;
	xmax = xmin + o.offsetWidth;
	ymin = o.offsetTop;
	ymax = ymin + o.offsetHeight;
	if (x <= xmin || x >= xmax || y <= ymin || y >= ymax)
	{	o.style.visibility = 'hidden';
		idBulleOn = "";
	}
}
function popInfoInit()
{
	if (isDHTML) 
	{
		attacherAction();
		var collImg = document.getElementsByTagName("img");
	   	var parentImg;
	   	var classParent;
	   	var texte;
	   	var pathImg
	   	for (var i=0;i< collImg.length;i++)
		{
			if (collImg[i].className.toLowerCase().indexOf("a_popinfotexte")>-1)
			{
				texte = collImg[i].getAttribute('alt');
				texte= texte.replace(/^\s+|\s+$/g,"");
				texte = " (" + texte + ")";
				parentImg = collImg[i].parentNode;
				if (!IEMacNav)
				{
					if (parentImg)
						parentImg.innerHTML = parentImg.innerHTML + "<span class='a_popinfotexte'>" + texte + "</span>";
				}
				classParent = parentImg.className;
				if (classParent!=null && classParent !="")
					parentImg.className = classParent + " a_popinfocompl";
				else
					parentImg.className = "a_popinfocompl";
				pathImg = collImg[i].src;
				if (pathImg.lastIndexOf("/")>-1)
					collImg[i].src = pathImg.substring(0,pathImg.lastIndexOf("/")) + "/rien.gif";
				else
					collImg[i].src = "rien.gif";
			}
		}
	}
}
function attacherAction() 
{
	var collSpan = document.getElementsByTagName("span");
	for (var i=0;i< collSpan.length;i++)
	{
		if (collSpan[i].className.toLowerCase().indexOf("a_popinfoevents")>-1)
			ajouterEvents(collSpan[i]);
	}
	var collTh = document.getElementsByTagName("th");
	for (var i=0;i< collTh.length;i++)
	{
		if (collTh[i].className.toLowerCase().indexOf("a_popinfoevents")>-1)
			ajouterEvents(collTh[i]);
	}
	var collTd = document.getElementsByTagName("td");
	for (var i=0;i< collTd.length;i++)
	{
		if (collTd[i].className.toLowerCase().indexOf("a_popinfoevents")>-1)
			ajouterEvents(collTd[i]);
	}
}
function ajouterEvents(obj)
{
	obj.onmouseout = function() {popDownInfo()};
	if (IEWinNav || IEMacNav)
		obj.onmouseover = function() {popUpTextRelativeInfo(event,this,-10,15,true)};
	else
	{
		obj.onmouseover = function(event) {popUpTextRelativeInfo(event,this,-10,15,true)};
		obj.onmousemove = function(event) {popUpTextRelativeInfo(event,this,-10,15,true)};
	}
}