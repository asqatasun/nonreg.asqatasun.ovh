var timer;
var totWidth = imgWidth*nbImages;
var imgWidthOffset = Math.round(imgWidth/10);

function scroll() {
    var scrollingLeft = parseInt($("#scrolling").css("left"));
    var scrollingOffset = scrollingLeft % totWidth;
    if (-totWidth < scrollingLeft) $("#scrolling").css("left", (scrollingLeft-1)+"px");
    else $("#scrolling").css({ left: scrollingOffset+"px" });
    timer = setTimeout("scroll()", 50);
}

function scrollFwd() {
    var scrollingLeft = parseInt($("#scrolling").css("left"));
    var scrollingOffset = scrollingLeft % totWidth;
    if (-totWidth < scrollingLeft) $("#scrolling").css("left", (scrollingLeft-1)+"px");
    else $("#scrolling").css({ left: scrollingOffset+"px" });
    timer = setTimeout("scrollFwd()", 1);
}

function scrollRwd() {
    var scrollingLeft = parseInt($("#scrolling").css("left"));
    var scrollingOffset = scrollingLeft % totWidth;
    if ((carWidth-totWidth) > scrollingLeft) $("#scrolling").css("left", (scrollingLeft+1)+"px");
    else $("#scrolling").css({ left: (scrollingOffset-totWidth)+"px" });
    timer = setTimeout("scrollRwd()", 1);
}

$(document).ready(function(){
    $("#scrolling").append($("#scrolling").html());
    
    $("#btFwd").hover(function(){
        clearTimeout(timer);
        scroll();
    }, function(){
        clearTimeout(timer);
        scroll();
    }
    ).mouseup(function(){
        clearTimeout(timer);
        scroll();
    }).mousedown(function(){
        clearTimeout(timer);
        scrollFwd();
    });
    
    $("#btRwd").hover(function(){
        clearTimeout(timer);
        scroll();
    }, function(){
        clearTimeout(timer);
        scroll();
    }
    ).mouseup(function(){
        clearTimeout(timer);
        scroll();
    }).mousedown(function(){
        clearTimeout(timer);
        scrollRwd();
    });
    
    $("#scrolling div").hover(function(){
        $("#scrolling div").not(this).children("img").fadeTo("fast", 0.7);
        $(this).children("img").animate({"width": "+="+imgWidthOffset+"px", "height": "+=13px"}, "fast");
        $(this).children("p:first").css("bottom", "45px").css("background-color", "#326c77").css("opacity", 0.7);
        $(this).children("p").show("fast");
        clearTimeout(timer);
    }, function(){
        $("#scrolling div").not(this).children("img").fadeTo("fast", 1.0);
        $(this).children("img").animate({"width": "-="+imgWidthOffset+"px", "height": "-=13px"}, "fast");
        $(this).children("p").hide("fast");
        scroll();
    }).click(function(e){
        location.href=($(this).children("p").eq(1).children("a:first").attr("href"));
    });
    
    scroll();
});

