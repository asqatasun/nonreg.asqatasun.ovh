<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<title>JQuery Cycle Plugin - Intermediate Demos (Part 1)</title>
<link rel="stylesheet" type="text/css" media="screen" href="jq.css" />
<link rel="stylesheet" type="text/css" media="screen" href="cycle.css" />
<script type="text/javascript" src="jquery-1.2.6.js"></script>
<script type="text/javascript" src="chili-1.7.pack.js"></script>
<script type="text/javascript" src="jquery.cycle.all.js"></script>
<script type="text/javascript" src="jquery.easing.1.1.1.js"></script>
<script type="text/javascript">
$.fn.cycle.defaults.timeout = 6000;
$(function() {
    // run the code in the markup!
    $('td pre code').each(function() {
        eval($(this).text());
    });
});
</script>
</head>
<body>
<div id="main">

<div id="demos">

    <table cellspacing="0" ><tr><td>
        <div id="s2" class="pics">
<img src='COMBAVA.jpg' width='270' height='270' />
<img src='http://www.bdbretagne.com/site/pub/images/ECO&#32;CO2.jpg' width='270' height='270' />
<img src='AFFICHE_PRET_PRI.jpg' width='270' height='270' />
<img src='AFFICHE_PRET_PR0.jpg' width='270' height='270' />
<img src='X2.jpg' width='270' height='270' />
<img src='Pub5.jpg' width='270' height='270' />
<img src='pub1.jpg' width='270' height='270' />
<img src='pub3.jpg' width='270' height='270' />
<img src='ELAN.jpg' width='270' height='270' />
<img src='ENVOL.jpg' width='270' height='270' />
<img src='http://www.bdbretagne.com/site/pub/images/PLEIN&#32;CIEL.jpg' width='270' height='270' />            <!--img src="images/pub1.jpg" width="270" height="270" />
            <img src="images/pub2.jpg" width="270" height="270" />
            <img src="images/pub3.jpg" width="270" height="270" />
			<img src="images/pub4.jpg" width="270" height="270" /-->
        </div>
        <div style="display:none;"><pre><code class="mix">$('#s2').cycle({
    fx:     'scrollDown',
    easing: 'bounceout',
    delay:  -2000
});</code></pre><div>
        </td></tr>
    </table>        

 
</div>
</div>


</body>
</html>
