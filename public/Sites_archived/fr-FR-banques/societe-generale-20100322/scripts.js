// JavaScript Document

$(document).ready(function(){
   // Menu centre
	$('#eimm_contenu_centre li a')
		.css( {backgroundPosition: "-15px 1px"} )
		.mouseover(function(){
			$(this).stop().animate({backgroundPosition:"(0px 1px)"}, {duration:200})
		})
		.mouseout(function(){
			$(this).stop().animate({backgroundPosition:"(-15px 1px)"}, {duration:200, complete:function(){
				$(this).css({backgroundPosition: "-15px 1px"})
			}})
		})
// Alpha sur les liens menu gauche et centre

	$("#eimm_menu_gauche li a").blend({speed:250});
	$(".eimm_menu_centre_lien").blend({speed:150});
	
	$("#eimm_col_particuliers a").blend({target:".eimm_menu_centre_lien.eimm_lien_particuliers", speed:150})
	$("#eimm_col_pro a").blend({target:".eimm_menu_centre_lien.eimm_lien_pro", speed:150})
	$("#eimm_menu_ent a").blend({target:".eimm_menu_centre_lien.eimm_lien_ent", speed:150})
	$("#eimm_menu_asso a").blend({target:".eimm_menu_centre_lien.eimm_lien_asso", speed:150})

	
// Chargement des actualitÃ©s
		$.ajax({
			type: "GET",
			url: "/com_data.xml",
			dataType: "xml",
			complete : function(data, status) {
			var donnees = data.responseXML;
                                       $(donnees).find('com').each(function()
                                {
                                if   ( $(this).find('flash').text().match('flash') )  { $("#"+$(this).attr('id')).append($(this).find('flash').text());  }
                                else { $("#"+$(this).attr('id')).append('<a xtclib="'+$(this).attr('id')+'" href="'+$(this).find('lien').text()+'" title="'+$(this).find('accroche').text()+'" '+$(this).find('popup').text()+' ><img src="'+$(this).find('image').text()+'" height="'+$(this).find('hauteur').text()+'" width="'+$(this).find('largeur').text()+'" /></a>'); }
                                });
			}});
		
// Lightbox
	$(".popin").fancybox({
		'zoomOpacity'	: 	true,
		'frameWidth'	:	855,
		'frameHeight'	:	625,
		'overlayShow'	:	true,
		'overlayOpacity':	0.3,
		'overlayColor'	:	'#000',
		'centerOnScroll' :	true,
		'hideOnContentClick' : false
	});

		
});
