


/**********
* Navigation
**********/

var NavLeft={
	init:function(id){
		var _id = $(id);
		if (!_id) return;
		var _ul = _id.getElement('ul');
		var _lis = _ul.getChildren();
		var _uls = _id.getElements('li ul');
		var _that = this;
		
		_uls.each(function(ul,i){
			var _li = ul.parentNode;
			
			if(!_li.hasClass('current')){
				ul.setStyle('display','none');
			}else{
				_that.current = _li;
			}
			
			$(_li).getElement('a').addEvent('click',function(e){
				e = new Event(e);
				e.stop(), e.preventDefault();
				
				if (!_li.hasClass('current')){
					$(ul).setStyle('display','');
					_that.current.removeClass('current');
					_that.current.getElement('ul').setStyle('display','none');
					_li.addClass('current');
					_that.current = _li;
				}
			});
		})
	}
}
/* inputValue : function
	Permet de mettre le texte par defaut d'un champ de type texte.
*/ 
function inputValue(elm, state) {
	elm.oldValue=elm.value;
	elm.onfocus=function() {
		if (!this.isChecking && this.value==this.oldValue) this.value='';			
	}
	elm.onblur=function() {
		if(this.value=='') this.value=this.oldValue;
	}
	if (!elm.isChecking) elm.onfocus();
}

function forceCheck(elm, linkedInput) {
	linkedInput = typeof linkedInput=="string" ? document.getElementById(linkedInput) : linkedInput;
	linkedInput.checked = elm.checked;
}


/* generates corners and others elements if needed */
function generateElements(parent, stringClasses){
	var i, x;
	parent = (typeof parent == "string") ? document.getElementById(parent) : parent;
	var content = parent || document.body;
	var div = content.getElementsByTagName("div");
	
	//recupere un node avec la class blockInsideParDefaut
	function getIsd(node, className) { return getNode(node, {className: (className || "blockInside")})};
	
	// fonction de creation d'un coin (b avec className) 
	function nc(clN) {var b = document.createElement("b");b.className=clN;return b;};
	
	//ajoute un element ou une liste d'elements (c) sur l'element x
	function add(x, c) {
		var i=0; if (!x) return; 
		if (c.length) for (i=0; i<c.length; i++) x.appendChild(c[i].cloneNode(true));
		else x.appendChild(c.cloneNode(true));
	};
	
	//-- creation des elements qui seronts clones --
	var corners = [nc("tl"), nc("tr"), nc("bl"), nc("br")]; //corners 
	var shadow = nc("specialShadow"); add(shadow, [nc("lt"), nc("rt"), nc("trame")]); // specialShadow
	var overtl = nc("overtl"); // overtl : coin arrodis supplementaire pour les blocks avec des bordures speciales
	
	// -- creation des coins ou autres elements -- 
	// parcours des divs pour leur rajouter les corners
	for (i=div.length-1; i>=0; i--) {
		x=div[i];
		if (!x.alreadyProcessed) {
			if (x.className.match(/\bblock\b/)) //block par defaut
				add(getIsd(x), corners);
			if (x.className.match(/\bdataArray\b/)) //block par defaut
				add(x, corners);
			if (x.className.indexOf("arrayBlock")!=-1) {
				add(x, corners);
			}
			if (x.className.match(/\bspecialShadow\b/)) // block avec ombre dessous
				add(x, shadow);
			if (x.className.match(/\bblockToggle|blockShadowSimple\b/)) { //blocks avec coins absolus sur tous les cotes.
				add(getIsd(x), overtl);
			}
			x.alreadyProcessed = true;	
			initOtherBlocks(x); // fonction d'initialisation d'autres blocks
		}
	}
	fixColumns();
}
/* initOtherBlocks() : fonction rajoute d'autres fonctionnalites sur differents blocks  (toggle, onglets),
   Cette fonction est forcement lancee depuis generateElements, et cela evite de faire une deuxieme passe sur les divs de la page
 */
function initOtherBlocks(x) {
	// block a onglets
	if (x.className.match(/\bblockTabs(Sub)?\b/)) //block d'onglets en general (gere tous types d'onglets).
		tabs.init(x);
	if (x.className.indexOf("blockToggle")!=-1) //block ouvert/ferme
		toggleBlock.init(x);
	if (x.className.indexOf("scrollH")!=-1)  //block de scroll horizontal
		scroll.init(x);
	if (x.className.match(/\bline\b/)) //si ligne de block, on la stocke dans un tableau
		linesOfBlocks.push(x);
}




/******* 
* Tabs
 *******/
var tabs = {
	init : function(elm) {
		var ul = getNode(elm, {nodeName:"ul"});
		if (!ul) return;
		var a = ul.getElementsByTagName("a");
		for (var i=0; i<a.length; i++) {
			if (!hasAttributes(a[i], {className:"nochange"}))  {
				a[i].onclick = function() {
					tabs.change(this);
					return false;
				}
			}
			tabs.size(a[i], ul);
		}
	},
	size : function(a, ul) {
		var newSize =  a.offsetHeight + (ul.offsetHeight-a.offsetHeight) - getVStyles(a);
		a.style[heightPropertyToUse] = (newSize<0) ? 0 : newSize + "px"; //IE doesn't compute size under 0
	},
	change : function(elm) {
		var i, n, tabs, ul, li, body, tabCtns, current=0, block;
		ul = getParent(elm, {nodeName:"ul", className:"tabs(Big|Sub)?"});
		li = getParent(elm, {nodeName:"li"});
		tabs = ul.getElementsByTagName("li");
		block = getParent(ul, {nodeName:"div", className:"blockTabs(Sub)?"});
		// get What is the index of the new Tab and remove otherClass "current"
		for (i=0; i<tabs.length; i++) {
			if (tabs[i]==li) {
				current = i;
				addClass(li, "current");
			} else 
				removeClass(tabs[i], "current");
		}
		//get the tabCtn blocks, and show the contentTab that is match with clicked tab
		body = getNode(block, {nodeName:"div", className:"body"});
		tabCtns = getChildNodes(body, {nodeName:"div", className:"tabCtn"});
		for (i=0; i<tabs.length; i++) {
			n = tabCtns[i];
			removeClass(n, "tabCurrent");
			if (i==current) {
				addClass(n ,"tabCurrent");
			}
		}
		fixHeights(block); fixCorners(); fixColumns();
	}
}

/*********
* ToggleBlock :  block qui s'ouvre et qui se ferme
**********/
var toggleBlock = {
	init : function(elm) {
		var head = getNode(elm, {className:"head"});
		if (head) 
			var a = getNode(head, {nodeName:"a"});
			if (a)
				if (!a.className.match(/\bnochange\b/)) {
					a.onclick = function() {
						toggleBlock.toggle(this);
						return false;
					}
					var span = getNode(head, {nodeName:"span"})
					if (span) span.onclick = a.onclick;
					
				}
	},
	// ajout les fonctionnalites du open/close (toggle);
	toggle : function(elm) {
		elm.blur();
		var scrollTop = document.body.scrollTop;
		var block = getParent(elm, {nodeName:"div", className:"blockToggle"});
		window[(hasAttributes(block, {className:"toggleClosed"}) ? "remove" : "add")+"Class"](block, "toggleClosed"); //addClass or RemoveClass
		fixHeights(block); fixCorners(); fixColumns();
		document.body.scrollTop = scrollTop;
	}
};


/******** 
* Scroll
**********/
var scroll = {
	time : 20,
	increment : 4,
	// initialise les blocks de scrolling en rajoutant les fonctions sur les boutons, ainsi qu'en alignant ce qui doit etre aligne
	init : function(elm) {
		var left, right, scrollIsd, scrollCtn, totalWidth = 0;
		left = getNode(elm, {className:"scrollLeft"});
		right = getNode(elm, {className:"scrollRight"});
		elm.scrollIsd = getNode(elm, {className:"scrollIsd"});
		function initButton(btn, sens) {
			btn.onmouseover = function() { scroll.scrollH(this, sens); };
			btn.onmouseout = function() { scroll.stop(this); };
		};
		initButton(left, -1);
		initButton(right, 1);
		function init() {scroll.initButtonsPosition(elm,[left,right]);};
		init();
		addOnLoadFunc(function() {
			init();
			scroll.sizeHContent(elm);
		})
	},
	// centre verticalement les boutons de scrolling
	initButtonsPosition : function(elm, arrElms) {
		removeClass(document.documentElement, 'hasJS');
		arrElms.each(function(btn) {
			if (btn) {
				btn.style.marginTop = (elm.offsetHeight-btn.offsetHeight)/2+"px";
				if (elm.scrollIsd.scrollWidth>elm.scrollIsd.offsetWidth) 
					btn.style.visibility = "visible";
			}
		});
		addClass(document.documentElement, 'hasJS');
		
	},
	//resize les contenus horizontaux, cela permet d'afficher les produits correctement sans qu'aucun ne soit coupe � l'affichage.
	sizeHContent : function(elm) {
		scroll.getScrollElm(elm, "scrollH");
		if (!elm.scrollElm) return;
		var prods = getNode(elm, {nodeName:"table", className:"prods"}); //on recupere le tableau de produits
		if (!prods || elm.scrollElm.scrollWidth<=elm.scrollElm.offsetWidth) return;
		
		if (prods.rows[0]) {
			var tds = prods.rows[0].cells;
		}
		var width=0, paddings=0, td, sumWidth=0;
		//on recupere le nombre maximum de produits qu'on peut afficher, on recupere aussi les paddings sur les TD 
		//et on joue avec les paddings pour reduire ou augmenter la largeur entre les produits
		for (var i=0; i<tds.length; i++) { 
			td = tds[i]	
			width+=td.offsetWidth;
			paddings += intStyle(td, "padding-left")+intStyle(td, "padding-right");
			if (width>elm.scrollElm.offsetWidth) break;
		}
		var lastProd = td;
		if (width-elm.scrollElm.offsetWidth>=lastProd.offsetWidth) return;
		var paddingsMin = 5*2*(i+1); //on calcule les paddings minimum qu'on peut accepter pour reduire les paddings afin d'afficher le dernier element qui est coupe
		var numberOfProductsToShow = (paddingsMin>=width-elm.scrollElm.offsetWidth) ? i+1 : i;
		for (var i=0; i<numberOfProductsToShow; i++) { 
			var td = tds[i];
			sumWidth += td.offsetWidth-intStyle(td, "padding-left")-intStyle(td, "padding-right");
		}
		var newPaddings = parseInt(((elm.scrollElm.offsetWidth-sumWidth)/numberOfProductsToShow)/2);
		var moduloPaddings = (elm.scrollElm.offsetWidth-sumWidth)%numberOfProductsToShow;
		for (var i=0; i<tds.length; i++) {
			var td=tds[i];
			td.style.paddingLeft = newPaddings + "px";
			td.style.paddingRight = newPaddings + (i<moduloPaddings ? 1 : 0) + "px";
		}
	},
	//scrollH() : fonction de scrolling (elle se rappelle elle meme, elle est initiee sur l'evenement onmouseover d'un bouton
	scrollH : function(elm, sens) {
		scroll.getScrollElm(elm, "scrollH");
		elm.scrollElm.scrollLeft += scroll.increment * sens;
		elm.scrollElm.scrollTimer = setTimeout(function() {scroll.scrollH(elm, sens)}, scroll.time);
	},
	getScrollElm : function(elm, className) {
		if (!elm.scrollElm) {
			var par = getParent(elm, {nodeName:"div", className:className});
			elm.scrollElm = getNode(par, {className:"scrollIsd"});
		}
	},
	//scrollStop() : stop le scrolling en se basant sur le timer accroche au block qui scrolle.
	stop : function(elm) {
		if (elm.scrollElm) 
			clearTimeout(elm.scrollElm.scrollTimer);
	}
};

/**************
* sizeBlocks : alignement des blocks en hauteurs
**************/
var linesOfBlocks=[];
function sizeBlocks(parentBlock) {
	function size(block, size){
		if (block){
			var body = block.className.match(/\bblock\b/) ? getNode(block, {nodeName:"div", className:"body"}) : block; //si on a une line ou bien un block
			if (body) body.style[heightPropertyToUse] = body.offsetHeight + size - getVStyles(body) + "px";
		}
	};
	linesOfBlocks.eachInv(function (line) { //les lignes fournies sont
		var units = getChildNodes(line, {className:"unit"});
		units.each(function(unit) {
			var blocks = getChildNodes(unit, {className:"(block|line)"}, {className:"(noresize|blockTabs)"});
			var sizeToApply = line.offsetHeight-unit.offsetHeight;
			var sizePerBlock = parseInt(sizeToApply/blocks.length);
			blocks.each(function(block) {
				size(block, sizePerBlock);
			});
			//sur une division on tombe parfois sur un calcul pas precis, on resize le dernier element d'un unit, afin que le calcul soit correct
			if (blocks.length>1) size(blocks.last(), line.offsetHeight-unit.offsetHeight);
		});
	});
}

/***************
* fixColumns()
****************/
function fixColumns() {
	function fix() {
		var colonnes = ['main','rightColumn'],
			colonnesInside = ["", ""];
		var hMax=0, i, b, minMax=0, sum=0; hToU = heightPropertyToUse;
		
		function each(f) { //fonction d'iteration
			for (i=0; i<colonnes.length; i++){
				b = $(colonnes[i]);
				f();
			}
		}
		
		each(function() {
			sum=0;
			if (b) {
				getChildNodes(b, {}).each(function() { sum+=this.offsetHeight });
				if (sum>minMax) minMax=sum;
			}
		});
		// on remet la hauteur par defaut a toutes les colonnes (hauteur minimum)
		each(function() {
			var bToSize = $(colonnesInside[i]) || b;
			if (bToSize) bToSize.style[hToU] = minMax + "px";
		});
		// on recupere la hauteur la plus grande
		each(function() {
			if (b){
				if (b.offsetHeight + intStyle(b, "margin-top") >= hMax) {
					hMax = b.offsetHeight - intStyle(b, "margin-top");
				}
			}
		});
		// on applique la nouvelle hauteur sur les colonnes
		each(function() {
			var bIsd = $(colonnesInside[i]);
			if (b && bIsd) {
				var diff = (hMax - b.offsetHeight) + bIsd.clientHeight - getVStyles(bIsd);
				bIsd.style[hToU] = diff + 'px';
			} else if (b) {
				getVStyles(b);
				b.style[hToU] = hMax  - getVStyles(b) - intStyle(b, "margin-top") + 'px';
			}
			
		});
	};
	setTimeout(fix,1);
}

/*************
* PopLayer
**************/
var popLayer = {
	template : '<b class="popt"><b></b></b><div class="popInside"><b class="popl"></b><b class="popr"></b><div class="popBody"><div class="popHead"><b class="close"></b></div><div class="popContent">Content Here</div></div></div><b class="popb"><b></b></b>',
	pop : null,
	currentPosition:null,
	popContent : null,
	timeout : null,
	elmSource : null,
	eventsArray : ["click", "mouseover", "mouseout", "mousemove", "mouseup", "mousedown", "keyup", "keydown", "keypress", "abort", "blur", "change", "dblclick", "error", "load", "reset", "resize", "select", "submit"],
	init : function(parent) {
		if (!popLayer.pop) {
			parent = (typeof parent =="string") ? document.getElementById(parent) : parent;
			parent = parent || document.body;
			var pop = document.getElementById("popLayer");
			if (!pop) {
				pop = parent.appendChild(document.createElement("div"));
				pop.id = "popLayer";
				pop.innerHTML = popLayer.template;
			}
			popLayer.pop = pop;
			popLayer.popContent = getNode(pop,  {nodeName:"div", className:"popContent"});
			pop.style.marginLeft = -pop.offsetWidth/2+"px"
			var close = getNode(pop, {className:"close"});
			close.onclick = function() { popLayer.close(); };
		}
		popLayer.close();
		popLayer.pop.style.width = "";
		popLayer.pop.className = "";
		popLayer.pop.style.display = "block";
		popLayer.popContent.innerHTML = "";
		popLayer.popContent.style.height = "";
	},
	openHtml : function(elm, content, width, height, mask) { popLayer.open(elm, 'html', content, width, height, mask); },
	openTag : function(elm, content, width, height, mask) { popLayer.open(elm, 'tag', content, width, height, mask); },
	openUrl : function(elm, content, width, height, mask) { popLayer.open(elm, 'url', content, width, height, mask); },
	open : function(elm, type, content, width, height, mask) {
		popLayer.elmSource = elm;
		popLayer.init();
		popLayer.setPosition(elm);
		switch(type) {
			case 'html' : 
				popLayer.popContent.innerHTML = content;
				break;
			case 'tag' : 
				content = typeof(content)=="string" ? document.getElementById(content) : content;
				popLayer.copyNodes(content, popLayer.popContent);
				break;
			case 'url' : 
				popLayer.pop.className = "iframe loading";
				popLayer.popContent.innerHTML = '<iframe class="popIframe" onload="popLayer.loaded()" frameborder="0"></iframe>'; //cause IE, we must use innerHTML instead of DOM functions
				var loader = popLayer.popContent.appendChild(document.createElement("b"));
				loader.className="loader";
				var ifr = getNode(popLayer.popContent, {nodeName:"iframe"});
				popLayer.iframe = ifr;
				ifr.src = content;
				popLayer.timeout = setTimeout(popLayer.loaded,3000);
				break;
			default :
				alert("Vous n'avez pas specifie le bon type de contenu");
				return;
		}
		if (mask) {
			popLayer.showMask();
		}
		setTimeout(function() {
			if (popLayer.iframe) {
				var pWin = popLayer.iframe.contentWindow || popLayer.iframe.window;
				if (pWin && elm) {
					pWin.onload = function() {
						popLayer.setPosition(elm);
					};
				}
			}
		}, 10);
		popLayer.resize(width, height);
		popLayer.setPosition(elm);
		popLayer.pop.style.visibility = "visible";
		ifrlayer.make(popLayer.pop);
	},
	close : function(url) {
		popLayer.hideMask();
		if (url) {
			if (window.parent) {
				window.parent.location.href=url;
			}
		}
		function close(from) {
			from.popLayer.hideMask();
			from.popLayer.pop.style.visibility = "hidden";
			from.popLayer.pop.style.display = "none";
			ifrlayer.hide(from.popLayer.pop);
		};
		if (window.parent) close(window.parent);
		ifrlayer.hide(popLayer.pop);
		try {close(window);}
		catch(e){ifrlayer.hide(popLayer.pop);};
		
	},
	resize: function(width, height) {
		var doc, objectSized;
		if (width) {
			var pWin = popLayer.iframe && (popLayer.iframe.contentWindow || popLayer.iframe.window);
			if (pWin) {
				popLayer.pop.style.width = width + (popLayer.pop.offsetWidth-popLayer.popContent.offsetWidth) + "px";
			}
			else
				popLayer.pop.style.width = width + ((width+"").match(/%/) ? "%" : "px");
		}
		if (height) { 
			if (popLayer.iframe) {
				if (isNaN(height)) {
					objectSized = height;
					height = objectSized.offsetHeight;
				}
				popLayer.iframe.style.height="10px";
				if (objectSized && objectSized.scrollHeight>=popLayer.iframe.offsetHeight) {
					popLayer.iframe.style.height = objectSized.scrollHeight + 1 + "px"; //on met la taille pour supprimer le scroller
					popLayer.iframe.style.height = "10px"; //on remet la taille � 10 pour avoir le bon scrollHeight
					popLayer.iframe.style.height = objectSized.scrollHeight + 1 + "px"; //on resize definitifement via le bon scrollHeight
				} else {
					popLayer.iframe.style.height = height+"px";
				}
			}
			else
				popLayer.popContent.style.height = height+"px";
		}
		
		var sides = getNodes(popLayer.pop, {nodeName:"b", className:"pop(r|l)"});
		sides.each(function(side) { side.style.height = side.parentNode.offsetHeight + "px"; });
		ifrlayer.make(popLayer.pop);
	},
	setPosition : function(elm, top, left) {
		popLayer.pop.style.marginLeft = -popLayer.pop.offsetWidth/2+"px";
		if (elm && typeof elm!="string") 
			popLayer.pop.style.top = findPos(elm)[1] + 10 + "px";
		else 
			popLayer.pop.style.top = document.documentElement.clientHeight>popLayer.pop.offsetHeight ? parseInt((document.documentElement.clientHeight-popLayer.pop.offsetHeight)/2) + document.documentElement.scrollTop + "px" :  document.documentElement.scrollTop + 10 + "px";
		ifrlayer.move(popLayer.pop);	
	},
	fixSize : function(elmSrc) {
		if (window.parent && window.parent.popLayer && window.parent.popLayer.iframe) {
			window.parent.popLayer.iframe.style.height="10px";
			window.parent.popLayer.resize(null, document.documentElement.scrollHeight+1);
			window.parent.popLayer.setPosition(elmSrc);
		}
	},
	loaded : function(elm) {
		clearTimeout(popLayer.timeout);
		var from = window.parent || window;
		if (from) {
			from.popLayer.pop.className = from.popLayer.pop.className.replace(/loading/g, "");
			var obj = from.popLayer.iframe.contentWindow || from.popLayer.iframe.window;
			try {
				obj.close =  function() {
					return function() {
						popLayer.close();
					}();
				}
				if (document.all && window.print && !window.opera) {
					var objToSend = obj.document.body || obj.document.documentElement;
				} else {
					var objToSend = (obj.document.body && obj.document.documentElement && obj.document.body.scrollHeight>obj.document.documentElement.offsetHeight) ? obj.document.body : obj.document.documentElement;
				}
				popLayer.resize(obj.document.documentElement.scrollWidth+10, objToSend);
			} catch(e) {}
		}
		popLayer.setPosition(popLayer.elmSource);
		popLayer.elmSource = null;
	},
	copyNodes : function(sourceElm, destElm) {
		for (var i=0; i<sourceElm.childNodes.length; i++) {
			destElm.appendChild(sourceElm.childNodes[i].cloneNode(true));
		}
		var allSourceNodes = $n.getByTagName(sourceElm, "*");
		var allDestNodes = $n.getByTagName(destElm, "*");
		for (var i=0; i<allSourceNodes.length; i++) {
			popLayer.copyEvents(allSourceNodes[i], allDestNodes[i]);
		}
	},
	copyEvents : function(sourceElm, destElm) {
		for (var i=0; i<popLayer.eventsArray.length; i++) {
			var evt = "on"+popLayer.eventsArray[i];
			destElm[evt] = sourceElm[evt];
		}
	},
	
	showMask : function() {
		if (!popLayer.mask) {
			var div = document.createElement('div');
			div.id = 'popLayerMask';
			popLayer.mask = document.body.appendChild(div);
		}
		popLayer.mask.style.height = document.documentElement.scrollHeight +'px';
		popLayer.mask.style.width = document.documentElement.scrollWidth +'px';
		popLayer.mask.style.display = 'block';
		ifrlayer.make(popLayer.mask);
	},
	
	hideMask : function() {
		if (popLayer.mask) {
			popLayer.mask.style.display = 'none';
			ifrlayer.kill(popLayer.mask);
		}
	}
}

function showAndHide(field) {
		var allFields = getNodes(field.form, {name:field.name}, null, false, field.form.elements);
	allFields.each(function(field) {
		var elm = document.getElementById(field.value);
		elm.style.display = field.checked ? "block" : "none";
	});
	fixHeights(field);
}



/**********
* page print
*********/

/*recup�rer de la LCL*/


/**********
* font size
*********/
function SizeFont() {
	var mainContent = $('main');
	if(!mainContent) return;
	var oASmall = mainContent.getElement('#tools a.asmall');
	if(!oASmall) return;
	oASmall.addEvent('click', function(e) {
		e = new Event(e);
		e.stop();
		mainContent.addClass('normal');
		mainContent.removeClass('big');
	});

	mainContent.getElement('#tools a.abig').addEvent('click', function(e) {
		e = new Event(e);
		e.stop();
		mainContent.addClass('big');
		mainContent.removeClass('normal');
	});
}

/************
* reflection images
**************/
var Reflection = {
	defaultHeight : 0.3,
	defaultOpacity: 0.14,
	
	add: function(image, options) {
		Reflection.remove(image);
		
		doptions = { "height" : Reflection.defaultHeight, "opacity" : Reflection.defaultOpacity }
		if (options) {
			for (var i in doptions) {
				if (!options[i]) {
					options[i] = doptions[i];
				}
			}
		} else {
			options = doptions;
		}
	
		try {
			var d = document.createElement('div');
			var p = image;
			
			var classes = p.className.split(' ');
			var newClasses = '';
			for (j=0;j<classes.length;j++) {
				if (classes[j] != "reflect") {
					if (newClasses) {
						newClasses += ' '
					}
					
					newClasses += classes[j];
				}
			}

			var reflectionHeight = Math.floor(p.height*options['height']);
			var divHeight = Math.floor(p.height*(1+options['height']));
			
			var reflectionWidth = p.width;
			
			if (document.all && !window.opera) {
				/* Fix hyperlinks */
                if(p.parentElement.tagName == 'A') {
	                var d = document.createElement('a');
	                d.href = p.parentElement.href;
                }  
                    
				/* Copy original image's classes & styles to div */
				d.className = newClasses;
				p.className = 'reflected';
				
				d.style.cssText = p.style.cssText;
				p.style.cssText = 'vertical-align: bottom';
			
				var reflection = document.createElement('img');
				reflection.src = p.src;
				reflection.style.width = reflectionWidth+'px';
				reflection.style.display = 'block';
				reflection.style.height = p.height+"px";
				
				reflection.style.marginBottom = "-"+(p.height-reflectionHeight)+'px';
				reflection.style.filter = 'flipv progid:DXImageTransform.Microsoft.Alpha(opacity='+(options['opacity']*100)+', style=1, finishOpacity=0, startx=0, starty=0, finishx=0, finishy='+(options['height']*100)+')';
				
				d.style.width = reflectionWidth+'px';
				d.style.height = divHeight+'px';
				p.parentNode.replaceChild(d, p);
				
				d.appendChild(p);
				d.appendChild(reflection);
			} else {
				var canvas = document.createElement('canvas');
				if (canvas.getContext) {
					/* Copy original image's classes & styles to div */
					d.className = newClasses;
					p.className = 'reflected';
					
					d.style.cssText = p.style.cssText;
					p.style.cssText = 'vertical-align: bottom';
			
					var context = canvas.getContext("2d");
				
					canvas.style.height = reflectionHeight+'px';
					canvas.style.width = reflectionWidth+'px';
					canvas.height = reflectionHeight;
					canvas.width = reflectionWidth;
					
					d.style.width = reflectionWidth+'px';
					d.style.height = divHeight+'px';
					p.parentNode.replaceChild(d, p);
					
					d.appendChild(p);
					d.appendChild(canvas);
					
					context.save();
					
					context.translate(0,image.height-1);
					context.scale(1,-1);
					
					context.drawImage(image, 0, 0, reflectionWidth, image.height);
	
					context.restore();
					
					context.globalCompositeOperation = "destination-out";
					var gradient = context.createLinearGradient(0, 0, 0, reflectionHeight);
					
					gradient.addColorStop(1, "rgba(255, 255, 255, 1.0)");
					gradient.addColorStop(0, "rgba(255, 255, 255, "+(1-options['opacity'])+")");
		
					context.fillStyle = gradient;
					if (navigator.appVersion.indexOf('WebKit') != -1) {
						context.fill();
					} else {
						context.fillRect(0, 0, reflectionWidth, reflectionHeight*2);
					}
				}
			}
		} catch (e) {
	    }
	},
	
	remove : function(image) {
		if (image.className == "reflected") {
			image.className = image.parentNode.className;
			image.parentNode.parentNode.replaceChild(image, image.parentNode);
		}
	}
}

function addReflections() {
	var rimages = document.getElementsByTagName('img');
	
	for (i=0;i<rimages.length;i++) {
		if (rimages[i].className.match(/\breflected\b/)) {
			var rheight = null;
			var ropacity = null;
			
			var classes = rimages[i].className.split(' ');
			for (j=0;j<classes.length;j++) {
				if (classes[j].indexOf("rheight") == 0) {
					var rheight = classes[j].substring(7)/100;
				} else if (classes[j].indexOf("ropacity") == 0) {
					var ropacity = classes[j].substring(8)/100;
				}
			}
			
			Reflection.add(rimages[i], { height: rheight, opacity : ropacity});
		}
	}
}

function printMe(){
	var tools = $('tools');
	if (tools)
		tools.getElement('.aprint').addEvent('click',function(e){
			new Event(e).stop();
			window.print();
		});
}

function mailMe(mail){
	var tools = $('tools');
	if (tools)
		tools.getElement('.amail').setProperty('href','mailto:'+mail);
}

/*permet d'imprimer un tableau*/
window.printTable;
function openTable(elm){ //elm = le lien click� 
	var links = document.getElements('a.print');
	var tables = document.getElements('div.dataArray');
	var tableCoor, win;
	
	links.each(function(link,i){
		if (elm === link){
			window.printTable = tables[i].clone();
			tableCoor = tables[i].getCoordinates();
			window.printTable.setStyles({
				'height':tableCoor.height,
				'width':tableCoor.width
			});
			
			win = window.open('printTable.html','printTable',"height="+tableCoor.height+",width="+tableCoor.width);
		}
	});
	
	return false;
}

function setOpenTable(){
	document.getElements('a.print').each(function(elm,i){
		elm.addEvent('click',function(e){
			new Event(e).stop();
			openTable(elm);
		}.bind(this));
	})
}


var $tooltip = {
	options : {
		templateHTML : [
			'<div class="ctn">',
				'__CONTENT__',
			'</div>',
		].join(''),
		content : ''
	},
	
	show : function(elm, event, options) {
		this.dom.extend(this.options, options);
		var content = this.options.content;
		var infoBulleLayer = document.createElement('div');
		infoBulleLayer.id = 'infoBulleLayer';
		$tooltip.id = infoBulleLayer.id;
		infoBulleLayer.className = 'infobulleSpe';
		infoBulleLayer.innerHTML = this.options.templateHTML;
		infoBulleLayer.innerHTML = infoBulleLayer.innerHTML.replace(/__CONTENT__/g, content);
		document.body.appendChild(infoBulleLayer);
		
		if (options.width) {
				infoBulleLayer.style.width = options.width;
		}
		
		elm.onmousemove = function(e) {
			$tooltip.setPosition(e);
		};
		
		elm.onmousemove(event);
		//this.setPosition(elm);
		ifrlayer.make(infoBulleLayer);
		elm.onmouseout = function() {
			$tooltip.hide();
		}
	},
	
	setPosition : function(event) {
		event = event || window.event;
		
		var infoBulleLayer = $($tooltip.id);
		var mouse = this.dom.getMouse(event);
		if (infoBulleLayer)
			with(infoBulleLayer.style) {
				left = mouse.x + 10 + 'px';
				top = mouse.y  -  infoBulleLayer.offsetHeight - 10 + 'px';
			}
	},

	hide : function() {
		var infoBulle = document.getElementById($tooltip.id);
		if (infoBulle) {
			ifrlayer.hide(infoBulle);
			infoBulle.parentNode.removeChild(infoBulle);
		}
	},
	
	dom : {
		getPos : function(obj) {
			var curleft = curtop = 0;
			if (obj.offsetParent) {
				do {
						curleft += obj.offsetLeft;
						curtop += obj.offsetTop;
					} while (obj = obj.offsetParent);
				return {x:curleft,y:curtop};
			}
		},
		getMouse : function(e) {
			 e = e || window.event;
		     var posx = 0;
		     var posy = 0;
		     if (!e) var e = window.event;
		     if (e.pageX || e.pageY)  {
			     posx = e.pageX;
			     posy = e.pageY;
		     }
		     else if (e.clientX || e.clientY) {
			     posx = e.clientX + document.documentElement.scrollLeft;
			     posy = e.clientY + document.documentElement.scrollTop;
	      	 }
			return {x:posx, y:posy};
		},
		extend : function() {
			var args = arguments;
			if (!args[1]) args = [this, args[0]];
			for (var property in args[1]) args[0][property] = args[1][property];
			return args[0];
		}
	}
}

function showImage(element, event, imgUrl) {
	showImageElement = element;
	$tooltip.show(element, event, {
		width : document.all && /MSIE [56]/.test(navigator.userAgent) ? '1%' : 'auto',
		content: '<img src="'+ imgUrl + '" onload="resizeImageTipOnLoad(this)" />'
	});
}

function resizeImageTipOnLoad(img) {
	var infoBulleLayer = $('infoBulleLayer');
	if (infoBulleLayer) {
		 infoBulleLayer.style.width =  infoBulleLayer.offsetWidth+'px';
	}
}

/***********
* Init
************/

//fonction lancee pendant le chargement de la page
function domLoadFunctions() {
	NavLeft.init('navLeft');
	generateElements();
	SizeFont();
	
}
//fonction lancee une fois toute la page chargee
function onLoadFunctions() {
	//contentSize();
	addReflections();
	sizeBlocks();
	fixCorners();
	fixHeights();
	fixColumns();
	printMe();
	mailMe('');
	setOpenTable();
	//oblige pour laisser au navigateur le temps de se rafraichir
}

