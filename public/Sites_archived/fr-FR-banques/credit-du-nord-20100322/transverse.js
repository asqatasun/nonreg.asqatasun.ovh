

function openPara(parametre1, parametre2) {
		document.getElementById(parametre1).style.display='none';
		document.getElementById(parametre2).style.display='block';
}


$().ready(function(){	

/* -------------------------- Comportement des listbox ---------------------------*/
	
	/* Fermeture des listbox si l'on clique n'importe ou sur la page*/
			var painted = 0;
			$('body').click(
					function(event){
					painted=0;
					$('ul.bt').hide();				
					}
				);						
		
			$('a.listbox').toggle(
				function(event){
					if(painted==0){
						$('ul.bt').hide();
						$(this).next('div').children('ul').toggle();
						painted=1;
					} else {
						painted=0;
						$('ul.bt').hide();
					}					
				}, 
			function(event){
					if(painted==1){
						painted=0;
						$('ul.bt').hide();
					} else {
						painted=1;
						$('ul.bt').hide();
						$(this).next('div').children('ul').toggle();
					}
			}); 
			
			$('a.list').click(
				function(event){
				$(this).parent('li').parent('ul').hide();
			});

	}
);	