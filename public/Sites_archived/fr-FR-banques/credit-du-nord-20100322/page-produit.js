/* -------------------------------------------------- Page Produit ----------------------------------------------------------*/	
	$().ready(function(){	
	

		
/* Comportement plie / deplie du 1er regroupement (de type Classique depliable)	d'une fiche produit */	
		$('div#centrage div.dp2 > div.entetedp2 > a.none').click(
			function(event) {
				$(this).parent('div').parent('div').hide().siblings('div.dp1').show();
			}
		);
			
		$('div#centrage div.dp1 > a.none').click(
			function(event) {
				$(this).parent('div').hide().siblings('div.dp2').show();
			}
		);	
		
/* Comportement plie / deplie des autres regroupements (de type Classique depliable) d'une fiche produit */		
		$('div#centrage > div.dp11 > a.none').click(
			function(event){
				$(this).parent('div').hide().next('div.dp12').show();
			}
		);	
		
		$('div#centrage > div.dp12 > div.entetedp12 > a.none').click(
			function(event) {
				$(this).parent('div').parent('div').hide().prev('div.dp11').show();
			}
		);	
		
/* Comportement des titres de regroupement au survol*/
$('a.none > span').hover (
		function(event) {
			$(this).addClass("underline");	
		},
		function(event) {
			$(this).removeClass("underline");	
		}
);			

	
	}
);	
		