jQuery(window).load(function(){
	//$getNavigateur();
	if (jQuery("#referencement > p").length > 0 && jQuery("#editorial_src").length > 0) {
		if(jQuery.trim(jQuery("#referencement > p").text()) != "") 
			jQuery("#referencement > p").text(jQuery("#editorial_src").text());
		else jQuery("#referencement > p").html(jQuery("#editorial_src").html());
	}
});

var tabDivAjax = new Array();

function timeout(div, message) {
	if(jQuery("div[id='"+div+"']").find("img").attr("src") == '/datas/images/loading.gif') {
		jQuery("div[id='"+div+"']").empty();
		jQuery("div[id='"+div+"']").append(message);
	}
}
function asynch_ajax(time) {
	lst = jQuery("input:hidden[id^='asynch']");
	if(lst.length>0) {
		params = "";
		lst.each(function(i) {
			if(i!=0) {
				params += "&";
			}
			params += "key"+i+"="+this.name+"&div"+i+"="+this.value;
		});
		params += "&time="+time;
		
		jQuery.getJSON("/AsynchAjax",params,function(json){
			if(json.data != undefined) {
				if(json.time != undefined)
					wait(false,json.time);
				jQuery.each(json.data, function(i){
					jQuery("div[id='"+this.key+"']").empty();
					jQuery("div[id='"+this.key+"']").append(this.flux);
				})
			}
		});
	}
	
	lst = jQuery("input:hidden[id^='js_asynch']");
	if(lst.length>0) {
		lst.each(function(i) {
			document.getElementsByTagName("head")[0].appendChild(eval(this.value));
			try {
				par = document.getElementById(this.id).parentNode;
				par.removeChild(document.getElementById(this.id));
			}catch(e){}
		});
	}
}

function wait(test,time){
	if(test){
		asynch_ajax(time);	
	} else {
		setTimeout("wait("+!test+","+time+")",time);
	}
}

// Fonction de connexion ajax
function appelAjax(url, id) {
	jQuery.ajax({url: url,success: function(html){jQuery("div[id='"+id+"']").empty();jQuery("div[id='"+id+"']").append(html);}});
}

//Getter Date du jour : utilis�e en parties publique et priv�e. Ex : Mar 10 f�vrier 2009
//origine = 0-> publique, origine = 1-> priv�e
function date_jour(origine){
	
	var X_tab_jour = new Array(I18n.message('txt.dim'), I18n.message('txt.lun'), I18n.message('txt.mar'), I18n.message('txt.mer'), I18n.message('txt.jeu'), I18n.message('txt.ven'), I18n.message('txt.sam'));
	var X_tab_jourComplet = new Array(I18n.message('txt.dimanche'), I18n.message('txt.lundi'), I18n.message('txt.mardi'), I18n.message('txt.mercredi'), I18n.message('txt.jeudi'), I18n.message('txt.vendredi'), I18n.message('txt.samedi'));
	var X_tab_mois = new Array(I18n.message('txt.janvier'), I18n.message('txt.fevrier'), I18n.message('txt.mars'), I18n.message('txt.avril'), I18n.message('txt.mai'), I18n.message('txt.juin'), I18n.message('txt.juillet'), I18n.message('txt.aout'), I18n.message('txt.septembre'), I18n.message('txt.octobre'), I18n.message('txt.novembre'), I18n.message('txt.decembre'));

	X_date = new Date();
	X_jour = origine == '0' ? X_tab_jourComplet[X_date.getDay()] : X_tab_jour[X_date.getDay()];
	X_njour = X_date.getDate()
	X_mois = X_tab_mois[X_date.getMonth()];
	X_annee = X_date.getFullYear();
	var date = X_jour + ' ' + X_njour + ' ' + X_mois + ' ' +X_annee;
	
	if (origine == '0')
		jQuery("#date_visiteur").append(date);
	else if (origine == '1')
		jQuery("#calendrier_jour > span:first").append(date);
}

function date_jour_complet(){
	var X_tab_jour = new Array(I18n.message('txt.dimanche'), I18n.message('txt.lundi'), I18n.message('txt.mardi'), I18n.message('txt.mercredi'), I18n.message('txt.jeudi'), I18n.message('txt.vendredi'), I18n.message('txt.samedi'));
	var X_tab_mois = new Array(I18n.message('txt.janvier'), I18n.message('txt.fevrier'), I18n.message('txt.mars'), I18n.message('txt.avril'), I18n.message('txt.mai'), I18n.message('txt.juin'), I18n.message('txt.juillet'), I18n.message('txt.aout'), I18n.message('txt.septembre'), I18n.message('txt.octobre'), I18n.message('txt.novembre'), I18n.message('txt.decembre'));

	X_date = new Date();
	X_jour = X_tab_jour[X_date.getDay()];
	X_njour = X_date.getDate()
	X_mois = X_tab_mois[X_date.getMonth()];
	X_annee = X_date.getFullYear();
	document.write(X_jour + ' ' + X_njour + ' ' + X_mois + ' ' +X_annee);
}

function debuteTemps(X_champ ,X_decalage_signe_gmt ,X_decalage_heure_gmt ,delai1) {

	if(jQuery("#" + X_champ).length > 0) {
		var dd, delai; 
		var hhmmss = "", min, sec;
		delai = delai1;
		decalage_signe_gmt = X_decalage_signe_gmt;
		decalage_heure_gmt = X_decalage_heure_gmt;
		adate = new Date();

		if(decalage_signe_gmt == '+'){
			hhmmss += adate.getHours() + (adate.getTimezoneOffset() / 60) + decalage_heure_gmt;
		}else{
			hhmmss += adate.getHours() + (adate.getTimezoneOffset() / 60) - decalage_heure_gmt;
		}

		if (hhmmss>=24){hhmmss=hhmmss-24;}
		if (hhmmss < 10) hhmmss = "0" + hhmmss;

		min = adate.getMinutes();

		if (min < 10) hhmmss += ":0" + min;
		else hhmmss += ":" + min;
		sec = adate.getSeconds();

		if (sec < 10) hhmmss += ":0" + sec;
		else hhmmss += ":" + sec;

		hhmmss = " " + hhmmss;

		jQuery("#" + X_champ).html(hhmmss);

		dd = setTimeout("debuteTemps('" + X_champ + "' ,'" + decalage_signe_gmt + "' ," + decalage_heure_gmt + "," + delai + ")",delai1);
	}
}

// Initialise de maniere distante (depuis le serveur) la date/heure a utiliser
// pour commencer le decompte de temps.
// X_champ : champ a modifier (nom de la div ou du span)
// X_date  : date en milliseconde
// delai   : delai de rafraichissement du temps (typiquement 1000ms)
function debuteTempsDist(X_champ, X_date, delai){
	if(jQuery("#" + X_champ).length > 0) {
		var hhmmss = " ";
		adate = new Date(X_date);
		
		if (adate.getHours() < 10) hhmmss += "0";
		hhmmss += adate.getHours() + ":";
		if (adate.getMinutes() < 10) hhmmss += "0";
		hhmmss += adate.getMinutes() + ":";
		if (adate.getSeconds() < 10) hhmmss += "0";
		hhmmss += adate.getSeconds();
		
		jQuery("#" + X_champ).html(hhmmss);

		dd = setTimeout("debuteTempsDist('" + X_champ + "', " + (X_date + delai) + ", " + delai + ")", delai);
	}
}

// Affcihe ou masque un calques selon son etat et gestion de l'icone (agrandir ou reduire)
function gestion_calques(X_id, X_this){
	// Gestion du calque
	if(jQuery("#" + X_id).length <= 0){return false;}
	
	if (jQuery.trim(jQuery("#" + X_id).css("display")) == '' || jQuery("#" + X_id).css("display") == 'block') jQuery("#" + X_id).css("display", "none"); else jQuery("#" + X_id).css("display", "block");

	switch(X_id){
		case 'tab_synthese_structure':
			if(jQuery("#" + X_id).css("display") == 'block') jQuery("#synthese_content_entete").removeClass("agrandir");		
			else jQuery("#synthese_content_entete").addClass("agrandir");		
		break
		case 'tab_portefeuille_structure':
			if(jQuery("#" + X_id).css("display") == 'block') jQuery("#portefeuille_content_entete").removeClass("agrandir");		
			else jQuery("#portefeuille_content_entete").addClass("agrandir");		
		break
		case 'tab_synthese_compte_structure':
			if(jQuery("#" + X_id).css("display") == 'block') jQuery("#synthese_compte_content_entete").removeClass("agrandir");		
			else jQuery("#synthese_compte_content_entete").addClass("agrandir");		
		break
		default:
			if(jQuery(X_this).length <= 0){return false;}
			if(jQuery(X_this).attr("class").indexOf('bt_reduire') > -1) jQuery(X_this).addClass("bt_agrandir").removeClass("bt_reduire");	
			else jQuery(X_this).addClass("bt_reduire").removeClass("bt_agrandir");	
		break;
	}
}

// Gestion du cadre teaser
var teaserTimerId;
function teaser(action, save){
    // date expiration cookie
	var cookieExpires=new Date()

	if (action=='toggle'){
		if (teaserTimerId) {
			window.clearTimeout(teaserTimerId);
			teaserTimerId = null;
		}
		jQuery("#devenez_client_teaser").toggle();
		if (jQuery("#devenez_client_teaser").is(":visible")) {
			jQuery("#teaser_toggle").addClass("bt_reduire00").removeClass("bt_agrandir00");
			if(save) {
			  	cookieExpires.setDate(cookieExpires.getDate()-30)	
				setCookie("teaser", "", cookieExpires, document.location.pathname);
			}
		} else {
			jQuery("#teaser_toggle").addClass("bt_agrandir00").removeClass("bt_reduire00");
			if(save) {
			  	cookieExpires.setDate(cookieExpires.getDate()+30)	
				setCookie("teaser", "minimized", cookieExpires, document.location.pathname);
			}
		}
	}

	if (action=='close') {
		jQuery("#devenez_client").hide();
		if(save) {
		  	cookieExpires.setDate(cookieExpires.getDate()+30)	
			setCookie("teaser", "closed", cookieExpires, document.location.pathname);
		}
	}
	
	if (action=='auto') {
		teaserTimerId = window.setTimeout(function(){teaser('toggle',false);},8000);
	}	
}

function $hover(event){	
	if (jQuery.browser.msie) {
		if (event.srcElement.nodeName.toLowerCase() == "tr") jQuery(event.srcElement).addClass("hover");
		else jQuery(event.srcElement).parents("tr:first").addClass("hover");
	} else {
		if (event.currentTarget.nodeName.toLowerCase() == "tr") jQuery(event.currentTarget).addClass("hover");
		else if (event.currentTarget.nodeName.toLowerCase() == "td") jQuery(event.currentTarget).parent("tr").get(0).addClass("hover");
	}
}

function $_hover(event){
	if (jQuery.browser.msie) {
		 if (event.srcElement.nodeName.toLowerCase() == "tr") jQuery(event.srcElement).removeClass("hover");
		 else jQuery(event.srcElement).parents("tr:first").removeClass("hover");
	} else {
		if (event.currentTarget.nodeName.toLowerCase() == "tr") jQuery(event.currentTarget).removeClass("hover");
		else if (event.currentTarget.nodeName.toLowerCase() == "td") jQuery(event.currentTarget).parent("tr").get(0).removeClass("hover");
	}
}


/**
* Fonction cacher div
**/
function cacherDiv(idDiv) {
	if (jQuery("#" + idDiv).length > 0) jQuery("#" + idDiv).addClass("ui-helper-hidden");
}
/**
* Fonction afficher div
**/
function afficherDiv(idDiv) {
	if (jQuery("#" + idDiv).length > 0) jQuery("#" + idDiv).removeClass("ui-helper-hidden");
}

/**
* Formulaire d'Information Client et de Configuration
**/
var sauvegardeValeurChamp = new Array();
function activeBlocConsultation(idBloc){
	activeBloc(idBloc);
	cacherDiv('consultation'+idBloc);
	afficherDiv('modification'+idBloc);
	jQuery("#annuler" + idBloc).click(function () {freezeBlocConsultation(idBloc);});
}
function freezeBlocConsultation(idBloc){
	freezeBloc(idBloc);
	afficherDiv('consultation'+idBloc);
	cacherDiv('modification'+idBloc);
}

function activeBloc(idBloc){
	
	jQuery.each(listeForm, function(i,val) {
		freezeBlocConsultation(val);
    });
	
	afficherDiv('blocvalider'+idBloc);
 	cacherDiv('blocmodifier'+idBloc);
 	if (jQuery("#" + idBloc).length > 0) {
 		sauvegardeValeurChamp[idBloc] = new Array();
 		jQuery("#" + idBloc + " :input").filter(function () {
 			return jQuery(this).attr("type") != 'button' && jQuery(this).attr("type") != 'submit';
 		}).each( function(j) {
 			if(jQuery(this).attr("type") == 'checkbox' || jQuery(this).attr("type") == 'radio') sauvegardeValeurChamp[idBloc][j] = jQuery(this).is(":checked");
 			else sauvegardeValeurChamp[idBloc][j] = jQuery(this).val();
 			jQuery(this).attr("disabled", false);
		});
 	}
 	jQuery("#annuler" + idBloc).click(function () {freezeBloc(idBloc);});
}
function freezeBloc(idBloc){
	afficherDiv('blocmodifier'+idBloc);
 	cacherDiv('blocvalider'+idBloc);
 	if (jQuery("#" + idBloc).length > 0) {
 		jQuery("#" + idBloc + " :input").filter(function () {
 			return jQuery(this).attr("type") != 'button' && jQuery(this).attr("type") != 'submit';
 		}).each( function(j) {
 			if (sauvegardeValeurChamp[idBloc] != null) {
		 		if(jQuery(this).attr("type") == 'checkbox' || jQuery(this).attr("type") == 'radio') jQuery(this).attr("checked", sauvegardeValeurChamp[idBloc][j]);
			 	else jQuery(this).val(sauvegardeValeurChamp[idBloc][j]);	
	 		}
 			jQuery(this).attr("disabled", true);
		});
 	}
}

/**
* Fonction permettant de tester si une chaine est num�rique
**/
function is_numeric(num) {
	var exp = new RegExp("(^[0-9]*)([.]?)([0-9]*$)","g");
	if (num == '')
		return false;
	else
		return exp.test(num.replace(/[\,]/,"."));
}

function controlNumeric(id) {
    var value = jQuery("#" + id).val();

    if (value != null && value != "") {
	    value = value.replace(" ", "");
	    value = value.replace(/[\,]/,".");
    	if (!is_numeric(value)) {
    		alert(I18n.message('txt.notANumber'));
    		jQuery("#" + id).val("");
    	}
    	else jQuery("#" + id).val(value);
    }
}

/**
* Fonction permettant de tester si une chaine est un entier
**/
function is_entier(num) {
	var exp = new RegExp("^[0-9]*$","g");
	return exp.test(num);
}

function replaceAlpha(alpha) {
	return '12345678912345678923456789'.charAt(alpha.charCodeAt(0) - 65);
}

function calculAge(annee, mois, jour) {
	var actu =new Date();

	var day = parseInt(jour);
	var month = parseInt(mois);
	var year = parseInt(annee);

	if ((actu.getMonth()+1) >= month) {
		if ((actu.getMonth()+1) == month) {
			if (actu.getDate() >= day) {
				mois = actu.getMonth() + 1 - month;
				ans = actu.getFullYear() - year;
			} else {
				mois = 12-month+actu.getMonth()+1;
				ans = actu.getFullYear() - year - 1;
			}
		} else {
			mois = actu.getMonth() + 1 - month;
			ans = actu.getFullYear() - year;
		}
	} else {
		mois =12 - month + actu.getMonth() + 1;
		ans = actu.getFullYear() - year - 1;
	}
	
	if (actu.getDate() > day) {
		jours = actu.getDate() - day;
	} else {
		jours = 30 - day + actu.getDate();
	}

	while (jours > 30) {
		jours -= 30;
		mois += 1;
	}

	while (mois > 12) {
		mois -= 12;
		ans += 1;
	}
	return ans;
}