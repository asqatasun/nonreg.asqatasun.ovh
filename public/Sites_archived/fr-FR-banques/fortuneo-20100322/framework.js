var I18nMessages = {
  fr: {
    "txt.test": "test",
    "txt.defaultPattern": "Libell�, Mn�mo, ISIN, mot cl�",
    "url.accueil":"/fr/bourse/bienvenue.jsp",
    "txt.patternVide": "Saisissez une valeur",
    "txt.patternRechercheVideOuDefaut": "Veuillez saisir un libell�, un Mn�mo, un code ISIN ou un mot-cl�.",
    "txt.actionNonClient": "Veuillez vous connecter pour passer vos ordres.",
    "txt.bienvenue": "bienvenue ${prenom} ${nom}",
    "url.listeNews": "/fr/bourse/actions/actualite/depeches.jsp",
    "url.topPerformance": "/fr/sicav-fonds/palmares/top-performance/performance-sicav.jsp",
    "url.topCategorie": "/fr/sicav-fonds/palmares/top-categorie/categorie-amf-sicav.jsp",
    "url.surPerformance": "/fr/sicav-fonds/palmares/top-surperformance/surperformance-sicav.jsp",
    "url.plusPersistant": "/fr/sicav-fonds/palmares/plus-persistants/les-plus-persistants.jsp",
    "url.plusRisque": "/fr/sicav-fonds/palmares/plus-risques/volatilite-sicav.jsp",
    "url.newStar": "/fr/sicav-fonds/palmares/nouveaux-etoiles/rating-sicav.jsp",
    "url.comparaisonBase100": "/fr/sicav-fonds/recherche-sicav/sicav-comparaison-base-100.jsp",
    "url.comparaisonPlusieursFonds": "/fr/sicav-fonds/recherche-sicav/comparateur-plusieurs-fonds.jsp",
    "url.backtesting": "/fr/bourse/outils/backtesting.jsp",
    "url.validSuppressionCompteExterne": "/fr/mes-comptes/compte-especes/liste-comptes-externes/validation-suppression-compte-externe.jsp",
    "url.opcvmScreening": "/fr/sicav-fonds/recherche-sicav/selecteur-fonds-multicritere.jsp",
    "txt.saisirSJ": "Veuillez saisir un sous jacent.",
	"txt.saisirVal": "Veuillez saisir une valeur ou un nombre d'actif valide.",
	"txt.saisirCode": "Veuillez saisir un code.",
	"txt.saisirCoursEtVariation": "Le cours du support et la variation de la volatilit� sont obligatoires.",
    "url.detailAlerteCours": "/fr/bourse/outils/detail-alerte.jsp",
    "txt.saisirStrike": "S�lectionnez un strike",
    "txt.selectionLettre": "Veuillez s�lectionner une lettre pour afficher la liste",
    "url.rechercheWarrant": "/fr/bourse/warrants/recherche/recherchez-warrant.jsp",
    "url.selectionneurWarrant": "/fr/bourse/outils/selectionneur-warrants.jsp",
    "url.resultatRechercheWarrant": "/fr/bourse/warrants/recherche/resultat-recherche-warrants.jsp",
    "url.resultatSelectionneurWarrant": "/fr/bourse/warrants/recherche/resultat-selectionneur-warrants.jsp",
    "url.ordre": "/fr/mes-comptes/ordre.jsp?cdSens=${cdSens}&cdReferentiel=${cdReferentiel}",
    "url.preordre": "/fr/mes-comptes/pre-ordre.jsp?cdSens=${cdSens}&cdReferentiel=${cdReferentiel}&indicateurEtapeValidation=${indicateurEtapeValidation}",
    "url.preparationOrdre":"/fr/mes-comptes/pre-ordre.jsp",
    "url.passageOrdre":"/fr/mes-comptes/ordre.jsp",
    "url.passageOrdre.indicateurTR":"/fr/mes-comptes/ordre.jsp?tr=${indTR}",
    "url.fenetreOrdre":"/fr/bourse/outils/fenetre-ordre.jsp",
    "url.informationsClient":"/fr/informations-client.jsp",
    "url.suppressionPreOrdre":"/fr/bourse/outils/suppression-carnet-pre-ordres.jsp",
    "url.envoiPreOrdre":"/fr/bourse/outils/envoi-carnet-pre-ordres.jsp",
    "url.creationPreOrdre":"/fr/bourse/outils/recherche-valeur-pre-ordres.jsp",
    "txt.nbreComparaisonMax": "Vous ne pouvez pas comparer plus de 10 valeurs � la fois.",
    "txt.nbreSelectionMax": "Les membres ne peuvent recevoir plus de 10 types d'alertes diff�rents.",
    "txt.selectionLSJ": "Veuillez s�lectionner un sous-jacent ou saisir un code/libell�.",
    "txt.selectionL": "Veuillez saisir un code ou un libell�.",
    "txt.selectionnerSousJacent": "Veuillez s�lectionner un sous-jacent.",
    "url.optimisation": "/fr/mes-comptes/compte-titres-pea/analyse-compte/optimisation.jsp",
    "url.performance": "/fr/mes-comptes/compte-titres-pea/analyse-compte/diagnostic-performance.jsp",
    "url.repartition": "/fr/mes-comptes/compte-titres-pea/analyse-compte/diagnostic-repartition.jsp",
    "url.securite": "/fr/mes-comptes/compte-titres-pea/analyse-compte/diagnostic-securite.jsp",
    "url.observation": "/fr/mes-comptes/compte-titres-pea/analyse-compte/observation.jsp",
    "url.configurationSite": "/fr/configuration-site.jsp",
    "url.abonnementUs1": "/fr/abonnement-us-1.jsp",
    "txt.confirmTRUS": "Confirmez-vous vouloir vous d�sabonner du temps r�el US ?",
    "txt.selectionnerSousJacent": "Veuillez s�lectionner un sous-jacent.",
    "txt.selectionnerValeurs": "Aucune valeur s�letionn�e.",
    "txt.notANumber": "La quantit� saisie n'est pas un nombre",
    "txt.notAEntier": "La quantit� saisie n'est pas un entier",
    "txt.dateEcheanceObligatoire": "La date d'�ch�ance est obligatoire",
    "url.composition": "/fr/bourse/actions/indices/indices-mondiaux/fiche-valeur-composition.jsp",
    "txt.selectionMinCertificat": "Veuillez s�lectionner au moins un des crit�res sous-jacent/�metteurs/type.",
    "txt.1an": "1 an",
    "txt.3ans": "3 ans",
    "txt.5ans": "5 ans",
    "txt.1sem": "1 sem",
    "txt.1mois": "1 mois",
    "txt.3mois": "3 mois",
    "txt.1jan": "1er janv",
    "txt.dim": "Dim",
    "txt.lun": "Lun",
    "txt.mar": "Mar",
    "txt.mer": "Mer",
    "txt.jeu": "Jeu",
    "txt.ven": "Ven",
    "txt.sam": "Sam",
    "txt.dimanche": "Dimanche",
    "txt.lundi": "Lundi",
    "txt.mardi": "Mardi",
    "txt.mercredi": "Mercredi",
    "txt.jeudi": "Jeudi",
    "txt.vendredi": "Vendredi",
    "txt.samedi": "Samedi",
    "txt.janvier": "janvier",
    "txt.fevrier": "f�vrier",
    "txt.mars": "mars",
    "txt.avril": "avril",
    "txt.mai": "mai",
    "txt.juin": "juin",
    "txt.juillet": "juillet",
    "txt.aout": "ao�t",
    "txt.septembre": "septembre",
    "txt.octobre": "octobre",
    "txt.novembre": "novembre",
    "txt.decembre": "d�cembre",
    "txt.confirmSuppression": "Etes-vous s�r de vouloir supprimer le(s) informations s�lectionn�es ?",
    "url.detailAlerteEcheance": "/fr/bourse/outils/detail-alerte-sur-echeance.jsp",
    "url.listeValeurAlerteNews": "/fr/bourse/outils/liste-valeur-alerte-sur-news.jsp",
    "url.detailAlerteNews": "/fr/bourse/outils/detail-alerte-sur-news.jsp",
    "url.modeGestion": "/fr/bourse/action/mode-gestion-service.jsp",
    "url.avisOperes": "/dyndocuments/avisOperes",
    "url.mail7": "/fr/nous-contacter/mail7.jsp",
    "url.mail7.akio": "/fr/nous-contacter/mail7_akio.jsp",
    "txt.1jan": "1er janv",
    "pop.confirm_delete_liste_perso": "Etes-vous certains de vouloir supprimer cette liste ",
    "txt.error_name_liste_perso": "Nom d�j� choisi - Veuillez donner un autre nom",
    "txt.error_noname_liste_perso": "Veuillez donner un nom � la liste",
    "txt.error_max_col_reach": "Le nombre maximum de 11 colonnes est atteint pour ce profil.",
    "txt.error_profil_non_mis_a_jour": "Une erreur s'est produite, le profil n'a pas pu �tre mis � jour.",
    "url.devenirMembre1": "/fr/devenir-membre-1.jsp",
    "url.devenirMembre2": "/fr/devenir-membre-2.jsp",
    "url.devenirMembre3": "/fr/devenir-membre-3.jsp",
    "txt.precisionPortManquant": "Vous n'avez pas renseign� le champ 'Possedez-vous un portefeuille boursier'.",
    "txt.precisionNbreOrdresManquant": "Vous n'avez pas renseign� le champ 'Combien d'ordres passez-vous par mois'.",
    "txt.precisionAVManquant": "Vous n'avez pas renseign� le champ 'Possedez-vous une assurance-vie'.",
    "txt.mdpManquant": "Vous n'avez pas renseign� de mot de passe.",
    "txt.mdp2Manquant": "Vous n'avez pas confirm� le mot de passe.",
    "txt.mdp2errone": "La confirmation de votre mot de passe est erron�e.",
    "txt.mdpTropCourt":"Votre mot de passe doit comporter entre 6  et 12 caract�res.",
    "url.updateMembre":"/fr/mon-profil/confirmation-profil-membre.jsp",
    "txt.err.maxlistreach":"Vous ne pouvez pas cr�er plus de 5 listes personnelles sur votre Espace Membre Fortuneo.",
    "url.souscriptionMessage":"/fr/souscription-message.jsp",
    "url.SCLivretREtape1":"/fr/souscription-livret-a-etape1.jsp",
    "url.FormLivretROuverture":"/fr/ouverture-livret-a.jsp",
    "url.SCLivretNREtape1":"/fr/souscription-livret-plus-etape1.jsp",
    "url.FormLivretNROuverture":"/fr/ouverture-livret-plus.jsp",
    "url.autoPrelev":"/dyndocuments/autorisationPrelevement",
    "url.projetContrat":"/livret/generationPdf",
    "url.SCLivretREtape2":"/fr/souscription-livret-a-etape2.jsp",
    "url.SCLivretNREtape2":"/fr/souscription-livret-plus-etape2.jsp",
    "url.SCLivretREtape3":"/fr/souscription-livret-a-etape3.jsp",
    "url.SCLivretNREtape3":"/fr/souscription-livret-plus-etape3.jsp",
    "url.syntheseTousComptes":"/fr/mes-comptes/synthese-tous-comptes.jsp",
    "SCLivret.notANumber": "La valeur saisie n'est pas un nombre",
    "txt.messagePaysAR":"Si vous r�sidez dans un pays ne figurant pas dans la liste, merci de contacter le service client ou de remplir et renvoyer une demande d'ouverture de compte.",
    "txt.pasDeconsultationCdtGenerales":"Veuillez consulter les informations ci-dessous et cocher la case pour valider votre souscription",
    "txt.pasDeconsultationCdtGeneralesFormulaires":"Veuillez cocher la case et prendre connaissance des documents indiqu�s",
	"url.FormLivretRRecap":"/fr/ouverture-livret-a-recapitulatif.jsp",
	"url.FormLivretNRRecap":"/fr/ouverture-livret-plus-recapitulatif.jsp",
	"url.FormLivretCotitulaire":"/fr/ouverture-livret-cotitulaire.jsp",
	"url.formLivretTitulaire":"/fr/ouverture-livret-titulaire.jsp",
	"url.formLivretATitulaire":"/fr/ouverture-livret-a-titulaire.jsp",
	"url.formLivretPTitulaire":"/fr/ouverture-livret-plus-titulaire.jsp",
	"url.SCLivretAPresentation":"/fr/souscription-livret-a-presentation.jsp",
	"url.SCLivretNRPresentation":"/fr/souscription-livret-plus-presentation.jsp",
	"url.FormConfirmationOuvertureA":"/fr/confirmation-demande-ouverture-livret-a.jsp",
	"url.FormConfirmationOuvertureP":"/fr/confirmation-demande-ouverture-livret-plus.jsp",
	"url.FormLivretPdf":"ouverture-livret-edition-pdf.jsp",
	"url.FormLivretTransfertPdf":"/datas/files/bordereau_transfert_dynamique.pdf",
	"SCLivret.etape3.txt.livretVisible":"Il sera visible sur le site d'ici 3 jours.",
	"SCLivret.etape3.txt.messageConfirmation1":"Votre contrat et votre ticket d'archivage viennent d'�tre g�n�r�s. Veuillez cliquer ci-dessous pour les consulter. Vous n'avez pas besoin de nous renvoyer ces documents, ni de les signer.<br/><br/><a href='${urlContrat}' target='_blank' class='gras souligne'>Visualiser le contrat</a><br/><a href='${urlTicket}' target='_blank' class='gras souligne'>Visualiser le ticket d'archivage</a><br/><br/>Nous vous conseillons toutefois de les enregistrer et d'en conserver une copie. Le ticket d'archivage facilitera vos d�marches en cas de r��dition de documents.",
	"SCLivret.etape3.txt.messageConfirmation2":"Vous pourrez consulter votre contrat sur la page 'Consultation de votre compte' de votre livret d�s que celui-ci sera visible.",
	"txt.err.mdp.invalide":"Le mot de passe doit contenir des chiffres et/ou des lettres (minuscules et/ou majuscules).",
	"txt.maxFavoris":"Vous ne pouvez pas renseigner plus de favoris.",
	"txt.favorisExiste":"Cette page fait d�j� partie de vos favoris.",
	"txt.confirmerSuppressionFavoris":"Souhaitez-vous supprimer le lien '${titreFavoris}' de la liste de vos favoris ?",
	"txt.masquerMesFavoris":"Masquer mes favoris",
	"txt.afficherMesFavoris":"Afficher mes favoris"
  },
  en: {
    "txt.test": "test",
    "txt.bienvenue": "welcome ${prenom} ${nom}" 
  }
};

var I18n = {
  locale: function() {
    return document.getElementsByTagName("html")[0].getAttribute("lang");
  },
  message: function(key, params) {
    var locale = this.locale();
    var trans = (I18nMessages && I18nMessages[locale])?I18nMessages[locale][key]:key;
    if (params) {
      for ( var name in params ) {
      	trans = eval("trans.replace(/\\${" + name + "\}/, params[name])");
      }
    }
    return trans;
  }
};

function $(elt) {
 if (typeof elt == 'string')
  elt = document.getElementById(elt);
 return elt;
};
function $F(elt) {
 if (typeof elt == 'string')
  elt = document.forms[elt];
 return elt;
};

var Elt = {
 visible: function (elt) {
  return $(elt).style.display != 'none';
 },
 toggle: function(elt,display) {
  Elt[Elt.visible(elt) ? 'hide' : 'show'](elt,display);
 },
 hide: function(elt) {
  $(elt).style.display = 'none';
 },
 show: function(elt,display) {
  $(elt).style.display = (display) ? display : '';
 },
 addClass: function(elt,cl) {
  if ($(elt).className.indexOf(cl) > -1)
   return;
  $(elt).className += ' ' + cl;
 },
 remClass: function(elt,cl) {
  if ($(elt).className.indexOf(cl) < 0)
   return;
  var cls = $(elt).className.split(/\s+/);
  for(i in cls) {
    if (cls[i] == cl) {
     cls[i] = '';
     break;
    }
  }
  $(elt).className = cls.join(' ');
 },
 hasClass: function(elt,cl) {
  var cls = $(elt).className.split(/\s+/);
  for(i in cls) {
    if (cls[i] == cl) {
     return true;
    }
  }
  return false;
 },
 hasType: function(elt,type) {
  var typeElt = $(elt).type.toLowerCase();
   if (type == typeElt) {
     return true;
    }
  return false;
 },
 observe: function(elt,evtName,callback) {
    elt = $(elt);
    if(elt) {
	  if (elt.addEventListener){
	   elt.addEventListener(evtName, callback, false);
	  } else if (elt.attachEvent) {
	   elt.attachEvent('on' + evtName, callback);
	  } else {
	   elt['on' + evtName] = callback;
	  }
    }
 },
 getElementsByTagAndClassName : function(tagName, cl, parentElement) {
  var children = ($(parentElement) || document.body).getElementsByTagName(tagName);
  var elements = new Array();
  for(var i=0; i < children.length; i++) {
  	var elt = children[i];
  	if (Elt.hasClass(elt,cl)) {
  		elements.push(elt);
  	}
  }
  return elements;
 },
 
 getElementsByTagNameAndType : function(tagName, type, parentElement) {
  var children = ($(parentElement) || document.body).getElementsByTagName(tagName);
  var elements = new Array();
  for(var i=0; i < children.length; i++) {
  	var elt = children[i];
  	if (Elt.hasType(elt,type)) {
  		elements.push(elt);
  	}
  }
  return elements;
 }
 
};

var Evt = {
 element: function(event) {
  return event.target || event.srcElement;
 },
 observe: function(elt,evtName,callback) {
  if (elt.addEventListener){
   elt.addEventListener(evtName, callback, false);
  } else if (elt.attachEvent) {
   elt.attachEvent('on' + evtName, callback);
  } else {
   elt['on' + evtName] = callback;
  }
 },
 stop: function(event) {
    if (event.preventDefault) {
      event.preventDefault();
      event.stopPropagation();
    } else {
      event.returnValue = false;
      event.cancelBubble = true;
    }
  }
}

function popup(url,id,width,height) {
	var dateDuJour = new Date();
	var nomPopup = id + "_" + dateDuJour.getTime();
	window.open(url, nomPopup, "toolbar=no,location=no,status=no,scrollbars=yes,resizable=yes,width=" + width + ",height=" + height);
}


function popup_sc(url,id,width,height,scrollbar) {
	window.open(url, id, "toolbar=no,location=no,status=no,scrollbars="+scrollbar+",resizable=yes,width=" + width + ",height=" + height);
}

function toOpener(url, closed) {
	if (window.opener) {
		window.opener.location.href = url;
		if(closed == 'true') {
			window.close();
		}
	} else {
		window.location.href = url;
	}
}

function setCookie(name, value, expires, path, domain, secure) {
alert("framework :   "+name);
	document.cookie =
		escape(name) + '=' + escape(value)
		+ (expires ? '; expires=' + expires.toGMTString() : '')
		+ (path ? '; path=' + path : '')
		+ (domain ? '; domain=' + domain : '')
		+ (secure ? '; secure' : '');
}


/**
* Formattage de valeur � 2 chiffres apr�s la virgule
*/
function formatNumber(valeur, locale) {

  	var partieDecimale=Math.round(Math.pow(10,2)*(Math.abs(valeur)-Math.floor(Math.abs(valeur)))) ; 
	var resultat=Math.floor(Math.abs(valeur)) + "";
	var nombre=resultat.length;
	
	//Gestion des milliers
  	for (var index = 1; index < 4; index++) {
		if (Math.floor(Math.abs(valeur)) >= Math.pow(10,(3*index))) {
			resultat=resultat.substring(0,nombre-(3*index))+" "+resultat.substring(nombre-(3*index));
		}
	}
	
	//r�ajustement partie d�cimale si n�cessaire
	var reajustDec = ""; 
	for (var index = 0; index < (2-partieDecimale.toString().length); index++) {
		reajustDec += "0";
	}
	partieDecimale = reajustDec + partieDecimale.toString();
	if (locale == 'fr')
		resultat = resultat+ "," + partieDecimale;
	else {
		resultat = resultat+ "." + partieDecimale;
	}
		
	if (parseFloat(valeur) < 0) {resultat = "-" + resultat;}
	
	return resultat;
}

String.prototype.trim = function(){
	return this.replace(/^\s+/, "").replace(/\s+$/, "");
}

String.prototype.trim2 = function(){
	var reg=new RegExp("  ", "g");
	return this.replace(reg, "").replace(/^\s+/, "").replace(/\s+$/, "");
}
