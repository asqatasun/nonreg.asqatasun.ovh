function ImageBanniere(sImg, iImgTime)
{
    var sImgId = sImg;
    var iImgTime = iImgTime;
    this.ImageId = sImgId;
    this.ImageTime = iImgTime;
}

function BanniereTournante(sNameId, iTotalTime)
{
    var sName = sNameId;
    var tStart  = null;
    var iTime = iTotalTime;
    var tListImage = new Array();
    var iVisibleImg = 0;
    var timerID = 0;

    function RegisterImage(sImgId, iPartTime) {
    
        var iImgTime = iTime * iPartTime / 100 * 1000;
        var oBanniereImage = new ImageBanniere(sImgId, iImgTime);
        var oCurrentImg = document.getElementById(sImgId);
        
        if (tListImage.length > 0) oCurrentImg.style.display = "none";
        tListImage.push(oBanniereImage);
    }
    this.RegisterImage = RegisterImage;
    function Run()
    {
        if(timerID) {
          clearTimeout(timerID);
        }
            if(this.Start == null)
            {
              this.Start   = new Date();
            }
            var   tDate = new Date();
            var   tDiff = tDate.getTime() - this.Start.getTime();
            
            if (this.ListImage[this.VisibleImg].ImageTime < tDiff)
            {
                this.VisibleImg = (this.VisibleImg + 1) % this.ListImage.length;
                for (var i = 0; i < this.ListImage.length; i++) {
                    var oCurrentImg = document.getElementById(this.ListImage[i].ImageId); 
                     if (i == this.VisibleImg)
                     {
                        oCurrentImg.style.display = "block";
                     }
                     else
                     {
                        oCurrentImg.style.display = "none";
                     }
                }
                this.Start = new Date();
            } 

        timerID = setTimeout(this.Name+".Run()", 100);
    }
    this.Run = Run;    
    this.Name = sName;
    this.Start = tStart;
    this.Time = iTime;
    this.ListImage = tListImage;
    this.VisibleImg = iVisibleImg;
}