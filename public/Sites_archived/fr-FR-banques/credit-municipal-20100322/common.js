//BEGIN CTLM extra JS SEP05 v1*/
// D�tection du navigateur
var ie4 = (document.all)? true:false;
var ns4 = (document.layers)? true:false;
var ns6 = (document.getElementById)? true:false;
var ns	= (ns4 || ns6);
var dom = (ie4 || ns6);

// systeme d'exploitation
var sys=navigator.platform;

// ----------------------------- fonction permettant de r�cupere la s�lection d'une liste d�roulante 
function getDropdownValue(theDropdown) {
	if ( (theDropdown.length!=0) && (theDropdown.selectedIndex!=-1) )
		return theDropdown.options[theDropdown.selectedIndex].value;
	else
		return '';
} // fin fonction 'getDropdownValue()'

// ----------------------------- fonction v�rifiant la validit� du formatage d'un email 
 function isValidEmail(strEmail) { // v�rif validit� email par REGEXP
   var reg = /^[a-z0-9._-]+@[a-z0-9.-]{2,}[.][a-z]{2,3}$/
   regObject = new RegExp();
   regObject = reg.exec(strEmail.toLowerCase());
   return (regObject!=null)
}// fin fonction 'isValidEmail()'

// fonction remplacement de chaine
function str_replace(whatStr, byStr, string) {
	while (string.indexOf(whatStr)>-1)
		string=string.replace(whatStr , byStr)
	return string;
}

//Pop up centered over screen
function popup(adresse,hpop,lpop,scrollbars) {
	var hauteur_popup=hpop;
	var H = (screen.height - hauteur_popup) / 2;
	var largeur_popup=lpop;
	var L = (screen.width - largeur_popup) / 2;
	pop_up = window.open(adresse,"pop","status=yes,resizable=yes,height="+hauteur_popup+",width="+largeur_popup+",top="+H+",left="+L+",scrollbars="+scrollbars);
}

// fonction de resize pour IE5 et Netscape6
function autoResize() {
	mac = (navigator.appVersion.indexOf("Macintosh")>0)? true:false;

	if(!mac) {
		largeur = document.body.offsetWidth;
		hauteur = document.body.scrollHeight+31;
		if (ns)	largeur = document.body.offsetWidth+6;

		if (hauteur) {
			hauteur = hauteur+19;
			this.resizeTo(largeur, hauteur);
			this.focus();
		}
	}
}

function openPopup(titre, top, right, width, height) {
    var win = new Window({
	        className: "alphacube",
	        title: titre,
	        showEffect: Element.show,
	        hideEffect: Element.hide,
	        minimizable:false,
	        top: top,
	        right: right,
	        width: width,
	        height: height,
	        destroyOnClose:true
	    });
	
    return win;
}

var winForgot;
function forgotPassword(url) {
	winForgot = openPopup("Mot de passe oubli&eacute;", 70, 100, 317, 235);
	
	winForgot.setAjaxContent(url);
    winForgot.showCenter(true);
    return false;
}


var winViral;
function sendFriend(url) {
	winViral = openPopup("Envoyer &agrave; un ami", 70, 100, 517, 370);
	
	winViral.setAjaxContent(url);
    winViral.showCenter(true);
    return false;
}

var winAlert;
function addAlert(url) {
	winAlert = openPopup("Ajouter une alerte", 70, 100, 450, 200);
	
	winAlert.setAjaxContent(url);
    winAlert.showCenter(true);
    return false;
}

var winOrdre;
function addOrdre(url) {
	winOrdre = openPopup("Pr&eacute;parer un ordre d'achat", 70, 100, 500, 285);
	
	winOrdre.setAjaxContent(url);
    winOrdre.showCenter(true);
    return false;
}

var winAbon;
function abonMailing(url) {
	winAbon = openPopup("Calendrier des ventes", 70, 100, 317, 150);
	
	winAbon.setAjaxContent(url);
    winAbon.showCenter(true);
    return false;
}

var winIdent;
function identRequired(url, callback) {
	winIdent = openPopup("Identification requise", 70, 100, 317, 235);
	winIdent.callback = callback;
	
	winIdent.setAjaxContent(url);
    winIdent.showCenter(true);
    return false;
}

function submitAlert(url) {
	if (winAlert) {
        if ( $('searchTy1').checked ) fv = 0;
        else fv = 1;
	    winAlert.setAjaxContent(url,
	    {
	        method: 'post',
			parameters: { searchKw: $('searchKw').value,
			                searchTy: fv,
							searchPriceMini: $('searchPriceMini').value,
							searchPriceMax: $('searchPriceMax').value,
							themes: $('themes').value }
	    }
	    );
	}
}

function submitOrdre(url) {
	if (winOrdre) {
	    winOrdre.setAjaxContent(url,
	    {
	        method: 'post',
			parameters: { Prix: $('Prix').value,
						  LotID: $('LotID').value,
						  VenteID: $('VenteID').value }
	    }
	    );
	}
}

function submitFriend(url) {
	if (winViral) {
	    winViral.setAjaxContent(url,
	    {
	        method: 'post',
			parameters: { LotID: $('LotID').value,
						  VenteID: $('VenteID').value,
						  nom: $('nom').value,
						  email: $('email').value,
						  dest1: $('dest1').value,
						  dest2: $('dest2').value,
						  dest3: $('dest3').value,
						  dest4: $('dest4').value,
						  message: $('message').value }
	    }
	    );
	}
}

var popupIMG;
function DisplayImageSize(type,id){
	var ImageId = 0;
		ImageId = id;
	var url = "/popup.php?Id="+id+"";
		
	popupIMG = openPopup("", 500, 500, 317, 235);	
	popupIMG.setAjaxContent(url);
    popupIMG.showCenter(true,0);
    popupIMG.setZIndex(10);
    return false;
}
