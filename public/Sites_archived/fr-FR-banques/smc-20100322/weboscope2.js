/*
	weboscope.js
	Version : Bo�te Noire V2
	Fonctionnalit�s :
		- groupe
		- contenu
		- alphanum
*/

/* ------------------ Variables ------------------------- */

var NB_CONTENUS_ = 0;
var NB_MAX_CONTENU_ = 20;
var TAILLE_MAX_CONTENU_ = 100;
var TAILLE_MAX_ALPHANUM_ = 30;
var WEBO_ID_GROUPE = 0;
var WEBO_VALIDE_ = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-.,;:_ %";

/* ------------------ G�n�rales ------------------------- */ 


function webo_traite_chaine (s, bag, taille_max)
{   
	var i;
	var returnString = "";
	if (s == null) return "";
	s = "" + s;
	for (i = 0; i < s.length; i++) {   
		var c = s.charAt(i);
		if (bag.indexOf(c) != -1) returnString += c;
	}
	returnString = escape(returnString);
	return returnString.substr(0,taille_max);
}

function ajout_webo_contenu (chaine_contenu)
{
	if (chaine_contenu == null) return "";
	NB_CONTENUS_++;
	if ( NB_CONTENUS_ > NB_MAX_CONTENU_ ) return "";
	chaine_contenu = webo_traite_chaine(chaine_contenu,WEBO_VALIDE_,TAILLE_MAX_CONTENU_);
	if ( NB_CONTENUS_ > 1 ) chaine_contenu = '%7C' + chaine_contenu;
	return chaine_contenu;

}

function get_webo_arg_zpi(_WEBOID,_WEBO_ALPHANUM,_WEBO_CONTENU,_ACC,_WEBOID2)
{
	var _wbs_da=new Date();
	_wbs_da=parseInt(_wbs_da.getTime()/1000 - 60*_wbs_da.getTimezoneOffset());
	var _wbs_ref=''+escape(document.referrer);
	var _wbs_ta='0x0';
	var _wbs_co=0;
	var _wbs_nav=navigator.appName;
	if (parseInt(navigator.appVersion)>=4) {
		_wbs_ta=screen.width+"x"+screen.height;
		_wbs_co=(_wbs_nav!="Netscape")?screen.colorDepth:screen.pixelDepth;
	}
	if((_ACC != null)&&(_wbs_nav!="Netscape")) {
		var _reftmp = 'parent.document.referrer';
		if((_ACC<5)&&(_ACC>0)) {
			for(var _k=_ACC;_k>1;_k--) _reftmp = 'parent.' + _reftmp;
		}
		var _mon_ref = eval(_reftmp);
		if(document.referrer == parent.location || document.referrer=='') _wbs_ref=''+escape(_mon_ref);
	}
	var _wbs_arg = "/fcgi-bin/comptage_bn2.fcgi?ID="+_WEBOID;
	if ( location.protocol == 'https:'){
		_wbs_arg = "https://recolte-v2.banquepopulaire.fr" + _wbs_arg;
	}
	else {
		_wbs_arg =  "http://recolte-v2.banquepopulaire.fr" + _wbs_arg;
	}
	if (_WEBOID2 != null) _wbs_arg+="&ID2="+_WEBOID2;
	if(_WEBO_ALPHANUM.length) _wbs_arg += _WEBO_ALPHANUM;
	if(_WEBO_CONTENU.length)  _wbs_arg += "&CONTENU="+_WEBO_CONTENU;
	_wbs_arg += "&ver=2&da2="+_wbs_da+"&ta="+_wbs_ta+"&co="+_wbs_co+"&ref="+_wbs_ref;
	return _wbs_arg;
}

function webo_bn2(_WEBO_RUBRIQUE,_WEBO_SOUS_RUBRIQUE,_WEBOID,_ACC)
{
	_WEBO_RUBRIQUE = webo_traite_chaine(_WEBO_RUBRIQUE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	_WEBO_SOUS_RUBRIQUE = webo_traite_chaine(_WEBO_SOUS_RUBRIQUE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	var WEBO_CONTENU = "";
	NB_CONTENUS_ = 0;
	for(a=4;a<arguments.length;a++) {
		WEBO_CONTENU += ajout_webo_contenu(arguments[a]);
	}
	var WEBO_ALPHANUM = "&RUBRIQUE="+_WEBO_RUBRIQUE+"&SOUS_RUBRIQUE="+_WEBO_SOUS_RUBRIQUE;
	var bnv2_wbs_arg = get_webo_arg_zpi (_WEBOID,WEBO_ALPHANUM,WEBO_CONTENU,_ACC);
	document.write('<IMG SRC="'+bnv2_wbs_arg+'" border="0" height="1" width="1" alt="">');
	return 1;
}

function webo_bn2_groupe(_WEBO_RUBRIQUE,_WEBO_SOUS_RUBRIQUE,_WEBOID,_WEBO_RUBRIQUE_GROUPE,_WEBO_SOUS_RUBRIQUE_GROUPE,_ACC)
{
	_WEBO_RUBRIQUE = webo_traite_chaine(_WEBO_RUBRIQUE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	_WEBO_SOUS_RUBRIQUE= webo_traite_chaine(_WEBO_SOUS_RUBRIQUE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	_WEBO_RUBRIQUE_GROUPE = webo_traite_chaine(_WEBO_RUBRIQUE_GROUPE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	_WEBO_SOUS_RUBRIQUE_GROUPE = webo_traite_chaine(_WEBO_SOUS_RUBRIQUE_GROUPE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	var WEBO_CONTENU = "";
	NB_CONTENUS_ = 0;
	for(a=6;a<arguments.length;a++) {
		WEBO_CONTENU += ajout_webo_contenu(arguments[a]);
	}
	var WEBO_ALPHANUM = "&RUBRIQUE="+_WEBO_RUBRIQUE+"&SOUS_RUBRIQUE="+_WEBO_SOUS_RUBRIQUE;
	WEBO_ALPHANUM += "&RUBRIQUE2="+_WEBO_RUBRIQUE_GROUPE+"&SOUS_RUBRIQUE2="+_WEBO_SOUS_RUBRIQUE_GROUPE;
	var bnv2_wbs_arg;
	if ( (_WEBO_RUBRIQUE_GROUPE != null) && (_WEBO_SOUS_RUBRIQUE_GROUPE != null) ) {
		bnv2_wbs_arg = get_webo_arg_zpi(_WEBOID,WEBO_ALPHANUM,WEBO_CONTENU,_ACC,WEBO_ID_GROUPE);
	}
	else {
		bnv2_wbs_arg = get_webo_arg_zpi(_WEBOID,WEBO_ALPHANUM,WEBO_CONTENU,_ACC);
	}
	document.write('<IMG SRC="'+bnv2_wbs_arg+'" border="0" height="1" width="1" alt="">');
	return 1;
}

function clic_bn2(_WEBO_RUBRIQUE,_WEBO_SOUS_RUBRIQUE,_WEBOID,_ACC)
{
	_WEBO_RUBRIQUE = webo_traite_chaine(_WEBO_RUBRIQUE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	_WEBO_SOUS_RUBRIQUE = webo_traite_chaine(_WEBO_SOUS_RUBRIQUE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	var WEBO_CONTENU = "";
	NB_CONTENUS_ = 0;
	for(a=4;a<arguments.length;a++) {
		WEBO_CONTENU += ajout_webo_contenu(arguments[a]);
	}
	var WEBO_ALPHANUM = "&RUBRIQUE="+_WEBO_RUBRIQUE+"&SOUS_RUBRIQUE="+_WEBO_SOUS_RUBRIQUE;
	var bnv2_wbs_arg = get_webo_arg_zpi (_WEBOID,WEBO_ALPHANUM,WEBO_CONTENU,_ACC);
	var webo_compteur_bn2 = new Image(1,1);
	webo_compteur_bn2.src=bnv2_wbs_arg;
	return 1;
}

function clic_bn2_groupe(_WEBO_RUBRIQUE,_WEBO_SOUS_RUBRIQUE,_WEBOID,_WEBO_RUBRIQUE_GROUPE,_WEBO_SOUS_RUBRIQUE_GROUPE,_ACC)
{
	_WEBO_RUBRIQUE = webo_traite_chaine(_WEBO_RUBRIQUE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	_WEBO_SOUS_RUBRIQUE= webo_traite_chaine(_WEBO_SOUS_RUBRIQUE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	_WEBO_RUBRIQUE_GROUPE = webo_traite_chaine(_WEBO_RUBRIQUE_GROUPE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	_WEBO_SOUS_RUBRIQUE_GROUPE = webo_traite_chaine(_WEBO_SOUS_RUBRIQUE_GROUPE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	var WEBO_CONTENU = "";
	NB_CONTENUS_ = 0;
	for(a=6;a<arguments.length;a++) {
		WEBO_CONTENU += ajout_webo_contenu(arguments[a]);
	}
	var WEBO_ALPHANUM = "&RUBRIQUE="+_WEBO_RUBRIQUE+"&SOUS_RUBRIQUE="+_WEBO_SOUS_RUBRIQUE;
	WEBO_ALPHANUM += "&RUBRIQUE2="+_WEBO_RUBRIQUE_GROUPE+"&SOUS_RUBRIQUE2="+_WEBO_SOUS_RUBRIQUE_GROUPE;
	var bnv2_wbs_arg;
	if ( (_WEBO_RUBRIQUE_GROUPE != null) && (_WEBO_SOUS_RUBRIQUE_GROUPE != null) ) {
		bnv2_wbs_arg = get_webo_arg_zpi(_WEBOID,WEBO_ALPHANUM,WEBO_CONTENU,_ACC,WEBO_ID_GROUPE);
	}
	else {
		bnv2_wbs_arg = get_webo_arg_zpi(_WEBOID,WEBO_ALPHANUM,WEBO_CONTENU,_ACC);
	}
	var webo_compteur_bn2 = new Image(1,1);
	webo_compteur_bn2.src=bnv2_wbs_arg;
	return 1;
}

webo_ok	= 1;
