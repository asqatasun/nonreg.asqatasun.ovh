/*
Objet: Scripts d'enregistrement des statistiques
Version: 2.0
D�pendance : nbp_statistique_cst_v2.js
Historiques :
	04/06/2007 - Int�gration de la nouvelle version weboscope.js
	07/09/2007 - MAJ Parc GIE - natixis.com - init console de consolidation
*/

var gNBPStatScriptPresent=true;

function FWKActiver_stat(lSiteId, lServeurId)
{
	if (lSiteId!=null) top.gNBPSiteId=lSiteId;
	else if (top.gNBPSiteIdSave) top.gNBPSiteId=top.gNBPSiteIdSave;
	if (lServeurId!=null) top.gNBPServeurId=lServeurId;
}

function FWKDesactiver_stat()
{
	if (top.gNBPSiteId) {
		top.gNBPSiteIdSave=top.gNBPSiteId;
		top.gNBPSiteId=null;
	}
}

/* modifi�e par Archiweb*/
function FWKEnvoi_stat(aZoneId,aPageId,aZoneIdGroupe,aPageIdGroupe,aProfondeur,aSiteId){
	/* WEBO_ID*/
	var lSiteId=aSiteId;
	
	/* identifiant du serveur*/
	var lServeurId;
	
	var lWeboContenu = "";
	
	/*
	  Variables utilis�es dans le cas de la consolidation 
	  des donn�es de tous les sites dans une vue globale. 
	  WEBO_ZONE_GROUPE
	  WEBO_PAGE_GROUPE
	*/
	var lZoneIdGroupe;
	var lPageIdGroupe;
	
	/**
	   Tableau utilis� dans le cas de l'insertion des donn�es
	   de contenus. 
	   Contient les param�tres ajout�s durant l'appel de la fonction
	   FWKEnvoi_stat. Le maximum de contenu est de 20.
	   WEBO_CONTENU1
	   WEBO_CONTENU2
	   ...
	   WEBO_CONTENU20
	 */
	var lArgument= new Array;
	var j=0;
	
	//Si absent ou chaine vide
	if (lSiteId==null) {
		lSiteId=top.gNBPSiteId;
	}
	//Si absent ou chaine vide
	if (lServeurId==null) {
		lServeurId=top.gNBPServeurId;
	}
		
	/* 
	   Dans le cas de l'insertion des donn�es de contenus
	   R�cup�ration des nouveaux param�tres pass�s la fonction FWKEnvoi_stat.
	   Les param�tres ajout�s doivent �tre inser�s apr�s le param�tre: aServeurId.
	 */
	for(i=6; i<arguments.length; i++){
			lWeboContenu+=ajout_webo_contenu(arguments[i]);	
	}

	/* Option =1: Consolidation Horizentale */
	if(top.gNBPConsolidationType =='1'){
		lZoneIdGroupe=aZoneId;
            lPageIdGroupe=aPageId;	
	}

	/* Option =2: Consolidation vertical, 1 niveau */
	if(top.gNBPConsolidationType =='2'){
		lZoneIdGroupe=lSiteId;
            lPageIdGroupe=aZoneId;	
	}

	/* Option =3: Consolidation vertical, 2 niveaux */
	if(top.gNBPConsolidationType =='3'){
                    lZoneIdGroupe=aZoneIdGroupe;
            lPageIdGroupe=lSiteId;	
	}


	/* Option =4: Consolidation Libre */
	if(top.gNBPConsolidationType =='4'){
		lZoneIdGroupe=aZoneIdGroupe;
		lPageIdGroupe=aPageIdGroupe;
	}


	//Appel fct originale
	if (lSiteId!=null && lServeurId!=null) {
		if(lZoneIdGroupe!=null && lPageIdGroupe!=null){
			webo_bn2_groupe(aZoneId,aPageId,lSiteId,lZoneIdGroupe,lPageIdGroupe,aProfondeur,lServeurId,lWeboContenu);
		}else{
			webo_bn2(aZoneId,aPageId,lSiteId,aProfondeur,lServeurId,lWeboContenu);
		}
	}
}


/*
	weboscope.js : version 2007
	Version : Bo�te Noire V2
	Fonctionnalit�s :
		- groupe
		- contenu
		- alphanum
*/

/* ------------------ Variables ------------------------- */

var NB_CONTENUS_ = 0;
var NB_MAX_CONTENU_ = 20;
var TAILLE_MAX_CONTENU_ = 100;
var TAILLE_MAX_ALPHANUM_ = 30;
var WEBO_ID_GROUPE = '103';
var WEBO_VALIDE_ = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-.,;:_ %";
var WEBO_SERV="weboscope-recolte-internet2.natixis.com";

/* ------------------ G�n�rales ------------------------- */ 


function webo_traite_chaine (s, bag, taille_max)
{   
	var i;
	var returnString = "";
	if (s == null) return "";
	s = "" + s;
	for (i = 0; i < s.length; i++) {   
		var c = s.charAt(i);
		if (bag.indexOf(c) != -1) returnString += c;
	}
	returnString = escape(returnString);
	return returnString.substr(0,taille_max);
}

function ajout_webo_contenu (chaine_contenu)
{
	if (chaine_contenu == null) return "";
	NB_CONTENUS_++;
	if ( NB_CONTENUS_ > NB_MAX_CONTENU_ ) return "";
	chaine_contenu = webo_traite_chaine(chaine_contenu,WEBO_VALIDE_,TAILLE_MAX_CONTENU_);
	if ( NB_CONTENUS_ > 1 ) chaine_contenu = '%7C' + chaine_contenu;
	return chaine_contenu;

}

/* 
 	modif ATIA-AAP-MCO
 	Ajout d'un nouveau argument � la fonction: get_webo_arg_zpi().
 	_SRV : identifiant du serveur.
 		
*/

function get_webo_arg_zpi(_WEBOID,_WEBO_ALPHANUM,_WEBO_CONTENU,_ACC,_SRV,_WEBOID2)
{
	var _wbs_da=new Date();
	_wbs_da=parseInt(_wbs_da.getTime()/1000 - 60*_wbs_da.getTimezoneOffset());
	var _wbs_ref=''+escape(document.referrer);
	var _wbs_ta='0x0';
	var _wbs_co=0;
	var _wbs_nav=navigator.appName;
	if (parseInt(navigator.appVersion)>=4) {
		_wbs_ta=screen.width+"x"+screen.height;
		_wbs_co=(_wbs_nav!="Netscape")?screen.colorDepth:screen.pixelDepth;
	}
	if((_ACC != null)&&(_wbs_nav!="Netscape")) {
		var _reftmp = 'parent.document.referrer';
		if((_ACC<5)&&(_ACC>0)) {
			for(var _k=_ACC;_k>1;_k--) _reftmp = 'parent.' + _reftmp;
		}
		var _mon_ref = eval(_reftmp);
		if(document.referrer == parent.location || document.referrer=='') _wbs_ref=''+escape(_mon_ref);
	}
	
	
	//MODIF NBP
	if (_wbs_ref.indexOf('%3F')>0) _wbs_ref=_wbs_ref.substring(0,_wbs_ref.indexOf('%3F'));
	var _wbs_arg = location.protocol+"//"+_SRV+"/fcgi-bin/comptage_bn2.fcgi?ID="+_WEBOID;
	
	/*
		//MODIF NBP
		var _wbs_arg = "/fcgi-bin/comptage_bn2.fcgi?ID="+_WEBOID;
		if ( location.protocol == 'https:'){
			_wbs_arg = "https://weboscope-recolte-internet1.natixis.com" + _wbs_arg;
		}
		else {
			_wbs_arg =  "http://weboscope-recolte-internet1.natixis.com" + _wbs_arg;
		}
	*/
	
	if (_WEBOID2 != null) _wbs_arg+="&ID2="+_WEBOID2;
	if(_WEBO_ALPHANUM.length) _wbs_arg += _WEBO_ALPHANUM;
	if(_WEBO_CONTENU.length)  _wbs_arg += "&CONTENU="+_WEBO_CONTENU;
	_wbs_arg += "&ver=2&da2="+_wbs_da+"&ta="+_wbs_ta+"&co="+_wbs_co+"&ref="+_wbs_ref;
	return _wbs_arg;
}

/* 
 	modif ATIA-AAP-MCO
 	Ajout d'un nouveau argument � la fonction: webo_bn2().
 	_SRV : identifiant du serveur.
 	_WEBO_CONTENU: le contenu web
 		
*/

function webo_bn2(_WEBO_RUBRIQUE,_WEBO_SOUS_RUBRIQUE,_WEBOID,_ACC, _SRV, _WEBO_CONTENU)
{
	_WEBO_RUBRIQUE = webo_traite_chaine(_WEBO_RUBRIQUE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	_WEBO_SOUS_RUBRIQUE = webo_traite_chaine(_WEBO_SOUS_RUBRIQUE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	
	NB_CONTENUS_ = 0;
	
	/*	modif ATIA-AAP-MCO
		var WEBO_CONTENU = "";
		for(a=4;a<arguments.length;a++) {
			WEBO_CONTENU += ajout_webo_contenu(arguments[a]);
		}
	*/
	var WEBO_ALPHANUM = "&RUBRIQUE="+_WEBO_RUBRIQUE+"&SOUS_RUBRIQUE="+_WEBO_SOUS_RUBRIQUE;
	var bnv2_wbs_arg = get_webo_arg_zpi (_WEBOID,WEBO_ALPHANUM,_WEBO_CONTENU,_ACC,_SRV);
	//document.write('<IMG SRC="'+bnv2_wbs_arg+'" border="0" height="1" width="1" alt="">'); //FL/EJ le 26/0/07 -> Pose un probl�me avec JCMS
	return 1;
}

/* 
 	modif ATIA-AAP-MCO
 	Ajout d'un nouveau argument � la fonction: webo_bn2_groupe().
 	_SRV : identifiant du serveur.
 	_WEBO_CONTENU: le contenu web
 		
*/

function webo_bn2_groupe(_WEBO_RUBRIQUE,_WEBO_SOUS_RUBRIQUE,_WEBOID,_WEBO_RUBRIQUE_GROUPE,_WEBO_SOUS_RUBRIQUE_GROUPE,_ACC,_SRV,_WEBO_CONTENU)
{
	_WEBO_RUBRIQUE = webo_traite_chaine(_WEBO_RUBRIQUE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	_WEBO_SOUS_RUBRIQUE= webo_traite_chaine(_WEBO_SOUS_RUBRIQUE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	_WEBO_RUBRIQUE_GROUPE = webo_traite_chaine(_WEBO_RUBRIQUE_GROUPE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	_WEBO_SOUS_RUBRIQUE_GROUPE = webo_traite_chaine(_WEBO_SOUS_RUBRIQUE_GROUPE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	
	NB_CONTENUS_ = 0;
	
	/*      modif ATIA-AAP-MCO
		var WEBO_CONTENU = "";
		for(a=6;a<arguments.length;a++) {
			WEBO_CONTENU += ajout_webo_contenu(arguments[a]);
	}*/
	
	var WEBO_ALPHANUM = "&RUBRIQUE="+_WEBO_RUBRIQUE+"&SOUS_RUBRIQUE="+_WEBO_SOUS_RUBRIQUE;
	WEBO_ALPHANUM += "&RUBRIQUE2="+_WEBO_RUBRIQUE_GROUPE+"&SOUS_RUBRIQUE2="+_WEBO_SOUS_RUBRIQUE_GROUPE;
	var bnv2_wbs_arg;
	if ( (_WEBO_RUBRIQUE_GROUPE != null) && (_WEBO_SOUS_RUBRIQUE_GROUPE != null) ) {
		bnv2_wbs_arg = get_webo_arg_zpi(_WEBOID,WEBO_ALPHANUM,_WEBO_CONTENU,_ACC,_SRV,WEBO_ID_GROUPE);
	}
	else {
		bnv2_wbs_arg = get_webo_arg_zpi(_WEBOID,WEBO_ALPHANUM,_WEBO_CONTENU,_ACC,_SRV);
	}
	document.write('<IMG SRC="'+bnv2_wbs_arg+'" border="0" height="1" width="1" alt="">');
	return 1;
}


/* 
 	modif ATIA-AAP-MCO
 	Ajout d'un nouveau argument � la fonction: clic_bn2().
 	_SRV : identifiant du serveur.
 	_WEBO_CONTENU: le contenu web
 		
*/
function clic_bn2(_WEBO_RUBRIQUE,_WEBO_SOUS_RUBRIQUE,_WEBOID,_ACC, _SRV, _WEBO_CONTENU)
{
	_WEBO_RUBRIQUE = webo_traite_chaine(_WEBO_RUBRIQUE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	_WEBO_SOUS_RUBRIQUE = webo_traite_chaine(_WEBO_SOUS_RUBRIQUE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	
	NB_CONTENUS_ = 0;
	
	/*
		 modif ATIA-AAP-MCO
		var WEBO_CONTENU = "";
		for(a=4;a<arguments.length;a++) {
			WEBO_CONTENU += ajout_webo_contenu(arguments[a]);
		}
	*/
	var WEBO_ALPHANUM = "&RUBRIQUE="+_WEBO_RUBRIQUE+"&SOUS_RUBRIQUE="+_WEBO_SOUS_RUBRIQUE;
	var bnv2_wbs_arg = get_webo_arg_zpi (_WEBOID,WEBO_ALPHANUM,_WEBO_CONTENU,_ACC,_SRV);
	var webo_compteur_bn2 = new Image(1,1);
	webo_compteur_bn2.src=bnv2_wbs_arg;
	return 1;
}


/* 
 	modif ATIA-AAP-MCO
 	Ajout d'un nouveau argument � la fonction: clic_bn2_groupe().
 	_SRV : identifiant du serveur.
 	_WEBO_CONTENU: le contenu web
 		
*/
function clic_bn2_groupe(_WEBO_RUBRIQUE,_WEBO_SOUS_RUBRIQUE,_WEBOID,_WEBO_RUBRIQUE_GROUPE,_WEBO_SOUS_RUBRIQUE_GROUPE,_ACC,_SRV,_WEBO_CONTENU)
{
	_WEBO_RUBRIQUE = webo_traite_chaine(_WEBO_RUBRIQUE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	_WEBO_SOUS_RUBRIQUE= webo_traite_chaine(_WEBO_SOUS_RUBRIQUE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	_WEBO_RUBRIQUE_GROUPE = webo_traite_chaine(_WEBO_RUBRIQUE_GROUPE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	_WEBO_SOUS_RUBRIQUE_GROUPE = webo_traite_chaine(_WEBO_SOUS_RUBRIQUE_GROUPE,WEBO_VALIDE_,TAILLE_MAX_ALPHANUM_);
	
	NB_CONTENUS_ = 0;
	
	/*
		modif ATIA-AAP-MCO
		var WEBO_CONTENU = "";
		for(a=6;a<arguments.length;a++) {
			WEBO_CONTENU += ajout_webo_contenu(arguments[a]);
		}
	*/
	var WEBO_ALPHANUM = "&RUBRIQUE="+_WEBO_RUBRIQUE+"&SOUS_RUBRIQUE="+_WEBO_SOUS_RUBRIQUE;
	WEBO_ALPHANUM += "&RUBRIQUE2="+_WEBO_RUBRIQUE_GROUPE+"&SOUS_RUBRIQUE2="+_WEBO_SOUS_RUBRIQUE_GROUPE;
	var bnv2_wbs_arg;
	if ( (_WEBO_RUBRIQUE_GROUPE != null) && (_WEBO_SOUS_RUBRIQUE_GROUPE != null) ) {
		bnv2_wbs_arg = get_webo_arg_zpi(_WEBOID,WEBO_ALPHANUM,_WEBO_CONTENU,_ACC,_SRV,WEBO_ID_GROUPE);
	}
	else {
		bnv2_wbs_arg = get_webo_arg_zpi(_WEBOID,WEBO_ALPHANUM,_WEBO_CONTENU,_ACC,_SRV);
	}
	var webo_compteur_bn2 = new Image(1,1);
	webo_compteur_bn2.src=bnv2_wbs_arg;
	return 1;
}

webo_ok	= 1;
