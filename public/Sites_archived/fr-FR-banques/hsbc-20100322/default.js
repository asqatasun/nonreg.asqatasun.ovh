function previousSiblingByTagName( tagName, referenceElement )
{
	//goes through the previous siblings until it finds the node with the tag name specified
	while( referenceElement.previousSibling != null )
	{
		if( referenceElement.previousSibling.nodeName == tagName )
		{
			//return the node when we have found it
			return referenceElement.previousSibling;
		}
		referenceElement = referenceElement.previousSibling;
	}
	//if it cannot find a previous sibling that matches the tag name i.e. a previousSibling=null null is returned
	return null;		
}

function nextSiblingByTagName( tagName, referenceElement )
{
	//goes through the next siblings until it finds the node with the tag name specified
	while( referenceElement.nextSibling != null )
	{
		if( referenceElement.nextSibling.nodeName == tagName )
		{
			//return the node when we have found it
			return referenceElement.nextSibling;
		}
		referenceElement = referenceElement.nextSibling;
	}
	//if it cannot find a previous sibling that matches the tag name i.e. a previousSibling=null null is returned
	return null;		
}

function showPara( event )
{
	for( var i = 0; i<hideShowParagraphList.length; i++ )
	{
		hideShowParagraphList[i].style.display = "none";
	}
	var thisNode = this;
	if( event.srcElement )
	{
		thisNode = 	event.srcElement;
	}
	//window.alert( thisNode.tagName );
	var pToShow = nextSiblingByTagName( "P", thisNode.parentNode )
	pToShow.style.display = "";
	pToShow.className = "jsParagraph";
}
//not used
function hidePara( event )
{
	var thisNode = this;
	if( event.srcElement )
	{
		thisNode = 	event.srcElement;
	}
	pToHide = nextSiblingByTagName( "P", thisNode.parentNode )
	pToHide.style.display = "none";
	pToHide.className = "";
}

//cls = class
//n = node
//t = tag
document.getElementsByClassName = function(cls,n,t)
{
	var rtn = [];
	n=n===null?document:n;
	t=t===null?'*':t;
	var els = n.getElementsByTagName ? n.getElementsByTagName(t) : document.all;
	els = (!els||!els.length ) && document.all ? document.all : els;
	if(cls==null){return els;}
	for (var i=0,j=0; i < els.length; i++)
	{
		if(els[i].className.match("(^|\\s)"+cls+"(\\s|$)"))
		{
			rtn[j++] = els[i];
		}
	}
	return rtn;
};

function jsfAttachEvent(obj,evt,fnc)
{
	if(window.addEventListener)
	{
		obj.addEventListener(evt, fnc, false);
	}
	else if(window.attachEvent)
	{
		obj.attachEvent('on'+evt, fnc);
	}
	else if (obj.getElementById && evt=='load')
	{
		obj.onload = fnc;
	}
}

jsfAttachEvent(window,'load',jsfOnLoad);

var hideShowParagraphList;

function jsfOnLoad()
{
	//window.alert(document.getElementsByClassName( "hsbcGP2007Ext1024", document.body, "DIV" ).length == 0);
	if( document.getElementById( "jsPresentationTransform01" ) && document.getElementsByClassName( "hsbcGP2007Ext1024", document.body, "DIV" ).length == 0 )//check to see if the element exists and make sure the script does not run on 1024 sites
	{
		//handle on the root node
		var rootNode = document.getElementById( "jsPresentationTransform01" );
		//hide all the p tags within the root node
		hideShowParagraphList = rootNode.getElementsByTagName( "P" );
		var h2List = rootNode.getElementsByTagName( "H2" );
		var colList = document.getElementsByClassName( "hsbcGP2007Column", rootNode, "DIV" );
		for( var i=0; i<hideShowParagraphList.length; i++ )/* There should always be as many paragraphs as there are columns and headings */
		{
			hideShowParagraphList[i].style.display = "none";
			h2List[i].className = "jsHeading";
			colList[i].className = "jsColumn";
			jsfAttachEvent( h2List[i].getElementsByTagName( "A" )[0], "mouseover", showPara );//assign onhover event handlers
		}
		//show the first one by default
		hideShowParagraphList[0].style.display = "";
		hideShowParagraphList[0].className = "jsParagraph";
	}
}
