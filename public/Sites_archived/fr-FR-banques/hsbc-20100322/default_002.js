/*
----------------------------------------------------------------------------------------------
JavaScript for: On-Hover extended information display
Author:   Gordon Dougal
Created:  03-01-2007
Updated:  

History:

---------------------------------------------------------------------------------------------- */

var isIE  = (navigator.appVersion.indexOf("MSIE") != -1) ? true : false;
var isWin = (navigator.appVersion.toLowerCase().indexOf("win") != -1) ? true : false;
var isOpera = (navigator.userAgent.indexOf("Opera") != -1) ? true : false;
var isSafari = (navigator.userAgent.indexOf("Safari") != -1) ? true : false;
var requiredMajorVersion = 6;
var requiredMinorVersion = 0;
var requiredRevision = 0;
var tabRelatedBlocksList = new Array();
var arrayOfLinks = new Array();

// Setup global variables
//
// fontSize - default value
// fMax - largest font size
// fMin - smallest font size
// fStep - font size increment
var fontSize = 16;
var fMax = 30;
var fMin = 10;
var fStep = 2;

// setFontSize
// Retrieves cookie value and applies to font size
function setFontSize() {
	var tempSize = getCookie("fontSize");
	if((tempSize != null) && (tempSize >= fMin) && (tempSize <= fMax)) {
		fontSize = tempSize;		
	} else {
		setCookie("fontSize", 16, "", "/");
	}
	document.body.style.fontSize = (fontSize) + "px";
}

// changeFontSize(bool increment)
// changes document font size and records size value in cookie
function changeFontSize(increment) {
	if(increment) {
		fontSize=parseInt(fontSize) + parseInt(fStep);
	} else {
		fontSize=parseInt(fontSize) - parseInt(fStep);
	}

	if(fontSize > fMax) {
		fontSize = fMax;
	}
	if(fontSize < fMin) {
		fontSize = fMin;
	}
	
	document.body.style.fontSize = (fontSize) + "px";
	setCookie('fontSize', fontSize, "", "/");
}
// incrementFontSize
function incrementFontSize() {
	changeFontSize(true);
}

// decrementFontSize
function decrementFontSize() {
	changeFontSize(false);
}

// do_onload
function do_onload() {
	setFontSize();
}

// trigger onLoad function (do_onload)
if (window.addEventListener) {
	window.addEventListener("load", do_onload, false);
} else {
	if (window.attachEvent) {
		window.attachEvent("onload", do_onload);
	} else {
		if (document.getElementById) {
			window.onload = do_onload;
		}
	}
}



//for search box
function checkForm(){
 var valid = false;
 if (document.form01.sq.value == '') {
	alert("Please type the word(s) you wish to search for.");
 }else{
	valid = true;
 }
 return valid;
}
	
//
// Sets a Cookie with the given name and value.
//
// name       Name of the cookie
// value      Value of the cookie
// [expires]  Expiration date of the cookie (default: end of current session)
// [path]     Path where the cookie is valid (default: path of calling document)
// [domain]   Domain where the cookie is valid
//              (default: domain of calling document)
// [secure]   Boolean value indicating if the cookie transmission requires a
//              secure transmission
//
function setCookie(name, value, expires, path, domain, secure) {
    document.cookie= name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires.toGMTString() : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}

//
// Gets the value of the specified cookie.
//
// name  Name of the desired cookie.
//
// Returns a string containing value of specified cookie,
//   or null if cookie does not exist.
//
function getCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    } else {
        begin += 2;
    }
    var end = document.cookie.indexOf(";", begin);
    if (end == -1) {
        end = dc.length;
    }
    return unescape(dc.substring(begin + prefix.length, end));
}

function stringToDate (stringDate) {
	// DD-MM-YYYY HH:MI
	return new Date( sd.substr(6,4), sd.substr(3,2), sd.substr(0,2), sd.substr(11,2), sd.substr(14,2), "00");
}
function todayDelayedOf(offset){
	// offset's unit is day
	var d = new Date();
	d.setTime(d.getTime() + (1000*60*60*24*offset));
	return d;
}
// gets an array of elements with a specific class name within a specific type of tag, and element.
document.getElementsByClassName = function(cls,n,t)
{
	var rtn = [];
	n=n===null?document:n;
	t=t===null?'*':t;
	var els = n.getElementsByTagName ? n.getElementsByTagName(t) : document.all;
	els = (!els||!els.length ) && document.all ? document.all : els;
	if(cls==null){return els;}
	for (var i=0,j=0; i < els.length; i++)
	{
		if(els[i].className.match("(^|\\s)"+cls+"(\\s|$)"))
		{
			rtn[j++] = els[i];
		}
	}
	return rtn;
};

// attaches events to objects
function jsfAttachEvent(obj,evt,fnc)
{
	if(window.addEventListener)
	{
		obj.addEventListener(evt, fnc, false);
	}
	else if(window.attachEvent)
	{
		obj.attachEvent('on'+evt, fnc);
	}
	else if (obj.getElementById && evt=='load')
	{
		obj.onload = fnc;
	}
}

jsfAttachEvent(window,'load',jsfOnLoad);


// the onload function below sets up all the events on the page.  In this case it hides all the details divs, and then adds 
// trigger events on to the title areas.  These triggers add an event to the custom event queue described below.
function jsfOnLoad()
{

	var extInfoAreas = document.getElementsByClassName("jsfAdditionalInfo", document, "h3");

	for (var i=0; i<extInfoAreas.length; i++)
	{
		if (extInfoAreas[i].childNodes[0].tagName == "A")
			var linkTag = extInfoAreas[i].childNodes[0];
		else
			var linkTag = extInfoAreas[i].childNodes[1];
		
		jsfAttachEvent(linkTag, 'mouseover', function(event) 
		{	
			var srcElm = getEventSource(event);			
			var tarElm = srcElm.parentNode.nextSibling;
			while (tarElm.nodeType != 1)
			{
				tarElm = tarElm.nextSibling;
			}
			
			tarElm.className = "extShown";
			
		});
		
		jsfAttachEvent(linkTag, 'mouseout', function(event) 
		{	
			var srcElm = getEventSource(event);	
			var tarElm = srcElm.parentNode.nextSibling;
			
			while (tarElm.nodeType != 1)
			{
				tarElm = tarElm.nextSibling;
			}
			
			if (isSafari)
			{
				tarElm.className = "hsbcTextStyle04";
			}
			else
			{
				setTimeout(function() {tarElm.className = "hsbcTextStyle04";}, 400);
			}
		});

	}
	
	if( document.getElementById( "jsHideShowTabBlock" ) )
	{
		initTabRelatedBlocksList( "jsHideShowTabBlock", "tabContentBox" );
		registerHideShowTabBlock( "jsHideShowTabBlock" );
		showOneHideTheRest( tabRelatedBlocksList, 0 );//needs to be called so that we know which one to display by default
	}
}

function getEventSource(event)
{
	if (event.srcElement)
		return event.srcElement;
	else
		return event.target;
}



/* in-page tab */

function previousSiblingByTagName( tagName, referenceElement )
{
	//goes through the previous siblings until it finds the node with the tag name specified
	while( referenceElement.previousSibling != null )
	{
		if( referenceElement.previousSibling.nodeName == tagName )
		{
			//return the node when we have found it
			return referenceElement.previousSibling;
		}
		referenceElement = referenceElement.previousSibling;
	}
	//if it cannot find a previous sibling that matches the tag name i.e. a previousSibling=null null is returned
	return null;		
}

function nextSiblingByTagName( tagName, referenceElement )
{
	//goes through the next siblings until it finds the node with the tag name specified
	while( referenceElement.nextSibling != null )
	{
		if( referenceElement.nextSibling.nodeName == tagName )
		{
			//return the node when we have found it
			return referenceElement.nextSibling;
		}
		referenceElement = referenceElement.nextSibling;
	}
	//if it cannot find a previous sibling that matches the tag name i.e. a previousSibling=null null is returned
	return null;		
}

function initTabRelatedBlocksList( mainContainerEleId, classToRegister )
{
	if( document.getElementById && document.getElementsByTagName )
	{
		if( document.getElementById( mainContainerEleId ) )
		{
			tabRelatedBlocksList = document.getElementsByClassName( classToRegister, document.getElementById( mainContainerEleId ),"DIV" );			
		}
	}
}
function showOneHideTheRest( arrayOfElements, position )
{
	for( var i=0; i<arrayOfElements.length; i++ )
	{
		if( i == position )				
		{
			arrayOfElements[i].style.display = "";
			//arrayOfLinks[i].className=="extTwoLineTab"||arrayOfLinks[i].className=="tabSelected extTwoLineTab" ? arrayOfLinks[i].className="tabSelected extTwoLineTab" : arrayOfLinks[i].className="tabSelected";
			switch (arrayOfLinks[i].className)
				{
				case "extTwoLineTab", "tabSelected extTwoLineTab":
					{
					arrayOfLinks[i].className="tabSelected extTwoLineTab";
					break;
					}
				case "lastItem", "tabSelected lastItem":
					{
					arrayOfLinks[i].className="tabSelected lastItem";
					break;
					}
				default:
					{
					arrayOfLinks[i].className="tabSelected";
					}
				}
					
		}
		else
		{
			arrayOfElements[i].style.display = "none";
		}
	}
}

function registerHideShowTabBlock( mainContainerEleId )
{
	//check for support of the DOM
	if( document.getElementById && document.getElementsByTagName )
	{
		//check the node I am looking for exists
		if( document.getElementById( mainContainerEleId ) )
		{
			//get hold of the main container
			var mainContainerNode = document.getElementById( mainContainerEleId );
			
			//get hold of the first UL in the container - this is our toggle block
			var firstULNode = mainContainerNode.getElementsByTagName( "UL" )[0];
			
			//go through the LI elements and find the A tags and assign onclick event handlers
			var LINodeList = firstULNode.getElementsByTagName( "LI" );
			
			for( var i=0; i<LINodeList.length; i++ )
			{
				LINodeList[i].getElementsByTagName( "A" )[0].onclick = showTabAndRelatedBlock;
				arrayOfLinks.push(LINodeList[i]);
			}
		}
	}
}
function showTabAndRelatedBlock()
{
	var positionInList;
	
	//takes the position and assigns the right class to the LI elements to reflect the selected state
	//removes all the class names from the other LI elements
	
	//'this' keyword refers to the A tag
	//get the LI which holds the A
	var parentLI = this.parentNode;
	
	//get the parent LI class name, to make sure you add this back on when changing the class later
	var parentLIClass = parentLI.className;
	
			
	//get the previous LI element
	var previousLISibling = previousSiblingByTagName( "LI", parentLI );			
	
	//get the list which holds the LI's
	var parentUL = parentLI.parentNode;
	
	//get the array of LI items within the list
	var LIItems = parentUL.getElementsByTagName( "LI" );

	//go through list and remove all classes
	//find the location of the list item within the list
	for( var i=0; i<LIItems.length; i++ )
	{
	
		if(LIItems[i].className=="tabSelected extTwoLineTab" || LIItems[i].className=="tabSelected extTwoLineTab" || LIItems[i].className=="extTwoLineTab"){
			LIItems[i].className="extTwoLineTab";
		}
		else
			{
			if (LIItems[i].className.indexOf("lastItem",0) == -1)
				{
				LIItems[i].className="";
				}
				else 
				{
				LIItems[i].className="lastItem";
				}
			}
		if( LIItems[i] == parentLI )
		{
			positionInList = i;
		}
	}	
	
	//set the appropriate class on the selected LI
	
	if(parentLIClass != "")
	{
		positionInList==0?parentLI.className = "tabSelected "+parentLIClass:parentLI.className = "tabSelected "+parentLIClass;
	}
	else
	{
		positionInList==0?parentLI.className = "tabSelected":parentLI.className = "tabSelected";
	}
	
	//set the appropriate tag on the preceeding LI
	//if( previousLISibling != null )
	//{
		//previousLISibling.className += "beforeSelectedElement";
	//}		
	
	//takes the position and assigns the block with the correct class (might want to assign style rather than class. Class is likely to be more elegant)
	showOneHideTheRest( tabRelatedBlocksList, positionInList );
	
	return false;
}
