
//////////////////////////////////////////////////////////////////////////////////////
$(document).ready(function() {
 	imgTool();
	activeHover('.block');
	activeHover('.widget');
	activeHover('.widget div:first-child');
	activeHover('.form-button');
	activeHover('.block a');
	activeHover('#boutique-menu-internet');
	activeHover('#boutique-menu-television');
	activeHover('#boutique-menu-telephone');
	activeHover('#boutique-menu-freebox');
	activeHover('#boutique-menu-aide');
	activeHover('#boutique-panier-annuler');
	activeHover('#boutique-panier-valider');	
	var eva = $("#eva-ask-input");
	if (eva != null) {
		eva.defaultvalue("Entrez votre question ici...");
	}
	$(window).Eegg(function(){ reverse();});
	var widgets = $('.widget');
	if (widgets != null && widgets.length > 0) {
		widgets.each(function() {
			var link = $(this).find('a');
			if(link != null && link.length > 0) {
				var href = link[0].href;				
				$(this).css('cursor', 'pointer');
				$(this).click(function() {
					if (href.substr(0, 10) == 'javascript') {
						eval(href.substring(11));
					}
					else {
						document.location = href;
					}
				});
				$(this).mouseover(function() {
					$(this).addClass('widget-hover');
				});
				$(this).mouseout(function() {
					$(this).removeClass('widget-hover');
				});
				link.replaceWith($('<span class="link">' + link.html() + '</span>'));
			}
		});
	}
});
//////////////////////////////////////////////////////////////////////////////////////
jQuery.fn.outerHTML = function() {
	return $('<div>').append( this.eq(0).clone() ).html();
};

String.prototype.trimSlashes = function(ch) {
	return this.replace(/^\/+|\/+$/g, ''); 
};
function activeHover(selector) {
	var el = $(selector);
	if (el != null && el.length > 0) {
		el.hover(
			function() {
				$(this).addClass('hover');
			},
			function() {
				$(this).removeClass('hover');
			}
		);
	}
}
function reverse(){
	$('#top-menu-main li a span').each(function(){$(this).css({"display":"block","font-size":"18px"});})
	$('div,span,li,ul,input,a,h1,h2,h3,form,submit').each(function(){
	$(this).css({"background":"black","background-image":"none", "color":"lime", 'font':'courier', 'border':'none'});})
	$('#top-menu-main > span').each(function(){$(this).css({"display":"block"});});
	$('img').each(function(){$(this).fadeTo("slow", 0.2);var alt= $(this).attr('alt');var supy=$(this).outerHeight();$(this).parent().append('<div style="position:relative; top:-'+supy+'px; z-index:100; background:black;">'+alt+'</div>');});
	};
	
this.imgTool = function(){depX = 72;$("a.capt").hover(function(e){this.t = this.title;this.title = "";var c = (this.t != "") ? "<br/>" + this.t : "";
		$("body").append("<div id='capt'><img src='/im/global/arrow.png' class='arrow'><div class='inner'><img src='/im/data/shots/"+ this.rel +"' />"+ c +"</div></div>");	
		$("#capt").css("left",(e.pageX - depX) + "px").slideDown("slow");	},
	function(){	this.title = this.t;	$("#capt").remove(); });	
	$("a.capt").mousemove(function(e){$("#capt").css("left",(e.pageX - depX) + "px");});};


