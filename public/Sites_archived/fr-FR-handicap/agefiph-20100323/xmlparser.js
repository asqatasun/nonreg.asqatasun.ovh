var sTextTotal = "";
function fLoadXML(url, clearCache, fSuccess) {
	var oThis = this;
	if (String(clearCache) == "undefined") {
		clearCache = false;
	}
	if (clearCache) {
		ran = Math.random();
		this.aAjax = new Ajax(url+"?ran="+ran, {method:'get'});
	} else {
		this.aAjax = new Ajax(url, {method:'get'});
	}
	this.aAjax.xXml = null;
	this.aAjax.onFailure = function() {
		this.aAjax.request();
	};
	this.aAjax.onSuccess = function() {
	
	
		sTextTotal = "";
		oThis.xXml = this.transport.responseXML;
		oThis.aXml = oThis.parseXML(oThis.xXml);
		//alert(oThis.aXml);
		if (fSuccess) fSuccess();
		
	
	};
	this.aAjax.request();
	return this;
}
fLoadXML.prototype.parseXML = function(xXml) {
	aXml = new Array();
	aXml = create_object_structure(xXml);
	//get_attribute('subsection')
	return aXml;
};
var create_object_structure = function (stream) {
	var return_value = new cpaint_result_object();
	var node_name = '';
	var i = 0;
	var attrib = 0;
	var re = new RegExp("[^a-zA-Z0-9_]*", "g");
	var sTest = "";
	if (stream.hasChildNodes() == true) {
		//
		for (i=0; i<stream.childNodes.length; i++) {
		
			node_name = stream.childNodes[i].nodeName;
			node_name = node_name.replace(re, '');
			// reset / create subnode
			if (typeof return_value[node_name] != 'object') {
				return_value[node_name] = new Array();
			}
			
			if (stream.childNodes[i].nodeType == 1) {
				var tmp_node = create_object_structure(stream.childNodes[i]);
				for (attrib=0; attrib<stream.childNodes[i].attributes.length; attrib++) {
					tmp_node.set_attribute(stream.childNodes[i].attributes[attrib].nodeName, stream.childNodes[i].attributes[attrib].nodeValue);
				}
				return_value[node_name].push(tmp_node);
				sTest = "-node_type=element";
				sTest += "--node_name="+node_name;
			} else if (stream.childNodes[i].nodeType == 3) {
				sTest = "-node_type=text";
				val = decode(String(stream.firstChild.data));
				return_value.data = val;
				if (val !== "" && val !== " " && val !== "  " && val !== " " && val !== "   ") {
					sTest += "--node_value=--"+val+"--l="+val.length;
					sTest += "--node_name="+node_name;
				}
			} else if (stream.childNodes[i].nodeType == 4) {
				sTest = "-node_type=cdata";
				return_value.data = decode(String(stream.firstChild.data));
				if (return_value.data !== "") {
					sTest += "--node_value="+return_value.data;
					sTest += "--node_name="+node_name;
				}
			}
			//alert("sTest:"+sTest);      
			sTextTotal += "  </br>-----"+sTest+"";
		
		}
	}
	return return_value;
};
function cpaint_result_object() {
	this.id = 0;
	this.data = '';
	var __attributes = new Array();

	this.find_item_by_id = function() {
		var return_value = null;
		var type = arguments[0];
		var id = arguments[1];
		var i = 0;
		if (this[type]) {
			for (i=0; i<this[type].length; i++) {
				if (this[type][i].get_attribute('id') == id) {
					return_value = this[type][i];
					break;
				}
			}
		}
		return return_value;
	};

	this.get_attribute = function() {
		var return_value = null;
		var id = arguments[0];
		if (typeof __attributes[id] != 'undefined') {
			return_value = __attributes[id];
		}
		return return_value;
	};

	this.set_attribute = function() {
		__attributes[arguments[0]] = arguments[1];
	};
}
// =========================================================================
var decode = function (rawtext) {
	var plaintext = '';
	var i = 0;
	var c1 = 0;
	var c2 = 0;
	var c3 = 0;
	var u = 0;
	var t = 0;
	// remove special JavaScript encoded non-printable characters
	while (i<rawtext.length) {
		if (rawtext.charAt(i) == '\\' && rawtext.charAt(i+1) == 'u') {
			u = 0;
			for (j=2; j<6; j += 1) {
				t = parseInt(rawtext.charAt(i+j), 16);
				if (!isFinite(t)) {
					break;
				}
				u = u*16+t;
			}
			plaintext += String.fromCharCode(u);
			i += 6;
		} else {
			plaintext += rawtext.charAt(i);
			i++;
		}
	}
	// convert numeric data to number type
	var re = new RegExp("^\\s+$", "g");
	if (plaintext != '' && plaintext.search(re) == -1 && !isNaN(plaintext) && isFinite(plaintext)) {
		plaintext = Number(plaintext);
	}
	return plaintext;
};
