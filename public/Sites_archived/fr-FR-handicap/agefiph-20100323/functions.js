//submit for newletter
function dosubmit_mail()
{
	var frm = document.getElementById('frmperso_mail');
	var arrControl= new Array(
		new Array("!isEmail( trim(frm.email.value) )", "Merci de renseigner votre email !", "email")
		);
	if(checkForm(frm,arrControl))
	{
		frm.submit();
		return true;
	}
	return false;
}

// JavaScript Document
function createEditor(id,value,height)
{
	if (document.all){
		var idGenerator = new IDGenerator(id);
		if (isNaN(height)) height=200;
		var editor = new Editor(idGenerator,value,height);
		editor.Instantiate();
	}else{
		document.write("Sorry! your browser does not support this action. Please use Internet Explorer.")
	}
	return true;
}
function get_region()
{
	var no = new Image();
	var fn = new Image();	
	var ne = new Image();
	var se = new Image();
	var so = new Image();
	no.src = "img/imageregion_nordouest.jpg";
	se.src = "img/imageregion_sudest.jpg";
	ne.src = "img/imageregion_nordest.jpg";
	fn.src = "img/imageregion_nord.jpg";
	so.src = "img/imageregion_sudouest.jpg";
	var frm = document.getElementById('frmperso');
	var imsrc=document.getElementById('imreg');
	//alert (frm.region.options[frm.region.selectedIndex].value) ;
	frm.region.value=frm.region.options[frm.region.selectedIndex].value;
	//alert(frm.region.value);
	imselected = frm.region.value;
	if (imselected == "Nord-ouest")	{
		imsrc.src=no.src;		
	} else if (imselected == "France-nord") {
		imsrc.src=fn.src;
	} else if (imselected == "Nord-est") {
		imsrc.src=ne.src;
	} else if (imselected == "Sud-est") {
		imsrc.src=se.src;
	} else if (imselected == "Sud-ouest") {
		imsrc.src=so.src;
	}		
}

function popWin(url, width, height, windowName, scrollbar, content){
	var x = (screen.width) ? (screen.width-width) / 2 : 100;
	var y = (screen.height) ? (screen.height-height) / 2 : 100;

	var features = "width=" + width + "px, height=" + height + "px, top=" + y + "px, left=" + x + "px";
		features += (scrollbar) ? ", scrollbars=yes" : "";
		features += ", resizable=yes";
		
	if (!windowName){
		windowName = "untitled";
	}
	var popWindow = window.open(url, windowName, features);

	if (popWin.arguments.length == 6 && content != ""){
		popWindow.document.write(content);
	}
	if (document.layers){
		window.moveTo(x, y);
	}
	popWindow.focus();
}

function resizePopWin(nest_object){
	var body_margin = 32;
	var scroll_bar = 20;
	var title_bar = 25;
	var form_space = 12;
	var scale = 1.3;	//	scale = height/width;
	var additional_range = 100;
	
	var window_width = nest_object.clientWidth + body_margin + scroll_bar;
	var content_height = nest_object.clientHeight + body_margin + form_space + title_bar;
	var window_height = Math.min(Math.round(window_width * scale), Math.min(content_height, screen.height));
	
	if (window_height == Math.round(window_width * scale) && Math.abs(window_height - content_height) < additional_range && content_height < screen.height - additional_range){
		window_height = content_height;
	}
	
	if (window_height < content_height){
		document.body.scroll = "auto";
	}else{
		document.body.scroll = "no";
	}

	window.resizeTo(window_width, window_height);
	
	var x = (screen.width) ? (screen.width - window_width) / 2 : 100;
	var y = (screen.height) ? (screen.height - window_height) / 2 : 100;
	
	window.moveTo(x, y);
}
//
function view_service_history(sli,service_id)
{
	url="view_service_logs.php?sli="+sli
	popWin(url,400,200,"service_log",true);
}

//---------------------------------------------------------------------------------
function open_calendar(frm_name,frm_field,func){
	if(!func)
		func=""

	var url="style_js/calendar/calendar.htm#"+frm_name+"="+frm_field +"="+func
	popWin(url,230,220,"calendar",0,0)
		//popWin(url,width,height,windowName,scrollbar,content)

}
//---------------------------------------------------------------------------------
function edit_hotel(service_id,segment_num)
{
	segment_num=1
	url="select_hotel.php?service_id="+service_id+"&segment_num=" + segment_num
	popWin(url,400,200,"service_log",true);

}
/**
//Lan.tt
 * Type = 0: dd mm yyyy
 *		  1: mm dd yyyy
 *		  2: dd mmmm yyyy	
 */
function isDate(date,type){
	if (!(type==0||type==1||type==2)) {alert("Please specify date type in check form function!"); return false;}
	var day, month, year;
	var dateText=new Array("null","january","february","march","april","may","june","july","august","september","october","november","december");
	var thisdate=trim(date);

	//validate basic format
	var dateType=new Array();
	dateType[0]=/^\d{1,2}(\-|\/|\.|\s)\d{1,2}(\-|\/|\.|\s)\d{1,4}$/;	//date format dd mm yyyy
	dateType[1]=/^\d{1,2}(\-|\/|\.|\s)\d{1,2}(\-|\/|\.|\s)\d{1,4}$/;	//date format mm dd yyyy
	dateType[2]=/^\d{1,2}(\-|\/|\.|\s)\w{3,9}(\-|\/|\.|\s)\d{1,4}$/;	//date format dd mmmm yyyy
	if (thisdate.search(dateType[type])==-1) return false;
	
	var seperator=(thisdate.indexOf("-")!=-1)?"-":(thisdate.indexOf("/")!=-1)?"/":(thisdate.indexOf(".")!=-1)?".":(thisdate.indexOf(" ")!=-1)?" ":"";
	if (seperator=="") return false;	//no seperator

	var dateParam=thisdate.split(seperator);
	if (dateParam.length!=3) return false;	//use difference seperator
	
	//day month year from date string
	day=dateParam[0]; 
	year=dateParam[2];

	switch (type){
		case 0:
			month=dateParam[1];
		break;
		case 1:
			day=dateParam[1];
			month=dateParam[0];
		break;
		case 2:
			month=inArray(dateParam[1].toLowerCase(),dateText);
		break;
	}
//	alert('2323234324'+ day + "  " + month + "   " +year);	
	if (!month) return false;
	if (!senseDate(day,month,year)) return false;	
	return true;
}

//Lan.tt
function inArray(itemToCheck,targetArray){
	var i=-1; var result=false;
	if (!isArray(targetArray)) return false;
	while ((i<targetArray.length-1)&&(!result)){
		i++;
		result=(targetArray[i].indexOf(itemToCheck)!=-1)?true:false;
	}
	if (result) return i;
	return false;
}

//Lan.tt
function isArray(obj){
	if (obj.constructor.toString().indexOf("Array")==-1)
		return false;
	return true;
}
//Lan.tt
function senseDate(day,month,year){ 
//alert(day + "  " + month + "   " +year);
	if ((day<1)||(day>31)) return false;
	if ((month<1)||(month>12)) return false;
	if (year<1900||year>2020) return false;
	if ((month==2)&&(day>29)) return false;
	if (((month==4)||(month==6)||(month==9)||(month==11))&&(day>30)) return false;
	if ((month==2)&&(day==29)){
		var div4=year%4;
        var div100=year%100;
        var div400=year%400;
		if (div4!=0) return false;
		if ((div100==0)&&(div400!=0)) return false;
	}
	return true;
}

//Lan.tt
function compareDate(date1,date2,type){
	var dateInput=(type==1)?1:0;
	var dateType=new Array();
	dateType[0]=/^\d{1,2}(\-|\/|\.|\s)\d{1,2}(\-|\/|\.|\s)\d+$/;	//date format dd mm yyyy
	dateType[1]=/^\d{1,2}(\-|\/|\.|\s)\d{1,2}(\-|\/|\.|\s)\d+$/;	//date format mm dd yyyy

	if (date1.search(dateType[dateInput])==-1||date2.search(dateType[dateInput])==-1) return 0;
	
	var seperator1=(date1.indexOf("-")!=-1)?"-":(date1.indexOf("/")!=-1)?"/":(date1.indexOf(".")!=-1)?".":(date1.indexOf(" ")!=-1)?" ":"";
	var seperator2=(date2.indexOf("-")!=-1)?"-":(date2.indexOf("/")!=-1)?"/":(date2.indexOf(".")!=-1)?".":(date2.indexOf(" ")!=-1)?" ":"";
	
	if (seperator1==""||seperator2=="") return 0;
	
	var dateArr1=date1.split(seperator1);
	var dateArr2=date2.split(seperator2);
	
	if (dateArr1.length!=3||dateArr2.length!=3) return 0;
	
	if (dateInput==1){
		var year1 = dateArr1[2];
		var year2 = dateArr2[2];
		var month1 = dateArr1[0];
		var month2 = dateArr2[0];
		var day1 = dateArr1[1];
		var day2 = dateArr2[1];
	}else{
		var year1 = dateArr1[2];
		var year2 = dateArr2[2];
		var month1 = dateArr1[1];
		var month2 = dateArr2[1];
		var day1 = dateArr1[0];
		var day2 = dateArr2[0];
	}

	if (compareNumber(year1, year2) != 3) return compareNumber(year1, year2);
	if (compareNumber(month1, month2) != 3) return compareNumber(month1, month2);
	if (compareNumber(day1, day2) != 3) return compareNumber(day1, day2);
	return 3;
}

function compareNumber(num1, num2){
	if (parseFloat(num1)>parseFloat(num2)){
		return 1;
	}else if (parseFloat(num1)<parseFloat(num2)){
		return 2;
	}else{
		return 3;
	}
}
function comparepass(chr1, chr2){
	if(trim(chr1)==trim(chr2)){
		return 0;
	}else if (trim(chr1)!=trim(chr2)){
		return 1;
	}
}

function chkChar (fieldObj, maxchar) {
	if (fieldObj.length <= maxchar) 
		return true;	
	return false;
}

//Lan.tt
function checkImage(image)
{
	var reg = /\b(.gif|.jpg|.ico|.bmp|.jpeg)$/i;
	return reg.test(image);
}
//Lan.tt
function checkDoc(docfile)
{
	var reg = /\b(.doc|.pdf)$/i;
	return reg.test(docfile);
}
//Lan.tt
function trim(str) {
	return str.replace(/^\s+/g, '').replace(/\s+$/g, '');
}
//Lan.tt
function isBlank(str) {
	if( str == "" ) 
		return true ;
	return false ;
}
//Lan.tt
function isEmail(s){
	if (s.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]{2,4}$/) != -1)
		return true ;
	return false ;
}
//Lan.tt
function checkForm(frm, arrControl) {
	var nNumControl = arrControl.length ;
	var i = 0;
	for(i = 0; i < nNumControl; i++) 
	{
		bValid = trim(arrControl[i][0] ) ;
		if(eval( bValid )){ //== 
			alert( arrControl[i][1] ) ;
			if(arrControl[i][2] != "")
			{
				eval( "frm."  + arrControl[i][2] ).focus() ;
				eval( "frm."  + arrControl[i][2] ).select();
			}
			return false ; //== Error
		}
	}
	return true ; //== OK
}

function GetCountry_City(action, adhoc)
{
	if (action == "new")
	{		
		document.form1.country.value = opener.document.form1.country.options[opener.document.form1.country.selectedIndex].value;
		document.form1.city.value = opener.document.form1.city.options[opener.document.form1.city.selectedIndex].value;
		if (adhoc == true)
		{
			document.form1.country_id.value = opener.document.form1.country.options[opener.document.form1.country.selectedIndex].value;
			document.form1.city_id.value = opener.document.form1.city.options[opener.document.form1.city.selectedIndex].value;
		}
	}	
}

function isPositive(number)
{
	if (isNaN(number) || number<=0) return false;
	return true;
}

function isPositive1(number)
{
	if (isNaN(number) || number<0) return false;
	return true;
}

//check time hh:mm
function checkTime(value)
{
		var re = /\b(0?[0-9]|1[0-9]|2[0-3]):([0-5][0-9])/
		if (re.test(value)) return true
		else return false
}

function Cancel_Click(refresh)
{
	if (refresh)
	{
		opener.window.location = opener.window.location;
	}
	window.close();
}

 function Pop_Go(){
        window.print();
        return
 }

function openProfile(value)
{
	popWin('profile.php?id='+value, 720, 550, 'Edit_Profile',true);
}
function formatCurrency(num)
 {
	num = num.toString().replace(/\$|\,/g,'');
	if(isNaN(num))
		num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
		cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+
	num.substring(num.length-(4*i+3));
	//return (((sign)?'':'-') + '$' + num + '.' + cents);
	return (((sign)?'':'-') +  num + '.' + cents);
}

function isPhone(s)
{
	if (s.search(/^((\+?\d*)?\([1-9]\))?\d*$/) != -1) return true;
	return false;
}
function frmagentalert(){
	var f=document.frmalert;
	var fs=document.consulter;
	if(f.email.value==""){
		alert ("Format d'email non valide !");
		f.email.focus();
		return false;
	}
	if(!isEmail(f.email.value)){
		alert ("Format d'email non valide !");
		f.email.focus();
		f.email.select();
		return false;
	}
	//alert(fs.fonction.options[fs.fonction.selectedIndex].value);
	f.search_condition.value=fs.fonction.options[fs.fonction.selectedIndex].value+","+fs.secteur.options[fs.secteur.selectedIndex].value+","+fs.region.options[fs.region.selectedIndex].value+","+fs.experience.options[fs.experience.selectedIndex].value;	
	return true;
}

//Lan.tt
function isUsername(s){
	if (s.search(/^[A-Za-z0-9\_\.]+$/) != -1) return true;
	return false;
}

window.addEvent('domready', function(){
									 
	if($("conventionPlaylist")){		
				 
		$("playlist1").addEvent("click", function(){										  
			$("conventionPlaylist").innerHTML = '<embed src="http://www.youtube.com/cp/vjVQa1PpcFP6mlsZV6mNPW6onJQCHH8nKGoXKL5Rycw=" type="application/x-shockwave-flash" width="416" height="337"></embed>';
		});
		
		$("playlist2").addEvent("click", function(){
			
			$("conventionPlaylist").innerHTML = '<embed src="http://www.youtube.com/cp/vjVQa1PpcFP6mlsZV6mNPdk-NHHJOgkJqyL2pQb9pFc=" type="application/x-shockwave-flash" width="416" height="337"></embed>';
		});
		
		$("playlist3").addEvent("click", function(){										  
			$("conventionPlaylist").innerHTML = '<embed src="http://www.youtube.com/cp/vjVQa1PpcFP6mlsZV6mNPYwra2rnb-mk9Nt4Xk453-M=" type="application/x-shockwave-flash" width="416" height="337"></embed>';
		});
		
		$("playlist4").addEvent("click", function(){										  
			$("conventionPlaylist").innerHTML = '<embed src="http://www.youtube.com/cp/vjVQa1PpcFP6mlsZV6mNPY3ZWrRK-NpfpFFh6hstMgc=" type="application/x-shockwave-flash" width="416" height="337"></embed>';
		});
		
		$("playlist5").addEvent("click", function(){										  
			$("conventionPlaylist").innerHTML = '<embed src="http://www.youtube.com/cp/vjVQa1PpcFP6mlsZV6mNPeRiYzC7_EHh-NaM6qCMEEs=" type="application/x-shockwave-flash" width="416" height="337"></embed>';
		});
		
		$("playlist6").addEvent("click", function(){										  
			$("conventionPlaylist").innerHTML = '<embed src="http://www.youtube.com/cp/vjVQa1PpcFP6mlsZV6mNPYKV3RCDds5JOzaFU9pfhRo=" type="application/x-shockwave-flash" width="416" height="337"></embed>';
		});
		
		$("playlist7").addEvent("click", function(){										  
			$("conventionPlaylist").innerHTML = '<embed src="http://www.youtube.com/cp/vjVQa1PpcFP6mlsZV6mNPfGJtE6PtdRTQ26zXi1BjTk=" type="application/x-shockwave-flash" width="416" height="337"></embed>';
		});
		
		$("playlist8").addEvent("click", function(){										  
			$("conventionPlaylist").innerHTML = '<embed src="http://www.youtube.com/cp/vjVQa1PpcFP6mlsZV6mNPesrk784QU74e9Jk_KEUqfI=" type="application/x-shockwave-flash" width="416" height="337"></embed>';
		});
	}
});

