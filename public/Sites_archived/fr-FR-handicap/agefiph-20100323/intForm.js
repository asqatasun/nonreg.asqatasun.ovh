﻿function isPhone(s) {
	var re = new RegExp("^[ .0-9]{3,}$");
	return (s.search(re) != -1);
}

function isEmail(s){
	var re = new RegExp("^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]{2,4}$");
	return (s.search(re) != -1);
}

function checkFormFormulaire2(elementId) {
	var result = $('Nom1').value || $('Prnom1').value || ($('Organisme').value != $('Organisme1').value) || $('Fonction1').value || $('phone1').value || $('Adresse1').value || $('CodePostal1').value || $('Ville1').value || $('AdresseEmail1').value;
	if (result != "") {
		return $(elementId).value != "";
	}
	
	return true;
}

function initFormFormulaire() {
	if (!$("formulaire")) return;
	//
	var alertXML = new fLoadXML("xml/formulaire.xml", true, function(){
		var width = 235;
		if (window.ie) {
			width = 245;
		}
		
		new validerAlert(
			"formulaireAlert", 
			"formulaire", 
			"btn_formgif", 
			this.aXml['form'][0].field, 
			{
				font: "11px/11px Arial",
				txtColor: "#000",
				appTxtColor: "#000",
				background: "#fff url(images/_alert_bar.gif) 0 0 no-repeat",
				closeImage: "images/_alert_close.gif",
				border: "1px solid #ccc",
				width: width + "px",
				textAlign: "left"
			}
		);
	});
}
	
function initForm(){
	if (!$("form1")) return;
	//
	var alertXML = new fLoadXML("xml/form.xml", true, function(){
		document.frmContactValider = new validerAlert(
			"frmAlertBox", 
			"form1", 
			"btnOK", 
			this.aXml['form'][0].field, 
			{
				font: "11px/11px Arial",
				txtColor: "#000",
				appTxtColor: "#000",
				background: "#fff url(images/_alert_bar.gif) 0 0 no-repeat",
				closeImage: "images/_alert_close.gif",
				border: "1px solid #ccc",
				width: "183px"
			}
		);
	});
	
}
///////////////////////
function initFormEvenement(){
	if (!$("coupon")) return;
	//
	var alertXML = new fLoadXML("xml/evenement.xml", true, function(){
		document.frmContactValider = new validerAlert(
			"frmFormEvenement", 
			"coupon", 
			"btnValider", 
			this.aXml['form'][0].field, 
			{
				font: "11px/11px Arial",
				txtColor: "#000",
				appTxtColor: "#000",
				background: "#fff url(images/_alert_bar.gif) 0 0 no-repeat",
				closeImage: "images/_alert_close.gif",
				border: "1px solid #ccc",
				width: "180px"
			}
		);
	});
	
}
//////////////////////
window.addEvent("load", function(){
	initForm();
	initFormEvenement();
	initFormFormulaire(); 
});