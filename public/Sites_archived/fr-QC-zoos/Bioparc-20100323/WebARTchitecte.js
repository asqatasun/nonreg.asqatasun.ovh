//--WEBARTCHITECTE VERSION 2006 LITE ---//
//--- Dernière mise à jour juin 2006 ----//
//-------------- par JT -----------------//
function openDetailsWindow(theURL) { //v2.0
 var theWindow = window.open(theURL,'popup','toolbar=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,width=500,height=300');
theWindow.focus();
}

function openJPGWindow(theURL,w,h) { //v2.0
  var width=w, height=h;
var left = (screen.width/2) - width/2;
var top = (screen.height/2) - height/2;
var styleStr = 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbar=no,resizable=yes,width='+width+',height='+height+',left='+left+',top='+top+',screenX='+left+',screenY='+top;
var ImageWindow = window.open("","ImageWindow", styleStr);
var head = '<html><head><title>Image</title>\n';
head = head+'<style type="text/css">body {margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px;}</style></head>\n';
var bbody ='<body onBlur="self.close();">\n<img src="'+theURL+'" width="'+w+'" height="'+h+'" alt="Image" name="openJPGWindow" id="openJPGWindow">\n</body></html>';
//alert(ImageWindow.document.getElementsByTagName("html"));
if (ImageWindow.document.getElementById("openJPGWindow")){
$currentJPG=ImageWindow.document.getElementById("openJPGWindow");
$currentJPG.src=theURL;
$currentJPG.width=w;
$currentJPG.height=h;
ImageWindow.resizeTo(w,h);
}else{
ImageWindow.document.write(head+bbody);
}
ImageWindow.focus();
}

function openEditWindow(theURL) { //v2.0
var theWindow =window.open(theURL,'popupedit','toolbar=no,status=yes,menubar=no,resizable=yes,scrollbars=yes,width=740,height=560');
theWindow.focus();
}
function openWebArtchitecteWindow(theURL) { //v2.0
var theWindow =window.open(theURL,'popup','toolbar=no,status=yes,menubar=no,resizable=yes,scrollbars=yes,width=400,height=300');
theWindow.focus();
}
function openOptionsWindow(theURL) { //v2.0
 var theWindow = window.open(theURL,'popup','toolbar=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,width=375,height=300');
theWindow.focus();
}
function MM_openBrWindow(theURL,winName,features) { //v2.0
  var theWindow =window.open(theURL,winName,features);
theWindow.focus();
}
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
function delArticle(url){
 if (confirm("Voulez-vous vraiment supprimer cet article?\nCet article peut être présent dans une autre page...")) {
           document.location=url;
  }
}
function OpenCartes(URLtoOpen){
var w1 = screen.width/2-270;
var h1 = screen.height/2-200;
var w2 = 540;
var h2 = 405;
window.open(URLtoOpen,'carte','status=yes,width='+w2+',height='+h2+',top='+h1+',left='+w1+'');
}
function MM_showHideLayers() { //v6.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function JT_ShowHideRow(Row) {
divRow=MM_findObj(Row+'_Child');
	if (divRow.style.display != "none"){
		MM_showHideLayers(Row+'_Child','','hide');
		divRow.style.display = "none";
	}else{
		MM_showHideLayers(Row+'_Child','','show');
		divRow.style.display = "block";
	}
	
}


function JT_IniRow() {
if (loadPage) {
MenuTimer=setTimeout("JT_IniRow()", 500);
}else{
tempDiv = document.getElementsByTagName('DIV');
	for (i=0; i<tempDiv.length; i++) {
		if (tempDiv[i].className == "ListeItemChild_auto") tempDiv[i].style.display = "none";
		if (tempDiv[i].className == "ListeItemChildFAQ_auto") tempDiv[i].style.display = "none";
	}
}

}

function JT_ShowHideEvent(Row) {
ShowDivID=MM_findObj('CurrentEvent');
	if (MM_findObj(Row)){
		divRow=MM_findObj(Row);
		MM_showHideLayers('CurrentEvent','','show');
		ShowDivID.style.display = "block";
		ShowDivID.innerHTML=divRow.innerHTML;
	}else{
		MM_showHideLayers('CurrentEvent','','hide');
		ShowDivID.style.display = "none";
		ShowDivID.innerHTML="";
	}
}

function positionInfoLayer(MenuID){
	theimgName='img_'+MenuID;
    imgMenu=MM_findObj(theimgName);
	menuName=MM_findObj(MenuID);
	var pos =getXYcoord(imgMenu);
	menuName.style.top=pos[1];
	varLeft =parseInt(pos[0]-menuName.offsetWidth);
	if (varLeft<150) varLeft=parseInt(pos[0]+imgMenu.offsetWidth);
	menuName.style.left=varLeft;
}

function JT_ShowHideInfoLayer(Name, Flag) {
positionInfoLayer(Name);
MM_showHideLayers(Name,'',Flag);
}
function delImage(url){
 if (confirm("Voulez-vous vraiment supprimer cette image?")) {
           document.location=url;
  }
}

function bandeauFlash(theflash,wi,he){
	document.write('<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="'+wi+'" height="'+he+'">');
    document.write('<param name="movie" value="'+theflash+'">');
    document.write('<param name="quality" value="high">');
	document.write('<param name="wmode" value="transparent">');
    document.write('<embed src="'+theflash+'" wmode="transparent" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="'+wi+'" height="'+he+'"></embed>');	   
	document.write(' </object>');
}
function SetIniValueForm(Name, iniVal) {
fieldName=MM_findObj(Name);
if (fieldName) fieldName.value=iniVal;
}