var monInterval=null;
var haut=-60;
var bas=0;

//positionne l'objet
function bouge_recherche(direction){
	var pos=parseInt(document.getElementById('recherche').offsetTop);
	var vitesse=5;
	
	document.getElementById('recherche').style.top=pos+(direction * vitesse) +"px";
	
	if (direction >0 && pos>=bas){
		window.clearInterval(monInterval);
		monInterval=null;
		document.getElementById('recherche').style.top=bas +"px";	
	}
	else if (direction <0 && pos<=haut){
		window.clearInterval(monInterval);
		monInterval=null;
		document.getElementById('recherche').style.top=haut +"px";	
	}
}


//part l'interval
function scroll_recherche(direction){
		if (monInterval !=null){
			window.clearInterval(monInterval);
			monInterval=null;
			}
		
		monInterval=window.setInterval('bouge_recherche('+direction+')',1);
}

// affiche/cache résultats et change le plus
function show_hide(obj){
	if (document.getElementById('res_' + obj).style.display == 'block'){
	document.getElementById('res_' + obj).style.display	=	'none';	
	document.getElementById('btn_' + obj).innerHTML		=	'( + ) ';	
	}
	else{
	document.getElementById('res_' + obj).style.display	=	'block';
		document.getElementById('btn_' + obj).innerHTML		=	'( - ) ';
	}
}

//validation de la recherche
function validRech(lng,action){
	var nobug=true;
	//document.form_rech.r1.checked !=true &&  
	//document.form_rech.s1.checked !=true && 
	
	if (lng=='fr'){
		valeur= (trim(document.form_rech.recherche.value));
	
		if (document.form_rech.r2.checked !=true && document.form_rech.r3.checked !=true)
		nobug=false;
	}
	else{
		valeur= (trim(document.form_rech.search.value));

		if ( document.form_rech.s2.checked !=true && document.form_rech.s3.checked !=true)
		nobug=false;

	}

	if (valeur=='')
	nobug=false;
	
	if (action==true)
	return nobug;
	else{
	if (nobug==true)
	document.form_rech.submit();
	}

}