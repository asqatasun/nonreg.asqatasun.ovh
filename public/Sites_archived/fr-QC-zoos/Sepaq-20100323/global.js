var dot=null;
var timerLogMessage = null;
var loginStatus = false;
  //Coordonees x,y de chaque parc
    var Parcs = {
	//parcs quebec
	AIG:[12,66],
	PAN:[274,48],
	BIC:[177,81],
	FRO:[134,143],
	GAS:[215,64],
	GRJ:[139,98],
	HGO:[148,91],
	BON:[253,70],
	BOU:[94,145],
	JAC:[131,107],
	MIG:[216,83],
	MME:[135,153],
	MOR:[116,156],
	MSB:[98,150],
	MOT:[81,124],
	MVA:[143,71],
	OKA:[84,150],
	PIN:[233,117],
	PLA:[66,146],
	PTA:[123,69],
	SAG:[149,82],
	SSL:[164,81],
	YAM:[107,153],
	//reserves fauniques
	ASH:[98,66],
	ASN:[66,29],
	CHC:[223,61],
	AMW:[82,8],
	LAU:[131,93],
	LVY:[38,97],
	MAS:[97,118],
	MAT:[202,66],
	PAL:[62,135],
	SPC:[193,18],
	POD:[241,74],
	POR:[112,103],
	RIM:[186,85],
	ROM:[77,115],
	STM:[98,109],
	//centres touristiques
	AMC:[216,65],
	PRE:[249,62],
	BDP:[248,74],
	VOL:[111,138],
	KEN:[133,78],
	SIM:[68,139],
	PCM:[139,111],
	DUC:[128,118],
	VAL:[118,78]
	}
	
var sepaqNetwork = jQuery.cookie("sepaqnetwork");
if ((sepaqNetwork != null) && (sepaqNetwork != undefined) && (sepaqNetwork != "")) {
    sepaqNetwork = sepaqNetwork.toLowerCase();
}
	
jQuery(document).ready(function() {
  checkLoginStatus();
  checkLogoutStatus();
  
  jQuery("#modal").jqm({ajax:"@href", trigger: "a.modal"});
  jQuery("#modal_friend").jqm({ajax:"@href", trigger: "a.modal_friend"});
  
	sepaqNetwork = checkCookieStatus(sepaqNetwork);
	//var sepaqNetwork = sepaqNetwork;
    //jQuery("body").append(jQuery(document.createElement("span")).attr("id", "AIG-coords").data("coords", "22,50"))

    if (sepaqNetwork == "pq") {
        jQuery("#mnu_parcsquebec").css("background-position", "-109px -33px");
    } else if (sepaqNetwork == "rf") {
        jQuery("#mnu_reserves").css("background-position", "-249px -33px");
    } else if (sepaqNetwork == "ct") {
        jQuery("#mnu_centres").css("background-position", "-436px -33px");
    }

    var isParcsMenuHidden = false;
    var isReservesMenuHidden = false;
    var isCentresMenuHidden = false;
    var isOrgMenuHidden = false;
    var isAccountMenuHidden = false;

	dot = jQuery('.dot-over')
	//Attache les evenements de mouseover et mouseout a tous les elements qui ont la classe "trigger"
	jQuery('.trigger').bind('mouseover',{show:true},handleArrow);
	jQuery('.trigger').bind('mouseout',{show:false},handleArrow);
    
    jQuery("#mnu_parcsquebec, #menu_dropdown_parcsquebec").hover(function() {
        if (isParcsMenuHidden) {
            clearTimeout(isParcsMenuHidden);
        }
        jQuery("#mnu_parcsquebec").css("background-position", "-109px -33px");
        jQuery("#menu_dropdown_reserves, #menu_dropdown_centres").hide();
        jQuery("#menu_dropdown_parcsquebec").show();        
    }, function() {
        isParcsMenuHidden = setTimeout(function() {
            if (sepaqNetwork != "pq") {
                jQuery("#mnu_parcsquebec").css("background-position", "-109px 0");
            }
            jQuery("#menu_dropdown_parcsquebec").hide();
        }, 0);
    });
    
    jQuery("#mnu_reserves, #menu_dropdown_reserves").hover(function() {
        if (isReservesMenuHidden) {
            clearTimeout(isReservesMenuHidden);
        }
        jQuery("#mnu_reserves").css("background-position", "-249px -33px");
        jQuery("#menu_dropdown_parcsquebec, #menu_dropdown_centres").hide();
        jQuery("#menu_dropdown_reserves").show();
    }, function() {
        isReservesMenuHidden = setTimeout(function() {
            if (sepaqNetwork != "rf") {
                jQuery("#mnu_reserves").css("background-position", "-249px 0");
            }
            jQuery("#menu_dropdown_reserves").hide();
        }, 0);
    });
    
    
    jQuery("#mnu_centres, #menu_dropdown_centres").hover(function() {
        if (isCentresMenuHidden) {
            clearTimeout(isCentresMenuHidden);
        }
        jQuery("#mnu_centres").css("background-position", "-436px -33px");
        jQuery("#menu_dropdown_parcsquebec, #menu_dropdown_reserves").hide();
        jQuery("#menu_dropdown_centres").show();
    }, function() {
        isCentresMenuHidden = setTimeout(function() {
            if (sepaqNetwork != "ct") {
                jQuery("#mnu_centres").css("background-position", "-436px 0");
            }
            jQuery("#menu_dropdown_centres").hide();
        }, 0);
    });
    
    jQuery("#mnu_organisation, #menu_dropdown_organisation").hover(function() {
        if (isOrgMenuHidden) {
            clearTimeout(isOrgMenuHidden);
        }
        jQuery("#mnu_organisation").css("background-color", "#9c9a9b").css("color","#ffffff");
        jQuery("#menu_dropdown_organisation").show();
    }, function() {
        isOrgMenuHidden = setTimeout(function() {
            jQuery("#menu_dropdown_organisation").hide();
	    jQuery("#mnu_organisation").css("background-color", "transparent").css("color","#5D605D");
        }, 0);
    });
	
	jQuery('body').click(function(){
		jQuery('#menu_dropdown_account').hide();
		jQuery('#mnu_account').css({'background-color':'transparent','color':'#0070AF'})
	})

    jQuery("#mnu_account").click(function(event) {
    	event.stopPropagation();
        if (isAccountMenuHidden) {
            clearTimeout(isAccountMenuHidden);
        }
        if(jQuery('#menu_dropdown_account').css('display')=='none') {
        	jQuery("#mnu_account").css("background-color", "#9c9a9b").css("color","#ffffff");
	        jQuery("#menu_dropdown_account").show();
	        jQuery("#menu_dropdown_account #emailAddress").focus();
        }
        else {
        	jQuery('#menu_dropdown_account').hide();
			jQuery('#mnu_account').css({'background-color':'transparent','color':'#0070AF'})
        }
    });
    
    jQuery('#menu_dropdown_account').click(function(event) {
    	event.stopPropagation();
    });
    
    jQuery("#mnu_aquarium").hover(function() {
        jQuery("#mnu_aquarium").css("background-position", "-628px -33px");
    }, function() {
        jQuery("#mnu_aquarium").css("background-position", "-628px 0");
    });

    jQuery("#mnu_anticosti").hover(function() {
        jQuery("#mnu_anticosti").css("background-position", "-816px -33px");
    }, function() {
        jQuery("#mnu_anticosti").css("background-position", "-816px 0");
    });
    
    //Filtres actif dans activites ou hebergement
    if(jQuery('#radioNetwork #filtre_reseaux')) {
    	jQuery('#radioNetwork #filtre_reseaux span input').click(handleActiveFilter)
   		handleActiveFilter();
    }
});

//Affiche ou masque la fleche, selon le parametre "show". Positionne la fleche a l'aide de l'objet "Parcs"
function handleArrow(e) {
	if(e.data.show) {
		dot.removeClass('specialCT');
		if(jQuery(this).hasClass('ct_map_item'))
		{
			dot.addClass('specialCT');
		}
		dot.show();
		dot.css({'left' : Parcs[e.target.rel][0]+'px', 'top' : Parcs[e.target.rel][1]+'px','display' : 'block'});
		if(jQuery(this).attr('class')=="trigger") {
			var curRel = jQuery(this).attr('rel');
			jQuery('.menu_dropdown:visible ul li a[rel="'+curRel+'"]').addClass('actif');
		}
	}
	else {
		dot.hide();
		jQuery('.menu_dropdown ul li a').removeClass('actif');
	}
}

function checkLoginStatus() {
	var url = window.location.href
	var login = url.indexOf('?login=1') != -1;
	var accountCreation = url.indexOf('?created=1') != -1;
	var titre = "";
	var message = "";
	if(loginStatus && login && jQuery('#customerAccountForm').length<1) {
		titre = loginText;
		message = loginMessageText;
		buildPopUp(titre,message);
		timerLogMessage = setTimeout(function(){
			jQuery('#logMessage').fadeOut("slow", function() {jQuery('#logMessage').remove();});
		},2500)
	}
	else if(loginStatus && accountCreation && jQuery('#customerAccountForm').length<1) {
		titre = loginText;
		message = accountCreationMessageText;
		buildPopUp(titre,message);
		timerLogMessage = setTimeout(function(){
			jQuery('#logMessage').fadeOut("slow", function() {jQuery('#logMessage').remove();});
		},2500)
	}	
}

function checkLogoutStatus() {
	if(loginStatus) {
		jQuery('#logoutLink').click(confirmLogout);
	}
}

function confirmLogout() {
	var titre = logoutText;
	var message = logoutMessageText;
	var href = this.href;
	
	message = message + '</div><hr/><div class="clear_both float_right"><a class="button vert" id="doLogout" href="'+href+'">'+logoutText+'</a><a class="button vert annuler" id="cancelLogout" href="#">'+cancelText+'</a><br />';
	
	buildPopUp(titre,message)
	return false
}

function buildPopUp(titre,message) {
	if(jQuery('#logMessage')) {
		jQuery('#logMessage').remove();
		if(timerLogMessage) {
			clearTimeout(timerLogMessage);
		}
	}
	jQuery('#container').append('<div style="position:absolute;" id="logMessage" class="pop_up"><div class="pop_up_body" style="width:auto;padding-bottom:15px;"><div id="pop_up_1" style="width:380px"><div style="background-position:0 -2px;height:38px;width:auto;" id="titre_popover"><span>'+titre+'</span></div><div style="margin-left:-10px;font-size:12px;">'+message+'</div></div><div class="clear_both"></div></div></div>')
		
	var unitWidth = document.getElementById('logMessage').offsetWidth;
	var unitX = document.getElementById('logMessage').offsetLeft;
	var containerWidth = document.getElementById('container').offsetWidth;
	var containerX = document.getElementById('container').offsetLeft;
	var unitMargin = (containerWidth-unitWidth)/2;
	var fixBrowser = containerX-unitX;
	unitMargin = unitMargin+fixBrowser;
	
	jQuery('#logMessage').css("margin-left",unitMargin+"px");
	jQuery('#logMessage').vCenter();
	
	var top = jQuery('#logMessage').css('top');
	top = top.replace('px',"");
	jQuery('#logMessage').css('top',top-100)
	
	if(jQuery('#doLogout')) {
		bindLogoutEvents();
	}
}

function bindLogoutEvents() {
	jQuery('#cancelLogout').click(function() {
		jQuery('#logMessage').fadeOut("slow", function() {jQuery('#logMessage').remove();});
		return false
	})
}

(function(jQuery){
  jQuery.fn.vCenter = function(options) {
    var pos = {
      sTop : function() {
        return window.pageYOffset || document.documentElement && document.documentElement.scrollTop ||	document.body.scrollTop;
      },
      wHeight : function() { 
        return window.innerHeight || document.documentElement && document.documentElement.clientHeight || document.body.clientHeight;
      }
    };
    return this.each(function(index) {
      if (index == 0) {
        var $this = jQuery(this);
        var elHeight = $this.height();
		    var elTop = pos.sTop() + (pos.wHeight() / 2) - (elHeight / 2);
        $this.css({
          position: 'absolute',
          marginTop: '0',
          top: elTop
        });


      }
    });
  };

})(jQuery);

function handleActiveFilter() {
	jQuery('#radioNetwork #filtre_reseaux span').removeClass('active');
	
	 var sepaqNetwork = jQuery.cookie("sepaqnetwork");
      if ((sepaqNetwork != null) && (sepaqNetwork != undefined) && (sepaqNetwork != "")) {
        sepaqNetwork = sepaqNetwork.toLowerCase();
        jQuery("#" + sepaqNetwork + "_radio").parent().addClass('active');
      } else if (sepaqNetwork == null) {
        jQuery("#sepaq_radio").parent().addClass('active');
      }

}

function checkCookieStatus(cookieNetwork) {
	var sepaqNetwork = cookieNetwork;
	var options = {path: '/', domain:'sepaq.com'};
	if(jQuery('body.pq').length && cookieNetwork != "pq") {
		jQuery.cookie('sepaqnetwork',"PQ", options);
		sepaqNetwork = "pq";
	}
	else if(jQuery('body.rf').length && cookieNetwork != "rf") {
		jQuery.cookie('sepaqnetwork',"RF", options);
		sepaqNetwork = "rf";
	}
	else if(jQuery('body.ct').length && cookieNetwork != "ct") {
		jQuery.cookie('sepaqnetwork',"CT", options);
		sepaqNetwork = "ct";
	}
	return sepaqNetwork;
}