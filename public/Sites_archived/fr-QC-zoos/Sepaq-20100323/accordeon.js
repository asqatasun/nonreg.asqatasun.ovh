$(document).ready(function() {

if($("#products").length) {
	$("#products ul.tabs li").click(changeTab);
	$("#products .tab-content ul.sub-tabs li").click(changeSubTab);
	
	$('#products .tab-content .item').click(showItem)
	$('#products .tab-content .item .desc').click(function(e) {e.stopPropagation();})
	
	$('a[href*=/#Item]').click(checkInternalLink);
	$('a[href*=/#tab]').click(checkInternalLink);
	$('a[href*=/#sub-tab]').click(checkInternalLink);
	
	openDefaults();
	anchorRedirect();
}
else if($('#zonevideo').length) {
	$('#zonevideo div.item').click(showItem);
	$('#zonevideo div.item .desc').click(function(e) {e.stopPropagation();})
	openFirstItem($('#zonevideo'));
}	

});

function changeTab() {
	$("#products ul.tabs li").removeClass("active"); //Remove any "active" class
	$("#products .tab-content").hide(); //Hide all tab content
	activateTab(this); //Activate clicked tab
	return false;
};

function changeSubTab(){
	$("#products .tab-content ul.sub-tabs li").removeClass("active"); //Remove any "active" class
	$("#products .tab-content .sub-tab-content").hide(); //Hide all tab content
	activateTab(this);//Activate clicked tab
	return false;
}

function activateTab(element){
	$(element).addClass('active'); //Add "active" class to selected tab
	var activeTab = $(element).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
	$(activeTab).show(); //Show the active ID content
	//window.location.hash=($(element).find('a').attr('href'))
}

function showItem(){
	if($(this).find('.desc:visible').length) {
		$(this).find('.desc').slideUp(100);
	}
	else {
		$('.item .desc').slideUp(100);
		$(this).find('.desc').slideDown(100);
	}
	return false
}

function anchorRedirect(){
	//If there is an anchor in the URL
	if(window.location.hash){
		var anchorId = window.location.hash//Get the anchor id from the URL
		handleAnchor(anchorId,true);
	}
	else {
		if((window.location.href).indexOf('/ct/duc')!= -1) {
			$('.sub-tabs li a[href="#sub-tab-reunions"]').trigger('click');
		}
                else if((window.location.href).indexOf('/ct/pcm')!= -1) {
			$('.sub-tabs li a[href="#sub-tab-reunions"]').trigger('click');
		}
                else if((window.location.href).indexOf('/pq/gma')!= -1) {
			$('.tabs li a[href="#tab-hostings"]').trigger('click');
		}

	}
}

function openDefaults(){
	$("#products .tab-content ul.sub-tabs li:first").trigger('click');
	jQuery.fx.off = true;
	$("#products .tab-content").each(function() {
		openFirstItem(this)
	})
	$("#products .tab-content .sub-tab-content").each(function() {
		$(this).find('.item:first .desc').show();
	})
	jQuery.fx.off = false;	
}

function openFirstItem(element){
	$(element).find('.item:first').trigger('click');
}

function checkInternalLink() {
	var longHref = $(this).attr('href');
	var anchorPos = longHref.indexOf('#');
	var anchorId = longHref.substring(anchorPos);
	if($('#products '+anchorId).length) {
		handleAnchor(anchorId,false);
		return false;
	}
}

function handleAnchor(anchorId,animOff) {
	if(anchorId.indexOf('tab')!= -1) {//If its a tab or sub-tab
		if(anchorId.indexOf('sub')==-1) {//If its a sub-tab
			$('.tabs li a[href="'+anchorId+'"]').trigger('click');
		}
		else {//Else (its a normal tab)
			$('.sub-tabs li a[href="'+anchorId+'"]').trigger('click');
		}
	}
	else if(anchorId.indexOf('Item')!= -1) {
		if($(anchorId).parents('.sub-tab-content').length)//If the item is in a sub-tab, trigger click on the sub-tab
		{
			var subTabId = $(anchorId).parents('.sub-tab-content').attr('id');
			$('.sub-tabs li a[href="#'+subTabId+'"]').trigger('click');
		}
		else//Else, trigger click on the parent tab
		{
			var tabId = $(anchorId).parents('.tab-content').attr('id');
			$('.tabs li a[href="#'+tabId+'"]').trigger('click');
		}
		
		if($(anchorId).find('.desc:hidden').length) {
			if(animOff) {
				jQuery.fx.off = true;
			}
			$(anchorId).trigger('click')
			jQuery.fx.off = false;
		}
	}
}