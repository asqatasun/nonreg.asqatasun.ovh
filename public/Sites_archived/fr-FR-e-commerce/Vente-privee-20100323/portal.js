/* Porte 

Company : Vente-privee.com
Project : VP4 (Front)
Support : Javascript JQUERY (compatible 1.3.3)

Version : V 0.1
Date : 17 10 2009
Author : Gilles MANZATO

*/

$(document).ready(function() {

    // GESTION DU FOCUS EMAIL
    if ($('.pEmail').val() != '') {
        $('.labelEmail').hide();
    }
    
    $('.pEmail').focus(hideEmail);
    $('.labelEmail').click(function() {
        $('.pEmail').focus();
    });

    function hideEmail() {
        $('.labelEmail').hide();
        if (this.value == this.defaultValue) {
            this.value = '';
        }
        if (this.value != this.defaultValue) {
            this.select();
        }
    };
    $('.pEmail').blur(function() {

        if (this.value == '') {
            $('.labelEmail').attr('style', '');
        }
    });
   // GESTION DU FOCUS PASS
    $('.pPass').focus(hidePass);
    $('.labelPass').click(function() {
        $('.pPass').focus();
    });

    function hidePass() {
        $('.labelPass').hide();
        if (this.value == this.defaultValue) {
            this.value = '';
        }
        if (this.value != this.defaultValue) {
            this.select();
        }
    };
    $('.pPass').blur(function() {

        if (this.value == '') {
            $('.labelPass').attr('style','');
        }
    });
    setInterval(checkPass, 200);
    function checkPass() {
        if ($('.pPass').val() != '') {
            $('.labelPass').hide();
        }
    }
});