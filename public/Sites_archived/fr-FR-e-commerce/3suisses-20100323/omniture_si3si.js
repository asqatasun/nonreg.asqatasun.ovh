/* JavaScript Document
 *
 * Liste de fonction SI3SI pour omniture
 *
 * V 1.0 - SDI - 2007.09.10 - Creation bibliotheque
 * V 1.1 - SDI - 2007.09.27 - Maj SiteCatalyst code version: H.13.
 * V 1.2 - SDI - 2007.12.05 - Ajout variable jspPageName dans l'objet Omniture
 * V 1.3 - SDI - 2008.01.30 - Correction du channel pour le niveau de hierarchie
 * V 1.4 - MFS - 2008.11.12 - Modification de la variable eVar4 - FAN 1530
 */

/* Declaration des variables globales
*************************************/
  /*Activation d'Omniture - Permet ou non l'utilisation du marqueur Omniture*/
  var vOMNI_Enable = true;
  var vOMNI_SilentMode = false; /*Mode silencieux : true = pas d'appel composantOmniture, false = appel composant Omniture*/

  /*Affiche des alertes en mode debug*/
  var vOMNI_ModeDebug = false; //Activation des alertes
  var vOMNI_VisuTraffic = false; //Les variables servant au Trafic
  var vOMNI_VisuCommerce = false; //Les variables servant au Commerce
  var vOMNI_VisuSpecific = false; //Les variables specifiques
  var vOMNI_DebugOnPortlet = ''; //Affiche les alertes sur la portlet uniquement

  /*Caractere de separation pour hierarchie*/
  var vOMNI_Separator = "|";


/*******************************************************************************
 * Marqueur Omniture
 ******************************************************************************/
function OMNI_Marqueur(omniObject)
{
  var methodName = 'Methode - OMNI_Marqueur(omniObject)';

  /*Test l'activation du Marqueur*/
  if (!vOMNI_Enable)
    return;

  /*Page not Found - On alimente la variable pageType uniquement*/
  if (omniObject.pageType) {
    OMNI_MarquerError(omniObject.pageType);
    return;
  }

  /* Mise en forme des variables avant utilisation*/
  if (omniObject.channel) {
    omniObject.channel = omniObject.channel.toUpperCase();
  }

  try{
  omniObject = getVersionSite(omniObject);
  }catch(err){
  //cas autre que accueil/univers;
  }
  try{
  omniObject = getInfosClient(omniObject);
  }catch(err){
  //cas autre que accueil/univers;
  }

  /* You may give each page an identifying name, server, and channel on the next lines. */
  s_omtr.pageName = omniObject.pageName;
  s_omtr.channel  = omniObject.channel;
  s_omtr.prop1    = omniObject.prop1;
  s_omtr.prop2    = omniObject.prop2;
  s_omtr.prop3    = omniObject.prop3;
  s_omtr.prop4    = omniObject.prop4;
  s_omtr.prop5    = omniObject.prop5;
  s_omtr.prop6    = omniObject.prop6;
  s_omtr.prop7    = omniObject.prop7;
  s_omtr.prop8    = omniObject.prop8;
//  s_omtr.prop9    = omniObject.prop9; pas utilise car initialise dans s_code.js
//  s_omtr.prop10   = omniObject.prop10; pas utilise car initialise dans s_code.js
  s_omtr.prop11    = omniObject.prop11;

  /* E-commerce Variables */
  s_omtr.campaign   = omniObject.campaign;
  s_omtr.state      = omniObject.state;
  s_omtr.zip        = omniObject.zip;
  s_omtr.events     = omniObject.events;
  s_omtr.products   = omniObject.products;
  s_omtr.purchaseID = omniObject.purchaseID;
  s_omtr.eVar1      = omniObject.eVar1;
  s_omtr.eVar2      = omniObject.eVar2;
  s_omtr.eVar3      = omniObject.eVar3;
  s_omtr.eVar4      = OMNI_GetNavigationMerchandising(omniObject);
  s_omtr.eVar5      = omniObject.eVar5;
  s_omtr.eVar6      = omniObject.eVar6;
  s_omtr.eVar7      = omniObject.eVar7;
  s_omtr.eVar8      = omniObject.eVar8;
  s_omtr.eVar9      = omniObject.eVar9;
  s_omtr.eVar10     = omniObject.eVar10;
  s_omtr.eVar11     = omniObject.eVar11;
  s_omtr.eVar12     = omniObject.eVar12;
  s_omtr.eVar13     = omniObject.eVar13;
  s_omtr.eVar14     = omniObject.eVar14;
  s_omtr.eVar26     = omniObject.eVar26;

  /* Hierarchy Variables */
  s_omtr.hier1 = OMNI_GetHierarchie(omniObject);

  /*Trace variables Omniture*/
  OMNI_TraceMarqueur(methodName, omniObject)

  /*Marquage Omniture*/
  OMNI_Marquer();

  /* /DO NOT REMOVE/ */
}


/*******************************************************************************
 * Marqueur Omniture pour les pages statiques
 ******************************************************************************/
function OMNI_MarqueurPage(vPageName, vChannel)
{
  var vObjOmniture = new OMNI_InitObject();
  vObjOmniture.pageName = vPageName;
  vObjOmniture.channel = vChannel;
  OMNI_Marqueur(vObjOmniture);
}

function OMNI_MarqueurPage(vPageName, vChannel, vEvents, vProp11)
{
  var vObjOmniture = new OMNI_InitObject();
  vObjOmniture.pageName = vPageName;
  vObjOmniture.channel = vChannel;
  vObjOmniture.events = vEvents;
  vObjOmniture.prop11 = vProp11;
  OMNI_Marqueur(vObjOmniture);
}

/*******************************************************************************
 * Marquer la page d'erreur - Page not Found
 ******************************************************************************/
function OMNI_MarquerError(vPageType)
{
  /*Init Message*/
  var messTraffic = 'Methode - OMNI_MarquerError(pageType)';

  /*Init variable Omniture*/
  s_omtr.pageType = vPageType;

  /*Init Message avec les variables de trafic*/
  if (vOMNI_VisuTraffic) {
    messTraffic = '\n'
      +  'pageType = ' + s_omtr.pageType;
  }

  /*Affichage du message*/
  if (vOMNI_ModeDebug)
    OMNI_Alert(messTraffic);

  /*Marquage Omniture*/
  OMNI_Marquer();
}

/*******************************************************************************
 * Marquer Omniture
 ******************************************************************************/
function OMNI_Marquer()
{
  /*Mode silencieux*/
  if (vOMNI_SilentMode)
    return;

  /************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
  var s_code=s_omtr.t();
  if(s_code)
    document.write(s_code);

  /* /DO NOT REMOVE/ */
}


/*******************************************************************************
 * Affiche une popup alert omniture
 ******************************************************************************/
function OMNI_Alert(messageAlerte)
{
  alert('-- MESSAGE OMNITURE-- \n' + messageAlerte);
}


/*******************************************************************************
 * Trace les variables Omniture
 ******************************************************************************/
function OMNI_TraceMarqueur(traceMarqueur, omniObject)
{
  /*Test activation trace*/
  if (!vOMNI_ModeDebug)
    return;

  /*Test si portlet renseigne*/
  if (vOMNI_DebugOnPortlet && vOMNI_DebugOnPortlet != omniObject.portletName)
    return;

  /*Init Message avec les variables de trafic*/
  var messTraffic = traceMarqueur;
  if (vOMNI_VisuTraffic) {
    messTraffic += '\n---------TRAFIC---------------'
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'events', s_omtr.events);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'products', s_omtr.products);

    messTraffic = OMNI_AddTraceVariable(messTraffic, 'pageName', s_omtr.pageName);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'channel', s_omtr.channel);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'prop1', s_omtr.prop1);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'prop2', s_omtr.prop2);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'prop3', s_omtr.prop3);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'prop4', s_omtr.prop4);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'prop5', s_omtr.prop5);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'prop6', s_omtr.prop6);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'prop7', s_omtr.prop7);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'prop8', s_omtr.prop8);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'prop9', s_omtr.prop9);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'prop10', s_omtr.prop10);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'prop11', s_omtr.prop11);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'hier1', s_omtr.hier1);
  }

  /*Init Message avec les variables de Commerce*/
  if (vOMNI_VisuCommerce) {
    messTraffic += '\n---------COMMERCE-------------'
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'purchaseID', s_omtr.purchaseID);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'campaign', s_omtr.campaign);

//    messTraffic = OMNI_AddTraceVariable(messTraffic, 'eVar1', s_omtr.eVar1);  s_omtr.eVar1=s_omtr.prop1 capture automatiquement
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'eVar2', s_omtr.eVar2);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'eVar3', s_omtr.eVar3);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'eVar4', s_omtr.eVar4);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'eVar5', s_omtr.eVar5);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'eVar6', s_omtr.eVar6);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'eVar7', s_omtr.eVar7);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'eVar8', s_omtr.eVar8);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'eVar9', s_omtr.eVar9);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'eVar10', s_omtr.eVar10);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'eVar11', s_omtr.eVar11);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'eVar12', s_omtr.eVar12);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'eVar13', s_omtr.eVar13);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'eVar14', s_omtr.eVar14);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'eVar26', s_omtr.eVar26);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'zip', s_omtr.zip);
  }

  /*Init Message avec les variables specifiques*/
  if (vOMNI_VisuSpecific) {
    messTraffic += '\n---------SPECIFIC-------------'
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'portletName', omniObject.portletName);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'labelDefinitionName', omniObject.labelDefinitionName);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'jspPageName', omniObject.jspPageName);
    messTraffic = OMNI_AddTraceVariable(messTraffic, 'previousLabelDefinitionName', omniObject.previousLabelDefinitionName);
  }

  /*Affichage du message*/
  OMNI_Alert(messTraffic);
}


/*******************************************************************************
 * Ajoute les variables du marqueur pour les traces
 ******************************************************************************/
function OMNI_AddTraceVariable(vTraceMarqueur, vVariableLabel, vVariableValue)
{
  var vMessage = vTraceMarqueur;
  if(vVariableValue)
    vMessage += '\n' + vVariableLabel + ' = ' + vVariableValue;

  return vMessage;
}

/*******************************************************************************
 * Initialisation variable a chaine vide si undefined
 ******************************************************************************/
function OMNI_CheckString(vVariable)
{
  if(vVariable)
    return ;

  return vVariable;
}


/*******************************************************************************
 * Ajout d'un message a une variable
 ******************************************************************************/
function OMNI_AddTexte(vVariable, vTexte, vSeparator)
{
  var vNewMessage = vVariable;
  if (vTexte) {
    if (vVariable)
      vNewMessage = vVariable + vSeparator + vTexte;
    else
      vNewMessage = vTexte;
  }

  return vNewMessage;
}


/*******************************************************************************
 * Recupere l'arborescence
 ******************************************************************************/
function OMNI_GetNavigationMerchandising(omniObject)
{
  /*Init avec le pays*/
  var vNavigation = "";

  if (omniObject.channel && omniObject.channel.toLowerCase()!='commande'){
    /*Construction de la hierarchie*/
    /*Ajout channel / Univers*/
    vNavigation = OMNI_AddTexte(vNavigation, omniObject.channel, vOMNI_Separator);
    /*Ajout Boutique*/
    if (omniObject.prop3)
      vNavigation = OMNI_AddTexte(vNavigation, omniObject.prop3, vOMNI_Separator);

    /*Ajout Rayon*/
    if (omniObject.prop4)
      vNavigation = OMNI_AddTexte(vNavigation, omniObject.prop4, vOMNI_Separator);

    /*Ajout SousRayon*/
    if (omniObject.prop5)
      vNavigation = OMNI_AddTexte(vNavigation, omniObject.prop5, vOMNI_Separator);
  }
  if (omniObject.channel && omniObject.channel.toLowerCase()=='commande')
    vNavigation = "Pas une navigation";

  return vNavigation;
}


function OMNI_GetNavigation(omniObject)
{
  /*Init avec le pays*/
  var vNavigation = "";

  /*Cas ou la hierarchie est deja construite*/
  if (omniObject.hier1) {
    vNavigation = OMNI_AddTexte(vNavigation, omniObject.channel, vOMNI_Separator);
    vNavigation = OMNI_AddTexte(vNavigation, omniObject.hier1, vOMNI_Separator);
  }
  else {
    /*Construction de la hierarchie*/
    /*Ajout channel / Univers*/
    if (omniObject.channel)
      vNavigation = OMNI_AddTexte(vNavigation, omniObject.channel, vOMNI_Separator);
    /*Ajout Boutique*/
    if (omniObject.prop3)
      vNavigation = OMNI_AddTexte(vNavigation, omniObject.prop3, vOMNI_Separator);

    /*Ajout Rayon*/
    if (omniObject.prop4)
      vNavigation = OMNI_AddTexte(vNavigation, omniObject.prop4, vOMNI_Separator);

    /*Ajout SousRayon*/
    if (omniObject.prop5)
      vNavigation = OMNI_AddTexte(vNavigation, omniObject.prop5, vOMNI_Separator);

    /*Ajout Libelle Produit*/
    if (omniObject.productLabel)
      vNavigation = OMNI_AddTexte(vNavigation, omniObject.productLabel, vOMNI_Separator);
  }

  return vNavigation;
}


/*******************************************************************************
 * Recupere la hierarchie
 ******************************************************************************/
function OMNI_GetHierarchie(omniObject)
{
    return OMNI_AddTexte(s_omtr.prop9, OMNI_GetNavigation(omniObject), vOMNI_Separator);
}

/*******************************************************************************
 * Initialisation liste des attributs omniture
 ******************************************************************************/
function OMNI_InitObject()
{
  this.pageName="";
  this.channel="";
  this.pageType="";
  this.prop1="";
  this.prop2="";
  this.prop3="";
  this.prop4="";
  this.prop5="";
  this.prop6="";
  this.prop7="";
  this.prop8="";
//  this.prop9=""; pas utilise car initialise dans s_code.js
//  this.prop10=""; pas utilise car initialise dans s_code.js
  /* E-commerce Variables */
  this.campaign="";
  this.state="";
  this.zip="";
  this.events="";
  this.products="";
  this.purchaseID="";
  this.eVar1="";
  this.eVar2="";
  this.eVar3="";
  this.eVar4="";
  this.eVar5="";
  this.eVar6="";
  this.eVar7="";
  this.eVar8="";
  this.eVar9="";
  this.eVar10="";
  this.eVar11="";
  this.eVar12="";
  this.eVar13="";
  this.eVar14="";
  /* Hierarchy Variables */
//  this.hier1=""; pas utilise car initialise dans OMNI_Marqueur()

  /*Variables specifiques*/
  this.productLabel = "";
  this.portletName = "";
  this.labelDefinitionName = "";
  this.jspPageName = "";
  this.previousLabelDefinitionName = "";
}

