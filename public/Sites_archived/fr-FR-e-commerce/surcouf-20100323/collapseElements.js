function setupCollapse(){
	if(!document.getElementsByTagName)
		return;
	var all_links=document.getElementsByTagName("span");
	for(var i=0; i<all_links.length; i++){
		var link=all_links[i];
		if(link.className && (" "+link.className+" ").indexOf(" linkCollapse ")!=-1){
		
			//tma 2699
			if(link.parentNode.parentNode.childNodes[1].id == "idSimulationFinancement")
				{
				link.parentNode.parentNode.childNodes[1].className = "displayNone";
				link.className="linkCollapse collapseClose";
				}
			else
				{
				link.parentNode.parentNode.childNodes[1].className = link.parentNode.parentNode.childNodes[1].className + " displayBlock";
				link.className="linkCollapse collapseOpen";
				}

			addEvent(link, "click", clickCollapse, false);
		}
	}

	var openLay=document.getElementById("openLay");
	var closeLay=document.getElementById("closeLay");

	if (openLay)
		addEvent(openLay, "click", clickLay, false);
	if (closeLay)
		addEvent(closeLay, "click", clickLay, true);
}

function findTarget(e){
	/* part of the DOM EVENT */

	var target;

	if(window.event && window.event.srcElement)
		target=window.event.srcElement;
	else if(e && e.target)
		target=e.target;
	if(!target)
		return null;

	return target;
}

/* Ouverture des layer de droite */
function clickCollapse(e){
	var target= findTarget(e);
	if(!target) return;

	var monDivtoShow=target.parentNode.parentNode.childNodes[1];
	var maClassShow=monDivtoShow.className;
	if(maClassShow.indexOf("displayBlock")==-1){
		monDivtoShow.className=maClassShow.replace("displayNone", "displayBlock");
		target.className=target.className.replace("collapseClose", "collapseOpen");
	}else{
		monDivtoShow.className=maClassShow.replace("displayBlock", "displayNone");
		target.className=target.className.replace("collapseOpen", "collapseClose");
	}
}


/* Ouverture du layer catalogue */
function hideSelect(){
var selectRight=document.getElementById('SecondCol').getElementsByTagName('select');
for (i=0; i<selectRight.length; i++){
	//alert(selectRight.length);
	selectRight[i].className="visibleHid";
	}
}

function ShowSelect(){
var selectRight=document.getElementById('SecondCol').getElementsByTagName('select');
for (i=0; i<selectRight.length; i++){
	//alert(selectRight.length);
	selectRight[i].className="visibleShow";
	}
}

var bSimulfinance = false;

function clickLay(){
	if(document.getElementById("layerCatalog").className=="displayNone"){
		document.getElementById("layerCatalog").className="displayBlock";
		document.getElementById("openLay").className="openlay";
		hideSelect();
		/* Mise en place de l'Iframe en dessous du layer afin d'eviter d'avoir des <select> au dessus du layer */
		/* C'est une correction d'un bug IE*/
		var top=document.getElementById("layerCatalog").offsetTop;
		var left=document.getElementById("layerCatalog").offsetLeft;
		var width=document.getElementById("layerCatalog").offsetWidth;
		var height=document.getElementById("layerCatalog").offsetHeight;
		var styleLayerCatalogue=document.getElementById("IframeLayerCatalogue").style;
		styleLayerCatalogue.width=width+"px";
		styleLayerCatalogue.height=height+"px";
		styleLayerCatalogue.top=top+"px";
		styleLayerCatalogue.left=left+"px";
		styleLayerCatalogue.visibility = "visible";
		
		if(document.getElementById("idSimulationFinancement"))
		{
			if(document.getElementById("idSimulationFinancement").className == "displayBlock")
			{
				bSimulfinance = true;
				document.getElementById("idSimulationFinancement").className = "displayNone";
				document.getElementById("idSimulationFinancement").parentNode.childNodes[0].childNodes[0].className = document.getElementById("idSimulationFinancement").parentNode.childNodes[0].childNodes[0].className.replace("collapseOpen", "collapseClose");
			}
			else
			{
				bSimulfinance = false;
			}
		}
	

		return(false);
	}
	else{
		document.getElementById("layerCatalog").className="displayNone";
		document.getElementById("openLay").className="closelay";
		var styleLayerCatalogue=document.getElementById("IframeLayerCatalogue").style;
		styleLayerCatalogue.visibility = "hidden";
		styleLayerCatalogue.height = "0px";
		ShowSelect();
		
		if((document.getElementById("idSimulationFinancement")) && (bSimulfinance))
		{
			document.getElementById("idSimulationFinancement").className = "displayBlock";
			document.getElementById("idSimulationFinancement").parentNode.childNodes[0].childNodes[0].className = document.getElementById("idSimulationFinancement").parentNode.childNodes[0].childNodes[0].className.replace("collapseClose", "collapseOpen");
		}

		return(false);
	}
}

//fonction affichage bock de simulation financement s'il existe de droite tma 2699
function OpenSimulationFinancement()
{
	var SimulationFinance;
	
	if(document.all)
	{
		SimulationFinance = document.all["idSimulationFinancement"]; //IE4
	}
	else
	{
		SimulationFinance= document.getElementById("idSimulationFinancement");
	}	
	
	if(SimulationFinance)
	{
		var TbBaliseSpam;
		TbBaliseSpam = SimulationFinance.parentNode.getElementsByTagName("span");
		
		if(TbBaliseSpam.length > 0)
		{
			TbBaliseSpam[0].className = "collapseOpen";
			SimulationFinance.className = "displayBlock";	
			
			document.location = "#AncreSimulFinance"; //repositionne la page
		}
	}

}



addLoadListener(setupCollapse);
