
			/** variables **/
			var nbimg=0;
			var isTiming=0;
			var zoneImg = 'diapoImg';
			var zoneThumbnails = 'diapoThb';
			var arrowLeft = 'fleche_gauche';
			var arrowRight = 'fleche_droite';
			/** classes **/
			var arrowClass="available";
			var thmbClass="diapoOn";

			/** diaporama **/
			var tempoDiapo = 2000;//en millisecondes
			var nb_img_affichees = 3;
			var premiere_img_affichee = 0;
			diapo = {
			init:function(){
			if(document.getElementById && document.getElementsByTagName && document.createElement && document.getElementById(zoneImg) &&  document.getElementById(arrowRight) && document.getElementById(arrowLeft))
			{

					/** init des �l�ments pour g�rer les fl�ches **/

					document.getElementById(arrowLeft).className=arrowClass;
					document.getElementById(arrowRight).className=arrowClass;
					document.getElementById(zoneThumbnails).className=thmbClass;


					document.getElementById(arrowLeft).onclick = function()
					{
						diapo.previous();
					}

					document.getElementById(arrowRight).onclick = function()
					{
						diapo.next();
					}

					if(document.getElementById(zoneThumbnails).getElementsByTagName('a'))
					{
						nbimg = document.getElementById(zoneThumbnails).getElementsByTagName('a');
						total_de_depart = parseInt(nbimg);
					}

					if(document.getElementById(zoneImg).getElementsByTagName('a'))
					{
						document.getElementById(zoneImg).getElementsByTagName('a')[0].onclick=function()
						{
							tmp = this.getElementsByTagName('img')[0].src.replace('250x250','320x320');
							diapo.openBig(tmp);
						}
						document.getElementById(zoneImg).getElementsByTagName('a')[0].href="#";
					}


					for(i=0;i<nbimg.length;i++)
					{
						nbimg[i].tmp = nbimg[i].href;
						nbimg[i].href='#';
						nbimg[i].onclick = function()
						{
							/** si on a un diaporama en cours, on le stoppe **/
							diapo.stop();

							var allSpecial = getElementsByClass('thumbon',document,'a');
							for(o=0;o<allSpecial.length;o++)
							{
								allSpecial[o].className="thumboff";
							}

							if(document.getElementById(zoneImg) && document.getElementById(zoneImg).getElementsByTagName('img') && document.getElementById(zoneImg).getElementsByTagName('a'))
							{
								document.getElementById(zoneImg).getElementsByTagName('img')[0].src=this.tmp;
								document.getElementById(zoneImg).getElementsByTagName('a')[0].onclick=function(){
									/** si on a un diaporama en cours, on le stoppe **/
									diapo.stop();

									diapo.openBig(document.getElementById(zoneImg).getElementsByTagName('img')[0].src.replace('250x250','320x320'));
								};
								document.getElementById(zoneImg).getElementsByTagName('a')[0].href="#";

							}

							this.className="thumbon";

						}
					}


			}else
			{
				return false;
			}
		},
		play:function(){
			isTiming = window.setTimeout(diapo.playloop,tempoDiapo)
			document.getElementById('pictDiapo').className='pictDiapo_on';
		},
		stop:function(){
			clearTimeout(isTiming);
			document.getElementById('pictDiapo').className='pictDiapo';
		},
		playloop:function(){

			diapo.nextDiapo();
			
			//eval(nbimg[premiere_img_affichee].onclick());
			diapo.play();
		},
		next:function(){

				var Node = document.getElementById(zoneThumbnails);

				oldObj = Node.getElementsByTagName('a')[0];


				Node.appendChild(oldObj);

		},
		previous:function(){

				var Node = document.getElementById(zoneThumbnails);

				oldObj = Node.lastChild;

				//Node.removeChild(oldObj);
				Node.insertBefore(oldObj,Node.childNodes[0]);
		},
		nextDiapo:function(){
			var Node = document.getElementById(zoneThumbnails);

				oldObj = Node.getElementsByTagName('a')[0];

				Node.appendChild(oldObj);
				if(Node.getElementsByTagName('a')[0].onclick)Node.getElementsByTagName('a')[0].onclick();

		},
		setImg:function()
		{
			var allSpecial = getElementsByClass('thumbon',document,'a');
			for(o=0;o<allSpecial.length;o++)
			{
				allSpecial[o].className="thumboff";
			}

			if(this.childNodes[0].src && (this.childNodes[0].src.indexOf('80x80')!=-1))
			{
				this.tmp = this.childNodes[0].src.replace('80x80','250x250');
			}else if(this.childNodes[0]){
				this.tmp = this.childNodes[0].src;
			}

			if(document.getElementById(zoneImg) && document.getElementById(zoneImg).getElementsByTagName('img') && document.getElementById(zoneImg).getElementsByTagName('a'))
			{
				document.getElementById(zoneImg).getElementsByTagName('img')[0].src=this.tmp;
				document.getElementById(zoneImg).getElementsByTagName('a')[0].onclick=function(){
					diapo.openBig(document.getElementById(zoneImg).getElementsByTagName('img')[0].src.replace('250x250','320x320'));
				};
				document.getElementById(zoneImg).getElementsByTagName('a')[0].href="#";

			}
			this.className="thumbon";
		},
		openBig:function(nom_img){
				var childEl = document.createElement("div");
				var imgEl = document.createElement("img");
				imgEl.setAttribute('src',nom_img);
				//alert(nom_img);
				childEl.appendChild(imgEl);
				childEl.onclick=diapo.closeBig;
				childEl.className='layer_zoom';
				document.getElementById(zoneImg).appendChild(childEl);
		},
		closeBig:function(){
			this.parentNode.removeChild(this);
		}

	};



/** on load**/
addEvent(window,'load',diapo.init, false);