/*
 * menuDropdown.js - implements an dropdown menu based on a HTML list
 * Author: Dave Lindquist (dave@gazingus.org)
 */

var currentMenu = null;
var navigateur = null;
var timehide= null;
if (!document.getElementById){
    document.getElementById = function(name) {
        if(document.all)
            return document.all[name];
        else
            return null;
         }
}

function initializeMenu(menuId, actuatorId, posX, posY) {
	navigateur=navigator.appName.substring(0,8);
		
    var menu = document.getElementById(menuId);
    var actuator = document.getElementById(actuatorId);

    if (menu == null || actuator == null) return;

    if (window.opera) return; // I'm too tired

    actuator.onmouseover = function() {
        if (currentMenu) {
            currentMenu.style.visibility = "hidden";
            clearTimeout(timehide);
            this.showMenu();
        }
        return false;
    }

    actuator.onclick = function() {
	    if(timehide){
			clearTimeout(timehide);
			timehide=null;
		}
        if (currentMenu == null) {
            this.showMenu();
        }
        else {
            currentMenu.style.visibility = "hidden";
            currentMenu = null;
        }
		return false;
    }

    actuator.showMenu = function() {
        menu.style.left = (this.offsetLeft+posX) + "px";

        menu.style.top=(findPosY(this)+  this.offsetHeight ) + "px";

        menu.style.visibility = "visible";

		menu.onmouseout= function (){
		    timehide=setTimeout("hideMenu()",2000);
		}
        menu.onmouseover= function(){
            clearTimeout(timehide);
			timehide=null;
        }
        timehide=setTimeout("hideMenu()",2000);
        currentMenu = menu;
    }
}

function hideMenu(){

    if( currentMenu && timehide){
	    currentMenu.style.visibility="hidden";
		currentMenu= null;
	}
    timehide=null;
}

function findPosY(obj){

	var curtop = 0;
	if (document.getElementById || document.all)
	{
		while (obj.offsetParent)
		{
			curtop += obj.offsetTop
			obj = obj.offsetParent;
		}
	}
	else if (document.layers)
		curtop += obj.y;

    	return curtop;
}

