/**
 * @author Victor Matekole (CC Marketing) 17/09/2009
 * This is the adidas Analytics Wrapper. Its purpose is for calling Analytics Provider tags such as CoreMetrics.
 * The Analytics tags in this library are to be used in web pages across adidas eComm and non-eComm sites.
 * 
 * The wrapper also specifies the Analytics accounts it will send the tags to. E.g. UK or Germany. 
 * It will determine the country of origin by the request parameter passed to all microsites by Akamai, namely, strCountry_adidascom, 
 * or by the URL in the case of the PID/adidas.com homepage or by a reading the value of a cookie. 
 * Version 1.01 - Support HTTPS domains
 * 	       1.02 - Support multi-currency and domains with a subdomain named origin
 * This library leverages the Revealing Module Pattern (http://www.wait-till-i.com/2007/08/22/again-with-the-module-pattern-reveal-something-to-the-world/)
 
 */
	var NOEXPLOREATTR = 15;
	var GLOBAL = "COM";
	var VERSION = "1.02";
	var BRANDSITES = "adidas";
	var USESHOP = "useshop";
	var ORIGINALS = "originals";
	var PID = "pid";
	var ROWESHOP = "roweshop";
	var USMIADIDAS = "miadidas";
	var ROWMIADIDAS = "rowmiadidas";
	var MITEAM = "miteam";
	var MIADIDAS = "miadidas";
	var RETAILMIADIDAS = "retailmiadidas";
	var CHECKOUTMIADIDAS = "checkoutmiadidas";
	var MICOACH = "micoach";
	var ADIDASTV = "adidastv";
	var CORPORATE = "corporatesite";
	var BLOGWIKI = "blogwiki";
	
   /**
    * CoreMetrics Provider class.
    * @base CoreMetricsProvider
    */
	var CoreMetricsProvider = function()
	{
		var self = this;
	   /**
	    * Worker method for splitting out Explore attributes into an array.
	    * @member GetExploreAttributes
	    * @returns {Array} of Explore Attributes
		*/
		var GetExploreAttributes = function(attributes)
		{
			var attr = null;
			if (attributes)
			{
				attr = attributes.split("-_-");
			} 
			return attr;
		};
		
	   /**
	    * Returns the CoreMetrics ClientId based on country code and site.
	    * @member GetclientId
	    * @param countryOfOrigin e.g. UK, DE
	    * @param site Site name
	    * @returns {String} CoreMetrics ClientId
		*/							
		var GetClientId = function(countryOfOrigin, site)
		{
			var clientId = null;
			
			/// CM Rollup Client Id's
			var CM_TEST_GLOBAL_ROLLUP = "90298033";
			var CM_GLOBAL_ROLLUP = "90297136";
			var CM_NONESHOP_ROLLUP = "90297149";
			var CM_USESHOP_ROLLUP = "90297162";
			var CM_MITEAM_ROLLUP = "90297188";
			var CM_MIADIDAS_ROLLUP = "90297175";
			
			// ROW (Rest Of World eComm) Site Client Id's
			
			var CM_DE_ESHOP = "90297227";
			var CM_FR_ESHOP = "90297214";
			var CM_NL_ESHOP = "90297240";
			var CM_UK_ESHOP = "90297253";
			var CM_CA_ESHOP = "90297201";
			
			// US eComm sites
			var CM_US_ESHOP = "90297266";
		
			// non eComm sites
			var CM_AE = "90297305";
			var CM_AT = "90297292";
			var CM_AU = "90297279";
			var CM_CH = "90297682";
			var CM_CN = "90297357";
			var CM_DE = "90297409";
			var CM_DK = "90297370";
			var CM_ES = "90297656";
			var CM_FL = "90297383";
			var CM_FR = "90297396";
			var CM_GR = "90297422";
			var CM_HK = "90297435";
			var CM_HU = "90297448";
			var CM_IN = "90297461";
			var CM_IT = "90297474";
			var CM_JP = "90297487";
			var CM_KR = "90297500";
			var CM_MY = "90297526";
			var CM_NL = "90297539";
			var CM_NO = "90297565";
			var CM_NZ = "90297552";
			var CM_PH = "90297578";
			var CM_PL = "90297591";
			var CM_PT = "90297604";
			var CM_RU = "90297617";
			var CM_SE = "90297669";
			var CM_SG = "90297630";
			var CM_TH = "90297708";
			var CM_TW = "90297695";
			var CM_UK = "90297721";
			var CM_ZA = "90297643";
			var CM_US = "90297734";
			var CM_CA = "90297331";
			var CM_CF = "90297344";
			var CM_BR = "90297318";
			var CM_LA = "90297513";
			// Other sites
			var CM_MICOACH  = "90297747";
			var CM_ADIDASTV = "90297760";
			var CM_CORPORATE = "90297773";
			var CM_BLOGWIKI = "90297786";	

			// Begin setup for Client Id 
			// CM_TEST_GLOBAL_ROLLUP, temporary client id for the purposes of testing
			//clientId = CM_TEST_GLOBAL_ROLLUP + ";";
			   clientId = CM_GLOBAL_ROLLUP + ";"; 
			// Assign any special rollups to the site
			switch(site)
			{
				case BRANDSITES: case PID:
					clientId = clientId + CM_NONESHOP_ROLLUP;
					break;
				case MITEAM:
					clientId = clientId + CM_MITEAM_ROLLUP + ";" + CM_MIADIDAS_ROLLUP;
					break;
				case MICOACH:
					clientId = clientId + CM_MICOACH;
					return clientId;
				case ROWMIADIDAS:
					clientId = clientId + CM_MIADIDAS_ROLLUP;
					break;
				case ROWESHOP:
					switch(countryOfOrigin)
					{
						case "DE":
						clientId = clientId + CM_DE_ESHOP;
						return clientId;
						case "FR":
						clientId = clientId + CM_FR_ESHOP;
						return clientId;
						case "NL":
						clientId = clientId + CM_NL_ESHOP;
						return clientId;
						case "UK":
						clientId = clientId + CM_UK_ESHOP;
						return clientId;
						case "CA":
						clientId = clientId + CM_CA_ESHOP;
						return clientId;
					}
					break;
				case USESHOP:
					clientId = clientId + CM_US_ESHOP + ";" + CM_US_ESHOP + ";" + CM_USESHOP_ROLLUP;
					return clientId;
				case USMIADIDAS:
					clientId = clientId + CM_USESHOP_ROLLUP + ";" + CM_MIADIDAS_ROLLUP;
					return clientId;
				case ADIDASTV:
					clientId = clientId + CM_ADIDASTV;
					return clientId;
				case CHECKOUTMIADIDAS:
					clientId = clientId + CM_USESHOP_ROLLUP + ";" + CM_MIADIDAS_ROLLUP;
					return clientId;
				case RETAILMIADIDAS:
					clientId = clientId + CM_MIADIDAS_ROLLUP;
					return clientId;
				
			}

			// brand sites
			if((site === BRANDSITES || site == PID) && countryOfOrigin !== null)
			{
				countryOfOrigin = countryOfOrigin.toUpperCase();
				switch (countryOfOrigin) 
				{
					case "US":
						clientId = clientId + CM_US + ";" + CM_USESHOP_ROLLUP + ";";
						break;	
					case "AE":
						clientId = clientId + ";" + CM_AE; 
						break;
					case "AT":
						clientId = clientId + ";" + CM_AT;
						break;
					case "AU":
						clientId = clientId + ";" + CM_AU;
						break;
					case "CH":
						clientId = clientId + ";" + CM_CH;
						break;
					case "CN":
						clientId = clientId + ";" + CM_CN;
						break;	
					case "DE":
						clientId = clientId + ";" + CM_DE;
						break;	
					case "DK":
						clientId = clientId + ";" + CM_DK;
						break;	
					case "ES":
						clientId = clientId + ";" + CM_ES;
						break;	
					case "FL":
						clientId = clientId + ";" + CM_FL;
						break;	
					case "FR":
						clientId = clientId + ";" + CM_FR;
						break;	
					case "GR":
						clientId = clientId + ";" + CM_GR;
						break;	
					case "HK":
						clientId = clientId + ";" + CM_HK;
						break;	
					case "HU":
						clientId = clientId + ";" + CM_HU;
						break;	
					case "IN":
						clientId = clientId + ";" + CM_IN;
						break;	
					case "IT":
						clientId = clientId + ";" + CM_IT;
						break;	
					case "JP":
						clientId = clientId + ";" + CM_JP;
						break;	
					case "KR":
						clientId = clientId + ";" + CM_KR;
						break;	
					case "MY":
						clientId = clientId + ";" + CM_MY;
						break;	
					case "NL":
						clientId = clientId + ";" + CM_NL;
						break;	
					case "NO":
						clientId = clientId + ";" + CM_NO;
						break;	
					case "NZ":
						clientId = clientId + ";" + CM_NZ;
						break;
 					case "PH":
						clientId = clientId + ";" + CM_PH;
						break;	
					case "PL":
						clientId = clientId + ";" + CM_PL;
						break;	
					case "PT":
						clientId = clientId + ";" + CM_PT;
						break;	
					case "RU":
						clientId = clientId + ";" + CM_RU;
						break;	
					case "SE":
						clientId = clientId + ";" + CM_SE;
						break;	
					case "SG":
						clientId = clientId + ";" + CM_SG;
						break;	
					case "TH":
						clientId = clientId + ";" + CM_TH;
						break;	
					case "TW":
						clientId = clientId + ";" + CM_TW;
						break;	
					case "UK":
						clientId = clientId + ";" + CM_UK;
						break;	
					case "ZA":
						clientId = clientId + ";" + CM_ZA;
						break;	
					case "US":
						clientId = clientId + ";" + CM_US;
						break;
					case "CA":
						clientId = clientId + ";" + CM_CA;
						break;				
					case "CF":
						clientId = clientId + ";" + CM_CF;
						break;
					case "BR":	
						clientId = clientId + ";" + CM_BR;
						break;
					case "LA":
						clientId = clientId + ";" + CM_LA;
						break;
				}
				return clientId;	
			}		
			return clientId;
		};
		
	   /**
	    * Manually set the CoreMetrics ClientId with a semi-colon delimited string e.g. "0000001;0000002"
	    * @member SetClientId
	    * @param clientId
		*/	
		var SetClientId = function(clientId)
		{
			cm_ClientID = clientId;
		};
	
	   /**
	    * Calls CM provider code for Page View tag, cmCreatePageviewTag
	    * @member trackPageViewed
	    * @param {AnalyticsDataPoints} Contains Analytics Data Points
		*/		
		var trackPageViewed = function(analyticsDataPoints)
		{
			var attr = GetExploreAttributes(analyticsDataPoints.getExploreAttributes());
			if (attr !== null) 
			{
				if (attr.length >= NOEXPLOREATTR) 
				{
					throw "You cannot pass more than 14 explore attributes as the 1st is prepopulated with the country code of the site.";
				}
			}
			var attributes = null;
			if(attr == null || attr.length == 0)
			{
				attributes = analyticsDataPoints.getCountryCode();  // Add country code as the first Explore attribute 
			}
			else
			{
				attributes = analyticsDataPoints.getCountryCode() + "-_-" + analyticsDataPoints.getExploreAttributes(); // Add country code as the first Explore attribute and additional attributes
			}
			cmCreatePageviewTag(analyticsDataPoints.getPageId(), analyticsDataPoints.getCategoryId(), analyticsDataPoints.getSearchTerm(), analyticsDataPoints.getTotalNumOfSearchResults(), attributes);
		};
		
	   /**
	    * Calls CM provider code for Page Element tag, cmCreatePageElementTag
	    * @member trackPageElement
	    * @param {AnalyticsDataPoints} Contains Analytics Data Points
		*/			
		var trackPageElement = function(analyticsDataPoints)
		{
			var attr = GetExploreAttributes(analyticsDataPoints.getExploreAttributes());
			if (attr !== null) 
			{
				if (attr.length >= NOEXPLOREATTR) 
				{
					throw "You cannot pass more than 14 explore attributes as the 1st is prepopulated with the country code of the site.";
				}
			}
			var attributes = null;
			if(attr == null || attr.length == 0)
			{
				attributes = analyticsDataPoints.getCountryCode();  // Add country code as the first Explore attribute 
			}
			else
			{
				attributes = analyticsDataPoints.getCountryCode() + "-_-" + analyticsDataPoints.getExploreAttributes(); // Add country code as the first Explore attribute and additional attributes
			}
			cmCreatePageElementTag(analyticsDataPoints.getElementId(),analyticsDataPoints.getCategoryId(),null,null,null,attributes);	
		};
		
	   /**
	    * Calls CM provider code for Product View tag, cmCreateProductviewTag
	    * @member trackProductView
	    * @param {AnalyticsDataPoints} Contains Analytics Data Points
		*/	
		var trackProductView = function(analyticsDataPoints)
		{	
			var attr = GetExploreAttributes(analyticsDataPoints.getExploreAttributes());
			if (attr !== null) 
			{
				if (attr.length >= NOEXPLOREATTR) 
				{
					throw "You cannot pass more than 14 explore attributes as the 1st is prepopulated with the country code of the site.";
				}
			}
			var attributes = null;
			if(attr == null || attr.length == 0)
			{
				attributes = analyticsDataPoints.getCountryCode();  // Add country code as the first Explore attribute 
			}
			else
			{
				attributes = analyticsDataPoints.getCountryCode() + "-_-" + analyticsDataPoints.getExploreAttributes(); // Add country code as the first Explore attribute and additional attributes
			}
			cmCreateProductviewTag(analyticsDataPoints.getProductId(), analyticsDataPoints.getProductName(), analyticsDataPoints.getCategoryId(), attributes);
		};
		
	   /**
	    * Calls cmCreateRegistrationTag for tracking User registrations
	    * @member trackRegistration
	    * @param {AnalyticsDataPoints} Contains Analytics Data Points
		*/			
		var trackRegistration = function(analyticsDataPoints)
		{
			var attr = GetExploreAttributes(analyticsDataPoints.getExploreAttributes());
			if (attr !== null) 
			{
				if (attr.length >= NOEXPLOREATTR) 
				{
					throw "You cannot pass more than 14 explore attributes as the 1st is prepopulated with the country code of the site.";
				}
			}
			var attributes = null;
			if(attr == null || attr.length == 0)
			{
				attributes = analyticsDataPoints.getCountryCode();  // Add country code as the first Explore attribute 
			}
			else
			{
				attributes = analyticsDataPoints.getCountryCode() + "-_-" + analyticsDataPoints.getExploreAttributes(); // Add country code as the first Explore attribute and additional attributes
			}
			cmCreateRegistrationTag(analyticsDataPoints.getMemberId(),analyticsDataPoints.getEmail(),analyticsDataPoints.getCity(),analyticsDataPoints.getState(),
									analyticsDataPoints.getPostalCode(),attributes);
		};
	   /**
	    * Calls cmCreateErrorTag for tracking application errors.
	    * @member trackError
	    * @param {AnalyticsDataPoints} Contains Analytics Data Points
		*/
		var trackError = function(analyticsDataPoints)
		{
			var attr = GetExploreAttributes(analyticsDataPoints.getExploreAttributes());
			if (attr !== null) 
			{
				if (attr.length >= NOEXPLOREATTR) 
				{
					throw "You cannot pass more than 14 explore attributes as the 1st is prepopulated with the country code of the site.";
				}
			}
			var attributes = null;
			if(attr == null || attr.length == 0)
			{
				attributes = analyticsDataPoints.getCountryCode();  // Add country code as the first Explore attribute 
			}
			else
			{
				attributes = analyticsDataPoints.getCountryCode() + "-_-" + analyticsDataPoints.getExploreAttributes(); // Add country code as the first Explore attribute and additional attributes
			}
			cmCreateErrorTag(analyticsDataPoints.getPageId(),analyticsDataPoints.getCategoryId(),attributes);
		};
		/**
		 * Calls cmCreateConversionEventTag. For tag conversion events.
		 * @param {Object} analyticsDataPoints
		 */
		var trackConversion = function(analyticsDataPoints)
		{
			var attr = GetExploreAttributes(analyticsDataPoints.getExploreAttributes());
			if (attr !== null) 
			{
				if (attr.length >= NOEXPLOREATTR) 
				{
					throw "You cannot pass more than 14 explore attributes as the 1st is prepopulated with the country code of the site.";
				}
			}
			var attributes = null;
			if(attr == null || attr.length == 0)
			{
				attributes = analyticsDataPoints.getCountryCode();  // Add country code as the first Explore attribute 
			}
			else
			{
				attributes = analyticsDataPoints.getCountryCode() + "-_-" + analyticsDataPoints.getExploreAttributes(); // Add country code as the first Explore attribute and additional attributes
			}
			cmCreateConversionEventTag(analyticsDataPoints.getEventId(),analyticsDataPoints.getActionType(),analyticsDataPoints.getCategoryId(),
													analyticsDataPoints.getPoints(), attributes);
		};
		/**
		 * Same as tracking a page element so this calls trackPageElement.
		 * @param {Object} analyticsDataPoints
		 */
		var trackFlashEvent = function(analyticsDataPoints)
		{
			var attr = GetExploreAttributes(analyticsDataPoints.getExploreAttributes());
			if (attr !== null) 
			{
				if (attr.length >= NOEXPLOREATTR) 
				{
					throw "You cannot pass more than 14 explore attributes as the 1st is prepopulated with the country code of the site.";
				}
			}
			var attributes = null;
			if(attr == null || attr.length == 0)
			{
				attributes = analyticsDataPoints.getCountryCode();  // Add country code as the first Explore attribute 
			}
			else
			{
				attributes = analyticsDataPoints.getCountryCode() + "-_-" + analyticsDataPoints.getExploreAttributes(); // Add country code as the first Explore attribute and additional attributes
			}
			trackPageElement(analyticsDataPoints);
		};
		/**
		 * Calls cmCreateManualLinkClickTag. Used for manually tagging links.
		 * @param {Object} analyticsDataPoints
		 */
		var trackFlashLink = function (analyticsDataPoints)
		{
			var attr = GetExploreAttributes(analyticsDataPoints.getExploreAttributes());
			if (attr !== null) 
			{
				if (attr.length >= NOEXPLOREATTR) 
				{
					throw "You cannot pass more than 14 explore attributes as the 1st is prepopulated with the country code of the site.";
				}
			}
			var attributes = null;
			if(attr == null || attr.length == 0)
			{
				attributes = analyticsDataPoints.getCountryCode();  // Add country code as the first Explore attribute 
			}
			else
			{
				attributes = analyticsDataPoints.getCountryCode() + "-_-" + analyticsDataPoints.getExploreAttributes(); // Add country code as the first Explore attribute and additional attributes
			}
			cmCreateManualLinkClickTag(analyticsDataPoints.getLink(),analyticsDataPoints.getLinkName(),analyticsDataPoints.getPageId(),attributes);
		};
		/**
		 * Calls cmCreateManualImpressionTag. Tags impressions. 
		 * @param {Object} analyticsDataPoints
		 */
		var trackFlashImpression = function(analyticsDataPoints)
		{
			var attr = GetExploreAttributes(analyticsDataPoints.getExploreAttributes());
			if (attr !== null) 
			{
				if (attr.length >= NOEXPLOREATTR) 
				{
					throw "You cannot pass more than 14 explore attributes as the 1st is prepopulated with the country code of the site.";
				}
			}
			var attributes = null;
			if(attr == null || attr.length == 0)
			{
				attributes = analyticsDataPoints.getCountryCode();  // Add country code as the first Explore attribute 
			}
			else
			{
				attributes = analyticsDataPoints.getCountryCode() + "-_-" + analyticsDataPoints.getExploreAttributes(); // Add country code as the first Explore attribute and additional attributes
			}
			cmCreateManualImpressionTag(analyticsDataPoints.getPageId(), analyticsDataPoints.getSitePromoTag(), 
							analyticsDataPoints.getRealEstateTag(),attributes);
		};
		/**
		 * Calls cmCreateOrderTag. Called on the confirmation page on final payment. 
		 * @param {Object} analyticsDataPoints
		 */
		var trackOrderPurchased = function(analyticsDataPoints)
		{
			var attr = GetExploreAttributes(analyticsDataPoints.getExploreAttributes());
			if (attr !== null) 
			{
				if (attr.length >= NOEXPLOREATTR) 
				{
					throw "You cannot pass more than 14 explore attributes as the 1st is prepopulated with the country code of the site.";
				}
			}
			var attributes = null;
			if(attr == null || attr.length == 0)
			{
				attributes = analyticsDataPoints.getCountryCode();  // Add country code as the first Explore attribute 
			}
			else
			{
				attributes = analyticsDataPoints.getCountryCode() + "-_-" + analyticsDataPoints.getExploreAttributes(); // Add country code as the first Explore attribute and additional attributes 
			}
			cmCreateOrderTag(analyticsDataPoints.getOrderId(),analyticsDataPoints.getOrderSubTotal(),analyticsDataPoints.getOrderShipping(),
							  analyticsDataPoints.getMemberId(), analyticsDataPoints.getCity(),analyticsDataPoints.getState(), analyticsDataPoints.getPostalCode(),
							  analyticsDataPoints.getCurrency(),attributes);
		};
		/**
		 * Coremetrics specific, calls cmDisplayShop5s. Required to be called only ONCE after the calls to tagProductItemAddtoBasket. 
		 */
		var displayShop5s = function()
		{
			cmDisplayShop5s();
		};
		/**
		 * Calls CM tag cmCreateShopAction5Tag for each item added to basket.
		 * @param {Object} analyticsDataPoints
		 */
		var trackProductItemAddtoBasket = function(analyticsDataPoints)
		{
			var attr = GetExploreAttributes(analyticsDataPoints.getExploreAttributes());
			if (attr !== null) 
			{
				if (attr.length >= NOEXPLOREATTR) 
				{
					throw "You cannot pass more than 14 explore attributes as the 1st is prepopulated with the country code of the site.";
				}
			}
			var attributes = null;
			if(attr == null || attr.length == 0)
			{
				attributes = analyticsDataPoints.getCountryCode();  // Add country code as the first Explore attribute 
			}
			else
			{
				attributes = analyticsDataPoints.getCountryCode() + "-_-" + analyticsDataPoints.getExploreAttributes(); // Add country code as the first Explore attribute and additional attributes
			}
			cmCreateShopAction5Tag(analyticsDataPoints.getProductId(),analyticsDataPoints.getProductName(),analyticsDataPoints.getQuantity(),
			                 analyticsDataPoints.getPrice(), analyticsDataPoints.getCategoryId(),analyticsDataPoints.getCurrency(),attributes);
		};
		/**
		 * CoreMetrics specific, calls cmDisplayShop9s. Required to be called only ONCE after the calls to the tagProductItemPurchased.
		 * 
		 */
		var displayShop9s = function()
		{
			cmDisplayShop9s();
		};
		/**
		 * Calls CM cmCreateShopAction9Tag for every item purchased by the customer. Called by the tagProductItemPurchased.
		 * @param {Object} analyticsDataPoints
		 */
		var trackProductItemPurchased = function(analyticsDataPoints)
		{
			var attr = GetExploreAttributes(analyticsDataPoints.getExploreAttributes());
			if (attr !== null) 
			{
				if (attr.length >= NOEXPLOREATTR) 
				{
					throw "You cannot pass more than 14 explore attributes as the 1st is prepopulated with the country code of the site.";
				}
			}
			var attributes = null;
			if(attr == null || attr.length == 0)
			{
				attributes = analyticsDataPoints.getCountryCode();  // Add country code as the first Explore attribute 
			}
			else
			{
				attributes = analyticsDataPoints.getCountryCode() + "-_-" + analyticsDataPoints.getExploreAttributes(); // Add country code as the first Explore attribute and additional attributes
			}
			cmCreateShopAction9Tag(analyticsDataPoints.getProductId(),analyticsDataPoints.getProductName(),analyticsDataPoints.getQuantity(),
							   analyticsDataPoints.getPrice(), analyticsDataPoints.getMemberId(),analyticsDataPoints.getOrderId(),
							   analyticsDataPoints.getOrderSubTotal(),analyticsDataPoints.getCategoryId(),analyticsDataPoints.getCurrency(),attributes);
		};
		
		//As part of the Revealing Module Pattern we must now expose those methods that require public access.
		return{GetClientId:GetClientId,
			   trackPageViewed:trackPageViewed,
			   trackPageElement:trackPageElement,
			   trackProductView:trackProductView,
			   trackRegistration:trackRegistration,
			   trackError:trackError,
			   trackConversion:trackConversion,
			   trackFlashEvent:trackFlashEvent,
			   trackFlashLink:trackFlashLink,
			   trackFlashImpression:trackFlashImpression,
			   trackOrderPurchased:trackOrderPurchased,
			   trackProductItemAddtoBasket:trackProductItemAddtoBasket,
			   trackProductItemPurchased:trackProductItemPurchased,
			   displayShop5s:displayShop5s,
			   displayShop9s:displayShop9s};		
	}()	;
	
   /**
    * Adidas Provider class.
    * @class Adidas namespace
    */
	var Adidas = function(production)
	{				
		var self = this;	
		
	   /**
	    * Contains Country of origin results COOResults. 
	    * Has two members site name and country code (UK, DE, etc, etc)
	    * @member COOResults
		*/
		function COOResults()
		{
			this.site = null;
			this.countryCode = null;
		}	
		

	   /**
	    * Contains Country of origin results COOResults. 
	    * Utility method for getting a request parameter from a URL.
	    * @param URL to be parsed
	    * @member GetURLParam
	    * @returns value of request parameter
	    * @link http://www.netlobo.com/url_query_string_javascript.html
		*/
		self.GetURLParam = function(name)
		{
			 name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
			 var regexS = "[\\?&]"+name+"=([^&#]*)";
			 var regex = new RegExp(regexS);
			 var results = regex.exec(window.location.href.toLowerCase());
			 if (results === null) 
			 {
			 	return "";
			 }
			 else 
			 {
			 	return results[1];
			 }
		}
		/**
		 * Gets a cookie with the specified name.
		 * @param {Object} name
		 */		
		self.GetCookie = function(name)
		{
			if (document.cookie.length > 0)
  			{
  				var cookieStart = document.cookie.indexOf(name + "=");
  				if (cookieStart != -1)
    			{
    				cookieStart = cookieStart + name.length + 1;
    				var cookieEnd = document.cookie.indexOf(";",cookieStart);
    				if (cookieEnd == -1) 
						cookieEnd = document.cookie.length;
    				return unescape(document.cookie.substring(cookieStart,cookieEnd));
    			}
  			}
			return null;
		}
		
		
	   /**
		 * Extracts country code from URL
		 * @returns {COOResults} 
		 * @member
		 * @param url 
		 * @param regexS - Regular Expressiom
		 * @param eComm - true or false
		 */
		self.determineCountryCode = function(url,regexS)
		{
			var cooResults = new COOResults();
			var regexS = regexS;
			var regex = new RegExp(regexS);
			var results = regex.exec(url.toLowerCase());
			if (results !== null) 
			{
				//Remove HTTP://
			 	results[0] = results[0].replace("http://","");
				cooResults.countryCode = results[0].slice(results[0].indexOf('/') + 1).slice(0,2) // Use Case for Url's of the format www.adidas.com/[CC]
				cooResults.countryCode = cooResults.countryCode.toUpperCase();
				return cooResults;	
			 }
			 return null;		
		}
		
			   /**
		 * Extracts country code from URL whose TLD is country specific
		 * @returns {COOResults} 
		 * @member
		 * @param url 
		 * @param regexS - Regular Expressiom
		 * @param eComm - true or false
		 */
		self.determineCountryCodeInTLD = function(url,regexS)
		{
			var cooResults = new COOResults();
			var regexS = regexS;
			var regex = new RegExp(regexS);
			var results = regex.exec(url.toLowerCase());
			if (results !== null) 
			{
				//Remove HTTP://
			 	results[0] = results[0].replace("http://","");
				cooResults.countryCode = results[0].slice(results[0].lastIndexOf('.') + 1).slice(0,2); // Use Case for Url's of the format www.adidas.[CC]/	
				cooResults.countryCode = cooResults.countryCode.toUpperCase();
				return cooResults;	
			 }
			 return null;		
		}
		
	   /**
	    * Contains business logic in determining the Country Of Origin for the site, from either the URL or cookie.
	    * @member GetCOO
	    * @returns {COOResults} Object containing country code and boolean value that dictates whether site is eComm or non-Ecommerce
		*/
		self.GetCOO = function()
		{
			var url = window.location.href.toLowerCase();
			var regexS = null;
			var cooResults = null;
	
			/*
			 * adidas.com homepage Use Cases
			 */
			regexS = "^htt(p|ps)://(origin.|)(development|staging|www).adidas.com/[a-zA-Z]{2}";
			//url = "http://development.adidas.com/pl/hjhgkjhhjk/?kjjl=345/jhjhg.kjhjkh/jkhhjk.asp";
			cooResults = self.determineCountryCode(url, regexS);
			if(cooResults !== null)
			{
				cooResults.site = BRANDSITES;
				// Set the Cookie domain
				cm_JSFPCookieDomain = "adidas.com";
				return cooResults
			}
			
			/*
			 * micoach use case
			 */
			regexS = "^htt(p|ps)://(origin.|)(development|staging|www).adidas.com/[a-zA-Z]{2}/micoach";
			//url = "http://development.adidas.com/de/micoach/jklklkl.jkhkjk/?kjhhk=23/hhjk.asp";
			cooResults = self.determineCountryCode(url, regexS);
			if(cooResults !== null)
			{
				cooResults.site = MICOACH;
				// Set the Cookie domain
				cm_JSFPCookieDomain = "adidas.com";
				return cooResults
			}
			/*
			 * miadidas/miteam Use Case
			 */ 
			regexS = "^htt(p|ps)://(origin.|)(development|staging|www|checkout).miadidas.com/[a-zA-Z]{2}";
			//url = "http://checkout.miadidas.com/de/jkhhjk.asp";
			cooResults = self.determineCountryCode(url, regexS);
			if(cooResults !== null)
			{
				cooResults.site = MIADIDAS;
				// Set the Cookie domain
				cm_JSFPCookieDomain = "miadidas.com";
				return cooResults;
			}
			
			regexS = "^htt(p|ps)://(origin.|)(development|staging|www).miteam.com/[a-zA-Z]{2}";
			//url = "http://www.miteam.com/Ue/jhghjjh/?jhgjgh=23";
			cooResults = self.determineCountryCode(url, regexS);
			if(cooResults !== null)
			{
				cooResults.site = MITEAM;
				// Set the Cookie domain
				cm_JSFPCookieDomain = "miteam.com";
				return cooResults;
			}
			
			 
			/*
			 * PID Use Case
			 */  
			regexS = "^htt(p|ps)://(origin.|)(staging.catalogue|catalogue).adidas.([a-zA-Z]{2}|[a-zA-Z]{2}.[a-zA-Z]{2})";
			//url = "https://origin.catalogue.adidas.pl/catalogue/pl/products/men/shoes.asp?color=grey";
			cooResults = self.determineCountryCodeInTLD(url, regexS);
			if(cooResults !== null)
			{
				cooResults.site = PID;
				// Set the Cookie domain
				if (cooResults.countryCode.toUpperCase() === "NZ") 
				{
					cm_JSFPCookieDomain = "adidas.co." + cooResults.countryCode;
				}
				else 
				{
					cm_JSFPCookieDomain = "adidas." + cooResults.countryCode;
				}
				return cooResults;
			}
			 
			/*
			 * adidas TV Use Case
			 */ 
			regexS = "^htt(p|ps)://(origin.|)(development|staging|www).adidas.tv";
			//url = "http://origin.www.adidas.tv/hjfhgfhgfhg/kjggjh";
			var regex = new RegExp(regexS);
			var results = regex.exec(url);
			 if (results !== null) 
			 {
			 	cooResults = new COOResults();
			 	cooResults.site = ADIDASTV;
				// Set the Cookie domain
				cm_JSFPCookieDomain = "adidas.tv";
			 	cooResults.countryCode = self.GetCookie("atvCountry");
				return cooResults;			
			 }
						
			   
			/*
			 * Originals Use Case 
			 */ 
			regexS = "^htt(p|ps)://(origin.|)(development|staging|www).adidas.com/originals";
			//url = "http://www.adidas.com/originals/khkjhkjhkj/kljlj.asdkjl/?kjkjh=7678";
			var regex = new RegExp(regexS);
			var results = regex.exec(url);
			 if (results !== null) 
			 {
			 	cooResults = new COOResults();
			 	cooResults.site = ORIGINALS;
				// Set the Cookie domain
				cm_JSFPCookieDomain = "adidas.com";
			 	cooResults.countryCode = self.GetCookie("country");
				return cooResults;			
			 }
			 
			
			/*
			 * REMEA/ROW eComm Use Cases
			 *
			 */
			 regexS = "^htt(p|ps)://(origin.|)((dev|stg).shop|shop).(adidas.[a-zA-Z]{2}/|adidas.co.uk)";
			 //url = "http://shop.adidas.fr/analytics/adidas/?kjhkjhjk=876876/v10/adidasAnalytics.js";
			 cooResults = self.determineCountryCodeInTLD(url, regexS);
			 if(cooResults !== null)
			 {
			 	cooResults.site = ROWESHOP;
				// Set the Cookie domain
				if (cooResults.countryCode.toUpperCase() === "UK") 
				{
					cm_JSFPCookieDomain = "adidas.co." + cooResults.countryCode;
				}
				else 
				{
					cm_JSFPCookieDomain = "adidas." + cooResults.countryCode;
				}
				return cooResults;
			 }
			 // Adidas Canada
			 regexS = "^htt(p|ps)://(origin.|)((dev|stg|www).shopadidas|shopadidas).ca/";
			 //url = "http://origin.www.shopadidas.ca/Ue/jhghjjh/jkhjkkj.asp?jhgjgh=23";
			 cooResults = self.determineCountryCodeInTLD(url, regexS);
			 if(cooResults !== null)
			 {
			 	cooResults.site = ROWESHOP;
				cm_JSFPCookieDomain = "shopadidas." + cooResults.countryCode;
				return cooResults;
			 }
			/*
			 * adidas.com microsites
			 */
		
			if (cooResults !== null && cooResults.site === BRANDSITES) 
			{
				var countryOfOrigin = self.GetURLParam("strcountry_adidascom");
				if (!(countryOfOrigin === "")) 
					cooResults.countryCode = countryOfOrigin;
				
				countryOfOrigin = self.GetURLParam("adidas_cc");
				if (!(countryOfOrigin === "")) 
					cooResults.countryCode = countryOfOrigin;
				
				
				countryOfOrigin = self.GetURLParam("strcountry_branch");
				if (!(countryOfOrigin === "")) 
					cooResults.countryCode = countryOfOrigin;
				
				countryOfOrigin = self.GetURLParam("cc");
				if (!(countryOfOrigin === "")) 
					cooResults.countryCode = countryOfOrigin;
				
				countryOfOrigin = self.GetURLParam("vcountry");
				if (!(countryOfOrigin === "")) 
					cooResults.countryCode = countryOfOrigin;
				
				countryOfOrigin = self.GetURLParam("country");
				if (!(countryOfOrigin === "")) 
					cooResults.countryCode = countryOfOrigin;
				cooResults.site = BRANDSITES;
				if (cooResults.countryCode !== null) 
					cooResults.countryCode = cooResults.countryCode.toUpperCase();
			}
			else
			{
				cooResults = new COOResults();
				cooResults.site = "UNDEFINED";
				cooResults.countryCode = "COM";
			}
			return cooResults;			
		}
		
	   /**
	    * Contains publically exposed methods for tagging pages. 
	    * @class Analytics
		*/			
		self.Analytics = function()
		{
			var self = this;
			/* -- References to the internal Classes to the Analytics class */

			var _prePendCC = true;				
			/**
	  		 * Business object containing Analytics Data Points.
			 * @class AnalyticsDataPoints
			 */		
			self.AnalyticsDataPoints = function() 
			{
				
				var _countryCode = null; // Country code
				var _domainName = null; // Current domain 
				var _pageId = null; // Page Identifier
				var _elementId = null ; // Page Element
				var _categoryId = null; // Category Identifier
				var _memberId = null; // Registered Member Identifier
				var _email = null; // Customer email address
				var _city = null; //Customer city
				var _state = null // Customer state
				var _postalCode = null // Postal code
				var _newsLetter = null // Newsletter 
				var _subscribed = null // Subscribed	
				var _eventId = null // Event Id			
				var _orderId = null; // Order Identifier
				var _productId = null; // Product Identifier
				var _productName = null; //Product Name
				var _productPrice = +(0.0); // Product line item price
				var _orderQuantity = +(0); // Product line quantity 
				var _orderSubTotal = +(0.00); // Order Subtotal
				var _orderShipping = +(0.00); // Order Shipping
				var _orderTax = +(0.0); // Product item tax 
				var _searchKeyword = null; // Search keywords, comma delimited 
				var _totalNumOfSearchResults = +(0.00); // Total number of search results
				var _actionID = null; // Generic analytics event identifier
				var _actionType = null; // Generic analytics event identifier
				var _conversionPoints = +(0); // Points per conversion 
				var _href = null; // href
				var _linkName = null; // Link name
				var _realEstateTag = null; // Real Estate tag
				var _sitePromotionTag = null; // Site Promotion tag
				var _exploreAttributes = null ; // Explore attributes
				var _currency = null; // ISO Currency type - Refer to Coremetrics document ReleaseNotes_Multi_Currency 
		
				var setDomain = function(domainName)
				{
					_domainName = domainName;
				};
		
				var getDomain = function()
				{
					return _domainName;
				};
				
				var setPageId = function(pageId)
				{
					_pageId = pageId;
				};
	
				var getPageId = function()
				{
					return _pageId;
				};
	
				var setElementId = function(elementId)
				{
					_elementId = elementId;
				};
	
				var getElementId = function()
				{
					return _elementId;
				};
				
				var setCategoryId = function(categoryId)
				{
					_categoryId = categoryId;
				};
	
				var getCategoryId = function()
				{
					return _categoryId;
				};

				var setMemberId = function(memberId)
				{
					_memberId = memberId;
				};

				var getMemberId = function()
				{
					return _memberId;
				};
				
				var setEmail = function(email)
				{
					_email = email;
				};

				var getEmail = function()
				{
					return _email;
				};
				
				var setCity = function(city)
				{
					_city = city;			
				};

				var getCity = function()
				{
					return _city;
				};

				var setState = function(state)
				{
					_state = state;
				};
	
				var getState = function()
				{
					return _state;
				};
				var setPostalCode = function(postalCode)
				{
					_postalCode = postalCode;
				};				
				var getPostalCode = function()
				{
					return _postalCode;
				};
				
				var setNewsLetter = function(newsLetter)
				{
					_newsLetter = newsLetter;
				};
				var getNewsLetter = function()
				{
					return _newsLetter;
				};
				
				var setSubscribed = function(subscribed)
				{
					_subscribed = subscribed;
				};
				var getSubscribed = function()
				{
					return _subscribed;
				};
				
				var setEventId = function(eventId)
				{
					_eventId = eventId;
				};
				var getEventId = function()
				{				
					return _eventId;
				}
							
				var setOrderId = function(orderId)
				{
					_orderId = orderId;
				};
				
				var getOrderId = function()
				{
					return _orderId;
				};

				var setProductId = function(productId)
				{
					_productId = productId;
				};

				var getProductId = function(){
					return _productId;
				};

				var setProductName = function(productName)
				{
					_productName = productName;
				};
				
				var getProductName = function()
				{
					return _productName;
				};

				var setPrice = function(productPrice)
				{
					if (!isNaN(productPrice)) 
					{
						_productPrice = productPrice;
					}
					else 
					{
						throw "The product price set is not a valid number";
					}
				};

				var getPrice = function()
				{
					return _productPrice;
				};

				var setQuantity = function(orderQuantity) 
				{ 
					if (!isNaN(orderQuantity)) 
					{
						_orderQuantity = orderQuantity;
					}
					else 
					{
						throw "The quantity set is not a valid number";
					}					
				};

				var getQuantity = function()
				{
					return _orderQuantity;
				};

				var setOrderSubTotal = function(orderSubTotal) 
				{ 
					if (!isNaN(orderSubTotal)) 
					{
						_orderSubTotal = orderSubTotal;
					}
					else
					{
						throw "The order subtotal set is not a valid number.";				
					}
				};

				var getOrderSubTotal = function()
				{
					return _orderSubTotal;
				};
				
				var setOrderShipping = function(orderShipping)
				{
					if (!isNaN(orderShipping))
					{
						_orderShipping = orderShipping;
					} 
					else
					{
						throw "The order shipping value is not a valid number";
					}
				};
				
				var getOrderShipping = function()
				{
					return _orderShipping;
				};

				var setTax = function(orderTax) 
				{ 
					if (!isNaN(orderTax)) 
					{
						_orderTax = orderTax;
					}
					else 
					{
						throw "The order tax set is not a valid number";
					}
						
				};
				var getTax = function()
				{
					return _orderTax;
				};		

				var setSearchTerm = function(searchKeyword)
				{
					_searchKeyword = searchKeyword;				
				};		
				var getSearchTerm = function()
				{
					return _searchKeyword;
				};

				var setTotalNumOfSearchResults = function(totalNumOfSearchResults)
				{
					_totalNumOfSearchResults = totalNumOfSearchResults;
				};
				var getTotalNumOfSearchResults = function()
				{
					return _totalNumOfSearchResults;
				};

				var setActionId = function(actionId)
				{
					_actionId = actionId;
				};
				var getActionId = function()
				{
					return _actionId;
				};

				var setActionType = function(actionType)
				{
					_actionType = actionType;
				};
				var getActionType = function()
				{
					return _actionType;	
				};

				var setPoints = function(conversionPoints)
				{
					if (!isNaN(conversionPoints)) 
					{
						_conversionPoints = conversionPoints;
					}
					else
					{
						throw "The number passed for Points is invalid";
					}
					 
				};
				var getPoints = function()
				{
					return _conversionPoints;
				};
				
				var setLink = function(href)
				{
					_href = href;
				};
				var getLink = function()
				{
					return _href;
				};
				
				var setLinkName = function(name)
				{
					_linkName = name
				};
				var getLinkName = function()
				{
					return _linkName;	
				};
				
				var setRealEstateTag = function(realEstateTag)
				{
					_realEstateTag = realEstateTag; 
				};
				var getRealEstateTag = function()
				{
					return _realEstateTag;
				};
				
				var setSitePromoTag = function(promoTag)
				{
					_sitePromotionTag = promoTag;
				};
				
				var getSitePromoTag = function()
				{
					return _sitePromotionTag;
				};
				
				var setExploreAttributes = function(explore)
				{
					_exploreAttributes = explore;
					
				};
				var getExploreAttributes = function()
				{
					return _exploreAttributes;
				};
				
				var setCountryCode = function(countryCode)
				{
					_countryCode = countryCode;
					
				};
				var getCountryCode = function()
				{
					return _countryCode;
				};
				
				var setCurrency = function(currency)
				{
					_currency  = currency;	
				};
				
				var getCurrency = function()
				{
					return _currency;
				};
				
				//As part of the Revealing Module Pattern we must now expose those methods that require public access.
				return {setDomain:setDomain,
							getDomain:getDomain,
							setPageId:setPageId,
							getPageId:getPageId,
							setElementId:setElementId,
							getElementId:getElementId,
							setCategoryId:setCategoryId,
							getCategoryId:getCategoryId,
							setMemberId:setMemberId,
							getMemberId:getMemberId,
							getEmail:getEmail,
							setEmail:setEmail,
							setCity:setCity,
							getCity:getCity,
							setState:setState,
							getState:getState,
							setPostalCode:setPostalCode,
							getPostalCode:getPostalCode,
							setNewsLetter:setNewsLetter,
							getNewsLetter:getNewsLetter,
							setSubscribed:setSubscribed,
							getSubscribed:getSubscribed,
							setOrderId:setOrderId,
							getOrderId:getOrderId,
							setProductId:setProductId,
							getProductId:getProductId,
							setProductName:setProductName,
							getProductName:getProductName,
							setPrice:setPrice,
							getPrice:getPrice,
							setQuantity:setQuantity,
							getQuantity:getQuantity,
							setOrderSubTotal:setOrderSubTotal,
							getOrderSubTotal:getOrderSubTotal,
							setOrderShipping:setOrderShipping,
							getOrderShipping:getOrderShipping,
							setTax:setTax,
							getTax:getTax,
							setSearchTerm:setSearchTerm,
							getSearchTerm:getSearchTerm,
							setTotalNumOfSearchResults:setTotalNumOfSearchResults,
							getTotalNumOfSearchResults:getTotalNumOfSearchResults,
							setEventId:setEventId,
							getEventId:getEventId,
							setActionId:setActionId,
							getActionId:getActionId,
							setActionType:setActionType,
							getActionType:getActionType,
							setPoints:setPoints,
							getPoints:getPoints,
							setLink:setLink,
							getLink:getLink,
							setLinkName:setLinkName,
							getLinkName:getLinkName,
							setRealEstateTag:setRealEstateTag,
							getRealEstateTag:getRealEstateTag,
							setSitePromoTag:setSitePromoTag,
							getSitePromoTag:getSitePromoTag,
							setExploreAttributes:setExploreAttributes,
							getExploreAttributes:getExploreAttributes,
							setCountryCode:setCountryCode,
							getCountryCode:getCountryCode,
							setCurrency:setCurrency,
							getCurrency:getCurrency};							
			}();
			/**
			 * Public method for tagging page views
			 * @param {Object} pageId Unique Page Identifier
			 * @param {Object} pageCategoryId Category of the page, these categories are defined in the CDF.
			 * @param {Object} searchTerm Search keywords used to find the page, should only be set on the first results page, all other pages it should be set to null.
			 * @param {Object} searchResults Number of search results, set on the first results page.
			 * @param {Object} attributes CM Explore attributes
			 */			
			var tagPageView = function(pageId, pageCategoryId, searchTerm, searchResults, attributes)
			{
				self.AnalyticsDataPoints.setCategoryId(pageCategoryId);
				self.AnalyticsDataPoints.setPageId(((_prePendCC ? cooResults.countryCode + ":" : "") 	+ pageId));
				self.AnalyticsDataPoints.setSearchTerm(searchTerm);
				self.AnalyticsDataPoints.setTotalNumOfSearchResults(searchResults);
				self.AnalyticsDataPoints.setExploreAttributes(attributes);
				self.AnalyticsDataPoints.setCountryCode(cooResults.countryCode);
					
				CoreMetricsProvider.trackPageViewed(self.AnalyticsDataPoints);
			};
			/**
			 * Tag a page element, useful for tracking AJAX application states
			 * @param {Object} elementId Unique Page Element Id
			 * @param {Object} elementCategoryId Associated category (self-contained, has nothing to do with the categories defined in the CDF) .
			 * @param {Object} attributes  CM Explore attributes
			 */
			var tagPageElement = function(elementId,elementCategoryId,attributes)
			{
				self.AnalyticsDataPoints.setCategoryId(elementCategoryId);
				self.AnalyticsDataPoints.setElementId(elementId);
				self.AnalyticsDataPoints.setExploreAttributes(attributes);
				self.AnalyticsDataPoints.setCountryCode(cooResults.countryCode);
				
				CoreMetricsProvider.trackPageElement(self.AnalyticsDataPoints);
			};
			/**
			 * Tag a product page
			 * @param {Object} productId. In the case of adidas this would be the article Id
			 * @param {Object} productName 
			 * @param {Object} productCategoryId. Defined in the CDF.
			 * @param {Object} attributes CM Explore Attributes.
			 */
			var tagProductView = function(productId, productName, productCategoryId, attributes)
			{
				self.AnalyticsDataPoints.setCategoryId(productCategoryId);
				self.AnalyticsDataPoints.setProductId(productId);
				self.AnalyticsDataPoints.setProductName(productName);
				self.AnalyticsDataPoints.setExploreAttributes(attributes);
				self.AnalyticsDataPoints.setCountryCode(cooResults.countryCode);
				
				CoreMetricsProvider.trackProductView(self.AnalyticsDataPoints);
			};
			/**
			 * Tag a User Registration event
			 * @param {Object} customerId Unique Customer Id
			 * @param {Object} email Customer email
			 * @param {Object} city Customer City
			 * @param {Object} state Customer state/region
			 * @param {Object} postalCode Customer postal code 
			 * @param {Object} attributes CM Explore attributes
			 */
			var tagRegistration = function(customerId, email, city, state, postalCode, attributes)
			{
				self.AnalyticsDataPoints.setMemberId(customerId);
				self.AnalyticsDataPoints.setEmail(email);
				self.AnalyticsDataPoints.setCity(city);
				self.AnalyticsDataPoints.setState(state);
				self.AnalyticsDataPoints.setPostalCode(postalCode);
				self.AnalyticsDataPoints.setExploreAttributes(attributes);
				self.AnalyticsDataPoints.setCountryCode(cooResults.countryCode);
				
				CoreMetricsProvider.trackRegistration(self.AnalyticsDataPoints);
			};
			/**
			 * Tag an application error.
			 * @param {Object} pageId Unique Page Id
			 * @param {Object} pageCategoryId Category Id for page defined in the CDF
			 * @param {Object} attributes CM Explore attributes
			 */
			var tagError = function(pageId, pageCategoryId, attributes)
			{
				self.AnalyticsDataPoints.setPageId(pageId);
				self.AnalyticsDataPoints.setCategoryId(pageCategoryId);
				self.AnalyticsDataPoints.setExploreAttributes(attributes);
				self.AnalyticsDataPoints.setCountryCode(cooResults.countryCode);
				
				CoreMetricsProvider.trackError(self.AnalyticsDataPoints);
			};
			/**
			 * Tag a conversion event
			 * @param {Object} eventId A unique id for the event 
			 * @param {Object} actionType A value of 1(Initiation) or 2(Success). Single step conversion should be set as 2.
			 * @param {Object} eventCategoryId Self contained category Id.
			 * @param {Object} points Points associated toa successful conversion
			 * @param {Object} attributes CM Explore attributes
			 */
			var tagConversionEvent = function(eventId, actionType, eventCategoryId, points, attributes)
			{
				self.AnalyticsDataPoints.setCategoryId(eventCategoryId);
				self.AnalyticsDataPoints.setEventId(eventId);
				self.AnalyticsDataPoints.setActionType(actionType);
				self.AnalyticsDataPoints.setPoints(points);
				self.AnalyticsDataPoints.setExploreAttributes(attributes);
				self.AnalyticsDataPoints.setCountryCode(cooResults.countryCode);
				
				CoreMetricsProvider.trackConversion(self.AnalyticsDataPoints);
			};
			/**
			 * Flash events are captured as Page Element events. This acts as a wrapper to the tagPageElement function.
			 * @param {Object} flashEventCategory Self-contained category name for the Flash event
			 * @param {Object} flashEvent Flash event Id
			 * @param {Object} attributes CM Explore attributes
			 */
			var tagFlashEvent = function(flashEventCategory, flashEvent, attributes)
			{
				tagPageElement(flashEventCategory,flashEvent, attributes);
			};
			/**
			 * Tag a apge link. Usually used for Flash as HTML anchors are automatically tagged by CoreMetrics core library.
			 * @param {Object} href URL for link
			 * @param {Object} name Name of link.
			 * @param {Object} pageId Unique Page Id
			 * @param {Object} attributes CM Explore attributes
			 */
			var tagLink = function(href,name,pageId, attributes)
			{
				self.AnalyticsDataPoints.setLink(href);
				self.AnalyticsDataPoints.setLinkName(name);
				self.AnalyticsDataPoints.setPageId(pageId);
				self.AnalyticsDataPoints.setExploreAttributes(attributes);
				self.AnalyticsDataPoints.setCountryCode(cooResults.countryCode);
				
				CoreMetricsProvider.trackFlashLink(self.AnalyticsDataPoints);
			};
			/**
			 * Tag an Impression event.
			 * @param {Object} pageId Unique Page identifier
			 * @param {Object} sitePromoId Site promotion Id 
			 * @param {Object} realEstateId Real estate Id
			 * @param {Object} attributes CM Explore attributes
			 */
			var tagImpression = function(pageId, sitePromoId, realEstateId, attributes)
			{
				self.AnalyticsDataPoints.setPageId(pageId);
				self.AnalyticsDataPoints.setSitePromoTag(sitePromoId);
				self.AnalyticsDataPoints.setRealEstateTag(realEstateId);
				self.AnalyticsDataPoints.setExploreAttributes(attributes);
				self.AnalyticsDataPoints.setCountryCode(cooResults.countryCode);
				
				CoreMetricsProvider.trackFlashImpression(self.AnalyticsDataPoints, attributes);
			};
			/**
			 * Tag a order purchase
			 * @param {Object} orderId Unique Order Id
			 * @param {Object} orderSubTotal Order Subtotal
			 * @param {Object} orderShipping Order shipping cost 
			 * @param {Object} customerId Unique Customer Id
			 * @param {Object} city Customer City
			 * @param {Object} state Customer state/region
			 * @param {Object} postalCode Customer Postal Code
			 * @param {Object} currency ISO Currency Type 
			 * @param {Object} attributes CM Explore attributes
			 */
			var tagOrderPurchased = function(orderId, orderSubTotal, orderShipping, customerId, city, state, postalCode, currency, attributes)
			{
				self.AnalyticsDataPoints.setOrderId(orderId);
				self.AnalyticsDataPoints.setOrderSubTotal(orderSubTotal);
				self.AnalyticsDataPoints.setOrderShipping(orderShipping);
				self.AnalyticsDataPoints.setMemberId(customerId);
				self.AnalyticsDataPoints.setCity(city);
				self.AnalyticsDataPoints.setState(state);
				self.AnalyticsDataPoints.setPostalCode(postalCode);
				self.AnalyticsDataPoints.setCurrency(currency);
				self.AnalyticsDataPoints.setExploreAttributes(attributes);
				self.AnalyticsDataPoints.setCountryCode(cooResults.countryCode);
				
				CoreMetricsProvider.trackOrderPurchased(self.AnalyticsDataPoints);
			};
			/**
			 * Commit all tagProductItemAddTobasketTag. Must be called ONLY ONCE, this is a wrapper function for the CM function displayShop5s().
			 */
			var commitProductItemAddToBasketTags = function()
			{
				CoreMetricsProvider.displayShop5s();
			};
			var tagProductItemAddToBasket = function(productId,productName,quantity,unitPrice,categoryId, currency, attributes)
			{
				self.AnalyticsDataPoints.setCategoryId(categoryId);
				self.AnalyticsDataPoints.setProductId(productId);
				self.AnalyticsDataPoints.setProductName(productName);
				self.AnalyticsDataPoints.setQuantity(quantity);
				self.AnalyticsDataPoints.setPrice(unitPrice);
				self.AnalyticsDataPoints.setCurrency(currency);	
				self.AnalyticsDataPoints.setExploreAttributes(attributes);
				self.AnalyticsDataPoints.setCountryCode(cooResults.countryCode);
				
				CoreMetricsProvider.trackProductItemAddtoBasket(self.AnalyticsDataPoints);
			};
			/**
			 * Commit all tagProductItemPurchasedTag. Must be called ONLY ONCE, this is a wrapper function for the CM function displayShop9s().
			 */
			var commitProductItemPurchasedTags = function()
			{
				CoreMetricsProvider.displayShop9s();
			};
			/**
			 * Tag a Prooduct Item purchased
			 * @param {Object} productId Unique Product Id. Usually the Article Id
			 * @param {Object} productName Product Name
			 * @param {Object} quantity Quantity purchased
			 * @param {Object} unitPrice Product price
			 * @param {Object} customerId Unique customer Id
			 * @param {Object} orderId Unique Order Id
			 * @param {Object} orderSubTotal Order subtotal 
			 * @param {Object} categoryId Category Id defined in the CDF
			 * @param {Object} attributes CM Explore attributes
			 */
			var tagProductItemPurchased = function(productId,productName,quantity,unitPrice,customerId,orderId,orderSubTotal,categoryId,currency, attributes)
			{
				self.AnalyticsDataPoints.setOrderId(orderId);
				self.AnalyticsDataPoints.setMemberId(customerId);
				self.AnalyticsDataPoints.setCategoryId(categoryId);
				self.AnalyticsDataPoints.setProductId(productId);
				self.AnalyticsDataPoints.setProductName(productName);
				self.AnalyticsDataPoints.setQuantity(quantity);				
				self.AnalyticsDataPoints.setPrice(unitPrice);
				self.AnalyticsDataPoints.setOrderSubTotal(orderSubTotal);
				self.AnalyticsDataPoints.setCurrency(currency);
				self.AnalyticsDataPoints.setExploreAttributes(attributes);
				self.AnalyticsDataPoints.setCountryCode(cooResults.countryCode);
				
				CoreMetricsProvider.trackProductItemPurchased(self.AnalyticsDataPoints);				
			};
			
			var prePendCountryCode = function(prePend)
			{
				_prePendCC = prePend;
			};	
			//As part of the Revealing Module Pattern we must now expose those methods that require public access.
			return {tagPageView:tagPageView,
					tagPageElement:tagPageElement,
					tagProductView:tagProductView,
					tagRegistration:tagRegistration,
					tagError:tagError,
					tagConversionEvent:tagConversionEvent,
					tagFlashEvent:tagFlashEvent,
					tagLink:tagLink,
					tagImpression:tagImpression,
					tagOrderPurchased:tagOrderPurchased,
					tagProductItemAddToBasket:tagProductItemAddToBasket,
					tagProductItemPurchased:tagProductItemPurchased,
					commitProductItemAddToBasketTags:commitProductItemAddToBasketTags,
					commitProductItemPurchasedTags:commitProductItemPurchasedTags,
					prePendCountryCode:prePendCountryCode};
		}();
	/**********************************************************  This code is executed on instantiation of the Adidas object ************************************************************************************************/			
			if(production === null || production) // Track to CM Production environment
			{
				cmSetProduction();	
			}
		
			// Set the Coremetrics ClientID
			cooResults = self.GetCOO();		
			// Set the clientId
			cm_ClientID = CoreMetricsProvider.GetClientId(cooResults.countryCode,cooResults.site);
			if (self.GetURLParam('debugme') == '1') 
			{
				var msg = 'adidas Tracking library\n     Version ' + VERSION + '\n\n';
				msg += 'Raw URL: ' + window.location.href + '\n';
				msg += 'Main domain: ' + cm_JSFPCookieDomain  + '\n';;
				msg += 'Site: ' + cooResults.site + '\n';
				msg += 'Country code: ' + cooResults.countryCode + '\n';
				msg += "Client Id's: " + cm_ClientID + '\n'; 
				alert(msg);
			}
	/***********************************************************************************************************************************************************************************************************************/		
	}

	 

	 
	 
