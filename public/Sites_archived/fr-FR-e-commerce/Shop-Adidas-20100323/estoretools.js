var hiddenConfirmOrder=0;
var hiddenPaymentContinue=0;
var hiddenPaymentError=0;
var hiddenPaymentAVSError=0;
var hiddenPaymentCancel=0;

function setDisableAddToCart(disableFlag,language)
{
	if (disableFlag)
	{
		$("span[id$='addToCartForm']").html('<img src="http://shop.adidas.fr/images/new/'+language+'/bt-addCart-disabled.gif"/>');
	}
	else
	{
	}	
}
function refreshFormButton()
{
	var component = document.getElementById('productDetailForm:refreshFormButton');
	if (component)
	{
		component.onclick();
	}
}

var oldColorName, oldArticleId;

function changeColorName(colorName,articleID,articleLabel)
{

	oldColorName=document.getElementById('ColorName').innerHTML;
	document.getElementById('ColorName').innerHTML='<strong>'+colorName+'</strong>';

	oldArticleId=document.getElementById('ArticleId').innerHTML; 
	document.getElementById('ArticleId').innerHTML=document.getElementById(articleLabel).innerHTML +' '+articleID;
	
}

function resetColorName()
{
	document.getElementById('ColorName').innerHTML=oldColorName;
	document.getElementById('ArticleId').innerHTML=oldArticleId;
}
function initMenuHovering()
{
	$(".nav").superfish({
		animation : { height:"show" }
		});

}

function setThumbsExpanding(productCode)
{

	$('#'+productCode+'ThumbsImage').toggle(
 		function () {
		$('#'+productCode+'ThumbsExpand').slideDown();
		document.getElementById(productCode+'ThumbsImage').src ="images/new/button.gif";
	  },
	  function () {
		$('#'+productCode+'ThumbsExpand').slideUp();
		document.getElementById(productCode+'ThumbsImage').src ="images/new/buttonPlus.gif";
	  }
);	
}

function changeProductImage(productCode,newImageSrc,variantCode,productPrice,productReducedPrice)
{
	//var formatedPrice = parseFloat(productPrice).toFixed(2);
	if(productReducedPrice){
		var priceOldComponent = document.getElementById(productCode+'PriceOld');
		var priceNewComponent = document.getElementById(productCode+'PriceNew');
		priceOldComponent.innerHTML = productPrice;
		priceNewComponent.innerHTML = productReducedPrice;
	} else {
		var priceComponent = document.getElementById(productCode+'Price');
		priceComponent.innerHTML = productPrice;
	}
	document.getElementById(productCode+'Image').src=newImageSrc;
	document.getElementById(productCode+'SelectedVariant').value=variantCode;

}

function setMainMenuHover(menuCode,language)
{
		//$("input[name$='letter']").val("a letter");
		//alert(menuCode+language);
		$("li[id$='"+menuCode+"Link']").hoverIntent({
			sensitivity: 1, // number = sensitivity threshold (must be 1 or higher)
			interval: 0,   // number = milliseconds of polling interval
			over: 
			function () {
			document.getElementById(menuCode+'MenuImage').src = '/images/new/mainMenu/'+language+'/'+menuCode+'_over.gif';
	  	}
		,  // function = onMouseOver callback (required)
		timeout: 300,   // number = milliseconds delay before onMouseOut function call
		out: 
		  function () {
		  document.getElementById(menuCode+'MenuImage').src = '/images/new/mainMenu/'+language+'/'+menuCode+'.gif';
	  }  // function = onMouseOut callback (required)
	 });      
}

function initProductDetailExpanding() {

$('div.selection-container:eq(0)> div:gt(0)').hide(); 

  $('div.selection-container:eq(0)> h3').click(function() 
  {

    $(this).next('div:hidden').slideDown('fast').siblings('div:visible').slideUp('fast');
	$(this).removeClass("selection-header-plus").siblings().addClass("selection-header-plus");
		
  });

}

function getSelectedQuantity()
{
	var component = document.getElementById('productDetailForm:selectedQuantity');
	if (component)
	{
		var selectedQuantity=component.value;
		return selectedQuantity;
	}
}
function getSelectedSize()
{
	var component = document.getElementById('productDetailForm:selectedSize');
	if (component)
	{
		var selectedSizePk=component.value;
		return selectedSizePk;
	}
}

function getSelectedEntryQuantity(entryNumber)
{
	return $("input[id$='"+entryNumber+"Quantity']").val();
}
function trackLinksHitbox(lid, lpos)
{
	_hbLink(formatHitboxName(lid), formatHitboxName(lpos));
}
function trackBannerLinksHitbox(lid, lpos)
{
	_hbLink(formatHitboxFileName(lid), formatHitboxName(lpos));	
}
function trackProductViewLinksHitbox(mediaBeanUrl, linkId, modelId, modelName, lpos, productCode)
{
	var articleId = document.getElementById(productCode+'Image').src;
	articleId = articleId.replace(mediaBeanUrl,'');
	var firstIndex = articleId.indexOf('_');
	articleId = articleId.substring(0,firstIndex);
	var lid = articleId + "-" + modelId + "-" + modelName; 
	
	_hbLink(formatHitboxName(lid), formatHitboxName(lpos));
}
function formatHitboxFileName(fileNamePath)
{
	var indexOfDot = fileNamePath.lastIndexOf('.');
	var fileName = fileNamePath.substring(0,indexOfDot)
	
	return formatHitboxName(fileName);
}
function formatHitboxName(name)
{
	//trim where white spaces and make the first letter Upper case
	var words = name.split(' ');
	var newName;
	
	for (i = words.length - 1; i >= 0; i--) 
	{
		words[i] = words[i].substr(0,1).toUpperCase() +  words[i].substr(1,words[i].length);
	}
	newName = words.join('');
	
	//trim where /signs (for color names)
	var colorWords = newName.split('/');
	for (i = colorWords.length - 1; i >= 0; i--) 
	{
		colorWords[i] = colorWords[i].substr(0,1).toUpperCase() +  colorWords[i].substr(1,colorWords[i].length);
	}
	newName = colorWords.join('_');
		
	//replace special characters
	newName = newName.replace('&','And');
	//remove whitespaces
	var whiteSpaceRegex = new RegExp('/\s+/g','g');
	newName = newName.replace(whiteSpaceRegex,'');
	//remove non alpha characters
	var alphaRegex = new RegExp('[^a-zA-Z0-9\-\_]','g');
	newName = newName.replace(alphaRegex,'');	
	//alert('changed from \n' + name + '\n to \n' + newName);
	return newName;
}
function formatHitboxCommaDelimitedNames(name)
{
	//trim where white spaces and make the first letter Upper case
	var words = name.split(' ');
	var newName;
	
	for (i = words.length - 1; i >= 0; i--) 
	{
		words[i] = words[i].substr(0,1).toUpperCase() +  words[i].substr(1,words[i].length);
	}
	newName = words.join('');
	
	//trim where /signs (for color names)
	var colorWords = newName.split('/');
	for (i = colorWords.length - 1; i >= 0; i--) 
	{
		colorWords[i] = colorWords[i].substr(0,1).toUpperCase() +  colorWords[i].substr(1,colorWords[i].length);
	}
	newName = colorWords.join('_');
		
	//replace and signs
	newName = newName.replace('&','And');
	//remove whitespaces
	var whiteSpaceRegex = new RegExp('/\s+/g','g');
	newName = newName.replace(whiteSpaceRegex,'');
	//remove non alpha characters
	var alphaRegex = new RegExp('[^a-zA-Z0-9\,\-\_]','g');
	newName = newName.replace(alphaRegex,'');	
	//alert('CM value changed from \n' + name + '\n to \n' + newName);
	return newName;
}

function insertHitboxHrefLangById(linkId, lid, lpos)
{
	var newlid = formatHitboxName(lid);
	var newlpos = formatHitboxName(lpos);
	
	var hrefLangValue = "&lid=" + newlid +"&lpos=" + newlpos;
	
	//find apropriate a tag by ID
	$("a[id$='" + linkId + "']").attr("hreflang", hrefLangValue);
}

function redirectToProductPage(productCode,productLink)
{

	//''+this.href+'?variantCode='+document.getElementById('#{product.code}SelectedVariant').value
	var selectedVariant=document.getElementById(productCode+'SelectedVariant').value;
	if (selectedVariant!=null && selectedVariant.length>0)
	{
	    var t = productLink.split("/");
	    
		t[t.length-4]=selectedVariant;
		
		productLink=t[0];
		
		for(var x=1; x < t.length; x++){
			productLink+= "/" + t[x];
		}
		
	}
	window.location = productLink;
}

function deleteOnunloadFunction()
{
	$("body").attr("onunload", "");
}

function evalHitboxScripts(formName)
{
	var htmlText = $('span[id$='+formName+']').html();

	var myScripts = htmlText.extractScripts();
	if (myScripts)
	{
		var myReturnedValues = myScripts.map(function(script) 
		{
	  		return window.eval(script);  		
		});
	}
}

function saveAjaxForms()
{
	//alert('save');
	saveAjaxFormByName('span','richCategoryTreeForm');
	saveAjaxFormByName('span','productListForm');
	saveAjaxFormByName('div','promoContainerForm');
	saveAjaxFormByName('span','breadcrumbForm');
	
}

function saveAjaxFormByName(formTag,formName)
{

	$("input[id$='"+formName+"Hidden']").val($(""+formTag+"[id$='"+formName+"']").html());
	
}

function restoreAjaxForms()
{
	//alert('restore');
	restoreAjaxFormByName('span','richCategoryTreeForm');
	restoreAjaxFormByName('span','productListForm');
	restoreAjaxFormByName('div','promoContainerForm');
	restoreAjaxFormByName('span','breadcrumbForm');
	
}

function restoreAjaxFormByName(formTag,formName)
{
	var el = document.getElementById(formName+'Hidden');
	
	if (el)
	{
		if (el.value!=null)
		{
			if (el.value.length>0)
			{
				var formComponent = $(""+formTag+"[id$='"+formName+"']");
				if (formComponent)
				{
					formComponent.html(el.value);
				}
			}
		} 
	}
}

function changeInputHiddenValue()
{
	var lastVisitedURL=window.location.href;
	var globalComponent=document.getElementById("topNavigationForm:lastVisitedURLGlobal");
	var productComponent=document.getElementById("productDetailForm:lastVisitedURLProduct");
	if (productComponent==null)
	{
		productComponent=document.getElementById("lastVisitedURLProduct");
	}
	
	var firstIndex=lastVisitedURL.indexOf("cart/detail.jsf");
	//dont save cart page as last visited page
	if (firstIndex==-1)
	{
		if (globalComponent!=null)
		{
			globalComponent.value=lastVisitedURL;
		}	
		if (productComponent!=null)
		{
			productComponent.value=lastVisitedURL;
		}
	}
}

function clickConfirmOrderLink()
{
	var component = document.getElementById('ConfirmForm:confirmOrder');
	if (component)
	{
		// turn off double click securing for this action
		component.disabled = false;
		component.className = 'commandButtonInTable';
		clickSubmitButtonOrLink('ConfirmForm:confirmOrder', 'ConfirmForm:confirmOrderLink');
	}
}

function clickPaymentContinue()
{
	var component = document.getElementById('PaymentInformationForm:addPaymentInfo');
	if (component)
	{
		component.onclick();
	}
}

function getSelectedSortBy()
{
	return $("select[id$='sortBy']").val();
}
function popUpLg(link) 
{
	var browser=navigator.appName;
	
	window.open(link,'myExample5','width=599,height=472,directories=0,location=0,menubar=0,scrollbars=yes,status=0,toolbar=0,resizable=0');
}

function printpage() {
	window.print();  
}
function adjustTextArea(){
	var platform = navigator.platform;
	var browser = navigator.appName;
	var browserVer = navigator.appVersion
	
	if( platform.indexOf("MAC")>=0 ){
		document.getElementById("contactUsForm:comment").rows=5;
	}
	if( platform.indexOf("Win")>=0 && browser.indexOf("Microsoft")>=0 && browserVer.indexOf("6.")>=0){
		document.getElementById("contactUsForm:comment").rows=5;
	}
}	

function reditectToViewUrl(link, indexParamName, indexParamValue, quantityParamName, quantityParamValue)
{
	window.location =  link.href + "?" + indexParamName + "=" + indexParamValue + "&" + quantityParamName + "="+quantityParamValue;
}

function clickButton(buttonName)
{
	var component = document.getElementById(buttonName);

	if (component)
	{ 
	    if (document.createEvent)
	    {
	    	var evObj = document.createEvent('MouseEvents');
		    evObj.initEvent( 'click', true, false );
			component.dispatchEvent(evObj);
		}
	    else if (document.createEventObject)
	    {
	    	component.fireEvent('onclick')
		}
	}
}

function clickSubmitButtonOrLink(buttonName, linkName)
{

	var platform = navigator.platform;
	var browser = navigator.appName;
		
	//for IE click on link
	if( browser.indexOf("Microsoft")>=0)
	{
		var componentLink = document.getElementById(linkName);
		
		try
		{
			componentLink.focus();					
		}
		catch(e){}
		if (document.createEvent)
	    {	    	
	    	var evObj = document.createEvent('MouseEvents');
		    evObj.initEvent( 'click', true, false );
			componentLink.dispatchEvent(evObj);
		}
	    else if (document.createEventObject)
	    {	        				 
			componentLink.click();	         
		}
	}
	//for opera and other browsers click on button
	else
	{
		var componentButton = document.getElementById(buttonName);
		try
		{
			componentButton.focus();
		}
		catch(e){}
		{
		}
		if (document.createEvent)
    	{
	    	var evObj = document.createEvent('MouseEvents');
		    evObj.initEvent( 'click', true, false );
			componentButton.dispatchEvent(evObj);
		}
    	else if (document.createEventObject)
    	{
	    	componentButton.fireEvent('onClick');
		}
	}
}

var deletePopupObject;
var deletePopupAction=false;


function closeDeletePopup()
{
	document.getElementById('deletePop').style.display='none';
	{
		deletePopupAction=false;
		deletePopupObject = null;
	}
	
	enableAllRemoveAndUpdateComponents('commandLinkOnAddToCart', 'update');
}

function closeDeleteAddressPopup()
{
	document.getElementById('deletePop').style.display='none';
	{
		deletePopupAction=false;
		deletePopupObject = null;
	}
	enableAllRemoveAndUpdateComponents('commandLinkOnAddress', 'edit');
}

function confirmPopup()
{
	deletePopupAction = true;
	deletePopupObject.onclick();
}
function openConfirmPopup(element, question, event)
{
	if (element == deletePopupObject)
	{
		if (deletePopupAction == true)
		{
			document.getElementById('deletePop').style.display='none';
			return true;
		}
	}
	else
	{
		disableAllRemoveAndUpdateComponents('commandLinkOnAddressDisabled', 'edit');
		
		var tempX = 0;
		var tempY = 0;
		
		if (navigator.appName == "Microsoft Internet Explorer") 
		{ 
			tempX = event.clientX;
			tempY = event.clientY+document.documentElement.scrollTop;
		} 
		else
		{
			tempX = event.pageX;
			tempY = event.pageY;
		}

		deletePopupAction = false;
		deletePopupObject = element;
		document.getElementById('deletePopQuestion').innerHTML = question;
		document.getElementById('deletePop').style.position = 'absolute';
		document.getElementById('deletePop').style.top = (tempY+100)+'px';
		document.getElementById('deletePop').style.left = (tempX-80)+'px';
		document.getElementById('deletePop').style.display='block';
		return false;
	}
}

function openMessagePopup(question, event)
{
	var tempX = getMouseClickX(event);
	var tempY = getMouseClickY(event);
	  
	document.getElementById('messagePopQuestion').innerHTML = question;
	document.getElementById('messagePop').style.top = (tempY + 155)+'px';
	document.getElementById('messagePop').style.left = (tempX - 90)+'px';
	document.getElementById('messagePop').style.display='block';
	$('#messagePop div.buffer').shield($('#messagePop'));
}

function openMessagePopup(question, event, popupCompName)
{
	var tempX = getMouseClickX(event);
	var tempY = getMouseClickY(event);
			
	var questionCompName = popupCompName+'Question';
	document.getElementById(questionCompName).innerHTML = question;
	document.getElementById(popupCompName).style.top = (tempY + 155)+'px';
	document.getElementById(popupCompName).style.left = (tempX - 90)+'px';
	document.getElementById(popupCompName).style.display='block';
	
	$('#'+popupCompName+' div.buffer').shield($('#'+popupCompName));
}

function closeMessagePopup(popupCompName)
{
	document.getElementById(popupCompName).style.display='none';
	$('#'+popupCompName+' div.buffer').unShield();
}

function openHelpPopup(question, event, popupCompName)
{
	var tempX = getMouseClickX(event);
	var tempY = getMouseClickY(event);
			
	var questionCompName = popupCompName+'Question';
	document.getElementById(questionCompName).innerHTML = question;
	document.getElementById(popupCompName).style.top = (tempY + 155)+'px';
	document.getElementById(popupCompName).style.left = (tempX - 280)+'px';
	document.getElementById(popupCompName).style.display='block';
	
	$('#'+popupCompName+' div.buffer').shield($('#'+popupCompName));
}

function closeHelpPopup(popupCompName)
{
	document.getElementById(popupCompName).style.display='none';
	$('#'+popupCompName+' div.buffer').unShield();
}

function checkSearchTermValue(name, defaultSearchTerm)
{
	var component = document.getElementById(name);
	
	if (component)
	{
		var searchTerm = component.value;
		 
		if (searchTerm==defaultSearchTerm)
			return false;
		else
			return true;
	}
}

function changeHiddenValue(hiddenVariable, newValue)
{
	hiddenVariable = newValue;
}


function checkHiddenValues()
{
	if ( (hiddenConfirmOrder == 1) || (hiddenPaymentContinue == 1) )
	{
		return true;
	}
	else
	{
		return false;
	}
}
		
function closeWPF()
{
 	clickConfirmOrderLink();
   	try{clickSubmitButtonOrLink('PaymentInformationForm:submit','PaymentInformationForm:submitLink');} catch (e) {}
   	try{paymentDialog.hide();} catch (e) {}
}

function StartTheTimer()
{
     if (checkHiddenValues())
     {
     	closeWPF();
     	parent.hiddenConfirmOrder = 0;
		parent.hiddenPaymentContinue = 0;
     } else if (hiddenPaymentError==1)
     {
     	hiddenPaymentError=0;
     	try { clickPaymentError(); } catch (e) {}
     	try { clickIdealPaymentError(); } catch (e) {}
     } else if (hiddenPaymentAVSError==1)
     {
     	hiddenPaymentAVSError=0;
     	clickPaymentAVSError();
     } else if (hiddenPaymentCancel==1)
     {
     	try { clickPaymentCancel();} catch (e) {}
     	try { clickIdealPaymentCancel();} catch (e) {}
     	
     }
     else
     {
     	self.setTimeout("StartTheTimer()", 2000);
     }
}	

function clickPaymentError()
{
	var component = document.getElementById('PaymentInformationForm:paymentErrorInfo');
	if (component)
	{
		component.onclick();
	}
}

function clickIdealPaymentError()
{
	var component = document.getElementById('ConfirmForm:paymentErrorInfo');
	if (component)
	{
		component.onclick();
	}
}

function clickPaymentAVSError()
{
	var component = document.getElementById('PaymentInformationForm:paymentAVSErrorInfo');
	if (component)
	{
		component.onclick();
	}
}

function clickPaymentCancel()
{
	var component = document.getElementById('PaymentInformationForm:paymentCancelInfo');
	if (component)
	{
		component.onclick();
	}
}

function clickIdealPaymentCancel()
{
	var component = document.getElementById('ConfirmForm:paymentCancelInfo');
	if (component)
	{
		component.onclick();
	}
}

function validateSessionID()
{

	if( validateSession == true ){

		var cookie = document.cookie;		
		var cookieSessionID;
		var values;
		if(cookie!=null){
			values = cookie.split(';');
			for( i=0; i<values.length;i++)
			{
				if( values[i].search('JSESSIONID=') > -1 )
				{	
					cookieSessionID = values[i].split('=')[1];
				}
			}
			if( serverSessionID.match(cookieSessionID) != cookieSessionID ) 
				alert("cookie Session: " + cookieSessionID + " // server Session: " + serverSessionID);
		}
	}
}
function disableLinkInHitboxForm(component, actionName)
{
	if (component)
	{		
		var className = component.className;
		var firstIndex = className.indexOf(' clicked');
			
		if (firstIndex==-1)
		{									
			component.className=className + ' clicked';
			
			firstIndex = actionName.indexOf('abandon');
			if (firstIndex!=-1)
			{
				hbFormAbandon();
			}
			firstIndex = actionName.indexOf('complete');
			if (firstIndex!=-1)
			{							    
				hbFormCompletion();						
			}
			return true;
		}
		else
		{			
			component.disabled='true';
			return false;
		}
	}
	return true;
}
function disableLink(component)
{
	if (component)
	{
		var className = component.className;
		var firstIndex = className.indexOf(' clicked');
	
		if (firstIndex==-1)
		{
			component.className=className + ' clicked';
			return true;
		}
		else
		{
			component.disabled='true';
			return false;
		}
	}
	return true;
}
function disableUpdateLink(clickedComponentIndex)
{
	//search for the text component
	var clickedComponentId = 'newProductInCartModalForm:update' + clickedComponentIndex;
	var textComponent = document.getElementById(clickedComponentId + 'Text');
	var updateComponent = document.getElementById(clickedComponentId);
		
	if (updateComponent)
	{		
		var className = updateComponent.className;
		var firstIndex = className.indexOf(' clicked');
	
		if (firstIndex==-1)
		{
			updateComponent.className=className + ' clicked';
			
			disableAllRemoveAndUpdateComponents('commandLinkOnAddToCartDisabled', 'update');
			return true;
		}
		else
		{
			if (textComponent)
			{
				textComponent.className='commandLinkOnAddToCartDisabled';
			}
			//disable anchor component
			updateComponent.className='hiddenButton';
			return false;
		}
	}
	return true;
}
function disableEditLink()
{
	disableAllRemoveAndUpdateComponents('commandLinkOnAddressDisabled', 'edit');
}
function changeSelectedCartEntry(name, entryNumber)
{
	var component = document.getElementById(name);
	if (component)
	{
		component.value=entryNumber;
	}
}
function changeSelectedCartEntryAndQuantity( realNumberComponentName, realEntryNumberValue, statusIndexComponentName, statusIndexValue )
{
	var component = document.getElementById(realNumberComponentName);
	if (component)
	{
		component.value=realEntryNumberValue;
	}
	
	component = document.getElementById(statusIndexComponentName);
	if (component)
	{
		component.value=statusIndexValue;
	}
}
function removeProductFromCartPopUp(realEntryNumberValue, statusIndexValue, realNumberComponentName, statusIndexComponentName, confirmMessage, hiddenRemoveButton, event)
{
	var clickedComponentId = 'newProductInCartModalForm:remove' + statusIndexValue;
	var textComponent = document.getElementById(clickedComponentId + 'Text');
	var removeComponent = document.getElementById(clickedComponentId);
	
	if (removeComponent == deletePopupObject)
	{
		if (deletePopupAction == true)
		{
			document.getElementById('deletePop').style.display='none';
			changeSelectedCartEntryAndQuantity( realNumberComponentName, realEntryNumberValue, statusIndexComponentName, statusIndexValue )
			//click on hidden button or link
			var buttonName = hiddenRemoveButton + 'Button'+ statusIndexValue;
			var linkName = hiddenRemoveButton + 'Link' + statusIndexValue;
			
			clickSubmitButtonOrLink(buttonName, linkName);		
		
			return false;
		}
	}
	else
	{
		disableAllRemoveAndUpdateComponents('commandLinkOnAddToCartDisabled', 'update');
		
		var tempX = getMouseClickX(event);
		var tempY = getMouseClickY(event);
			
		if (tempX == 0 && tempY == 0)
		{
			tempX = findPosX(textComponent);
			tempY = findPosY(textComponent);
		}
	  
		deletePopupAction = false;
		deletePopupObject = removeComponent;
		document.getElementById('deletePopQuestion').innerHTML = confirmMessage;
		document.getElementById('deletePop').style.top = (tempY+100)+'px';
		document.getElementById('deletePop').style.left = (tempX-70)+'px';
		document.getElementById('deletePop').style.display='block';
		
		return false;
	}
}

var idealModalWindow;

function idealModalWindow(url,title) {
	//if (window.showModalDialog) {
		//idealModalWindow=window.showModalDialog(url,title,
		//	"dialogWidth:255px;dialogHeight:250px");
	//} else {
		window.open(url,"WPF","height=700,width=500,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no");
	//}
}

var idealFormElement = null;

function idealCheck(messageComponentId)
{
	var idealForm = document.getElementById('idealForm');
	
	var acceptTerms = document.getElementById('ConfirmForm:acceptTerms');
	
	var checkMessage = document.getElementById(messageComponentId).innerHTML;
	
	if(!acceptTerms.checked)
	{
		alert(checkMessage);
		return true;
	}

	//check if not during page reload
	if (hiddenConfirmOrder==1 || hiddenPaymentContinue==1 || hiddenPaymentError==1 || hiddenPaymentAVSError==1 || hiddenPaymentCancel==1)
	{
		return true;
	}	
	
	if (idealForm!=null && hiddenConfirmOrder==0)
	{
		StartTheTimer();
		blockForm();
		idealFormElement = window.open("/pages/checkout/checkoutWPFAsyncPage.jsf","WPF","height=700,width=800,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes");
		StartBlockTimer();
		return true;
	}

	return false;
}

function blockForm()
{
	var interVeil=document.getElementById("interVeil"); //Reference "veil" div
	var pageEl=document.getElementById("pageWrapper");
		
	var myWidth = 0, myHeight = 0;
	if( typeof( window.innerWidth ) == 'number' ) {
	  //Non-IE
	  myWidth = window.innerWidth;
	  myHeight = window.innerHeight;
	} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
	  //IE 6+ in 'standards compliant mode'
	  myWidth = document.documentElement.clientWidth;
	  myHeight = document.documentElement.clientHeight;
	} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
	  //IE 4 compatible
	  myWidth = document.body.clientWidth;
	  myHeight = document.body.clientHeight;
	}

	if (interVeil.style.display!="block")
	{
		var pageWrapperWidth = pageEl.clientWidth;
		var pageWrapperHeight = pageEl.clientHeight;		

		if(pageWrapperWidth > myWidth)
			interVeil.style.width=pageWrapperWidth+"px" //set up veil over page
		else
			interVeil.style.width=myWidth+"px" //set up veil over page
			
		if (pageWrapperHeight > myHeight)	
			interVeil.style.height=pageWrapperHeight+"px" //set up veil over page
		else
			interVeil.style.height=myHeight+"px" //set up veil over page
		
	
		interVeil.style.left=0 //Position veil over page
		interVeil.style.top=0 //Position veil over page
		interVeil.style.visibility="visible" //Show veil over page
		interVeil.style.display="block" //Show veil over page
	}
	
	
}

function unblockForm()
{
	var interVeil=document.getElementById("interVeil"); //Reference "veil" div
	if (interVeil)
	{
		interVeil.style.display="none" //hide veil over page
	}	
}


function StartBlockTimer()
{
     if (idealFormElement.closed)
     {
		// if payment window closed without interaction with main window fire cancel event     	
     	if (hiddenConfirmOrder==0 && hiddenPaymentContinue==0 && hiddenPaymentError==0 && hiddenPaymentAVSError==0 && hiddenPaymentCancel==0)
     	{
     		hiddenPaymentCancel=1;
     	}

     	unblockForm();

     }
     else
     {
     	blockForm();
     	self.setTimeout("StartBlockTimer()", 2000);
     }
}	

function setFocusOnHiddenInput(hiddenInputId)
{
	var hiddenComponent = document.getElementById(hiddenInputId);
	try
	{
		hiddenComponent.focus();
	}
	catch(e){ }
}
function getMouseClickX(event)
{
	var tempX;
	var IE = document.all?true:false
	
	if (IE) 
	{ // grab the x-y pos.s if browser is IE
		tempX = event.clientX + document.body.scrollLeft
	} 
	else 
	{  // grab the x-y pos.s if browser is NS
		tempX = event.pageX
	}
	
	return tempX;
}
function getMouseClickY(event)
{
	var tempY;
	
	var IE = document.all?true:false
	
	if (IE) 
	{ // grab the x-y pos.s if browser is IE

		//Document scroll offset coordinates (DSOC) changes from document.body to 
		//document.documentElement if IE go into standards compliant mode 
		//(by using doctype at the top of the page)
	 	var iebody=(document.compatMode && document.compatMode != "BackCompat")? document.documentElement : document.body
	 	var dsoctop=document.all? iebody.scrollTop : pageYOffset
		
		tempY = event.clientY + dsoctop
	} 
	else 
	{  // grab the x-y pos.s if browser is NS
		tempY = event.pageY
	}
	
	return tempY;
}
function enableFocusOnSearchComponent()
{
	var searchComponent = document.getElementById('newProductSearchForm:search');
	if (searchComponent)
	{
		searchComponent.disabled="";
	}	
}
function disableFocusOnSearchComponent()
{
	var searchComponent = document.getElementById('newProductSearchForm:search');
	if (searchComponent)
	{
		searchComponent.disabled="disabled";
	}	
}

function disableBackContinueButtons(backComponentId, ContinueComponentId)
{
	var backComponent = document.getElementById(backComponentId);
	var continueComponent = document.getElementById(ContinueComponentId);
	
	if (backComponent && continueComponent)
	{
		var backComponentClassName = backComponent.className;
		var continueComponentclassName = continueComponent.className;
		
		backComponent.className=backComponentClassName + ' clicked';
		continueComponent.className=continueComponentclassName + ' clicked';
	
		backComponent.disabled="disabled";
		continueComponent.disabled="disabled";
	}
}

function disableBackButton(backComponentId)
{
	var backComponent = document.getElementById(backComponentId);
	if (backComponent)
	{
		var backComponentClassName = backComponent.className;
		backComponent.className=backComponentClassName + ' clicked';
		backComponent.disabled="disabled";
	}	
}

function disableBackContinueButtonsAndRefreshForm()
{
	disableBackContinueButtons('paymentButtonForm:back','paymentButtonForm:addPaymentInfo');
	clickSubmitButtonOrLink('PaymentInformationForm:refreshButton', 'PaymentInformationForm:refreshLink');
}

function disableAllRemoveAndUpdateComponents(className, secondComponentId)
{
	//disable all update inputs
	var updateItems = $("a[id*='" + secondComponentId + "']");	
	updateItems.each(function(i) 
	{
		this.className='hiddenButton';
	});
	//enable all update text inputs
	var updateItemsTexts = $("span[id*='" + secondComponentId + "']");	
	updateItemsTexts.each(function(i) 
	{	
		this.className=className;
	});
	//disable all other remove links
	var removeItems = $("a[id*='remove']");
	removeItems.each(function(i) 
	{
		//disable each inputs
		this.className='hiddenButton';		
	});
	
	var removeItemsTexts = $("span[id*='remove']");
	removeItemsTexts.each(function(i) 
	{
		//enable all text componets
		this.className=className;		
	});
}
function enableAllRemoveAndUpdateComponents(className, secondComponentId)
{
	//enable all update inputs
	var updateItems = $("a[id*='" + secondComponentId + "']");	
	updateItems.each(function(i) 
	{
		this.className=className;
	});
	//enable all update text inputs
	var updateItemsTexts = $("span[id*='" + secondComponentId + "']");	
	updateItemsTexts.each(function(i) 
	{
		this.className='hiddenButton';
	});
		
	//enable all remove inputs
	var removeItems = $("a[id*='remove']");	
	removeItems.each(function(i) 
	{
		this.className=className;
	});
	//enable all update text inputs
	var removeItemsTexts = $("span[id*='remove']");	
	removeItemsTexts.each(function(i) 
	{
		this.className='hiddenButton';
	});
}

function findPosX(obj)
  {
    var curleft = 0;
    if(obj.offsetParent)
        while(1) 
        {
          curleft += obj.offsetLeft;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.x)
        curleft += obj.x;
    return curleft;
  }

  function findPosY(obj)
  {
    var curtop = 0;
    if(obj.offsetParent)
        while(1)
        {
          curtop += obj.offsetTop;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.y)
        curtop += obj.y;
    return curtop;
  }