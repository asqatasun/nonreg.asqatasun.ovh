/* Adaptation � CFMX : correction sur la ligne suivante :
	old : var year=text_month.substring(positionEspace,text_month.length);
	new : var year=text_month.substring(positionEspace+1,text_month.length); (Maher - 19/02/2007)
*/
/*le formulaire de saisie d'une date est parametrable
	Ainsi on pourra utiliser la fonction saisie_date() pour avoir 
	des formulaires de saisies 	d'une date sans meme repeter les codes.
	Il suffit juste d'appeler la fonction mais seules les variables 
	sont diff�rentes.
	
	La syntaxe est comme suit :
	function saisie_date(var_day,var_month,var_year,form_result,default_value,evt)	
	var_day=la variable jour;
	var_month=la variable month;
	var_year=la variable mois;
	form_result=la variable qui contiendra la date complete 
				c-a-d le resultat; en principe un champ de saisie
	default_value=la date par d�faut.De format "mm/dd/yyyy"
	evt=chaine des �v�nements ou autre ex:"disabled","OnClick='....'"
*/
	var min_day=01;
	var max_day=31;
	var month=new Array();
	month[0]='Janvier ';
	month[1]='Fevrier ';
	month[2]='Mars ';
	month[3]='Avril ';
	month[4]='Mai ';
	month[5]='Juin ';
	month[6]='Juillet ';
	month[7]='Ao&ucirc;t ';
	month[8]='Septembre ';
	month[9]='Octobre ';
	month[10]='Novembre ';
	month[11]='D&eacute;cembre ';
	var min_year=2000;
	var max_year=min_year+10;
	var nbJours= 7;
		
	/* Fonction qui donnera comme le mois + l'annee */
	function leMois(i,mois,dateDuJour) {
	var an,res;
		if (i<dateDuJour.getMonth())
			an=dateDuJour.getYear()+1
		else
			an=dateDuJour.getYear();
		if (an<1900) // sur netscape 4.5 ou ulterieur
			an=an+1900
		res=mois+an;
		return res;
	}
	
		
	/*fonction qui donnera la date complete en fonction 
		des variables jours,mois,et annee
		Le resultat sera affect� � "form_result"
	*/
	function verifier_date_vol(form_day,form_month,form_year,form_result,jour_ttl)
	{
		var indice_day=form_day.selectedIndex;
		var day=indice_day+1;
		var indice_month=form_month.options[form_month.selectedIndex].value;
		var text_month=form_month.options[form_month.selectedIndex].text;
		var positionEspace=text_month.indexOf(' ');
		var year=text_month.substring(positionEspace+1,text_month.length);
		var month=indice_month;
		var indice_year=form_year.selectedIndex;
		
		//var year=min_year+indice_year;
		//var year=min_year;
		
		if(month==2)
		{
			// si on est Fevrier 
			if(year%4==0)
			{
				if(day>29) form_day.selectedIndex=28;
			}
			else 
				if (day>28) form_day.selectedIndex=27;
		}
		else
		{
			if(month<7)	var reste=0;
			else 
				if(month>8) var reste=1;
				
			if((month<7)||(month>8))
			{
				if(month%2==reste)//dernier jour =30
					if(day>29) form_day.selectedIndex=29;
			}
		}
		form_year.value=year;
		form_result.value=form_day.options[form_day.selectedIndex].value+"/"+form_month.options[form_month.selectedIndex].value+"/"+year;
		
		var date1_sav = form_month.options[form_month.selectedIndex].value+"/"+form_day.options[form_day.selectedIndex].value+"/"+year;
				
		// Selection automatique du date de retour...
		if ( form_day.name == 'DEPART_DD' && form_month.name == 'DEPART_MM' ) 
		{
			var d_day	= form_day.selectedIndex+1;
			var d_month	= form_month.options[form_month.selectedIndex].value-1;
			var d_year	= form_year.value;
			var secondDate = Ajout_date(d_day,d_month,d_year,nbJours);
			document.moteur.RETOUR_DD.selectedIndex = secondDate.getDate()-1;
			for (var ii = 0;ii<=11;ii++) 
			{
				if (document.moteur.RETOUR_MM.options[ii].value == secondDate.getMonth() + 1 ) 
					var currentCible = ii;
			}
			Afficher_Jour_Aller_Vol(d_day,d_month,d_year);			
			
			document.moteur.RETOUR_MM.selectedIndex = currentCible;
			//document.moteur.RETOUR_YYYY.value = form_year.value;
			document.moteur.RETOUR_YYYY.value = secondDate.getFullYear();

			Afficher_Jour_Retour_Vol(secondDate.getDate(),secondDate.getMonth(),secondDate.getFullYear());
		}
		else	//retour
		{
			var d_day	= form_day.selectedIndex+1;
			var d_month	= form_month.options[form_month.selectedIndex].value-1;
			var d_year	= form_year.value;
			Afficher_Jour_Retour_Vol(d_day,d_month,d_year);			
		}
		
		//code qui r�cup�re le jour "Lundi", "Mardi", ...
		sjour = GetTodayName(date1_sav);
		//document.moteur.jour_ttl.options.selectedIndex=null; //efface la valeur d�j� existante
		//alert("jour_ttl" + jour_ttl.options[0].value);	


	}
	//fin fonction verifier
	
	//Ajoute date de n (jours) jours
	function Ajout_date(d_day,d_month,d_year,jours) {
	
		var DaysOfMonths = Array(); //Nombre de jours par mois
		
		for (var i=0;i<=6;i++) {
			DaysOfMonths[i] = i%2 ? 30 : 31;
		}
		for (var i=7;i<=11;i++) {
			DaysOfMonths[i] = i%2 ? 31 : 30;
		}
		DaysOfMonths[1] = d_year%4 ? 28 : 29; // For fevrier only
		
		var C_Day 	= d_day + jours;
		var C_Month = d_month;
		var C_Year  = d_year;
		
		while ( C_Day > DaysOfMonths[C_Month] ) {
			C_Day	= C_Day - DaysOfMonths[C_Month];	
			C_Month++;
			if (C_Month == 12) break;
		}

		if (C_Month == 12) {
			C_Month = 0;
			C_Year++;
			C_Day--;
			var outputDate = Ajout_date(1,0,C_Year,C_Day);
		}
		else var outputDate = new Date(C_Year,C_Month,C_Day);
		
		return outputDate;	
	}
	
	
	//fonction saisie_date
	function saisie_date(var_day,var_month,var_year,form_result,dateJour,default_value,evt)
	{	
		var date_default=new Date(default_value);
		var now=new Date(dateJour);
		
		document.write("<select class='input1' name='"+var_day+"' tabindex='3' style='width:45px; color:#000000; font-size: 10px; font-family: Arial; font-weight: normal;' OnChange='verifier_date_vol(form."+var_day+",form."+var_month+",form."+var_year+","+form_result+")'"+evt+">");
			for(i=min_day;i<=max_day;i++)
			{
				if(date_default.getDate()==i)
					document.write("<option value='" +i+"' selected>"+((i<10)?'0'+i:i));
				else
					document.write("<option value='" +i+"'>"+((i<10)?'0'+i:i));				
			}	
		document.write("</select>");
		
		//saisie mois
		document.write(	"<select class='input1' name='"+var_month+"' tabindex='4' style='width:100px; color:#000000; font-size: 10px; font-family: Arial; font-weight: normal;' OnChange='verifier_date_vol(form."+var_day+",this,form."+var_year+","+form_result+");'"+evt+">");		
			for(i=now.getMonth();i<month.length;i++)
			{
				if(date_default.getMonth()==i)		
					document.write("<option value='" +(i+1)+"' selected>"+leMois(i,month[i],now));
				else
					document.write("<option value='" +(i+1)+"'>"+leMois(i,month[i],now));
			}
			for(i=0;i<now.getMonth();i++)
			{
				if(date_default.getMonth()==i)		
					document.write("<option value='" +(i+1)+"' selected>"+leMois(i,month[i],now));
				else
					document.write("<option value='" +(i+1)+"'>"+leMois(i,month[i],now));
			}
		document.write("</select>");
		
		//saisie ann�e
		if (date_default.getYear()<1900)
			annee=date_default.getYear()+1900
		else
			annee=date_default.getFullYear();
			document.write("<input type='hidden' name='"+var_year+"' value='"+annee+"'>");	
			
		//Rafra�chir l'affichage des jours de la semaine - 03/05/2001 - Maher
		if (evt == 'D')	Afficher_Jour_Aller_Vol(date_default.getDate(),date_default.getMonth(),date_default.getFullYear());
		if (evt == 'R')	Afficher_Jour_Retour_Vol(date_default.getDate(),date_default.getMonth(),date_default.getFullYear());
	}
	//fin sisie_date
	
	//�limination des blancs n'importe o� dans le champ
	function Trim(val)
	{
		var res='';
		for(i=0;i<val.length;i++)
		{
			if(!(val.charAt(i)==" "))
				res=res+val.charAt(i);
		}
		return res;
	}
	
	//retourne vraie si la valeur est numerique fausse sinon
	function IsNumeric(val)
	{
		var test=true;
		for(i=0;i<val.length;i++)
		{
			temp=val.charAt(i);
			if( !((temp>=0) && (temp<=9) ||(temp=='.')) )
			{
				test=false;
				break;
			}
		}
		return test;		
	}
	
	//teste un champ s'il est numerique au cours du saisie
	// si on specifie l'evenement OnChange
	function Test_Numeric(form)
	{
		form.value=Trim(form.value);
		if(!IsNumeric(form.value))
		{
			alert("La valeur doit �tre num�rique");
			form.focus();
		}
	}
	
	//compte la valeur d'une chaine 'str' dans 'val'
	function Compter_Chaine(str,val)
	{
		var temp=val;
		var compteur=0;
		var indice=0;
		var pos=0;
		for(var i=0;i<temp.length;i++)
		{
			pos=temp.indexOf(str,0);
			if(pos!=-1)
			{
				compteur++;
				temp=temp.substring(pos+str.length,temp.length);
			}
		}
		return compteur;
	}
	
	function IsMail(val)
	{
		var test=false;
		if( (val.indexOf('@')!=-1) && (val.indexOf('@')!=val.length-1) &&  (val.indexOf('@')!=0))
		{
			if(Compter_Chaine('@',val)==1)
				test=true;
		}
		
		return test;
	}
	
	// Fonction qui retourne le prochain samedi <13 jours
function GetSaturday(maintenant,n)	//ajoute n jours au r�sultat final
{
	var Aujourdhui = new Date(maintenant);
	var jour = Aujourdhui.getDate();
	var mois = Aujourdhui.getMonth();
	var annee = Aujourdhui.getYear();
	var day = Aujourdhui.getDay();
	var date;date2="";
	
	if (annee<1900) //sur Netscape 4.5 ou ult�rieur
		annee=annee+1900;
		
	if (day==0)
	{
		//case 0 : //dimanche
			date2=( ((jour<10) ? '0' + jour:jour) + '/' + ((mois<10) ? '0' + mois:mois) + '/' + annee);
			date2=Ajout_date(jour,mois,annee,6+n);
			return date2;
	}
		
		if (day== 1) //lundi
		{
			date2= (((jour<10) ? '0' + jour:jour) + '/' + ((mois<10) ? '0' + mois:mois) + '/' + annee);
			date2=Ajout_date(jour,mois,annee,12+n);
			return date2;
		}
		
		if (day== 2) //mardi
		{
			date2=(((jour<10) ? '0' + jour:jour) + '/' + ((mois<10) ? '0' + mois:mois) + '/' + annee);
			date2=Ajout_date(jour,mois,annee,11+n); 
			return date2;
		}
		
		if (day == 3) //mercredi
		{
			date2=(((jour<10) ? '0' + jour:jour) + '/' + ((mois<10) ? '0' + mois:mois) + '/' + annee);
			date2=Ajout_date(jour,mois,annee,10+n); 
			return date2;
		}
		
		if (day == 4)  //jeudi
		{
			date2=(((jour<10) ? '0' + jour:jour) + '/' + ((mois<10) ? '0' + mois:mois) + '/' + annee);
			date2=Ajout_date(jour,mois,annee,9+n); 
			return date2;
		}
		
		if (day== 5) //vendredi
		{
			date2=(((jour<10) ? '0' + jour:jour) + '/' + ((mois<10) ? '0' + mois:mois) + '/' + annee);
			date2=Ajout_date(jour,mois,annee,8+n); 
			return date2;
		}
		
		if (day== 6) //samedi
		{
			date2=(((jour<10) ? '0' + jour:jour) + '/' + ((mois<10) ? '0' + mois:mois) + '/' + annee);
			date2=Ajout_date(jour,mois,annee,7+n); 
			return date2;
		}
		
}
	
	// Fonction qui renvoie la date du jour
	
	function GetTodayDate(datejour)
	{
		Aoujourdhui = new Date(datejour);
		jour = Aoujourdhui.getDate();
		mois = Aoujourdhui.getMonth()+1;
		annee = Aoujourdhui.getYear();
		
		date=((mois<10)?'0'+mois:mois)+'/'+((jour<10)?'0'+jour:jour)+'/'+annee
		return date;
	}

	// Fonction qui renvoi le libell� du jour "Lundi", "Mardi", ...  
	// Le 27/02/2001 - Maher
	
	function GetTodayName(datejour)
	{
		Aoujourdhui = new Date(datejour);
		jour = Aoujourdhui.getDay();
		//alert("xx datejour = " + datejour);
		//alert("jour = " + jour);
				
		if (jour == 0)
			return "Dim";
		if (jour == 1)
			return "Lun";
		if (jour == 2)
			return "Mar";
		if (jour == 3)
			return "Mer";
		if (jour == 4)
			return "Jeu";
		if (jour == 5)
			return "Ven";
		if (jour == 6)
			return "Sam";
	}	

	//fonction qui r�affiche en toutes lettres le jour de la semaine - Maher 19/04/2001		
	function Afficher_Jour_Aller_Vol(d_day,d_month,d_year) //d�part
	{
		DateSaisie = new Date(d_year,d_month,d_day,0,0,0,0);
		jour = DateSaisie.getDay();

		if (jour == 0) document.moteur.image1.src="/images/dimanche.gif";
		if (jour == 1) document.moteur.image1.src="/images/lundi.gif";
		if (jour == 2) document.moteur.image1.src="/images/mardi.gif";
		if (jour == 3) document.moteur.image1.src="/images/mercredi.gif";
		if (jour == 4) document.moteur.image1.src="/images/jeudi.gif";
		if (jour == 5) document.moteur.image1.src="/images/vendredi.gif";
		if (jour == 6) document.moteur.image1.src="/images/samedi.gif";
	}

	function Afficher_Jour_Retour_Vol(d_day,d_month,d_year) //retour
	{
		DateSaisie = new Date(d_year,d_month,d_day,0,0,0,0);
		jour = DateSaisie.getDay();
		if (jour == 0) document.moteur.image2.src="/images/dimanche.gif";
		if (jour == 1) document.moteur.image2.src="/images/lundi.gif";
		if (jour == 2) document.moteur.image2.src="/images/mardi.gif";
		if (jour == 3) document.moteur.image2.src="/images/mercredi.gif";
		if (jour == 4) document.moteur.image2.src="/images/jeudi.gif";
		if (jour == 5) document.moteur.image2.src="/images/vendredi.gif";
		if (jour == 6) document.moteur.image2.src="/images/samedi.gif";
	}	