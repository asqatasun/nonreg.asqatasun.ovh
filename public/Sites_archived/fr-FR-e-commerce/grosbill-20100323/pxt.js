function getAnchorPosition(anchorname){var useWindow=false;var coordinates=new Object();var x=0,y=0;var use_gebi=false, use_css=false, use_layers=false;if(document.getElementById){use_gebi=true;}else if(document.all){use_css=true;}else if(document.layers){use_layers=true;}if(use_gebi && document.all){x=AnchorPosition_getPageOffsetLeft(document.all[anchorname]);y=AnchorPosition_getPageOffsetTop(document.all[anchorname]);}else if(use_gebi){var o=document.getElementById(anchorname);x=AnchorPosition_getPageOffsetLeft(o);y=AnchorPosition_getPageOffsetTop(o);}else if(use_css){x=AnchorPosition_getPageOffsetLeft(document.all[anchorname]);y=AnchorPosition_getPageOffsetTop(document.all[anchorname]);}else if(use_layers){var found=0;for(var i=0;i<document.anchors.length;i++){if(document.anchors[i].name==anchorname){found=1;break;}}if(found==0){coordinates.x=0;coordinates.y=0;return coordinates;}x=document.anchors[i].x;y=document.anchors[i].y;}else{coordinates.x=0;coordinates.y=0;return coordinates;}coordinates.x=x;coordinates.y=y;return coordinates;}
function getAnchorWindowPosition(anchorname){var coordinates=getAnchorPosition(anchorname);var x=0;var y=0;if(document.getElementById){if(isNaN(window.screenX)){x=coordinates.x-document.body.scrollLeft+window.screenLeft;y=coordinates.y-document.body.scrollTop+window.screenTop;}else{x=coordinates.x+window.screenX+(window.outerWidth-window.innerWidth)-window.pageXOffset;y=coordinates.y+window.screenY+(window.outerHeight-24-window.innerHeight)-window.pageYOffset;}}else if(document.all){x=coordinates.x-document.body.scrollLeft+window.screenLeft;y=coordinates.y-document.body.scrollTop+window.screenTop;}else if(document.layers){x=coordinates.x+window.screenX+(window.outerWidth-window.innerWidth)-window.pageXOffset;y=coordinates.y+window.screenY+(window.outerHeight-24-window.innerHeight)-window.pageYOffset;}coordinates.x=x;coordinates.y=y;return coordinates;}
function AnchorPosition_getPageOffsetLeft(el){var ol=el.offsetLeft;while((el=el.offsetParent) != null){ol += el.offsetLeft;}return ol;}
function AnchorPosition_getWindowOffsetLeft(el){return AnchorPosition_getPageOffsetLeft(el)-document.body.scrollLeft;}
function AnchorPosition_getPageOffsetTop(el){var ot=el.offsetTop;while((el=el.offsetParent) != null){ot += el.offsetTop;}return ot;}
function AnchorPosition_getWindowOffsetTop(el){return AnchorPosition_getPageOffsetTop(el)-document.body.scrollTop;}
function switchdivpos (divid){
var cpos=getAnchorPosition('divid');
var cdiv;
cdiv= document.getElementById(divid);
cdiv.style.left=cpos.x+'px';
cdiv.style.top=cpos.y+'px';
cdiv.style.position='absolute';
}
function correctPNG() {
  for(var i=0; i<document.images.length; i++) {
    var img = document.images[i]
    var imgName = img.src.toUpperCase()
    if (imgName.substring(imgName.length-3, imgName.length) == "PNG") {
      var imgID = (img.id) ? "id='" + img.id + "' " : ""
      var imgClass = (img.className) ? "class='" + img.className + "' " : ""
      var imgTitle = (img.title) ? "title='" + img.title + "' " : "title='" + img.alt + "' "
      var imgStyle = "display:inline-block;" + img.style.cssText
      if (img.align == "left") imgStyle = "float:left;" + imgStyle
      if (img.align == "right") imgStyle = "float:right;" + imgStyle
      if (img.parentElement.href) imgStyle = "cursor:hand;" + imgStyle
      var strNewHTML = "<span " + imgID + imgClass + imgTitle
      + " style=\"" + "width:" + img.width + "px; height:" + img.height + "px;" + imgStyle + ";"
      + "filter:progid:DXImageTransform.Microsoft.AlphaImageLoader"
      + "(src=\'" + img.src + "\', sizingMethod='scale');\"><\/span>"
      img.outerHTML = strNewHTML
      i = i-1
    }
  }
}
if (navigator.platform == "Win32" && navigator.appName == "Microsoft Internet Explorer" && window.attachEvent) {
	////document.writeln('<style type="text/css">img { visibility:hidden; } </style>');
	window.attachEvent("onload", fnLoadPngs);
}

var IE6transpng = new Array();
function IE6Trans()
{
	var rslt = navigator.appVersion.match(/MSIE (\d+\.\d+)/, '');
	var itsAllGood = (rslt != null && Number(rslt[1]) >= 5.5);
	if(!itsAllGood)return;
	if(typeof document.body.style.maxHeight != "undefined")
	{
		//alert("IE7");
		return;
	}
	
	//itsAllGood = 1;
  var reloadme=0;
  for(var i in IE6transpng)
  {
	 img = IE6transpng[i];
	 //if(eval(img.width)>0 && eval(img.height)>0 ){}else	setTimeout("IE6Trans("+img+")", 500);
	 if(eval(img.width)>0 && eval(img.height)>0 )
	 {	
		if(img.src!="http://v2.grosbill.com/x.gif" && (img.style.filter==null || img.style.filter==""))
		{
		  img.style.visibility='hidden';
		  var src = img.src;
		  img.style.width = img.width + "px";
		  img.style.height = img.height + "px";
		  //alert("w"+img.width+" h"+img.height);
		  img.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + src + "', sizingMethod='scale')";
		  //img.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='http://v2.grosbill.com/iv2/pxt_sn_accueiil.png', sizingMethod='scale')";
		  img.src = "/x.gif";
		  img.style.visibility = "visible";
		}
	 }
	 else reloadme++;
	 if(reloadme>0)
	 	setTimeout("IE6Trans()", 500);
  }
  /*

	  for (var i = document.images.length - 1, img = null; (img = document.images[i]); i--) {
	 if (img.src.match(/\.png$/i) != null) {
	 if(img.src=="http://v2.grosbill.com/iv2/pxt_sn_accueiil.png" && (img.style.filter==null || img.style.filter==""))
	 {
		img.style.visibility='hidden';
		  //alert("src="+img.src);
		  var src = img.src;
		  img.style.width = img.width + "px";
		  img.style.height = img.height + "px";
		  img.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + src + "', sizingMethod='scale')"
		  img.src = "/x.gif";
		  img.onload = null;
		  }
	 }
	 img.style.visibility = "visible";
  }*/

}



function fnLoadPngs() {

if (typeof document.body.style.maxHeight != "undefined") {
// alert('IE7');
} else {
var rslt = navigator.appVersion.match(/MSIE (\d+\.\d+)/, '');
var itsAllGood = (rslt != null && Number(rslt[1]) >= 5.5);
	/*for (var i = document.images.length - 1, img = null; (img = document.images[i]); i--) {
		if (itsAllGood && img.src.match(/\.png$/i) != null) {
			img.style.visibility='hidden';
			//var src = img.src;
			//img.style.width = img.width + "px";
			//img.style.height = img.height + "px";
			//img.style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + src + "', sizingMethod='scale')"
			//img.src = "/x.gif";
		}
		img.style.visibility = "visible";
	}
	*/
//document.writeln('<style type="text/css">img { visibility:visible; } </style>');

}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}


var PXT_lastsrcdiv=new Array();
function PXT_switchdiv(s,d,imgswitch,sn){
var src = document.getElementById(s);
var dest = document.getElementById(d);
dest.innerHTML=src.innerHTML;if(imgswitch){
if(PXT_lastsrcdiv[sn]==undefined){PXT_lastsrcdiv[sn]=s;}
PXT_switchdiv_img(s,PXT_lastsrcdiv[sn]);
}
PXT_lastsrcdiv[sn]=s;
}
function PXT_switchdiv_img(newi,oldi){ ;
var imgold=document.getElementById('img_'+oldi);
imgold.src="/iv2/pxt_btn_onglet_left_off.png";
var imgold2=document.getElementById('img2_'+oldi);
imgold2.src="/iv2/pxt_btn_onglet_right_off.png";
var imgnew=document.getElementById('img_'+newi);
imgnew.src="/iv2/pxt_btn_onglet_left_on.png";
document.getElementById('class_'+newi).className='pxt_btn_onglet_on_txt';
document.getElementById('class_'+oldi).className='pxt_btn_onglet_off_txt';
document.getElementById('class2_'+oldi).className='pxt_btn_onglet_off';
document.getElementById('class2_'+newi).className='pxt_btn_onglet_on';
var imgnew2=document.getElementById('img2_'+newi);
imgnew2.src="/iv2/pxt_btn_onglet_right_on.png";
}

function popUpCentrer (page, largeur, hauteur, options)//JP070926
{
	var top  = (screen.height-hauteur)/2;
	var left = (screen.width-largeur) /2;
	window.open(page,"","top="+top+",left="+left+",width="+largeur+",height="+hauteur+","+options);
}


//Check position objet par rapport au bord haut de l'objet pass� en parametre - (peut etre a l'interieur d'un autre objet)
function getTop(MyObject0)
{
        coordotop = 0;
        MyObject = document.getElementById(MyObject0);
        if (MyObject.offsetParent)//si inclus dans un autre element
        {
                coordotop = MyObject.offsetTop;
                while ((MyObject = MyObject.offsetParent)!=null)
                {
                        coordotop += MyObject.offsetTop;
                }
        }
        else
        {
                coordotop = MyObject.offsetTop;
        }
}
function getLeft(MyObject0)
{
        coordoleft = 0;
        MyObject = document.getElementById(MyObject0);
        if (MyObject.offsetParent)//si inclus dans un autre element
        {
                coordoleft = MyObject.offsetLeft;
                while ((MyObject = MyObject.offsetParent)!=null)
                {
                        coordoleft += MyObject.offsetLeft;
                }
        }
        else
        {
                coordoleft = MyObject.offsetLeft;
        }
}
//PANIER-check si client a bien choisi un lieu de livraison + alert
function cache_div_alert()
{
	div_info_client=document.getElementById("div_confirmation");
	div_info_client.style.visibility = "hidden";
	div_info_client.style.display = "none";
}
	var client_a_choisi_liv="ko";
	//var nbre_radio = document.getElementsByName("choixliv").length;
	function check_choix_liv_client()
	{
		var nbre_radio = document.getElementsByName("choixliv").length;
		for (i=0;i<nbre_radio;i++)
		{
			if (document.getElementsByName("choixliv")[i].checked==true)
			{
				  client_a_choisi_liv = "ok";
			}
		}
		if(client_a_choisi_liv=="ok")
		{
			/*
			//message qui rappelle les fdp gratuits - debut
			var seuil = parseFloat(49);
			//var prix_total_commande = parseFloat(prix_total_commande);
			if(prix_total_commande>seuil)
			{
				document.forms["comm"].submit();
			}
			else
			{

				var reste_pour_coli_gratuit = seuil - prix_total_commande;
				reste_pour_coli_gratuit = reste_pour_coli_gratuit.toFixed(2);

				getTop("ancre_confirmation");
				getLeft("ancre_confirmation");
				//on decale un peu sur la gauche par rapport aux coordonn�es de l'ancre
				coordoleft -= 550;
				//on decale un peu en haut par rapport aux coordonn�es de l'ancre
				coordotop -= 100;
				//le calque
				div_info_client=document.getElementById("div_confirmation");
				div_info_client.style.width = '498px';
				//div_info_client.style.height = '80px';
				div_info_client.style.color = 'white';
				div_info_client.style.background = 'white';
				div_info_client.style.left = coordoleft+'px';
				div_info_client.style.top = coordotop+'px';
				div_info_client.style.backgroundRepeat = 'no-repeat';
			
				div_info_client.innerHTML='<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td colspan=\"2\"><a href=\'#\' onclick=\'document.forms["comm"].submit();\'><img src=\"http://images.grosbill.com/panier/checklivclient_01.gif\" width=\"498\" height=\"36\" /></a></td></tr><tr><td colspan=\"2\" background=\"http://images.grosbill.com/panier/checklivclient_02.gif\" align=\"center\">Compl�tez vos achats de<b> '+reste_pour_coli_gratuit +' euros</b> pour b�n�ficier de la livraison gratuite.<br>Frais de livraison offerts � partir de 49 euros  d\'achat jusqu\'au 31 janvier.</b></td></tr><tr><td><a href=\'#\' onclick=\'cache_div_alert();\'><img src=\"http://images1.grosbill.com/panier/checklivclient_03.gif\" width=\"249\" height=\"41\" border=\"0\"/></a></td><td><a href=\'#\' onclick=\'document.forms["comm"].submit();\'><img src=\"http://images.grosbill.com/panier/checklivclient_04.gif\" width=\"249\" height=\"41\" /></a></td></tr></table>';

				div_info_client.style.visibility = "visible";
				div_info_client.style.display = "inline";
			}	
			//message qui rappelle les fdp gratuits - FIN
			*/
			//Si desactivation du test, decommenter juste la ligne ci dessous
			document.forms["comm"].submit();

		}
		else if(client_a_choisi_liv=="ko")//si client n a pas choisi de lieu de livraieon
		{	

		//on prend les coordonnees de l'ancre
			getTop("ancre_confirmation");
			getLeft("ancre_confirmation");
			//on decale un peu sur la gauche par rapport aux coordonn�es de l'ancre		
			coordoleft -= 550;
			//on decale un peu en haut par rapport aux coordonn�es de l'ancre
	       		coordotop -= 100;
			//le calque
			div_info_client=document.getElementById("div_confirmation");
			div_info_client.style.width = '498px';
			div_info_client.style.height = '60px';
			div_info_client.style.left = coordoleft+'px';
			div_info_client.style.top = coordotop+'px';
			div_info_client.style.backgroundRepeat = 'no-repeat';
			//le message d'erreur differe si
			if (getCookie("ismagasin"))//si on est en mag
			{
				div_info_client.innerHTML='<a href=\'#\' onclick=\'cache_div_alert();\' ><img src=\'https://images.grosbill.com/checklivclient.gif\' width=\'498\' height=\'60\'>';
			}
			else//si pas en mag
			{
				div_info_client.innerHTML='<a href=\'#\' onclick=\'cache_div_alert();\' ><img src=\'https://images.grosbill.com/checklivclient.gif\' width=\'498\' height=\'60\'>';

			}	
			div_info_client.style.visibility = "visible";
			div_info_client.style.display = "inline";
		}
	
	
	}
//TDG mini boutiques - 350x285 > 2 emplacements
function change_div_tdg(no_du_div, contenu)
{
	var div_name = "innerDiv_"+no_du_div;
	var div_a_remplacer = document.getElementById(div_name);
	div_a_remplacer.innerHTML = contenu;
}
//JP080709 - pour stickers
function setCookieGarantie(idpdt, idext)
{
	var name = "extensiongarantie";
	var value = idpdt + "|" + idext + "#";
	//var duree = ";expires=";
	var domaine = ";domain=www.grosbill.com;"
	var dossier = ";path=/";
	////document.cookie = name + "=" + value + ";expires=;domain=www.grosbill.com;path=/";
	var the_cookie =  name + "=" + value ;
	var the_cookie = the_cookie + "path=/;";
	var the_cookie = the_cookie + "domain=grosbill.com;";
	document.cookie = the_cookie;
}
//JP081205 - popup flash
var newwin;

function launchwin(url, nomFenetre, parametres)
{
	newwin = window.open(url, nomFenetre, parametres);
	//if(javascript_version > 1.0)
	//{
		setTimeout('newwin.focus();',250);
	//}
}

function MM_openBrWindow(url, nomFenetre, parametres)
{ //v2.0
	window.open(url, nomFenetre, parametres);
}
