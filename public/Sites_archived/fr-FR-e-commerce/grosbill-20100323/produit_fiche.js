function endebug()
{
	if(document.cookie.indexOf("karel_debug")>=0)
		return true;
	return false;
}

var dispstatus=0;
var dispinit=0;
var dispdel=0;
var dispotimer;
var dispotimertip;

var dispstatus2=0;
var dispinit2=0;
var dispdel2=0;
var dispotimer;
var desc_timer;
var newdatadispo;
var newdatadispo2;
var dispoidold;
var dispoidold2;

var firstdispodiv=0;
var firstdispodiv2=0;
var firstbilltvdiv=0;
var flag_over_stock=0;
var islayerexist=0;
var islayerexist2=0;

var default_text_produit="<body><table><tr><td class=\"pxt_layer_t_left_co_off\"><img src=\"/bk.gif\" width=\"45\" height=\"48\" /></td><td class=\"w01 pxt_layer_t_bg\"></td><td class=\"pxt_layer_t_right_co_off\"><img src=\"/bk.gif\" width=\"50\" height=\"48\" alt=\"\" /></td></tr><tr><td class=\"pxt_layer_cl_bg\"></td><td class=\"pb10 WH\"><div class=\"pxt_layer_over\"><table width=\"340\" height=\"200\" class=\"normalpetit\"><tr valign=\"top\"><td colspan=\"2\" class=\"nomproduit2\">Chargement ...</td></tr></table></div></td><td class=\"pxt_layer_cr_bg\"></td></tr><tr><td class=\"pxt_layer_b_left_co_off\"><img src=\"/bk.gif\" width=\"45\" height=\"48\" alt=\"\" /></td><td class=\"pxt_layer_b_bg\"></td><td class=\"pxt_layer_b_right_co_off\"><img src=\"/bk.gif\" width=\"50\" height=\"48\" alt=\"\" /></td></tr></table>";

var default_text_dispo="<table><tr><td class=\"pxt_layer_t_left_co_off\"><img src=\"/bk.gif\" width=\"45\" height=\"48\" alt=\"\" /></td><td class=\"w01 pxt_layer_t_bg\"></td><td class=\"pxt_layer_t_right_co_off\"><img src=\"/bk.gif\" width=\"50\" height=\"48\" alt=\"\" /></td></tr><tr><td class=\"pxt_layer_cl_bg\"></td><td class=\"pb10 WH\"><div class=\"pxt_layer_over\"><table width=\"240\" class=\"normalpetit\"><tr valign=\"top\"><td class=\"nomproduit2 pdg10\">Chargement ...</td></tr></table></div></td><td class=\"pxt_layer_cr_bg\"></td></tr><tr><td class=\"pxt_layer_b_left_co_off\"><img src=\"/bk.gif\" width=\"45\" height=\"48\" alt=\"\" /></td><td class=\"pxt_layer_b_bg\"></td><td class=\"pxt_layer_b_right_co_off\"><img src=\"/bk.gif\" width=\"50\" height=\"48\" alt=\"\" /></td></tr></table>";

var firsttipdiv=0;
var oldtiptext="";
var tipdiv=document.createElement("div");
tipdiv.setAttribute("id", "dyntipplace");
tipdiv.style.zIndex="24";
tipdiv.style.display="block";
//tipdiv.style.visibility="hidden";
tipdiv.style.position="absolute";
tipdiv.style.fontFamily="Arial";
tipdiv.style.fontWeight="bold";
tipdiv.style.backgroundColor="white";
if (tipdiv.addEventListener) {
  tipdiv.addEventListener("mouseover",function () {window.clearTimeout(dispotimertip); },false);
  tipdiv.addEventListener("mouseout",function () { hide_div_tip(0); },false);
} else if (tipdiv.attachEvent) {
  tipdiv.attachEvent('onmouseover',function () {window.clearTimeout(dispotimertip); });
  tipdiv.attachEvent('onmouseout',function () { hide_div_tip(0); });
}

var dispodiv=document.createElement("div");
dispodiv.setAttribute("id", "dyndispoplace");
dispodiv.style.zIndex="2";
dispodiv.style.display="block";
dispodiv.style.visibility="hidden";
dispodiv.style.position="absolute";
dispodiv.innerHTML=default_text_produit;
if (dispodiv.addEventListener) {
	dispodiv.addEventListener("mouseover",function () { window.clearTimeout(dispotimer); },false);
	dispodiv.addEventListener("mouseout",function () { hide_div_produit(0); },false);
} else if (dispodiv.attachEvent) {
	dispodiv.attachEvent('onmouseover',function () { window.clearTimeout(dispotimer); });
	dispodiv.attachEvent('onmouseout',function () { hide_div_produit(0); });
}

var dispodiv2=document.createElement("div");
dispodiv2.setAttribute("id", "dyndispoplace2");
dispodiv2.style.zIndex="3";
dispodiv2.style.display="block";
dispodiv2.style.visibility="hidden";
dispodiv2.style.position="absolute";
//dispodiv2.innerHTML=default_text_dispo;
if (dispodiv2.addEventListener) {
	dispodiv2.addEventListener("mouseover",function () { window.clearTimeout(dispotimer); },false);
	dispodiv2.addEventListener("mouseout",function () { hide_div_produit(0); },false);
} else if (dispodiv2.attachEvent) {
	dispodiv2.attachEvent('onmouseover',function () { window.clearTimeout(dispotimer); });
	dispodiv2.attachEvent('onmouseout',function () { hide_div_produit(0); });
}

var billtvdiv=document.createElement("div");
billtvdiv.setAttribute("id", "dynbilltvplace");
billtvdiv.style.zIndex="99";
billtvdiv.style.border = "black";
billtvdiv.style.position="absolute";

//var dispodiv=document.getElementById('dyndispoplace');
//var dispodiv2=document.getElementById('dyndispoplace2');

var idp_old=0;
var gposition_x=0;
var gposition_y=0;
var gposition_x_save;
var gposition_y_save;
var pointeur_fixe=0;
var time_disp=new Array();
var pop_produits=new Array();

window.onload = initialisation;
function initialisation() {
	if (firstdispodiv2==0) {
		var tmpBody=document.getElementsByTagName("body")[0];
		tmpBody.appendChild(dispodiv2);
		firstdispodiv2=1;
	}
	if (firstdispodiv==0) {
		var tmpBody=document.getElementsByTagName("body")[0];
		tmpBody.appendChild(dispodiv);
		firstdispodiv=1;
	}
	if (firsttipdiv==0) {
		var tmpBody=document.getElementsByTagName("body")[0];
		tmpBody.appendChild(tipdiv);
		firsttipdiv=1;
	}
}


function getMousePos(evt) {
	if (document.all) {
		gposition_x=event.clientX;
		gposition_y=event.clientY;
		gpagepos_x = event.clientX + document.documentElement.scrollLeft;
		gpagepos_y = event.clientY + document.documentElement.scrollTop;
	} else if (document.getElementById) {
		gposition_x=evt.clientX;
		gposition_y=evt.clientY;
		gpagepos_x = evt.pageX;
		gpagepos_y = evt.pageY;
	}
	if (Math.abs(gposition_x_save-gposition_x)<2 || Math.abs(gposition_y_save-gposition_y)<2) {
		pointeur_fixe=1;
	}
	gposition_x_save=gposition_x;
	gposition_y_save=gposition_y;

	// catch possible negative values in NS4
	if (gpagepos_x < 0) {
		gpagepos_x = 0;
	}
	if (gpagepos_y < 0) {
		gpagepos_y = 0;
	}
	if(islayerexist>0){
		packprodlayer.style.left=eval(gpagepos_x+2)+"px";
		packprodlayer.style.top=eval(gpagepos_y+2)+"px";
	}
	if(islayerexist2>0){
		assoclayer.style.left=eval(gpagepos_x+2)+"px";
		assoclayer.style.top=eval(gpagepos_y+2)+"px";
	}
}

if (document.layers)
document.captureEvents(Event.MOUSEMOVE);
if (document.layers || document.all)
document.onmousemove = getMousePos;
if (document.addEventListener)
document.addEventListener('mousemove', getMousePos, true);

function hide_comparaison(id1,id2,id3,id4,id5,univers)
{
	divcomp=document.getElementById('id_comparaison');
	var xhr_object = null;

	if(window.XMLHttpRequest)
	{ // Firefox
		xhr_object = new XMLHttpRequest();
	}
	else if(window.ActiveXObject)
	{ // Internet Explorer
		xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
	}
	else
	{ // XMLHttpRequest non support� par le navigateur
		//alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
		return;
	}
	xhr_object.open("GET", 'http://www.grosbill.com/comparaisonv2.cgi?view=view&produit1='+id1+'&produit2='+id2+'&produit3='+id3+'&produit4='+id4+'&univers='+univers, true);
	xhr_object.onreadystatechange = function()
	{
		if(xhr_object.readyState != 4) {
			//newdatadispo=xhr_object.responseText;
			divcomp.style.visibility='hidden';

			divcomp.innerHTML='';
			//dispoidold=idproduit;
			//dispstatus=1;
		} else

		if(xhr_object.readyState == 4)
		{
			//document.write(xhr_object.responseText);
			//alert(xhr_object.responseText);
			if(xhr_object.status==200){
				newdatadispo=xhr_object.responseText;
				divcomp.style.visibility='visible';
				divcomp.innerHTML=newdatadispo;
				dispstatus=1;
			}
			//document.getElementById('toto').innerHTML=newdatadispo;
			return;
		}
	}
	xhr_object.send(null);
}

function catalogue(lien)
{

	var lien_http=lien;
	lien_http.replace("grosbill.com/frs/","grosbill.com/fr/");
	lien_http.replace("&head=no","");
	var load_timeout=window.setTimeout("window.location.replace('"+lien_http+"');",7000);

	var dispstatus=0;
	var dispinit=0;
	var dispdel=0;
	var dispotimer;

	var dispstatus2=0;
	var dispinit2=0;
	var dispdel2=0;
	var dispotimer;
	var newdatadispo;
	var newdatadispo2;
	var dispoidold;
	var dispoidold2;
	divcat=document.getElementById('catalogue');
	divcat.style.visibility='visible';
	divcat.style.display='block';

	divcat.innerHTML='<br><br><br><br><br><br><br><br><br><br><div align=\"center\"><img src=\"http://v2.grosbill.com/iv2/patienter.png\" width=\"419\" height=\"131\"></div>';

	var xhr_object = null;
	if(window.XMLHttpRequest)
	{ // Firefox
		xhr_object = new XMLHttpRequest();
	}
	else if(window.ActiveXObject)
	{ // Internet Explorer
		xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
	}
	else
	{ // XMLHttpRequest non support� par le navigateur
		//alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
		return;
	}

	xhr_object.open("GET", lien, true);
	xhr_object.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=iso-8859-1');
	xhr_object.onreadystatechange = function()
	{
		if(xhr_object.readyState != 4) {
			divcat.style.visibility='visible';
			divcat.style.display='block';
		}
		else if(xhr_object.readyState == 4)
		{
			if(xhr_object.status==200){
				newdatadispo=xhr_object.responseText;
				window.clearTimeout(load_timeout);
				divcat.style.visibility='visible';
				divcat.innerHTML=newdatadispo;

				var	PXT_typeid=document.getElementById('PXT_typeid').value;
				var pxtmode=document.getElementById('pxtmode').value;
				if (document.getElementById('icones'))
				{
					var picto=document.getElementById('icones').value;
					document.getElementById('picto').src='http://v2.grosbill.com/'+picto;
				}
				if (document.getElementById('filtre_id'))
				{
					var filtre_id=document.getElementById('filtre_id').value;
					PXT_switchdiv(filtre_id,'dest',true,1);

				}
				var	liste=document.getElementById('liste').value;
				var lien_split= lien.split('/frs/');
				var lien_good=lien_split[0]+'/fr/'+lien_split[1];
				calc_minipanier();
				function setCookie2(name, value, expires, path, domain, secure)
				{
					document.cookie= name + "=" + escape(value) +
					((expires) ? "; expires=" + expires : "") +
					((path) ? "; path=" + path : "") +
					((domain) ? "; domain=" + domain : "") +
					((secure) ? "; secure" : "");
				}
				function deleteCookie2(name, path, domain)
				{
					if (getCookie(name))
					{
						document.cookie = name + "=" +
						((path) ? "; path=" + path : "") +
						((domain) ? "; domain=" + domain : "") +
						"; expires=Thu, 01-Jan-70 00:00:01 GMT";
					}
				}
				//JP071114-setCookie2('url_back',PXT_typeid+'~'+lien_good+'~'+liste,'','/');
				setCookie2('url_back',PXT_typeid+'~'+lien_good+'~'+liste,'','/','.grosbill.com','');

	PXT_typeid=32;
				YAHOO.DDApp.init();
				YAHOO.DDApp2.init();
				dispstatus=1;
			}
			return;
		}
	}

	xhr_object.send(null);
}

function close_div_video ()
{
	firstbilltvdiv = 0;
	billtvdiv.style.display = 'none';
	billtvdiv.innerHTML = "";
}

function show_div_video (billtvurl)
{
	if (firstbilltvdiv==0)
	{
      var tmpBody=document.getElementsByTagName("body")[0];
      tmpBody.appendChild(billtvdiv);
		firstbilltvdiv=1
	}

	//alert("billtv: "+billtvdiv.style.display+"\nvisibility"+billtvdiv.style.visibility+"\n"+billtvdiv.innerHTML);
	if(billtvdiv.style.display!='block')
	{
      var largeur=window.screen.availWidth;
      if (typeof(window.innerWidth) == 'number')
		{
      	largeur = window.innerWidth;
      }
		else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight))
		{
      	largeur = document.documentElement.clientWidth;
      }
		else if (document.body && (document.body.clientWidth || document.body.clientHeight))
		{
      	largeur = document.body.clientWidth;
      }


		billtvdiv.style.left = eval(largeur/2 - 130 - 50)+'px';
//		billtvdiv.style.top = '50px';//avec frame
		billtvdiv.style.top = '176px';//sans frame
//		billtvdiv.style.width = '260px';
//		billtvdiv.style.height = '320px';

		newdatabilltv = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0' height='320' width='260'>  <param name='movie' value='"+billtvurl+"'>          <param name='quality' value='high'>          <embed src='"+billtvurl+"' quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer' type='application/x-shockwave-flash' height='320' width='260'></object>";
		//newdatabilltv = "televiseur";
		billtvdiv.innerHTML = '<table><tr><td class="pxt_layer_t_left_co_off"><img src="/bk.gif" alt="" height="48" width="45"></td><td class="w01 pxt_layer_t_bg"><p align="right"><a href="#" onclick="javascript:close_div_video();" style="position: relative;z-index:1;"><img src="/iv2/pxt_btn_croix.png" alt=""></a></p></td><td class="pxt_layer_t_right_co_off"><img src="/bk.gif" alt="" height="48" width="50"></td></tr><tr><td class="pxt_layer_cl_bg"></td><td class="pb10 WH">'+newdatabilltv+'<td class="pxt_layer_cr_bg"></td></tr><tr><td class="pxt_layer_b_left_co_off"><img src="/bk.gif" alt="" height="48" width="45"></td><td class="pxt_layer_b_bg"></td><td class="pxt_layer_b_right_co_off"><img src="/bk.gif" alt="" height="48" width="50"></td></tr></table>';
		billtvdiv.style.display='block';
	}
}

function show_div_stock (st, idproduit,univers,mode)
{
	//hide_div_stock(1);
	if (firstdispodiv2==0) {
		var tmpBody=document.getElementsByTagName("body")[0];
		tmpBody.appendChild(dispodiv2);
		firstdispodiv2=1;
	}
	window.clearTimeout(dispotimer);
	dispdel2=0;
	flag_over_stock=1;
	if (st>0 && dispstatus2<1)
	{
		if (mode==1) {
			var tmp_top=dispodiv.style.top;
			var tmp_top=eval(tmp_top.substring(0,tmp_top.indexOf('px')))+100;
			var tmp_left=dispodiv.style.left;
			var tmp_left=eval(tmp_left.substring(0,tmp_left.indexOf('px')))+150;
			dispodiv2.style.left = tmp_left+'px';
			dispodiv2.style.top = tmp_top+'px';
		} else {
			var c = getAnchorPosition('id_stock_'+idproduit);
			//JP0812 - 140 avant Frais de resa
			dispodiv2.style.left = eval(c.x-170)+'px';
			dispodiv2.style.top = eval(c.y+30)+'px';
		 	//dispodiv2.style.left=(gpagepos_x+2)+"px";
			//dispodiv2.style.top=(gpagepos_y+2)+"px";
		}
		dispodiv2.style.visibility="visible";
		if(idproduit==0){
			idproduit=dispoidold2;
		}
		if(dispoidold2!=idproduit){
			newdatadispo2=call_xml_stock(idproduit);
		}
		else if(flag_over_stock==1)
		{
			dispodiv2.style.visibility='visible';
		}
	}
}


function hide_div_stock(ht){
	flag_over_stock=0;
	dispodiv2.style.visibility='hidden';
	dispodiv2.innerHTML=default_text_dispo;
	dispstatus2=0;
	dispdel2=0;
}

function show_div_produit(st,idproduit,couleur,tdg)
{
	if (firstdispodiv==0) {
		var tmpBody=document.getElementsByTagName("body")[0];
		tmpBody.appendChild(dispodiv);
		firstdispodiv=1;
	}
	try
	{
		if (idp_old!=idproduit) {
			idp_old=idproduit;
			pop_produits[idproduit]="";
			time_disp[idproduit]=window.setTimeout('show_div_produit_suite('+st+','+idproduit+',\''+couleur+'\',\''+tdg+'\','+gposition_x+','+gposition_y+')',300);
			tmp_idpdiv=document.getElementById('dd_'+idproduit);
			var c = getAnchorPosition('dd_'+idproduit);
			c.l=document.getElementById('dd_'+idproduit).offsetWidth;
			var b_pos= getAnchorPosition('position_bas');
			var position;
			var fleche;
			var fleche2;
			var largeur=window.screen.availWidth;
			if (typeof(window.innerWidth) == 'number') {
				largeur = window.innerWidth;
			} else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
				largeur = document.documentElement.clientWidth;
			} else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
				largeur = document.body.clientWidth;
			}
			position=largeur-c.l-c.x*2;
			gposition=position;
			gposition_x_save=(position>0) ? c.x : eval(c.x+c.l) ;
			var position_y;
			if (b_pos.y>0)
			{
				// patch pour IE en AJAX
				position_y=eval(b_pos.y)-eval(c.y);
			}
			else
			{
				position_y=0;
			}
			if (document.all) {
				position_y=document.documentElement.scrollTop+document.body.clientHeight/2-c.y;
			} else {
				position_y=window.pageYOffset+window.innerHeight/2-c.y;
			}
			if (position<0)
			{
				dispodiv.style.left = eval(c.x-420)+'px';
				fleche=1;
			}
			else
			{
				dispodiv.style.left = eval(c.x+c.l-20)+'px';
				fleche=0;
			}
			if (position_y>0)
			{
				fleche2=0;
				dispodiv.style.top = eval(c.y+20)+'px';
			}
			else
			{
				fleche2=1;
				dispodiv.style.top = eval(c.y-160)+'px';
			}
			call_xml(idproduit,couleur,position,fleche,fleche2,tdg);
			if (typeof(time_disp[idproduit])!=undefined) {
				if (tmp_idpdiv.addEventListener) {
					tmp_idpdiv.addEventListener("mouseout",function () { window.clearTimeout(time_disp[idproduit]); },false);
				} else if (tmp_idpdiv.attachEvent) {
					tmp_idpdiv.attachEvent('onmouseout',function () { window.clearTimeout(time_disp[idproduit]); });
				}
			}
		}
		idp_old=idproduit;
	}
	catch(e)
	{
		//alert("E/"+e);
		window.clearTimeout(time_disp[idproduit]);
	}
}

function show_div_produit_suite(st,idproduit,couleur,tdg,posX,posY)
{
	if (firstdispodiv==0) {
		var tmpBody=document.getElementsByTagName("body")[0];
		tmpBody.appendChild(dispodiv);
		firstdispodiv=1;
	}
	try
	{
		if ((Math.abs(posX-gposition_x)<2 || Math.abs(posY-gposition_y)<2) && idp_old==idproduit && pointeur_fixe==1)
		{
			hide_div_produit(1);
			dispodiv.style.visibility='visible';
			if (pop_produits[idproduit]!="")
			{
				if(ispxtdrag<1)
				{
					// If NS -- that is, !IE -- then set up for mouse capture
					// Set-up to use getMouseXY function onMouseMove
					dispdel=0;
					if (st>0 && dispstatus<1)
					{
						dispodiv.innerHTML=pop_produits[idproduit];
						var tmp_span=document.getElementById("idspan_stock_"+idproduit);
						if (tmp_span.addEventListener) {
							tmp_span.addEventListener("mouseover",function () { show_div_stock(1,idproduit,"",'1'); },false);
							tmp_span.addEventListener("mouseout",function () { hide_div_stock(0); },false);
						} else if (tmp_span.attachEvent) {
							tmp_span.attachEvent('onmouseover',function () { show_div_stock(1,idproduit,"",'1'); });
							tmp_span.attachEvent('onmouseout',function () { hide_div_stock(0); });
						}
					}
				}
			}
			else
			{
				time_disp[idproduit]=window.setTimeout('show_div_produit_suite('+st+','+idproduit+',\''+couleur+'\',\''+tdg+'\','+gposition_x+','+gposition_y+')',20);
			}
		}
		else if (idp_old==idproduit)
		{
			time_disp[idproduit]=window.setTimeout('show_div_produit_suite('+st+','+idproduit+',\''+couleur+'\',\''+tdg+'\','+gposition_x+','+gposition_y+')',300);
		}
		idp_old=idproduit;
	}
	catch(e)
	{
		window.clearTimeout(time_disp[idproduit]);
	}
}

var dispstatus=0;
var dispinit=0;
var dispdel=0;
function hide_div_produit(ht){
	window.clearTimeout(dispotimer);
	if (dispodiv2!=null) {
		hide_div_stock(0);
	}
	idp_old=0;
	if(ht<1){
		dispotimer=window.setTimeout('hide_div_produit(1)', 250);
	}
	else
	{
		/*
		if (! dispodiv)
		{
			dispodiv=document.getElementById('dyndispoplace');
		}
		*/
		dispodiv.style.visibility='hidden';
		dispodiv.innerHTML=default_text_produit;
		dispstatus=0;
		dispdel=0;
	}
}

function call_xml_stock(idproduit)
{
	if(ispxtdrag<1){
		var xhr_object = null;
		if(window.XMLHttpRequest)
		{ // Firefox
			xhr_object = new XMLHttpRequest();
		}
		else if(window.ActiveXObject)
		{ // Internet Explorer
			xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
		}
		else
		{ // XMLHttpRequest non support� par le navigateur
			//alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
			return;
		}
		xhr_object.open("GET", "http://www.grosbill.com/stockv2.cgi?p1="+idproduit, true);
		xhr_object.onreadystatechange = function()
		{
			if(xhr_object.readyState != 4) {
				//dispodiv2.style.visibility="hidden";
			}
			else if(xhr_object.readyState == 4)
			{
				if(xhr_object.status==200)
				{
					newdatadispo2=xhr_object.responseText;
					if (flag_over_stock==1) {
						dispodiv2.style.visibility="visible";
					}
					dispodiv2.innerHTML=newdatadispo2;
					dispoidold=idproduit;
					dispstatus=1;
				}
				return;
			}
		}
		xhr_object.send(null);
	}
}

function centerLeft(width)
{
	var largeur=window.screen.availWidth;
	if (typeof(window.innerWidth) == 'number')
	{
		largeur = window.innerWidth;
	}
	else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight))
	{
		largeur = document.documentElement.clientWidth;
	}
	else if (document.body && (document.body.clientWidth || document.body.clientHeight))
	{
		largeur = document.body.clientWidth;
	}

	return eval(largeur)/2-eval(width)/2;

}

function show_div_popup(url, iLeft, iTop, iWidth, iHeight)
{
	try {
		kpopup=document.getElementById('karel_popup');
		kpopup.style.visibility="hidden";
		kpopup.style.left=iLeft+"px";
		kpopup.style.top=iTop+"px";
		kpopup.style.height=iHeight+"px";
		kpopup.style.width=iWidth+"px";
	}
	catch(e)
	{
		var kpopup=document.createElement("div");
		kpopup.setAttribute("id", "karel_popup");
		kpopup.style.zIndex="3";
		kpopup.style.display="block";
		kpopup.style.visibility="hidden";
		kpopup.style.position="absolute";
		kpopup.style.left=iLeft+"px";
		kpopup.style.top=iTop+"px";
		kpopup.style.height=iHeight+"px";
		kpopup.style.width=iWidth+"px";
		var tmpBody=document.getElementsByTagName("body")[0];
		tmpBody.appendChild(kpopup);
	}
	//alert(""+"'"+iLeft+"'"+"'"+iTop+"'"+"'"+iWidth+"'"+"'"+iHeight+"'");

	kpopup.innerHTML = "<iframe src=\""+url+"\" style=\"width: "+iWidth+"px; height: "+iHeight+"px;\"></iframe>";
	kpopup.style.visibility="visible";
	/*
	var xhr_object = null;
	if(window.XMLHttpRequest)
	{ // Firefox
		xhr_object = new XMLHttpRequest();
	}
	else if(window.ActiveXObject)
	{ // Internet Explorer
		xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
	}
	else
	{ // XMLHttpRequest non support� par le navigateur
		//alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
		return;
	}

	xhr_object.open("GET", url, true);
	xhr_object.onreadystatechange = function()
	{
		if(xhr_object.readyState != 4) {
			//dispodiv.style.visibility="hidden";
			//return;
		} else if(xhr_object.readyState == 4)
		{
			if(xhr_object.status==200){
				dispodiv.style.width="300px";
				kpopup.innerHTML = xhr_object.responseText;
				kpopup.style.visibility="visible";
			} else {
				alert("Erreur AJAX");
			}
			return;
		}
	}
	xhr_object.send(null);
	*/

}

function call_xml(idproduit,couleur,position,fleche,fleche2,tdg)
{
//	alert("/fiche_xml.cgi?p1="+idproduit+"&univers="+couleur+"&fleche="+fleche+"&fleche2="+fleche2+"&tdg="+tdg);
	if(ispxtdrag<1)
	{
		var xhr_object = null;
		if(window.XMLHttpRequest)
		{ // Firefox
			xhr_object = new XMLHttpRequest();
		}
		else if(window.ActiveXObject)
		{ // Internet Explorer
			xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
		}
		else
		{ // XMLHttpRequest non support� par le navigateur
			//alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
			return;
		}

		xhr_object.open("GET", "http://www.grosbill.com/fiche_xml.cgi?p1="+idproduit+"&univers="+couleur+"&fleche="+fleche+"&fleche2="+fleche2+"&tdg="+tdg, true);
		xhr_object.onreadystatechange = function()
		{
			if(xhr_object.readyState != 4) {
				//dispodiv.style.visibility="hidden";
				//return;
			} else if(xhr_object.readyState == 4)
			{
				if(xhr_object.status==200){
					dispodiv.style.width="300px";
					pop_produits[idproduit]=xhr_object.responseText;
					dispoidold=idproduit;
					dispstatus=1;
				} else {
					//alert("Erreur AJAX");
				}
				return;
			}
		}
		xhr_object.send(null);
	}
}



var CpathIMG;
function zoomIMG(pathIMG) {
	CpathIMG=pathIMG;
	document.getElementById('zoom').src = "http://images.grosbill.com/imagesproduitnew/imagesgallery/zoom_250/"+pathIMG+".jpg";
	var test=1;
}
function PopupCentrer(page,largeur,hauteur,options) {
	var top=(screen.height-hauteur)/2;
	var left=(screen.width-largeur)/2;
	window.open(page,"","top="+top+",left="+left+",width="+largeur+",height="+hauteur+","+options);
}
function popUpZoom(){
	day = new Date();
	id = day.getTime();
	if(CpathIMG.length< 10){ eval("page" + id + " = PopupCentrer('http://images.grosbill.com/imagesproduitnew/imagesgallery/zoom/'+CpathIMG+'.jpg', 900,700, 'menubar=no,status=no');");}
}
function popUpZoom2()
{
	day = new Date();
	id = day.getTime();
	if(CpathIMG.length< 10){ eval("page" + id + " = PopupCentrer('http://www.grosbill.com/html/zoom_image.html?img=imagesproduitnew/imagesgallery/zoom/'+CpathIMG+'.jpg', 900,700, 'menubar=no,status=no');");}
}
function popUpZoom3(pathIMG)
{
day = new Date();
id = day.getTime();
CpathIMG=pathIMG;
if(CpathIMG.length< 10){ eval("page" + id + " = PopupCentrer('http://www.grosbill.com/html/zoom_image.html?img=imagesproduitnew/imagesgallery/zoom/'+CpathIMG+'.jpg', 900,700, 'menubar=no,status=no');");}
}
function popUpZoom4(pathIMG)
{
day = new Date();
id = day.getTime();
CpathIMG=pathIMG;
if(CpathIMG.length< 10){ eval("page" + id + " = PopupCentrer('http://www.grosbill.com/html/zoom_image.html?img=imagesproduitnew/images800jpg/'+CpathIMG+'.jpg', 900,700, 'menubar=no,status=no');");}
}



//affiche un zoom de l'image avec lien pour fermer -> ?img=idpdt - JP071204
function popUpZoomImg()
{
	var url_img = "";
	var requete = window.location.search;
	//si parametre pass�
	if (requete)
	{//si argument pass� dans l'url
		//on se place a la 5eme position pour ne recuperer que l'id
		requete=requete.substring(5);
		url_img = '<a href="javascript:void(0);" onClick="window.close();"><img src="http://images.grosbill.com/'+requete+'" border="0"><a>';
	}
	else
	{//si pas d'argument pass� dans l'url
		url_img = '<a href="javascript:void(0);" onClick="window.close();"><img src="http://images.grosbill.com/iv2/acc_deg_05.gif" border="0"><a>';
	}
	////requete = unescape(requete);
	return(url_img);
}


function popUp(URL) {
	day = new Date();
	id = day.getTime();
	eval("page" + id + " = window.open(URL, 'http://v2.grosbill.com/gallery/toto.php?id=" + id + "', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=1,width=900,height=700,left = 750,top = 550');");
}


function showtip(text)
{
	if(firsttipdiv==0)
	{
		var tmpBody=document.getElementsByTagName("body")[0];
		tmpBody.appendChild(tipdiv);
		firstdispodiv=1;
	}

	if(1||oldtiptext!=text)
	{
		hide_div_tip(1);
		tipdiv.innerHTML=text;
 	 	tipdiv.style.top=gposition_y+3;
  		tipdiv.style.left=gposition_x+3;
		//if(endebug()) alert('keil'+gposition_x+':'+gposition_y);
  		tipdiv.style.visibility="visible";
		oldtiptext=text;
	}
}

function hide_div_tip(ht){
  window.clearTimeout(dispotimertip);
  if(ht<1){
	 dispotimertip=window.setTimeout('hide_div_tip(1)', 250);
  }
  else
  {
	 tipdiv.style.visibility='hidden';
	 oldtiptext="";
  }
}

