/* Modules de gestion de cookie pour afficher / masquer le promoTrip - Dur�e de vie = fin de journ�e */
/* function ScanCookie(variable) {
	cook = document.cookie;
	variable += "=";
	place = cook.indexOf(variable,0);
	if (place <= -1)
		return("0");
	else {
		end = cook.indexOf(";",place)
		if (end <= -1)
			return(unescape(cook.substring(place+variable.length,cook.length)));
		else
			return(unescape(cook.substring(place+variable.length,end)));
	}
}

function CreationCookie(nom,valeur) {
	var tabNbDay = new Array(31,28,31,30,31,30,31,31,30,31,30,31);
	var date = new Date(); var year = date.getFullYear(); var month = date.getMonth(); var day = date.getDate(); day++; 
	if (day > tabNbDay[month]){ day = 1; month++; }
	if (month > 12){ month = 1; year++; }
	dateExp = new Date(year,month,day);
	dateExp = dateExp.toGMTString();
	document.cookie = nom + '=' + escape(valeur) + '; expires=' + dateExp + ';';
}

function initializeCook() {
	if(ScanCookie("promoTrip") == 0) {
		CreationCookie("promoTrip","oui"); 
	}
	document.getElementById('sousnav').style.display = "none";
}

function scanCookPromTrip() {
	if(ScanCookie("promoTrip") == 'oui')
		document.getElementById('sousnav').style.display = "none";
} */

/* --------- Gestion des param�tres URL --------- */
list = new Array; 	
list_valeur = new Array;
pos = document.location.href.indexOf("?",0);
if(pos >= 0){
	requete = document.location.href.substring(pos+1, document.location.href.length);
	pos_egal = requete.indexOf("=",0);
	if (pos_egal > 0){
		pos_and = requete.indexOf("&",0);
		if(pos_and >= 0)
			list = requete.split("&");
		else
			list[0] = requete;
		for(i=0;i<list.length;i++){
			temp = list[i].split("=");
			variable = temp[0];
			valeur = temp[1];
			plus_pos = valeur.indexOf("+",0);
			while (plus_pos >-1){
				temp2 = valeur.substring(0,plus_pos) + ' ' + valeur.substring(plus_pos+1,valeur.length);
				valeur = temp2;
				plus_pos = valeur.indexOf("+",plus_pos+2);
			}
			list_valeur[variable] = valeur;
		}
	}
}
function get(get){
	if (list_valeur[get])
		return(unescape(list_valeur[get]));
	else
		return("");
}

/* --------- Module d'affichage des sous-Menu IE6 + Navigation clavier --------- */
var oldSousmenu = "";
function showMenu(idRef,UpAndDown){
	var sousMenu = "ul_ssMenu_" + idRef.slice(idRef.indexOf("_") + 1,idRef.length);
	var onglet = "li_" + idRef.slice(idRef.indexOf("_") + 1,idRef.length);
	showUnderMenu(onglet,sousMenu,UpAndDown);
	if (oldSousmenu != idRef){
		if (oldSousmenu != ''){
			var oldMenu = "ul_ssMenu_" + oldSousmenu.slice(oldSousmenu.indexOf("_") + 1,oldSousmenu.length);
			var oldOnglet = "li_" + oldSousmenu.slice(oldSousmenu.indexOf("_") + 1,oldSousmenu.length);
			showUnderMenu(oldOnglet,oldMenu,false);
		}
		oldSousmenu = idRef;
	}
}
function showUnderMenu(idTab,idMenu,UpAndDown){
	document.getElementById(idMenu).style.display = (UpAndDown) ? "block" : "none";
	document.getElementById(idTab).style.backgroundColor = (UpAndDown) ? "#FF6600" : "#990000";
	if (idTab == 'li_promo'){
		document.getElementById('a_promo').style.backgroundColor = (UpAndDown) ? "#FF6600" : "#FFCC66";
		document.getElementById('a_promo').style.color = (UpAndDown) ? "#FFFFFF" : "#990000";
	}
}
function write_ssMenu(id){
	var URL = ""; var LABEL = "";var idDiv="";
	switch(id){
		case 'vol' :	URL = "http://www.opodo.fr/billets-avion/";
						LABEL = "Tous nos vols";
						idDiv ="allVols";
						break;
		case 'sejour' :	URL = "http://www.opodo.fr/sejours/";
						LABEL = "Tous nos s&eacute;jours";
						idDiv ="allSejours";
						break;
		case 'promo' :	URL = "http://www.opodo.fr/Promos/";
						LABEL = "Toutes nos promos";
						idDiv ="allPromos";
						break;
		case 'france' :	URL = "http://locations.opodo.fr/default.aspx?saison=E";
						LABEL = "Locations Printemps-Et&eacute;";
						idDiv ="vacancesFrance";
						break;
		case 'weekend':
						URL = 'http://www.opodo.fr/week-ends/';
						LABEL = 'Tous nos week-ends';
						break;
	}
 	if (URL != '')
		document.write("<li id=\""+idDiv+"\"><a href=\"" + URL + "\" target=\"_top\">" + LABEL + "</a></li>");
}

/* --------- CSS Patcher --------- */
var nameNavigator = ""; 
var versionNavigator = "";
var strChUserAgent = navigator.userAgent;
var navigNameVersion = new Array();
var cssStyle = "/css_opodo/header2/headerFooter";
navigNameVersion = strChUserAgent.split(" ");
for(i=0; i<navigNameVersion.length; i++){
	if (navigNameVersion[i].indexOf("Chrome") == 0){
		nameNavigator = "Chrome";
		break;
	}
	if (navigNameVersion[i].indexOf("Safari") == 0){
		nameNavigator = "Safari";
		break;
	}
	if (navigNameVersion[i].indexOf("Opera") == 0){
		nameNavigator = "Opera";
		break;
	}
}
if (nameNavigator != '') {
	document.write("<link href=\"" + cssStyle + "_" + nameNavigator + versionNavigator + ".css\" rel=\"stylesheet\" type=\"text/css\" />");
}

/* --------- CSS Debug Patcher --------- */
var URLTab = new Array;
var CssPatcherDebug = "";
var versionNavigator = ""
URLTab = document.location.href.split("/");
for(k=0; k<navigNameVersion.length; k++){
	if (navigNameVersion[k].indexOf("Safari") == 0){
		nameNavigator = "_Safari";
		break;
	}
	if (navigNameVersion[k].indexOf("Opera") == 0){
		nameNavigator = "_Opera";
		break;
	}
	if (navigNameVersion[k].indexOf("MSIE") == 0){
		nameNavigator = "_ie";
		var versionNavig = navigator.appVersion.slice(0,25);
		if (parseInt(versionNavig.slice(versionNavig.length - 3, versionNavig.length - 2)) < 7)
				versionNavigator = "6-";
		else
				versionNavigator = ""; 
		break;
	}
}

/* Gestion du MultiStop Central */
if ((URLTab[URLTab.length - 1].slice(0,16) == 'multistopFlights')
	|| (URLTab[URLTab.length - 1].slice(0,16) == 'multistopDetails')
	|| (URLTab[URLTab.length - 1].slice(0,7) == 'results')
	|| (URLTab[URLTab.length - 1].slice(0,19) == 'airv2SwitchToBasket')){ 
	CssPatcherDebug = "multiStopFlight";
}
if (URLTab[URLTab.length - 1].slice(0,22) == 'multistopFlightsSearch'){ 
	CssPatcherDebug = "multiStopFlight-patch";
}
if ((URLTab[URLTab.length - 1].slice(0,21) == 'airv2RenderTravellers')
	|| (URLTab[URLTab.length - 1].slice(0,13) == 'airv2Register')){ 
	CssPatcherDebug = "multiStopFlight2";
	versionNavigator = ""; 
}
if ((URLTab[URLTab.length - 1].slice(0,21) == 'hotelv2SwitchToBasket')
	|| (URLTab[URLTab.length - 1].slice(0,12) == 'HotelBooking')
	|| (URLTab[URLTab.length - 1].slice(0,16) == 'hotelv2BookingV2')){
	CssPatcherDebug = "hotelToBasketCentral";
	versionNavigator = ""; 
}
if (URLTab[URLTab.length - 1].slice(0,17) == 'carSwitchToBasket'){
	CssPatcherDebug = "carSwitchToBasket";
	versionNavigator = ""; 
}
if (URLTab[URLTab.length - 1].slice(0,7) == 'flights'){
	CssPatcherDebug = "flightToBasketCentral";
}
if (URLTab[URLTab.length - 1].slice(0,6) == 'search'){
	CssPatcherDebug = "flightToBasketCentral-patch";
}

/* Gestion des pages SEO billet-avion.opodo.fr */
if (URLTab[2] == 'billet-avion.opodo.fr'){
	CssPatcherDebug = "billetAvionOpodoFr";
	versionNavigator = ""; 
}

/* Gestion des pages centrales Vols billetavion.opodo.fr
if (URLTab[2] == 'billetavion.opodo.fr'){
	CssPatcherDebug = "billetAvionOpodoFr-central";
}
if ((URLTab[2] == 'billetavion.opodo.fr') && (URLTab[URLTab.length - 1].slice(0,6) == 'search')){
	CssPatcherDebug = "billetAvionOpodoFr-centralSearch";
}
if ((URLTab[2] == 'billetavion.opodo.fr') && (URLTab[URLTab.length - 1].slice(0,21) == 'airv2RenderTravellers')){
	CssPatcherDebug = "billetAvionOpodoFr-centralBasket";
}
*/

/* Gestion de la page Index SEO vol.opodo.fr */
if ((URLTab[2] == 'vol.opodo.fr') && (URLTab[3] == '')){
	CssPatcherDebug = "volOpodoFrSeo";
}

/* Gestion des pages Location France + Ski */
if (URLTab[2] == 'locations.opodo.fr'){
		if (get("saison") == 'E'){
			CssPatcherDebug = "locationOpodoFr";
			versionNavigator = ""; 
		}
		if (get("saison") == 'H'){
			CssPatcherDebug = "locationSkiOpodoFr";
			versionNavigator = ""; 
		}
}

/* Gestion de la page Transfert */
if (URLTab[2] == 'transferts.opodo.fr'){
	CssPatcherDebug = "transfertOpodoFr";
	versionNavigator = ""; 
}

/* Gestion des pages OhMyGlobe! */
if (URLTab[2] == 'www.ohmyglobe.com'){
	CssPatcherDebug = "ohMyGlobe";
	versionNavigator = ""; 
}

/* Gestion des pages Eurostar + H�tel */
if (URLTab[2] == 'www.advences.com'){
	CssPatcherDebug = "eurostarHotel";
	versionNavigator = ""; 
}

/* Gestion des pages Campings */
if (URLTab[2] == 'camping.opodo.fr'){
	CssPatcherDebug = "campingOpodoFr";
	versionNavigator = ""; 
}

/* Gestion des pages de d�sinscription */
if (URLTab[2] == 'newsletter.opodo.fr'){
	CssPatcherDebug = "newsletterOpodoFr";
}

/* Gestion des pages SEO Agence de Voyage */
if (URLTab[2] == 'agence-de-voyage.opodo.fr'){
	CssPatcherDebug = "agenceVoyageOpodoFr";
}

/* Gestion des pages France */
if (URLTab[2] == 'vacances.opodo.fr'){
	CssPatcherDebug = "vacancesOpodoFr";
}

/* Gestion des pages France */
if (URLTab[2] == 'voyages.opodo.fr'){
	CssPatcherDebug = "voyagesOpodoFr";
	versionNavigator = ""; 
}

/* Gestion des pages SEO Ski */
if (URLTab[2] == 'ski.opodo.fr'){
	CssPatcherDebug = "skiOpodoFr";
	versionNavigator = ""; 
}

if (CssPatcherDebug != ''){
	document.write("<link href=\"https://www.opodo.fr/css_opodo/header2/debug/" + CssPatcherDebug + nameNavigator + versionNavigator + ".css\" rel=\"stylesheet\" type=\"text/css\" />");
}

if (((URLTab[URLTab.length - 1].slice(0,22) == 'AirAvailabilityServlet') || (URLTab[URLTab.length - 2] == 'nos-valeurs') || (URLTab[URLTab.length - 2] == 'presse')) && (strChUserAgent.indexOf("MSIE") > 0)){
	document.write("<link href=\"https://www.opodo.fr/css_opodo/header2/debug/debugAlignSky.css\" rel=\"stylesheet\" type=\"text/css\" />");
}
