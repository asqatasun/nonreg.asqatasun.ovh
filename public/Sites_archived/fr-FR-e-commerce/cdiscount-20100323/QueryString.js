var QueryStringList = new Array();

/*function to add query*/
function AddQuery(key,value) 
{
    if (!document.cookie) 
    {
		QueryStringList[key]=value;
		//alert(key + ' : ' + value);
    }
}

/* Use to add server defined querystring */
function BuildQueryString() 
{
	var j,i,key,newref;
	if (!document.cookie)
    {
		/* Build queryStringExtension */
        var queryExtension = '' ;
        for (key in QueryStringList)
        {
            if (typeof(QueryStringList[key]) != 'function' && QueryStringList[key] != '') {
                if (queryExtension != '') {
                    queryExtension = queryExtension + '&' + key + "=" + QueryStringList[key];
                } else {
                    queryExtension =  key + "=" + QueryStringList[key];
                }
            }
       }

        if (queryExtension != '')
        {
            /* Add Query String to a tag */
			var anchorTab = document.getElementsByTagName('a');
			//alert(anchorTab.length);
			for(i=0; i<anchorTab.length; i++) 
            {
                var href = anchorTab[i].href;
                /* Check if this link is internal to the site*/
                if (
                        (
                            href.indexOf(RootPath) >= 0
                            || href.indexOf('/')   == 0
                        ) 
                            &&
                        (   
                            href.indexOf('javascript:') == -1
                        //    && href.indexOf('#') == -1
                        )
                    )
                {
					for (key in QueryStringList)
                    {
						newref = AddQueryString(anchorTab[i].href, key, QueryStringList[key]);
						anchorTab[i].href = newref;
                    }
                }
            }
        }
    }
}

/* Build the path in addind or remplacing the querystring */
function AddQueryString(href,key,value)
{
    var queryExtension =  key + "=" + value;
    var n = href.indexOf('?')
	var ancor = '';
	var res;
	if (href.indexOf('#')>=0)
	{
		var tabTmp = href.split('#');
		href = tabTmp[0];
		ancor = '#' + tabTmp[1];
	}
    if (n>0) {
        var queryString = href.substring(n+1);
		if ((queryString.indexOf(key + '='))>=0)
        {
			var queryStringTab = new Array();
            queryStringTab = queryString.split('&');
			res = href.substring(0,n) + '?';
            var item;
			var str;
            for (item in queryStringTab)
            {
				str = queryStringTab[item];
				if (str.indexOf(key + '=')>=0)
                {
                    str = queryExtension;
                }
				if (item ==0)
				{
					res = res + str;
				}
				else
				{
					res = res + '&' + str;
				}
				//alert('item : ' + queryStringTab[item] + ' contain : ' + (queryStringTab[item].indexOf(key + '=')>=0) + ' key :' + key + ' res :
            }
        }
        else
        {
            res = href + '&' + queryExtension;
        }
    } else {
        res= href + '?' + queryExtension;
    }
	//alert('In AddQueryString href : ' + href + ' key : ' + key + ' value  ' + value + ' res : ' + res);
	res = res + ancor;
	return res;
}

/* Redirect function (Cookieless care)*/
function Redirect(path) 
{
    if (path.indexOf(RootPath)<0)
        path = RootPath + path;
    if (!document.cookie) 
    {
        var queryExtension = '' ;
        for (key in QueryStringList)
        {
            if (typeof(QueryStringList[key]) != 'function' && QueryStringList[key] != '') {
                if (queryExtension != '') {
                    queryExtension = queryExtension + '&' + key + "=" + QueryStringList[key];
                } else {
                    queryExtension =  key + "=" + QueryStringList[key];
                }
            }
        }
        if (queryExtension != '') 
        {
            if (path.indexOf('?')>0) {
                path = path + '&' + queryExtension;
            } else {
                path = path + '?' + queryExtension;
            }
        }
    }
    window.location = path;
}

/* Add this function to window.onload Event */
//if (!document.cookie) 
//{
//    OnLoadFunctions(BuildQueryString);
//}



