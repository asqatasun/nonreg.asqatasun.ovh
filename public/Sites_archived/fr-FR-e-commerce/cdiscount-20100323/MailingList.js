function checkitfooter(errMessage) 
{
    var testform = 0;
    var txtEmail=document.getElementById('EMAIL');
    if (!txtEmail || txtEmail.value == '')
    {
        txtEmail=document.getElementById('EMAIL2');
    }
    if (!txtEmail)
    {
        return false;
    }
    
    if (txtEmail)
    {
    
        if (txtEmail.value == '' || txtEmail.value.indexOf('@') == -1 || txtEmail.value.indexOf('.') == -1) 
        {
            txtEmail.focus();
            alert(errMessage);
            testform = 1;
        }
        if (testform == 0)
        {
        //TODO - le path pas en dur
        window.open('http://localhost:1137' + RootPath + '/Customer/NewsLetterForm.aspx?Email=' + txtEmail.value ,'po','scrollbars=yes,resizable=no,toolbar=no,status=no,menubar=no,width=580,height=570');
        //window.open('','po','scrollbars=yes,resizable=no,toolbar=no,status=no,menubar=no,width=580,height=570');
           
            return true;
        }
        else
        { 
            return false;
        }
    }
}

function checkMail()
{
	var x = document.forms[0].email.value;
	var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if (filter.test(x)) 
	    alert('YES! Correct email address');
	else 
	    alert('NO! Incorrect email address');
}


function checkMailAddress(email) {
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return filter.test(email);
}
function DoClickNewsLetter(elemId, pageUrl, pagename, errmsg) {
    var email = document.getElementById(elemId).value;    
    if (checkMailAddress(email)) {
        window.open(pageUrl+'?email='+email, pagename, 'width=790,height=810,scrollbars=1,left=20,top=20,resizable=1');
        return true;       
    }
    else {
        alert(errmsg);
        return false;
       
    }
    return false;
}

function DoClickGoodDeal(e, elemId, pageUrl, pagename, errmsg) {    
    DoClickNewsLetter(elemId, pageUrl, pagename, errmsg);
    if (e && e.preventDefault)
        e.preventDefault(); // DOM style
    e.returnValue = false;
    return e.returnValue; // IE style
}
