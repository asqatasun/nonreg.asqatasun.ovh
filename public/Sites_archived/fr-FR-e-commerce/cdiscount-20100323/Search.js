function BuildSearchLinkForLib(LibrayDepartment, idTxt, errMessage) {
    var navid = LibrayDepartment;
    var querystr = document.getElementById(idTxt).value;

    // liste des caracteres supprim�s : < > ^ | : ? . / * % # ' " \
    querystr = querystr.replace(/[<>^|:\?\.\/\*%#\'\"\\]/g, '');

    querystr = querystr.replace(/[&]/g, 'et');      //transformer & en et
    querystr = querystr.replace(/[\s+]+/g, ' ');    //r�duire espaces multiples et les +
    querystr = querystr.replace(/^[\s+]+/, '');     //espaces d�but
    querystr = querystr.replace(/[\s+]+$/, '');     //espaces fin
    querystr = querystr.replace(/\s+/g, '+');       //transformer espaces

    if (querystr == '') {
        alert(errMessage);
    }
    else {
        if (navid != '') {
            window.location.href = encodeURI(RootPath + '/search/' + querystr + '/s-' + navid + '.html');
        }
        else {
            window.location.href = encodeURI(RootPath + '/search/' + querystr + '/sa-10.html');
        }
    }
}


function BuildSearchLink2(searchPath, idList, idTxt, oldId, errMessage) {
    var navid = document.getElementById(idList).value;
    var querystr = document.getElementById(idTxt).value;

    // liste des caracteres supprim�s : < > ^ | : ? . / * % # ' " \
    querystr = querystr.replace(/[<>^|:\?\.\/\*%#\'\"\\]/g, '');

    querystr = querystr.replace(/[&]/g, 'et');      //transformer & en et
    querystr = querystr.replace(/[\s+]+/g, ' ');    //r�duire espaces multiples et les +
    querystr = querystr.replace(/^[\s+]+/, '');     //espaces d�but
    querystr = querystr.replace(/[\s+]+$/, '');     //espaces fin
    querystr = querystr.replace(/\s+/g, '+');       //transformer espaces

    if (querystr == '') {
        alert(errMessage);
    }
    else {
        if (navid != '') {
            window.location.href = encodeURI(searchPath + '/search/' + querystr + '/s-' + navid + '.html?q=' + querystr);
        }
        else {
            window.location.href = encodeURI(searchPath + '/search/' + querystr + '/sa-10.html?q=' + querystr);
        }
    }
}

function BuildSearchLink(idList, idTxt, oldId, errMessage, DefaultLbl) {
    var navid = document.getElementById(idList).value;
    var querystr = document.getElementById(idTxt).value;

    querystr = querystr.replace(DefaultLbl, '');

    // liste des caracteres supprim�s : < > ^ | : ? . / * % # ' " \
    querystr = querystr.replace(/[<>^|:\?\.\/\*%#\'\"\\]/g, '');

    querystr = querystr.replace(/[&]/g, 'et');      //transformer & en et
    querystr = querystr.replace(/[\s+]+/g, ' ');    //r�duire espaces multiples et les +
    querystr = querystr.replace(/^[\s+]+/, '');     //espaces d�but
    querystr = querystr.replace(/[\s+]+$/, '');     //espaces fin
    querystr = querystr.replace(/\s+/g, '+');       //transformer espaces

    if (querystr == '') {
        alert(errMessage);
    }
    else {
        if (navid != "") {
            window.location.href = encodeURI(RootPath + '/search/' + querystr + '/s-' + navid + '.html');
        }
        else {
            window.location.href = encodeURI(RootPath + '/search/' + querystr + '/sa-10.html');
        }
    }
}

//concatenation avec le fichier Suggestion
function lookup(search_sinequa) {

    if (search_sinequa.length == 0) {
        $('#suggestions').hide();
    }
    else {
        $.post('/Catalog/Suggestions.aspx', { q: "" + search_sinequa + "" }, function(data) {
            //  alert('hello');
            if (data.length > 0) {
                $('#suggestions').show();
                data = data.replace("/", " ");
                data = data.replace(":", " ");

                $('#autoSuggestionsList').html(data);
            }

        });
    }
} // lookup

function fill(thisValue) {
    $('#txtSearch').val(thisValue);
    setTimeout("$('#suggestions').hide();", 200);
}
