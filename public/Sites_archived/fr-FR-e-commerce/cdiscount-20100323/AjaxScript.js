
//PageLoading Event Handler
function PageLoadingHandler(sender, args) {

    //Gets DataItems
   var dataItems = args.get_dataItems();
   //WebAnalytic DataItems
   eval(dataItems['WebAnalytics']);
   eval(dataItems['FloatingBasket']);
}

//Function to run on PageLoad
function WebAnalyticRegisterEvent() 
{
    Sys.WebForms.PageRequestManager.getInstance().add_pageLoading(PageLoadingHandler);
}

//Add function WebAnalyticRegisterEvent to PageLoadEvents
OnLoadFunctions(WebAnalyticRegisterEvent);
