
//ne jamais commenter car utiliser dans les 3 versions
function cacher() {
	layer.style.visibility='hidden';
}

function fermer() {
	document.getElementById('overlayer').style.visibility='hidden';
}

// Fichier JScript

function setLyr(obj,lyr) {
	if (obj != undefined)
	{	
		var newX = findPosX(obj);
		var newY = findPosY(obj);
		var x = new getObj(lyr);
		x.style.top = newY + 'px';
		x.style.left = newX + 'px';
		x.style.visibility = "visible";
	}
}

function findPosX(obj) {
	var curleft = 0;

	if (obj.offsetParent) {
		while (obj.offsetParent) {
			curleft += obj.offsetLeft;
			obj = obj.offsetParent;
		}
	}
	else if (obj.x)
		curleft += obj.x;

	return curleft;
}

function findPosY(obj) {
	var curtop = 0;
	var printstring = '';

	if (obj.offsetParent) {
		while (obj.offsetParent) {
			printstring += ' element ' + obj.tagName + ' has ' + obj.offsetTop;
			curtop += obj.offsetTop;
			obj = obj.offsetParent;
		}
	}
	else if (obj.y)
		curtop += obj.y;

	return curtop;
}

function getObj(name) {
	if (document.getElementById && document.getElementById(name) != null) {
		this.obj = document.getElementById(name);
		this.style = document.getElementById(name).style;
	}
	else if (document.all && document.all[name] != null) {
		this.obj = document.all[name];
		this.style = document.all[name].style;
	}
	else if (document.layers && document.layers[name] != null) {
		if (document.layers[name]) {
			this.obj = document.layers[name];
			this.style = document.layers[name];
		}
	}
}

//function addLoad(func) {
//  if (window.addEventListener)
//   window.addEventListener("load", func, false);
//  else if (document.addEventListener)
//   document.addEventListener("load", func, false);
//  else if (window.attachEvent)
//   window.attachEvent("onload", func);
//  else if (typeof window.onload != "function")
//   window.onload = func;
//  else {
//   var oldonload = window.onload;
//   window.onload = function() {
//	oldonload();
//	func();
//   };
//  }
// }

 function initPub() {
  var apos = OAS_listpos.split(',');
  var olddocwrite = document.write;
  for(var i = 0; i < apos.length; i++) {
   var object_togo = new getObj('PUB'+apos[i]);
   var object_tomove = new getObj('HiddenPub'+apos[i]);
   html = "";
   if (typeof(object_togo.obj) != "undefined" && typeof(object_tomove.obj) != "undefined") {
    object_togo.obj.appendChild(object_tomove.obj);
    object_tomove.style.visibility = "visible";
   }
  }
 }

//function goPub() {
//	var obj;

//	// Placement Top
//	obj = new getObj("PUBTop");
//	setLyr(obj.obj, "PUBTopL");
//	
//	// Placement Left
//	obj = new getObj("PUBLeft");
//	setLyr(obj.obj, "PUBLeftL");
//	
//	// Placement Left1
//	obj = new getObj("PUBLeft1");
//	setLyr(obj.obj, "PUBLeft1L");
//	

//	
//	// Placement Right
//	obj = new getObj("PUBRight");
//	setLyr(obj.obj, "PUBRightL");
//	
//	// Placement Right1
//	obj = new getObj("PUBRight1");
//	setLyr(obj.obj, "PUBRight1L");
//	
//	// Placement Position1
//	obj = new getObj("PUBPosition1");
//	setLyr(obj.obj, "PUBPosition1L");
//	
//	// Placement Middle
//	obj = new getObj("PUBMiddle");
//	setLyr(obj.obj, "PUBMiddleL");
//	
//		// Placement Middle1
//	obj = new getObj("PUBMiddle1");
//	setLyr(obj.obj, "PUBMiddle1L");
//	
//	// Placement Bottom
//	obj = new getObj("PUBBottom");
//	setLyr(obj.obj, "PUBBottomL");
//	
//	// Placement Bottom1
//	obj = new getObj("PUBBottom1");
//	setLyr(obj.obj, "PUBBottom1L");
//	
//	
//	// Placement Middle1
//	obj = new getObj("PUBMiddle1");
//	setLyr(obj.obj, "PUBMiddle1L");
//	
//	// Placement Middle2
//	obj = new getObj("PUBMiddle2");
//	setLyr(obj.obj, "PUBMiddle2L");

//}