function display(id) {
	document.getElementById(id).style.display = document.getElementById(id).style.display == "block" ? "none" : "block";
}

function changeImage(id, path) {
	var sel = document.getElementById(id);
	var value = sel.options[sel.selectedIndex].value;
	document.getElementById('preview_' + id).innerHTML='<img src="' + path + '/' + value + ' " />'
}

function modifierListe(type_element, action, valeur, organisation_id, id) {
  dojo.xhrGet({
    url: 	"/organisation/" + action +
    		"element?nom=" + valeur +
    		"&type_element=" + type_element +
    		"&organisation_id=" + organisation_id +
    		"&id=" + id,
    load: function(response, ioArgs){
      dojo.byId("liste_" + type_element).innerHTML = response;
      return response;
    },
    error: function(response, ioArgs){
      dojo.byId("liste_" + type_element).innerHTML =
        "Une erreur est apparue : " + response;
      return response;
    },
    handleAs: "text"
  });
}

function detectTouche(e) {
	/*
	if(e.keyCode == 9)
	{
		var d = document.getElementById('liens_internes');
		if (d) {d.style.visibility='visible';}
		
		document.getElementById('liens_internes').focus();
	}
	*/
}

window.onload=function(){
	var d = document.getElementById('caracteres');
	if (d) {d.style.visibility='visible';}
	var d = document.getElementById('caracteres_sans_js');
	if (d) {d.style.visibility='hidden';}

	$.datepicker.setDefaults($.extend({showMonthAfterYear: false}, $.datepicker.regional['']));
	$("#date_debut").datepicker($.datepicker.regional['fr']);
	$("#date_fin").datepicker($.datepicker.regional['fr']);
	
    /*
    $(".diaporama a").lightBox();
	settings = {
	      tl: { radius: 20 },
	      tr: { radius: 20 },
	      bl: { radius: 20 },
	      br: { radius: 20 },
	      antiAlias: true,
	      autoPad: true,
	      validTags: ["div"]
	}
	var arrondi_bloc_class = new curvyCorners(settings, "bloc_couleur");
	arrondi_bloc_class.applyCornersToAll();
    */
	/*
	settings_small = {
	      tl: { radius: 8 },
	      tr: { radius: 8 },
	      bl: { radius: 8 },
	      br: { radius: 8 },
	      antiAlias: true,
	      autoPad: true,
	      validTags: ["div"]
	}
	var espace_perso_elmt = document.getElementById("espace_perso");
	var arrondi_espace_perso_elmt = new curvyCorners(settings, espace_perso_elmt);
	arrondi_espace_perso_elmt.applyCornersToAll();

	var video_traduction_lsf_elmt = document.getElementById("video_traduction_lsf");
	if(video_traduction_lsf_elmt != null) {
		var arrondi_video_traduction_lsf_elmt = new curvyCorners(settings, video_traduction_lsf_elmt);
		arrondi_video_traduction_lsf_elmt.applyCornersToAll();
	}


	var arrondi_fond_adresse_postes = new curvyCorners(settings_small, "fond_adresse_postes");
	arrondi_fond_adresse_postes.applyCornersToAll();

	Nifty("a.bouton_violet", "transparent");

	var roundCorner    = RUZEE.ShadedBorder.create({ corner:22 });
	roundCorner.render('menu_1');
	roundCorner.render('menu_2');
	roundCorner.render('menu_3');
	roundCorner.render('menu_4');
	roundCorner.render('menu_5');
	*/
}

