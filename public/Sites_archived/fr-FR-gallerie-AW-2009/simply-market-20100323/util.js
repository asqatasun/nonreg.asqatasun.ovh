/*-- Gestion rollover et map ecole --*/
//Initialisation
function init(){
 if(document.getElementById('charte_etudiant')){
 		init_alternance();
 }
 if(document.getElementById('liste_ecole')){
 		init_liste_ecole();
 } 
}
//Affectation des �venements pour la page etudiants alternatnce
function init_alternance(){
    document.getElementById('charte_etudiant').className='charte_etudiant';
  	for(i=1;i<4;i++){
		  var ref="lnk"+i;
			document.getElementById(ref).onmouseover=setYes;
			document.getElementById(ref).onfocus=setYes;
			document.getElementById(ref).onblur=setNo;						
			document.getElementById(ref).onclick=setYes;
	  	document.getElementById(ref).onmouseout=setNo;			
		}
		//Rustine pour IE6 pour ratrapper le pied de page
		document.getElementById("arbres_buisson").className="IECnul";
		document.getElementById("contenu").className="IECTnul";
}
//affectation des evenements pour la carte ecoles partenaires
function init_liste_ecole(){
  	for(i=1;i<22;i++){
		  var ref="Ecl"+i;
			var refLink="C"+i;
			document.getElementById(ref).className="affN";
			if(document.getElementById(refLink)){
					 document.getElementById(refLink).onmouseover=setYesEcl;
					 document.getElementById(refLink).onfocus=setEtqY;
					 document.getElementById(refLink).onblur=setEtqN;						
					 document.getElementById(refLink).onclick=setYesEclClick;
	  			 //document.getElementById(refLink).onmouseout=setNoEcl;
		 }			
		}
		//Rustine pour IE6 pour ratrapper le pied de page
		document.getElementById("arbres_buisson").className="IECnul";
		document.getElementById("contenu").className="IECTnul";		
}
/*-- Fonction techniques --*/
//Gestion affichage O/N page alternance
function setYes(){
 this.nextSibling.className='affY';
 return false;
}
function setNo(){
 this.nextSibling.className='affN';
 return false;
}
//Gestion affichage O/N carte �coles pour mouseover
function setYesEcl(){
 reset_ecl();
 resetBackP();
 var ref="Ecl"+this.getAttribute('id').substring(1);
 document.getElementById(ref).className='affY';
 return false;
}
//Gestion affichage O/N carte �cole pour clavier (click)
function setYesEclClick(){
 reset_ecl();
 var refLink=this.getAttribute('id');
 var ref="Ecl"+this.getAttribute('id').substring(1);
 setTabnxN(ref,refLink);
 document.getElementById(ref).className='affY';
 document.getElementById(ref).focus();
}
function setNoEcl(){
 var ref="Ecl"+this.getAttribute('id').substring(1);
 document.getElementById(ref).className='affN';
 return false;
}
function reset_ecl(){
		for(i=1;i<22;i++){
		  var ref="Ecl"+i;
			document.getElementById(ref).className="affN";
	 }
}
//Creation de l'�tiquette sur le focus clavier (reprise de title)
function setEtqY(){
 var newelm=document.createElement("span");
 var textLink=document.createTextNode(this.firstChild.getAttribute('title'));
 newelm.appendChild(textLink);
 this.appendChild(newelm);
 //Affectation tabindex -1 sur le dernier plot pour �viter la tabulation dans le vide
 if(document.getElementById('C2'))LastLinkFocus(); 
}
function setEtqN(){
 var cible=this.lastChild;
 if(cible.nodeName=="SPAN")this.removeChild(cible);
}
//Reset des liens "retour � la liste" sur l'evenement mouse over
function resetBackP(){
 for(i=1;i<22;i++){
  var RefNdx="Ecl"+i;
  var Src=document.getElementById(RefNdx).childNodes;
	for(j=0;j<Src.length;j++){
	 if(Src[j].nodeName=="P"){		
			if(Src[j].className=="BackP"){
			document.getElementById(RefNdx).removeChild(Src[j]);
			}
		}
	}
 }
}
/*-- Affectation des tabindex et cr�ation du lien "retour � la liste" sur click keybord
1. lors du click  tous le liens de la liste des datas sont pass� � tabindex -1
2. sur le div "Eclx" qui va etre affich� cr�ation d'un lien de retour au plot activ�
3. sur le div "Eclx" tous les liens sont pass�s � tabindex 0
*/
function setTabnxN(elm,backelm){
  var Src=document.getElementById('liste_ecole2').childNodes;
	//Boucle d'affectation tabindex -1
	for(i=0;i<Src.length;i++){
	 if(Src[i].nodeName=="DIV"){
	  var Src2=Src[i].childNodes;
			for(j=0;j<Src2.length;j++){ 
		 	 if(Src2[j].nodeName=="P"){
			  var cible=Src2[j].firstChild;
			  if(cible.nodeName=="A")cible.setAttribute("tabindex","-1");
			 }
			 if(Src2[j].nodeName=="UL"){
			   var Src3=Src2[j].childNodes;
				  for(k=0;k<Src3.length;k++){
					  if(Src3[k].nodeName=="LI"){
						 var Src4=Src3[k].childNodes;
						 for(l=0;l<Src4.length;l++){
						  if(Src4[l].nodeName=="A"){
							  var cible=Src4[l];
								cible.setAttribute("tabindex","-1");
							}
						 }
			 			}
					}
			 }
		 }
	 }
	}
	//Cr�ation du lien "retour � la liste"
	var Src=document.getElementById(elm).childNodes;
	var BackP=document.createElement('P');
	var BackLink=document.createElement('A');
	var BackLi=document.createElement('LI');
	var BackTxt=document.createTextNode('Retourner � la liste');
	var Href="#"+backelm;
	var BackDel=document.getElementById(elm).lastChild;
		if(BackDel.nodeName=="P"){		
			if(BackDel.className="BackP"){
			document.getElementById(elm).removeChild(BackDel);
		  }
    }
	BackLink.setAttribute("href",Href);
	BackLink.appendChild(BackTxt);
	BackLink.className="Back";
	BackP.appendChild(BackLink);
	BackP.className="BackP";
	document.getElementById(elm).appendChild(BackP);
	//Affectation tabindex 0
	for(i=0;i<Src.length;i++){
		if(Src[i].nodeName=="P"){
		 var cible=Src[i].firstChild;
		 if(cible.nodeName=="A")cible.setAttribute("tabindex","0");
		}
		if(Src[i].nodeName=="UL"){
		 var Src2=Src[i].childNodes;
		 for(j=0;j<Src2.length;j++){
		  if(Src2[j].nodeName=="LI"){
			 var Src3=Src2[j].childNodes;
			  for(k=0;k<Src3.length;k++){
				 if(Src3[k].nodeName=="A"){
				  var cible=Src3[k];
						cible.setAttribute("tabindex","0");
				 }
				}
			}
		 }
		}
	}	
}
function LastLinkFocus(){
  var Src=document.getElementById('liste_ecole2').childNodes;
	//Boucle d'affectation tabindex -1
	for(i=0;i<Src.length;i++){
	 if(Src[i].nodeName=="DIV"){
	  var Src2=Src[i].childNodes;
			for(j=0;j<Src2.length;j++){ 
		 	 if(Src2[j].nodeName=="P"){
			  var cible=Src2[j].firstChild;
			  if(cible.nodeName=="A")cible.setAttribute("tabindex","-1");
			 }
			 if(Src2[j].nodeName=="UL"){
			   var Src3=Src2[j].childNodes;
				  for(k=0;k<Src3.length;k++){
					  if(Src3[k].nodeName=="LI"){
						 var Src4=Src3[k].childNodes;
						 for(l=0;l<Src4.length;l++){
						  if(Src4[l].nodeName=="A"){
							  var cible=Src4[l];
								cible.setAttribute("tabindex","-1");
							}
						 }
			 			}
					}
			 }
		 }
	 }
	}
}
//Fonction popup
function popup(elm){
 Cible=elm.getAttribute('href');
 window.open(Cible,"Moteur de recherche d'offres d'emplois Simply Market - ","menubar=no, status=no, scrollbars=no, width=810, height=600");
}