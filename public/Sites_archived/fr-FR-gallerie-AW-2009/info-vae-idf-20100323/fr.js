var I18N = {

  lang: "fr",

  glp: function(msg){
    return I18N[msg];
  },
	
	'error.unknown': "Une erreur s'est produite, contactez un administrateur.",
	'warn.edit.contentlost'   : "Votre document n'a pas été enregistré, toutes les modifications seront perdues.",
  'warn.popup.blocker':       "La fenêtre a été bloqué par un bloqueur de fenêtre intempestive.\nVeuillez le désactiver et réessayez, ou contactez votre administrateur système.",
  'warn.json.sessiontimeout': "La connection au serveur a échouée ou une erreur s'est produite, votre action n'a pu être éxécutée.\nVotre session à probablement expirée ou le serveur n'est plus joignable.",
  'ui.adm.admin-notes.error': "La connection au serveur a échouée ou une erreur s'est produite lors " 
                              + "de l'enregistrement, votre note n'a pu être sauvegardée.\n" 
                              + "Votre session à probablement expirée ou le serveur n'est plus joignable.",
  'ui.textarea-resizer.title': "Glissez pour agrandir la zone de texte",
  'info.msg.loading':         "Traitement en cours...",
  'msg.confirm.dragdrop': "Veuillez confirmer le déplacement ?"
  
}