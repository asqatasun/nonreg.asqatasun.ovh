var I18N = {

  lang: "fr",

  glp: function(msg){
    return I18N[msg];
  },
	
	'error.unknown': "Une erreur s'est produite, contactez un administrateur.",
	'warn.edit.contentlost'   : "Votre document n'a pas été enregistré, toutes les modifications seront perdues.",
  'warn.popup.blocker':       "La fenêtre a été bloquée par un bloqueur de fenêtres intempestives.\nVeuillez le désactiver et réessayer, ou contactez votre administrateur système.",
  'warn.json.sessiontimeout': "La connexion au serveur a échoué ou une erreur s'est produite, votre action n'a pu être éxécutée.\nVotre session a probablement expiré ou le serveur n'est plus joignable.",
  'ui.adm.admin-notes.error': "La connexion au serveur a échoué ou une erreur s'est produite lors " 
                              + "de l'enregistrement, votre note n'a pu être sauvegardée.\n" 
                              + "Votre session a probablement expiré ou le serveur n'est plus joignable.",
  'info.msg.loading':         "Traitement en cours...",
  'msg.confirm.dragdrop': "Veuillez confirmer le déplacement ?",
  'msg.info.opennewwindow': "Nouvelle fenêtre",
  'datechooser.js.date-format': "dd/MM/yyyy",
  'datechooser.js.date-time-format': "dd/MM/yyyy HH:mm"
}