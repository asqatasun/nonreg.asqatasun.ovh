

// -----------------------------------------------------------------------------
//  AUTOCHOOSER
// -----------------------------------------------------------------------------

'Ajax.Autochooser'.namespace({

  /**
   * Inits language specifics AutoChooser properties
   */
  initLanguage: function(){
    
    if (I18N.lang == 'en'){
      I18N['info.msg.autocomplete']      = 'Enter searched text';
      I18N['info.msg.autocomplete.done'] = 'Category selected !';
    }
    else if (I18N.lang == 'fr'){
      I18N['info.msg.autocomplete']      = 'Entrer le texte à rechercher';
      I18N['info.msg.autocomplete.done'] = 'Catégorie sélectionnée';
    }
  },
  
  /**
   * Inits (lazy) all elements matching DIV.DataChooser INPUT.formChooserLabelfield.
   * Inits all TreeCats.
   * 
   * @see Ajax.Autochooser._initElement
   */
  initChoosers: function(){
    
    JcmsLogger.info("Autochooser","Init Autochooser Manager");
    Ajax.Autochooser.initLanguage(); 
     
    // Create div to populate
    Ajax.Autochooser._initAutoCompleteDIV();
    
    
    $$('DIV.DataChooser INPUT.formChooserLabelfield').each(function(elm,idx){
      Ajax.Autochooser._initElement(elm);
    });
    
    // TreeCat
    $$('UL.TreeCat').each(function(elm,idx){
      Ajax.Autochooser._initTreecat(elm);
    });
  },

  _initAutoCompleteDIV: function(){
    
    if (Ajax.Autochooser.autoCompleteDivId){ return; }
    
    var acDiv = $(document.createElement('DIV'));
    acDiv.addClassName('autocomplete');
    document.body.appendChild(acDiv);
  
    Ajax.Autochooser.autoCompleteDivId = acDiv.identify();
  },

  _initFragment: function(event){
  
    // Case where there is no autocomplete ...
    Ajax.Autochooser.initLanguage(); 
  
    // Create div to populate
    Ajax.Autochooser._initAutoCompleteDIV();
  
    // Init part of the 
    var fragment = $(document.body);
    if (event && event.memo && event.memo.wrapper){
      fragment = $(event.memo.wrapper);
      if (!fragment){ return; }
    }
    
    fragment.select('INPUT.formChooserLabelfield').each(function(elm,idx){
      Ajax.Autochooser._initElement(elm);
    });
    
    fragment.select('UL.TreeCat').each(function(elm,idx){
      Ajax.Autochooser._initTreecat(elm);
    });
  },
  
  _disposeFragment: function(event){
    var fragment = $(document);
    if (event && event.memo && event.memo.wrapper){
      fragment = $(event.memo.wrapper);
    }
    
    /* empty */
  },
  
  // --------------------------------------------------
  //  INIT FUNCTIONS
  // --------------------------------------------------
  
  /**
   * Lazy initialisation of Aucompleter. 
   * Called only on click on the input
   * 
   * @param elm the clicked input
   */
  _initChoosersLazy: function(elm){
    JcmsLogger.info("Autochooser","Init Autochooser Lazy");
    
    // Retrieve the enclosing div type
    var field = elm.up('DIV.DataChooser');
    if (!field){
      return;
    }
    
    // Retrieve Chooser type
    var type = field.className.match(/UI_EDITOR_\S+/);
    if (!type){
      return;
    }
    
    // Init the right autocompleter
    if ('UI_EDITOR_PUBLICATIONCHOOSER' == type){
      Ajax.Autochooser._initPublications(elm, field);
    } else if ('UI_EDITOR_CATEGORYCHOOSER' == type){
      Ajax.Autochooser._initCategories(elm, field);
    } else if ('UI_EDITOR_CATEGORIESCHOOSER' == type){
      Ajax.Autochooser._initCategories(elm, field);
    } else if ('UI_EDITOR_GROUPCHOOSER' == type){
      Ajax.Autochooser._initGroups(elm, field);
    } else if ('UI_EDITOR_MEMBERCHOOSER' == type){
      Ajax.Autochooser._initMembers(elm, field);
    } else if ('UI_EDITOR_WORKSPACECHOOSER' == type){
      Ajax.Autochooser._initWorkspace(elm, field);
    }
  },
  
  /**
   * Init the Chooser InputFormLabel
   * - Set input enabled
   * - Add class name 'autocomplete'
   * - Bind onClick a lazy autocompleter initialisation
   * 
   * @see Ajax.Autochooser._initChoosersLazy
   * @param input The element to initialise
   */
  _initElement: function(input){
    input = $(input);
    
    if (input.hasClassName("disabled")) {
      return;
    }
    
    if (!input.fastUp('DIV','DataChooser',true,5)){
      return;
    }
    
    input.disabled = '';
    input.addClassName('autocomplete');
    
    var observer = function() { 
      Event.stopObserving(input, 'focus'     , observer);
      Ajax.Autochooser._initChoosersLazy(input); 
    };
    Event.observe(input, 'focus' , observer);
  },
  
  /**
   * Init all publication choosers
   */
  _initPublications: function(input, field){
    var type = Ajax.Autochooser._getChooserContentType(field);
    
    // Debug
    if (JcmsLogger.isDebug && JcmsLogger.Autochooser){
      var inputName = input.next('INPUT.formChooserfield').name;
      JcmsLogger.debug("Autochooser",inputName+" Type:",type);
    }
    
    Ajax.Autochooser._newAutocompleter(input,"jcore/autocomplete/acpublication.jsp?type="+type);
  },
  
  /**
   * Init all member choosers
   */
  _initMembers: function(input, field){
	  
    // Compute additional parameters from css options
	var queryParams = '';
    if (field.hasClassName('adminOnly')){
      queryParams += 'admin=true&';
    }
    if (field.hasClassName('workerOnly')){
      queryParams += 'worker=true&';
    }
    if (field.hasClassName('jstoreOnly')){
      queryParams += 'jstore=true&jcmsdb=false&';
    }
    
    // Retrieve the enclosing div groups
    var gids = Ajax.Autochooser._getChooserGroups(field); 
    
    // Debug
    if (JcmsLogger.isDebug && JcmsLogger.Autochooser){
      var inputName = input.next('INPUT.formChooserfield').name;
      JcmsLogger.debug("Autochooser",inputName+" Groups:",gids);
    }
    
    Ajax.Autochooser._newAutocompleter(input,"jcore/autocomplete/acmember.jsp?" + queryParams + gids);
  },
  
  /**
   * Init all groups choosers
   */
  _initGroups: function(input, field){
    // Retrieve the enclosing div groups
    var gids = Ajax.Autochooser._getChooserGroups(field); 
    
    // Debug
    if (JcmsLogger.isDebug && JcmsLogger.Autochooser){
      var inputName = input.next('INPUT.formChooserfield').name;
      JcmsLogger.debug("Autochooser",inputName+" Groups:",gids);
    }
    
    Ajax.Autochooser._newAutocompleter(input,"jcore/autocomplete/acgroup.jsp?"+gids);
  },
  
  /**
   * Init all category choosers
   */
  _initCategories: function(input, field){
    Ajax.Autochooser._newAutocompleter(input,"jcore/autocomplete/accategory.jsp");
  },
  
  /**
   * Init all workspace choosers
   */
  _initWorkspace: function(input, field){
    Ajax.Autochooser._newAutocompleter(input,"jcore/autocomplete/acworkspace.jsp");
  },
  
  /**
   * Instanciate a new Scriptaculous AJAX Autocompleter
   */
  _newAutocompleter: function(input, url){
    // Autocomplete => DOM so DOM must not reference Autocomplete
    new Ajax.Autocompleter(input,Ajax.Autochooser.autoCompleteDivId, url, {paramName: 'autocomplete', minChars: 2, afterUpdateElement: Ajax.Autochooser._populate });
  },
  
  /**
   * Init all treecat
   */
  _initTreecat: function(elm){
    
	  elm = $(elm);
	  
	  // Treecat must declare "autocomplete" class
	  if (!elm.hasClassName('autocomplete')){
	    return;
	  }
	  
	  // Check texfield doesn't already exists
	  var tfId = elm.id+'_autocomplete';
	  var tf = $(tfId);
	  if (tf){
	   tf.value = I18N.glp('info.msg.autocomplete');
	   return;
	  }
	  
	  // Create textfield for autocompleter
	  var textfield      = document.createElement("INPUT");
	  textfield.type      = 'text';
	  textfield.className = 'formTextfield autocomplete treecatcomplete';
	  textfield.treecat   = elm.id;
	  textfield.value     = I18N.glp('info.msg.autocomplete');
	  textfield.id        = tfId;
	  
	  // Append textfield before TreeCat
	  elm.parentNode.insertBefore(textfield, elm);
	  
	  Event.observe(textfield, 'focus', function(){ 
	    textfield.value = ""; 
	  });
	  
	  var uri = 'jcore/autocomplete/accategory.jsp?cids='+elm.getJcmsIds().join('&cids=');
	  
	  // Check filter editmode otherwise it is read mode
	  uri += elm.hasClassName('filter') ? '&filter=true' : '';
	  
	  // Check selectable
	  uri += elm.hasClassName('selectable') ? '&selectable=true' : '';
	  
	  // Check rootDisplay
    uri += elm.hasClassName('rootDisplay') ? '&rootDisplay=true' : '';
	  
	  // Check Removed/Disabled Set
    uri += "&rm="+elm.getJcmsIds('RM_').join('&rm=');
	  
	  // Bind an ajax autocompleter
	  new Ajax.Autocompleter(textfield,Ajax.Autochooser.autoCompleteDivId, uri, {paramName: 'autocomplete', minChars: 2, afterUpdateElement: Ajax.Autochooser._populateTreeCat });
    
  },
  
  // --------------------------------------------------
  //  CLASS PARSING FUNCTIONS
  // --------------------------------------------------
  
  /**
   * Parses classes of the given field to look for
   * the publication chooser type.
   * 
   * The matching classes should be super_PType (ie super_Article).
   * This classe is generated by the widget.
   * 
   * @param field the enclosing DIV
   * @return String the JCMS type of the chooser
   */
  _getChooserContentType: function(field){
    var type  = field.className.match(/super_\S+/);
    if (!type){
      JcmsLogger.warn("Autochooser","Missing Publication chooser type");
      return;
    }
    return type.toString().substr(6);
  },
  
  /**
   * Parses classes of the given field to look for
   * parameter gids (parent groups id).
   * 
   * The matching classes should be gids_GroupId (ie gids_c_1234).
   * This classe is generated by the widget parsing chooserQS.
   * 
   * @param field the enclosing DIV
   * @return String the JCMS parent groups of the chooser
   */
  _getChooserGroups: function(field){
    var gids  = field.className.match(/gids_\S+/g);
    if (!gids){
      return "";
    }
    var gids  = gids.join('&').replace(/gids_/g,'gids=');
    return gids;
  },
  
  // --------------------------------------------------
  //  CALLBACK FUNCTIONS
  // --------------------------------------------------
  
  /**
   * 
   * @param li the item to check
   */
  _checkItem: function(input, li){
    var li = $(li);
    
    if (li.hasClassName('info')){
      input.value = I18N.glp('info.msg.autocomplete');
      return false;
    }
    
    if (li.hasClassName('nomatch')){
      input.value = I18N.glp('info.msg.autocomplete');
      return false;
    }
    return true;
  },
  
  /**
   * Callback used by Chooser Autocompleter. Called when a 
   * selection has been done.
   * 
   * @param input the form chooser label input
   * @param li the selected LI tag
   */
  _populate: function(input,li){
    var input = $(input);

    if (!Ajax.Autochooser._checkItem(input, li))
      return;
    
    var chooser = input.next('INPUT.formChooserfield');
	  chooser.value = li.id.substring(5);
  },
  
  /**
   * Callback used by TreeCat Autocompleter. Called when a 
   * selection has been done.
   * 
   * @param input the form chooser label input
   * @param li the selected LI tag
   */
  _populateTreeCat: function(input,li){
    var input = $(input);
    
    if (!Ajax.Autochooser._checkItem(input, li))
      return;
      
	  input.value = I18N.glp('info.msg.autocomplete.done');
	  input.addClassName('treecatrefresh');
	  input.blur();
	  
	  setTimeout(function(){
      input.value = I18N.glp('info.msg.autocomplete');
      input.removeClassName('treecatrefresh');
    },2000);
	  
	  Ajax.Tree.refresh(input.treecat, li.id.substring(5), true);
  }
});

// -----------------------------------------------------------------------------
//  INITIALISATION
// -----------------------------------------------------------------------------

  Event.observe(window,   'load' ,          Ajax.Autochooser.initChoosers,      false);
  Event.observe(document, 'refresh:after',  Ajax.Autochooser._initFragment,     false);
  Event.observe(document, 'refresh:before', Ajax.Autochooser._disposeFragment , false);

