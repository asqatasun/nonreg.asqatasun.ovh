$(document).ready(function() {
    if (devImageOpened == undefined) {
      var devImageOpened = 'squelettes/images/puce_grise_moins.png';
      var devImageClosed = 'squelettes/images/puce_grise.png';
    }
    $('a.lien_dev').click(function(evt) {
        evt.preventDefault();

        var targetId = '#' + this.id.replace('lien_','com_');
        var imgId = '#' + this.id.replace('lien_', 'fleche_');

        $('div.com_dev').not(targetId).hide();
        $(imgId).attr('src', devImageOpened);
        $(targetId).show();
        $('img.fleche_dev').not(imgId).attr('src',devImageClosed);

        return false;
      });
  });
