(function($) {
    // -------------------------------------------------------------------------    
    // UI helpers
    // -------------------------------------------------------------------------    
    $.getPageSize = function() {
        var de = document.documentElement;
        var w = window.innerWidth || self.innerWidth || (de&&de.clientWidth) || document.body.clientWidth;
        var h = window.innerHeight || self.innerHeight || (de&&de.clientHeight) || document.body.clientHeight;
        return {height:h,width:w};
    };

    // -------------------------------------------------------------------------
    // Popup
    // -------------------------------------------------------------------------    
    // simplification for popup windows
    // usage : $('a.<someclass>).Popup({options})
    // XXX : document options ?
    // -------------------------------------------------------------------------    
    function buildPopupOpts(options) {
        var opts = [];
        var val; 
        for (var name in options) {
            val = options[name];
            if (val !== undefined) {
                opts.push(name + '=' + val);
            }
        }
        return opts.join(',');
    };
        
    $.fn.Popup = function(options) {
        options = $.extend({}, $.fn.Popup.defaults, options);
        options.popupOpts = buildPopupOpts(options);
        var onClick = $.fn.Popup.onClick;
        return this.click(function(evt) {
                return onClick(evt, this, options);
            });
    };
    
    $.fn.Popup.onClick = function(evt, link, options) {
        evt.preventDefault();
        var pagename = options.pagename || link.title || undefined;
        if (pagename !== undefined) {
            pagename = pagename.toLowerCase().replace(/\W/g, '');
        }
        
        //alert('opening ' + link.href + ' with : "' + pagename + '", "' + options.popupOpts + '"');
        window.open(link.href, pagename, options.popupOpts);
        link.blur();
        return false;
    };

    // window.open params 
    $.fn.Popup.defaults = {
      dependent:'yes',
      height:undefined,   
      hotkeys:'yes',
      innerHeight:undefined,  
      innerWidth:undefined,
      left:undefined,
      location:'yes',     
      menubar:'yes',
      resizable:'yes',
      screenX:undefined,
      screenY:undefined,
      scrollbars:'yes',
      status:'yes',       
      toolbar:'yes',
      top:undefined,
      width:undefined
    };

    // -------------------------------------------------------------------------
    // OneOfTogglers - anyone to come with a better name ???
    // -------------------------------------------------------------------------    
    // many items, only one opened at a time
    // options:
    // * buttonclass : the css class for buttons, defaults to 'togbutton'
    // * targetclass : the css class for targets, defaults to 'togtarget'
    // XXX : document other options
    //
    // Each button/target pair must have an id built on (resp) the buttonclass 
    // and targetclass, ie 'togbutton-47' and 'togtarget-47'.
    //
    // This method can be applied to many distinct 'blocks' in one pass - each 
    // domchunk will be handled separately thanks to closures magic.
    // -------------------------------------------------------------------------        
    $.fn.OneOfTogglers = function(options) {
        //console.profile("oneOfTogglers");
        options = $.extend({}, $.fn.OneOfTogglers.defaults, options);
        var containerSelector = options.togglerContainerSelector;
        var buttonClass = options.buttonClass;
        var buttonSelector = options.buttonElt + "." + buttonClass;
        var targetClass = options.targetClass;
        var targetSelector = options.targetElt + "." + targetClass;
        var buttonOpen = options.buttonOpen;
        var buttonClose = options.buttonClose;
        var onOpen = options.onOpen;
        var onClose = options.onClose;
        var autoClose = options.autoClose;

        var setClosedState = function() {
            $("img", this).attr(buttonOpen); 
            onClose(this, options);
        };

        var setup = function() {
                var domchunk = this; 
                var $targets = $(targetSelector, domchunk).hide();
                var $buttons = $(buttonSelector, domchunk); //.filter(filter);
                $buttons
                    //.children('img').attr(buttonOpen).end()
                    //.unbind('click')
                    .click(function(evt) {
                            var targetId = "#" + this.id.replace(buttonClass, targetClass);
                            var $target = $(targetId, domchunk) 
                            var $img = $('img', this);                           
                            var closed = $target.css('display') == 'none';
                            // are we closed yet ?
                            if (closed) {
                                // if so, close the other       
                                if (autoClose) {
                                    $targets.not(targetId).hide();
                                    $buttons.each(setClosedState);
                                }
                                // then open this
                                $img.attr(buttonClose);
                                onOpen(this, options);
                                $target.show();
                            }
                            else {
                                $img.attr(buttonOpen);
                                $target.hide();
                                onClose(this, options);
                            }
                            return false;
                        });                       
        };
        
        $(containerSelector, this).each(setup);

        //console.profileEnd();
        return this;
    };
    
    $.fn.OneOfTogglers.defaults = {
        togglerContainerSelector: "ul.toglist",
        buttonClass: "togbutton",
        buttonElt  : "a",
        targetClass: "togtarget",
        targetElt:   "div",
        buttonOpen: {
          src:'images/plus.gif',
          alt:'+'
        },
        buttonClose: {
          src:'images/minus.gif',
          alt:'-'
        },
        onOpen:function(elt, options) {},
        onClose:function(elt, options) {},
        //filterChunk:function(i) { return true; },
        autoClose: true,
        nada: null
    };
    
    
    // -------------------------------------------------------------------------    
    // tabs
    // -------------------------------------------------------------------------    
    $.fn.Tabbed = function(options) {
        options = $.extend({}, $.fn.Tabbed.defaults, options);

        this.each(function() {
                var chunk = this;
                
                $(options.tabSelector, chunk).bind(options.triggerEvent, function(evt) {
                        var tab = $(this);
                        var tabSiblings = $(options.tabSelector, chunk).not(this);
                        // XXX : simplifier sur le modele de OneOffTogglers 
                        var contentId = '#' + this.id.replace(options.tabIdPrefix, options.tabContentIdPrefix);
                        var content = $(contentId);
                        var contentSiblings = $(options.tabContentSelector, chunk).not(content);
                        tab.addClass(options.activeTabClass);
                        content.show();                        
                        contentSiblings.hide();
                        tabSiblings.removeClass(options.activeTabClass);

                        if ($.isFunction(options.onChange)) {
                            options.onChange(this);
                        }
                        // XXX : pas sûr que... 
                        evt.stopPropagation();
                        evt.preventDefault();
                        return false;
                    });

            });

        return this;
    };


    $.fn.Tabbed.defaults = {
        triggerEvent: 'click',
        tabSelector:'.wsbuiTab',
        tabIdPrefix: 'tab',
        tabContentSelector:'.wsbuiTabContent',
        tabContentIdPrefix: 'tabcontent',
        activeTabClass : 'on',
        onChange:null,
        whatNot: 'yadda'
    };


    // -------------------------------------------------------------------------    
    // filling forms errors
    // -------------------------------------------------------------------------    
    function fillErrors(form, errors) {
        $('i.form-erreur', form).remove();
        if (errors) {
            for (var fname in errors) {
                var f = $('[name=' + fname + ']', form);
                if (f && f.addClass) {
                    f.addClass('error').before("<i class='form-erreur'>[" + errors[fname] + "]</i");
                }
            }
        }
        return $.WsbForms;
    };
    
    function clearErrors(form) {
        $('div.error', form).remove();
        return $.WsbForms;
    };
    
    $.WsbForms = {
      fillErrors: fillErrors,
      clearErrors: clearErrors
    };


    // -------------------------------------------------------------------------    
    // to be continued
    // -------------------------------------------------------------------------    
    

 })(jQuery);   

