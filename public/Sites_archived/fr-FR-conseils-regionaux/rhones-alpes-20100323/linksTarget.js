linksTarget = {
    /**
     *  Sur un click sur le document v�rifie s'il faut ouvrir les liens dans une nouvelle fen�tre ()
     */         
    init: function() {
        var cA = document.getElementsByTagName("a"), oA;
        for (i = 0; oA = cA[i]; i++) {
            if (linksTarget.util.hasClassName(oA,"external")
                || linksTarget.util.hasClassName(oA,"document"))
            {
            	linksTarget.util.addEventLst(oA,"click",linksTarget.clickEvtLst);
            }
        }
    },
    clickEvtLst : function (evt) {
    	if (!evt && window.event) {evt = window.event;}
        if (!evt) {return;}
        window.open(this.href);
        if (evt && evt.preventDefault) {
        	evt.preventDefault();
        } else if (window.event) {
        	evt.returnValue = false;
        }
        return false;
    },
    unload : function () {
        var cA = document.getElementsByTagName("a"), oA;
        for (i = 0; oA = cA[i]; i++) {
            if (linksTarget.util.hasClassName(oA,"external")
                || linksTarget.util.hasClassName(oA,"document"))
            {
            	linksTarget.util.removeEventLst(oA,"click",linksTarget.clickEvtLst);
            }
        }
		linksTarget.util.removeEventLst(window,"load",linksTarget.init);
		linksTarget.util.removeEventLst(window,"unload",linksTarget.unload);
    },
    /*
     * Objet contenant quelques fonctions utilis�es r�guli�rement et d�velopp�es dans un autre cadre
     */
    util : {
        /**
         * Ajout un gestionnaire d'�v�nnement sur un noeud du DOM
         * @param   Node        EventTarget noeud sur lequel enregistrer le gestionnaire
         * @param   string      type        cha�ne corresondant au type d'�v�nement � g�rer (load, submit, click, etc...)
         * @param   function    listener    Objet fonction correspondant au gestionnaire � d�clencher
         * @param   bool        useCapture  Booleen pr�sisant - pour le mobel standard - si le gestionnaire est capturant ou pas
         */
        addEventLst: function(EventTarget,type,listener,useCapture) {
        	useCapture = typeof(useCapture)=="boolean"?useCapture:false;
        	if (EventTarget.addEventListener) {
        		EventTarget.addEventListener(type, listener, useCapture);
        	} else if ((EventTarget==window) && document.addEventListener) {
        		document.addEventListener(type, listener, useCapture);
        	} else if (EventTarget.attachEvent) {
        		EventTarget["e"+type+listener] = listener;
        		EventTarget[type+listener] = function() {EventTarget["e"+type+listener]( window.event );}
        		EventTarget.attachEvent("on"+type, EventTarget[type+listener]);
        	}
        },
        /**
         * Enl�ve un gestionnaire d'�v�nnement sur un noeud du DOM
         * @param   Node        EventTarget noeud sur lequel enlever le gestionnaire
         * @param   string      type        cha�ne corresondant au type d'�v�nement � g�rer (load, submit, click, etc...)
         * @param   function    listener    Objet fonction correspondant au gestionnaire � enlever
         * @param   bool        useCapture  Booleen pr�sisant - pour le mobel standard - si le gestionnaire a enlever est capturant ou pas
         */
		removeEventLst: function(EventTarget,type,listener,useCapture) {
			useCapture = typeof(useCapture)=="boolean"?useCapture:false;
			if (EventTarget.removeEventListener) {
				EventTarget.removeEventListener(type,listener, useCapture);
			} else if ((EventTarget==window) && document.removeEventListener) {
				document.removeEventListener(type,listener, useCapture);
			} else if (EventTarget.detachEvent) {
				EventTarget.detachEvent("on"+type, EventTarget[type+listener]);
				EventTarget[type+listener]=null;
				EventTarget["e"+type+listener]=null;
			}
		},
    	/**
    	 * D�termine si une classe HTML est sp�cifi�e sur un noeud de type �l�ment 
    	 * @param  Node    oNode        Noeud � interroger
    	 * @param  string  className    Cha�ne repr�sentant la classe recherch�e
    	 * @return bool    true si le noeud poss�de la classe, non dans les autres cas	 
    	 */
        hasClassName: function(oNode,className) {
        	return (oNode.nodeType==1)?
        		((" "+oNode.className+" ").indexOf(" "+className+" ")!=-1):false;
        }
    }
}
linksTarget.util.addEventLst(window,"load",linksTarget.init);
linksTarget.util.addEventLst(window,"unload",linksTarget.unload);
