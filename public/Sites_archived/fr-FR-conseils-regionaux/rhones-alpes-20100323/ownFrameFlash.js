/**
 * @author olivier
 */

YAHOO.namespace("YAHOO.ownFrameFlash");
YAHOO.ownFrameFlash.divId = 'ownFrame';

YAHOO.util.Event.addListener(window, 'load', function() {
	oDoc = document.getElementById('document');
	YAHOO.ownFrameFlash.initialisation();
});

YAHOO.ownFrameFlash.initialisation = function() {

	YAHOO.util.Dom.getElementsByClassName('popupFlash','a','document', function() {

		this.href = 'javascript:void(0)';

		var idtf = this.getAttribute('rel');
		var aDim = this.getAttribute('rev').split("@");
		var hauteur = aDim[0]
		var largeur = aDim[1]
		
		YAHOO.util.Event.addListener(this, 'click', function() {
			YAHOO.ownFrameFlash.show(idtf,hauteur,largeur);
			return false;
		});	

	});
}

/*YAHOO.ownFrameFlash.coverFocus = function(evt) {
    YAHOO.util.Event.stopEvent(evt);
}*/

YAHOO.ownFrameFlash.show = function(idtf,hauteur,largeur) {

	YAHOO.ownFrameFlash.showCover(hauteur,largeur);
	document.getElementById('coverContenu').innerHTML = '<div id="coverFlash">Chargement en cours...</div>';
	/* Appel AJAX pour l'affichage de l'élément flash */
	
	var callback = {
		success: function(o) {
			document.getElementById('coverContenu').innerHTML = '<div id="coverFlash">'+ o.responseText+ '</div>';
		}
	}
	var urlAction = SERVER_ROOT + "include/tpl/tpl_getFlashAsync.php";
	YAHOO.util.Connect.asyncRequest("post",urlAction,callback,"idtf="+idtf);

}

YAHOO.ownFrameFlash.close = function() {
	oBloc = document.getElementById('coverContenu');
	if (oBloc) YAHOO.util.Dom.setStyle(oBloc, 'display', 'none');
	
	oBloc = document.getElementById('coverClose');
	if (oBloc) YAHOO.util.Dom.setStyle(oBloc, 'display', 'none');
	
	/*oBloc.innerHTML = '<div id="popupAffichageVideo">&nbsp;</div>';*/
	oBloc.innerHTML = '';
	YAHOO.ownFrameFlash.hideCover();
	
	for(i=0;i<document.getElementsByTagName('select').length;i++){
		document.getElementsByTagName('select').item(i).style.visibility='visible';
	}
	for(i=0;i<document.getElementsByTagName('object').length;i++){
		document.getElementsByTagName('object').item(i).style.visibility='visible';
	}
}

YAHOO.ownFrameFlash.showCover = function(hauteur,largeur) {
	
	winH = YAHOO.util.Dom.getViewportHeight();
	winW = YAHOO.util.Dom.getViewportWidth();
	
    var oTemplatecover = document.getElementById('cover');
	if (!oTemplatecover) {
		var oTemplatecover = document.createElement('div');
		oTemplatecover.id = 'cover';
		document.getElementsByTagName('body')[0].appendChild(oTemplatecover);
	}
	
	oTemplatecover.style.opacity = "0";
	oTemplatecover.style.display = "block";
	oTemplatecover.style.height = YAHOO.util.Dom.getDocumentHeight() + 'px';
	oTemplatecover.style.width = YAHOO.util.Dom.getDocumentWidth() + 'px';

	var bloc_apparition = {opacity:{from:0,to:0.8}};
    var animApparition = new YAHOO.util.Anim('cover', bloc_apparition,.5);
    animApparition.animate();

	var oTemplatecover = document.getElementById('coverContenu');
	if (!oTemplatecover) {
		var oTemplatecover = document.createElement('div');
		oTemplatecover.id = 'coverContenu';
		document.getElementsByTagName('body')[0].appendChild(oTemplatecover);
	}
	oTemplatecover.style.display = "block";
	YAHOO.util.Dom.setStyle(oTemplatecover, "position", "absolute");
	YAHOO.util.Dom.setStyle(oTemplatecover, "z-index", 200);
	YAHOO.util.Dom.setStyle(oTemplatecover, "padding-top", 0);

	YAHOO.util.Dom.setY(oTemplatecover, YAHOO.util.Dom.getDocumentScrollTop() + (Math.ceil((winH-hauteur)/2)) );
	YAHOO.util.Dom.setX(oTemplatecover, (YAHOO.util.Dom.getDocumentScrollLeft() + (winW - largeur) / 2));

	var oTemplateClose = document.getElementById('coverClose');
	if (!oTemplateClose) {
		var oTemplateClose = document.createElement('div');
		oTemplateClose.id = 'coverClose';
		document.getElementsByTagName('body')[0].appendChild(oTemplateClose);
	}
	oTemplateClose.style.display = "block";
	YAHOO.util.Dom.setStyle(oTemplateClose, "position", "absolute");
	YAHOO.util.Dom.setStyle(oTemplateClose, "z-index", 210);
	YAHOO.util.Dom.setStyle(oTemplateClose, "padding-top", 0);


	YAHOO.util.Dom.setY(oTemplateClose, YAHOO.util.Dom.getDocumentScrollTop() + 10 );
	YAHOO.util.Dom.setX(oTemplateClose, YAHOO.util.Dom.getDocumentScrollLeft() + winW - 100 );
	
	oTemplateClose.innerHTML = '<div id=\"popupFlashClose\"><a href="#" class="closeCoin" onclick="YAHOO.ownFrameFlash.close();return false;"><span>Fermer</span></a></div>';
	
/*	YAHOO.util.Dom.setY(oTemplateClose,  );
	YAHOO.util.Dom.setX(oTemplateClose, (YAHOO.util.Dom.getDocumentScrollLeft() + (winW - largeur) / 2));*/

	for(i=0;i<document.getElementsByTagName('select').length;i++){
		document.getElementsByTagName('select').item(i).style.visibility='hidden';
	}
	for(i=0;i<document.getElementsByTagName('object').length;i++){
		document.getElementsByTagName('object').item(i).style.visibility='hidden';
	}
}
YAHOO.ownFrameFlash.hideCover = function() {

	var oTemplatecover = document.getElementById('cover');

	/*var bloc_disparition = {opacity:{from:1,to:0}};
    var animDisparition = new YAHOO.util.Anim(oTemplatecover, bloc_disparition,.5);
    animDisparition.animate();*/

	if (oTemplatecover) oTemplatecover.style.display = "none"; 
}


YAHOO.util.Event.addListener(window, 'load', function() {
	
	YAHOO.util.Dom.getElementsByClassName('boiteOutils','div','document', function() {

		YAHOO.util.Dom.getElementsByClassName('external','a',this, function() {
			regexp = "calameo.com";
			if(this.href.match(regexp)){
				YAHOO.util.Dom.addClass(this, 'calameoIcon'); 
			}
		});	

	});
});