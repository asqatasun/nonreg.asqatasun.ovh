YAHOO.namespace("CMS");

YAHOO.CMS.DDApp = function() {
	var Dom = YAHOO.util.Dom;
	var DDM = YAHOO.util.DragDropMgr;
	return {
		init: function () {
			try {
				var cModules = [];
				if (document.getElementById('colonneGauche')) {
					cBoutons = Dom.getElementsByClassName("pseudo_boutons_insertion", "div", document.getElementById('colonneGauche'));
					if (cBoutons[0]) {
						YAHOO.CMS.DDApp.PAR_LEFT = cBoutons[0].parentNode;
						new YAHOO.util.DDTarget(YAHOO.CMS.DDApp.PAR_LEFT, "modules");
						cModules = Dom.getElementsByClassName("edition", "div", document.getElementById('colonneGauche'));
					}
				}
				if (document.getElementById('colonneDroite')) {
					cBoutons = Dom.getElementsByClassName("pseudo_boutons_insertion", "div", document.getElementById('colonneDroite'));
					if (cBoutons[0]) {
						YAHOO.CMS.DDApp.PAR_RIGHT = cBoutons[0].parentNode;
						new YAHOO.util.DDTarget(YAHOO.CMS.DDApp.PAR_RIGHT, "modules");
						cModules = cModules.concat(Dom.getElementsByClassName("edition", "div", document.getElementById('colonneDroite')));
					}
				}
				for (var i=0; cModules[i]; i++) {
					new YAHOO.CMS.DDPara(cModules[i].id, "modules");
				}
				
				var cParagraphes = Dom.getElementsByClassName("edition", "div", document.getElementById('contenu'));
				for (var j=0; cParagraphes[j]; j++) {
					new YAHOO.CMS.DDPara(cParagraphes[j].id, "paragraphes");
				}
				DDM.mode = DDM.POINT;
			}
			catch (e) {
				/*alert(e);*/
				/* Voir PB retour makeRequest sous IE */
			}
		},
		
		makeRequest: function(sUrl) {
			var callback = {
				success: function(o) {
					if (o.responseText!="OK") {callback.failure(o)};
				},
				failure: function(o) {
					alert("Erreur DragNDrop :\n"+o.responseText);
				},
				argument: null
			};
			YAHOO.util.Connect.asyncRequest('GET', sUrl, callback);
		}	 
	}
} ();

YAHOO.util.Event.addListener(window, "load", YAHOO.CMS.DDApp.init);

(function() {

var Dom = YAHOO.util.Dom;
var Event = YAHOO.util.Event;
var DDM = YAHOO.util.DragDropMgr;
var Region = YAHOO.util.Region;

YAHOO.CMS.DDPara = function(id, sGroup, config) {
   if (id) {
   	this.init(id, sGroup, config);
		this.initFrame();
		this.setHandleElId('dragBar_' + id.replace('par', ''));
	}

	this.setXConstraint(Dom.getX(id) - Dom.getX(Dom.get('corps')), Region.getRegion(Dom.get('corps')).right - Region.getRegion(Dom.get(id)).right);
	this.setYConstraint(Dom.getY(id) - Dom.getY(Dom.get('corps')), Region.getRegion(Dom.get('corps')).bottom - Dom.getY(id));	
	var dragEl = this.getDragEl();
	Dom.setStyle(dragEl, "opacity", 0.6);
	this.setPadding(-4);
	this.goingUp = false;
	this.lastY = 0;
};

YAHOO.extend(YAHOO.CMS.DDPara, YAHOO.util.DDProxy, {

	startDrag: function(x, y) {
		var proxy = this.getDragEl();
		var srcEl = this.getEl();
		proxy.innerHTML = srcEl.innerHTML;
		Dom.setStyle(proxy, "backgroundColor", Dom.getStyle(srcEl, "backgroundColor"));
		Dom.setStyle(proxy, "border", "1px dashed red");		
 		Dom.addClass(srcEl, 'move');
    },

	endDrag: function(e) {
		var srcEl = this.getEl();
		var proxy = this.getDragEl();
		Dom.setStyle(proxy, "visibility", "visible");

		var a = new YAHOO.util.Motion(proxy, {points: {to: Dom.getXY(srcEl)}}, 0.5, YAHOO.util.Easing.easeOut);
		var proxyid = proxy.id;
		var id = this.id;
		a.onComplete.subscribe(function() {
		       Dom.setStyle(proxyid, "visibility", "hidden");
		       Dom.removeClass(id, 'move')
		   });
		a.animate();
		query = this.updateMoveBars();
		YAHOO.CMS.DDApp.makeRequest("PRT/PRT_Submit.php?DragDrop=1" + query);
	},

	onDragDrop: function(e, id) {},

	onDrag: function(e, id) {
		var y = Event.getPageY(e);
		
		if (y < this.lastY) {
		   this.goingUp = true;
		} else if (y > this.lastY) {
		   this.goingUp = false;
		}
		
		this.lastY = y;
	},

	onDragOver: function(e, id) {
		var srcEl = this.getEl();
		var destEl = Dom.get(id);

		switch (destEl) {
			case YAHOO.CMS.DDApp.PAR_LEFT:
			case YAHOO.CMS.DDApp.PAR_RIGHT:
				if (this.goingUp) {
					cBoutons = Dom.getElementsByClassName("pseudo_boutons_insertion", "div", destEl);
					destEl.insertBefore(srcEl, cBoutons[0].nextSibling);
				}
				else {
					destEl.appendChild(srcEl);
				}
				break;
			default:
				if (this.goingUp) {
				   destEl.parentNode.insertBefore(srcEl, destEl);
				}
				else {
		   		destEl.parentNode.insertBefore(srcEl, destEl.nextSibling);
				}
				break;
		}
		DDM.refreshCache();
    },

	onDragEnter: function(e, id) {},
	
	onDragOut: function(e, id) {},
	
	toString: function() {
	  return "DDPara " + this.id;
	},

	updateMoveBars: function() {
		var moveBarSrc = {
			up: "../images/pseudo_upParagraphe.gif",
			down: "../images/pseudo_downParagraphe.gif",
			left: "../images/pseudo_leftParagraphe.gif",
			right: "../images/pseudo_rightParagraphe.gif"
		};

		var moveBarLibelle ;
		if (cms_lang == 'en') {
			moveBarLibelle = {
				up: "Move up",
				down: "Move down",
				left: "Move left",
				right: "Move right"
			};
		}
		else {
			moveBarLibelle = {
				up: "Monter",
				down: "Descendre",
				left: "Déplacer à gauche",
				right: "Déplacer à droite"
			};
		}
		
		var query = "";
		var oGroupsMoveBars = {};
		for (group in this.groups) {
			switch(group) {
		  		case "modules":
					if (YAHOO.CMS.DDApp.PAR_LEFT) {
						oGroupsMoveBars['PAR_LEFT'] = Dom.getElementsByClassName("moveBar", "span", YAHOO.CMS.DDApp.PAR_LEFT);
					}
					if (YAHOO.CMS.DDApp.PAR_RIGHT) {
						oGroupsMoveBars['PAR_RIGHT'] = Dom.getElementsByClassName("moveBar", "span", YAHOO.CMS.DDApp.PAR_RIGHT);
					}
		  			break;
		  		case "paragraphes":
		  			oGroupsMoveBars['PAR_CENTRAL'] = YAHOO.util.Dom.getElementsByClassName("moveBar", "span", document.getElementById('contenu'));
		  			break;
			}
			break;
		}
		
		var i, cMoveBars, nEdition, parId, nNewMoveBar, nAMove, nImgMove;
		// Pour chacun des groupes (sColonne=> PAR_LEFT, PAR_CENTRAL, PAR_RIGHT)
		for (sColonne in oGroupsMoveBars) {
			//* Pour chacune des barres de déplacement du groupe
			cMoveBars = oGroupsMoveBars[sColonne];
			for (i=0; cMoveBars[i]; i++) {
				// On récupère le paragraphe en cours d'édition pour pouvoir recontruire les liens de déplacement avec le bon id
				nEdition = cMoveBars[i];
				while (nEdition && !YAHOO.util.Dom.hasClass(nEdition, "edition")) {nEdition = nEdition.parentNode;}
				par_id = nEdition.id.replace("par", "");
				query += "&order["+sColonne+"][]=" + par_id;
				
				nNewMoveBar = document.createElement("span");
				nNewMoveBar.className=cMoveBars[i].className;
				if (i != 0) {
					nAMove = document.createElement("a");
					nAMove.href = "PRT/PRT_Submit.php?Up="+par_id;
					nAMove.title = moveBarLibelle["up"];
					nNewMoveBar.appendChild(nAMove);
					nImgMove = document.createElement("img");
					nImgMove.src = moveBarSrc["up"];
					nImgMove.alt = moveBarLibelle["up"];
					nAMove.appendChild(nImgMove);
				}
				// La dernière ne doit pas avoir de bouton descendre
				if (i != (cMoveBars.length-1)) {
					nAMove = document.createElement("a");
					nAMove.href = "PRT/PRT_Submit.php?Down="+par_id;
					nAMove.title = moveBarLibelle["down"];
					nNewMoveBar.appendChild(nAMove);
					nImgMove = document.createElement("img");
					nImgMove.src = moveBarSrc["down"];
					nImgMove.alt = moveBarLibelle["down"];
					nAMove.appendChild(nImgMove);
				}
				// déplacement à gauche ?
				if (sColonne == 'PAR_RIGHT' && YAHOO.CMS.DDApp.PAR_LEFT) {
					nAMove = document.createElement("a");
					nAMove.href = "PRT/PRT_Submit.php?Left="+par_id;
					nAMove.title = moveBarLibelle["left"];
					nNewMoveBar.appendChild(nAMove);
					nImgMove = document.createElement("img");
					nImgMove.src = moveBarSrc["left"];
					nImgMove.alt = moveBarLibelle["left"];
					nAMove.appendChild(nImgMove);
				}
				// déplacement à droite ?
				if (sColonne == 'PAR_LEFT' && YAHOO.CMS.DDApp.PAR_RIGHT) {
					nAMove = document.createElement("a");
					nAMove.href = "PRT/PRT_Submit.php?Right="+par_id;
					nAMove.title = moveBarLibelle["right"];
					nNewMoveBar.appendChild(nAMove);
					nImgMove = document.createElement("img");
					nImgMove.src = moveBarSrc["right"];
					nImgMove.alt = moveBarLibelle["right"];
					nAMove.appendChild(nImgMove);
				}
				cMoveBars[i].parentNode.replaceChild(nNewMoveBar,cMoveBars[i]);
			}
		} 
		return query;
	}
});
})(); 

