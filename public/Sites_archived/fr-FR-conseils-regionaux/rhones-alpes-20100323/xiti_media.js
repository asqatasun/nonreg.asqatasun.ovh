var currentPosition		= new Array();
var currentRemaining	= new Array();
var currentTime			= new Array();
var currentState		= new Array();
var currentLoad			= new Array();
var aTitle				= new Array();
var aMedia				= new Array();

YAHOO.util.Event.addListener(window, 'load', function(){
	xt_mediaInit();
});
function xt_mediaInit (){
    //*
	var aEmbed	= document.getElementsByTagName('embed');
	for(var i = 0; i<aEmbed.length; i++){
		if (aEmbed[i].src.lastIndexOf("mediaplayer.swf") != -1){
			xt_mediaAdd(aEmbed[i].id, aEmbed[i].title)
		}
	}
	//*/
}
function xt_mediaAdd(id, title){
	if(!xt_mediaExists(id)){
		aMedia[id] = {id:id, title:title };
		xt_mediaAddCloseListener(id);
	}		
}

function xt_mediaExists(id){
	return (aMedia[id] != undefined);
}

function xt_mediaAddCloseListener(id){
	YAHOO.util.Event.addListener(document.getElementsByTagName('a'), 'click', function(){
		xt_med("M", '1', aMedia[id].title, 'stop', '');
	});
	YAHOO.util.Event.addListener(document, 'unload', function(){
		xt_med("M", '1', aMedia[id].title, 'stop', '');
	});
}


// these functions is called by the JavascriptView object of the player.
function getUpdate(typ,pr1,pr2, swf) { 

	var state = null;
	
	if		(typ == "time"	) { currentPosition[swf]= pr1; 
								pr2 == undefined ? null : currentRemaining[swf] = Math.round(pr2); 
								if(currentTime[swf] == undefined && currentLoad[swf]){
									currentTime[swf] = Math.round(currentRemaining[swf]);
								}
							}
	else if	(typ == "state"	) { currentState[swf]	= pr1;	}
	else if	(typ == "load"	) { currentLoad[swf]	= pr1;	}
	
	
	if(pr1 != 1 && currentLoad[swf]){

		if(typ == 'state'){ 
		
			switch(pr1){
				case 0: // Pause
					if(currentPosition[swf] > 0){
						state = 'stop';
					}
				break;
	
				case 1:
					// loading
				break;
				
				case 2: // Play
					state = 'play';
				break;
				default: break;
			}
		}else{

			if(typ == 'volume'){
				state = 'volume';
			}
		}
		
		if(state != null && currentLoad[swf] > 0){
			currentTime[swf] = '';
			xt_med("M", '1', aMedia[swf].title, state, currentTime[swf]);
		}
	}

};
