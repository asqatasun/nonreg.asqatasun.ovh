/****** BEGIN LICENSE BLOCK *****
 * Copyright (c) 2005-2006 Harmen Christophe and contributors. All rights reserved.
 * 
 * This script is free software; you can redistribute it and/or
 *   modify under the terms of the Creative Commons - Attribution-ShareAlike 2.0
 * <http://creativecommons.org/licenses/by-sa/2.0/>
 * You are free:
 *     * to copy, distribute, display, and perform the work
 *     * to make derivative works
 *     * to make commercial use of the work
 * 
 * Under the following conditions:
 * _Attribution_. You must attribute the work in the manner specified by the
 *   author or licensor.
 * _Share Alike_. If you alter, transform, or build upon this work, you may
 *   distribute the resulting work only under a license identical to this one.
 *     * For any reuse or distribution, you must make clear to others 
 *      the license terms of this work.
 *     * Any of these conditions can be waived if you get permission from 
 *      the copyright holder.
 * 
 * Your fair use and other rights are in no way affected by the above.
 * 
 * This is a human-readable summary of the Legal Code (the full license). 
 * <http://creativecommons.org/licenses/by-sa/2.0/legalcode>
 ***** END LICENSE BLOCK ******/
function trim(s) {return s.replace(/^\s+|\s+$/g,"");}
function hasClassName(oNode,className) {
	return (oNode.nodeType==1)?
		((" "+oNode.className+" ").indexOf(" "+className+" ")!=-1):false;
}
function addClassName(oNode,className) {
	if ((oNode.nodeType==1) && !hasClassName(oNode,className))
		oNode.className = trim(oNode.className+" "+className);
}
function deleteClassName(oNode,className) {
	if (oNode.nodeType==1)
    oNode.className = trim((" "+oNode.className+" ").replace(" "+className+" "," "));
}
function isChildNodeOf(oNode,other) {
	if (oNode.compareDocumentPosition) {
		return (oNode.compareDocumentPosition(other)==10);
	} else if (other.contains) {
		return other.contains(oNode);
	}
	var bIsChildNodeOf = false;
	function _isChildNodeOf(oNode,other) {
		while (other) {
			if (other==oNode) {
				bIsChildNodeOf = true;
				return;
			} else _isChildNodeOf(oNode,other.firstChild);
			other = other.nextSibling;
		}
	}
	_isChildNodeOf(oNode,other.firstChild);
	return bIsChildNodeOf;
}
function addEventLst(EventTarget,type,listener,useCapture) {
	useCapture = typeof(useCapture)=="boolean"?useCapture:false;
	if (EventTarget.addEventListener) {
		EventTarget.addEventListener(type, listener, useCapture);
	} else if ((EventTarget==window) && document.addEventListener) {
		document.addEventListener(type, listener, useCapture);
	} else if (EventTarget.attachEvent) {
		EventTarget["e"+type+listener] = listener;
		EventTarget[type+listener] = function() {EventTarget["e"+type+listener](window.event);}
		EventTarget.attachEvent("on"+type, EventTarget[type+listener]);
	} else {
		EventTarget["on"+type] = listener;
	}
}
function removeEventLst(EventTarget,type,listener,useCapture) {
	useCapture = typeof(useCapture)=="boolean"?useCapture:false;
	if (EventTarget.removeEventListener) {
		EventTarget.removeEventListener(type,listener, useCapture);
	} else if ((EventTarget==window) && document.removeEventListener) {
		document.removeEventListener(type,listener, useCapture);
	} else if (EventTarget.detachEvent) {
		EventTarget.detachEvent("on"+type, EventTarget[type+listener]);
		EventTarget[type+listener]=null;
		EventTarget["e"+type+listener]=null;
	} else {
		EventTarget["on"+type]=null;
	}
}
function loaded() {
	if (!document.createElement && !document.createTextNode 
			&& !document.getElementById && !document.getElementsByTagName
			&& !document.insertBefore)
	{
		if (hasClassName(document.body,"dynamik"))
			deleteClassName(document.body,"dynamik");
		return;
	}
	/* Déplacé dans initMenus()
	if (document.getElementById("menuControle")) {
//log("\n\n* loaded() déjà exécutée (#menuControle existant)");
		return;
	}
	//*/
	/* Non nécessaire dans le cas présent
	var imgLoaded = new Image();
	var tmp = new Date();
	var suffix = tmp.getTime();
	imgLoaded.src = SERVER_ROOT + "images/SIT_RHONEALPES/menu_testImg.gif?"+suffix;
	imgLoaded.onload = initMenus;
	//*/
	initMenus();
}
/*
addEventLst(window,"load",function() {
//log("\n\n* window is loaded");
    loaded();
});
//*/
//YAHOO.util.Event.onDOMReady(loaded);
//*
YAHOO.util.Event.onDOMReady(function() {
//log("\n\n* window is onDOMReady");
    loaded();
});
//*/

var menu_timerID = null;
function initMenus() {
	if (document.getElementById("menuControle")) {
//log("\n\n* initMenus() déjà exécutée (#menuControle existant)");
		return;
	}
//log("\n\n* initMenus()");
	var nPCtrl, nACtrl, nTxt;
	nPCtrl = document.createElement("p");
	nPCtrl.id= "menuControle";
	nACtrl = document.createElement("a");
	nACtrl.href="javascript:menuController();";
	nPCtrl.appendChild(nACtrl);
	nTxt = document.createTextNode("Désactiver le menu");
	nACtrl.appendChild(nTxt);
	var nMenu = document.getElementById("menu");
    nMenu.parentNode.insertBefore(nPCtrl,nMenu);
    
    /*
	var cSelected = YAHOO.util.Dom.getElementsByClassName("selected",
        "li",
        document.getElementById("menu"));
    if (cSelected.length > 0) {
        addClassName(cSelected[0],"currentTheme");
    }
    //*/
    //* On affiche le theme selectionné
    var nChild = nMenu.firstChild;
	while (nChild) {
		if (hasClassName(nChild,"theme") && hasClassName(nChild,"selected")) {
            addClassName(nChild,"currentTheme");
            break;
        }
		nChild=nChild.nextSibling;
	}
	//*/

    loadMenus();

}
function loadMenus() {
    // S'il y a déjà la classe dynamik et le #menuControl
	if (hasClassName(document.body,"dynamik") && (!document.getElementById("menuControle"))) {
//log("\n\n* loadMenus() : sortie prématurée (?¿)");
	   return;
	}
//log("\n\n* loadMenus()");
	addClassName(document.body,"dynamik");
	var nMenu = document.getElementById("menu");
	if (nMenu.addEventListener) {
		nMenu.addEventListener("mouseover",eventLstMontrerMenu,true);
		nMenu.addEventListener("focus",eventLstMontrerMenu,true);
		nMenu.addEventListener("DOMFocusIn",eventLstMontrerMenu,true);
		nMenu.addEventListener("mouseout",eventLstCacherMenus,true);
		nMenu.addEventListener("blur",eventLstCacherMenus,true);
		nMenu.addEventListener("DOMFocusOut",eventLstCacherMenus,true);
	} else {
		var nA;
		var nChild=nMenu.firstChild;
		while (nChild) {
			if (hasClassName(nChild,"theme")) {
				addEventLst(nChild,"mouseover",eventLstMontrerMenu);
				addEventLst(nChild,"mouseout",eventLstCacherMenus);
				for (var j=0; nA = nChild.getElementsByTagName("a")[j]; j++) {
					addEventLst(nA,"focus",eventLstMontrerMenu);
					addEventLst(nA,"blur",eventLstCacherMenus);
				}
			}
			nChild=nChild.nextSibling;
		}
	}
	var mCtrl = document.getElementById("menuControle");
	mCtrl.firstChild.firstChild.nodeValue="Désactiver le menu";
}
function unloadMenus() {
    // S'il y a déjà la classe dynamik et le #menuControl
	if (!hasClassName(document.body,"dynamik") && (!document.getElementById("menuControle"))) {
//log("\n\n* unloadMenus() : sortie prématurée (?¿)");
	   return;
	}
//log("\n\n* unloadMenus()");
	deleteClassName(document.body,"dynamik");
	var nMenu = document.getElementById("menu");
	if (nMenu.removeEventListener) {
		nMenu.removeEventListener("mouseover",eventLstMontrerMenu,true);
		nMenu.removeEventListener("focus",eventLstMontrerMenu,true);
		nMenu.removeEventListener("DOMFocusIn",eventLstMontrerMenu,true);
		nMenu.removeEventListener("mouseout",eventLstCacherMenus,true);
		nMenu.removeEventListener("blur",eventLstCacherMenus,true);
		nMenu.removeEventListener("DOMFocusOut",eventLstCacherMenus,true);
	} else {
		var nA;
		var nChild=nMenu.firstChild;
		while (nChild) {
			if (hasClassName(nChild,"theme")) {
				removeEventLst(nChild,"mouseover",eventLstMontrerMenu);
				removeEventLst(nChild,"mouseout",eventLstCacherMenus);
				for (var j=0; nA = nChild.getElementsByTagName("a")[j]; j++) {
					removeEventLst(nA,"focus",eventLstMontrerMenu);
					removeEventLst(nA,"blur",eventLstCacherMenus);
				}
			}
			nChild=nChild.nextSibling;
		}
	}
	var mCtrl = document.getElementById("menuControle");
	mCtrl.firstChild.firstChild.nodeValue="Activer le menu";
}
function menuController() {
	if (hasClassName(document.body,"dynamik")) unloadMenus();
	else loadMenus();
}
function eventLstMontrerMenu(evt) {
	var oNode;
	if (evt && evt.target) {
		oNode = evt.target;
	} else if (window.event) {
		oNode = window.event.srcElement;
	} else {
		oNode = this;
	}
//log("\n\n* eventLstMontrerMenu:: oNode > "+oNode.nodeName);
	if (menu_timerID!=null) {cacherMenus();}
	while (oNode.id!="menu") {
		if (hasClassName(oNode,"theme")) {
			addClassName(oNode,"currentTheme");
			break;
		}
		oNode = oNode.parentNode;
	}
	if (window.event &&
		(typeof(window.event.cancelBubble)=="boolean") )
	{
		window.event.cancelBubble = true;
	}
	return false;
}
function eventLstCacherMenus(evt) {
	var oNode, nRelatedTarget;
	if (evt && evt.target) {
		oNode = evt.target;
		nRelatedTarget = evt.relatedTarget;
	} else if (window.event) {
		oNode = window.event.srcElement;
		nRelatedTarget = window.event.toElement;
	} else {
		oNode = this;
	}
	while (oNode.id!="menu") {
		if (hasClassName(oNode,"theme")) {
			if (nRelatedTarget && (!isChildNodeOf(nRelatedTarget,oNode))) {
				menu_timerID = setInterval("cacherMenus(true)",800);
//log("\n* eventLstCacherMenus>SetInterval : "+menu_timerID);
			} else if (!nRelatedTarget) {
				cacherMenus();
//log("\n\n* eventLstCacherMenus>cacherMenus : tous");
			}
			break;
		}
		oNode = oNode.parentNode;
	}
	if (window.event &&
		(typeof(window.event.cancelBubble)=="boolean") )
	{
		window.event.cancelBubble = true;
	}
	return false;
}
function cacherMenus(bDisplaySelected) {
	if (menu_timerID!=null) {
//log("\n\n ==> cacherMenus>clearInterval : "+menu_timerID);
		clearInterval(menu_timerID);
		menu_timerID = null;
	}
	var nChild=document.getElementById("menu").firstChild;
	while (nChild) {
		if (hasClassName(nChild,"theme")) deleteClassName(nChild,"currentTheme");
		nChild=nChild.nextSibling;
	}
	//* On affiche le theme selectionné
	if (bDisplaySelected) {
    	var nChild=document.getElementById("menu").firstChild;
    	while (nChild) {
    		if (hasClassName(nChild,"theme") && hasClassName(nChild,"selected")) {
                addClassName(nChild,"currentTheme");
            }
    		nChild=nChild.nextSibling;
    	}
	}
	//*/
}
//* Debug
function exp(o) {
	var _t;
	for (p in o) {
		_t+= "\n"+p;
		switch (typeof o[p]) {
			case "function":
					_t+= " : function";
				break;
			case "object":
				_t+= " : Object";
				break;
			case "string":
				_t+= " : "+o[p]+" (String)";
				break;
			case "number":
				_t+= " : "+o[p]+" (number)";
				break;
			case "boolean":
				_t+= " : "+o[p]+" (boolean)";
				break;
			default:
				_t+= " undefined ("+typeof o[p]+")";
				break;
		}
	}
	return _t;
}
function log(s) {
	if (!document.getElementById("debug")) {
		return;
	}
	with (document.getElementById("debug")) {
		value=s+value;
	}
}
//*/
