/****** BEGIN LICENSE BLOCK *****
 * Copyright (c) 2005-2006 Harmen Christophe and contributors. All rights reserved.
 * 
 * This script is free software; you can redistribute it and/or
 *   modify under the terms of the Creative Commons - Attribution-ShareAlike 2.0
 * <http://creativecommons.org/licenses/by-sa/2.0/>
 * You are free:
 *     * to copy, distribute, display, and perform the work
 *     * to make derivative works
 *     * to make commercial use of the work
 * 
 * Under the following conditions:
 * _Attribution_. You must attribute the work in the manner specified by the
 *   author or licensor.
 * _Share Alike_. If you alter, transform, or build upon this work, you may
 *   distribute the resulting work only under a license identical to this one.
 *     * For any reuse or distribution, you must make clear to others 
 *      the license terms of this work.
 *     * Any of these conditions can be waived if you get permission from 
 *      the copyright holder.
 * 
 * Your fair use and other rights are in no way affected by the above.
 * 
 * This is a human-readable summary of the Legal Code (the full license). 
 * <http://creativecommons.org/licenses/by-sa/2.0/legalcode>
 ***** END LICENSE BLOCK ******/
function trim(s) {return s.replace(/(^\s+)|(\s+$)/g,"");}
function hasClassName(oNode,className) {
	return (oNode.nodeType==1)?
		((" "+oNode.className+" ").indexOf(" "+className+" ")!=-1):false;
}
function addClassName(oNode,className) {
	if ((oNode.nodeType==1) && !hasClassName(oNode,className))
		oNode.className = trim(oNode.className+" "+className);
}
function deleteClassName(oNode,className) {
	if (oNode.nodeType==1)
    oNode.className = trim((" "+oNode.className+" ").replace(" "+className+" "," "));
}
function isChildNodeOf(oNode,other) {
	if (oNode.compareDocumentPosition) {
		return (oNode.compareDocumentPosition(other)==10);
	} else if (other.contains) {
		return other.contains(oNode);
	}
	var bIsChildNodeOf = false;
	function _isChildNodeOf(oNode,other) {
		while (other) {
			if (other==oNode) {
				bIsChildNodeOf = true;
				return;
			} else _isChildNodeOf(oNode,other.firstChild);
			other = other.nextSibling;
		}
	}
	_isChildNodeOf(oNode,other.firstChild);
	return bIsChildNodeOf;
}
function addEventLst(EventTarget,type,listener,useCapture) {
	useCapture = typeof(useCapture)=="boolean"?useCapture:false;
	if (EventTarget.addEventListener) {
		EventTarget.addEventListener(type, listener, useCapture);
	} else if ((EventTarget==window) && document.addEventListener) {
		document.addEventListener(type, listener, useCapture);
	} else if (EventTarget.attachEvent) {
		EventTarget["e"+type+listener] = listener;
		EventTarget[type+listener] = function() {EventTarget["e"+type+listener](window.event);}
		EventTarget.attachEvent("on"+type, EventTarget[type+listener]);
	} else {
		EventTarget["on"+type] = listener;
	}
}
function removeEventLst(EventTarget,type,listener,useCapture) {
	useCapture = typeof(useCapture)=="boolean"?useCapture:false;
	if (EventTarget.removeEventListener) {
		EventTarget.removeEventListener(type,listener, useCapture);
	} else if ((EventTarget==window) && document.removeEventListener) {
		document.removeEventListener(type,listener, useCapture);
	} else if (EventTarget.detachEvent) {
		EventTarget.detachEvent("on"+type, EventTarget[type+listener]);
		EventTarget[type+listener]=null;
		EventTarget["e"+type+listener]=null;
	} else {
		EventTarget["on"+type]=null;
	}
}
function loaded() {
	if (!document.createElement && !document.createTextNode 
			&& !document.getElementById && !document.getElementsByTagName
			&& !document.insertBefore)
	{
		if (hasClassName(document.body,"dynamik"))
			deleteClassName(document.body,"dynamik");
		return;
	}
	loadMenus();
	/*
	var cSelected = YAHOO.util.Dom.getElementsByClassName("selected",
        "li",
        document.getElementById("menu").getElementsByTagName('ul').item(0));
    if (cSelected.length > 0) {
        addClassName(cSelected[0],"currentTheme");
    } else {
        try {vireLombre();} catch(e) {}
    }
    //*/
	// On affiche le theme selectionné
	//*
    var nChild=document.getElementById("menu").getElementsByTagName('ul').item(0).firstChild;
	countCurrentTheme = 0;
	while (nChild) {
		if (hasClassName(nChild,"theme") && hasClassName(nChild,"selected")) {
            addClassName(nChild,"currentTheme");
            countCurrentTheme ++;
        }
		nChild=nChild.nextSibling;
	}
	if (countCurrentTheme == 0) {
		try {vireLombre();} catch(e) {}
	}
	//*/
}
YAHOO.util.Event.onDOMReady(loaded);
var menu_timerID = null;
function loadMenus() {
	addClassName(document.body,"dynamik");
	var nMenu = document.getElementById("menu").getElementsByTagName('ul').item(0);
	/*
    if (nMenu.addEventListener) {
		nMenu.addEventListener("mouseover",eventLstMontrerMenu,true);
		nMenu.addEventListener("focus",eventLstMontrerMenu,true);
		nMenu.addEventListener("DOMFocusIn",eventLstMontrerMenu,true);
		nMenu.addEventListener("mouseout",eventLstCacherMenus,true);
		nMenu.addEventListener("blur",eventLstCacherMenus,true);
		nMenu.addEventListener("DOMFocusOut",eventLstCacherMenus,true);
	} else {
	//*/
		var nA;
		var nChild=nMenu.firstChild;
		while (nChild) {
			if (hasClassName(nChild,"theme")) {
				addEventLst(nChild,"mouseover",eventLstMontrerMenu);
				addEventLst(nChild,"mouseout",eventLstCacherMenus);
                for (var j=0; nA = nChild.getElementsByTagName("a")[j]; j++) {
					addEventLst(nA,"focus",eventLstMontrerMenu);
					addEventLst(nA,"blur",eventLstCacherMenus);
				}
			}
			nChild=nChild.nextSibling;
		}
	/*
    }
    //*/
}
function unloadMenus() {
	deleteClassName(document.body,"dynamik");
	var nMenu = document.getElementById("menu").getElementsByTagName('ul').item(0);
	/*
    if (nMenu.removeEventListener) {
		nMenu.removeEventListener("mouseover",eventLstMontrerMenu,true);
		nMenu.removeEventListener("focus",eventLstMontrerMenu,true);
		nMenu.removeEventListener("DOMFocusIn",eventLstMontrerMenu,true);
		nMenu.removeEventListener("mouseout",eventLstCacherMenus,true);
		nMenu.removeEventListener("blur",eventLstCacherMenus,true);
		nMenu.removeEventListener("DOMFocusOut",eventLstCacherMenus,true);
	} else {
	//*/
		var nA;
		var nChild=nMenu.firstChild;
		while (nChild) {
			if (hasClassName(nChild,"theme")) {
				removeEventLst(nChild,"mouseover",eventLstMontrerMenu);
				removeEventLst(nChild,"mouseout",eventLstCacherMenus);
                for (var j=0; nA = nChild.getElementsByTagName("a")[j]; j++) {
					removeEventLst(nA,"focus",eventLstMontrerMenu);
					removeEventLst(nA,"blur",eventLstCacherMenus);
				}
			}
			nChild=nChild.nextSibling;
		}
	/*
    }
    //*/
}
window.onunload = unloadMenus;
function eventLstMontrerMenu(evt) {
	var oNode;
	if (evt && evt.target) {
		oNode = evt.target;
	} else if (window.event) {
		oNode = window.event.srcElement;
	} else {
		oNode = this;
	}
	cacherMenus();
	while (oNode.id!="menu") {
		if (hasClassName(oNode,"theme")) {
			addClassName(oNode,"currentTheme");
			break;
		}
		oNode = oNode.parentNode;
	}
	if (window.event &&
		(typeof(window.event.cancelBubble)=="boolean") )
	{
		window.event.cancelBubble = true;
	}
	return false;
}
function eventLstCacherMenus(evt) {
	var oNode, nRelatedTarget;
	if (evt && evt.target) {
		oNode = evt.target;
		nRelatedTarget = evt.relatedTarget;
	} else if (window.event) {
		oNode = window.event.srcElement;
		nRelatedTarget = window.event.toElement;
	} else {
		oNode = this;
	}
	while (oNode.id!="menu") {
		if (hasClassName(oNode,"theme")) {
		if (nRelatedTarget && (!isChildNodeOf(nRelatedTarget,oNode))) {
				menu_timerID = setInterval("cacherMenus(true)",800);
			} else if (!nRelatedTarget) {
				cacherMenus();
			}
			break;
		}
		oNode = oNode.parentNode;
	}
	if (window.event &&
		(typeof(window.event.cancelBubble)=="boolean") )
	{
		window.event.cancelBubble = true;
	}
	return false;
}
function cacherMenus(bDisplaySelected) {
	if (menu_timerID!=null) {
		clearInterval(menu_timerID);
		menu_timerID = null;
	}
	var nChild=document.getElementById("menu").getElementsByTagName('ul').item(0).firstChild;
	while (nChild) {
		if (hasClassName(nChild,"theme")) deleteClassName(nChild,"currentTheme");
		nChild=nChild.nextSibling;
	}
	if (bDisplaySelected) {
    	// On affiche le theme selectionné
    	var nChild=document.getElementById("menu").getElementsByTagName('ul').item(0).firstChild;
    	countCurrentTheme = 0;
    	while (nChild) {
    		if (hasClassName(nChild,"theme") && hasClassName(nChild,"selected")) {
                addClassName(nChild,"currentTheme");
                nChild.getElementsByTagName("p").item(0).onmouseover();
                countCurrentTheme ++;
            }
    		nChild=nChild.nextSibling;
    	}
    	if (countCurrentTheme == 0) {
    		try {
                vireLombre();
            	document.getElementById('prose').style.backgroundColor='transparent';
            	document.getElementById('pvert').style.backgroundColor='transparent';
            	document.getElementById('pbleu').style.backgroundColor='transparent';
            	document.getElementById('rose').style.backgroundColor='transparent';
            	document.getElementById('vert').style.backgroundColor='transparent';
            	document.getElementById('bleu').style.backgroundColor='transparent';
            } catch(e) {}
    	}
	}
}

/*function passedessusmenu(id,image){
	document.getElementById('prose').className='backTransparent';
	document.getElementById('pvert').className='backTransparent';
	document.getElementById('pbleu').className='backTransparent';
	document.getElementById('rose').className='backTransparent';
	document.getElementById('vert').className='backTransparent';
	document.getElementById('bleu').className='backTransparent';
	document.getElementById(id).className='backOpak';
	alert(id);
	document.getElementById('bandeauHaut').className="avecFond";
}

function afficheLombre(image){
	document.getElementById('bandeauHaut').className="avecFond";
}
function vireLombre(){
	document.getElementById('bandeauHaut').className="sansFond";
}
*/


function passedessusmenu(id,image,couleur){
	document.getElementById('prose').style.backgroundColor='transparent';
	document.getElementById('pvert').style.backgroundColor='transparent';
	document.getElementById('pbleu').style.backgroundColor='transparent';
	document.getElementById('rose').style.backgroundColor='transparent';
	document.getElementById('vert').style.backgroundColor='transparent';
	document.getElementById('bleu').style.backgroundColor='transparent';
	document.getElementById(id).style.backgroundColor=couleur;
	document.getElementById('bandeauHaut').style.backgroundImage="url(\'"+image+"\')";
	document.getElementById('bandeauHaut').style.backgroundPosition="bottom left";
	document.getElementById('bandeauHaut').style.backgroundRepeat="no-repeat";
	document.getElementById('bandeauHaut').className="avecFond";
}

function afficheLombre(image){
	document.getElementById('bandeauHaut').style.backgroundImage="url(\'"+image+"\')";
	document.getElementById('bandeauHaut').style.backgroundPosition="bottom left";
	document.getElementById('bandeauHaut').style.backgroundRepeat="no-repeat";
}
function vireLombre(){
	document.getElementById('bandeauHaut').style.backgroundImage="none";
}
