// JavaScript Document
$(document).ready(function() {
	// Carousel
	$(".carousel").jCarouselLite({
		btnNext: ".next",
		btnPrev: ".prev",
		visible: 1,
		speed: 800
	});
	// Onglets
	$('ul.onglets').tabs({selected:0, fx:{opacity:"toggle"}});
	// Lightbox
	$('a.lightbox').lightBox();
	// Identifie les liens pointant vers des sites externes (ajout d'un pictogramme � droite du lien)
	$("#principal a[@href^=\"http\"]").addClass("lien_externe");
	$("#principal a.lightbox").removeClass("lien_externe");
	$("#readspeaker").removeClass("lien_externe");
	$("div.image a").removeClass("lien_externe");
	// Aspect des lignes (<tr>) des tableaux de donn�es au survol
	$('table.tableau_donnees tr').mouseover(function(){$(this).addClass('survol');}).mouseout(function(){$(this).removeClass('survol');});
	
	// autocompletion
		function findValueCallback(event, data, formatted) {
		$("<li>").html( !data ? "No match!" : "Selected: " + formatted).appendTo("#result");
		}

		$("#event_communes").autocomplete("/suggest/communes", {
				multiple: true,
				width: 150,
				selectFirst: false
			});
		$("#event_communes").result(function(event, data, formatted) {
			if (data)
				$(this).parent().next().find("input").val(data[1]);
		});
		
		$("#fiche_elu_nom").autocomplete("/suggest/fiche_elu_nom", {
				multiple: true,
				width: 150,
				selectFirst: false
			});
		$("#fiche_elu_nom").result(function(event, data, formatted) {
			if (data)
				$(this).parent().next().find("input").val(data[1]);
		});
		
		$("#fiche_elu_cesr_nom").autocomplete("/suggest/fiche_elu_cesr_nom", {
				multiple: true,
				width: 150,
				selectFirst: false
			});
		$("#fiche_elu_cesr_nom").result(function(event, data, formatted) {
			if (data)
				$(this).parent().next().find("input").val(data[1]);
		});
		
		$("#fiche_elu_cesr_college").autocomplete("/suggest/fiche_elu_cesr_college", {
				multiple: true,
				width: 150,
				selectFirst: false
			});
		$("#fiche_elu_cesr_college").result(function(event, data, formatted) {
			if (data)
				$(this).parent().next().find("input").val(data[1]);
		});
		
});