/**
 * 2 functions to correct a bug on Linux/Mac system with modalbox
 * all flash contents are hidden
 */
var ModalboxFix = {

	onShow:function(instance)
	{
		if (navigator.appVersion.indexOf("Mac") > 0||navigator.appVersion.indexOf('X11') > 0 ||navigator.appVersion.indexOf('linux') > 0)
			this.toggleSwf('hidden');
	},
	afterHide:function(instance)
	{
		if (navigator.appVersion.indexOf("Mac") > 0||navigator.appVersion.indexOf('X11') > 0 ||navigator.appVersion.indexOf('linux') > 0)
			this.toggleSwf('visible');
	},
	toggleSwf:function(visibility)
	{
		flashes2 = document.getElementsByTagName("embed");
		for(i = 0; i < flashes2.length; i++) {
			flashes2[i].style.visibility = visibility;
		}
	}
}