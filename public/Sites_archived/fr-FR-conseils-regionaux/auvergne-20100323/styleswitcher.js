function changeTextSize (a)  
{
	var active_style = Number(getActiveStyleSheet());
	if (a == "+")
	{
		style = active_style < 3 ? active_style+1 : active_style;
	}
	if (a == "-")
	{
		style = active_style > 0 ? active_style-1 : active_style;
	}
	if ( style == 0 )
	{
		setNoActiveStyleSheet();
	}
	else if ( style < 3 )
	{
		setActiveStyleSheet(style);
	}
}
function setActiveStyleSheet(title) {
  var i, a, main;
  for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
    if(a.getAttribute("rel").indexOf("style") != -1 && a.getAttribute("title")) {
      a.disabled = true;
      if(a.getAttribute("title") == "bigger"+title) a.disabled = false;
    }
  }
  setFooter();
}
function setNoActiveStyleSheet() {
  var i, a, main;
  for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
    if(a.getAttribute("rel").indexOf("style") != -1 && a.getAttribute("title")) {
      a.disabled = true;
    }
  }
  setFooter();
}
function getActiveStyleSheet() {
  var i, a;
  for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
    if(a.getAttribute("rel").indexOf("style") != -1 && a.getAttribute("title") && !a.disabled) 
	{
		title = a.getAttribute("title");		
		return title.substring(6, title.length);
	}
  }
  return null;
}
function getPreferredStyleSheet() {
  var i, a;
  for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
    if(a.getAttribute("rel").indexOf("style") != -1
       && a.getAttribute("rel").indexOf("alt") == -1
       && a.getAttribute("title")
       ) return a.getAttribute("title");
  }
  return null;
}
function createCookie(name,value,days) {
  if (days) {
    var date = new Date();
    date.setTime(date.getTime()+(days*24*60*60*1000));
    var expires = "; expires="+date.toGMTString();
  }
  else expires = "";
  document.cookie = name+"="+value+expires+"; path=/";
}
function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1,c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}
activeSheetnumber = 1;
window.onload = function(e) {
  var cookie = readCookie("style");
  cookie ? setActiveStyleSheet(cookie) : setNoActiveStyleSheet();
}
window.onunload = function(e) {
  var title = getActiveStyleSheet();
  createCookie("style", title, 365);
}