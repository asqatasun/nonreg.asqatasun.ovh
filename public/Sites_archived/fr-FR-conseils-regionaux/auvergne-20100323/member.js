/**
 * CraMember is an object that encapsulate the logon, register ...
 * transaction requests & callback
 */
 
var PmsUrl = {
	getBaseUrl : function()
	{
		 // loc is the relative path your wish to redirect to
		 var b = document.getElementsByTagName('base');
		 var loc = '' ;
		 if (b && b[0] && b[0].href) {
		   if (b[0].href.substr(b[0].href.length-1) == '/' && loc.charAt(0) == '/')
		     loc = loc.substr(1);
		   loc = b[0].href + loc;
		 }
		 return loc;
	}
}
 
var CraMember = {
	/**
	 * complete callback
	 */
	completeRegister : function(request){
		var result = eval('(' + request.responseText + ')');
	
		$('logon_wait').style.display = 'none';
		if (result.isValid)
		{
			Modalbox.show('', 'abonnement_confirme.html', {width:380, height:250, afterHide:function(){window.location.reload();}});
		}
		else
		{
			$('logon_error').innerHTML = result.message;
			$('logon_error').style.display = 'block';
		}
	},
	/**
	 * the post method
	 */
	register: function(form){
		$('logon_error').style.display = 'none';
		$('logon_wait').style.display = 'block';
	
		var ajax = new Ajax.Request(PmsUrl.getBaseUrl() + 'cra/member/register', 
		{
			method: 'post', 
			parameters: Form.serialize(form),
			onComplete: CraMember.completeRegister
		});
	},
	/**
	 * complete callback
	 */
	completeSave : function(request){
		var result = eval('(' + request.responseText + ')');
	
		$('profil_wait').style.display = 'none';
		if (result.isValid)
		{
			Modalbox.show('', 'profil_save.html', {width:380, height:250, afterHide:function(){window.location.href=PmsUrl.getBaseUrl() +  'monprofil.html'}});
		}
		else
		{
			$('profil_error').innerHTML = result.message;
			$('profil_error').style.display = 'block';
		}
	},
	/**
	 * the post method
	 */
	save: function(form){
		$('profil_error').style.display = 'none';
		$('profil_wait').style.display = 'block';
	
		var ajax = new Ajax.Request(PmsUrl.getBaseUrl() + 'cra/member/save', 
		{
			method: 'post', 
			parameters: Form.serialize(form),
			onComplete: CraMember.completeSave
		});
	},
	/**
	 * complete callback
	 */
	 completeDelete : function(request){
		Modalbox.show('Suppression inscription', 'profil_delete_confirm.html', {width:380, height:250, afterHide:function(){window.location.href=PmsUrl.getBaseUrl() + 'index.html'}});
	},
	deleteProfil: function(id){
		var ajax = new Ajax.Request(PmsUrl.getBaseUrl() + 'cra/member/delete', 
		{
			method: 'post', 
			parameters: Form.serialize(id),
			onComplete: CraMember.completeDelete
		});
	},
	 
	 /**
	 * complete callback
	 */
	completeAddToSelection : function(request){
		var result = eval('(' + request.responseText + ')');
		if (result.isValid)
		{
			Modalbox.show('', 'selection_confirmee.html', {width:380, height:250});
		}
		else
		{
			Modalbox.show('', 'selection_erreur.html', {width:380, height:250});
		}
	},
	/**
	 * the addToSelection method
	 */
	addToSelection: function(id_page){
		var ajax = new Ajax.Request(PmsUrl.getBaseUrl() + 'cra/member/addtoselection', 
		{
			method: 'post', 
			parameters: 'id_page=' + id_page,
			onComplete: CraMember.completeAddToSelection
		});
	},
	/**
	 * complete callback
	 */
	completeDelToSelection : function(request){
		var result = eval('(' + request.responseText + ')');
		Modalbox.show('', 'selection_supprime.html', {width:380, height:250, afterHide:function(){window.location.reload();}});
	},
	/**
	 * the delToSelection method
	 */
	delToSelection: function(id_page, site){
		var ajax = new Ajax.Request(PmsUrl.getBaseUrl() + 'cra/member/deltoselection', 
		{
			method: 'post', 
			parameters: 'id_page=' + id_page + '&site=' + site,
			onComplete: CraMember.completeDelToSelection
		});
	},
	/**
	 * complete callback
	 */
	completeSendArticle : function(request){
		var result = eval('(' + request.responseText + ')');
		if (result.isValid)
		{
			$('MB_content').innerHTML = result.text ;
		}
		else
		{
			$('send_error').innerHTML = result.message;
			$('send_error').style.display = 'block';
		}
			
	},
	/**
	 * the sendArticle method
	 */
	sendArticle : function(form){
		$('send_error').style.display = 'none';
		var ajax = new Ajax.Request(PmsUrl.getBaseUrl() + 'cra/member/sendarticle', 
		{
			method: 'post', 
			parameters: Form.serialize(form),
			onComplete: CraMember.completeSendArticle
		});
	},		
	/**
	 * complete callback
	 */
	completeLogon : function(request){
		var result = eval('(' + request.responseText + ')');
	
		if (result.isValid)
		{
			Modalbox.show('', 'connexion_confirmee.html', {width:380, height:250, afterHide:function(){window.location.reload();}});
		}
		else
		{
			Modalbox.show('', 'connexion_erreur.html', {width:380, height:250, afterHide:function(){$('abo_email').focus();}});
		}
	},
	
	logon: function(form){
		var ajax = new Ajax.Request(PmsUrl.getBaseUrl() + 'cra/member/login', 
		{
			method: 'post', 
			parameters: Form.serialize(form),
			onComplete: CraMember.completeLogon
		});
		CraMember.logonAll(form) ; 
	},
	logonAll: function(form)
	{
	   	/*var s = document.createElement('script'); 
        s.src = 'http://www.auvergne.org/cra/member/loginget?' + Form.serialize(form) ;
        s.type = 'text/javascript'; 
        
        document.body.appendChild(s); 
        
        var s1 = document.createElement('script'); 
        s1.src = 'http://www.auvergneinfo.com/cra/member/loginget?' + Form.serialize(form) ;
        s1.type = 'text/javascript';
        
        document.body.appendChild(s1);*/
        
        var urls=new Array(
            'http://www.auvergne.org/cra/member/loginget?' + Form.serialize(form),
            'http://www.auvergne.info/cra/member/loginget?' + Form.serialize(form),
			'http://www.auvergne.eu/account/login/processget?' + Form.serialize(form)
        );
        
        for(var i=0; i<urls.length; i++){
            var f1=document.createElement('iframe');
            f1.src=urls[i];
            f1.style.display='none';
            document.body.appendChild(f1);
        } 
	},
	/**
	 * complete callback
	 */
	completeLogonJour : function(request){
		var result = eval('(' + request.responseText + ')');
	
		if (result.isValid)
		{
			Modalbox.show('', 'connexion_confirmee.html', {width:380, height:250, afterHide:function(){window.location = 'presse/espace_jour.html';}});
		}
		else
		{
			Modalbox.show('', 'connexion_erreur.html', {width:380, height:250, afterHide:function(){$('login').focus();}});
		}
	},
	
	logonJour: function(form){
		var ajax = new Ajax.Request(PmsUrl.getBaseUrl() + 'cra/member/login', 
		{
			method: 'post', 
			parameters: Form.serialize(form),
			onComplete: CraMember.completeLogonJour
		});
	},
	
	/**
	 * complete callback
	 */
	completeLogout : function(request){
		window.location = PmsUrl.getBaseUrl() + 'index.php';
	},
	
	logout:function(){
		CraMember.logoutAll() ;
		var ajax = new Ajax.Request(PmsUrl.getBaseUrl() + 'cra/member/logout', 
		{
			method: 'post', 
			onComplete: CraMember.completeLogout
		});
	},
	logoutAll: function()
	{
	   	var s = document.createElement('script'); 
        s.src = 'http://www.auvergne.org/cra/member/logout' ;
        s.type = 'text/javascript'; 
        
        document.body.appendChild(s); 
        
        var s1 = document.createElement('script'); 
        s1.src = 'http://www.auvergneinfo.com/cra/member/logout' ;
        s1.type = 'text/javascript';
        
        document.body.appendChild(s1); 
	},
	
	/**
	 * complete callback
	 */
	completeContact : function(request){
		var result = eval('(' + request.responseText + ')');
	
		if (result.isValid)
		{
			Modalbox.show('', 'contact_confirme.html', {width:380, height:250, afterHide:function(){window.location='index.html';}});
		}
		else
		{
			$('contact_error').innerHTML = result.message;
			$('contact_error').style.display = 'block';
		}
	},
	
	sendContact : function(form){
		$('contact_error').style.display = 'none';
		var ajax = new Ajax.Request(PmsUrl.getBaseUrl() + 'cra/member/contact', 
		{
			method: 'post', 
			parameters: Form.serialize(form),
			onComplete: CraMember.completeContact
		});
	},
	
	/**
	 * complete callback
	 */
	completeAskPasswordJour : function(request){
		var result = eval('(' + request.responseText + ')');
	
		if (result.isValid)
		{
			Modalbox.show('', 'demande_jour_confirme.html', {width:380, height:250});
		}
		else
		{
			$('contact_error').innerHTML = result.message;
			$('contact_error').style.display = 'block';
		}
	},
	
	askPasswordJour : function(form){
		$('contact_error').style.display = 'none';
		var ajax = new Ajax.Request(PmsUrl.getBaseUrl() + 'cra/journaliste/register', 
		{
			method: 'post', 
			parameters: Form.serialize(form),
			onComplete: CraMember.completeAskPasswordJour
		});
	},
	
	/**
	 * complete callback
	 */
	completeSendPassword : function(request){
		var result = eval('(' + request.responseText + ')');
	
		if (result.isValid)
		{
			Modalbox.show('', 'envoi_motdepasse_confirme.html', {width:380, height:250});
		}
		else
		{
			$('password_error').innerHTML = result.message;
			$('password_error').style.display = 'block';
		}
	},
	
	sendPassword : function(form){
		$('password_error').style.display = 'none';
		var ajax = new Ajax.Request(PmsUrl.getBaseUrl() + 'cra/member/password', 
		{
			method: 'post', 
			parameters: Form.serialize(form),
			onComplete: CraMember.completeSendPasswordJour
		});
	},
	
	/**
	 * complete callback
	 */
	completeSendPasswordJour : function(request){
		var result = eval('(' + request.responseText + ')');
	
		if (result.isValid)
		{
			Modalbox.show('', 'envoi_motdepasse_confirme.html', {width:380, height:250});
		}
		else
		{
			$('password_error').innerHTML = result.message;
			$('password_error').style.display = 'block';
		}
	},
	
	sendPasswordJour : function(form){
		$('password_error').style.display = 'none';
		var ajax = new Ajax.Request(PmsUrl.getBaseUrl() + 'cra/journaliste/password', 
		{
			method: 'post', 
			parameters: Form.serialize(form),
			onComplete: CraMember.completeSendPasswordJour
		});
	},
	/**
     * complete callback
     */
    completeDesabo : function(request){
        var result = eval('(' + request.responseText + ')');
    
        if (result.isValid)
        {
            $('contact_error').innerHTML = result.message ;
            $('contact_error').style.display = 'block';
        }
        else
        {
            $('contact_error').innerHTML = result.message;
            $('contact_error').style.display = 'block';
        }
    },
    sendDesabo : function(form){
       
        $('contact_error').style.display = 'none';
        var ajax = new Ajax.Request(PmsUrl.getBaseUrl() + 'cra/member/desabo', 
        {
            method: 'post', 
            parameters: Form.serialize(form),
            onComplete: CraMember.completeDesabo
        }); 
    }
};
