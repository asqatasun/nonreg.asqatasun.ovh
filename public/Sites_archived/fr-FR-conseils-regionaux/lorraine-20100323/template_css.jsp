





/* ------------------------------------------------ */
/*               GLOBAL SETTINGS                    */
/* ------------------------------------------------ */
body {
    background-color: #dddddd;
    text-align: left;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 11px;
    color: #333333;
    margin:0px 0 0 0;
}

a {
    text-decoration: none;
    color: #333333;
}
a:hover {
    text-decoration: underline;
}
div.dotted {
    background: url(dotted.gif) repeat-x top;
    display:block;
    height:1px;
}

/* ------------------------------------------------ */
/*                 MAIN STRUCT                      */
/* ------------------------------------------------ */

.MainShell {
  background-color: inherit;
  visibility: hidden;
}


#wrapper {
    margin: 0;
    width: 1004px;
    text-align: left;
    display:block;
}

#topmodules {
	margin: 0;
	top: 0px;
	width: 1004px;
  border-bottom: 1px dotted #AAAAAA;
}


#toplinks {
	clear:both;
}

#header {
    /*background: url(../img/header.gif) no-repeat top left;*/
    display:block;
    width:1004px;
    float:left;
    clear:both;
    background-color:#fff;
}
#pagebody {
    /*background: url(../img/main.gif) repeat-y;*/
    display:block;
    padding: 0 0px 20px 0px;
    width: 1004px;
    float:left;
    clear:both;
    background-color:#fff;
}
#footer {
    background: url(dotted.gif) repeat-x top left;
    display:block;
    width: 1004px;
    float:left;
    clear:both;
    text-align: left;
    background-color:#fff;
    padding-top:10px;
}
#footer .margin {
    padding: 0 20px 40px 20px;
    display:block;
}
#pagetitle {
    display:none;
}
/* ------------------------------------------------ */
/*                   HEADER                         */
/* ------------------------------------------------ */


#thechat {
	display: block;
	float: left;
	margin: 0 10px 0 10px;
}

#theprocess {
    display: block;
    float: left;
    margin: 0 30px 0 20px;
}
#adminbuttons {
    display: block;
    float: left;
    margin: 0 30px 0 20px;
}

#adminbuttons a {
    color: #402808;
    padding: 5px 5px 0 5px;
    font-size:10px;
}
a.green {
    color: green !important;
}
a.orange {
    color: orange !important;
}
a.red {
    color: red !important;
}

#theprocess a {
    color: #333;
    font-size:11px;
}

#quicklinkright {
    float:right;
    padding:0px 20px 0 0;
}
#quicklinkright ul {
    list-style: none;
    margin: 0;
    padding:0;
}
#quicklinkright li {
    display: inline;
    margin: 0;
    padding: 0;
}
#quicklinkright a {
    white-space:nowrap;
    font-size:10px;
    color: #402808;
    float: left;
    display: block;
    padding: 10px 5px 0 5px;
    border-right: 1px solid #d2d2d2;
}
#logo {
    padding: 5px 20px 0px 20px;
    float:left;
}
#search {
    position:absolute;
    width: 300px;
    height: 40px;
    margin: 25px 0 0 10px;
    display:none;
    padding:10px 5px 5px 10px;
    background-color:#e9e9e9;
    border:1px #ccc solid;
    z-index:100;
}
#search input {
    display:inline;
}
.search ul {
    margin:0;
    padding:0;
}
.search li {
    display:block;
    padding-bottom:10px;
}
.bold {
    font-weight: bold;
}
span.hl {
    background: #FFFF66;
}
/* ------------------------------------------------ */
/*                   topmenu                           */
/* ------------------------------------------------ */

#topmenu {
    float:left;
    clear:both;
    font-size:11px;
    display:block;
    width:1004px;
    padding: 10px 0 0 40px;
    width: 964px !important;
    background-color:#bbd9ee;
    border-top:4px solid #54a4de;
}
#topmenu div.selected,
#topmenu div.notselected {
    float:left;
}
#topmenu div.actions {
    float:left;
    padding: 3px 5px 3px 5px;
}
#topmenu  a {
    float:left;
    text-decoration: none;
}
#topmenu  a:hover {
    text-decoration: none;
}
#topmenu  a span {
    padding: 7px 17px 7px 15px;
    color:#000000;
    text-decoration: none;
    display:block;
}
#topmenu a.link {
    background: url(top_right.gif) no-repeat 100% 0;
}
#topmenu a.link span {
    background: url(top_left.gif) no-repeat 0 0;
}
#topmenu a:hover.link {
    background: url(top_right.gif) no-repeat 100% 0;
    background-position: 100% -30px;
}
#topmenu a:hover.link span {
    background: url(top_left.gif) no-repeat 0 0;
    background-position: 0 -30px;
}
#topmenu div.selected a.link {
    background-position: 100% -60px;
}
#topmenu div.selected a.link span {
    background-position: 0 -60px;

}
#topmenu div.notselected a.link span {
    border-bottom:1px solid #bbd9ee;
}
#topmenu div.selected a.link span {
    border-width:0px !important;
}
/* ------------------------------------------------ */
/*                   NAV                            */
/* ------------------------------------------------ */
#navigation {
    width: 240px;
    float: left;
    background: #f1efe2 url(nav_top.gif) no-repeat right top;
    margin-top:15px;
    padding-top:27px;
}
#navigation .navdeco {
    background: #f1efe2 url(nav_bottom.gif) no-repeat right bottom;
    padding-bottom:27px;
}
#navigation .shortcuts {
    float: left;
    display:block;
    padding:10px;
    width: 220px !important;
    width/**/:240px;
    color:white;
}
#navigation h1 {
    padding-left:10px;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 18px !important;
    color:#402808;
}
#navigation h2 {
    margin-left:10px;
}
#navigation a {
    font-family: Arial, Helvetica, sans-serif;
    font-size: 11px;
    color: black;
    font-weight:bold;
    padding: 2px;
    text-decoration: none;
}
#navigation a:hover {
    text-decoration: underline;
}

#navigation a.current {
    color: #507076;
}
#navigation div.level1 {
    padding: 5px 0 5px 10px;
    display:block;
    border-bottom:2px solid #ffffff;
}
#navigation div.isFirst div.level1 {
    background: none;
}
#navigation div.level1 a.link1 {
    background: url(arrow.gif) no-repeat 0 3px;
    padding-left:20px;
}
#navigation div.level2 {
    padding: 2px 0 2px 30px;
    display:block;
    border-bottom:1px solid #ffffff;
}
#navigation div.level2 a.link2 {
    background: url(arrow2.gif) no-repeat 0 3px;
    padding-left:13px;
}
#navigation div.level2 a.current {
    background: url(arrow3.gif) no-repeat 0 3px;
    padding-left:13px;
}

#navigation div.level3 {
    padding: 2px 0 2px 40px;
    display:block;
    border-bottom:1px solid #ffffff;
}
#navigation div.level3 a {
    font-weight:normal;
}
#navigation div.level3 a.link3 {
    background: url(arrow4.gif) no-repeat 0 6px;
    padding-left:10px;
}
#navigation div.level3 a.current {
    font-weight:bold;
    background: url(arrow5.gif) no-repeat 0 6px;
    padding-left:10px;
}
#navigation div.level4 {
    padding: 2px 0 2px 50px;
    display:block;
    border-bottom:1px solid #ffffff;
}
#navigation div.level4 a {
    font-weight:normal;
}
#navigation div.level4 a.link4 {
    background: url(arrow6.gif) no-repeat 0 8px;
    padding-left:6px;
}
#navigation div.level4 a.current {
    font-weight:bold;
    background: url(arrow7.gif) no-repeat 0 8px;
    padding-left:6px;
}
#navigation div.level5 {
    padding: 2px 0 2px 60px;
    display:block;
    border-bottom:1px solid #ffffff;
}
#navigation div.level5 a {
    font-weight:normal;
}
#navigation div.level5 a.current {
    font-weight:bold;
}
#navigation div.level6 {
    padding: 2px 0 2px 70px;
    display:block;
}
#navigation div.level6 a {
    font-weight:normal;
    border-bottom:1px solid #ffffff;
}
#navigation div.level6 a.current {
    font-weight:bold;
}
#navigation div.level7 {
    padding: 2px 0 2px 70px;
    display:block;
    border-bottom:1px solid #ffffff;
}
#navigation div.level7 a {
    font-weight:normal;
}
#navigation div.level7 a.current {
    font-weight:bold;
}
#navigation form {
    padding:10px;
}
#navigation form a {
    font-weight:normal;
}
#navigation .searchbox2 {
    clear:both;
    background: #548a94 url(dotted.gif) repeat-x top left;
    padding: 2px 0 2px 5px;
    margin: 20px 0 10px 0;
    display: block;
}
#navigation .searchbox2 form {
}
#navigation .searchbox2 form input {
    font-size:10px;
    color:#878787;
    margin: 2px 2px 2px 0;
}
#navigation .searchbox2 form a {
    color: #ffffff;
    padding: 2px 5px 2px 5px;
    margin: 2px 2px 2px 0;
    font-weight: bold;
}
#navigation box {
    display: block;
    float: left;
    clear:both;
    width: 180px;
    padding-bottom: 15px;
}
#navigation .box .content {
    padding: 0 0 5px 0;
    width: 175px;
    display: block;
}

.new {
    padding: 5px 0px 5px 0;
    display: block;
}
.new .title,
.new .title a {
    color: #dd6330;
    font-size:11px;
    font-weight: bold;
}
.new .date {
    font-size:9px;
    color: #d2d2d2;
}


/* ------------------------------------------------ */
/*                  content                         */
/* ------------------------------------------------ */
#main content.splash {
    border:0;
    display:block;
}
#content {
    float: right;
    width: 744px;
    width/**/: 754px;
    padding-right: 10px;
}
.path {
    float: left;
    width: 734px;
    width/**/: 744px;
    padding: 18px 10px 3px 10px;
    display:block;
    /*background-color:#faf1c3;*/
}
.path,
.path a {
    color:#444444;
}

#content .full {
    float: left;
    width: 693px;
    width/**/: 713px;
    padding: 5px 10px 10px 10px;
    display:block;
}
#content .full2 {
    float: left;
    width: 693px;
    width/**/: 713px;
    padding: 5px 10px 10px 10px;
    display:block;
}
#content .leftcol {
    clear: both;
    float: left;
    width: 460px;
    width/**/: 480px;
    margin-top: 10px;
    padding-left: 10px;
    padding-right: 10px;
    background: url(dotted.gif) repeat-y top right;

}
#content .rightcol {
    float: left;
    width: 224px;
    width/**/: 234px;
    margin: 10px 0 0 10px;;
}
#content .maincontent img {
    border: 1px solid #d2d2d2;
    padding: 2px;
}
#content .maincontent img.left {
    margin-right: 10px;
}
#content .maincontent img.right {
    margin-left: 10px;
}
#content .maincontent a {
    text-decoration:underline;
}
#content .splash {
/*
    background: url(../img/splash.gif) no-repeat;
    display: block;
    width: 539px;
    height: 82px;
    */
}
/* ------------------------------------------------ */
/*                  footer                          */
/* ------------------------------------------------ */
#footer {
    text-align: left;
}
#footer div.footerlink1,
#footer div.footerlink {
    float:left;
    font-size:9px;
    font-family: Verdana,Arial, Helvetica, sans-serif;
    padding: 0px 5px 0 5px;
}
#footer div.footerlink {
    border-left: 1px solid #d2d2d2;
}
#footer div.footerlinklogo {
    display: block;
    clear: both;
}

#footer a.flink {
    color: #402808;
    text-decoration:none;
}
#footer a.flink:hover {
    text-decoration:underline;
}

#footer .copyright {
    width: 710px;
    display: block;
    float: left;
    clear: both;
    margin-top: 10px;
    padding-top: 10px;
    margin-bottom: 20px;
}
/* ------------------------------------------------ */
/*                  files                           */
/* ------------------------------------------------ */
table.files tr td {
    vertical-align: top;
    padding: 1px;
    font-size:11px;
}
table.files tr:hover {
    background: #e9e9e9;
}
table.files tr td.nowrap {
    white-space: nowrap;
}
.searchResultListing a,
table.files a {
    color:#402808;
    display: block;
    padding-bottom: 2px;
}
table.files a {
    font-weight:bold;
}
a.file {
    background: url(file.gif) no-repeat;
    padding-left: 20px !important;
}
a.video {
    background: url(video.gif) no-repeat;
    padding-left: 20px !important;
}
a.ppt {
    background: url(ppt.gif) no-repeat;
    padding-left: 20px !important;
}
a.exe {
    background: url(exe.gif) no-repeat;
    padding-left: 20px !important;
}
a.doc {
    background: url(doc.gif) no-repeat;
    padding-left: 20px !important;
}
a.dir {
    background: url(dir.gif) no-repeat;
    padding-left: 20px !important;
}
a.html {
    background: url(html.gif) no-repeat;
    padding-left: 20px !important;
}

a.img {
    background: url(img.gif) no-repeat;
    padding-left: 20px !important;
}

a.pdf {
    background: url(pdf.gif) no-repeat;
    padding-left: 20px !important;
}

a.sound {
    background: url(sound.gif) no-repeat;
    padding-left: 20px !important;
}

a.txt {
    background: url(txt.gif) no-repeat;
    padding-left: 20px !important;
}

a.xls {
    background: url(xls.gif) no-repeat;
    padding-left: 20px !important;
}
a.csv {
    background: url(xls.gif) no-repeat;
    padding-left: 20px !important;
}

a.zip {
    background: url(zip.gif) no-repeat;
    padding-left: 20px !important;
}
/* ------------------------------------------------ */
/*                  links                           */
/* ------------------------------------------------ */
.error {
    color: #ff0000;
}
.fieldset {
    padding: 0;
    margin: 0;
}
.links {
    display: block;
    padding-bottom:3px;
}
.links a {
    color:#402808;
    font-weight:bold;
}
DIV#errors { color : #B42C29; }
DIV#errors li { color : #B42C29; }

#content ul.list {
    list-style: none;
    margin: 0;
    padding:0;
}
#content ul.list li {
    margin: 0;
    padding:0 0 20px 0;
}



table.forms td {
    padding: 0 5px 0 5px;
    background: #e9e9e9;
    vertical-align: top;
}
table.forms tr.title td {
    background: #402808;
    font-weight: bold;
    color: #ffffff;
}




 /* calendar */
div.calendar {
    background-color: #FFFFFF;
    display: block;
    /*float: left;*/
    /*font-family: Arial, Helvetica, sans-serif;*/
    font-family: Courrier;
    font-size: 11px;
    line-height: 17px;
    margin-right: 10px; /* Invalid value: width: 170; */
}
div.calendar div.pre {
    white-space: pre;
}

div.calendar a {
    color: #000066;
    text-decoration: none;
    font-family: "Courier New", Courier, mono;
    font-size: 11px;
}
div.calendar a.weekOfYear {
    color: #75777B;
}
div.calendar .currentweek,
div.calendar a.currentday {
    background-color: #E7E7E7;
    border: 1px solid #B42C29;
    font-weight: bold;
}

div.calendar a:hover {
    background-color: #E7E7E7;
}

div.calendar h2 {
    color: #006699;
    font-weight: bold;
    font-size: 12px;
    text-align: center;
}

div.calendar h2 a {
    color: #006699;
    padding: 0 5px 0 5px;
}
div.calendar span.days {
    background-color: #E7E7E7;
    font-weight: bold;
    padding: 0 2px 0 2px;
}

div.xmlButton {
    font-family: verdana,helvetica,arial,sans-serif;
    font-size: 10px;
    background-color: #ff6600;
    color: #FFFFFF;
    text-decoration:  none;
    padding: 0px 1px 0px 1px;
}

a.xmlButton {
    font-family: verdana,helvetica,arial,sans-serif;
    font-size: 10px;
    font-weight: bold;
    color: #FFFFFF;
    text-decoration: none;
}

div.xmlWrap {
    border: 1px solid #666;
    padding: 1px;
    text-decoration:  none;
    background-color: transparent;
    margin: 0px 0px 0px 0px;
    width: 34px;
    text-align: center;
}

div.spacer {
    clear: both;
    font-size: 1px;
    line-height: 0px;
}

/* ------------------------------------------------ */
/*                     blog                         */
/* ------------------------------------------------ */
.blog {
    font-size:12px;
}
.blog a {
    font-size:12px;
}
.entry {
    margin:0 0 1.75em;
}
.entry h3 {
    margin:1em 0 .2em;
    font:175%/1.3em Georgia,Serif;
    line-height:1.3em;
    color:#245;
}
.blog h2.date {
    padding-bottom:.3em;
    border-bottom:1px dotted #ccc;
    margin-bottom:.5em;
}
.blog h3 {
    margin:.75em 0 .5em;
    border-bottom:1px dotted #ccc;
    padding:0 0 .2em;
    font-size:140%;
    line-height:1.5em;
}
.posted {
    color:#998;
}
.blog .posted,.blog .posted a {
    font-size:85%;
    line-height:1.6em;
}
.posted-top .commentlink, .posted-top .commentlink:visited {
  margin-left:0;
  border-left-width:0;
  background-position:0 50%;
  padding-left:14px;
  }

.blog dd.posted {
    margin-top:.5em;
    border-top:1px dotted #ccd;
    padding-top:4px;
}
.posted strong {
    color:#887;
    font-weight:normal;
}
a.permalink {
    background:url("icon_pg.gif") no-repeat 0 0;
    float:left;
    display:block;
    width:9px;
    height:10px;
    margin:.3em 5px 0 0;
    text-indent:-10000px;
    border-width:0;
}
.commentlink, .commentlink:visited {
    background:url("http://www.cr-lorraine.fr/jahia/jsp/jahia/templates/e-internet/crlo_portal_templates_OP2/img/icon_comment.gif") no-repeat 8px 50%;
    border-width:0;
    padding-left:22px;
    border-left:1px solid #ccb;
    margin-left:.5em;
    color:#359;
    white-space:nowrap;
}
.posted-top .commentlink {
    margin-left:0;
    border-left-width:0;
    background-position:0 50%;
    padding-left:14px;
}
span.commentlink {
    color:#887;
}
a.commentlink:hover {
    color:#933;
    border-color:#ccb;
    text-decoration:none;
}
.commentlink img {
    margin-top:.5em;
}
a.commentlink:hover img {
    width:0;
    padding-left:48px;
}
dl.previously {
    margin-bottom:1.25em;
}
.previously dt a    {
    font:150%/1.4em Georgia,Serif;
    border-width:0;
}
.previously dd {
    margin-bottom:1em;
    margin-left:0;
}
.previously dd.summary {
    margin-bottom:0;
}
.blog .previously dd.posted {
    margin-top:.7em;
    margin-bottom:1.75em;
}

.blog a.continued {
    background:url("arrow.gif") no-repeat 100% 2px;
    text-decoration: none;
    border-width:0;
    padding-right:16px;
    color:#402808;
    border-bottom: 1px #402808 dotted;
}
.blog a.continued:hover {
    /*background-image:url("../img/arrow_on.gif");*/
    /*color: #e9e9e9;*/
    text-decoration: underline;
    border-bottom: 1px #e9e9e9 solid;
}

/* ---------------------------------------------------- */
/* 0003 D�but des modifications							*/
/* ---------------------------------------------------- */
#description {
	padding:0 0 10px 0;
	display: inline-block;
}

#description p {
	float:left;
	width:265px;
	text-align: justify;
}

#description img.leftside {
	float:left;
	display:block;
	margin:0 10px 0 0;
}

#description p {
	width:100%;
}

#description p {
	float:none;
	width:auto;
}

div#description p {
	font-size:1em!important;
}
/* ---------------------------------------------------- */
/* 0003 Fin des modifications							*/
/* ---------------------------------------------------- */


/* ------------------------------------------------ */
/*                  comments                        */
/* ------------------------------------------------ */
#comments {
    padding:10px;
}
#comments h4 {
    font:140% Verdana,Sans-serif;
    margin:0 0 1em;
}
#comments h4 span {
    font:85% Verdana,Sans-serif;
    color:#776;
}
#comments dl {
    font-size:85%;
    line-height:1.6em;
}
#comments dt {
    padding:8px 6px .5em;
    background:#eeeeee;
    font-weight:normal;
}
#comments dd {
    padding:2px 20px 8px;
    margin:0 0 1.5em;
    background:#eeeeee;
}
form table tr td {
    vertical-align: top;
}
#comments dt.alt,
#comments dd.alt {
    background:#dddddd;
}
#comments dd h3 {
    background-image: none;
}
#comments dd del:hover,
#comments dd del:active {
    color:#333;
    text-decoration:none;
}
#comments .postno {
    background:url("http://www.cr-lorraine.fr/jahia/jsp/jahia/templates/e-internet/crlo_portal_templates_OP2/img/icon_comment.gif") no-repeat 0 55%;
    border-width:0;
    padding:0 5px 0 14px;
    border-right:1px solid #ccc;
    margin-right:2px;
    font:180% Georgia,Serif;
    color:#358;
}
#comments .alt .postno {
    border-color:#bbb;
}
#comments dt.owner {
    background:#357;
    padding-bottom:1em;
    color:#cde;
}
#comments dd.owner {
    padding-top:8px;
}
#comments dt.owner .postno {
    background-image:url("http://www.cr-lorraine.fr/jahia/jsp/jahia/templates/e-internet/crlo_portal_templates_OP2/img/icon_comment.gif");
    color:#9bd;
    border-right-color:#579;
    font-weight:normal;
}
#comments dt.owner a {
    color:#eee;
    border-width:0;
    font-weight:bold;
}
#comment-notes p {
    font-size:85%;
    line-height:1.6em;
    color:#554;
}
#subcol #comment-notes p {
    font-size:100%;
}
h4#postcomment {
    padding-top:.5em;
    margin:0 0 .5em;
}
#comment-notes {
    margin-top:2em;
}
#navigation .layout0,
#navigation .layout1 {
    margin-left:10px;
}

/* ---------------------------------------------------- */
/* 0003 D�but des modifications							*/
/* ---------------------------------------------------- */
#comments .validateButtons {
	float: right;
}

#comments .deleteComments {
	float: right;
	z-index: 9999;
}

/* ---------------------------------------------------- */
/* 0003 Fin des modifications							*/
/* ---------------------------------------------------- */

.layout0 {
    clear: both;
    margin-right:5px;
    margin-bottom:10px;
    display:block;
}
.layout0 .title    {
    margin:0;
    padding:1px 5px 1px 5px;
    color:#000000;
    font-weight:bold;
    display:block;
    background: url(arrow2.gif) no-repeat 0 3px;
    padding-left:13px;
}
.layout0 .deco {
    padding-bottom:3px;
    background: url(dotted.gif) repeat-x bottom;
}

.layout0 .boxcontent    {
    display:block;
}
.layout1 {
    clear: both;
    margin-right:5px;
    margin-bottom:10px;
    display:block;
}
.layout1 .title    {
    margin:0;
    padding:5px 5px 5px 15px;
    color:#ffffff;
    font-weight:bold;
    display:block;
    background: url(box_title_orange.gif);
}
.layout1 .boxcontent    {
    padding:5px;
    display:block;
    background:#f2f2f2;
}
.splash0,
.splash1,
.splash2,
.splash3 {
    display:block;
    background-repeat: repeat-x;
    height: 160px;
}
.splash1 {
    height: 80px;
}
.splash2 {
    height:40px;
}
.splash3 {
    height:20px;
}




.boxEPcontentcolor1 {
    background-color: white;
}
.boxEPbordercolor1 {
	border-left: 1px solid #402808;
	border-right: 1px solid #402808;
	border-bottom: 1px solid #402808;
	border-top: 1px solid #402808;
}
.boxEPTitlecolor1 {
    color: #FFFFFF;
    text-align: left;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 11px;
    font-weight: bold;
    background-color: #402808;
    padding-top: 2px;
    padding-bottom: 2px;
    white-space: nowrap;
}
.boxEPLinkcolor1 {
    color: #FFFFFF;
    text-align: left;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 11px;
    font-weight: bold;
    white-space: nowrap;
}
.boxEPPaginationPos {
    color: #402808;
    text-align: left;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 11px;
    font-style: normal;
    white-space: nowrap;
}
.boxEPPaginationNav {
    color: #402808;
    text-align: right;
    font-family: Arial, Helvetica, sans-serif;
    font-size: 11px;
    font-style: normal;
    white-space: nowrap;
}
.EPMetaDataTitle {
	color: #000000;
	text-align: left;
	vertical-align: top;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	white-space: nowrap;
}
.EPMetaDataValue {
	color: #000000;
	text-align: left;
	vertical-align: top;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
}
a.sort {
    background:url(sort.gif) no-repeat 100% 4px;
    padding-right: 12px;
    font-weight: bold;
    color:#ffffff;
}

.input { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; font-style: normal; color: #000000}

.tabs_li {
    display: inline;
    list-style-type: none;
    margin:0 0 0;
    padding:0 0 0 0;
    text-indent:0;
}

.centeredBox {
    display: block;
    margin:0 auto;
    width: 95%;
}

#searchPanel {
    display: block;
    margin:0 auto;
    width: 100%;
  	clear:both;
}

.searchPageDetailFrame {
    width: 650px;
    height: 100%;
    display: block;
}

.showSearchHitDetails {
  background-color: #eeeeee;
}

.searchHit {
  clear: both;
}

.searchHitInfo {
  padding: 10px 0px 10px 0px;
}

.searchHitInfo tr td {
  font-size: 10px;
  text-align: left;
  vertical-align: top;
}

.searchHitInfo tr td {
  font-size: 10px;
  text-align: left;
  vertical-align: top;
}

.searchHitMetadataLabel {
  float: left;
}

.searchHitMetadataColon {
  float: right;
}

.searchLink {
  background-color: #eeeeee;
}

div.searchHit hr {
  clear: both;
  height: 1px;
  background: url(line_200.gif) repeat-x 100%;
  border: 0px solid;
}


.davbox ul {
    list-style: none;
    margin: 0;
    padding:0;
}
.davbox li {
    display: inline;
    margin: 0;
    padding: 0;
}
.davbox a {
    display:block;
	line-height: 18px;
}
/*
*********** JFORUM CSS ***********************
*/

.jforum INPUT {
	BORDER-TOP-WIDTH: 1px; BORDER-LEFT-WIDTH: 1px; BORDER-BOTTOM-WIDTH: 1px; BORDER-RIGHT-WIDTH: 1px
}
.jforum TEXTAREA {
	BORDER-TOP-WIDTH: 1px; BORDER-LEFT-WIDTH: 1px; BORDER-BOTTOM-WIDTH: 1px; BORDER-RIGHT-WIDTH: 1px
}
.jforum SELECT {
	BORDER-TOP-WIDTH: 1px; BORDER-LEFT-WIDTH: 1px; BORDER-BOTTOM-WIDTH: 1px; BORDER-RIGHT-WIDTH: 1px
}
.jforum INPUT {
	TEXT-INDENT: 2px
}
.jforum INPUT.button {
	BORDER-TOP-WIDTH: 1px; BORDER-LEFT-WIDTH: 1px; BORDER-BOTTOM-WIDTH: 1px; BORDER-RIGHT-WIDTH: 1px
}
.jforum .postbody {
	LINE-HEIGHT: 18px
}


.jforum FONT {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.jforum TH {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.jforum TD {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}

.jforum PRE {
	FONT-SIZE: 11px; COLOR: #444444; FONT-FAMILY: Courier, 'Courier New', sans-serif
}

.jforum P {
	FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.jforum A:link {
	COLOR: #01336b
}
.jforum A:active {
	COLOR: #01336b
}
.jforum A:visited {
	COLOR: #01336b
}
.jforum A:hover {
	COLOR: #DD6900; TEXT-DECORATION: underline
}
.jforum HR {
	BORDER-RIGHT: #dfdfdf 0px solid; BORDER-TOP: #dfdfdf 1px solid; BORDER-LEFT: #dfdfdf 0px solid; BORDER-BOTTOM: #dfdfdf 0px solid; HEIGHT: 0px
}
.jforum .bodyline {
	BORDER-RIGHT: #cccccc 1px solid; BORDER-TOP: #cccccc 1px solid; BORDER-LEFT: #cccccc 1px solid; BORDER-BOTTOM: #cccccc 1px solid; BACKGROUND-COLOR: #ffffff
}
.jforum .forumline {
	BORDER-RIGHT: #006699 2px solid; BORDER-TOP: #006699 2px solid; BORDER-LEFT: #006699 2px solid; BORDER-BOTTOM: #006699 2px solid; BACKGROUND-COLOR: #ffffff
}
.jforum TD.row1 {
	BACKGROUND-COLOR: #fafafa
}
.jforum TD.row2 {
	BACKGROUND-COLOR: #f7f7f8
}
.jforum TD.row3 {
	BACKGROUND-COLOR: #dfdfdf
}
.jforum TD.rowpic {
	BACKGROUND-IMAGE:  url(http://www.cr-lorraine.fr/jahia/jsp/jahia/templates/e-internet/crlo_portal_templates_OP2/css/images/cellpic2.jpg); BACKGROUND-REPEAT: repeat-y; BACKGROUND-COLOR: #ffffff
}
.jforum TH {
	FONT-WEIGHT: bold; FONT-SIZE: 11px; BACKGROUND-IMAGE:  url(http://www.cr-lorraine.fr/jahia/jsp/jahia/templates/e-internet/crlo_portal_templates_OP2/css/images/cellpic3.gif); COLOR: #ffa34f; HEIGHT: 25px; BACKGROUND-COLOR: #01336b
}
.jforum TD.cat {
	BORDER-RIGHT: #ffffff solid; BORDER-TOP: #ffffff solid; BACKGROUND-IMAGE:  url(http://www.cr-lorraine.fr/jahia/jsp/jahia/templates/e-internet/crlo_portal_templates_OP2/css/images/cellpic1.gif); BORDER-LEFT: #ffffff solid; BORDER-BOTTOM: #ffffff solid; HEIGHT: 28px; BACKGROUND-COLOR: #dfdfdf
}
.jforum TD.catHead {
	BORDER-RIGHT: #ffffff solid; BORDER-TOP: #ffffff solid; BACKGROUND-IMAGE:  url(http://www.cr-lorraine.fr/jahia/jsp/jahia/templates/e-internet/crlo_portal_templates_OP2/css/images/cellpic1.gif); BORDER-LEFT: #ffffff solid; BORDER-BOTTOM: #ffffff solid; HEIGHT: 28px; BACKGROUND-COLOR: #dfdfdf
}
.jforum TD.catSides {
	BORDER-RIGHT: #ffffff solid; BORDER-TOP: #ffffff solid; BACKGROUND-IMAGE:  url(http://www.cr-lorraine.fr/jahia/jsp/jahia/templates/e-internet/crlo_portal_templates_OP2/css/images/cellpic1.gif); BORDER-LEFT: #ffffff solid; BORDER-BOTTOM: #ffffff solid; HEIGHT: 28px; BACKGROUND-COLOR: #dfdfdf
}
.jforum TD.catLeft {
	BORDER-RIGHT: #ffffff solid; BORDER-TOP: #ffffff solid; BACKGROUND-IMAGE:  url(http://www.cr-lorraine.fr/jahia/jsp/jahia/templates/e-internet/crlo_portal_templates_OP2/css/images/cellpic1.gif); BORDER-LEFT: #ffffff solid; BORDER-BOTTOM: #ffffff solid; HEIGHT: 28px; BACKGROUND-COLOR: #dfdfdf
}
.jforum TD.catRight {
	BORDER-RIGHT: #ffffff solid; BORDER-TOP: #ffffff solid; BACKGROUND-IMAGE:  url(http://www.cr-lorraine.fr/jahia/jsp/jahia/templates/e-internet/crlo_portal_templates_OP2/css/images/cellpic1.gif); BORDER-LEFT: #ffffff solid; BORDER-BOTTOM: #ffffff solid; HEIGHT: 28px; BACKGROUND-COLOR: #dfdfdf
}
.jforum TD.catBottom {
	BORDER-RIGHT: #ffffff solid; BORDER-TOP: #ffffff solid; BACKGROUND-IMAGE:  url(http://www.cr-lorraine.fr/jahia/jsp/jahia/templates/e-internet/crlo_portal_templates_OP2/css/images/cellpic1.gif); BORDER-LEFT: #ffffff solid; BORDER-BOTTOM: #ffffff solid; HEIGHT: 28px; BACKGROUND-COLOR: #dfdfdf
}
.jforum TD.cat {
	BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; HEIGHT: 29px; BORDER-RIGHT-WIDTH: 0px
}
.jforum TD.catHead {
	BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; HEIGHT: 29px; BORDER-RIGHT-WIDTH: 0px
}
.jforum TD.catBottom {
	BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; HEIGHT: 29px; BORDER-RIGHT-WIDTH: 0px
}
.jforum TH.thHead {
	BORDER-RIGHT: #ffffff solid; BORDER-TOP: #ffffff solid; FONT-WEIGHT: bold; BORDER-LEFT: #ffffff solid; BORDER-BOTTOM: #ffffff solid; HEIGHT: 28px
}
.jforum TH.thSides {
	BORDER-RIGHT: #ffffff solid; BORDER-TOP: #ffffff solid; FONT-WEIGHT: bold; BORDER-LEFT: #ffffff solid; BORDER-BOTTOM: #ffffff solid; HEIGHT: 28px
}
.jforum TH.thTop {
	BORDER-RIGHT: #ffffff solid; BORDER-TOP: #ffffff solid; FONT-WEIGHT: bold; BORDER-LEFT: #ffffff solid; BORDER-BOTTOM: #ffffff solid; HEIGHT: 28px
}
.jforum TH.thLeft {
	BORDER-RIGHT: #ffffff solid; BORDER-TOP: #ffffff solid; FONT-WEIGHT: bold; BORDER-LEFT: #ffffff solid; BORDER-BOTTOM: #ffffff solid; HEIGHT: 28px
}
.jforum TH.thRight {
	BORDER-RIGHT: #ffffff solid; BORDER-TOP: #ffffff solid; FONT-WEIGHT: bold; BORDER-LEFT: #ffffff solid; BORDER-BOTTOM: #ffffff solid; HEIGHT: 28px
}
.jforum TH.thBottom {
	BORDER-RIGHT: #ffffff solid; BORDER-TOP: #ffffff solid; FONT-WEIGHT: bold; BORDER-LEFT: #ffffff solid; BORDER-BOTTOM: #ffffff solid; HEIGHT: 28px
}
.jforum TH.thCornerL {
	BORDER-RIGHT: #ffffff solid; BORDER-TOP: #ffffff solid; FONT-WEIGHT: bold; BORDER-LEFT: #ffffff solid; BORDER-BOTTOM: #ffffff solid; HEIGHT: 28px
}
.jforum TH.thCornerR {
	BORDER-RIGHT: #ffffff solid; BORDER-TOP: #ffffff solid; FONT-WEIGHT: bold; BORDER-LEFT: #ffffff solid; BORDER-BOTTOM: #ffffff solid; HEIGHT: 28px
}
.jforum TD.row3Right {
	BORDER-RIGHT: #ffffff solid; BORDER-TOP: #ffffff solid; BORDER-LEFT: #ffffff solid; BORDER-BOTTOM: #ffffff solid; BACKGROUND-COLOR: #dfdfdf
}
.jforum TD.spaceRow {
	BORDER-RIGHT: #ffffff solid; BORDER-TOP: #ffffff solid; BORDER-LEFT: #ffffff solid; BORDER-BOTTOM: #ffffff solid; BACKGROUND-COLOR: #dfdfdf
}
.jforum TH.thHead {
	BORDER-TOP-WIDTH: 1px; BORDER-LEFT-WIDTH: 1px; FONT-SIZE: 12px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 1px
}
.jforum TD.catHead {
	BORDER-TOP-WIDTH: 1px; BORDER-LEFT-WIDTH: 1px; FONT-SIZE: 12px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 1px
}
.jforum TH.thSides {
	BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 1px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 1px
}
.jforum TD.catSides {
	BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 1px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 1px
}
.jforum TD.spaceRow {
	BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 1px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 1px
}
.jforum TH.thRight {
	BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 1px
}
.jforum TD.catRight {
	BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 1px
}
.jforum TD.row3Right {
	BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 1px
}
.jforum TH.thLeft {
	BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 1px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 0px
}
.jforum TD.catLeft {
	BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 1px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 0px
}
.jforum TH.thBottom {
	BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 1px; BORDER-BOTTOM-WIDTH: 1px; BORDER-RIGHT-WIDTH: 1px
}
.jforum TD.catBottom {
	BORDER-TOP-WIDTH: 0px; BORDER-LEFT-WIDTH: 1px; BORDER-BOTTOM-WIDTH: 1px; BORDER-RIGHT-WIDTH: 1px
}
.jforum TH.thTop {
	BORDER-TOP-WIDTH: 1px; BORDER-LEFT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 0px
}
.jforum TH.thCornerL {
	BORDER-TOP-WIDTH: 1px; BORDER-LEFT-WIDTH: 1px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 0px
}
.jforum TH.thCornerR {
	BORDER-TOP-WIDTH: 1px; BORDER-LEFT-WIDTH: 0px; BORDER-BOTTOM-WIDTH: 0px; BORDER-RIGHT-WIDTH: 1px
}
.jforum .maintitle {
	FONT-WEIGHT: bold; FONT-SIZE: 22px; COLOR: #000000; LINE-HEIGHT: 120%; FONT-FAMILY: "Trebuchet MS",Verdana, Arial, Helvetica, sans-serif; TEXT-DECORATION: none
}
.jforum .gen {
	FONT-SIZE: 12px
}
.jforum .genmed {
	FONT-SIZE: 11px
}
.jforum .gensmall {
	FONT-SIZE: 10px
}
.jforum .gen {
	COLOR: #000000
}
.jforum .genmed {
	COLOR: #000000
}
.jforum .gensmall {
	COLOR: #000000
}
.jforum A.gen {
	COLOR: #01336b; TEXT-DECORATION: none
}
.jforum A.genmed {
	COLOR: #01336b; TEXT-DECORATION: none
}
.jforum A.gensmall {
	COLOR: #01336b; TEXT-DECORATION: none
}
.jforum A.gen:hover {
	COLOR: #01336b; TEXT-DECORATION: underline
}
.jforum A.genmed:hover {
	COLOR: #01336b; TEXT-DECORATION: underline
}
.jforum A.gensmall:hover {
	COLOR: #01336b; TEXT-DECORATION: underline
}
.jforum .mainmenu {
	FONT-SIZE: 11px; COLOR: #000000
}
.jforum A.mainmenu {
	COLOR: #01336b; TEXT-DECORATION: none
}
.jforum A.mainmenu:hover {
	COLOR: #01336b; TEXT-DECORATION: underline
}
.jforum .cattitle {
	FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: #01336b; LETTER-SPACING: 1px
}
.jforum A.cattitle {
	COLOR: #01336b; TEXT-DECORATION: none
}
.jforum A.cattitle:hover {
	TEXT-DECORATION: underline
}
.jforum .forumlink {
	FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: #01336b
}
.jforum A.forumlink {
	COLOR: #01336b; TEXT-DECORATION: none
}
.jforum A.forumlink:hover {
	COLOR: #01336b; TEXT-DECORATION: underline
}
.jforum .nav {
	FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #000000
}
.jforum A.nav {
	COLOR: #01336b; TEXT-DECORATION: none
}
.jforum A.nav:hover {
	TEXT-DECORATION: underline
}
.jforum .topictitle {
	FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #000000
}
.jforum H1 {
	FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #000000
}
.jforum H2 {
	FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #000000
}
.jforum A.topictitle:link {
	COLOR: #01336b; TEXT-DECORATION: none
}
.jforum A.topictitle:visited {
	COLOR: #01336b; TEXT-DECORATION: none
}
.jforum A.topictitle:hover {
	COLOR: #01336b; TEXT-DECORATION: underline
}
.jforum .name {
	FONT-SIZE: 11px; COLOR: #000000
}
.jforum .postdetails {
	FONT-SIZE: 10px; COLOR: #000000
}
.jforum .postbody {
	FONT-SIZE: 12px; LINE-HEIGHT: 18px
}
.jforum A.postlink:link {
	COLOR: #01336b; TEXT-DECORATION: none
}
.jforum A.postlink:visited {
	COLOR: #01336b; TEXT-DECORATION: none
}
.jforum A.postlink:hover {
	COLOR: #01336b; TEXT-DECORATION: underline
}
.jforum .code {
	FONT-SIZE: 11px; COLOR: #444444; FONT-FAMILY: Courier, 'Courier New', sans-serif
}
.jforum .quote {
	FONT-SIZE: 11px; COLOR: #444444; LINE-HEIGHT: 125%; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}
.jforum .copyright {
	FONT-SIZE: 10px; COLOR: #444444; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; LETTER-SPACING: -1px
}
.jforum A.copyright {
	COLOR: #444444; TEXT-DECORATION: none
}
.jforum A.copyright:hover {
	COLOR: #000000; TEXT-DECORATION: underline
}
.jforum INPUT {
	BORDER-LEFT-COLOR: #000000; BORDER-BOTTOM-COLOR: #000000; FONT: 11px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000; BORDER-TOP-COLOR: #000000; BORDER-RIGHT-COLOR: #000000
}
.jforum TEXTAREA {
	BORDER-LEFT-COLOR: #000000; BORDER-BOTTOM-COLOR: #000000; FONT: 11px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000; BORDER-TOP-COLOR: #000000; BORDER-RIGHT-COLOR: #000000
}
.jforum SELECT {
	BORDER-LEFT-COLOR: #000000; BORDER-BOTTOM-COLOR: #000000; FONT: 11px Verdana, Arial, Helvetica, sans-serif; COLOR: #000000; BORDER-TOP-COLOR: #000000; BORDER-RIGHT-COLOR: #000000
}
.jforum INPUT.post {
	BACKGROUND-COLOR: #ffffff
}
.jforum TEXTAREA.post {
	BACKGROUND-COLOR: #ffffff
}
.jforum SELECT {
	BACKGROUND-COLOR: #ffffff
}
.jforum INPUT {
	TEXT-INDENT: 2px
}
.jforum INPUT.button {
	FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif; BACKGROUND-COLOR: #fafafa
}
.jforum INPUT.mainoption {
	FONT-WEIGHT: bold; BACKGROUND-COLOR: #fafafa
}
.jforum INPUT.liteoption {
	FONT-WEIGHT: normal; BACKGROUND-COLOR: #fafafa
}
.jforum .helpline {
	BORDER-TOP-STYLE: none; BORDER-RIGHT-STYLE: none; BORDER-LEFT-STYLE: none; BACKGROUND-COLOR: #f7f7f8; BORDER-BOTTOM-STYLE: none
}

.jforum .moderatortitle {
	FONT-WEIGHT: bold; FONT-SIZE: 12px; COLOR: #ffa34f
}

.jforum .moderator {
	FONT-SIZE: 12px; color: #006600
}

.jforum .admin {
	FONT-SIZE: 12px; color: #FFA34F
}

/*
****************** ADV SEARCH CSS ******************
*/

.input { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 9px; font-style: normal; color: #000000}

div.searchResultListing {
    width: 100%;
    overflow: auto;
    overflow-y:hidden;
    font-size:10px;
    font-size:11px;
}

.tabs_li {
    display: inline;
    list-style-type: none;
    margin:0 0 0;
    padding:0 0 0 0;
    text-indent:0;
}

#searchPanel {
    width: 100%;
    float:left;
  	clear:both;
}

#searchPanel * {
    font-size: 11px;
}

table.searchResultListing {
	/*border: 1px solid #666;*/
	width: 80%;
	margin: 10px 0 10px 0 !important;
	font-size:11px;
}

table.searchResultListing th, table.searchResultListing td {
	padding: 2px 10px 2px 4px !important;
	text-align: left;
	vertical-align: top;
}

table.searchResultListing thead tr {
	background-color: #faf1c3;
}

table.searchResultListing th.sorted {
	background-color: #f6e896;
	font-weight:bold;
}

table.searchResultListing th a,table.searchResultListing th a:visited {
	color: black;
}

table.searchResultListing th a:hover {
	text-decoration: underline;
	color: black;
}

table.searchResultListing th.sorted a,table.searchResultListing th.sortable a {
	background-position: right top;
	display: block;
	width: 100%;
}

table.searchResultListing th.sortable a {
  padding-right: 5px;
	background-image: url(arrow_off.png);
	background-repeat:no-repeat;
}

table.searchResultListing th.order1 a {
  padding-right: 5px;
	background-image: url(arrow_down.png);
	background-repeat:no-repeat;
}

table.searchResultListing th.order2 a {
  padding-right: 5px;
	background-image: url(arrow_up.png);
	background-repeat:no-repeat;
}

table.searchResultListing tr.odd {
	background-color: #ecf7ce;
}

table.searchResultListing tr.tableRowEven,tr.even {
	/*background-color: red;*/
}


table.searchResultListing {
  padding:0px;
  width: 100%;
  margin-left: -2px;
  margin-right: -2px;
}

table.searchResultListing thead tr {
	background-color: #faf1c3;
}
table.searchResultListing tr.even {
	background-color: #ffffff;
}

div.searchResultListing span.pagelinks a {
  display: inline;
}

/*
************* ACTIONS MENUS CSS ********************
*/

#menu div.menu {
    background-color: #d0d0d0 !important;
}
div.menu a {
    font-weight: normal !important;
}
.nostyle,
.nostyle a {
    background:transparent !important;
    font-weight: normal !important;
}
div.menu,
div.menu a.menuItem {
  font-family: "MS Sans Serif", Arial, sans-serif!important;
  font-size: 8pt!important;
  font-style: normal!important;
  font-weight: normal !important;
  color: #000000!important;
  white-space: nowrap!important;
}
#navigation span a {
    padding:0 !important;
}
div.menu {
  background-color: #d0d0d0!important;
  border: 2px solid!important;
  border-color: #f0f0f0 #909090 #909090 #f0f0f0!important;
  left: 0px;
  padding: 0px 1px 1px 0px !important;
  position: absolute;
  top: 0px;
  visibility: hidden;
  z-index: 101;
}

div.menu a.menuItem {
  color: #000000 !important;
  cursor: default!important;
  display: block !important;
  clear:both!important;
  padding: 3px 1em !important;
  text-decoration: none!important;
  white-space: nowrap!important;
  background-image:none !important;
  width:120px!important;
  font-weight:normal !important;
  font-family: Arial, Helvetica, sans-serif!important;
}

div.menu a.menuItem:hover, div.menu a.menuItemHighlight {
  background-color: #000080 !important;
  color: #ffffff !important;
  text-decoration:none !important;
}

div.menu a.menuItem span.menuItemText {}

div.menu a.menuItem span.menuItemArrow {
  margin-right: -.75em !important;
}

div.menu div.menuItemSep {
  border-top: 1px solid #909090!important;
  border-bottom: 1px solid #f0f0f0!important;
  margin: 4px 2px!important;
}

fieldset {
    padding:0;
    margin: 0;
    clear: both;
}


/*
**************************  PORTLETS **************************
*/




ul.portletModes {
	background: url(tabBorder.gif) repeat-x bottom;
	color: #515C6A;
	float: right;
	list-style: none;
	margin: 0px;
	padding: 0px;
	padding-left: 2px;
}
ul.portletModes a {
	background: url(tabRight.gif) no-repeat right top;
	border-bottom: 1px solid #515C6A;
	float: left;
	margin-right: 2px;
	text-decoration: none;
}
ul.portletModes a:hover {
	background-position: 100% -26px;
}
ul.portletModes a:hover span {
	background-position: 0% -26px;
}
ul.portletModes li {
	display: inline!important;
	margin: 0px!important;
	padding: 0px!important;
}
ul.portletModes li.current a {
	background-position: 100% -26px;
	border-width: 0px;
}
ul.portletModes li.current span {
	background-position: 0% -26px;
	padding-bottom: 1px;
}
ul.portletModes span {
	background: url(tabLeft.gif) no-repeat left top;
	display: block;
	float: left;
	padding: 1px 6px 1px 22px;
	white-space: nowrap;
	color: #515C6A;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9px;
}
/* Commented Backslash Hack hides rule from IE5-Mac \*/
ul.portletModes span {
	float: none;
}
/* End IE5-Mac hack */

ul.windowStates {
	background: url(tabBorder.gif) repeat-x bottom;
	color: #515C6A;
	float: right;
	list-style: none;
	margin: 0px;
	padding: 0px;
}

ul.windowStates a {
	background: url(http://www.cr-lorraine.fr/jahia/jsp/jahia/templates/e-internet/crlo_portal_templates_OP2/css/images/windowStates.gif) no-repeat left top;
	border-bottom: 1px solid #515C6A;
	float: left;
	height: 14px;
	margin-right: 2px;
	text-decoration: none;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9px;
	width:17px;
	z-index: 1;
}

ul.windowStates a span {
    display:none;
}

ul.windowStates a.maximized {
	background-image: url(windowStates_maximized.gif);
}

ul.windowStates a.minimized {
	background-image: url(windowStates_minimized.gif);
}

ul.windowStates a.normal {
	background-image: url(windowStates_normal.gif);
}

ul.windowStates a:hover {
	background-position: 0% -15px;
}
ul.windowStates li {
	display: inline;
	margin: 0px;
	padding: 0px;
}

ul.windowStates li.current a {
	display: none;
}
/* Commented Backslash Hack hides rule from IE5-Mac \*/
ul.windowStates div {
	float: none;
}
/* End IE5-Mac hack */

/* #### PORTAL-PORTLET CLASS REFERENCES #### */


.Portal-Portlet-Title {
   background: transparent;
   margin-left: auto;
   margin-right: auto;
   font-family: Verdana, Arial, Helvetica;
   font-weight: bold;
   font-size: 180%;
   background-color: #CCCC99;
   color: #000000;
   text-align: center;
   padding: 4px;
   border: 1px solid #000000;
   }


.Portal-Portlet-Information {
   width: 400px;
   margin-left: auto;
   margin-right: auto;
   margin-bottom: 1em;
   margin-top: .5em;
   font-family: Verdana, Arial, Helvetica;
   font-style: oblique;
   font-size: 120%;
   font-weight: bold;
   color: #000000;
   text-align: center;
   padding: 4px;
   }


.Portal-Portlet-Group {
   width: 400px;
   margin-left: auto;
   margin-right: auto;
   margin-top: 1em;
   background-color: transparent;
   border: 2px solid #000000;
   text-align: center;
   }


.Portal-Portlet-Legend { }


.Portal-Portlet-Footer {
   font-family: Verdana, Arial, Helvetica;
   font-size: 90%;
   color: #000000;
   }


/* ##### PORTAL_PORTLET_GROUP CLASS REFERENCES ##### */
.Portal-Portlet-Group-Title {
   font-family: Verdana, Arial, Helvetica;
   font-weight: bold;
   font-size: 150%;
   background-color: #CCCC99;
   border-bottom: 2px solid #000000;
   color: #000000;
   padding: 4px;
   }


.Portal-Portlet-Group-Information { }


.Portal-Portlet-Group-Content {
   font-family: Verdana, Arial, Helvetica;
   font-size: 110%;
   color: #000000;
   margin-top: 1em;
   text-decoration: none;
   }


.Portal-Portlet-Group-Legend { }


.Portal-Portlet-Group-Footer {
   margin-top: 2em;
   text-align: center;
   }


/* #### PORTAL-PORTLET-GROUP-SUBGROUP CLASS REFERENCES #### */
.Portal-Portlet-Group-SubGroup {
   font-family: Verdana, Arial, Helvetica;
   font-size: 100%;
   font-weight: bold;
   background: #87ACD6;
   color: #000000;
   padding: 4px;
   margin: 0 1em;
   }


/* #### PORTAL-PORTLET-GROUP-CONTENT CLASS REFERENCES #### */
/* Used in products like JMV for displaying a list of areas */
.Portal-Portlet-Group-Content-ListItem {
   font-family: Verdana, Arial, Helvetica;
   font-size: 100%;
   margin-top: 3px;
   font-weight: bold;
   color: #000000;
   padding: 4px;
   text-align: left;
   }


.Portal-Portlet-Group-Content-ListItem a:link
{
    text-decoration: underline;
    color: #035AB4;
}


.Portal-Portlet-Group-Content-ListItem a:visited
{
    text-decoration: underline;
    color: #8403AF;
}


.Portal-Portlet-Group-Content-ListItem a:hover
{
    text-decoration: underline;
    color: #D9011A;
}

.graph {
    position: relative; /* IE is dumb */
    width: 200px;
    border: 1px solid #7ca648;
    padding: 1px;
    color: #333;
}
.graph .bar {
    display: block;
    position: relative;
    background: #7ca648;
    text-align: center;
    /*color: #333;*/
    color: #fff;
    height: 1.5em;
    line-height: 1.5em;
}
.graph .bar span { position: absolute; left: 1em; }

/* ####      PORTAL-PORTLET-GROUP-MATRIX      #### */
/* # This Class reference group handles formatting */
/* # objects-products-etc within a matrix.         */

.Portal-Portlet-Group-Content-Matrix {
   width: 90%;
   margin-right: auto;
   margin-left: auto;
   text-align: center;
   border: solid #6495ED; /* cornflowerblue */
   border-width: 2px;
   }


.Portal-Portlet-Group-Content-Matrix-Image {
   border: solid #6495ED; /* cornflowerblue */
   border-width: 2px;
   background-color: #C8C8C8; /* a light-medium gray */
   }


.Portal-Portlet-Group-Content-Matrix-Loop {
   border: solid #6495ED; /* cornflowerblue */
   border-width: 2px;
   }


.Portal-Portlet-Group-Content-Matrix-Loopcontrol {
   background-color: #C8C8C8; /* a light-medium gray */
   color: white;
   }

.inputError { color:red; }

/* ####      COMPARE HIGHLIGHTING STYLE     #### */

   
span.compareAddedDifference,
div#navigation span.compareAddedDifference,
div#topmenu span.compareAddedDifference {
  display:inline !important;
  margin: 0px 0px !important;
  padding: 0px 0px !important;
  background: none !important;
  background-color:aqua !important;
  background-position: 0% 0% !important;
  font-color:black !important;
  border-bottom:none !important;
}

span.compareChangedDifference,
div#navigation span.compareChangedDifference,
div#topmenu span.compareChangedDifference {
  display:inline !important;
  margin: 0px 0px !important;
  padding: 0px 0px !important;
  background: none !important;
  background-color:lime !important;
  background-position: 0% 0% !important;
  font-color:black !important;
  border-bottom:none !important;
} 

span.compareDeletedDifference,
div#navigation a span.compareDeletedDifference,
div#topmenu span.compareDeletedDifference {
  display:inline !important;
  margin: 0px 0px !important;
  padding: 0px 0px !important;
  background:none !important;
  background-color:red !important; 
  background-position: 0% 0% !important;
  text-decoration:line-through !important; 
  font-color:black !important;
  border-bottom:none !important;
}

/* ---------------------------------------------------- */
/* 0024 - D�but des modifications						*/
/* ---------------------------------------------------- */

/*** Description du vade-mecum ***/
.vdDesc {
	display: block;
	clear: both;
	padding: 15px 5px 15px 0;
}

/*** Zone de t�l�chargement de documents ***/
.vdDLZone_Title {
	background-color:#9660AD !important;
	color:white;
	font-size:1.2em;
	font-weight:bold;
	padding:5px !important;
}

.vdDLZone_DocList_SingleDocBlock {
	margin-bottom: 3px !important;
}

.docLink {
	font-weight:bold;
}

.docLink a {
	color:#F36F21 !important;
}

.docInfos {
	padding-left:15px !important;
	font-size: .9em;
}

/*** Zone de recherche */
.vdSearch { /* Bloc entier */
	border:1px solid black;
	display:block;
	margin:5px 5px 5px 0;
	padding:10px;
}

.vdSearchTitle {
	font-size: 1.3em;
	font-weight: bold;
	margin-bottom:10px;
}

.vdSearchSelect {
	width: 300px;
}

.vdSearchInput {
	width:295px;
}

.vdSearchButton {
	background-color:black;
	color:white;
	font-weight:bold;
	text-transform:uppercase;
	float:left;
	margin-right: 33px;
}

/***** Ecran d'�dition des fiches d'aide *****/
.vdFichesAidesTitre {
	background-color:#9660AD !important;
	text-transform: uppercase;
	color:white;
	font-size:1.2em;
	font-weight:bold;
	padding:5px !important;
	margin-top:15px;
}

.vdFichesAides_ImageDomaine {
	margin-bottom:-2px;
	margin-left:2px;
	margin-top:20px;
}

.vdFichesAides_Intitule {
	display:block;
	border-bottom:2px solid black;
	padding-bottom: 2px;
	margin: 8px 0; /* margin-top necessaire pour IE 8 */
	width:98%;
}

.vdFichesAides_Intitule_NomDomaine {
	float:left;
	font-size:1.1em;
	font-weight:bolder;
	padding-left: 3px;
	text-transform:uppercase;
	margin-top: 3px;
}

.vdFichesAides_Intitule_Pagination {
	float:right;
	margin-bottom:1px;
}

.vdFichesAides_Intitule_Pagination a,
.vdFichesAides_Intitule_Pagination a:hover,
.vdFichesAides_Intitule_Pagination a:visited {
	color: black;
}

.vdFichesAides_TableFiches_Header {
	background-color:#FFAF09;
	font-weight: bold;
	clear:both;
	color:white;
	width:98%;
}

.vdFichesAides_TableFiches_Header a {
	color: white;
	text-decoration: underline;
}

.vdFichesAides_TableFiches_Row {
	float: left; /* sinon IE 7 ne rend pas bien */
	width:98%;
	margin-top:1px;
	margin-bottom:1px;
	padding-top: 1px;
}

.vdFichesAides_TableFiches_Row a,
.vdFichesAides_TableFiches_Row a:hover,
.vdFichesAides_TableFiches_Row a:visited {
	color:darkblue;
}

.vdFichesAides_TableFiches_Deleter {
	font-weight: bolder;
	margin-top: 3px;
}

.vdFichesAides_TableFiches_Deleter a, 
.vdFichesAides_TableFiches_Deleter a:hover {
	color: black;
	text-decoration: none;
}

.vdFichesAides_Empty {
	text-decoration: italic;
}

.vdErrorMessage {
	font-weight: bolder;
	color: red;
}

/* R�sultats de recherche */
.vdSearchRes {
	border: 1px solid black; 
	margin: 0pt 5px 15px 0pt;
}

.vdSearchRes_GreyFrame {
	background-color: #E0E0E0;
	border: 1px solid black;
	margin: 10px 5px;
}

.vdSearchRes_TotalRes {
	font-weight: bolder;
	font-size: 1.5em;
	padding: 5px;
}

.vdSearchRes_TotalResList {
	font-weight:bolder;
	padding: 3px;
	margin-left:10px;
}

.vdSearchRes_TotalResList a,
.vdSearchRes_TotalResList a:hover,
.vdSearchRes_TotalResList a:visited {
	color: black;
}

.vdSearchRes_DomainName {
	font-size:1.3em;
	font-weight:bolder;
	margin: 20px 0 10px;
	text-align: center;
	text-transform:uppercase;
}

.vdSearchRes_Fiche {
}

.vdSearchRes_TitreFiche {
	border:1px solid black;
	padding:10px 5px;
	clear: both;
	background-color: #F8F8F8;
}

.vdSearchRes_TitreGras {
	font-weight:bolder;
}

.vdSearchRes_FicheMeta {
	margin-left:25px;
	margin-top:5px;
}

.vdSearchRes_Link {
	float:right;
	margin-right:10px;
	margin-top: 5px;
}

.vdSearchRes_Pagination {
	padding: 10px; 
	text-align: center; 
	font-size: 1.2em; 
	font-weight: bolder;
}

.vdSearchRes_Correction {
	margin:10px;
	font-size: 1.1em;
}

.vdSearchRes_DomainImage {
	margin-left: 5px; 
	margin-top: 5px;
}

.vdSearchRes_Affinage {
	background-color: #F5F5F5;
	border: 1px solid black;
	margin: 10px 5px;
}
.vdSearchRes_Affinage_Titre {
	font-weight:bold;
	margin:5px;
}

.hidden { display: none; }
.unhidden { display: block; }

/* ---------------------------------------------------- */
/* 0024 - Fin des modifications							*/
/* ---------------------------------------------------- */