/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - \
|		
|		Copyright (c) 2007 conseil r�gional de Lorraine
|		Design + HTML/CSS/DOM JavaScript : Smart Agence
|		http://www.smartagence.com/
|		
\ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


/* ______________________[ 01 | Gestion de la taille du texte d�un article ]________________________ */

function SmartSize() {
	var args=SmartSize.arguments;
	if (document.getElementById&&document.getElementById("Tplus")&&document.getElementById("Tmoins")) {
		var cibleplus=document.getElementById("Tplus");
		var ciblemoins=document.getElementById("Tmoins");
		cibleplus.onclick=function() {
			for (n=0;n<args.length;n++) {
				if (document.getElementById(args[n])) {
					var cibletxt=document.getElementById(args[n]);
					var sizestr=cibletxt.style.fontSize.substring(0,cibletxt.style.fontSize.length-2);
					var sizeinter=cibletxt.style.lineHeight.substring(0,cibletxt.style.lineHeight.length-2);
					sizestr=sizestr!=""?parseInt(sizestr):12;
					cibletxt.style.fontSize=sizestr+1+"px";
				}
			}
		}
		ciblemoins.onclick=function() {
			for (n=0;n<args.length;n++) {
				if (document.getElementById(args[n])) {
					var cibletxt=document.getElementById(args[n]);
					var sizestr=cibletxt.style.fontSize.substring(0,cibletxt.style.fontSize.length-2);
					var sizeinter=cibletxt.style.lineHeight.substring(0,cibletxt.style.lineHeight.length-2);
					sizestr=sizestr!=""?parseInt(sizestr):12;
					cibletxt.style.fontSize=sizestr-1+"px";
				}
			}
		}
	}
}


/* ______________________[ 02 | Lancement d�une impression pour les navigateurs compatibles ]________________________ */

function DirectPrint() {
	if (window.print) self.print();
}


/* ______________________[ 03 | Miscellaneous ]________________________ */

function OpenPopup(url,nom,option) {
	window.open(url,nom,option);
}


/* ______________________[ 04 | Ajout de la page courante aux favoris de Microsoft Internet Explorer ]________________________ */

/* Mettre la page courante en favoris � MSIE and Firefox */
function AddBookmark(btn) {
	if (document.getElementById&&document.getElementById(btn)) {
		var el=document.getElementById(btn);
		var operaStr="� Ctrl + T � pour ajouter aux favoris.";
		if (document.all&&navigator.userAgent.indexOf("Opera")==-1) {
			el.onclick=function() {
				window.external.AddFavorite(document.location,document.title);
			}
		} else if (window.sidebar) {
			el.onclick=function () {
				window.sidebar.addPanel(document.title,document.location.href,"");
			}
		} else if (navigator.userAgent.indexOf("Opera")!=-1) {
			el.title=operaStr;
			window.status=operaStr;
		}
	}
}


/* ______________________[ 05 | Plan du site ]________________________ */

/*
aqtree3clickable.js

Converts an unordered list to an explorer-style tree, with clickable
icons

To make this work, simply add one line to your HTML:
<script type="text/javascript" src="aqtree3clickable.js"></script>

and then make the top UL of your nested unordered list of class
"aqtree3clickable".

That's it. No registration function, nothing.

http://www.kryogenix.org/code/browser/aqlists/

Stuart Langridge, November 2002
sil@kryogenix.org

Inspired by Aaron's labels.js (http://youngpup.net/demos/labels/) and Dave Lindquist's menuDropDown.js (http://www.gazingus.org/dhtml/?id=109)

*/

function makeTreesC() {
     // We don't actually need createElement, but we do
    // need good DOM support, so this is a good check.
    if (!document.createElement) return;
    msie5 = navigator.userAgent.indexOf('MSIE 5.0'); 
    uls = document.getElementsByTagName("ul");
    if (msie5 == -1){
    for (uli=0;uli<uls.length;uli++) {
        ul = uls[uli];
        if (ul.nodeName == "UL" && ul.className == "treeview") {
            processULELC(ul);
        }
	   }
   }  
}

function processULELC(ul) {
    if (!ul.childNodes || ul.childNodes.length == 0) return;
    // Iterate LIs
    for (var itemi=0;itemi<ul.childNodes.length;itemi++) {
        var item = ul.childNodes[itemi];
        if (item.nodeName == "LI") {
            // Iterate things in this LI
            var a;
            var subul;
	    subul = "";
            for (var sitemi=0;sitemi<item.childNodes.length;sitemi++) {
                var sitem = item.childNodes[sitemi];
                switch (sitem.nodeName) {
                    case "A": a = sitem; break;
                    case "UL": subul = sitem; 
                               processULELC(subul);
                               break;
                }
            }
            if (subul) {
                associateELC(a,subul);
            } else {
                a.parentNode.className = "aq3bullet";
            }
        }
    } 
}

function associateELC(a,ul) {
	if (a.parentNode.className.indexOf('aq3open') == -1)
	a.parentNode.className = 'aq3closed';
	a.onclick = function () {
		this.parentNode.className = (this.parentNode.className=='aq3open') ? "aq3closed" : "aq3open";
		return false;
	}
}

	
/* Utility functions */

function addEvent(obj, evType, fn){
  /* adds an eventListener for browsers which support it
     Written by Scott Andrew: nice one, Scott */
  if (obj.addEventListener){
    obj.addEventListener(evType, fn, false);
    return true;
  } else if (obj.attachEvent){
	var r = obj.attachEvent("on"+evType, fn);
    return r;
  } else {
	return false;
  }
}

/* ______________________[ 06 | Lancement des scripts ]________________________ */

window.onload=function() {
	AddBookmark("Favoris");
	makeTreesC();
}

function openFicheElu(url){
	window.open(url,'','top=10,left=10,location=no,width=700,height=550');	
	return false;
}

