	var taille_police_defaut;
	var max_click_grand;
	/* Initialisation de la taille par d�faut */ 
	var taille_police = taille_police_defaut;
	
	/* lire le champ d'un cookie */
	function readFontSizeCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		 return null;
	}
	
	/* �crire le champ d'un cookie */
	function createFontSizeCookie(name,value,days) {
		if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			var expires = "; expires="+date.toGMTString();	
		}
		else expires = "";
		document.cookie = name+"="+value+expires+"; path=/";
	}
	
	/* mise � jour de la taille de police pour la page */
	function updateFontSize(){		
		body = document.getElementsByTagName('body')[0];		
		body.style.fontSize = taille_police + 'px';
	}	
	
	/* R�cup�re la taille de police en cours au chargement de la page */
	function setPageFontSize(){
		var taille_lue =  readFontSizeCookie("tailpol");
		if(taille_lue != null) taille_police = taille_lue;
		else taille_police = taille_police_defaut;
		
		updateFontSize();
	}
	
	/* met � jour la taille de la police en cours lorsque la page est quitt�e */
	function getPageFontSize(){
		createFontSizeCookie("tailpol",taille_police,10);
	}

	/* Augmente ou diminue la taille de la police de l'�l�ment BODY */ 
	function taillePolice(p) {
		if(p) {
			taille_police++;
			if(taille_police > (taille_police_defaut + max_click_grand)) taille_police--;
		} else {
			taille_police--;
		}
		body = document.getElementsByTagName('body')[0];
		body.style.fontSize = taille_police + 'px'; 
		getPageFontSize();
	}
	//window.onload = setPageFontSize;
	//window.onunload = getPageFontSize;

	/* R�initialisation de la taille de la police */ function init_police() {
	taille_police = taille_police_defaut;
	body = document.getElementsByTagName('body')[0];
	body.style.fontSize = taille_police + 'px'; }
