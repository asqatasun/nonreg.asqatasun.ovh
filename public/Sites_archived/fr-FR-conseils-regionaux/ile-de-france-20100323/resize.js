function initdiapo () {
	var image = document.getElementById('diapo');
	if (!image) return;
	var bloc = document.getElementById('photo');
	if (!bloc) return;
	image.style.marginTop = parseInt((bloc.offsetHeight - image.height) / 2) + 'px';
}
function montrelegende () {
	var bloc = document.getElementById('legende');
	if (!bloc) return;
	bloc.style.display = (bloc.style.display == 'block') ? 'none' : 'block';
}

function favori(phrase,lien) {
   if (window.sidebar) {
   	window.sidebar.addPanel(phrase, lien,"");
   } else if( document.all ) {
   	window.external.AddFavorite(lien, phrase);
   } else {
   	return true;
   }
}
function rollicone(idimg, srcimg) {
	var image = document.getElementById(idimg);
	if (!image) return;
	image.src = 'fileadmin/templates/images/icones/'+srcimg;
}
function divequal (div1, div2) {
	var colg;
	var colcont;
	colg = document.getElementById(div1);
	if (!colg) return;
	colcont = document.getElementById(div2);
	if (!colcont) return;
	if (colg.offsetHeight > colcont.offsetHeight) {
		colcont.style.height = colg.offsetHeight + 'px';
	}
	else {
		colg.style.height = colcont.offsetHeight + 'px';
	}
}
function lienevenement() {
	var obj = document.getElementById('evenement');
	if (!obj) return;
	var image = obj.getElementsByTagName("img");
	if (!image[0]) return;
	var lien = obj.getElementsByTagName("h3");
	if (!lien[0]) return;
	lien[0].style.position = 'relative';
	lien[0].style.top = (image[0].offsetHeight - lien[0].offsetHeight - 20) + 'px';
}
function lien5 () {
	var une5 = document.getElementById('une5');
	var uned5 = document.getElementById('uned5');
	if (!une5 || !uned5) return;
	var div1 = une5.getElementsByTagName("div");
	var div2 = uned5.getElementsByTagName("div");
	var h = 0;
	if (une5.offsetHeight > h) h = une5.offsetHeight;
	if (uned5.offsetHeight > h) h = uned5.offsetHeight;
	//h -= 15;
	div1[0].style.height = (h - 15) + 'px';
	div2[0].style.height = (h - 15) + 'px';
	uned5.style.height = (h + 15) + 'px';
	une5.style.height = (h + 15) + 'px';
}

function tableletter (idtable) {
	var table = document.getElementById(idtable);
	var h = 0;
	var i = 0;
	var j = 0;
	if (!table) return;
	var tr = table.getElementsByTagName("tr");
	if (!tr) return;
	var td;
	for (i=0; i<tr.length;i++) {
		td = tr[i].getElementsByTagName("td");
		if (!td) return;
		h = 0;
		for (j=0; j<td.length;j++) {
			if (h < td[j].getElementsByTagName("div")[1].offsetHeight) h = td[j].getElementsByTagName("div")[1].offsetHeight;
		}
		for (j=0; j<td.length;j++) {
			td[j].getElementsByTagName("div")[1].style.height = h + 'px';
		}
	}
}

function lienstablebas (idtable) {
	var table = document.getElementById(idtable);
	var h = 0;
	var i = 0;
	var j = 0;
	if (!table) return;
	var tr = table.getElementsByTagName("tr");
	if (!tr) return;
	var td;
	for (i=0; i<tr.length;i++) {
		td = tr[i].getElementsByTagName("td");
		if (!td) return;
		h = 0;
		for (j=0; j<td.length;j++) {
			if (h < td[j].getElementsByTagName("div")[0].offsetHeight) h = td[j].getElementsByTagName("div")[0].offsetHeight;
		}
		for (j=0; j<td.length;j++) {
			td[j].getElementsByTagName("div")[0].style.height = h + 'px';
		}
	}
}
function navbasse (navig,conteneur,delta) {
	var colnav;
	var colcont;
	colnav = document.getElementById(navig);
	colcont = document.getElementById(conteneur);
	if (!colcont || !colnav) return;
	var w = colcont.offsetWidth;
	//alert(w);
	colnav.style.width = (w - delta) + 'px';
}
function posnav (obNav, obRef, delta) {
	var ref = document.getElementById(obRef);
	var nav = document.getElementById(obNav);
	if (!ref || !nav) return;
	nav.style.top = (nav.offsetHeight - delta) + 'px';
}
function equne () {
	var col;
	var lien;
	var h = 0;
	var par;
	for (var i=1;i<=3;i++) {
		col = document.getElementById("alun"+i);
		if (!col) return;
		if (col.offsetHeight > h) h = col.offsetHeight;
	}
	for (i=1;i<=3;i++) {
		col = document.getElementById("alun"+i);
		col.style.height = h + 'px';
	}
	
	h = 0;
	for (i=1;i<=3;i++) {
		col = document.getElementById("albs"+i);
		if (!col) return;
		if (col.offsetHeight > h) h = col.offsetHeight;
	}
	for (i=1;i<=3;i++) {
		col = document.getElementById("albs"+i);
		col.style.height = h + 'px';
	}

	eqserie ("pun", 3);
}
function eqserie (base, nb) {
	var h = 0;
	for (i=1;i<=nb;i++) {
		col = document.getElementById(base+i);
		if (!col) return;
		if (col.offsetHeight > h) h = col.offsetHeight;
	}
	for (i=1;i<=nb;i++) {
		col = document.getElementById(base+i);
		col.style.height = h + 'px';
	}
}
function posecontact() {
	var cont;
	var cpos;
	cont = document.getElementById('contacts');
	if (!cont) return;
	cpos = document.getElementById('menu-g');
	if (!cpos) return;
	var ht = cpos.offsetHeight;
	var hc = cont.offsetHeight;
	cont.style.top = (ht - Math.floor(hc * 1.3)) + 'px';
}

function changeClassTd(id) {
	if(document.getElementById(id) && document.getElementById(id).innerHTML == '') {
		document.getElementById(id).style.border='none';
	}	
}