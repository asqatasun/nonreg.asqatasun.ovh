/*******************************************************
 * VMaxFormValidator Lib  v1.02                        *
 * (C)Virtual_Max  ( http://come.to/vmax )             *
 * This library and it's derivatives  is  free for use * 
 * for noncommercial pupouses only, until this notice  *
 * remains unchanged and  present in all copies and    *
 * derivatives.                                        *
 *                                                     *
 * last revision: 28/03/2002                           *
 *******************************************************/


function rule(required, expr, alerttext) {
   this.expr=expr;
   this.required=required;
   this.alerttext=alerttext 
}

function checkFormElement(el,rule) {
   var val;
   if(el.type=='hidden'||el.type=='text'||el.type=='password'||el.type=='textarea') {
      val=el.value; 
      if(rule.required==false && (val==null || val=='')) return true;
      if(val==null || val=='')    return false;
      return val.match(rule.expr);
   }
   else if(el.type=='checkbox') {
      if(rule.required==true) {
         return el.checked==rule.expr;
      } 
      else return true;
   }
   else if(el.type=='select-one') {
      if(rule.required==true) {
        var inx = el.selectedIndex
        if(inx<0)  return false;
        if(el.options[inx].value) return true;
        return false; 
      }  
      else return true;
   }
   else if(el.type=='select-multiple') {
      if(rule.required==true) {
        opts = el.options;
        for(var i=0;i<opts.length;i++) {
           if(opts[i].selected && opts[i].value) return true;
        }
        return false;
      }  
      else return true;
   }
   else alert("input type "+el.type.toUpperCase() +" not supported by this library")
   return true;
}



function checkForm(f,rules) {
   
   for(var i=0;i<f.elements.length;i++) {
      var el=f.elements[i];
      var rule=rules[el.name];
      if(rule!=null) {
         if(el.type!='radio') {
           if(!checkFormElement(el,rule)) {
              if(rule.alerttext) alert(rule.alerttext);
              el.focus();
              return false;            
           }
         }
         else {
           var radios = f.elements[el.name];
           if(rule.required==true) {
               var found=false;
               for(var j=0; j < radios.length;j++) {
                 if(radios[j].checked) {
                    found=true;
                    break;
                 }
               }
               if(!found) {
                  if(rule.alerttext) alert(rule.alerttext);
                  el.focus();
                  return false;            
               }
           }           
         }
      }
   }
   return true;
}

var rules=new Array();