/*
var mes_demandes = {
	0 : {
		"selecteur" : "b.uneclass",
		"evenement" : "onclick",
		"action" : function()
			{
			alert(this.innerHTML);
			}
		},
	1 : {
		"selecteur" : "#un_identifiant u",
		"evenement" : "onmouseover",
		"action" : function()
			{
			this.innerHTML = "BLAH!";
			}
		}
	};

amelioration.ajouter_des_ameliorations( mes_demandes );

appeler amelioration.appliquer_les_ameliorations() pour r�appliquer les am�liorations
*/

var amelioration =
	{
	"liste_des_ameliorations" : new Array(),
	
	"ajouter_des_ameliorations" : function( demandes )
		{
		for( var num_demande=0 ; ma_demande=demandes[num_demande] ; num_demande++ )
			amelioration.liste_des_ameliorations.push( ma_demande );
		},
	
	"appliquer_les_ameliorations" : function()
		{
		var liste_des_elements = null;
		var ma_demande = null;
		var mon_element = null;

		for( var num_demande=0 ; ma_demande=amelioration.liste_des_ameliorations[num_demande] ; num_demande++ )
			{
			liste_des_elements = document.getElementsBySelector( ma_demande.selecteur );
				
			if( !liste_des_elements )
				continue;

			for( var num_element=0 ; mon_element=liste_des_elements[num_element] ; num_element++ )
				{
				if( document.all )
					{
					mon_element.detachEvent( ma_demande.evenement, ma_demande.action );
					mon_element.attachEvent( ma_demande.evenement, ma_demande.action );
					}
				else
					{
					mon_element.removeEventListener( ma_demande.evenement.substr( 2, ma_demande.evenement.length-2 ), ma_demande.action, false );
					mon_element.addEventListener( ma_demande.evenement.substr( 2, ma_demande.evenement.length-2 ), ma_demande.action, false );
					}
				}
			}
		}
	};

// ajout vb1 : propriete trim sur les string
String.prototype.trim = function(str) {
    str = this != window? this : str;
    // vb1 : modif de la regexp : remplac�  \s par [\s\xA0]
    // car replace de \s ( espace ) ne fonctionne pas sous IE quand la chaine comporte des espaces cr��s
    // par conversion de &nbsp; par la m�thode unescapeHTML() car le code de ce caractere est 160( A0 ) au lieu de 32
    return str.replace(/^[\s\xA0]+/g, '').replace(/[\s\xA0]+$/g, ''); 
};

// ajout vb1 : propriete removeaccent sur les string
String.prototype.removeaccent = function(str) {
    str = this != window? this : str;
    return str.replace(/[������]/g, 'a').replace(/[����]/g, 'e').replace(/[����]/g, 'i').
        replace(/[�����]/g, 'o').replace(/�/g, 'n').replace(/[����]/g, 'u').replace(/[��]/g, 'y').
            replace(/�/g, 'c').replace(/�/g, 'oe');
};

// ajout vb1 : propriete removeaccent sur les string
String.prototype.convertir_en_nom_de_variable = function(str) {
    str = this != window? this : str;
    return str.trim().removeaccent().toLowerCase().replace(/[ ]/g, '_');
};

if( document.all )
	window.attachEvent( "onload", amelioration.appliquer_les_ameliorations );
else
	window.addEventListener( "load", amelioration.appliquer_les_ameliorations, false );
