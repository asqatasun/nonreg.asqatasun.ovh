// Gestion des PopUp de types showModelessDialog 
var bDialogStatus = false;     // Indicateur d'ouverture
var bDialogStatus2 = false;     // Indicateur d'ouverture
BackOfficePath = "/SERLR/BackOffice/";


// Demande une confirmation (oui ou non)
function Confirmation(strPhr) {
	lefichier = BackOfficePath+"templates/pop_confirmation.php";
	if (!document.all) {
		sReponse = confirm(withoutHTML(strPhr));
	} else {
		sReponse = showModalDialog(lefichier,strPhr,"center:yes;help:no;resizable:no;status:no;dialogWidth:300px;dialogHeight:160px");
	}
	return sReponse;
}

// Simple avertissement
function Confirmation2(strPhr) {
	lefichier2 = BackOfficePath+"templates/pop_confirmation2.php";
	if (!document.all) {
		sReponse2 = alert(withoutHTML(strPhr));
	} else {	
		sReponse2 = showModalDialog(lefichier2,strPhr,"center:yes;help:no;resizable:no;status:no;dialogWidth:300px;dialogHeight:160px");
	}
	return sReponse2;
}

// Vire les <tag>
function withoutHTML(String) {
	var reg1 = /<br[^>]*>/gi;
	String =String.replace(reg1,"\n\r");
	var reg1 = /<[^>]*>/gi;
	return String.replace(reg1,"");
	
}

// Ouvir Lien
function OuvrirLien(URL,NOM,FEATURE) {
		window.open(URL,NOM,FEATURE);
}