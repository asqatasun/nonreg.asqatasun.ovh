User-agent: *
Disallow: /cms/
Disallow: /include/
Disallow: /eam/
Disallow: /externe/
Disallow: /formulaire/
Disallow: /images/
Disallow: /stats/
Disallow: /tinymce/
Disallow: /tinymce_old/
Disallow: /uploads/
Disallow: /webotheque/

Sitemap: /sitemap.xml