function submitForm(methode, url, page, id)
{
	var xhr; 
	try
	{
		xhr = new ActiveXObject('Msxml2.XMLHTTP');
	}
	catch (e) 
	{
		try
		{
			xhr = new ActiveXObject('Microsoft.XMLHTTP');
		}
		catch (e2) 
		{
			try
			{
				xhr = new XMLHttpRequest();
			}
			catch (e3)
			{
				xhr = false;
			}
		}
	}

	xhr.onreadystatechange = function()
	{
		if(xhr.readyState == 4)
		{
			if(xhr.status == 200)
				switch (page)
				{
					case "affiche":
						affichageListeAffiches(xhr);
						break;
					case "resoc":
						remplissageResoc(xhr);
						break;
					case "auteurs":
						remplissageAuteurs(xhr);
						break;
					case "cartesPostales":
						remplissageCartesPostales(xhr);
						break;
					case "actualiteADMIN":
						remplissageActualite(xhr);
						break;
					case "afficheADMIN":
						remplissageAffiche(xhr);
						break;
					case "agendaADMIN":
						remplissageAgenda(xhr);
						break;
					case "MlisteAuteursADMIN":
						listerAuteurs(xhr, "m");
						break;
					case "SlisteAuteursADMIN":
						listerAuteurs(xhr, "s");
						break;
					case "M_auteursADMIN":
						remplissageAuteur(xhr);
						break;
					case "S_auteursADMIN":
						suppressionAuteur();
						break;
					case "lienADMIN":
						remplissageLien(xhr);
						break;
					case "MlisteCartesPostalesADMIN":
						listerCartesPostales(xhr, "m");
						break;
					case "SlisteCartesPostalesADMIN":
						listerCartesPostales(xhr, "s");
						break;
					case "M_cartesPostalesADMIN":
						remplissageCartePostale(xhr);
						break;
					case "S_cartesPostalesADMIN":
						suppressionCartePostale();
						break;
					case "resocADMIN":
						modifierResoc(xhr);
						break;
				}
		}
	};

	if (methode == "GET")
	{
		xhr.open("GET", "admin/"+url+"?page="+page+"&id="+id, true);
		xhr.overrideMimeType('text/html; charset=ISO-8859-1');							// avec -15 probleme d'apostrophe -> a voir
		xhr.send(null);
	}
	else if (page == "affiche")
	{
		xhr.open("POST", url+"?page="+page, true);
		xhr.overrideMimeType('text/html; charset=ISO-8859-15');							// resoud le probl�me des caract�res accentu�s
		xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhr.setRequestHeader("Content-length", id.length);
		xhr.setRequestHeader("Connection", "close");
		xhr.send("theme="+id['theme']+"&serie="+id['serie']+"&titre="+id['titre']+"&lieu="+id['lieu']+"&region="+id['region']+"&dateDeb="+id['dateDeb']+"&dateFin="+id['dateFin']);
	}
	else if (page == "resoc")
	{
		xhr.open("POST", url+"?page="+page, true);
		xhr.overrideMimeType('text/html; charset=ISO-8859-15');
		xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhr.setRequestHeader("Content-length", id.length);
		xhr.setRequestHeader("Connection", "close");
		xhr.send("type="+id['type']+"&departement="+id['departement']);
	}
	else if (page == "auteurs" || page == "MlisteAuteursADMIN" || page == "SlisteAuteursADMIN")
	{
		if (url == "ajax.php")
			url = "admin/"+url;
		xhr.open("POST", url+"?page="+page, true);
		xhr.overrideMimeType('text/html; charset=ISO-8859-15');
		xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhr.setRequestHeader("Content-length", id.length);
		xhr.setRequestHeader("Connection", "close");
		xhr.send("nom="+id['nom']);
	}
	else if (page == "cartesPostales" || page == "MlisteCartesPostalesADMIN" || page == "SlisteCartesPostalesADMIN")
	{
		if (url == "ajax.php")
			url = "admin/"+url;
		xhr.open("POST", url+"?page="+page, true);
		xhr.overrideMimeType('text/html; charset=ISO-8859-15');
		xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhr.setRequestHeader("Content-length", id.length);
		xhr.setRequestHeader("Connection", "close");
		if (id['motClef'])
			parametre = "motClef="+id['motClef'];
		else if (id['categorie'] || id['sousCategorie'])
			parametre = "categorie="+id['categorie']+"&sousCategorie="+id['sousCategorie'];
		else if (id['departement'] || id['ville'] || id['date'])
			parametre = "departement="+id['departement']+"&ville="+id['ville']+"&date="+id['date'];
		xhr.send(parametre);
	}
}

function remplissageActualite(xhr)
{
	var chaine = xhr.responseText;
	var tableau = chaine.split('|');
	document.getElementsByName("titreOccitan")[0].value = html_entity_decode(tableau[2]);
	document.getElementsByName("titreFrancais")[0].value = html_entity_decode(tableau[1]);
	document.getElementsByName("debutParution")[0].value = html_entity_decode(tableau[3]);
	document.getElementsByName("finParution")[0].value = html_entity_decode(tableau[4]);
	document.getElementsByName("dateTri")[0].value = html_entity_decode(tableau[5]);
	document.getElementsByName("lien")[0].value = html_entity_decode(tableau[6]);
	document.getElementsByName("departement")[0].value = html_entity_decode(tableau[7]);
	document.getElementsByName("priorite")[0].value = html_entity_decode(tableau[8]);
	document.getElementsByName("texteCourtOccitan")[0].innerHTML = html_entity_decode(tableau[10]);
	document.getElementsByName("texteCourtFrancais")[0].innerHTML = html_entity_decode(tableau[9]);
	document.getElementsByName("texteLongOccitan")[0].innerHTML = html_entity_decode(tableau[12]);
	document.getElementsByName("texteLongFrancais")[0].innerHTML = html_entity_decode(tableau[11]);
	document.getElementsByName("num")[0].value = html_entity_decode(tableau[0]);
	if (tableau[17] == 1)
		document.getElementsByName("actif")[0].checked = true;
	else
		document.getElementsByName("actif")[0].checked = false;
	if (tableau[13] == 1)
		document.getElementsByName("portadoc")[0].checked = true;
	else
		document.getElementsByName("portadoc")[0].checked = false;
	if (tableau[14] == 1)
		document.getElementsByName("cirdoc")[0].checked = true;
	else
		document.getElementsByName("cirdoc")[0].checked = false;
	if (tableau[15] == 1)
		document.getElementsByName("mediatheque")[0].checked = true;
	else
		document.getElementsByName("mediatheque")[0].checked = false;
	if (tableau[16] == 1)
		document.getElementsByName("partenaire")[0].checked = true;
	else
		document.getElementsByName("partenaire")[0].checked = false;
}

function remplissageAffiche(xhr)
{
	var chaine = xhr.responseText;
	var tableau = chaine.split('|');
	document.getElementsByName("num")[0].value = tableau[0];
	document.getElementsByName("num_inventaire")[0].value = tableau[1];
	document.getElementsByName("titre")[0].value = html_entity_decode(tableau[2]);
	document.getElementsByName("entoile")[0].value = html_entity_decode(tableau[6]);
	document.getElementsByName("cote")[0].value = html_entity_decode(tableau[8]);
	document.getElementsByName("date_affiche")[0].value = html_entity_decode(tableau[13]);
	document.getElementsByName("date_acquisition")[0].value = html_entity_decode(tableau[21]);
	document.getElementsByName("collection")[0].value = html_entity_decode(tableau[23]);
	document.getElementsByName("manifestation_cido")[0].value = html_entity_decode(tableau[24]);
	document.getElementsByName("lieu_affiche")[0].value = html_entity_decode(tableau[10]);
	document.getElementsByName("pays_affiche")[0].value = html_entity_decode(tableau[12]);
	document.getElementsByName("nom_edition")[0].value = html_entity_decode(tableau[17]);
	document.getElementsByName("lieu_edition")[0].value = html_entity_decode(tableau[18]);
	document.getElementsByName("date_edition")[0].value = html_entity_decode(tableau[19]);
	document.getElementsByName("observations")[0].innerHTML = html_entity_decode(tableau[26]);
	/*document.getElementsByName("image")[0].value = html_entity_decode(tableau[27]);
	if (tableau[20] == 1)
		document.getElementsByName("culture")[0].options[0].selected = true;
	else
		document.getElementsByName("culture")[0].options[1].selected = true;*/
	for (var i = 0 ; i < document.getElementsByName("format")[0].options.length ; i++)
		if (document.getElementsByName("format")[0].options[i].value == tableau[3])
			document.getElementsByName("format")[0].options[i].selected = true;
	for (var i = 0 ; i < document.getElementsByName("sens_vh")[0].options.length ; i++)
		if (document.getElementsByName("sens_vh")[0].options[i].value == tableau[4])
			document.getElementsByName("sens_vh")[0].options[i].selected = true;
	for (var i = 0 ; i < document.getElementsByName("nature")[0].options.length ; i++)
		if (document.getElementsByName("nature")[0].options[i].value == tableau[5])
			document.getElementsByName("nature")[0].options[i].selected = true;
	for (var i = 0 ; i < document.getElementsByName("typologie")[0].options.length ; i++)
		if (document.getElementsByName("typologie")[0].options[i].value == tableau[7])
			document.getElementsByName("typologie")[0].options[i].selected = true;
	for (var i = 0 ; i < document.getElementsByName("theme")[0].options.length ; i++)
		if (document.getElementsByName("theme")[0].options[i].value == tableau[9])
			document.getElementsByName("theme")[0].options[i].selected = true;
	for (var i = 0 ; i < document.getElementsByName("recto_verso")[0].options.length ; i++)
		if (document.getElementsByName("recto_verso")[0].options[i].value == tableau[14])
			document.getElementsByName("recto_verso")[0].options[i].selected = true;
	for (var i = 0 ; i < document.getElementsByName("provenance")[0].options.length ; i++)
		if (document.getElementsByName("provenance")[0].options[i].value == tableau[15])
			document.getElementsByName("provenance")[0].options[i].selected = true;
	for (var i = 0 ; i < document.getElementsByName("serie")[0].options.length ; i++)
		if (document.getElementsByName("serie")[0].options[i].value == tableau[25])
			document.getElementsByName("serie")[0].options[i].selected = true;
	for (var i = 0 ; i < document.getElementsByName("region_affiche")[0].options.length ; i++)
		if (document.getElementsByName("region_affiche")[0].options[i].value == tableau[11])
			document.getElementsByName("region_affiche")[0].options[i].selected = true;
	if (tableau[20] == 1)
		document.getElementsByName("culture_oc")[0].checked = true;
	else
		document.getElementsByName("culture_oc")[0].checked = false;
	if (tableau[22] == 1)
		document.getElementsByName("culture_catalan")[0].checked = true;
	else
		document.getElementsByName("culture_catalan")[0].checked = false;
}

function remplissageAgenda(xhr)
{
	var chaine = xhr.responseText;
	var tableau = chaine.split('|');
	document.getElementsByName("periodeOccitan")[0].value = html_entity_decode(tableau[6]);
	document.getElementsByName("periodeFrancais")[0].value = html_entity_decode(tableau[5]);
	document.getElementsByName("libelleOccitan")[0].value = html_entity_decode(tableau[4]);
	document.getElementsByName("libelleFrancais")[0].value = html_entity_decode(tableau[3]);
	document.getElementsByName("dateTri")[0].value = html_entity_decode(tableau[9]);
	document.getElementsByName("lien")[0].value = html_entity_decode(tableau[8]);
	document.getElementsByName("sauvNomImg")[0].innerHTML = html_entity_decode(tableau[7]);
	document.getElementsByName("priorite")[0].value = html_entity_decode(tableau[2]);
	document.getElementsByName("num")[0].value = html_entity_decode(tableau[0]);
	if (tableau[1] == 1)
		document.getElementsByName("actif")[0].checked = true;
	else
		document.getElementsByName("actif")[0].checked = false;
}

function remplissageAuteur(xhr)
{
	var chaine = xhr.responseText;
	var tableau = chaine.split('|');
	document.getElementsByName("nomAuteur")[0].value = html_entity_decode(tableau[1]);
	document.getElementsByName("prenomAuteur")[0].value = html_entity_decode(tableau[2]);
	document.getElementsByName("anneeNaissance")[0].value = html_entity_decode(tableau[3]);
	document.getElementsByName("anneeDeces")[0].value = html_entity_decode(tableau[4]);
	document.getElementsByName("commentaire")[0].value = html_entity_decode(tableau[9]);
	document.getElementsByName("sauvNomImg")[0].innerHTML = html_entity_decode(tableau[10]);
	document.getElementsByName("num")[0].value = html_entity_decode(tableau[0]);
	if (tableau[5] == 1)
		document.getElementsByName("supportArticle")[0].checked = true;
	else
		document.getElementsByName("supportArticle")[0].checked = false;
	if (tableau[6] == 1)
		document.getElementsByName("supportIcono")[0].checked = true;
	else
		document.getElementsByName("supportIcono")[0].checked = false;
	if (tableau[7] == 1)
		document.getElementsByName("supportBrochure")[0].checked = true;
	else
		document.getElementsByName("supportBrochure")[0].checked = false;
	if (tableau[8] == 1)
		document.getElementsByName("supportAutre")[0].checked = true;
	else
		document.getElementsByName("supportAutre")[0].checked = false;
}

function suppressionAuteur()
{
	document.getElementById("listeAuteurs").style.marginTop = "20px";
	document.getElementById("listeAuteurs").innerHTML = "L'auteur a �t� suprim�.";
}

function remplissageCartePostale(xhr)
{
	var chaine = xhr.responseText;
	var tableau = chaine.split('|');
	document.getElementsByName("titre")[0].value = html_entity_decode(tableau[4]);
	document.getElementsByName("date_acquisition")[0].value = html_entity_decode(tableau[6]);
	document.getElementsByName("editeur")[0].value = html_entity_decode(tableau[3]);
	document.getElementsByName("natureOccitan")[0].value = html_entity_decode(tableau[2]);
	document.getElementsByName("natureFrancais")[0].value = html_entity_decode(tableau[1]);
	document.getElementsByName("cote")[0].value = html_entity_decode(tableau[24]);
	document.getElementsByName("couleur")[0].value = html_entity_decode(tableau[5]);
	document.getElementsByName("format")[0].value = html_entity_decode(tableau[9]);
	document.getElementsByName("taille")[0].value = html_entity_decode(tableau[3]);
	document.getElementsByName("code_bar")[0].value = html_entity_decode(tableau[22]);
	document.getElementsByName("num_inv")[0].value = html_entity_decode(tableau[23]);
	document.getElementsByName("destinataire")[0].value = html_entity_decode(tableau[12]);
	document.getElementsByName("expediteur")[0].value = html_entity_decode(tableau[13]);
	document.getElementsByName("date_edition")[0].value = html_entity_decode(tableau[14]);
	document.getElementsByName("cp_dest")[0].value = html_entity_decode(tableau[17]);
	document.getElementsByName("numSerie")[0].value = html_entity_decode(tableau[26]);
	document.getElementsByName("adersse1")[0].value = html_entity_decode(tableau[15]);
	document.getElementsByName("adersse2")[0].value = html_entity_decode(tableau[16]);
	document.getElementsByName("ville")[0].value = html_entity_decode(tableau[19]);
	document.getElementsByName("pays")[0].value = html_entity_decode(tableau[20]);
	document.getElementsByName("commentaireOccitan")[0].value = html_entity_decode(tableau[8]);
	document.getElementsByName("commentaireFrancais")[0].value = html_entity_decode(tableau[7]);
	document.getElementsByName("correspondance")[0].value = html_entity_decode(tableau[21]);

	document.getElementsByName("sauvNomImgR")[0].innerHTML = html_entity_decode(tableau[10])+"R"+html_entity_decode(tableau[11]);
	document.getElementsByName("sauvNomImgV")[0].innerHTML = html_entity_decode(tableau[10])+"V"+html_entity_decode(tableau[11]);
	document.getElementsByName("num")[0].value = html_entity_decode(tableau[0]);
	for (var i = 0 ; i < document.getElementsByName("fonds")[0].options.length ; i++)
		if (document.getElementsByName("fonds")[0].options[i].value == tableau[28])
			document.getElementsByName("fonds")[0].options[i].selected = true;
	for (var i = 0 ; i < document.getElementsByName("serie")[0].options.length ; i++)
		if (document.getElementsByName("serie")[0].options[i].value == tableau[25])
			document.getElementsByName("serie")[0].options[i].selected = true;
	for (var i = 0 ; i < document.getElementsByName("localisation")[0].options.length ; i++)
		if (document.getElementsByName("localisation")[0].options[i].value == tableau[27])
			document.getElementsByName("localisation")[0].options[i].selected = true;
	for (var i = 0 ; i < document.getElementsByName("departement")[0].options.length ; i++)
		if (document.getElementsByName("departement")[0].options[i].value == tableau[18])
			document.getElementsByName("departement")[0].options[i].selected = true;
}

function suppressionCartePostale()
{
	document.getElementById("listeCartesPostales").style.marginTop = "20px";
	document.getElementById("listeCartesPostales").innerHTML = "La carte postale a �t� suprim�e.";
}

function remplissageLien(xhr)
{
	var chaine = xhr.responseText;
	var tableau = chaine.split('|');
	document.getElementsByName("lien")[0].value = html_entity_decode(tableau[6]);
	document.getElementsByName("titreOccitan")[0].value = html_entity_decode(tableau[3]);
	document.getElementsByName("titreFrancais")[0].value = html_entity_decode(tableau[2]);
	document.getElementsByName("texteOccitan")[0].value = html_entity_decode(tableau[5]);
	document.getElementsByName("texteFrancais")[0].value = html_entity_decode(tableau[4]);
	document.getElementsByName("sauvNomImg")[0].innerHTML = html_entity_decode(tableau[1]);
	document.getElementsByName("num")[0].value = html_entity_decode(tableau[0]);
}

function modifierResoc(xhr)
{
	var chaine = xhr.responseText;
	var tableau = chaine.split('|');
	document.getElementsByName("etablissement")[0].value = html_entity_decode(tableau[3]);
	document.getElementsByName("adresse1")[0].value = html_entity_decode(tableau[4]);
	document.getElementsByName("adresse2")[0].value = html_entity_decode(tableau[5]);
	document.getElementsByName("codePostal")[0].value = html_entity_decode(tableau[6]);
	document.getElementsByName("numeroDeDepartement")[0].value = html_entity_decode(tableau[7]);
	document.getElementsByName("ville")[0].value = html_entity_decode(tableau[8]);
	document.getElementsByName("ccfr")[0].value = html_entity_decode(tableau[9]);
	document.getElementsByName("numEnquete")[0].value = html_entity_decode(tableau[2]);
	document.getElementsByName("mincult")[0].value = html_entity_decode(tableau[10]);
	document.getElementsByName("sudoc")[0].value = html_entity_decode(tableau[11]);
	document.getElementsByName("siteWeb")[0].value = html_entity_decode(tableau[12]);
	document.getElementsByName("catalogue")[0].value = html_entity_decode(tableau[13]);
	document.getElementsByName("contact")[0].value = html_entity_decode(tableau[14]);
	document.getElementsByName("telephone")[0].value = html_entity_decode(tableau[15]);
	document.getElementsByName("mail")[0].value = html_entity_decode(tableau[16]);
	document.getElementsByName("num")[0].value = html_entity_decode(tableau[0]);
	for (var i = 0 ; i < document.getElementsByName("type")[0].options.length ; i++)
		if (document.getElementsByName("type")[0].options[i].value == tableau[1])
			document.getElementsByName("type")[0].options[i].selected = true;
}

// on declare ces variables glogales car il y a pas moyen de les passer par la fonction dans le onClick
var affiche = new Array();
var nbResultats;
var numeroAffiche;
var nbAfficheAAfficher;
var numeroAfficheSuivante;

function affichageListeAffiches(xhr)
{
	var chaine = xhr.responseText;
	affiche = chaine.split('||');

	nbResultats = affiche[0];
	numeroAffiche = parseInt(document.getElementsByName("numeroAffiche")[0].value);
	nbAfficheAAfficher = parseInt(document.getElementsByName("nbAffiche")[0].value);
	numeroAfficheSuivante = numeroAffiche + nbAfficheAAfficher;

	if (nbResultats > nbAfficheAAfficher)
		document.getElementById("navigationAffiche").style.cssText = 'display:block;';
	else
		document.getElementById("navigationAffiche").style.cssText = 'display:none;';

	document.getElementById("nbResultats").innerHTML = "nombre de r�sultats :<br>"+nbResultats;
	document.getElementById("nbResultats").style.cssText = 'display:block;';

	afficherListe();
}

function afficherListe()
{
	// d'abord on efface les affichages precedants
	for (var j = 0 ; j < nbAfficheAAfficher ; j++)
		document.getElementById("listeAffiche"+j).style.display = "none";

	numeroAffiche + nbAfficheAAfficher >= nbResultats ? derniereAffiche = nbResultats : derniereAffiche = numeroAffiche + nbAfficheAAfficher;

	for (var i = numeroAffiche ; i < derniereAffiche ; i++)
	{
		var elementAffiche = affiche[i + 1].split('|');
		var j = i;
		if (j >= nbAfficheAAfficher)
			j = j % nbAfficheAAfficher;
		document.getElementById("listeAffiche"+j).style.display = "block";
		document.getElementById("infoImg"+j).innerHTML = "<img src = 'images/"+elementAffiche[26].replace('#',"").replace('#',"")+"' height = 50 width = 50 />";
		document.getElementById("nomImg"+j).value = "images/"+elementAffiche[26].replace('#',"").replace('#',"");
		document.getElementById("infoTitre"+j).innerHTML = html_entity_decode(elementAffiche[2].replace('#',"").replace('#',""));
		document.getElementById("infoFormat"+j).value = elementAffiche[3].replace('#',"").replace('#',"");
		document.getElementById("infoSens"+j).value = elementAffiche[4].replace('#',"").replace('#',"");
		document.getElementById("infoNature"+j).value = elementAffiche[5].replace('#',"").replace('#',"");
		document.getElementById("infoEntoile"+j).value = elementAffiche[6].replace('#',"").replace('#',"");
		document.getElementById("infoTypologie"+j).value = elementAffiche[7].replace('#',"").replace('#',"");
		document.getElementById("infoCote"+j).innerHTML = "Cote : "+elementAffiche[8].replace('#',"").replace('#',"");
		document.getElementById("infoTheme"+j).value = elementAffiche[9].replace('#',"").replace('#',"");
		document.getElementById("infoLieu_affiche"+j).value = elementAffiche[10].replace('#',"").replace('#',"");
		document.getElementById("infoRegion_affiche"+j).value = elementAffiche[11].replace('#',"").replace('#',"");
		document.getElementById("infoPays_affiche"+j).value = elementAffiche[12].replace('#',"").replace('#',"");
		document.getElementById("infoDate_affiche"+j).value = elementAffiche[13].replace('#',"").replace('#',"");
		document.getElementById("infoRecto_verso"+j).value = elementAffiche[14].replace('#',"").replace('#',"");
		document.getElementById("infoProvenance"+j).value = elementAffiche[15].replace('#',"").replace('#',"");
		document.getElementById("infoDate_acquisition"+j).value = elementAffiche[16].replace('#',"").replace('#',"");
		document.getElementById("infoNom_edition"+j).value = elementAffiche[17].replace('#',"").replace('#',"");
		document.getElementById("infoLieu_edition"+j).value = elementAffiche[18].replace('#',"").replace('#',"");
		document.getElementById("infoDate_edition"+j).value = elementAffiche[19].replace('#',"").replace('#',"");
		document.getElementById("infoCulture_oc"+j).value = elementAffiche[20].replace('#',"").replace('#',"");
		document.getElementById("infoCulture_catalan"+j).value = elementAffiche[21].replace('#',"").replace('#',"");
		document.getElementById("infoCollection"+j).value = elementAffiche[22].replace('#',"").replace('#',"");
		document.getElementById("infoManifestation_cido"+j).value = elementAffiche[23].replace('#',"").replace('#',"");
		document.getElementById("infoSerie"+j).innerHTML = "S�rie : "+elementAffiche[24].replace('#',"").replace('#',"");
		document.getElementById("infoObservations"+j).value = elementAffiche[25].replace('#',"").replace('#',"");
	}

	// affichage si elle existe de la page precedante
	if (numeroAffiche > 0)
		document.getElementById('precedant').style.cssText = 'display:block;';
	else
		document.getElementById('precedant').style.cssText = 'display:none;';
	// affichage si elle existe de la page suivante
	var pageSuivante = numeroAffiche + nbAfficheAAfficher;
	if (numeroAffiche + nbAfficheAAfficher < nbResultats)
		document.getElementById('suivant').style.cssText = 'display:block;';
	else
		document.getElementById('suivant').style.cssText = 'display:none;';
}

function afficherPrecedant()
{
	numeroAffiche = numeroAffiche - nbAfficheAAfficher;
	if (numeroAffiche < 0)
		numeroAffiche = 0;

	afficherListe();
}

function afficherSuivant()
{
	numeroAffiche = numeroAffiche + nbAfficheAAfficher;

	afficherListe();
}

function remplissageResoc(xhr)
{
	var chaine = xhr.responseText;
	var affiche = chaine.split('||');

	// on cre l'entete
	var listeResoc = "<div class = \"ligne\"><div class = \"caseTabResoc\" style = \"width: 220px;\"><strong>Etablissement</strong></div><div class = \"caseTabResoc\" style = \"width: 94px;\"><strong>Type</strong></div><div class = \"caseTabResoc\" style = \"width: 120px;\"><strong>Ville</strong></div><div class = \"caseTabResoc\"><strong>Site WEB</strong></div><div class = \"caseTabResoc\"><strong>Catalogue</strong></div><div class = \"caseTabResoc\" style = \"width: 50px;\"><strong>CCFR</strong></div><div class = \"caseTabResoc\" style = \"width: 80px;\"><strong>Minist�re Culture</strong></div><div class = \"caseTabResoc\"><strong>SUDOC</strong></div></div>";

	var x = 0;
	while (affiche[x] != "")
	{
		var elementAffiche = affiche[x].split('|');

		listeResoc += "<div class = \"caseTabResoc\" style = \"width: 220px;\">"+elementAffiche[0].replace('#',"").replace('#',"")+"</div>";
		listeResoc += "<div class = \"caseTabResoc\" style = \"width: 94px;\">"+elementAffiche[6].replace('#',"").replace('#',"")+"</div>";
		listeResoc += "<div class = \"caseTabResoc\" style = \"width: 120px;\">"+elementAffiche[7].replace('#',"").replace('#',"")+"</div>";
		listeResoc += "<div class = \"caseTabResoc\">";
			if (elementAffiche[1].replace('#',"").replace('#',"") != "")
				listeResoc += "<a href = '"+elementAffiche[1].replace('#',"").replace('#',"")+"'><img src = 'design/theme_original/image_look/bt_web2.gif'></a>";
		listeResoc += "</div>";
		listeResoc += "<div class = \"caseTabResoc\">";
			if (elementAffiche[2].replace('#',"").replace('#',"") != "")
				listeResoc += "<a href = '"+elementAffiche[2].replace('#',"").replace('#',"")+"'><img src = 'design/theme_original/image_look/bt_catalogue.gif'></a>";
		listeResoc += "</div>";
		listeResoc += "<div class = \"caseTabResoc\" style = \"width: 50px;\">";
			if (elementAffiche[3].replace('#',"").replace('#',"") != "")
				listeResoc += "<a href = '"+elementAffiche[3].replace('#',"").replace('#',"")+"'><img src = 'design/theme_original/image_look/bt_ccfr2.gif'></a>";
		listeResoc += "</div>";
		listeResoc += "<div class = \"caseTabResoc\" style = \"width: 80px;\">";
			if (elementAffiche[4].replace('#',"").replace('#',"") != "")
				listeResoc += "<a href = '"+elementAffiche[4].replace('#',"").replace('#',"")+"'><img src = 'design/theme_original/image_look/bt_mincult2.gif'></a>";
		listeResoc += "</div>";
		listeResoc += "<div class = \"caseTabResoc\">"+elementAffiche[5].replace('#',"").replace('#',"")+"</div>";
		x++;
	}

	document.getElementById("listeResoc").innerHTML = listeResoc;
}

function remplissageAuteurs(xhr)
{
	var chaine = xhr.responseText;
	var tableau = chaine.split('||');

	var listeAuteurs = "<ul>";
	var x = 0;
	while (tableau[x] != "")
	{
		var elementAuteur = tableau[x].split('|');

		listeAuteurs += "<li style = 'margin: 10px;'><a href = 'index.php?page=catalogue/catalogue&rubrique=auteurs&affiche=";
		listeAuteurs += elementAuteur[0].replace('#',"").replace('#',"")+"'><strong>";
		listeAuteurs += elementAuteur[1].replace('#',"").replace('#',"");
		listeAuteurs += "</strong> "+elementAuteur[2].replace('#',"").replace('#',"");
		listeAuteurs += " ("+elementAuteur[3].replace('#',"").replace('#',"");
		if (elementAuteur[4].replace('#',"").replace('#',"") != "")
			listeAuteurs += " - "+elementAuteur[4].replace('#',"").replace('#',"");
		listeAuteurs += ")</a></li>";
		x++;
	}
	listeAuteurs += "</ul>";

	document.getElementById("listeAuteurs").innerHTML = listeAuteurs;
}

var tableauCartesPostales = new Array();
var nbResultats;
var numeroCP = 0;
var nbCPAAfficher = 5;
var numeroCPSuivante;

function remplissageCartesPostales(xhr)
{
	var chaine = xhr.responseText;
	tableauCartesPostales = chaine.split('||');

	creerTableauCartesPostales();
}

function creerTableauCartesPostales()
{
	nbResultats = tableauCartesPostales[0];

	if (document.getElementsByName("numeroCP")[0])
		numeroCP = parseInt(document.getElementsByName("numeroCP")[0].value);
	if (document.getElementsByName("nbCPAAfficher")[0])
		nbCPAAfficher = parseInt(document.getElementsByName("nbCPAAfficher")[0].value);

	// d'abord on efface les affichages precedants
	var numeroCPPrecedent = numeroCP - nbCPAAfficher;
	if (numeroCPPrecedent < 0)
		numeroCPPrecedent = 0;
	for (var j = numeroCPPrecedent ; j < numeroCP ; j++)
		if (document.getElementById("menuCP"))
			document.getElementById("menuCP").style.display = "none";
	if (document.getElementById("listeCP"+j))
		document.getElementById("listeCP"+j).style.display = "none";

	var listeCP = "";
	numeroCP + nbCPAAfficher >= nbResultats ? derniereCP = nbResultats : derniereCP = numeroCP + nbCPAAfficher;

	for (var i = numeroCP ; i < derniereCP ; i++)
	{
		var elementCP = tableauCartesPostales[i + 1].split('|');
		var j = i;
		if (j >= nbCPAAfficher)
			j = j % nbCPAAfficher;
		listeCP += "<div class = \"actualite\" id = \"listeCP"+i+"\" style = \"width: 800px; margin-left: auto; margin-right: auto;\">";
		listeCP += "<a href = 'index.php?page=catalogue/catalogue&rubrique=cartesPostales&cartePostale=";
		listeCP += elementCP[0].replace('#',"").replace('#',"")+"'>";
		listeCP += "<img src = \"catalogue/cartesPostales/images/"+elementCP[2].replace('#',"").replace('#',"");
		listeCP += "R"+elementCP[3].replace('#',"").replace('#',"")+"\" height = 100 width = 100 />";
		listeCP += elementCP[1].replace('#',"").replace('#',"")+"</a></div>";
	}
	var numeroCPSuivant = numeroCP + nbCPAAfficher;
	listeCP += "<input type = \"hidden\" name = \"numeroCP\" value = \""+numeroCP+"\"/>";
	listeCP += "<input type = \"hidden\" name = \"nbCPAAfficher\" value = \""+nbCPAAfficher+"\"/>";

	// creation du menu permettant d'acceder aux donn�es suivantes
	if (nbResultats > nbCPAAfficher)
	{
		listeCP += "<div id = \"menuCP\">";
		if (numeroCP > 0)
			listeCP += "<a id = \"precedant\" onclick = \"CPPrecedante()\">Page pr�c�dente</a>";
		if (nbResultats > derniereCP)
			listeCP += "<a id = \"suivant\" onclick = \"CPSuivante()\">Page suivante</a>";
		listeCP += "</div>";
	}

	if (document.getElementById("cartePostale"))
		document.getElementById("cartePostale").innerHTML = "";
	document.getElementById("listeCP").innerHTML = listeCP;
}

function CPPrecedante()
{
	document.getElementsByName("numeroCP")[0].value = parseInt(document.getElementsByName("numeroCP")[0].value) - nbCPAAfficher;
	if (parseInt(document.getElementsByName("numeroCP")[0].value) < 0)
		document.getElementsByName("numeroCP")[0].value = 0;

	creerTableauCartesPostales();
}

function CPSuivante()
{
	document.getElementsByName("numeroCP")[0].value = parseInt(document.getElementsByName("numeroCP")[0].value) + nbCPAAfficher;
	if (parseInt(document.getElementsByName("numeroCP")[0].value) > nbResultats)
		document.getElementsByName("numeroCP")[0].value = nbResultats;

	creerTableauCartesPostales();
}

function listerAuteurs(xhr, action)
{
	var chaine = xhr.responseText;
	var tableau = chaine.split('||');

	if (tableau.length >= 3)
	{
		var listeAuteurs = "<ul>";
		var x = 0;
		while (tableau[x] != "")
		{
			var elementAuteur = tableau[x].split('|');

			if (action == "m")
				listeAuteurs += "<li style = 'margin: 10px; cursor: pointer;' onclick = \"submitForm('GET', 'ajax.php', 'M_auteursADMIN', ";
			else
				listeAuteurs += "<li style = 'margin: 10px; cursor: pointer;' onclick = \"submitForm('GET', 'ajax.php', 'S_auteursADMIN', ";
			listeAuteurs += elementAuteur[0].replace('#',"").replace('#',"")+")\"><strong>";
			listeAuteurs += elementAuteur[1].replace('#',"").replace('#',"");
			listeAuteurs += "</strong> "+elementAuteur[2].replace('#',"").replace('#',"");
			listeAuteurs += " ("+elementAuteur[3].replace('#',"").replace('#',"");
			if (elementAuteur[4].replace('#',"").replace('#',"") != "")
				listeAuteurs += " - "+elementAuteur[4].replace('#',"").replace('#',"");
			listeAuteurs += ")</a></li>";
			x++;
		}
		listeAuteurs += "</ul>";

		document.getElementById("listeAuteurs").innerHTML = listeAuteurs;
	}
	else
		document.getElementById("listeAuteurs").innerHTML = "<br><br>Aucun auteur n'a �t� trouv�.";
}

function listerCartesPostales(xhr, action)
{
	var chaine = xhr.responseText;
	var tableau = chaine.split('||');

	if (tableau.length >= 2)
	{
		var listeCartesPostales = "<ul>";
		var x = 0;
		while (tableau[x] != "")
		{
			var elementAuteur = tableau[x].split('|');

			if (action == "m")
				listeCartesPostales += "<li style = 'margin: 10px; cursor: pointer;' onclick = \"submitForm('GET', 'ajax.php', 'M_cartesPostalesADMIN', ";
			else
				listeCartesPostales += "<li style = 'margin: 10px; cursor: pointer;' onclick = \"submitForm('GET', 'ajax.php', 'S_cartesPostalesADMIN', ";
			listeCartesPostales += elementAuteur[0].replace('#',"").replace('#',"")+")\"><strong>";
			listeCartesPostales += elementAuteur[1].replace('#',"").replace('#',"");
			listeCartesPostales += "</strong></li>";
			x++;
		}
		listeCartesPostales += "</ul>";

		document.getElementById("listeCartesPostales").innerHTML = listeCartesPostales;
	}
	else
		document.getElementById("listeCartesPostales").innerHTML = "<br><br>Aucune carte postale n'a �t� trouv�.";
}