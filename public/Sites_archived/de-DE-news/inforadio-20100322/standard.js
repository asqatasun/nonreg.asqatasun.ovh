/*
* RBB inforadio
* standard.js
* 
* Author: Tino Urbiks, Holger M. Stangl
* Date: Juli 2008
*/

jQuery.noConflict();

jQuery(document).ready(function () {
	jQuery('a').attr("onfocus", "if(this.blur)this.blur()");
	jQuery.noConflict();
});


function navigateToHandle(selectId) 
{
  var sel = document.getElementById(selectId);
  var url = sel.options[sel.selectedIndex].value;
  if (url && url != "-") {
	if (url.indexOf("http:") == -1 && url.indexOf("https:") == -1) {
		url = externalize(url) + ".html";;
	}
	window.location.href = url;
  }
}


function openWindow(placement,url,name,width,height)
{
    
	if (placement=='top')
    {
    	var x=(screen.width - screen.width);
   		var y=(screen.height - screen.height);
    }
    else if (placement=='center')
    {
    	var x=((screen.width - width) / 2);
    	var y=((screen.height - height) /2);
    }
    
	var popupWindow = window.open(url,name,'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width='+width+',height='+height+',left='+x+',top='+y);
	popupWindow.focus();
	
}

function zoomIn( id, area, smallSrc, bigSrc ) 
{
	document.getElementById('zoomTeaserImg'+id).src = bigSrc;
	
	if (BrowserDetect.browser=="Explorer" && BrowserDetect.version=="6")
		{
			document.getElementById('zoomTeaserImgBox'+id).style.width = "39em";
		} else {
			document.getElementById('zoomTeaserImgBox'+id).style.width = "40em";
		}
	
		
	if (area == "imageBoxA" || area == "imageBoxC")
	{

		if (BrowserDetect.browser=="Explorer" && BrowserDetect.version=="6")
		{
			document.getElementById('zoomTeaserImg'+id).style.width = "39em";
		} else {
			document.getElementById('zoomTeaserImg'+id).style.width = "40em";
		}

	
		if (BrowserDetect.browser=="Explorer" && BrowserDetect.version=="6")
		{
			document.getElementById('zoomTeaser'+id).style.width = "39em";
		} else {
			document.getElementById('zoomTeaser'+id).style.width = "40em";
		}
		
		
	} 
	else if (area == "imageBoxB")
	{
		document.getElementById('zoomTeaserImg'+id).style.width = "32em";
		document.getElementById('zoomTeaser'+id).style.width = "42em";
	}
	
	if (area == "imageBoxA" || area == "imageBoxB" || area == "imageBoxC")
	{
		document.getElementById('zoomTeaserImgBox'+id).style.float = "none";
		
		if (BrowserDetect.browser=="Explorer" && BrowserDetect.version=="6")
		{
			document.getElementById('zoomTeaser'+id).style.width = "39em";
		} 
		//document.getElementById('zoomTeaserTextBox'+id).style.height = "auto";
		//document.getElementById('zoomTeaserTextBox'+id).style.float = "none";
		//document.getElementById('zoomTeaserText'+id).style.position = "relative";
		//document.getElementById('zoomTeaserText'+id).style.bottom = "0";
	}
	
	document.getElementById('zoomTeaserLink'+id).style.backgroundImage = "url(" + externalize("/grafik/icons/icon_minus.gif") + ")";
	document.getElementById('zoomTeaserLink'+id).href = "javascript:zoomOut(" + id + ", '" + area + "', '" + smallSrc + "', '" + bigSrc + "');";
	
}


function zoomOut( id, area, smallSrc, bigSrc ) 
{

	document.getElementById('zoomTeaserImg'+id).src = smallSrc;
	
	if (area == "imageBoxA")
	{
		document.getElementById('zoomTeaserImgBox'+id).style.float = "left";
	}
	
	if (area == "imageBoxB")
	{
		document.getElementById('zoomTeaserImgBox'+id).style.float = "none";
		document.getElementById('zoomTeaserImg'+id).style.float = "left";
		//document.getElementById('zoomContainer'+id).style.display = "block";
		document.getElementById('zoomContainer'+id).style.position = "absolute";
		//document.getElementById('zoomContainer'+id).style.float = "right";
		//document.getElementById('zoomTeaserLink'+id).style.display = "block";
		document.getElementById('zoomTeaserLink'+id).style.margin = "0 0 0 37.8em";
	}
	
	if (area == "imageBoxC")
	{
		document.getElementById('zoomTeaserImgBox'+id).style.float = "none";
	}
	
	if (area == "imageBoxA")
	{
		document.getElementById('zoomTeaserImg'+id).style.width = "16em";
		document.getElementById('zoomTeaserImgBox'+id).style.width = "16em";
		document.getElementById('zoomTeaser'+id).style.width = "16em";
	}
	
	if (area == "imageBoxB")
	{
		document.getElementById('zoomTeaserImg'+id).style.width = "16em";
		document.getElementById('zoomTeaserImgBox'+id).style.width = "16em";
		document.getElementById('zoomTeaser'+id).style.width = "42em";
	}
	
	if (area == "imageBoxC")
	{
		document.getElementById('zoomTeaserImg'+id).style.width = "12em";
		document.getElementById('zoomTeaserImgBox'+id).style.width = "12em";
		document.getElementById('zoomTeaser'+id).style.width = "12em";
	}
	
	document.getElementById('zoomTeaserLink'+id).style.backgroundImage = "url(" + externalize("/grafik/icons/icon_plus.gif") + ")";
	document.getElementById('zoomTeaserLink'+id).href = "javascript:zoomIn(" + id + ", '" + area + "', '" + smallSrc + "', '" + bigSrc + "');";

}


var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari"
		},
		{
			prop: window.opera,
			identity: "Opera"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]

};
BrowserDetect.init();








