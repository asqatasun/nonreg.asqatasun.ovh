// nuggAd variables

var n_pbt = "";
var p_gwp1 = "";
var p_gwp2 = "";
var p_gwp3 = "";
var p_gwp4 = "";
var p_gwp5 = "";

// used as a "global" random variable for AdBanner
var rdo = Math.random()*100000000000000000;

/*AJAX Logic*/

/* timeout in ms, action as function with target as param */
var gReq;
var gAction;
function rfu(url,target,timeout,action){

	var httpReq = getHttpObj(); 
	httpReq.onreadystatechange = function() {procReq(target);};
 
 	httpReq.open("GET", url); 
 	httpReq.send(false);
	gReq = httpReq;


	if (timeout)
	{
		gAction= action+"('"+target+"')";	
		var rfuTimeout=setTimeout("callTimeout(gReq);",timeout);
	}		

 
	function procReq(target){
  	if (httpReq.readyState == 4){
   		if (httpReq.status == 200){
			if (timeout) {	
				clearTimeout(rfuTimeout); 
			}	
				var cnt = httpReq.responseText;
				cnt = cnt.replace('id="'+target+'"','id="_'+target+'_"');
		 		document.getElementById(target).innerHTML = cnt;
		 		execJS(document.getElementById(target));
   		} 
   		else{ 
   			document.getElementById(target).innerHTML="Error: " + httpReq.statusText;
			} 
		} 
	}
} 

function getHttpObj(){
	var httpObj= false;
	try{
		httpObj = new ActiveXObject('Msxml2.XMLHTTP');
	} 
 	catch(e){
 		try{
 			httpObj = new ActiveXObject('Microsoft.XMLHTTP');
 		}
 		catch(e){
 			httpObj = new XMLHttpRequest();
 		}
 	}
 	return httpObj;
 }

function callTimeout(h)
{
   h.abort();
   try { eval(gAction); } catch(e) { ; }
} 

function execJS(node) {
 /* Element auf Javascript �berpr�fen, und falls n�tig ausf�hren */
 var bSaf = (navigator.userAgent.indexOf('Safari') != -1);
 var bOpera = (navigator.userAgent.indexOf('Opera') != -1);
 var bMoz = (navigator.appName == 'Netscape');
 var st = node.getElementsByTagName('script'); var strExec;
   
	for(var i=0;i<st.length; i++){ 
		if (bSaf){ 
			strExec = st[i].innerHTML; 
		} 
		else if (bOpera){
			strExec = st[i].text; 
		} 
		else if (bMoz){ 
			strExec = st[i].textContent; 
		} 
		else{ 
			strExec = st[i].text; 
		} 
		try{
			eval(strExec);
		} 
		catch(e){
			err=1;/*alert(e);*/
		}
	}
}
/* END New Ajax Logic*/		

var loadingImage = new Image();
loadingImage.src =window.location.protocol + '//' + window.location.hostname + '/' + 'images/icon/loadingchart.gif';
function changeMiniChart(obj,days,elemid,path,width,height,symbol,template)
{

    var exp=300;
    if(days=='1')
        exp='300';
    else
        exp='3600';
    
    var chartgif = document.getElementById('wp1_bmchart'+elemid);
    var basesrc=path+'?exp='+exp+ '&cmd=0&o=h&tmpl='+template+'&ct=99&w='+width+'&h='+height+'&aa=0&sl=0&sym='+symbol+'&bc=';

	chartgif.src = loadingImage.src;
	var chartImage = new Image();
	chartImage.onload = function() { chartgif.src = chartImage.src;  } 
	chartImage.src = basesrc + days;	

    if (obj != 'null')
    {
    
	    ulObj = obj.parentNode.parentNode;
	    for(i=0;i<ulObj.getElementsByTagName('li').length;i++) {
		    if(ulObj.getElementsByTagName('li')[i] != obj.parentNode) {
			    ulObj.getElementsByTagName('li')[i].className = "";
		    } else {
			    obj.parentNode.className = "active";
		    }
	    }
    }
    return;
}

function setAnalyserImage(obj, objNum,elemid,symbol,path)
{
    var objAI = eval("document.analyserImg"+elemid);
    var src = path+'form=';
	
    switch (objNum)
    {
        case 0:
            objAI.src = src + 'analyserrecommendations31s&w=226&h=71&sym='+symbol;
            break;
        case 1:
            objAI.src = src + 'analyserpricetarget31s&w=226&h=71&sym='+symbol;
            break;
        case 2:
            objAI.src = src + 'analyseraverage31s&w=226&h=71&sym='+symbol;
            break;
    }
    
    if (obj != 'null')
    {
    
	    ulObj = obj.parentNode.parentNode;
	    for(i=0;i<ulObj.getElementsByTagName('li').length;i++) {
		    if(ulObj.getElementsByTagName('li')[i] != obj.parentNode) {
			    ulObj.getElementsByTagName('li')[i].className = "";
		    } else {
			    obj.parentNode.className = "active";
		    }
	    }
    }
    
}

/* EOLAS - Problematik -- Start */
function hbcomToObject(id) {
	if(document.layers){
		return (document.layers[id])?eval(document.layers[id]):null;
	}
	else if(document.all && !document.getElementById){
		return (eval("window."+id))?eval("window."+id):null;
	}
	else if(document.getElementById && document.body.style) {
		return (document.getElementById(id))?eval(document.getElementById(id)):null;
	}
}
  
function hbcomFlashWrite(string){
  document.write(string);
  }

function hbcomFlashInnerHTML(htmlElementId,code){
  var x=hbcomToObject(htmlElementId);
  if(x){ 
    if(document.getElementById||document.all){
      x.innerHTML='';
      x.innerHTML=code;
      }
    else if(document.layers){
      x.document.open();
      x.document.write(code);
      x.document.close();
      }
    }
  }

/* EOLAS - Problematik -- End */


/* Cookies -- Start */
function setCookie(name,value,expiry,path,domain,secure) {
   var nameString = name + "=" + value;
   var expiryString = (expiry == null) ? "" : "; expires=" + expiry.toGMTString();
   var pathString = (path == null) ? "" : "; path=" + path;
   var domainString = (path == null) ? "" : "; domain=" + domain;
   var secureString = (secure) ? "; secure" : "";
   document.cookie = nameString + expiryString + pathString + domainString + secureString;
}

function getCookie(name) {
   var cookieFound = false;
   var start = 0;
   var end = 0;
   var cookieString = document.cookie;
   var i = 0;
   while (i <= cookieString.length) {
      start = i;
      end = start + name.length;
      if (cookieString.substring(start,end) == name)
	  {
         cookieFound = true;
         break;
      }
      i++;
   }

   if (cookieFound) {
      	if( cookieString.substring(end,end+1) == "=")
	{
      		start = end + 1;
      		end = cookieString.indexOf(";",start);
      		if (end < start)
         		end = cookieString.length;
      		return unescape(cookieString.substring(start,end));
	}
	else
		return "";
   }
   return "";
}

function deleteCookie(name) {
   var expires = new Date();
   expires.setTime (expires.getTime() - 36*3600*365*1000);
   setCookie(name,"Delete Cookie",expires,"/",document.domain,false);
}

function CheckCookie( )
{
	var cookieName = 'Cookietest';
	document.cookie = cookieName + '=cookieValue';
	var cookiesEnabled = document.cookie.indexOf(cookieName) != -1;
	if (cookiesEnabled)
	{
		deleteCookie('CookieTest');
		return true;
	}
   	else
		return false;
}
/* Cookies -- End */

// Random Teaser Functions (Sandwiches) -- Start

var isRandomTeaserGenerated = 0;
var currentTeaserPosition = 0;
var rAll = new Array();

function GenerateRandoms(rMax)
{

	var rCurrent = 0;
	var isFound = 0;
	for (var i = 0; i < rAll.length; ++i)
		rAll[i] = -1;
		
	for (var i=0; i<rMax; ++i)
	{
		do
		{
			rCurrent = Math.round(Math.random()*1000) % rMax + 1;
		
			isFound = 0;
			for (var j=0; j<i; ++j)
			{
				if ( rAll[j] == rCurrent )
				{
					isFound = 1;
					break;
				}
			}
		}
		while (isFound == 1)
		
		rAll[i] = rCurrent;
	}	
	
		
	isRandomTeaserGenerated = 1;
}

function getCurrentTeaserRandom()
{
	if (!isNaN(rAll[currentTeaserPosition])) 
		return rAll[currentTeaserPosition++];
	else
		return currentTeaserPosition++ +1;	
}	

function fillTeaserAds ()
{
	var i=0;
	while (document.getElementsByTagName("DiV")[i])
	{
		if (document.getElementsByTagName("DiV")[i].id.substr(0,8) == 'teaserAd' &&
			document.getElementsByTagName("DiV")[i].innerHTML.substr(0,11) == '<!--http://')
		{	
			rfu('tunnel.aspx?req='+escape(document.getElementsByTagName("DiV")[i].innerHTML.substr(4,document.getElementsByTagName("DiV")[i].innerHTML.length-7)),document.getElementsByTagName("DiV")[i].id);
			// rfu(document.getElementsByTagName("DiV")[i].innerHTML.substr(4,document.getElementsByTagName("DiV")[i].innerHTML.length-7),document.getElementsByTagName("DiV")[i].id);
		}
		i++;
	}
}		

// Random Teaser Functions (Sandwiches) -- End

/* Begin new cnTicker */
function cnTickerOpenDetailView(Id) 
{
  if(document.getElementById(Id)!=null)
  {
	  if(document.getElementById(Id).style.display == "block")
		  document.getElementById(Id).style.display = "none";
	  else
	  {
		  document.getElementById(Id).style.display = "block";
/*		  
			var i = 0;
			while (document.getElementsByTagName("div")[i])
			{
				if (document.getElementsByTagName("div")[i].id.substr(0,15) == 'cntickercontent' && document.getElementsByTagName("div")[i].id != document.getElementById(Id).id)
				{
					if (document.getElementById(document.getElementsByTagName("div")[i].id).style.display == "block")
					{
						document.getElementById(document.getElementsByTagName("div")[i].id).style.display = "none";
					}						
				}
				i++;
			}  
*/			
		}		  
  }
  
  return false;
}

function cnTickerChangeDate(date,t,p,res)
{
    if(window.location.href.lastIndexOf("/")>-1)
    {
			nextref = window.location.href.substring(0,window.location.href.lastIndexOf("/")) + "/_p=" + p;
			if (date != "" && date != "0") nextref += ",date=" + date;
      if (res != "")  nextref += ",ressort=" + res;
      window.location.href = nextref;
    }
}
/* Ende new cnTicker */

/* Init ACTION variable to track on-Click-Actions with IndexTools */
var ACTION='';

// check if a variable is defined
// e.g.
// alert(isVarDefined("demo")) // false
// var demo = "somevalue";
// alert(isVarDefined("demo")) // true

var isVarDefined = function(variable){ 
	return this[variable] == undefined ? false : true;
};

function _s_action(c)
{
		if (typeof YWA != 'undefined')
		{
			if (typeof YWATracker == 'undefined') { var YWATracker = YWA.getTracker("10001602988643"); }
			YWATracker.setAction(c);
			YWATracker.submit();
		}
		return false;		
}

// will be user in "rubrikenteaser" to ajax some titles down

function setTeaserMultiplier(id, num)
{
	var i = 1;
	while(document.getElementById('f' + id + i) != null){
		if (i == num) document.getElementById('f' + id + i).className = i == 1 ? 'first active' : 'active';
		else document.getElementById('f' + id + i).className = '';
		i++;
	}
}
