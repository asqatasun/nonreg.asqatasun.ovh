
$(document).ready(function(){	
	if($("#mbpProgramSelector") != null){		
		 // pull current structure ID part from current URL
	    var file = jQuery.url.attr("file");
	    
	    // change all selection list links to use the struct ID
	    $("#mbpProgramSelector option").each(function(idx){
	           var url = $(this).attr("value");
	           var baseUrl = url.slice(0, url.indexOf("0,,"));
	           url = baseUrl + file;
	           $(this).attr("value", url);
	    });

	    
	    var schedule_id = $("#scheduleIdentifier").text();
	    
	    // detect artificial DW-TV AFRICA region and select 
	    // the appropriate dropdown box item 	   
	    if(schedule_id != null && schedule_id == "10202"){
	        $("#mbpProgramSelector option:first-child").attr("selected", "");
	        $("#mbpProgramSelector option:last-child").attr("selected", "selected");            
	    }  
	    
	    // inject region id into episode links, but only for non-default schedule
	    if(schedule_id != null && schedule_id != ""){ 

	        //schedule_id = schedule_id.slice(1, schedule_id.length) + "/";
	    	schedule_id += "/";
	    	
	        var ep_url = $(".episodField a").attr("href");
	        var ep_path = jQuery.url.setUrl(ep_url).attr("directory");
	        var ep_file = jQuery.url.setUrl(ep_url).attr("file");

	        $(".episodField a").attr("href", ep_path + schedule_id + ep_file);

	        ep_url = $(".episodField2 a").attr("href");
	        ep_path = jQuery.url.setUrl(ep_url).attr("directory");
	        ep_file = jQuery.url.setUrl(ep_url).attr("file");

	        $(".episodField2 a").attr("href", ep_path + schedule_id + ep_file);      
	    }	     		
	}
});