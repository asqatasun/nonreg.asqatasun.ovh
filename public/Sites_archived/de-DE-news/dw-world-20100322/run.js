var RUN=new Object();
var firstParam = true;

function _runVisDat(){
	var dCurrent=new Date();
	RUN.tz=dCurrent.getTimezoneOffset()/60*-1;
	if (RUN.tz==0){
		RUN.tz="0";
	}
	RUN.bh=dCurrent.getHours();
	RUN.ul=navigator.appName=="Netscape"?navigator.language:navigator.userLanguage;
	if (typeof(screen)=="object"){
		RUN.cd=screen.colorDepth;
		RUN.sr=screen.width+"x"+screen.height;
	}
	if (typeof(navigator.javaEnabled())=="boolean"){
		RUN.jo=navigator.javaEnabled()?"Yes":"No";
	}
	RUN.js="Yes";
	if (typeof(gVersion)!="undefined"){
		RUN.jv=gVersion;
	}
	RUN.dat=dCurrent.getTime();
}

var DW=new Object();

function _runDwDat(){
	var dCurrent=new Date();
	//Zusaetzlicher Parameter fuer tatsaechlich verwendete Domain
	DW.dom=window.location.hostname;
	DW.uri=window.location.pathname;
	if (window.location.search){
		DW.qry=window.location.search;
	}
	if ((window.document.referrer!="")&&(window.document.referrer!="-")){
		if (!(navigator.appName=="Microsoft Internet Explorer"&&parseInt(navigator.appVersion)<4)){
			DW.ref=window.document.referrer;
		}
	}

	
	var macaParam = null;
	var queryString = location.href;
	if (queryString && (queryString.indexOf("?") >= 0)) {
		queryString = queryString.substring(queryString.indexOf("?")+1);
		queryString = queryString.split("&");
		var i;
		for ( i = 0; i < queryString.length; i++ ) {
			var param = queryString [ i ].split ("=");
			if (param[0] == "maca") {
				macaParam = param[1];
				break;
			}
		}
	}
	if (macaParam) {
		DW.maca = macaParam;
	}
}

function _runAdd(N,V){
	if (firstParam) {
		firstParam = false;
		return N+"="+_runEsc(V);
	}
	return "&"+N+"="+_runEsc(V);
}

function _runEsc(S){
	if (typeof(RE)!="undefined"){
		var retStr = new String(S);
		for (R in RE){
			retStr = retStr.replace(RE[R],R);
		}
		return retStr;
	}
	else{
		return S;
	}
}

var _runImage=new Array;
var _runIndex=0;
function _runCreateImage(dcsSrc){
	if (document.images){
		_runImage[_runIndex]=new Image;
		_runImage[_runIndex].src=dcsSrc;
		_runIndex++;
	}
	else{
		document.write('<IMG BORDER="0" NAME="RUN" WIDTH="1" HEIGHT="1" SRC="'+dcsSrc+'">');
	}
}

function _runMeta(){
	var myDocumentElements;
	if (document.all){
		myDocumentElements=document.all.tags("meta");
	}
	else if (document.documentElement){
		myDocumentElements=document.getElementsByTagName("meta");
	}
	if (typeof(myDocumentElements)!="undefined"){
		for (var i=1;i<=myDocumentElements.length;i++){
			myMeta=myDocumentElements.item(i-1);
			if (myMeta.name){
				if (myMeta.name.indexOf('RUN.')==0){
					RUN[myMeta.name.substring(3)]=myMeta.content;
				} else if (myMeta.name.indexOf('DW.')==0){
					DW[myMeta.name.substring(3)]=myMeta.content;
				}
			}
		}
	}
}

function _runTag(){
	var dCurrent = new Date();
	var P="http"+(window.location.protocol.indexOf('https:')==0?'s':'')+"://zap.dw-world.de/run/" + dCurrent.getTime() + "/run.dw?";
	for (N in RUN){
		if (RUN[N]) {
			P+=_runAdd("RUN."+N,RUN[N]);
		}
	}
	for (N in DW){
		if (DW[N]) {
			P+=_runAdd("DW."+N,DW[N]);
		}
	}
	if (P.length>2048&&navigator.userAgent.indexOf('MSIE')>=0){
		P=P.substring(0,2040)+"&RUN.tu=1";
	}
	_runCreateImage(P);
}

_runVisDat();
_runDwDat();
_runMeta();
_runTag();
//-->
