Sitemap: http://www.stern.de/sitemap/
Sitemap: http://www.stern.de/sitemap-news/
Sitemap: http://www.stern.de/sitemap-image/
User-agent: *
Disallow: /static/
Disallow: /syndication/
Disallow: /archiv/
Disallow: /sport/archiv/
Disallow: /kultur/archiv/
Disallow: /politik/archiv/
Disallow: /panorama/archiv/
Disallow: /wirtschaft/archiv/
Disallow: /auto/archiv/
Disallow: /gesundheit/archiv/
Disallow: /lifestyle/archiv/
Disallow: /digital/archiv/
Disallow: /wissen/archiv/
Disallow: /reise/archiv/
Disallow: /fotografie/archiv/
Disallow: /computer-technik/archiv/
Disallow: /wissenschaft/archiv/
Disallow: /*?pr
Disallow: /*?backref
Disallow: /*?mode=comment
Disallow: /*?rendermode=comment
Disallow: /*?url=
Disallow: /*?rendererSet=
Disallow: /*?art_id=
Disallow: /*?title=
Disallow: /archiv_*