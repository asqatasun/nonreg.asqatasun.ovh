// #######################################################################################
// Teaser Meinung/Kolumne

/**
 * Constructor. Initialize a new tabbed teaser.
 * 
 * @param  string  listId  Id of list which holds the tabbed teaser
 * @param  string  activeElement  Id of element witch is the initial one
 */
function szTabbedTeaserHeaderTempFinanzen(listId, activeElement) {
    this.listId = listId;
    this.activeElement = activeElement;
    this.ok = false;
    if(!document.getElementById || !document.createTextNode){
        return;
    }
    
    var elem = document.getElementById(this.listId);
    if(!elem){ return; }

    this.list = elem.getElementsByTagName("li");
    if(!this.list){ return; }
    
    this.ok = true;
}


/**
 * Switch to tab based on passed key.
 * 
 * @param  string  key  Key of element to activate
 */
szTabbedTeaserHeaderTempFinanzen.prototype.show = function(key){
    if(!this.ok){
        return;
    }

    if (key == this.activeElement) {
        // passed key is allready active, get out here
        return;
    }
    
    if (this.activeElement !== "") {
        // hide previous displayed element
        document.getElementById(this.activeElement).style.display = "none";
    }

    // set active key, display new element and change classname
    // formatting definitions are set by different class selectors 
    this.activeElement = key;
    document.getElementById(this.activeElement).style.display = "block";

    // reset other list item
    for (var i=0; i < this.list.length; i++) {
        if (this.list[i].className == "firstActive") {
            this.list[i].className = "first";
        } else if (this.list[i].className == "middleActive") {
            this.list[i].className = "middle";
        } else if (this.list[i].className == "lastActive") {
            this.list[i].className = "last";
        }
    }

    // activate actual list item
    var item = document.getElementById(key+"Item");
    if (item.className == "first") {
        item.className = "firstActive";
    } else if (item.className == "middle") {
        item.className = "middleActive";
    } else if (item.className == "last") {
        item.className = "lastActive";
    }

}

// END Teaser Meinung/Kolumne
// #######################################################################################
