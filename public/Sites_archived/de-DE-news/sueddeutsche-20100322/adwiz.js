//  AdWiz Version 1.50             Programmed by Leon Becker
//  Copyright 2005 by Smartshop Strategies T. Sz�sz (Berlin)

function adwiz_popup (adwiz_id,obj) {
	if (adwiz_id) {
		var popupDir="http://www.sueddeutsche.de/app/szbeilagen/nas/"+obj+"/popup/";
		var overview="http://www.sueddeutsche.de/app/szbeilagen/"+obj+"/teaser/index.html";
		if (adwiz_id=="all") location.href=overview;
		else if (adwiz_id.length==4) location.href=overview+"#"+adwiz_id;
		else {
			switch (adwiz_id.substr(0,1).toUpperCase()) {
				case "A": var popWidth=800;	var popHeight=700; break;
				case "B": var popWidth=940;	var popHeight=700; break;
				case "S": var popWidth=940;	var popHeight=700; break;
				default:  var popWidth=940;	var popHeight=700;
			}
			if ((popWidth>screen.availWidth)||(popHeight>screen.availHeight)) {
				var adwizWin=window.open(popupDir+adwiz_id+"/index.php","","dependent=yes,resizable=yes,scrollbars=yes");
			}
			else {
				var xPos=Math.floor((screen.availWidth-popWidth)/2);
				var yPos=Math.floor((screen.availHeight-40-popHeight)/2);
				if (xPos<0) xPos=0;
				if (yPos<0) yPos=0;
				var adwizWin=window.open(popupDir+adwiz_id+"/index.php","","width="+popWidth+",height="+popHeight+",left="+xPos+",top="+yPos+",dependent=yes");
			}
			if (!adwizWin) window.alert("Das Fenster kann nicht dargestellt werden.\n\nWahrscheinlich sind Popup-Fenstern in den Voreinstellungen Ihres Browsers abgeschaltet oder Sie haben einen Popup-Blocker installiert.");
			else adwizWin.focus();
		}
	}
}

function adwiz_rotate (teaser, maxTeasers, type, timeout) {
	var skip=(typeof(adwiz_mouseflag)=="number") ? adwiz_mouseflag : 0;
	if (!skip) {
		if ((teaser)&&(document.getElementById("adwiz_"+type+teaser)))
			document.getElementById("adwiz_"+type+teaser).style.visibility="hidden";
		teaser=(teaser==maxTeasers) ? 1 : ++teaser;
		document.getElementById("adwiz_"+type+teaser).style.visibility="visible";
		if (type=="txt") adwiz_currentPage=teaser;
	}
	adwiz_timeout=window.setTimeout("adwiz_rotate("+teaser+","+maxTeasers+",'"+type+"',"+timeout+")",timeout);
}

function adwiz_init_paging (maxPages) {
	if (maxPages) {
		adwiz_maxPages=maxPages;
		document.getElementById("adwiz_arrow_prev").firstChild.nodeValue=document.getElementById("adwiz_tpl_prev").firstChild.nodeValue;
		document.getElementById("adwiz_arrow_next").firstChild.nodeValue=document.getElementById("adwiz_tpl_next").firstChild.nodeValue;
	}
	else adwiz_maxPages=0;
	adwiz_currentPage=1;
}

function adwiz_page (dir) {
	if ((adwiz_maxPages)&&(typeof(adwiz_currentPage)=="number")) {
		if (typeof(adwiz_timeout)=="number") clearTimeout(adwiz_timeout);
		document.getElementById("adwiz_txt"+adwiz_currentPage).style.visibility="hidden";
		if (dir=="prev") {
			if (--adwiz_currentPage==0) adwiz_currentPage=adwiz_maxPages;
		}
		else {
			if (++adwiz_currentPage>adwiz_maxPages) adwiz_currentPage=1;
		}
		document.getElementById("adwiz_txt"+adwiz_currentPage).style.visibility="visible";
	}
}
