/* ---- prototype ---- */
/*  Prototype JavaScript framework, version 1.6.0
 *  (c) 2005-2007 Sam Stephenson
 *
 *  Prototype is freely distributable under the terms of an MIT-style license.
 *  For details, see the Prototype web site: http://www.prototypejs.org/
 *
 *--------------------------------------------------------------------------*/

var Prototype = {
  Version: '1.6.0',

  Browser: {
    IE:     !!(window.attachEvent && !window.opera),
    Opera:  !!window.opera,
    WebKit: navigator.userAgent.indexOf('AppleWebKit/') > -1,
    Gecko:  navigator.userAgent.indexOf('Gecko') > -1 && navigator.userAgent.indexOf('KHTML') == -1,
    MobileSafari: !!navigator.userAgent.match(/Apple.*Mobile.*Safari/)
  },

  BrowserFeatures: {
    XPath: !!document.evaluate,
    ElementExtensions: !!window.HTMLElement,
    SpecificElementExtensions:
      document.createElement('div').__proto__ &&
      document.createElement('div').__proto__ !==
        document.createElement('form').__proto__
  },

  ScriptFragment: '<script[^>]*>([\\S\\s]*?)<\/script>',
  JSONFilter: /^\/\*-secure-([\s\S]*)\*\/\s*$/,

  emptyFunction: function() { },
  K: function(x) { return x }
};

if (Prototype.Browser.MobileSafari)
  Prototype.BrowserFeatures.SpecificElementExtensions = false;

if (Prototype.Browser.WebKit)
  Prototype.BrowserFeatures.XPath = false;

/* Based on Alex Arnell's inheritance implementation. */
var Class = {
  create: function() {
    var parent = null, properties = $A(arguments);
    if (Object.isFunction(properties[0]))
      parent = properties.shift();

    function klass() {
      this.initialize.apply(this, arguments);
    }

    Object.extend(klass, Class.Methods);
    klass.superclass = parent;
    klass.subclasses = [];

    if (parent) {
      var subclass = function() { };
      subclass.prototype = parent.prototype;
      klass.prototype = new subclass;
      parent.subclasses.push(klass);
    }

    for (var i = 0; i < properties.length; i++)
      klass.addMethods(properties[i]);

    if (!klass.prototype.initialize)
      klass.prototype.initialize = Prototype.emptyFunction;

    klass.prototype.constructor = klass;

    return klass;
  }
};

Class.Methods = {
  addMethods: function(source) {
    var ancestor   = this.superclass && this.superclass.prototype;
    var properties = Object.keys(source);

    if (!Object.keys({ toString: true }).length)
      properties.push("toString", "valueOf");

    for (var i = 0, length = properties.length; i < length; i++) {
      var property = properties[i], value = source[property];
      if (ancestor && Object.isFunction(value) &&
          value.argumentNames().first() == "$super") {
        var method = value, value = Object.extend((function(m) {
          return function() { return ancestor[m].apply(this, arguments) };
        })(property).wrap(method), {
          valueOf:  function() { return method },
          toString: function() { return method.toString() }
        });
      }
      this.prototype[property] = value;
    }

    return this;
  }
};

var Abstract = { };

Object.extend = function(destination, source) {
  for (var property in source)
    destination[property] = source[property];
  return destination;
};

Object.extend(Object, {
  inspect: function(object) {
    try {
      if (object === undefined) return 'undefined';
      if (object === null) return 'null';
      return object.inspect ? object.inspect() : object.toString();
    } catch (e) {
      if (e instanceof RangeError) return '...';
      throw e;
    }
  },

  toJSON: function(object) {
    var type = typeof object;
    switch (type) {
      case 'undefined':
      case 'function':
      case 'unknown': return;
      case 'boolean': return object.toString();
    }

    if (object === null) return 'null';
    if (object.toJSON) return object.toJSON();
    if (Object.isElement(object)) return;

    var results = [];
    for (var property in object) {
      var value = Object.toJSON(object[property]);
      if (value !== undefined)
        results.push(property.toJSON() + ': ' + value);
    }

    return '{' + results.join(', ') + '}';
  },

  toQueryString: function(object) {
    return $H(object).toQueryString();
  },

  toHTML: function(object) {
    return object && object.toHTML ? object.toHTML() : String.interpret(object);
  },

  keys: function(object) {
    var keys = [];
    for (var property in object)
      keys.push(property);
    return keys;
  },

  values: function(object) {
    var values = [];
    for (var property in object)
      values.push(object[property]);
    return values;
  },

  clone: function(object) {
    return Object.extend({ }, object);
  },

  isElement: function(object) {
    return object && object.nodeType == 1;
  },

  isArray: function(object) {
    return object && object.constructor === Array;
  },

  isHash: function(object) {
    return object instanceof Hash;
  },

  isFunction: function(object) {
    return typeof object == "function";
  },

  isString: function(object) {
    return typeof object == "string";
  },

  isNumber: function(object) {
    return typeof object == "number";
  },

  isUndefined: function(object) {
    return typeof object == "undefined";
  }
});

Object.extend(Function.prototype, {
  argumentNames: function() {
    var names = this.toString().match(/^[\s\(]*function[^(]*\((.*?)\)/)[1].split(",").invoke("strip");
    return names.length == 1 && !names[0] ? [] : names;
  },

  bind: function() {
    if (arguments.length < 2 && arguments[0] === undefined) return this;
    var __method = this, args = $A(arguments), object = args.shift();
    return function() {
      return __method.apply(object, args.concat($A(arguments)));
    }
  },

  bindAsEventListener: function() {
    var __method = this, args = $A(arguments), object = args.shift();
    return function(event) {
      return __method.apply(object, [event || window.event].concat(args));
    }
  },

  curry: function() {
    if (!arguments.length) return this;
    var __method = this, args = $A(arguments);
    return function() {
      return __method.apply(this, args.concat($A(arguments)));
    }
  },

  delay: function() {
    var __method = this, args = $A(arguments), timeout = args.shift() * 1000;
    return window.setTimeout(function() {
      return __method.apply(__method, args);
    }, timeout);
  },

  wrap: function(wrapper) {
    var __method = this;
    return function() {
      return wrapper.apply(this, [__method.bind(this)].concat($A(arguments)));
    }
  },

  methodize: function() {
    if (this._methodized) return this._methodized;
    var __method = this;
    return this._methodized = function() {
      return __method.apply(null, [this].concat($A(arguments)));
    };
  }
});

Function.prototype.defer = Function.prototype.delay.curry(0.01);

Date.prototype.toJSON = function() {
  return '"' + this.getUTCFullYear() + '-' +
    (this.getUTCMonth() + 1).toPaddedString(2) + '-' +
    this.getUTCDate().toPaddedString(2) + 'T' +
    this.getUTCHours().toPaddedString(2) + ':' +
    this.getUTCMinutes().toPaddedString(2) + ':' +
    this.getUTCSeconds().toPaddedString(2) + 'Z"';
};

var Try = {
  these: function() {
    var returnValue;

    for (var i = 0, length = arguments.length; i < length; i++) {
      var lambda = arguments[i];
      try {
        returnValue = lambda();
        break;
      } catch (e) { }
    }

    return returnValue;
  }
};

RegExp.prototype.match = RegExp.prototype.test;

RegExp.escape = function(str) {
  return String(str).replace(/([.*+?^=!:${}()|[\]\/\\])/g, '\\$1');
};

/*--------------------------------------------------------------------------*/

var PeriodicalExecuter = Class.create({
  initialize: function(callback, frequency) {
    this.callback = callback;
    this.frequency = frequency;
    this.currentlyExecuting = false;

    this.registerCallback();
  },

  registerCallback: function() {
    this.timer = setInterval(this.onTimerEvent.bind(this), this.frequency * 1000);
  },

  execute: function() {
    this.callback(this);
  },

  stop: function() {
    if (!this.timer) return;
    clearInterval(this.timer);
    this.timer = null;
  },

  onTimerEvent: function() {
    if (!this.currentlyExecuting) {
      try {
        this.currentlyExecuting = true;
        this.execute();
      } finally {
        this.currentlyExecuting = false;
      }
    }
  }
});
Object.extend(String, {
  interpret: function(value) {
    return value == null ? '' : String(value);
  },
  specialChar: {
    '\b': '\\b',
    '\t': '\\t',
    '\n': '\\n',
    '\f': '\\f',
    '\r': '\\r',
    '\\': '\\\\'
  }
});

Object.extend(String.prototype, {
  gsub: function(pattern, replacement) {
    var result = '', source = this, match;
    replacement = arguments.callee.prepareReplacement(replacement);

    while (source.length > 0) {
      if (match = source.match(pattern)) {
        result += source.slice(0, match.index);
        result += String.interpret(replacement(match));
        source  = source.slice(match.index + match[0].length);
      } else {
        result += source, source = '';
      }
    }
    return result;
  },

  sub: function(pattern, replacement, count) {
    replacement = this.gsub.prepareReplacement(replacement);
    count = count === undefined ? 1 : count;

    return this.gsub(pattern, function(match) {
      if (--count < 0) return match[0];
      return replacement(match);
    });
  },

  scan: function(pattern, iterator) {
    this.gsub(pattern, iterator);
    return String(this);
  },

  truncate: function(length, truncation) {
    length = length || 30;
    truncation = truncation === undefined ? '...' : truncation;
    return this.length > length ?
      this.slice(0, length - truncation.length) + truncation : String(this);
  },

  strip: function() {
    return this.replace(/^\s+/, '').replace(/\s+$/, '');
  },

  stripTags: function() {
    return this.replace(/<\/?[^>]+>/gi, '');
  },

  stripScripts: function() {
    return this.replace(new RegExp(Prototype.ScriptFragment, 'img'), '');
  },

  extractScripts: function() {
    var matchAll = new RegExp(Prototype.ScriptFragment, 'img');
    var matchOne = new RegExp(Prototype.ScriptFragment, 'im');
    return (this.match(matchAll) || []).map(function(scriptTag) {
      return (scriptTag.match(matchOne) || ['', ''])[1];
    });
  },

  evalScripts: function() {
    return this.extractScripts().map(function(script) { return eval(script) });
  },

  escapeHTML: function() {
    var self = arguments.callee;
    self.text.data = this;
    return self.div.innerHTML;
  },

  unescapeHTML: function() {
    var div = new Element('div');
    div.innerHTML = this.stripTags();
    return div.childNodes[0] ? (div.childNodes.length > 1 ?
      $A(div.childNodes).inject('', function(memo, node) { return memo+node.nodeValue }) :
      div.childNodes[0].nodeValue) : '';
  },

  toQueryParams: function(separator) {
    var match = this.strip().match(/([^?#]*)(#.*)?$/);
    if (!match) return { };

    return match[1].split(separator || '&').inject({ }, function(hash, pair) {
      if ((pair = pair.split('='))[0]) {
        var key = decodeURIComponent(pair.shift());
        var value = pair.length > 1 ? pair.join('=') : pair[0];
        if (value != undefined) value = decodeURIComponent(value);

        if (key in hash) {
          if (!Object.isArray(hash[key])) hash[key] = [hash[key]];
          hash[key].push(value);
        }
        else hash[key] = value;
      }
      return hash;
    });
  },

  toArray: function() {
    return this.split('');
  },

  succ: function() {
    return this.slice(0, this.length - 1) +
      String.fromCharCode(this.charCodeAt(this.length - 1) + 1);
  },

  times: function(count) {
    return count < 1 ? '' : new Array(count + 1).join(this);
  },

  camelize: function() {
    var parts = this.split('-'), len = parts.length;
    if (len == 1) return parts[0];

    var camelized = this.charAt(0) == '-'
      ? parts[0].charAt(0).toUpperCase() + parts[0].substring(1)
      : parts[0];

    for (var i = 1; i < len; i++)
      camelized += parts[i].charAt(0).toUpperCase() + parts[i].substring(1);

    return camelized;
  },

  capitalize: function() {
    return this.charAt(0).toUpperCase() + this.substring(1).toLowerCase();
  },

  underscore: function() {
    return this.gsub(/::/, '/').gsub(/([A-Z]+)([A-Z][a-z])/,'#{1}_#{2}').gsub(/([a-z\d])([A-Z])/,'#{1}_#{2}').gsub(/-/,'_').toLowerCase();
  },

  dasherize: function() {
    return this.gsub(/_/,'-');
  },

  inspect: function(useDoubleQuotes) {
    var escapedString = this.gsub(/[\x00-\x1f\\]/, function(match) {
      var character = String.specialChar[match[0]];
      return character ? character : '\\u00' + match[0].charCodeAt().toPaddedString(2, 16);
    });
    if (useDoubleQuotes) return '"' + escapedString.replace(/"/g, '\\"') + '"';
    return "'" + escapedString.replace(/'/g, '\\\'') + "'";
  },

  toJSON: function() {
    return this.inspect(true);
  },

  unfilterJSON: function(filter) {
    return this.sub(filter || Prototype.JSONFilter, '#{1}');
  },

  isJSON: function() {
    var str = this.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"/g, '');
    return (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(str);
  },

  evalJSON: function(sanitize) {
    var json = this.unfilterJSON();
    try {
      if (!sanitize || json.isJSON()) return eval('(' + json + ')');
    } catch (e) { }
    throw new SyntaxError('Badly formed JSON string: ' + this.inspect());
  },

  include: function(pattern) {
    return this.indexOf(pattern) > -1;
  },

  startsWith: function(pattern) {
    return this.indexOf(pattern) === 0;
  },

  endsWith: function(pattern) {
    var d = this.length - pattern.length;
    return d >= 0 && this.lastIndexOf(pattern) === d;
  },

  empty: function() {
    return this == '';
  },

  blank: function() {
    return /^\s*$/.test(this);
  },

  interpolate: function(object, pattern) {
    return new Template(this, pattern).evaluate(object);
  }
});

if (Prototype.Browser.WebKit || Prototype.Browser.IE) Object.extend(String.prototype, {
  escapeHTML: function() {
    return this.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
  },
  unescapeHTML: function() {
    return this.replace(/&amp;/g,'&').replace(/&lt;/g,'<').replace(/&gt;/g,'>');
  }
});

String.prototype.gsub.prepareReplacement = function(replacement) {
  if (Object.isFunction(replacement)) return replacement;
  var template = new Template(replacement);
  return function(match) { return template.evaluate(match) };
};

String.prototype.parseQuery = String.prototype.toQueryParams;

Object.extend(String.prototype.escapeHTML, {
  div:  document.createElement('div'),
  text: document.createTextNode('')
});

with (String.prototype.escapeHTML) div.appendChild(text);

var Template = Class.create({
  initialize: function(template, pattern) {
    this.template = template.toString();
    this.pattern = pattern || Template.Pattern;
  },

  evaluate: function(object) {
    if (Object.isFunction(object.toTemplateReplacements))
      object = object.toTemplateReplacements();

    return this.template.gsub(this.pattern, function(match) {
      if (object == null) return '';

      var before = match[1] || '';
      if (before == '\\') return match[2];

      var ctx = object, expr = match[3];
      var pattern = /^([^.[]+|\[((?:.*?[^\\])?)\])(\.|\[|$)/, match = pattern.exec(expr);
      if (match == null) return before;

      while (match != null) {
        var comp = match[1].startsWith('[') ? match[2].gsub('\\\\]', ']') : match[1];
        ctx = ctx[comp];
        if (null == ctx || '' == match[3]) break;
        expr = expr.substring('[' == match[3] ? match[1].length : match[0].length);
        match = pattern.exec(expr);
      }

      return before + String.interpret(ctx);
    }.bind(this));
  }
});
Template.Pattern = /(^|.|\r|\n)(#\{(.*?)\})/;

var $break = { };

var Enumerable = {
  each: function(iterator, context) {
    var index = 0;
    iterator = iterator.bind(context);
    try {
      this._each(function(value) {
        iterator(value, index++);
      });
    } catch (e) {
      if (e != $break) throw e;
    }
    return this;
  },

  eachSlice: function(number, iterator, context) {
    iterator = iterator ? iterator.bind(context) : Prototype.K;
    var index = -number, slices = [], array = this.toArray();
    while ((index += number) < array.length)
      slices.push(array.slice(index, index+number));
    return slices.collect(iterator, context);
  },

  all: function(iterator, context) {
    iterator = iterator ? iterator.bind(context) : Prototype.K;
    var result = true;
    this.each(function(value, index) {
      result = result && !!iterator(value, index);
      if (!result) throw $break;
    });
    return result;
  },

  any: function(iterator, context) {
    iterator = iterator ? iterator.bind(context) : Prototype.K;
    var result = false;
    this.each(function(value, index) {
      if (result = !!iterator(value, index))
        throw $break;
    });
    return result;
  },

  collect: function(iterator, context) {
    iterator = iterator ? iterator.bind(context) : Prototype.K;
    var results = [];
    this.each(function(value, index) {
      results.push(iterator(value, index));
    });
    return results;
  },

  detect: function(iterator, context) {
    iterator = iterator.bind(context);
    var result;
    this.each(function(value, index) {
      if (iterator(value, index)) {
        result = value;
        throw $break;
      }
    });
    return result;
  },

  findAll: function(iterator, context) {
    iterator = iterator.bind(context);
    var results = [];
    this.each(function(value, index) {
      if (iterator(value, index))
        results.push(value);
    });
    return results;
  },

  grep: function(filter, iterator, context) {
    iterator = iterator ? iterator.bind(context) : Prototype.K;
    var results = [];

    if (Object.isString(filter))
      filter = new RegExp(filter);

    this.each(function(value, index) {
      if (filter.match(value))
        results.push(iterator(value, index));
    });
    return results;
  },

  include: function(object) {
    if (Object.isFunction(this.indexOf))
      if (this.indexOf(object) != -1) return true;

    var found = false;
    this.each(function(value) {
      if (value == object) {
        found = true;
        throw $break;
      }
    });
    return found;
  },

  inGroupsOf: function(number, fillWith) {
    fillWith = fillWith === undefined ? null : fillWith;
    return this.eachSlice(number, function(slice) {
      while(slice.length < number) slice.push(fillWith);
      return slice;
    });
  },

  inject: function(memo, iterator, context) {
    iterator = iterator.bind(context);
    this.each(function(value, index) {
      memo = iterator(memo, value, index);
    });
    return memo;
  },

  invoke: function(method) {
    var args = $A(arguments).slice(1);
    return this.map(function(value) {
      return value[method].apply(value, args);
    });
  },

  max: function(iterator, context) {
    iterator = iterator ? iterator.bind(context) : Prototype.K;
    var result;
    this.each(function(value, index) {
      value = iterator(value, index);
      if (result == undefined || value >= result)
        result = value;
    });
    return result;
  },

  min: function(iterator, context) {
    iterator = iterator ? iterator.bind(context) : Prototype.K;
    var result;
    this.each(function(value, index) {
      value = iterator(value, index);
      if (result == undefined || value < result)
        result = value;
    });
    return result;
  },

  partition: function(iterator, context) {
    iterator = iterator ? iterator.bind(context) : Prototype.K;
    var trues = [], falses = [];
    this.each(function(value, index) {
      (iterator(value, index) ?
        trues : falses).push(value);
    });
    return [trues, falses];
  },

  pluck: function(property) {
    var results = [];
    this.each(function(value) {
      results.push(value[property]);
    });
    return results;
  },

  reject: function(iterator, context) {
    iterator = iterator.bind(context);
    var results = [];
    this.each(function(value, index) {
      if (!iterator(value, index))
        results.push(value);
    });
    return results;
  },

  sortBy: function(iterator, context) {
    iterator = iterator.bind(context);
    return this.map(function(value, index) {
      return {value: value, criteria: iterator(value, index)};
    }).sort(function(left, right) {
      var a = left.criteria, b = right.criteria;
      return a < b ? -1 : a > b ? 1 : 0;
    }).pluck('value');
  },

  toArray: function() {
    return this.map();
  },

  zip: function() {
    var iterator = Prototype.K, args = $A(arguments);
    if (Object.isFunction(args.last()))
      iterator = args.pop();

    var collections = [this].concat(args).map($A);
    return this.map(function(value, index) {
      return iterator(collections.pluck(index));
    });
  },

  size: function() {
    return this.toArray().length;
  },

  inspect: function() {
    return '#<Enumerable:' + this.toArray().inspect() + '>';
  }
};

Object.extend(Enumerable, {
  map:     Enumerable.collect,
  find:    Enumerable.detect,
  select:  Enumerable.findAll,
  filter:  Enumerable.findAll,
  member:  Enumerable.include,
  entries: Enumerable.toArray,
  every:   Enumerable.all,
  some:    Enumerable.any
});
function $A(iterable) {
  if (!iterable) return [];
  if (iterable.toArray) return iterable.toArray();
  var length = iterable.length, results = new Array(length);
  while (length--) results[length] = iterable[length];
  return results;
}

if (Prototype.Browser.WebKit) {
  function $A(iterable) {
    if (!iterable) return [];
    if (!(Object.isFunction(iterable) && iterable == '[object NodeList]') &&
        iterable.toArray) return iterable.toArray();
    var length = iterable.length, results = new Array(length);
    while (length--) results[length] = iterable[length];
    return results;
  }
}

Array.from = $A;

Object.extend(Array.prototype, Enumerable);

if (!Array.prototype._reverse) Array.prototype._reverse = Array.prototype.reverse;

Object.extend(Array.prototype, {
  _each: function(iterator) {
    for (var i = 0, length = this.length; i < length; i++)
      iterator(this[i]);
  },

  clear: function() {
    this.length = 0;
    return this;
  },

  first: function() {
    return this[0];
  },

  last: function() {
    return this[this.length - 1];
  },

  compact: function() {
    return this.select(function(value) {
      return value != null;
    });
  },

  flatten: function() {
    return this.inject([], function(array, value) {
      return array.concat(Object.isArray(value) ?
        value.flatten() : [value]);
    });
  },

  without: function() {
    var values = $A(arguments);
    return this.select(function(value) {
      return !values.include(value);
    });
  },

  reverse: function(inline) {
    return (inline !== false ? this : this.toArray())._reverse();
  },

  reduce: function() {
    return this.length > 1 ? this : this[0];
  },

  uniq: function(sorted) {
    return this.inject([], function(array, value, index) {
      if (0 == index || (sorted ? array.last() != value : !array.include(value)))
        array.push(value);
      return array;
    });
  },

  intersect: function(array) {
    return this.uniq().findAll(function(item) {
      return array.detect(function(value) { return item === value });
    });
  },

  clone: function() {
    return [].concat(this);
  },

  size: function() {
    return this.length;
  },

  inspect: function() {
    return '[' + this.map(Object.inspect).join(', ') + ']';
  },

  toJSON: function() {
    var results = [];
    this.each(function(object) {
      var value = Object.toJSON(object);
      if (value !== undefined) results.push(value);
    });
    return '[' + results.join(', ') + ']';
  }
});

// use native browser JS 1.6 implementation if available
if (Object.isFunction(Array.prototype.forEach))
  Array.prototype._each = Array.prototype.forEach;

if (!Array.prototype.indexOf) Array.prototype.indexOf = function(item, i) {
  i || (i = 0);
  var length = this.length;
  if (i < 0) i = length + i;
  for (; i < length; i++)
    if (this[i] === item) return i;
  return -1;
};

if (!Array.prototype.lastIndexOf) Array.prototype.lastIndexOf = function(item, i) {
  i = isNaN(i) ? this.length : (i < 0 ? this.length + i : i) + 1;
  var n = this.slice(0, i).reverse().indexOf(item);
  return (n < 0) ? n : i - n - 1;
};

Array.prototype.toArray = Array.prototype.clone;

function $w(string) {
  if (!Object.isString(string)) return [];
  string = string.strip();
  return string ? string.split(/\s+/) : [];
}

if (Prototype.Browser.Opera){
  Array.prototype.concat = function() {
    var array = [];
    for (var i = 0, length = this.length; i < length; i++) array.push(this[i]);
    for (var i = 0, length = arguments.length; i < length; i++) {
      if (Object.isArray(arguments[i])) {
        for (var j = 0, arrayLength = arguments[i].length; j < arrayLength; j++)
          array.push(arguments[i][j]);
      } else {
        array.push(arguments[i]);
      }
    }
    return array;
  };
}
Object.extend(Number.prototype, {
  toColorPart: function() {
    return this.toPaddedString(2, 16);
  },

  succ: function() {
    return this + 1;
  },

  times: function(iterator) {
    $R(0, this, true).each(iterator);
    return this;
  },

  toPaddedString: function(length, radix) {
    var string = this.toString(radix || 10);
    return '0'.times(length - string.length) + string;
  },

  toJSON: function() {
    return isFinite(this) ? this.toString() : 'null';
  }
});

$w('abs round ceil floor').each(function(method){
  Number.prototype[method] = Math[method].methodize();
});
function $H(object) {
  return new Hash(object);
};

var Hash = Class.create(Enumerable, (function() {
  if (function() {
    var i = 0, Test = function(value) { this.key = value };
    Test.prototype.key = 'foo';
    for (var property in new Test('bar')) i++;
    return i > 1;
  }()) {
    function each(iterator) {
      var cache = [];
      for (var key in this._object) {
        var value = this._object[key];
        if (cache.include(key)) continue;
        cache.push(key);
        var pair = [key, value];
        pair.key = key;
        pair.value = value;
        iterator(pair);
      }
    }
  } else {
    function each(iterator) {
      for (var key in this._object) {
        var value = this._object[key], pair = [key, value];
        pair.key = key;
        pair.value = value;
        iterator(pair);
      }
    }
  }

  function toQueryPair(key, value) {
    if (Object.isUndefined(value)) return key;
    return key + '=' + encodeURIComponent(String.interpret(value));
  }

  return {
    initialize: function(object) {
      this._object = Object.isHash(object) ? object.toObject() : Object.clone(object);
    },

    _each: each,

    set: function(key, value) {
      return this._object[key] = value;
    },

    get: function(key) {
      return this._object[key];
    },

    unset: function(key) {
      var value = this._object[key];
      delete this._object[key];
      return value;
    },

    toObject: function() {
      return Object.clone(this._object);
    },

    keys: function() {
      return this.pluck('key');
    },

    values: function() {
      return this.pluck('value');
    },

    index: function(value) {
      var match = this.detect(function(pair) {
        return pair.value === value;
      });
      return match && match.key;
    },

    merge: function(object) {
      return this.clone().update(object);
    },

    update: function(object) {
      return new Hash(object).inject(this, function(result, pair) {
        result.set(pair.key, pair.value);
        return result;
      });
    },

    toQueryString: function() {
      return this.map(function(pair) {
        var key = encodeURIComponent(pair.key), values = pair.value;

        if (values && typeof values == 'object') {
          if (Object.isArray(values))
            return values.map(toQueryPair.curry(key)).join('&');
        }
        return toQueryPair(key, values);
      }).join('&');
    },

    inspect: function() {
      return '#<Hash:{' + this.map(function(pair) {
        return pair.map(Object.inspect).join(': ');
      }).join(', ') + '}>';
    },

    toJSON: function() {
      return Object.toJSON(this.toObject());
    },

    clone: function() {
      return new Hash(this);
    }
  }
})());

Hash.prototype.toTemplateReplacements = Hash.prototype.toObject;
Hash.from = $H;
var ObjectRange = Class.create(Enumerable, {
  initialize: function(start, end, exclusive) {
    this.start = start;
    this.end = end;
    this.exclusive = exclusive;
  },

  _each: function(iterator) {
    var value = this.start;
    while (this.include(value)) {
      iterator(value);
      value = value.succ();
    }
  },

  include: function(value) {
    if (value < this.start)
      return false;
    if (this.exclusive)
      return value < this.end;
    return value <= this.end;
  }
});

var $R = function(start, end, exclusive) {
  return new ObjectRange(start, end, exclusive);
};

var Ajax = {
  getTransport: function() {
    return Try.these(
      function() {return new XMLHttpRequest()},
      function() {return new ActiveXObject('Msxml2.XMLHTTP')},
      function() {return new ActiveXObject('Microsoft.XMLHTTP')}
    ) || false;
  },

  activeRequestCount: 0
};

Ajax.Responders = {
  responders: [],

  _each: function(iterator) {
    this.responders._each(iterator);
  },

  register: function(responder) {
    if (!this.include(responder))
      this.responders.push(responder);
  },

  unregister: function(responder) {
    this.responders = this.responders.without(responder);
  },

  dispatch: function(callback, request, transport, json) {
    this.each(function(responder) {
      if (Object.isFunction(responder[callback])) {
        try {
          responder[callback].apply(responder, [request, transport, json]);
        } catch (e) { }
      }
    });
  }
};

Object.extend(Ajax.Responders, Enumerable);

Ajax.Responders.register({
  onCreate:   function() { Ajax.activeRequestCount++ },
  onComplete: function() { Ajax.activeRequestCount-- }
});

Ajax.Base = Class.create({
  initialize: function(options) {
    this.options = {
      method:       'post',
      asynchronous: true,
      contentType:  'application/x-www-form-urlencoded',
      encoding:     'UTF-8',
      parameters:   '',
      evalJSON:     true,
      evalJS:       true
    };
    Object.extend(this.options, options || { });

    this.options.method = this.options.method.toLowerCase();
    if (Object.isString(this.options.parameters))
      this.options.parameters = this.options.parameters.toQueryParams();
  }
});

Ajax.Request = Class.create(Ajax.Base, {
  _complete: false,

  initialize: function($super, url, options) {
    $super(options);
    this.transport = Ajax.getTransport();
    this.request(url);
  },

  request: function(url) {
    this.url = url;
    this.method = this.options.method;
    var params = Object.clone(this.options.parameters);

    if (!['get', 'post'].include(this.method)) {
      // simulate other verbs over post
      params['_method'] = this.method;
      this.method = 'post';
    }

    this.parameters = params;

    if (params = Object.toQueryString(params)) {
      // when GET, append parameters to URL
      if (this.method == 'get')
        this.url += (this.url.include('?') ? '&' : '?') + params;
      else if (/Konqueror|Safari|KHTML/.test(navigator.userAgent))
        params += '&_=';
    }

    try {
      var response = new Ajax.Response(this);
      if (this.options.onCreate) this.options.onCreate(response);
      Ajax.Responders.dispatch('onCreate', this, response);

      this.transport.open(this.method.toUpperCase(), this.url,
        this.options.asynchronous);

      if (this.options.asynchronous) this.respondToReadyState.bind(this).defer(1);

      this.transport.onreadystatechange = this.onStateChange.bind(this);
      this.setRequestHeaders();

      this.body = this.method == 'post' ? (this.options.postBody || params) : null;
      this.transport.send(this.body);

      /* Force Firefox to handle ready state 4 for synchronous requests */
      if (!this.options.asynchronous && this.transport.overrideMimeType)
        this.onStateChange();

    }
    catch (e) {
      this.dispatchException(e);
    }
  },

  onStateChange: function() {
    var readyState = this.transport.readyState;
    if (readyState > 1 && !((readyState == 4) && this._complete))
      this.respondToReadyState(this.transport.readyState);
  },

  setRequestHeaders: function() {
    var headers = {
      'X-Requested-With': 'XMLHttpRequest',
      'X-Prototype-Version': Prototype.Version,
      'Accept': 'text/javascript, text/html, application/xml, text/xml, */*'
    };

    if (this.method == 'post') {
      headers['Content-type'] = this.options.contentType +
        (this.options.encoding ? '; charset=' + this.options.encoding : '');

      /* Force "Connection: close" for older Mozilla browsers to work
       * around a bug where XMLHttpRequest sends an incorrect
       * Content-length header. See Mozilla Bugzilla #246651.
       */
      if (this.transport.overrideMimeType &&
          (navigator.userAgent.match(/Gecko\/(\d{4})/) || [0,2005])[1] < 2005)
            headers['Connection'] = 'close';
    }

    // user-defined headers
    if (typeof this.options.requestHeaders == 'object') {
      var extras = this.options.requestHeaders;

      if (Object.isFunction(extras.push))
        for (var i = 0, length = extras.length; i < length; i += 2)
          headers[extras[i]] = extras[i+1];
      else
        $H(extras).each(function(pair) { headers[pair.key] = pair.value });
    }

    for (var name in headers)
      this.transport.setRequestHeader(name, headers[name]);
  },

  success: function() {
    var status = this.getStatus();
    return !status || (status >= 200 && status < 300);
  },

  getStatus: function() {
    try {
      return this.transport.status || 0;
    } catch (e) { return 0 }
  },

  respondToReadyState: function(readyState) {
    var state = Ajax.Request.Events[readyState], response = new Ajax.Response(this);

    if (state == 'Complete') {
      try {
        this._complete = true;
        (this.options['on' + response.status]
         || this.options['on' + (this.success() ? 'Success' : 'Failure')]
         || Prototype.emptyFunction)(response, response.headerJSON);
      } catch (e) {
        this.dispatchException(e);
      }

      var contentType = response.getHeader('Content-type');
      if (this.options.evalJS == 'force'
          || (this.options.evalJS && contentType
          && contentType.match(/^\s*(text|application)\/(x-)?(java|ecma)script(;.*)?\s*$/i)))
        this.evalResponse();
    }

    try {
      (this.options['on' + state] || Prototype.emptyFunction)(response, response.headerJSON);
      Ajax.Responders.dispatch('on' + state, this, response, response.headerJSON);
    } catch (e) {
      this.dispatchException(e);
    }

    if (state == 'Complete') {
      // avoid memory leak in MSIE: clean up
      this.transport.onreadystatechange = Prototype.emptyFunction;
    }
  },

  getHeader: function(name) {
    try {
      return this.transport.getResponseHeader(name);
    } catch (e) { return null }
  },

  evalResponse: function() {
    try {
      return eval((this.transport.responseText || '').unfilterJSON());
    } catch (e) {
      this.dispatchException(e);
    }
  },

  dispatchException: function(exception) {
    (this.options.onException || Prototype.emptyFunction)(this, exception);
    Ajax.Responders.dispatch('onException', this, exception);
  }
});

Ajax.Request.Events =
  ['Uninitialized', 'Loading', 'Loaded', 'Interactive', 'Complete'];

Ajax.Response = Class.create({
  initialize: function(request){
    this.request = request;
    var transport  = this.transport  = request.transport,
        readyState = this.readyState = transport.readyState;

    if((readyState > 2 && !Prototype.Browser.IE) || readyState == 4) {
      this.status       = this.getStatus();
      this.statusText   = this.getStatusText();
      this.responseText = String.interpret(transport.responseText);
      this.headerJSON   = this._getHeaderJSON();
    }

    if(readyState == 4) {
      var xml = transport.responseXML;
      this.responseXML  = xml === undefined ? null : xml;
      this.responseJSON = this._getResponseJSON();
    }
  },

  status:      0,
  statusText: '',

  getStatus: Ajax.Request.prototype.getStatus,

  getStatusText: function() {
    try {
      return this.transport.statusText || '';
    } catch (e) { return '' }
  },

  getHeader: Ajax.Request.prototype.getHeader,

  getAllHeaders: function() {
    try {
      return this.getAllResponseHeaders();
    } catch (e) { return null }
  },

  getResponseHeader: function(name) {
    return this.transport.getResponseHeader(name);
  },

  getAllResponseHeaders: function() {
    return this.transport.getAllResponseHeaders();
  },

  _getHeaderJSON: function() {
    var json = this.getHeader('X-JSON');
    if (!json) return null;
    json = decodeURIComponent(escape(json));
    try {
      return json.evalJSON(this.request.options.sanitizeJSON);
    } catch (e) {
      this.request.dispatchException(e);
    }
  },

  _getResponseJSON: function() {
    var options = this.request.options;
    if (!options.evalJSON || (options.evalJSON != 'force' &&
      !(this.getHeader('Content-type') || '').include('application/json')))
        return null;
    try {
      return this.transport.responseText.evalJSON(options.sanitizeJSON);
    } catch (e) {
      this.request.dispatchException(e);
    }
  }
});

Ajax.Updater = Class.create(Ajax.Request, {
  initialize: function($super, container, url, options) {
    this.container = {
      success: (container.success || container),
      failure: (container.failure || (container.success ? null : container))
    };

    options = options || { };
    var onComplete = options.onComplete;
    options.onComplete = (function(response, param) {
      this.updateContent(response.responseText);
      if (Object.isFunction(onComplete)) onComplete(response, param);
    }).bind(this);

    $super(url, options);
  },

  updateContent: function(responseText) {
    var receiver = this.container[this.success() ? 'success' : 'failure'],
        options = this.options;

    if (!options.evalScripts) responseText = responseText.stripScripts();

    if (receiver = $(receiver)) {
      if (options.insertion) {
        if (Object.isString(options.insertion)) {
          var insertion = { }; insertion[options.insertion] = responseText;
          receiver.insert(insertion);
        }
        else options.insertion(receiver, responseText);
      }
      else receiver.update(responseText);
    }

    if (this.success()) {
      if (this.onComplete) this.onComplete.bind(this).defer();
    }
  }
});

Ajax.PeriodicalUpdater = Class.create(Ajax.Base, {
  initialize: function($super, container, url, options) {
    $super(options);
    this.onComplete = this.options.onComplete;

    this.frequency = (this.options.frequency || 2);
    this.decay = (this.options.decay || 1);

    this.updater = { };
    this.container = container;
    this.url = url;

    this.start();
  },

  start: function() {
    this.options.onComplete = this.updateComplete.bind(this);
    this.onTimerEvent();
  },

  stop: function() {
    this.updater.options.onComplete = undefined;
    clearTimeout(this.timer);
    (this.onComplete || Prototype.emptyFunction).apply(this, arguments);
  },

  updateComplete: function(response) {
    if (this.options.decay) {
      this.decay = (response.responseText == this.lastText ?
        this.decay * this.options.decay : 1);

      this.lastText = response.responseText;
    }
    this.timer = this.onTimerEvent.bind(this).delay(this.decay * this.frequency);
  },

  onTimerEvent: function() {
    this.updater = new Ajax.Updater(this.container, this.url, this.options);
  }
});
function $(element) {
  if (arguments.length > 1) {
    for (var i = 0, elements = [], length = arguments.length; i < length; i++)
      elements.push($(arguments[i]));
    return elements;
  }
  if (Object.isString(element))
    element = document.getElementById(element);
  return Element.extend(element);
}

if (Prototype.BrowserFeatures.XPath) {
  document._getElementsByXPath = function(expression, parentElement) {
    var results = [];
    var query = document.evaluate(expression, $(parentElement) || document,
      null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
    for (var i = 0, length = query.snapshotLength; i < length; i++)
      results.push(Element.extend(query.snapshotItem(i)));
    return results;
  };
}

/*--------------------------------------------------------------------------*/

if (!window.Node) var Node = { };

if (!Node.ELEMENT_NODE) {
  // DOM level 2 ECMAScript Language Binding
  Object.extend(Node, {
    ELEMENT_NODE: 1,
    ATTRIBUTE_NODE: 2,
    TEXT_NODE: 3,
    CDATA_SECTION_NODE: 4,
    ENTITY_REFERENCE_NODE: 5,
    ENTITY_NODE: 6,
    PROCESSING_INSTRUCTION_NODE: 7,
    COMMENT_NODE: 8,
    DOCUMENT_NODE: 9,
    DOCUMENT_TYPE_NODE: 10,
    DOCUMENT_FRAGMENT_NODE: 11,
    NOTATION_NODE: 12
  });
}

(function() {
  var element = this.Element;
  this.Element = function(tagName, attributes) {
    attributes = attributes || { };
    tagName = tagName.toLowerCase();
    var cache = Element.cache;
    if (Prototype.Browser.IE && attributes.name) {
      tagName = '<' + tagName + ' name="' + attributes.name + '">';
      delete attributes.name;
      return Element.writeAttribute(document.createElement(tagName), attributes);
    }
    if (!cache[tagName]) cache[tagName] = Element.extend(document.createElement(tagName));
    return Element.writeAttribute(cache[tagName].cloneNode(false), attributes);
  };
  Object.extend(this.Element, element || { });
}).call(window);

Element.cache = { };

Element.Methods = {
  visible: function(element) {
    return $(element).style.display != 'none';
  },

  toggle: function(element) {
    element = $(element);
    Element[Element.visible(element) ? 'hide' : 'show'](element);
    return element;
  },

  hide: function(element) {
    $(element).style.display = 'none';
    return element;
  },

  show: function(element) {
    $(element).style.display = '';
    return element;
  },

  remove: function(element) {
    element = $(element);
    element.parentNode.removeChild(element);
    return element;
  },

  update: function(element, content) {
    element = $(element);
    if (content && content.toElement) content = content.toElement();
    if (Object.isElement(content)) return element.update().insert(content);
    content = Object.toHTML(content);
    element.innerHTML = content.stripScripts();
    content.evalScripts.bind(content).defer();
    return element;
  },

  replace: function(element, content) {
    element = $(element);
    if (content && content.toElement) content = content.toElement();
    else if (!Object.isElement(content)) {
      content = Object.toHTML(content);
      var range = element.ownerDocument.createRange();
      range.selectNode(element);
      content.evalScripts.bind(content).defer();
      content = range.createContextualFragment(content.stripScripts());
    }
    element.parentNode.replaceChild(content, element);
    return element;
  },

  insert: function(element, insertions) {
    element = $(element);

    if (Object.isString(insertions) || Object.isNumber(insertions) ||
        Object.isElement(insertions) || (insertions && (insertions.toElement || insertions.toHTML)))
          insertions = {bottom:insertions};

    var content, t, range;

    for (position in insertions) {
      content  = insertions[position];
      position = position.toLowerCase();
      t = Element._insertionTranslations[position];

      if (content && content.toElement) content = content.toElement();
      if (Object.isElement(content)) {
        t.insert(element, content);
        continue;
      }

      content = Object.toHTML(content);

      range = element.ownerDocument.createRange();
      t.initializeRange(element, range);
      t.insert(element, range.createContextualFragment(content.stripScripts()));

      content.evalScripts.bind(content).defer();
    }

    return element;
  },

  wrap: function(element, wrapper, attributes) {
    element = $(element);
    if (Object.isElement(wrapper))
      $(wrapper).writeAttribute(attributes || { });
    else if (Object.isString(wrapper)) wrapper = new Element(wrapper, attributes);
    else wrapper = new Element('div', wrapper);
    if (element.parentNode)
      element.parentNode.replaceChild(wrapper, element);
    wrapper.appendChild(element);
    return wrapper;
  },

  inspect: function(element) {
    element = $(element);
    var result = '<' + element.tagName.toLowerCase();
    $H({'id': 'id', 'className': 'class'}).each(function(pair) {
      var property = pair.first(), attribute = pair.last();
      var value = (element[property] || '').toString();
      if (value) result += ' ' + attribute + '=' + value.inspect(true);
    });
    return result + '>';
  },

  recursivelyCollect: function(element, property) {
    element = $(element);
    var elements = [];
    while (element = element[property])
      if (element.nodeType == 1)
        elements.push(Element.extend(element));
    return elements;
  },

  ancestors: function(element) {
    return $(element).recursivelyCollect('parentNode');
  },

  descendants: function(element) {
    return $A($(element).getElementsByTagName('*')).each(Element.extend);
  },

  firstDescendant: function(element) {
    element = $(element).firstChild;
    while (element && element.nodeType != 1) element = element.nextSibling;
    return $(element);
  },

  immediateDescendants: function(element) {
    if (!(element = $(element).firstChild)) return [];
    while (element && element.nodeType != 1) element = element.nextSibling;
    if (element) return [element].concat($(element).nextSiblings());
    return [];
  },

  previousSiblings: function(element) {
    return $(element).recursivelyCollect('previousSibling');
  },

  nextSiblings: function(element) {
    return $(element).recursivelyCollect('nextSibling');
  },

  siblings: function(element) {
    element = $(element);
    return element.previousSiblings().reverse().concat(element.nextSiblings());
  },

  match: function(element, selector) {
    if (Object.isString(selector))
      selector = new Selector(selector);
    return selector.match($(element));
  },

  up: function(element, expression, index) {
    element = $(element);
    if (arguments.length == 1) return $(element.parentNode);
    var ancestors = element.ancestors();
    return expression ? Selector.findElement(ancestors, expression, index) :
      ancestors[index || 0];
  },

  down: function(element, expression, index) {
    element = $(element);
    if (arguments.length == 1) return element.firstDescendant();
    var descendants = element.descendants();
    return expression ? Selector.findElement(descendants, expression, index) :
      descendants[index || 0];
  },

  previous: function(element, expression, index) {
    element = $(element);
    if (arguments.length == 1) return $(Selector.handlers.previousElementSibling(element));
    var previousSiblings = element.previousSiblings();
    return expression ? Selector.findElement(previousSiblings, expression, index) :
      previousSiblings[index || 0];
  },

  next: function(element, expression, index) {
    element = $(element);
    if (arguments.length == 1) return $(Selector.handlers.nextElementSibling(element));
    var nextSiblings = element.nextSiblings();
    return expression ? Selector.findElement(nextSiblings, expression, index) :
      nextSiblings[index || 0];
  },

  select: function() {
    var args = $A(arguments), element = $(args.shift());
    return Selector.findChildElements(element, args);
  },

  adjacent: function() {
    var args = $A(arguments), element = $(args.shift());
    return Selector.findChildElements(element.parentNode, args).without(element);
  },

  identify: function(element) {
    element = $(element);
    var id = element.readAttribute('id'), self = arguments.callee;
    if (id) return id;
    do { id = 'anonymous_element_' + self.counter++ } while ($(id));
    element.writeAttribute('id', id);
    return id;
  },

  readAttribute: function(element, name) {
    element = $(element);
    if (Prototype.Browser.IE) {
      var t = Element._attributeTranslations.read;
      if (t.values[name]) return t.values[name](element, name);
      if (t.names[name]) name = t.names[name];
      if (name.include(':')) {
        return (!element.attributes || !element.attributes[name]) ? null :
         element.attributes[name].value;
      }
    }
    return element.getAttribute(name);
  },

  writeAttribute: function(element, name, value) {
    element = $(element);
    var attributes = { }, t = Element._attributeTranslations.write;

    if (typeof name == 'object') attributes = name;
    else attributes[name] = value === undefined ? true : value;

    for (var attr in attributes) {
      var name = t.names[attr] || attr, value = attributes[attr];
      if (t.values[attr]) name = t.values[attr](element, value);
      if (value === false || value === null)
        element.removeAttribute(name);
      else if (value === true)
        element.setAttribute(name, name);
      else element.setAttribute(name, value);
    }
    return element;
  },

  getHeight: function(element) {
    return $(element).getDimensions().height;
  },

  getWidth: function(element) {
    return $(element).getDimensions().width;
  },

  classNames: function(element) {
    return new Element.ClassNames(element);
  },

  hasClassName: function(element, className) {
    if (!(element = $(element))) return;
    var elementClassName = element.className;
    return (elementClassName.length > 0 && (elementClassName == className ||
      new RegExp("(^|\\s)" + className + "(\\s|$)").test(elementClassName)));
  },

  addClassName: function(element, className) {
    if (!(element = $(element))) return;
    if (!element.hasClassName(className))
      element.className += (element.className ? ' ' : '') + className;
    return element;
  },

  removeClassName: function(element, className) {
    if (!(element = $(element))) return;
    element.className = element.className.replace(
      new RegExp("(^|\\s+)" + className + "(\\s+|$)"), ' ').strip();
    return element;
  },

  toggleClassName: function(element, className) {
    if (!(element = $(element))) return;
    return element[element.hasClassName(className) ?
      'removeClassName' : 'addClassName'](className);
  },

  // removes whitespace-only text node children
  cleanWhitespace: function(element) {
    element = $(element);
    var node = element.firstChild;
    while (node) {
      var nextNode = node.nextSibling;
      if (node.nodeType == 3 && !/\S/.test(node.nodeValue))
        element.removeChild(node);
      node = nextNode;
    }
    return element;
  },

  empty: function(element) {
    return $(element).innerHTML.blank();
  },

  descendantOf: function(element, ancestor) {
    element = $(element), ancestor = $(ancestor);

    if (element.compareDocumentPosition)
      return (element.compareDocumentPosition(ancestor) & 8) === 8;

    if (element.sourceIndex && !Prototype.Browser.Opera) {
      var e = element.sourceIndex, a = ancestor.sourceIndex,
       nextAncestor = ancestor.nextSibling;
      if (!nextAncestor) {
        do { ancestor = ancestor.parentNode; }
        while (!(nextAncestor = ancestor.nextSibling) && ancestor.parentNode);
      }
      if (nextAncestor) return (e > a && e < nextAncestor.sourceIndex);
    }

    while (element = element.parentNode)
      if (element == ancestor) return true;
    return false;
  },

  scrollTo: function(element) {
    element = $(element);
    var pos = element.cumulativeOffset();
    window.scrollTo(pos[0], pos[1]);
    return element;
  },

  getStyle: function(element, style) {
    element = $(element);
    style = style == 'float' ? 'cssFloat' : style.camelize();
    var value = element.style[style];
    if (!value) {
      var css = document.defaultView.getComputedStyle(element, null);
      value = css ? css[style] : null;
    }
    if (style == 'opacity') return value ? parseFloat(value) : 1.0;
    return value == 'auto' ? null : value;
  },

  getOpacity: function(element) {
    return $(element).getStyle('opacity');
  },

  setStyle: function(element, styles) {
    element = $(element);
    var elementStyle = element.style, match;
    if (Object.isString(styles)) {
      element.style.cssText += ';' + styles;
      return styles.include('opacity') ?
        element.setOpacity(styles.match(/opacity:\s*(\d?\.?\d*)/)[1]) : element;
    }
    for (var property in styles)
      if (property == 'opacity') element.setOpacity(styles[property]);
      else
        elementStyle[(property == 'float' || property == 'cssFloat') ?
          (elementStyle.styleFloat === undefined ? 'cssFloat' : 'styleFloat') :
            property] = styles[property];

    return element;
  },

  setOpacity: function(element, value) {
    element = $(element);
    element.style.opacity = (value == 1 || value === '') ? '' :
      (value < 0.00001) ? 0 : value;
    return element;
  },

  getDimensions: function(element) {
    element = $(element);
    var display = $(element).getStyle('display');
    if (display != 'none' && display != null) // Safari bug
      return {width: element.offsetWidth, height: element.offsetHeight};

    // All *Width and *Height properties give 0 on elements with display none,
    // so enable the element temporarily
    var els = element.style;
    var originalVisibility = els.visibility;
    var originalPosition = els.position;
    var originalDisplay = els.display;
    els.visibility = 'hidden';
    els.position = 'absolute';
    els.display = 'block';
    var originalWidth = element.clientWidth;
    var originalHeight = element.clientHeight;
    els.display = originalDisplay;
    els.position = originalPosition;
    els.visibility = originalVisibility;
    return {width: originalWidth, height: originalHeight};
  },

  makePositioned: function(element) {
    element = $(element);
    var pos = Element.getStyle(element, 'position');
    if (pos == 'static' || !pos) {
      element._madePositioned = true;
      element.style.position = 'relative';
      // Opera returns the offset relative to the positioning context, when an
      // element is position relative but top and left have not been defined
      if (window.opera) {
        element.style.top = 0;
        element.style.left = 0;
      }
    }
    return element;
  },

  undoPositioned: function(element) {
    element = $(element);
    if (element._madePositioned) {
      element._madePositioned = undefined;
      element.style.position =
        element.style.top =
        element.style.left =
        element.style.bottom =
        element.style.right = '';
    }
    return element;
  },

  makeClipping: function(element) {
    element = $(element);
    if (element._overflow) return element;
    element._overflow = Element.getStyle(element, 'overflow') || 'auto';
    if (element._overflow !== 'hidden')
      element.style.overflow = 'hidden';
    return element;
  },

  undoClipping: function(element) {
    element = $(element);
    if (!element._overflow) return element;
    element.style.overflow = element._overflow == 'auto' ? '' : element._overflow;
    element._overflow = null;
    return element;
  },

  cumulativeOffset: function(element) {
    var valueT = 0, valueL = 0;
    do {
      valueT += element.offsetTop  || 0;
      valueL += element.offsetLeft || 0;
      element = element.offsetParent;
    } while (element);
    return Element._returnOffset(valueL, valueT);
  },

  positionedOffset: function(element) {
    var valueT = 0, valueL = 0;
    do {
      valueT += element.offsetTop  || 0;
      valueL += element.offsetLeft || 0;
      element = element.offsetParent;
      if (element) {
        if (element.tagName == 'BODY') break;
        var p = Element.getStyle(element, 'position');
        if (p == 'relative' || p == 'absolute') break;
      }
    } while (element);
    return Element._returnOffset(valueL, valueT);
  },

  absolutize: function(element) {
    element = $(element);
    if (element.getStyle('position') == 'absolute') return;
    // Position.prepare(); // To be done manually by Scripty when it needs it.

    var offsets = element.positionedOffset();
    var top     = offsets[1];
    var left    = offsets[0];
    var width   = element.clientWidth;
    var height  = element.clientHeight;

    element._originalLeft   = left - parseFloat(element.style.left  || 0);
    element._originalTop    = top  - parseFloat(element.style.top || 0);
    element._originalWidth  = element.style.width;
    element._originalHeight = element.style.height;

    element.style.position = 'absolute';
    element.style.top    = top + 'px';
    element.style.left   = left + 'px';
    element.style.width  = width + 'px';
    element.style.height = height + 'px';
    return element;
  },

  relativize: function(element) {
    element = $(element);
    if (element.getStyle('position') == 'relative') return;
    // Position.prepare(); // To be done manually by Scripty when it needs it.

    element.style.position = 'relative';
    var top  = parseFloat(element.style.top  || 0) - (element._originalTop || 0);
    var left = parseFloat(element.style.left || 0) - (element._originalLeft || 0);

    element.style.top    = top + 'px';
    element.style.left   = left + 'px';
    element.style.height = element._originalHeight;
    element.style.width  = element._originalWidth;
    return element;
  },

  cumulativeScrollOffset: function(element) {
    var valueT = 0, valueL = 0;
    do {
      valueT += element.scrollTop  || 0;
      valueL += element.scrollLeft || 0;
      element = element.parentNode;
    } while (element);
    return Element._returnOffset(valueL, valueT);
  },

  getOffsetParent: function(element) {
    if (element.offsetParent) return $(element.offsetParent);
    if (element == document.body) return $(element);

    while ((element = element.parentNode) && element != document.body)
      if (Element.getStyle(element, 'position') != 'static')
        return $(element);

    return $(document.body);
  },

  viewportOffset: function(forElement) {
    var valueT = 0, valueL = 0;

    var element = forElement;
    do {
      valueT += element.offsetTop  || 0;
      valueL += element.offsetLeft || 0;

      // Safari fix
      if (element.offsetParent == document.body &&
        Element.getStyle(element, 'position') == 'absolute') break;

    } while (element = element.offsetParent);

    element = forElement;
    do {
      if (!Prototype.Browser.Opera || element.tagName == 'BODY') {
        valueT -= element.scrollTop  || 0;
        valueL -= element.scrollLeft || 0;
      }
    } while (element = element.parentNode);

    return Element._returnOffset(valueL, valueT);
  },

  clonePosition: function(element, source) {
    var options = Object.extend({
      setLeft:    true,
      setTop:     true,
      setWidth:   true,
      setHeight:  true,
      offsetTop:  0,
      offsetLeft: 0
    }, arguments[2] || { });

    // find page position of source
    source = $(source);
    var p = source.viewportOffset();

    // find coordinate system to use
    element = $(element);
    var delta = [0, 0];
    var parent = null;
    // delta [0,0] will do fine with position: fixed elements,
    // position:absolute needs offsetParent deltas
    if (Element.getStyle(element, 'position') == 'absolute') {
      parent = element.getOffsetParent();
      delta = parent.viewportOffset();
    }

    // correct by body offsets (fixes Safari)
    if (parent == document.body) {
      delta[0] -= document.body.offsetLeft;
      delta[1] -= document.body.offsetTop;
    }

    // set position
    if (options.setLeft)   element.style.left  = (p[0] - delta[0] + options.offsetLeft) + 'px';
    if (options.setTop)    element.style.top   = (p[1] - delta[1] + options.offsetTop) + 'px';
    if (options.setWidth)  element.style.width = source.offsetWidth + 'px';
    if (options.setHeight) element.style.height = source.offsetHeight + 'px';
    return element;
  }
};

Element.Methods.identify.counter = 1;

Object.extend(Element.Methods, {
  getElementsBySelector: Element.Methods.select,
  childElements: Element.Methods.immediateDescendants
});

Element._attributeTranslations = {
  write: {
    names: {
      className: 'class',
      htmlFor:   'for'
    },
    values: { }
  }
};


if (!document.createRange || Prototype.Browser.Opera) {
  Element.Methods.insert = function(element, insertions) {
    element = $(element);

    if (Object.isString(insertions) || Object.isNumber(insertions) ||
        Object.isElement(insertions) || (insertions && (insertions.toElement || insertions.toHTML)))
          insertions = { bottom: insertions };

    var t = Element._insertionTranslations, content, position, pos, tagName;

    for (position in insertions) {
      content  = insertions[position];
      position = position.toLowerCase();
      pos      = t[position];

      if (content && content.toElement) content = content.toElement();
      if (Object.isElement(content)) {
        pos.insert(element, content);
        continue;
      }

      content = Object.toHTML(content);
      tagName = ((position == 'before' || position == 'after')
        ? element.parentNode : element).tagName.toUpperCase();

      if (t.tags[tagName]) {
        var fragments = Element._getContentFromAnonymousElement(tagName, content.stripScripts());
        if (position == 'top' || position == 'after') fragments.reverse();
        fragments.each(pos.insert.curry(element));
      }
      else element.insertAdjacentHTML(pos.adjacency, content.stripScripts());

      content.evalScripts.bind(content).defer();
    }

    return element;
  };
}

if (Prototype.Browser.Opera) {
  Element.Methods._getStyle = Element.Methods.getStyle;
  Element.Methods.getStyle = function(element, style) {
    switch(style) {
      case 'left':
      case 'top':
      case 'right':
      case 'bottom':
        if (Element._getStyle(element, 'position') == 'static') return null;
      default: return Element._getStyle(element, style);
    }
  };
  Element.Methods._readAttribute = Element.Methods.readAttribute;
  Element.Methods.readAttribute = function(element, attribute) {
    if (attribute == 'title') return element.title;
    return Element._readAttribute(element, attribute);
  };
}

else if (Prototype.Browser.IE) {
  $w('positionedOffset getOffsetParent viewportOffset').each(function(method) {
    Element.Methods[method] = Element.Methods[method].wrap(
      function(proceed, element) {
        element = $(element);
        var position = element.getStyle('position');
        if (position != 'static') return proceed(element);
        element.setStyle({ position: 'relative' });
        var value = proceed(element);
        element.setStyle({ position: position });
        return value;
      }
    );
  });

  Element.Methods.getStyle = function(element, style) {
    element = $(element);
    style = (style == 'float' || style == 'cssFloat') ? 'styleFloat' : style.camelize();
    var value = element.style[style];
    if (!value && element.currentStyle) value = element.currentStyle[style];

    if (style == 'opacity') {
      if (value = (element.getStyle('filter') || '').match(/alpha\(opacity=(.*)\)/))
        if (value[1]) return parseFloat(value[1]) / 100;
      return 1.0;
    }

    if (value == 'auto') {
      if ((style == 'width' || style == 'height') && (element.getStyle('display') != 'none'))
        return element['offset' + style.capitalize()] + 'px';
      return null;
    }
    return value;
  };

  Element.Methods.setOpacity = function(element, value) {
    function stripAlpha(filter){
      return filter.replace(/alpha\([^\)]*\)/gi,'');
    }
    element = $(element);
    var currentStyle = element.currentStyle;
    if ((currentStyle && !currentStyle.hasLayout) ||
      (!currentStyle && element.style.zoom == 'normal'))
        element.style.zoom = 1;

    var filter = element.getStyle('filter'), style = element.style;
    if (value == 1 || value === '') {
      (filter = stripAlpha(filter)) ?
        style.filter = filter : style.removeAttribute('filter');
      return element;
    } else if (value < 0.00001) value = 0;
    style.filter = stripAlpha(filter) +
      'alpha(opacity=' + (value * 100) + ')';
    return element;
  };

  Element._attributeTranslations = {
    read: {
      names: {
        'class': 'className',
        'for':   'htmlFor'
      },
      values: {
        _getAttr: function(element, attribute) {
          return element.getAttribute(attribute, 2);
        },
        _getAttrNode: function(element, attribute) {
          var node = element.getAttributeNode(attribute);
          return node ? node.value : "";
        },
        _getEv: function(element, attribute) {
          var attribute = element.getAttribute(attribute);
          return attribute ? attribute.toString().slice(23, -2) : null;
        },
        _flag: function(element, attribute) {
          return $(element).hasAttribute(attribute) ? attribute : null;
        },
        style: function(element) {
          return element.style.cssText.toLowerCase();
        },
        title: function(element) {
          return element.title;
        }
      }
    }
  };

  Element._attributeTranslations.write = {
    names: Object.clone(Element._attributeTranslations.read.names),
    values: {
      checked: function(element, value) {
        element.checked = !!value;
      },

      style: function(element, value) {
        element.style.cssText = value ? value : '';
      }
    }
  };

  Element._attributeTranslations.has = {};

  $w('colSpan rowSpan vAlign dateTime accessKey tabIndex ' +
      'encType maxLength readOnly longDesc').each(function(attr) {
    Element._attributeTranslations.write.names[attr.toLowerCase()] = attr;
    Element._attributeTranslations.has[attr.toLowerCase()] = attr;
  });

  (function(v) {
    Object.extend(v, {
      href:        v._getAttr,
      src:         v._getAttr,
      type:        v._getAttr,
      action:      v._getAttrNode,
      disabled:    v._flag,
      checked:     v._flag,
      readonly:    v._flag,
      multiple:    v._flag,
      onload:      v._getEv,
      onunload:    v._getEv,
      onclick:     v._getEv,
      ondblclick:  v._getEv,
      onmousedown: v._getEv,
      onmouseup:   v._getEv,
      onmouseover: v._getEv,
      onmousemove: v._getEv,
      onmouseout:  v._getEv,
      onfocus:     v._getEv,
      onblur:      v._getEv,
      onkeypress:  v._getEv,
      onkeydown:   v._getEv,
      onkeyup:     v._getEv,
      onsubmit:    v._getEv,
      onreset:     v._getEv,
      onselect:    v._getEv,
      onchange:    v._getEv
    });
  })(Element._attributeTranslations.read.values);
}

else if (Prototype.Browser.Gecko && /rv:1\.8\.0/.test(navigator.userAgent)) {
  Element.Methods.setOpacity = function(element, value) {
    element = $(element);
    element.style.opacity = (value == 1) ? 0.999999 :
      (value === '') ? '' : (value < 0.00001) ? 0 : value;
    return element;
  };
}

else if (Prototype.Browser.WebKit) {
  Element.Methods.setOpacity = function(element, value) {
    element = $(element);
    element.style.opacity = (value == 1 || value === '') ? '' :
      (value < 0.00001) ? 0 : value;

    if (value == 1)
      if(element.tagName == 'IMG' && element.width) {
        element.width++; element.width--;
      } else try {
        var n = document.createTextNode(' ');
        element.appendChild(n);
        element.removeChild(n);
      } catch (e) { }

    return element;
  };

  // Safari returns margins on body which is incorrect if the child is absolutely
  // positioned.  For performance reasons, redefine Position.cumulativeOffset for
  // KHTML/WebKit only.
  Element.Methods.cumulativeOffset = function(element) {
    var valueT = 0, valueL = 0;
    do {
      valueT += element.offsetTop  || 0;
      valueL += element.offsetLeft || 0;
      if (element.offsetParent == document.body)
        if (Element.getStyle(element, 'position') == 'absolute') break;

      element = element.offsetParent;
    } while (element);

    return Element._returnOffset(valueL, valueT);
  };
}

if (Prototype.Browser.IE || Prototype.Browser.Opera) {
  // IE and Opera are missing .innerHTML support for TABLE-related and SELECT elements
  Element.Methods.update = function(element, content) {
    element = $(element);

    if (content && content.toElement) content = content.toElement();
    if (Object.isElement(content)) return element.update().insert(content);

    content = Object.toHTML(content);
    var tagName = element.tagName.toUpperCase();

    if (tagName in Element._insertionTranslations.tags) {
      $A(element.childNodes).each(function(node) { element.removeChild(node) });
      Element._getContentFromAnonymousElement(tagName, content.stripScripts())
        .each(function(node) { element.appendChild(node) });
    }
    else element.innerHTML = content.stripScripts();

    content.evalScripts.bind(content).defer();
    return element;
  };
}

if (document.createElement('div').outerHTML) {
  Element.Methods.replace = function(element, content) {
    element = $(element);

    if (content && content.toElement) content = content.toElement();
    if (Object.isElement(content)) {
      element.parentNode.replaceChild(content, element);
      return element;
    }

    content = Object.toHTML(content);
    var parent = element.parentNode, tagName = parent.tagName.toUpperCase();

    if (Element._insertionTranslations.tags[tagName]) {
      var nextSibling = element.next();
      var fragments = Element._getContentFromAnonymousElement(tagName, content.stripScripts());
      parent.removeChild(element);
      if (nextSibling)
        fragments.each(function(node) { parent.insertBefore(node, nextSibling) });
      else
        fragments.each(function(node) { parent.appendChild(node) });
    }
    else element.outerHTML = content.stripScripts();

    content.evalScripts.bind(content).defer();
    return element;
  };
}

Element._returnOffset = function(l, t) {
  var result = [l, t];
  result.left = l;
  result.top = t;
  return result;
};

Element._getContentFromAnonymousElement = function(tagName, html) {
  var div = new Element('div'), t = Element._insertionTranslations.tags[tagName];
  div.innerHTML = t[0] + html + t[1];
  t[2].times(function() { div = div.firstChild });
  return $A(div.childNodes);
};

Element._insertionTranslations = {
  before: {
    adjacency: 'beforeBegin',
    insert: function(element, node) {
      element.parentNode.insertBefore(node, element);
    },
    initializeRange: function(element, range) {
      range.setStartBefore(element);
    }
  },
  top: {
    adjacency: 'afterBegin',
    insert: function(element, node) {
      element.insertBefore(node, element.firstChild);
    },
    initializeRange: function(element, range) {
      range.selectNodeContents(element);
      range.collapse(true);
    }
  },
  bottom: {
    adjacency: 'beforeEnd',
    insert: function(element, node) {
      element.appendChild(node);
    }
  },
  after: {
    adjacency: 'afterEnd',
    insert: function(element, node) {
      element.parentNode.insertBefore(node, element.nextSibling);
    },
    initializeRange: function(element, range) {
      range.setStartAfter(element);
    }
  },
  tags: {
    TABLE:  ['<table>',                '</table>',                   1],
    TBODY:  ['<table><tbody>',         '</tbody></table>',           2],
    TR:     ['<table><tbody><tr>',     '</tr></tbody></table>',      3],
    TD:     ['<table><tbody><tr><td>', '</td></tr></tbody></table>', 4],
    SELECT: ['<select>',               '</select>',                  1]
  }
};

(function() {
  this.bottom.initializeRange = this.top.initializeRange;
  Object.extend(this.tags, {
    THEAD: this.tags.TBODY,
    TFOOT: this.tags.TBODY,
    TH:    this.tags.TD
  });
}).call(Element._insertionTranslations);

Element.Methods.Simulated = {
  hasAttribute: function(element, attribute) {
    attribute = Element._attributeTranslations.has[attribute] || attribute;
    var node = $(element).getAttributeNode(attribute);
    return node && node.specified;
  }
};

Element.Methods.ByTag = { };

Object.extend(Element, Element.Methods);

if (!Prototype.BrowserFeatures.ElementExtensions &&
    document.createElement('div').__proto__) {
  window.HTMLElement = { };
  window.HTMLElement.prototype = document.createElement('div').__proto__;
  Prototype.BrowserFeatures.ElementExtensions = true;
}

Element.extend = (function() {
  if (Prototype.BrowserFeatures.SpecificElementExtensions)
    return Prototype.K;

  var Methods = { }, ByTag = Element.Methods.ByTag;

  var extend = Object.extend(function(element) {
    if (!element || element._extendedByPrototype ||
        element.nodeType != 1 || element == window) return element;

    var methods = Object.clone(Methods),
      tagName = element.tagName, property, value;

    // extend methods for specific tags
    if (ByTag[tagName]) Object.extend(methods, ByTag[tagName]);

    for (property in methods) {
      value = methods[property];
      if (Object.isFunction(value) && !(property in element))
        element[property] = value.methodize();
    }

    element._extendedByPrototype = Prototype.emptyFunction;
    return element;

  }, {
    refresh: function() {
      // extend methods for all tags (Safari doesn't need this)
      if (!Prototype.BrowserFeatures.ElementExtensions) {
        Object.extend(Methods, Element.Methods);
        Object.extend(Methods, Element.Methods.Simulated);
      }
    }
  });

  extend.refresh();
  return extend;
})();

Element.hasAttribute = function(element, attribute) {
  if (element.hasAttribute) return element.hasAttribute(attribute);
  return Element.Methods.Simulated.hasAttribute(element, attribute);
};

Element.addMethods = function(methods) {
  var F = Prototype.BrowserFeatures, T = Element.Methods.ByTag;

  if (!methods) {
    Object.extend(Form, Form.Methods);
    Object.extend(Form.Element, Form.Element.Methods);
    Object.extend(Element.Methods.ByTag, {
      "FORM":     Object.clone(Form.Methods),
      "INPUT":    Object.clone(Form.Element.Methods),
      "SELECT":   Object.clone(Form.Element.Methods),
      "TEXTAREA": Object.clone(Form.Element.Methods)
    });
  }

  if (arguments.length == 2) {
    var tagName = methods;
    methods = arguments[1];
  }

  if (!tagName) Object.extend(Element.Methods, methods || { });
  else {
    if (Object.isArray(tagName)) tagName.each(extend);
    else extend(tagName);
  }

  function extend(tagName) {
    tagName = tagName.toUpperCase();
    if (!Element.Methods.ByTag[tagName])
      Element.Methods.ByTag[tagName] = { };
    Object.extend(Element.Methods.ByTag[tagName], methods);
  }

  function copy(methods, destination, onlyIfAbsent) {
    onlyIfAbsent = onlyIfAbsent || false;
    for (var property in methods) {
      var value = methods[property];
      if (!Object.isFunction(value)) continue;
      if (!onlyIfAbsent || !(property in destination))
        destination[property] = value.methodize();
    }
  }

  function findDOMClass(tagName) {
    var klass;
    var trans = {
      "OPTGROUP": "OptGroup", "TEXTAREA": "TextArea", "P": "Paragraph",
      "FIELDSET": "FieldSet", "UL": "UList", "OL": "OList", "DL": "DList",
      "DIR": "Directory", "H1": "Heading", "H2": "Heading", "H3": "Heading",
      "H4": "Heading", "H5": "Heading", "H6": "Heading", "Q": "Quote",
      "INS": "Mod", "DEL": "Mod", "A": "Anchor", "IMG": "Image", "CAPTION":
      "TableCaption", "COL": "TableCol", "COLGROUP": "TableCol", "THEAD":
      "TableSection", "TFOOT": "TableSection", "TBODY": "TableSection", "TR":
      "TableRow", "TH": "TableCell", "TD": "TableCell", "FRAMESET":
      "FrameSet", "IFRAME": "IFrame"
    };
    if (trans[tagName]) klass = 'HTML' + trans[tagName] + 'Element';
    if (window[klass]) return window[klass];
    klass = 'HTML' + tagName + 'Element';
    if (window[klass]) return window[klass];
    klass = 'HTML' + tagName.capitalize() + 'Element';
    if (window[klass]) return window[klass];

    window[klass] = { };
    window[klass].prototype = document.createElement(tagName).__proto__;
    return window[klass];
  }

  if (F.ElementExtensions) {
    copy(Element.Methods, HTMLElement.prototype);
    copy(Element.Methods.Simulated, HTMLElement.prototype, true);
  }

  if (F.SpecificElementExtensions) {
    for (var tag in Element.Methods.ByTag) {
      var klass = findDOMClass(tag);
      if (Object.isUndefined(klass)) continue;
      copy(T[tag], klass.prototype);
    }
  }

  Object.extend(Element, Element.Methods);
  delete Element.ByTag;

  if (Element.extend.refresh) Element.extend.refresh();
  Element.cache = { };
};

document.viewport = {
  getDimensions: function() {
    var dimensions = { };
    $w('width height').each(function(d) {
      var D = d.capitalize();
      dimensions[d] = self['inner' + D] ||
       (document.documentElement['client' + D] || document.body['client' + D]);
    });
    return dimensions;
  },

  getWidth: function() {
    return this.getDimensions().width;
  },

  getHeight: function() {
    return this.getDimensions().height;
  },

  getScrollOffsets: function() {
    return Element._returnOffset(
      window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
      window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop);
  }
};
/* Portions of the Selector class are derived from Jack Slocum’s DomQuery,
 * part of YUI-Ext version 0.40, distributed under the terms of an MIT-style
 * license.  Please see http://www.yui-ext.com/ for more information. */

var Selector = Class.create({
  initialize: function(expression) {
    this.expression = expression.strip();
    this.compileMatcher();
  },

  compileMatcher: function() {
    // Selectors with namespaced attributes can't use the XPath version
    if (Prototype.BrowserFeatures.XPath && !(/(\[[\w-]*?:|:checked)/).test(this.expression))
      return this.compileXPathMatcher();

    var e = this.expression, ps = Selector.patterns, h = Selector.handlers,
        c = Selector.criteria, le, p, m;

    if (Selector._cache[e]) {
      this.matcher = Selector._cache[e];
      return;
    }

    this.matcher = ["this.matcher = function(root) {",
                    "var r = root, h = Selector.handlers, c = false, n;"];

    while (e && le != e && (/\S/).test(e)) {
      le = e;
      for (var i in ps) {
        p = ps[i];
        if (m = e.match(p)) {
          this.matcher.push(Object.isFunction(c[i]) ? c[i](m) :
    	      new Template(c[i]).evaluate(m));
          e = e.replace(m[0], '');
          break;
        }
      }
    }

    this.matcher.push("return h.unique(n);\n}");
    eval(this.matcher.join('\n'));
    Selector._cache[this.expression] = this.matcher;
  },

  compileXPathMatcher: function() {
    var e = this.expression, ps = Selector.patterns,
        x = Selector.xpath, le, m;

    if (Selector._cache[e]) {
      this.xpath = Selector._cache[e]; return;
    }

    this.matcher = ['.//*'];
    while (e && le != e && (/\S/).test(e)) {
      le = e;
      for (var i in ps) {
        if (m = e.match(ps[i])) {
          this.matcher.push(Object.isFunction(x[i]) ? x[i](m) :
            new Template(x[i]).evaluate(m));
          e = e.replace(m[0], '');
          break;
        }
      }
    }

    this.xpath = this.matcher.join('');
    Selector._cache[this.expression] = this.xpath;
  },

  findElements: function(root) {
    root = root || document;
    if (this.xpath) return document._getElementsByXPath(this.xpath, root);
    return this.matcher(root);
  },

  match: function(element) {
    this.tokens = [];

    var e = this.expression, ps = Selector.patterns, as = Selector.assertions;
    var le, p, m;

    while (e && le !== e && (/\S/).test(e)) {
      le = e;
      for (var i in ps) {
        p = ps[i];
        if (m = e.match(p)) {
          // use the Selector.assertions methods unless the selector
          // is too complex.
          if (as[i]) {
            this.tokens.push([i, Object.clone(m)]);
            e = e.replace(m[0], '');
          } else {
            // reluctantly do a document-wide search
            // and look for a match in the array
            return this.findElements(document).include(element);
          }
        }
      }
    }

    var match = true, name, matches;
    for (var i = 0, token; token = this.tokens[i]; i++) {
      name = token[0], matches = token[1];
      if (!Selector.assertions[name](element, matches)) {
        match = false; break;
      }
    }

    return match;
  },

  toString: function() {
    return this.expression;
  },

  inspect: function() {
    return "#<Selector:" + this.expression.inspect() + ">";
  }
});

Object.extend(Selector, {
  _cache: { },

  xpath: {
    descendant:   "//*",
    child:        "/*",
    adjacent:     "/following-sibling::*[1]",
    laterSibling: '/following-sibling::*',
    tagName:      function(m) {
      if (m[1] == '*') return '';
      return "[local-name()='" + m[1].toLowerCase() +
             "' or local-name()='" + m[1].toUpperCase() + "']";
    },
    className:    "[contains(concat(' ', @class, ' '), ' #{1} ')]",
    id:           "[@id='#{1}']",
    attrPresence: "[@#{1}]",
    attr: function(m) {
      m[3] = m[5] || m[6];
      return new Template(Selector.xpath.operators[m[2]]).evaluate(m);
    },
    pseudo: function(m) {
      var h = Selector.xpath.pseudos[m[1]];
      if (!h) return '';
      if (Object.isFunction(h)) return h(m);
      return new Template(Selector.xpath.pseudos[m[1]]).evaluate(m);
    },
    operators: {
      '=':  "[@#{1}='#{3}']",
      '!=': "[@#{1}!='#{3}']",
      '^=': "[starts-with(@#{1}, '#{3}')]",
      '$=': "[substring(@#{1}, (string-length(@#{1}) - string-length('#{3}') + 1))='#{3}']",
      '*=': "[contains(@#{1}, '#{3}')]",
      '~=': "[contains(concat(' ', @#{1}, ' '), ' #{3} ')]",
      '|=': "[contains(concat('-', @#{1}, '-'), '-#{3}-')]"
    },
    pseudos: {
      'first-child': '[not(preceding-sibling::*)]',
      'last-child':  '[not(following-sibling::*)]',
      'only-child':  '[not(preceding-sibling::* or following-sibling::*)]',
      'empty':       "[count(*) = 0 and (count(text()) = 0 or translate(text(), ' \t\r\n', '') = '')]",
      'checked':     "[@checked]",
      'disabled':    "[@disabled]",
      'enabled':     "[not(@disabled)]",
      'not': function(m) {
        var e = m[6], p = Selector.patterns,
            x = Selector.xpath, le, m, v;

        var exclusion = [];
        while (e && le != e && (/\S/).test(e)) {
          le = e;
          for (var i in p) {
            if (m = e.match(p[i])) {
              v = Object.isFunction(x[i]) ? x[i](m) : new Template(x[i]).evaluate(m);
              exclusion.push("(" + v.substring(1, v.length - 1) + ")");
              e = e.replace(m[0], '');
              break;
            }
          }
        }
        return "[not(" + exclusion.join(" and ") + ")]";
      },
      'nth-child':      function(m) {
        return Selector.xpath.pseudos.nth("(count(./preceding-sibling::*) + 1) ", m);
      },
      'nth-last-child': function(m) {
        return Selector.xpath.pseudos.nth("(count(./following-sibling::*) + 1) ", m);
      },
      'nth-of-type':    function(m) {
        return Selector.xpath.pseudos.nth("position() ", m);
      },
      'nth-last-of-type': function(m) {
        return Selector.xpath.pseudos.nth("(last() + 1 - position()) ", m);
      },
      'first-of-type':  function(m) {
        m[6] = "1"; return Selector.xpath.pseudos['nth-of-type'](m);
      },
      'last-of-type':   function(m) {
        m[6] = "1"; return Selector.xpath.pseudos['nth-last-of-type'](m);
      },
      'only-of-type':   function(m) {
        var p = Selector.xpath.pseudos; return p['first-of-type'](m) + p['last-of-type'](m);
      },
      nth: function(fragment, m) {
        var mm, formula = m[6], predicate;
        if (formula == 'even') formula = '2n+0';
        if (formula == 'odd')  formula = '2n+1';
        if (mm = formula.match(/^(\d+)$/)) // digit only
          return '[' + fragment + "= " + mm[1] + ']';
        if (mm = formula.match(/^(-?\d*)?n(([+-])(\d+))?/)) { // an+b
          if (mm[1] == "-") mm[1] = -1;
          var a = mm[1] ? Number(mm[1]) : 1;
          var b = mm[2] ? Number(mm[2]) : 0;
          predicate = "[((#{fragment} - #{b}) mod #{a} = 0) and " +
          "((#{fragment} - #{b}) div #{a} >= 0)]";
          return new Template(predicate).evaluate({
            fragment: fragment, a: a, b: b });
        }
      }
    }
  },

  criteria: {
    tagName:      'n = h.tagName(n, r, "#{1}", c);   c = false;',
    className:    'n = h.className(n, r, "#{1}", c); c = false;',
    id:           'n = h.id(n, r, "#{1}", c);        c = false;',
    attrPresence: 'n = h.attrPresence(n, r, "#{1}"); c = false;',
    attr: function(m) {
      m[3] = (m[5] || m[6]);
      return new Template('n = h.attr(n, r, "#{1}", "#{3}", "#{2}"); c = false;').evaluate(m);
    },
    pseudo: function(m) {
      if (m[6]) m[6] = m[6].replace(/"/g, '\\"');
      return new Template('n = h.pseudo(n, "#{1}", "#{6}", r, c); c = false;').evaluate(m);
    },
    descendant:   'c = "descendant";',
    child:        'c = "child";',
    adjacent:     'c = "adjacent";',
    laterSibling: 'c = "laterSibling";'
  },

  patterns: {
    // combinators must be listed first
    // (and descendant needs to be last combinator)
    laterSibling: /^\s*~\s*/,
    child:        /^\s*>\s*/,
    adjacent:     /^\s*\+\s*/,
    descendant:   /^\s/,

    // selectors follow
    tagName:      /^\s*(\*|[\w\-]+)(\b|$)?/,
    id:           /^#([\w\-\*]+)(\b|$)/,
    className:    /^\.([\w\-\*]+)(\b|$)/,
    pseudo:       /^:((first|last|nth|nth-last|only)(-child|-of-type)|empty|checked|(en|dis)abled|not)(\((.*?)\))?(\b|$|(?=\s)|(?=:))/,
    attrPresence: /^\[([\w]+)\]/,
    attr:         /\[((?:[\w-]*:)?[\w-]+)\s*(?:([!^$*~|]?=)\s*((['"])([^\4]*?)\4|([^'"][^\]]*?)))?\]/
  },

  // for Selector.match and Element#match
  assertions: {
    tagName: function(element, matches) {
      return matches[1].toUpperCase() == element.tagName.toUpperCase();
    },

    className: function(element, matches) {
      return Element.hasClassName(element, matches[1]);
    },

    id: function(element, matches) {
      return element.id === matches[1];
    },

    attrPresence: function(element, matches) {
      return Element.hasAttribute(element, matches[1]);
    },

    attr: function(element, matches) {
      var nodeValue = Element.readAttribute(element, matches[1]);
      return Selector.operators[matches[2]](nodeValue, matches[3]);
    }
  },

  handlers: {
    // UTILITY FUNCTIONS
    // joins two collections
    concat: function(a, b) {
      for (var i = 0, node; node = b[i]; i++)
        a.push(node);
      return a;
    },

    // marks an array of nodes for counting
    mark: function(nodes) {
      for (var i = 0, node; node = nodes[i]; i++)
        node._counted = true;
      return nodes;
    },

    unmark: function(nodes) {
      for (var i = 0, node; node = nodes[i]; i++)
        node._counted = undefined;
      return nodes;
    },

    // mark each child node with its position (for nth calls)
    // "ofType" flag indicates whether we're indexing for nth-of-type
    // rather than nth-child
    index: function(parentNode, reverse, ofType) {
      parentNode._counted = true;
      if (reverse) {
        for (var nodes = parentNode.childNodes, i = nodes.length - 1, j = 1; i >= 0; i--) {
          var node = nodes[i];
          if (node.nodeType == 1 && (!ofType || node._counted)) node.nodeIndex = j++;
        }
      } else {
        for (var i = 0, j = 1, nodes = parentNode.childNodes; node = nodes[i]; i++)
          if (node.nodeType == 1 && (!ofType || node._counted)) node.nodeIndex = j++;
      }
    },

    // filters out duplicates and extends all nodes
    unique: function(nodes) {
      if (nodes.length == 0) return nodes;
      var results = [], n;
      for (var i = 0, l = nodes.length; i < l; i++)
        if (!(n = nodes[i])._counted) {
          n._counted = true;
          results.push(Element.extend(n));
        }
      return Selector.handlers.unmark(results);
    },

    // COMBINATOR FUNCTIONS
    descendant: function(nodes) {
      var h = Selector.handlers;
      for (var i = 0, results = [], node; node = nodes[i]; i++)
        h.concat(results, node.getElementsByTagName('*'));
      return results;
    },

    child: function(nodes) {
      var h = Selector.handlers;
      for (var i = 0, results = [], node; node = nodes[i]; i++) {
        for (var j = 0, children = [], child; child = node.childNodes[j]; j++)
          if (child.nodeType == 1 && child.tagName != '!') results.push(child);
      }
      return results;
    },

    adjacent: function(nodes) {
      for (var i = 0, results = [], node; node = nodes[i]; i++) {
        var next = this.nextElementSibling(node);
        if (next) results.push(next);
      }
      return results;
    },

    laterSibling: function(nodes) {
      var h = Selector.handlers;
      for (var i = 0, results = [], node; node = nodes[i]; i++)
        h.concat(results, Element.nextSiblings(node));
      return results;
    },

    nextElementSibling: function(node) {
      while (node = node.nextSibling)
	      if (node.nodeType == 1) return node;
      return null;
    },

    previousElementSibling: function(node) {
      while (node = node.previousSibling)
        if (node.nodeType == 1) return node;
      return null;
    },

    // TOKEN FUNCTIONS
    tagName: function(nodes, root, tagName, combinator) {
      tagName = tagName.toUpperCase();
      var results = [], h = Selector.handlers;
      if (nodes) {
        if (combinator) {
          // fastlane for ordinary descendant combinators
          if (combinator == "descendant") {
            for (var i = 0, node; node = nodes[i]; i++)
              h.concat(results, node.getElementsByTagName(tagName));
            return results;
          } else nodes = this[combinator](nodes);
          if (tagName == "*") return nodes;
        }
        for (var i = 0, node; node = nodes[i]; i++)
          if (node.tagName.toUpperCase() == tagName) results.push(node);
        return results;
      } else return root.getElementsByTagName(tagName);
    },

    id: function(nodes, root, id, combinator) {
      var targetNode = $(id), h = Selector.handlers;
      if (!targetNode) return [];
      if (!nodes && root == document) return [targetNode];
      if (nodes) {
        if (combinator) {
          if (combinator == 'child') {
            for (var i = 0, node; node = nodes[i]; i++)
              if (targetNode.parentNode == node) return [targetNode];
          } else if (combinator == 'descendant') {
            for (var i = 0, node; node = nodes[i]; i++)
              if (Element.descendantOf(targetNode, node)) return [targetNode];
          } else if (combinator == 'adjacent') {
            for (var i = 0, node; node = nodes[i]; i++)
              if (Selector.handlers.previousElementSibling(targetNode) == node)
                return [targetNode];
          } else nodes = h[combinator](nodes);
        }
        for (var i = 0, node; node = nodes[i]; i++)
          if (node == targetNode) return [targetNode];
        return [];
      }
      return (targetNode && Element.descendantOf(targetNode, root)) ? [targetNode] : [];
    },

    className: function(nodes, root, className, combinator) {
      if (nodes && combinator) nodes = this[combinator](nodes);
      return Selector.handlers.byClassName(nodes, root, className);
    },

    byClassName: function(nodes, root, className) {
      if (!nodes) nodes = Selector.handlers.descendant([root]);
      var needle = ' ' + className + ' ';
      for (var i = 0, results = [], node, nodeClassName; node = nodes[i]; i++) {
        nodeClassName = node.className;
        if (nodeClassName.length == 0) continue;
        if (nodeClassName == className || (' ' + nodeClassName + ' ').include(needle))
          results.push(node);
      }
      return results;
    },

    attrPresence: function(nodes, root, attr) {
      if (!nodes) nodes = root.getElementsByTagName("*");
      var results = [];
      for (var i = 0, node; node = nodes[i]; i++)
        if (Element.hasAttribute(node, attr)) results.push(node);
      return results;
    },

    attr: function(nodes, root, attr, value, operator) {
      if (!nodes) nodes = root.getElementsByTagName("*");
      var handler = Selector.operators[operator], results = [];
      for (var i = 0, node; node = nodes[i]; i++) {
        var nodeValue = Element.readAttribute(node, attr);
        if (nodeValue === null) continue;
        if (handler(nodeValue, value)) results.push(node);
      }
      return results;
    },

    pseudo: function(nodes, name, value, root, combinator) {
      if (nodes && combinator) nodes = this[combinator](nodes);
      if (!nodes) nodes = root.getElementsByTagName("*");
      return Selector.pseudos[name](nodes, value, root);
    }
  },

  pseudos: {
    'first-child': function(nodes, value, root) {
      for (var i = 0, results = [], node; node = nodes[i]; i++) {
        if (Selector.handlers.previousElementSibling(node)) continue;
          results.push(node);
      }
      return results;
    },
    'last-child': function(nodes, value, root) {
      for (var i = 0, results = [], node; node = nodes[i]; i++) {
        if (Selector.handlers.nextElementSibling(node)) continue;
          results.push(node);
      }
      return results;
    },
    'only-child': function(nodes, value, root) {
      var h = Selector.handlers;
      for (var i = 0, results = [], node; node = nodes[i]; i++)
        if (!h.previousElementSibling(node) && !h.nextElementSibling(node))
          results.push(node);
      return results;
    },
    'nth-child':        function(nodes, formula, root) {
      return Selector.pseudos.nth(nodes, formula, root);
    },
    'nth-last-child':   function(nodes, formula, root) {
      return Selector.pseudos.nth(nodes, formula, root, true);
    },
    'nth-of-type':      function(nodes, formula, root) {
      return Selector.pseudos.nth(nodes, formula, root, false, true);
    },
    'nth-last-of-type': function(nodes, formula, root) {
      return Selector.pseudos.nth(nodes, formula, root, true, true);
    },
    'first-of-type':    function(nodes, formula, root) {
      return Selector.pseudos.nth(nodes, "1", root, false, true);
    },
    'last-of-type':     function(nodes, formula, root) {
      return Selector.pseudos.nth(nodes, "1", root, true, true);
    },
    'only-of-type':     function(nodes, formula, root) {
      var p = Selector.pseudos;
      return p['last-of-type'](p['first-of-type'](nodes, formula, root), formula, root);
    },

    // handles the an+b logic
    getIndices: function(a, b, total) {
      if (a == 0) return b > 0 ? [b] : [];
      return $R(1, total).inject([], function(memo, i) {
        if (0 == (i - b) % a && (i - b) / a >= 0) memo.push(i);
        return memo;
      });
    },

    // handles nth(-last)-child, nth(-last)-of-type, and (first|last)-of-type
    nth: function(nodes, formula, root, reverse, ofType) {
      if (nodes.length == 0) return [];
      if (formula == 'even') formula = '2n+0';
      if (formula == 'odd')  formula = '2n+1';
      var h = Selector.handlers, results = [], indexed = [], m;
      h.mark(nodes);
      for (var i = 0, node; node = nodes[i]; i++) {
        if (!node.parentNode._counted) {
          h.index(node.parentNode, reverse, ofType);
          indexed.push(node.parentNode);
        }
      }
      if (formula.match(/^\d+$/)) { // just a number
        formula = Number(formula);
        for (var i = 0, node; node = nodes[i]; i++)
          if (node.nodeIndex == formula) results.push(node);
      } else if (m = formula.match(/^(-?\d*)?n(([+-])(\d+))?/)) { // an+b
        if (m[1] == "-") m[1] = -1;
        var a = m[1] ? Number(m[1]) : 1;
        var b = m[2] ? Number(m[2]) : 0;
        var indices = Selector.pseudos.getIndices(a, b, nodes.length);
        for (var i = 0, node, l = indices.length; node = nodes[i]; i++) {
          for (var j = 0; j < l; j++)
            if (node.nodeIndex == indices[j]) results.push(node);
        }
      }
      h.unmark(nodes);
      h.unmark(indexed);
      return results;
    },

    'empty': function(nodes, value, root) {
      for (var i = 0, results = [], node; node = nodes[i]; i++) {
        // IE treats comments as element nodes
        if (node.tagName == '!' || (node.firstChild && !node.innerHTML.match(/^\s*$/))) continue;
        results.push(node);
      }
      return results;
    },

    'not': function(nodes, selector, root) {
      var h = Selector.handlers, selectorType, m;
      var exclusions = new Selector(selector).findElements(root);
      h.mark(exclusions);
      for (var i = 0, results = [], node; node = nodes[i]; i++)
        if (!node._counted) results.push(node);
      h.unmark(exclusions);
      return results;
    },

    'enabled': function(nodes, value, root) {
      for (var i = 0, results = [], node; node = nodes[i]; i++)
        if (!node.disabled) results.push(node);
      return results;
    },

    'disabled': function(nodes, value, root) {
      for (var i = 0, results = [], node; node = nodes[i]; i++)
        if (node.disabled) results.push(node);
      return results;
    },

    'checked': function(nodes, value, root) {
      for (var i = 0, results = [], node; node = nodes[i]; i++)
        if (node.checked) results.push(node);
      return results;
    }
  },

  operators: {
    '=':  function(nv, v) { return nv == v; },
    '!=': function(nv, v) { return nv != v; },
    '^=': function(nv, v) { return nv.startsWith(v); },
    '$=': function(nv, v) { return nv.endsWith(v); },
    '*=': function(nv, v) { return nv.include(v); },
    '~=': function(nv, v) { return (' ' + nv + ' ').include(' ' + v + ' '); },
    '|=': function(nv, v) { return ('-' + nv.toUpperCase() + '-').include('-' + v.toUpperCase() + '-'); }
  },

  matchElements: function(elements, expression) {
    var matches = new Selector(expression).findElements(), h = Selector.handlers;
    h.mark(matches);
    for (var i = 0, results = [], element; element = elements[i]; i++)
      if (element._counted) results.push(element);
    h.unmark(matches);
    return results;
  },

  findElement: function(elements, expression, index) {
    if (Object.isNumber(expression)) {
      index = expression; expression = false;
    }
    return Selector.matchElements(elements, expression || '*')[index || 0];
  },

  findChildElements: function(element, expressions) {
    var exprs = expressions.join(','), expressions = [];
    exprs.scan(/(([\w#:.~>+()\s-]+|\*|\[.*?\])+)\s*(,|$)/, function(m) {
      expressions.push(m[1].strip());
    });
    var results = [], h = Selector.handlers;
    for (var i = 0, l = expressions.length, selector; i < l; i++) {
      selector = new Selector(expressions[i].strip());
      h.concat(results, selector.findElements(element));
    }
    return (l > 1) ? h.unique(results) : results;
  }
});

function $$() {
  return Selector.findChildElements(document, $A(arguments));
}
var Form = {
  reset: function(form) {
    $(form).reset();
    return form;
  },

  serializeElements: function(elements, options) {
    if (typeof options != 'object') options = { hash: !!options };
    else if (options.hash === undefined) options.hash = true;
    var key, value, submitted = false, submit = options.submit;

    var data = elements.inject({ }, function(result, element) {
      if (!element.disabled && element.name) {
        key = element.name; value = $(element).getValue();
        if (value != null && (element.type != 'submit' || (!submitted &&
            submit !== false && (!submit || key == submit) && (submitted = true)))) {
          if (key in result) {
            // a key is already present; construct an array of values
            if (!Object.isArray(result[key])) result[key] = [result[key]];
            result[key].push(value);
          }
          else result[key] = value;
        }
      }
      return result;
    });

    return options.hash ? data : Object.toQueryString(data);
  }
};

Form.Methods = {
  serialize: function(form, options) {
    return Form.serializeElements(Form.getElements(form), options);
  },

  getElements: function(form) {
    return $A($(form).getElementsByTagName('*')).inject([],
      function(elements, child) {
        if (Form.Element.Serializers[child.tagName.toLowerCase()])
          elements.push(Element.extend(child));
        return elements;
      }
    );
  },

  getInputs: function(form, typeName, name) {
    form = $(form);
    var inputs = form.getElementsByTagName('input');

    if (!typeName && !name) return $A(inputs).map(Element.extend);

    for (var i = 0, matchingInputs = [], length = inputs.length; i < length; i++) {
      var input = inputs[i];
      if ((typeName && input.type != typeName) || (name && input.name != name))
        continue;
      matchingInputs.push(Element.extend(input));
    }

    return matchingInputs;
  },

  disable: function(form) {
    form = $(form);
    Form.getElements(form).invoke('disable');
    return form;
  },

  enable: function(form) {
    form = $(form);
    Form.getElements(form).invoke('enable');
    return form;
  },

  findFirstElement: function(form) {
    var elements = $(form).getElements().findAll(function(element) {
      return 'hidden' != element.type && !element.disabled;
    });
    var firstByIndex = elements.findAll(function(element) {
      return element.hasAttribute('tabIndex') && element.tabIndex >= 0;
    }).sortBy(function(element) { return element.tabIndex }).first();

    return firstByIndex ? firstByIndex : elements.find(function(element) {
      return ['input', 'select', 'textarea'].include(element.tagName.toLowerCase());
    });
  },

  focusFirstElement: function(form) {
    form = $(form);
    form.findFirstElement().activate();
    return form;
  },

  request: function(form, options) {
    form = $(form), options = Object.clone(options || { });

    var params = options.parameters, action = form.readAttribute('action') || '';
    if (action.blank()) action = window.location.href;
    options.parameters = form.serialize(true);

    if (params) {
      if (Object.isString(params)) params = params.toQueryParams();
      Object.extend(options.parameters, params);
    }

    if (form.hasAttribute('method') && !options.method)
      options.method = form.method;

    return new Ajax.Request(action, options);
  }
};

/*--------------------------------------------------------------------------*/

Form.Element = {
  focus: function(element) {
    $(element).focus();
    return element;
  },

  select: function(element) {
    $(element).select();
    return element;
  }
};

Form.Element.Methods = {
  serialize: function(element) {
    element = $(element);
    if (!element.disabled && element.name) {
      var value = element.getValue();
      if (value != undefined) {
        var pair = { };
        pair[element.name] = value;
        return Object.toQueryString(pair);
      }
    }
    return '';
  },

  getValue: function(element) {
    element = $(element);
    var method = element.tagName.toLowerCase();
    return Form.Element.Serializers[method](element);
  },

  setValue: function(element, value) {
    element = $(element);
    var method = element.tagName.toLowerCase();
    Form.Element.Serializers[method](element, value);
    return element;
  },

  clear: function(element) {
    $(element).value = '';
    return element;
  },

  present: function(element) {
    return $(element).value != '';
  },

  activate: function(element) {
    element = $(element);
    try {
      element.focus();
      if (element.select && (element.tagName.toLowerCase() != 'input' ||
          !['button', 'reset', 'submit'].include(element.type)))
        element.select();
    } catch (e) { }
    return element;
  },

  disable: function(element) {
    element = $(element);
    element.blur();
    element.disabled = true;
    return element;
  },

  enable: function(element) {
    element = $(element);
    element.disabled = false;
    return element;
  }
};

/*--------------------------------------------------------------------------*/

var Field = Form.Element;
var $F = Form.Element.Methods.getValue;

/*--------------------------------------------------------------------------*/

Form.Element.Serializers = {
  input: function(element, value) {
    switch (element.type.toLowerCase()) {
      case 'checkbox':
      case 'radio':
        return Form.Element.Serializers.inputSelector(element, value);
      default:
        return Form.Element.Serializers.textarea(element, value);
    }
  },

  inputSelector: function(element, value) {
    if (value === undefined) return element.checked ? element.value : null;
    else element.checked = !!value;
  },

  textarea: function(element, value) {
    if (value === undefined) return element.value;
    else element.value = value;
  },

  select: function(element, index) {
    if (index === undefined)
      return this[element.type == 'select-one' ?
        'selectOne' : 'selectMany'](element);
    else {
      var opt, value, single = !Object.isArray(index);
      for (var i = 0, length = element.length; i < length; i++) {
        opt = element.options[i];
        value = this.optionValue(opt);
        if (single) {
          if (value == index) {
            opt.selected = true;
            return;
          }
        }
        else opt.selected = index.include(value);
      }
    }
  },

  selectOne: function(element) {
    var index = element.selectedIndex;
    return index >= 0 ? this.optionValue(element.options[index]) : null;
  },

  selectMany: function(element) {
    var values, length = element.length;
    if (!length) return null;

    for (var i = 0, values = []; i < length; i++) {
      var opt = element.options[i];
      if (opt.selected) values.push(this.optionValue(opt));
    }
    return values;
  },

  optionValue: function(opt) {
    // extend element because hasAttribute may not be native
    return Element.extend(opt).hasAttribute('value') ? opt.value : opt.text;
  }
};

/*--------------------------------------------------------------------------*/

Abstract.TimedObserver = Class.create(PeriodicalExecuter, {
  initialize: function($super, element, frequency, callback) {
    $super(callback, frequency);
    this.element   = $(element);
    this.lastValue = this.getValue();
  },

  execute: function() {
    var value = this.getValue();
    if (Object.isString(this.lastValue) && Object.isString(value) ?
        this.lastValue != value : String(this.lastValue) != String(value)) {
      this.callback(this.element, value);
      this.lastValue = value;
    }
  }
});

Form.Element.Observer = Class.create(Abstract.TimedObserver, {
  getValue: function() {
    return Form.Element.getValue(this.element);
  }
});

Form.Observer = Class.create(Abstract.TimedObserver, {
  getValue: function() {
    return Form.serialize(this.element);
  }
});

/*--------------------------------------------------------------------------*/

Abstract.EventObserver = Class.create({
  initialize: function(element, callback) {
    this.element  = $(element);
    this.callback = callback;

    this.lastValue = this.getValue();
    if (this.element.tagName.toLowerCase() == 'form')
      this.registerFormCallbacks();
    else
      this.registerCallback(this.element);
  },

  onElementEvent: function() {
    var value = this.getValue();
    if (this.lastValue != value) {
      this.callback(this.element, value);
      this.lastValue = value;
    }
  },

  registerFormCallbacks: function() {
    Form.getElements(this.element).each(this.registerCallback, this);
  },

  registerCallback: function(element) {
    if (element.type) {
      switch (element.type.toLowerCase()) {
        case 'checkbox':
        case 'radio':
          Event.observe(element, 'click', this.onElementEvent.bind(this));
          break;
        default:
          Event.observe(element, 'change', this.onElementEvent.bind(this));
          break;
      }
    }
  }
});

Form.Element.EventObserver = Class.create(Abstract.EventObserver, {
  getValue: function() {
    return Form.Element.getValue(this.element);
  }
});

Form.EventObserver = Class.create(Abstract.EventObserver, {
  getValue: function() {
    return Form.serialize(this.element);
  }
});
if (!window.Event) var Event = { };

Object.extend(Event, {
  KEY_BACKSPACE: 8,
  KEY_TAB:       9,
  KEY_RETURN:   13,
  KEY_ESC:      27,
  KEY_LEFT:     37,
  KEY_UP:       38,
  KEY_RIGHT:    39,
  KEY_DOWN:     40,
  KEY_DELETE:   46,
  KEY_HOME:     36,
  KEY_END:      35,
  KEY_PAGEUP:   33,
  KEY_PAGEDOWN: 34,
  KEY_INSERT:   45,

  cache: { },

  relatedTarget: function(event) {
    var element;
    switch(event.type) {
      case 'mouseover': element = event.fromElement; break;
      case 'mouseout':  element = event.toElement;   break;
      default: return null;
    }
    return Element.extend(element);
  }
});

Event.Methods = (function() {
  var isButton;

  if (Prototype.Browser.IE) {
    var buttonMap = { 0: 1, 1: 4, 2: 2 };
    isButton = function(event, code) {
      return event.button == buttonMap[code];
    };

  } else if (Prototype.Browser.WebKit) {
    isButton = function(event, code) {
      switch (code) {
        case 0: return event.which == 1 && !event.metaKey;
        case 1: return event.which == 1 && event.metaKey;
        default: return false;
      }
    };

  } else {
    isButton = function(event, code) {
      return event.which ? (event.which === code + 1) : (event.button === code);
    };
  }

  return {
    isLeftClick:   function(event) { return isButton(event, 0) },
    isMiddleClick: function(event) { return isButton(event, 1) },
    isRightClick:  function(event) { return isButton(event, 2) },

    element: function(event) {
      var node = Event.extend(event).target;
      return Element.extend(node.nodeType == Node.TEXT_NODE ? node.parentNode : node);
    },

    findElement: function(event, expression) {
      var element = Event.element(event);
      return element.match(expression) ? element : element.up(expression);
    },

    pointer: function(event) {
      return {
        x: event.pageX || (event.clientX +
          (document.documentElement.scrollLeft || document.body.scrollLeft)),
        y: event.pageY || (event.clientY +
          (document.documentElement.scrollTop || document.body.scrollTop))
      };
    },

    pointerX: function(event) { return Event.pointer(event).x },
    pointerY: function(event) { return Event.pointer(event).y },

    stop: function(event) {
      Event.extend(event);
      event.preventDefault();
      event.stopPropagation();
      event.stopped = true;
    }
  };
})();

Event.extend = (function() {
  var methods = Object.keys(Event.Methods).inject({ }, function(m, name) {
    m[name] = Event.Methods[name].methodize();
    return m;
  });

  if (Prototype.Browser.IE) {
    Object.extend(methods, {
      stopPropagation: function() { this.cancelBubble = true },
      preventDefault:  function() { this.returnValue = false },
      inspect: function() { return "[object Event]" }
    });

    return function(event) {
      if (!event) return false;
      if (event._extendedByPrototype) return event;

      event._extendedByPrototype = Prototype.emptyFunction;
      var pointer = Event.pointer(event);
      Object.extend(event, {
        target: event.srcElement,
        relatedTarget: Event.relatedTarget(event),
        pageX:  pointer.x,
        pageY:  pointer.y
      });
      return Object.extend(event, methods);
    };

  } else {
    Event.prototype = Event.prototype || document.createEvent("HTMLEvents").__proto__;
    Object.extend(Event.prototype, methods);
    return Prototype.K;
  }
})();

Object.extend(Event, (function() {
  var cache = Event.cache;

  function getEventID(element) {
    if (element._eventID) return element._eventID;
    arguments.callee.id = arguments.callee.id || 1;
    return element._eventID = ++arguments.callee.id;
  }

  function getDOMEventName(eventName) {
    if (eventName && eventName.include(':')) return "dataavailable";
    return eventName;
  }

  function getCacheForID(id) {
    return cache[id] = cache[id] || { };
  }

  function getWrappersForEventName(id, eventName) {
    var c = getCacheForID(id);
    return c[eventName] = c[eventName] || [];
  }

  function createWrapper(element, eventName, handler) {
    var id = getEventID(element);
    var c = getWrappersForEventName(id, eventName);
    if (c.pluck("handler").include(handler)) return false;

    var wrapper = function(event) {
      if (!Event || !Event.extend ||
        (event.eventName && event.eventName != eventName))
          return false;

      Event.extend(event);
      handler.call(element, event)
    };

    wrapper.handler = handler;
    c.push(wrapper);
    return wrapper;
  }

  function findWrapper(id, eventName, handler) {
    var c = getWrappersForEventName(id, eventName);
    return c.find(function(wrapper) { return wrapper.handler == handler });
  }

  function destroyWrapper(id, eventName, handler) {
    var c = getCacheForID(id);
    if (!c[eventName]) return false;
    c[eventName] = c[eventName].without(findWrapper(id, eventName, handler));
  }

  function destroyCache() {
    for (var id in cache)
      for (var eventName in cache[id])
        cache[id][eventName] = null;
  }

  if (window.attachEvent) {
    window.attachEvent("onunload", destroyCache);
  }

  return {
    observe: function(element, eventName, handler) {
      element = $(element);
      var name = getDOMEventName(eventName);

      var wrapper = createWrapper(element, eventName, handler);
      if (!wrapper) return element;

      if (element.addEventListener) {
        element.addEventListener(name, wrapper, false);
      } else {
        element.attachEvent("on" + name, wrapper);
      }

      return element;
    },

    stopObserving: function(element, eventName, handler) {
      element = $(element);
      var id = getEventID(element), name = getDOMEventName(eventName);

      if (!handler && eventName) {
        getWrappersForEventName(id, eventName).each(function(wrapper) {
          element.stopObserving(eventName, wrapper.handler);
        });
        return element;

      } else if (!eventName) {
        Object.keys(getCacheForID(id)).each(function(eventName) {
          element.stopObserving(eventName);
        });
        return element;
      }

      var wrapper = findWrapper(id, eventName, handler);
      if (!wrapper) return element;

      if (element.removeEventListener) {
        element.removeEventListener(name, wrapper, false);
      } else {
        element.detachEvent("on" + name, wrapper);
      }

      destroyWrapper(id, eventName, handler);

      return element;
    },

    fire: function(element, eventName, memo) {
      element = $(element);
      if (element == document && document.createEvent && !element.dispatchEvent)
        element = document.documentElement;

      if (document.createEvent) {
        var event = document.createEvent("HTMLEvents");
        event.initEvent("dataavailable", true, true);
      } else {
        var event = document.createEventObject();
        event.eventType = "ondataavailable";
      }

      event.eventName = eventName;
      event.memo = memo || { };

      if (document.createEvent) {
        element.dispatchEvent(event);
      } else {
        element.fireEvent(event.eventType, event);
      }

      return event;
    }
  };
})());

Object.extend(Event, Event.Methods);

Element.addMethods({
  fire:          Event.fire,
  observe:       Event.observe,
  stopObserving: Event.stopObserving
});

Object.extend(document, {
  fire:          Element.Methods.fire.methodize(),
  observe:       Element.Methods.observe.methodize(),
  stopObserving: Element.Methods.stopObserving.methodize()
});

(function() {
  /* Support for the DOMContentLoaded event is based on work by Dan Webb,
     Matthias Miller, Dean Edwards and John Resig. */

  var timer, fired = false;

  function fireContentLoadedEvent() {
    if (fired) return;
    if (timer) window.clearInterval(timer);
    document.fire("dom:loaded");
    fired = true;
  }

  if (document.addEventListener) {
    if (Prototype.Browser.WebKit) {
      timer = window.setInterval(function() {
        if (/loaded|complete/.test(document.readyState))
          fireContentLoadedEvent();
      }, 0);

      Event.observe(window, "load", fireContentLoadedEvent);

    } else {
      document.addEventListener("DOMContentLoaded",
        fireContentLoadedEvent, false);
    }

  } else {
    document.write("<script id=__onDOMContentLoaded defer src=//:><\/script>");
    $("__onDOMContentLoaded").onreadystatechange = function() {
      if (this.readyState == "complete") {
        this.onreadystatechange = null;
        fireContentLoadedEvent();
      }
    };
  }
})();
/*------------------------------- DEPRECATED -------------------------------*/

Hash.toQueryString = Object.toQueryString;

var Toggle = { display: Element.toggle };

Element.Methods.childOf = Element.Methods.descendantOf;

var Insertion = {
  Before: function(element, content) {
    return Element.insert(element, {before:content});
  },

  Top: function(element, content) {
    return Element.insert(element, {top:content});
  },

  Bottom: function(element, content) {
    return Element.insert(element, {bottom:content});
  },

  After: function(element, content) {
    return Element.insert(element, {after:content});
  }
};

var $continue = new Error('"throw $continue" is deprecated, use "return" instead');

// This should be moved to script.aculo.us; notice the deprecated methods
// further below, that map to the newer Element methods.
var Position = {
  // set to true if needed, warning: firefox performance problems
  // NOT neeeded for page scrolling, only if draggable contained in
  // scrollable elements
  includeScrollOffsets: false,

  // must be called before calling withinIncludingScrolloffset, every time the
  // page is scrolled
  prepare: function() {
    this.deltaX =  window.pageXOffset
                || document.documentElement.scrollLeft
                || document.body.scrollLeft
                || 0;
    this.deltaY =  window.pageYOffset
                || document.documentElement.scrollTop
                || document.body.scrollTop
                || 0;
  },

  // caches x/y coordinate pair to use with overlap
  within: function(element, x, y) {
    if (this.includeScrollOffsets)
      return this.withinIncludingScrolloffsets(element, x, y);
    this.xcomp = x;
    this.ycomp = y;
    this.offset = Element.cumulativeOffset(element);

    return (y >= this.offset[1] &&
            y <  this.offset[1] + element.offsetHeight &&
            x >= this.offset[0] &&
            x <  this.offset[0] + element.offsetWidth);
  },

  withinIncludingScrolloffsets: function(element, x, y) {
    var offsetcache = Element.cumulativeScrollOffset(element);

    this.xcomp = x + offsetcache[0] - this.deltaX;
    this.ycomp = y + offsetcache[1] - this.deltaY;
    this.offset = Element.cumulativeOffset(element);

    return (this.ycomp >= this.offset[1] &&
            this.ycomp <  this.offset[1] + element.offsetHeight &&
            this.xcomp >= this.offset[0] &&
            this.xcomp <  this.offset[0] + element.offsetWidth);
  },

  // within must be called directly before
  overlap: function(mode, element) {
    if (!mode) return 0;
    if (mode == 'vertical')
      return ((this.offset[1] + element.offsetHeight) - this.ycomp) /
        element.offsetHeight;
    if (mode == 'horizontal')
      return ((this.offset[0] + element.offsetWidth) - this.xcomp) /
        element.offsetWidth;
  },

  // Deprecation layer -- use newer Element methods now (1.5.2).

  cumulativeOffset: Element.Methods.cumulativeOffset,

  positionedOffset: Element.Methods.positionedOffset,

  absolutize: function(element) {
    Position.prepare();
    return Element.absolutize(element);
  },

  relativize: function(element) {
    Position.prepare();
    return Element.relativize(element);
  },

  realOffset: Element.Methods.cumulativeScrollOffset,

  offsetParent: Element.Methods.getOffsetParent,

  page: Element.Methods.viewportOffset,

  clone: function(source, target, options) {
    options = options || { };
    return Element.clonePosition(target, source, options);
  }
};

/*--------------------------------------------------------------------------*/

if (!document.getElementsByClassName) document.getElementsByClassName = function(instanceMethods){
  function iter(name) {
    return name.blank() ? null : "[contains(concat(' ', @class, ' '), ' " + name + " ')]";
  }

  instanceMethods.getElementsByClassName = Prototype.BrowserFeatures.XPath ?
  function(element, className) {
    className = className.toString().strip();
    var cond = /\s/.test(className) ? $w(className).map(iter).join('') : iter(className);
    return cond ? document._getElementsByXPath('.//*' + cond, element) : [];
  } : function(element, className) {
    className = className.toString().strip();
    var elements = [], classNames = (/\s/.test(className) ? $w(className) : null);
    if (!classNames && !className) return elements;

    var nodes = $(element).getElementsByTagName('*');
    className = ' ' + className + ' ';

    for (var i = 0, child, cn; child = nodes[i]; i++) {
      if (child.className && (cn = ' ' + child.className + ' ') && (cn.include(className) ||
          (classNames && classNames.all(function(name) {
            return !name.toString().blank() && cn.include(' ' + name + ' ');
          }))))
        elements.push(Element.extend(child));
    }
    return elements;
  };

  return function(className, parentElement) {
    return $(parentElement || document.body).getElementsByClassName(className);
  };
}(Element.Methods);

/*--------------------------------------------------------------------------*/

Element.ClassNames = Class.create();
Element.ClassNames.prototype = {
  initialize: function(element) {
    this.element = $(element);
  },

  _each: function(iterator) {
    this.element.className.split(/\s+/).select(function(name) {
      return name.length > 0;
    })._each(iterator);
  },

  set: function(className) {
    this.element.className = className;
  },

  add: function(classNameToAdd) {
    if (this.include(classNameToAdd)) return;
    this.set($A(this).concat(classNameToAdd).join(' '));
  },

  remove: function(classNameToRemove) {
    if (!this.include(classNameToRemove)) return;
    this.set($A(this).without(classNameToRemove).join(' '));
  },

  toString: function() {
    return $A(this).join(' ');
  }
};

Object.extend(Element.ClassNames.prototype, Enumerable);

/*--------------------------------------------------------------------------*/

Element.addMethods();
/* ---- scriptaculous ---- */
// script.aculo.us scriptaculous.js v1.8.1, Thu Jan 03 22:07:12 -0500 2008

// Copyright (c) 2005-2007 Thomas Fuchs (http://script.aculo.us, http://mir.aculo.us)
// 
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// For details, see the script.aculo.us web site: http://script.aculo.us/

var Scriptaculous = {
  Version: '1.8.1',
  require: function(libraryName) {
    // inserting via DOM fails in Safari 2.0, so brute force approach
    document.write('<script type="text/javascript" src="'+libraryName+'"><\/script>');
  },
  REQUIRED_PROTOTYPE: '1.6.0',
  load: function() {
    function convertVersionString(versionString){
      var r = versionString.split('.');
      return parseInt(r[0])*100000 + parseInt(r[1])*1000 + parseInt(r[2]);
    }
 
    if((typeof Prototype=='undefined') || 
       (typeof Element == 'undefined') || 
       (typeof Element.Methods=='undefined') ||
       (convertVersionString(Prototype.Version) < 
        convertVersionString(Scriptaculous.REQUIRED_PROTOTYPE)))
       throw("script.aculo.us requires the Prototype JavaScript framework >= " +
        Scriptaculous.REQUIRED_PROTOTYPE);
    
    $A(document.getElementsByTagName("script")).findAll( function(s) {
      return (s.src && s.src.match(/scriptaculous\.js(\?.*)?$/))
    }).each( function(s) {
      var path = s.src.replace(/scriptaculous\.js(\?.*)?$/,'');
      var includes = s.src.match(/\?.*load=([a-z,]*)/);
      (includes ? includes[1] : 'builder,effects,dragdrop,controls,slider,sound').split(',').each(
       function(include) { Scriptaculous.require(path+include+'.js') });
    });
  }
}

Scriptaculous.load();
/* ---- effects ---- */
// script.aculo.us effects.js v1.8.1, Thu Jan 03 22:07:12 -0500 2008

// Copyright (c) 2005-2007 Thomas Fuchs (http://script.aculo.us, http://mir.aculo.us)
// Contributors:
//  Justin Palmer (http://encytemedia.com/)
//  Mark Pilgrim (http://diveintomark.org/)
//  Martin Bialasinki
// 
// script.aculo.us is freely distributable under the terms of an MIT-style license.
// For details, see the script.aculo.us web site: http://script.aculo.us/ 

// converts rgb() and #xxx to #xxxxxx format,  
// returns self (or first argument) if not convertable  
String.prototype.parseColor = function() {  
  var color = '#';
  if (this.slice(0,4) == 'rgb(') {  
    var cols = this.slice(4,this.length-1).split(',');  
    var i=0; do { color += parseInt(cols[i]).toColorPart() } while (++i<3);  
  } else {  
    if (this.slice(0,1) == '#') {  
      if (this.length==4) for(var i=1;i<4;i++) color += (this.charAt(i) + this.charAt(i)).toLowerCase();  
      if (this.length==7) color = this.toLowerCase();  
    }  
  }  
  return (color.length==7 ? color : (arguments[0] || this));  
};

/*--------------------------------------------------------------------------*/

Element.collectTextNodes = function(element) {  
  return $A($(element).childNodes).collect( function(node) {
    return (node.nodeType==3 ? node.nodeValue : 
      (node.hasChildNodes() ? Element.collectTextNodes(node) : ''));
  }).flatten().join('');
};

Element.collectTextNodesIgnoreClass = function(element, className) {  
  return $A($(element).childNodes).collect( function(node) {
    return (node.nodeType==3 ? node.nodeValue : 
      ((node.hasChildNodes() && !Element.hasClassName(node,className)) ? 
        Element.collectTextNodesIgnoreClass(node, className) : ''));
  }).flatten().join('');
};

Element.setContentZoom = function(element, percent) {
  element = $(element);  
  element.setStyle({fontSize: (percent/100) + 'em'});   
  if (Prototype.Browser.WebKit) window.scrollBy(0,0);
  return element;
};

Element.getInlineOpacity = function(element){
  return $(element).style.opacity || '';
};

Element.forceRerendering = function(element) {
  try {
    element = $(element);
    var n = document.createTextNode(' ');
    element.appendChild(n);
    element.removeChild(n);
  } catch(e) { }
};

/*--------------------------------------------------------------------------*/

var Effect = {
  _elementDoesNotExistError: {
    name: 'ElementDoesNotExistError',
    message: 'The specified DOM element does not exist, but is required for this effect to operate'
  },
  Transitions: {
    linear: Prototype.K,
    sinoidal: function(pos) {
      return (-Math.cos(pos*Math.PI)/2) + 0.5;
    },
    reverse: function(pos) {
      return 1-pos;
    },
    flicker: function(pos) {
      var pos = ((-Math.cos(pos*Math.PI)/4) + 0.75) + Math.random()/4;
      return pos > 1 ? 1 : pos;
    },
    wobble: function(pos) {
      return (-Math.cos(pos*Math.PI*(9*pos))/2) + 0.5;
    },
    pulse: function(pos, pulses) { 
      pulses = pulses || 5; 
      return (
        ((pos % (1/pulses)) * pulses).round() == 0 ? 
              ((pos * pulses * 2) - (pos * pulses * 2).floor()) : 
          1 - ((pos * pulses * 2) - (pos * pulses * 2).floor())
        );
    },
    spring: function(pos) { 
      return 1 - (Math.cos(pos * 4.5 * Math.PI) * Math.exp(-pos * 6)); 
    },
    none: function(pos) {
      return 0;
    },
    full: function(pos) {
      return 1;
    }
  },
  DefaultOptions: {
    duration:   1.0,   // seconds
    fps:        100,   // 100= assume 66fps max.
    sync:       false, // true for combining
    from:       0.0,
    to:         1.0,
    delay:      0.0,
    queue:      'parallel'
  },
  tagifyText: function(element) {
    var tagifyStyle = 'position:relative';
    if (Prototype.Browser.IE) tagifyStyle += ';zoom:1';
    
    element = $(element);
    $A(element.childNodes).each( function(child) {
      if (child.nodeType==3) {
        child.nodeValue.toArray().each( function(character) {
          element.insertBefore(
            new Element('span', {style: tagifyStyle}).update(
              character == ' ' ? String.fromCharCode(160) : character), 
              child);
        });
        Element.remove(child);
      }
    });
  },
  multiple: function(element, effect) {
    var elements;
    if (((typeof element == 'object') || 
        Object.isFunction(element)) && 
       (element.length))
      elements = element;
    else
      elements = $(element).childNodes;
      
    var options = Object.extend({
      speed: 0.1,
      delay: 0.0
    }, arguments[2] || { });
    var masterDelay = options.delay;

    $A(elements).each( function(element, index) {
      new effect(element, Object.extend(options, { delay: index * options.speed + masterDelay }));
    });
  },
  PAIRS: {
    'slide':  ['SlideDown','SlideUp'],
    'blind':  ['BlindDown','BlindUp'],
    'appear': ['Appear','Fade']
  },
  toggle: function(element, effect) {
    element = $(element);
    effect = (effect || 'appear').toLowerCase();
    var options = Object.extend({
      queue: { position:'end', scope:(element.id || 'global'), limit: 1 }
    }, arguments[2] || { });
    Effect[element.visible() ? 
      Effect.PAIRS[effect][1] : Effect.PAIRS[effect][0]](element, options);
  }
};

Effect.DefaultOptions.transition = Effect.Transitions.sinoidal;

/* ------------- core effects ------------- */

Effect.ScopedQueue = Class.create(Enumerable, {
  initialize: function() {
    this.effects  = [];
    this.interval = null;    
  },
  _each: function(iterator) {
    this.effects._each(iterator);
  },
  add: function(effect) {
    var timestamp = new Date().getTime();
    
    var position = Object.isString(effect.options.queue) ? 
      effect.options.queue : effect.options.queue.position;
    
    switch(position) {
      case 'front':
        // move unstarted effects after this effect  
        this.effects.findAll(function(e){ return e.state=='idle' }).each( function(e) {
            e.startOn  += effect.finishOn;
            e.finishOn += effect.finishOn;
          });
        break;
      case 'with-last':
        timestamp = this.effects.pluck('startOn').max() || timestamp;
        break;
      case 'end':
        // start effect after last queued effect has finished
        timestamp = this.effects.pluck('finishOn').max() || timestamp;
        break;
    }
    
    effect.startOn  += timestamp;
    effect.finishOn += timestamp;

    if (!effect.options.queue.limit || (this.effects.length < effect.options.queue.limit))
      this.effects.push(effect);
    
    if (!this.interval)
      this.interval = setInterval(this.loop.bind(this), 15);
  },
  remove: function(effect) {
    this.effects = this.effects.reject(function(e) { return e==effect });
    if (this.effects.length == 0) {
      clearInterval(this.interval);
      this.interval = null;
    }
  },
  loop: function() {
    var timePos = new Date().getTime();
    for(var i=0, len=this.effects.length;i<len;i++) 
      this.effects[i] && this.effects[i].loop(timePos);
  }
});

Effect.Queues = {
  instances: $H(),
  get: function(queueName) {
    if (!Object.isString(queueName)) return queueName;
    
    return this.instances.get(queueName) ||
      this.instances.set(queueName, new Effect.ScopedQueue());
  }
};
Effect.Queue = Effect.Queues.get('global');

Effect.Base = Class.create({
  position: null,
  start: function(options) {
    function codeForEvent(options,eventName){
      return (
        (options[eventName+'Internal'] ? 'this.options.'+eventName+'Internal(this);' : '') +
        (options[eventName] ? 'this.options.'+eventName+'(this);' : '')
      );
    }
    if (options && options.transition === false) options.transition = Effect.Transitions.linear;
    this.options      = Object.extend(Object.extend({ },Effect.DefaultOptions), options || { });
    this.currentFrame = 0;
    this.state        = 'idle';
    this.startOn      = this.options.delay*1000;
    this.finishOn     = this.startOn+(this.options.duration*1000);
    this.fromToDelta  = this.options.to-this.options.from;
    this.totalTime    = this.finishOn-this.startOn;
    this.totalFrames  = this.options.fps*this.options.duration;
    
    eval('this.render = function(pos){ '+
      'if (this.state=="idle"){this.state="running";'+
      codeForEvent(this.options,'beforeSetup')+
      (this.setup ? 'this.setup();':'')+ 
      codeForEvent(this.options,'afterSetup')+
      '};if (this.state=="running"){'+
      'pos=this.options.transition(pos)*'+this.fromToDelta+'+'+this.options.from+';'+
      'this.position=pos;'+
      codeForEvent(this.options,'beforeUpdate')+
      (this.update ? 'this.update(pos);':'')+
      codeForEvent(this.options,'afterUpdate')+
      '}}');
    
    this.event('beforeStart');
    if (!this.options.sync)
      Effect.Queues.get(Object.isString(this.options.queue) ? 
        'global' : this.options.queue.scope).add(this);
  },
  loop: function(timePos) {
    if (timePos >= this.startOn) {
      if (timePos >= this.finishOn) {
        this.render(1.0);
        this.cancel();
        this.event('beforeFinish');
        if (this.finish) this.finish(); 
        this.event('afterFinish');
        return;  
      }
      var pos   = (timePos - this.startOn) / this.totalTime,
          frame = (pos * this.totalFrames).round();
      if (frame > this.currentFrame) {
        this.render(pos);
        this.currentFrame = frame;
      }
    }
  },
  cancel: function() {
    if (!this.options.sync)
      Effect.Queues.get(Object.isString(this.options.queue) ? 
        'global' : this.options.queue.scope).remove(this);
    this.state = 'finished';
  },
  event: function(eventName) {
    if (this.options[eventName + 'Internal']) this.options[eventName + 'Internal'](this);
    if (this.options[eventName]) this.options[eventName](this);
  },
  inspect: function() {
    var data = $H();
    for(property in this)
      if (!Object.isFunction(this[property])) data.set(property, this[property]);
    return '#<Effect:' + data.inspect() + ',options:' + $H(this.options).inspect() + '>';
  }
});

Effect.Parallel = Class.create(Effect.Base, {
  initialize: function(effects) {
    this.effects = effects || [];
    this.start(arguments[1]);
  },
  update: function(position) {
    this.effects.invoke('render', position);
  },
  finish: function(position) {
    this.effects.each( function(effect) {
      effect.render(1.0);
      effect.cancel();
      effect.event('beforeFinish');
      if (effect.finish) effect.finish(position);
      effect.event('afterFinish');
    });
  }
});

Effect.Tween = Class.create(Effect.Base, {
  initialize: function(object, from, to) {
    object = Object.isString(object) ? $(object) : object;
    var args = $A(arguments), method = args.last(), 
      options = args.length == 5 ? args[3] : null;
    this.method = Object.isFunction(method) ? method.bind(object) :
      Object.isFunction(object[method]) ? object[method].bind(object) : 
      function(value) { object[method] = value };
    this.start(Object.extend({ from: from, to: to }, options || { }));
  },
  update: function(position) {
    this.method(position);
  }
});

Effect.Event = Class.create(Effect.Base, {
  initialize: function() {
    this.start(Object.extend({ duration: 0 }, arguments[0] || { }));
  },
  update: Prototype.emptyFunction
});

Effect.Opacity = Class.create(Effect.Base, {
  initialize: function(element) {
    this.element = $(element);
    if (!this.element) throw(Effect._elementDoesNotExistError);
    // make this work on IE on elements without 'layout'
    if (Prototype.Browser.IE && (!this.element.currentStyle.hasLayout))
      this.element.setStyle({zoom: 1});
    var options = Object.extend({
      from: this.element.getOpacity() || 0.0,
      to:   1.0
    }, arguments[1] || { });
    this.start(options);
  },
  update: function(position) {
    this.element.setOpacity(position);
  }
});

Effect.Move = Class.create(Effect.Base, {
  initialize: function(element) {
    this.element = $(element);
    if (!this.element) throw(Effect._elementDoesNotExistError);
    var options = Object.extend({
      x:    0,
      y:    0,
      mode: 'relative'
    }, arguments[1] || { });
    this.start(options);
  },
  setup: function() {
    this.element.makePositioned();
    this.originalLeft = parseFloat(this.element.getStyle('left') || '0');
    this.originalTop  = parseFloat(this.element.getStyle('top')  || '0');
    if (this.options.mode == 'absolute') {
      this.options.x = this.options.x - this.originalLeft;
      this.options.y = this.options.y - this.originalTop;
    }
  },
  update: function(position) {
    this.element.setStyle({
      left: (this.options.x  * position + this.originalLeft).round() + 'px',
      top:  (this.options.y  * position + this.originalTop).round()  + 'px'
    });
  }
});

// for backwards compatibility
Effect.MoveBy = function(element, toTop, toLeft) {
  return new Effect.Move(element, 
    Object.extend({ x: toLeft, y: toTop }, arguments[3] || { }));
};

Effect.Scale = Class.create(Effect.Base, {
  initialize: function(element, percent) {
    this.element = $(element);
    if (!this.element) throw(Effect._elementDoesNotExistError);
    var options = Object.extend({
      scaleX: true,
      scaleY: true,
      scaleContent: true,
      scaleFromCenter: false,
      scaleMode: 'box',        // 'box' or 'contents' or { } with provided values
      scaleFrom: 100.0,
      scaleTo:   percent
    }, arguments[2] || { });
    this.start(options);
  },
  setup: function() {
    this.restoreAfterFinish = this.options.restoreAfterFinish || false;
    this.elementPositioning = this.element.getStyle('position');
    
    this.originalStyle = { };
    ['top','left','width','height','fontSize'].each( function(k) {
      this.originalStyle[k] = this.element.style[k];
    }.bind(this));
      
    this.originalTop  = this.element.offsetTop;
    this.originalLeft = this.element.offsetLeft;
    
    var fontSize = this.element.getStyle('font-size') || '100%';
    ['em','px','%','pt'].each( function(fontSizeType) {
      if (fontSize.indexOf(fontSizeType)>0) {
        this.fontSize     = parseFloat(fontSize);
        this.fontSizeType = fontSizeType;
      }
    }.bind(this));
    
    this.factor = (this.options.scaleTo - this.options.scaleFrom)/100;
    
    this.dims = null;
    if (this.options.scaleMode=='box')
      this.dims = [this.element.offsetHeight, this.element.offsetWidth];
    if (/^content/.test(this.options.scaleMode))
      this.dims = [this.element.scrollHeight, this.element.scrollWidth];
    if (!this.dims)
      this.dims = [this.options.scaleMode.originalHeight,
                   this.options.scaleMode.originalWidth];
  },
  update: function(position) {
    var currentScale = (this.options.scaleFrom/100.0) + (this.factor * position);
    if (this.options.scaleContent && this.fontSize)
      this.element.setStyle({fontSize: this.fontSize * currentScale + this.fontSizeType });
    this.setDimensions(this.dims[0] * currentScale, this.dims[1] * currentScale);
  },
  finish: function(position) {
    if (this.restoreAfterFinish) this.element.setStyle(this.originalStyle);
  },
  setDimensions: function(height, width) {
    var d = { };
    if (this.options.scaleX) d.width = width.round() + 'px';
    if (this.options.scaleY) d.height = height.round() + 'px';
    if (this.options.scaleFromCenter) {
      var topd  = (height - this.dims[0])/2;
      var leftd = (width  - this.dims[1])/2;
      if (this.elementPositioning == 'absolute') {
        if (this.options.scaleY) d.top = this.originalTop-topd + 'px';
        if (this.options.scaleX) d.left = this.originalLeft-leftd + 'px';
      } else {
        if (this.options.scaleY) d.top = -topd + 'px';
        if (this.options.scaleX) d.left = -leftd + 'px';
      }
    }
    this.element.setStyle(d);
  }
});

Effect.Highlight = Class.create(Effect.Base, {
  initialize: function(element) {
    this.element = $(element);
    if (!this.element) throw(Effect._elementDoesNotExistError);
    var options = Object.extend({ startcolor: '#ffff99' }, arguments[1] || { });
    this.start(options);
  },
  setup: function() {
    // Prevent executing on elements not in the layout flow
    if (this.element.getStyle('display')=='none') { this.cancel(); return; }
    // Disable background image during the effect
    this.oldStyle = { };
    if (!this.options.keepBackgroundImage) {
      this.oldStyle.backgroundImage = this.element.getStyle('background-image');
      this.element.setStyle({backgroundImage: 'none'});
    }
    if (!this.options.endcolor)
      this.options.endcolor = this.element.getStyle('background-color').parseColor('#ffffff');
    if (!this.options.restorecolor)
      this.options.restorecolor = this.element.getStyle('background-color');
    // init color calculations
    this._base  = $R(0,2).map(function(i){ return parseInt(this.options.startcolor.slice(i*2+1,i*2+3),16) }.bind(this));
    this._delta = $R(0,2).map(function(i){ return parseInt(this.options.endcolor.slice(i*2+1,i*2+3),16)-this._base[i] }.bind(this));
  },
  update: function(position) {
    this.element.setStyle({backgroundColor: $R(0,2).inject('#',function(m,v,i){
      return m+((this._base[i]+(this._delta[i]*position)).round().toColorPart()); }.bind(this)) });
  },
  finish: function() {
    this.element.setStyle(Object.extend(this.oldStyle, {
      backgroundColor: this.options.restorecolor
    }));
  }
});

Effect.ScrollTo = function(element) {
  var options = arguments[1] || { },
    scrollOffsets = document.viewport.getScrollOffsets(),
    elementOffsets = $(element).cumulativeOffset(),
    max = (window.height || document.body.scrollHeight) - document.viewport.getHeight();  

  if (options.offset) elementOffsets[1] += options.offset;

  return new Effect.Tween(null,
    scrollOffsets.top,
    elementOffsets[1] > max ? max : elementOffsets[1],
    options,
    function(p){ scrollTo(scrollOffsets.left, p.round()) }
  );
};

/* ------------- combination effects ------------- */

Effect.Fade = function(element) {
  element = $(element);
  var oldOpacity = element.getInlineOpacity();
  var options = Object.extend({
    from: element.getOpacity() || 1.0,
    to:   0.0,
    afterFinishInternal: function(effect) { 
      if (effect.options.to!=0) return;
      effect.element.hide().setStyle({opacity: oldOpacity}); 
    }
  }, arguments[1] || { });
  return new Effect.Opacity(element,options);
};

Effect.Appear = function(element) {
  element = $(element);
  var options = Object.extend({
  from: (element.getStyle('display') == 'none' ? 0.0 : element.getOpacity() || 0.0),
  to:   1.0,
  // force Safari to render floated elements properly
  afterFinishInternal: function(effect) {
    effect.element.forceRerendering();
  },
  beforeSetup: function(effect) {
    effect.element.setOpacity(effect.options.from).show(); 
  }}, arguments[1] || { });
  return new Effect.Opacity(element,options);
};

Effect.Puff = function(element) {
  element = $(element);
  var oldStyle = { 
    opacity: element.getInlineOpacity(), 
    position: element.getStyle('position'),
    top:  element.style.top,
    left: element.style.left,
    width: element.style.width,
    height: element.style.height
  };
  return new Effect.Parallel(
   [ new Effect.Scale(element, 200, 
      { sync: true, scaleFromCenter: true, scaleContent: true, restoreAfterFinish: true }), 
     new Effect.Opacity(element, { sync: true, to: 0.0 } ) ], 
     Object.extend({ duration: 1.0, 
      beforeSetupInternal: function(effect) {
        Position.absolutize(effect.effects[0].element)
      },
      afterFinishInternal: function(effect) {
         effect.effects[0].element.hide().setStyle(oldStyle); }
     }, arguments[1] || { })
   );
};

Effect.BlindUp = function(element) {
  element = $(element);
  element.makeClipping();
  return new Effect.Scale(element, 0,
    Object.extend({ scaleContent: false, 
      scaleX: false, 
      restoreAfterFinish: true,
      afterFinishInternal: function(effect) {
        effect.element.hide().undoClipping();
      } 
    }, arguments[1] || { })
  );
};

Effect.BlindDown = function(element) {
  element = $(element);
  var elementDimensions = element.getDimensions();
  return new Effect.Scale(element, 100, Object.extend({ 
    scaleContent: false, 
    scaleX: false,
    scaleFrom: 0,
    scaleMode: {originalHeight: elementDimensions.height, originalWidth: elementDimensions.width},
    restoreAfterFinish: true,
    afterSetup: function(effect) {
      effect.element.makeClipping().setStyle({height: '0px'}).show(); 
    },  
    afterFinishInternal: function(effect) {
      effect.element.undoClipping();
    }
  }, arguments[1] || { }));
};

Effect.SwitchOff = function(element) {
  element = $(element);
  var oldOpacity = element.getInlineOpacity();
  return new Effect.Appear(element, Object.extend({
    duration: 0.4,
    from: 0,
    transition: Effect.Transitions.flicker,
    afterFinishInternal: function(effect) {
      new Effect.Scale(effect.element, 1, { 
        duration: 0.3, scaleFromCenter: true,
        scaleX: false, scaleContent: false, restoreAfterFinish: true,
        beforeSetup: function(effect) { 
          effect.element.makePositioned().makeClipping();
        },
        afterFinishInternal: function(effect) {
          effect.element.hide().undoClipping().undoPositioned().setStyle({opacity: oldOpacity});
        }
      })
    }
  }, arguments[1] || { }));
};

Effect.DropOut = function(element) {
  element = $(element);
  var oldStyle = {
    top: element.getStyle('top'),
    left: element.getStyle('left'),
    opacity: element.getInlineOpacity() };
  return new Effect.Parallel(
    [ new Effect.Move(element, {x: 0, y: 100, sync: true }), 
      new Effect.Opacity(element, { sync: true, to: 0.0 }) ],
    Object.extend(
      { duration: 0.5,
        beforeSetup: function(effect) {
          effect.effects[0].element.makePositioned(); 
        },
        afterFinishInternal: function(effect) {
          effect.effects[0].element.hide().undoPositioned().setStyle(oldStyle);
        } 
      }, arguments[1] || { }));
};

Effect.Shake = function(element) {
  element = $(element);
  var options = Object.extend({
    distance: 20,
    duration: 0.5
  }, arguments[1] || {});
  var distance = parseFloat(options.distance);
  var split = parseFloat(options.duration) / 10.0;
  var oldStyle = {
    top: element.getStyle('top'),
    left: element.getStyle('left') };
    return new Effect.Move(element,
      { x:  distance, y: 0, duration: split, afterFinishInternal: function(effect) {
    new Effect.Move(effect.element,
      { x: -distance*2, y: 0, duration: split*2,  afterFinishInternal: function(effect) {
    new Effect.Move(effect.element,
      { x:  distance*2, y: 0, duration: split*2,  afterFinishInternal: function(effect) {
    new Effect.Move(effect.element,
      { x: -distance*2, y: 0, duration: split*2,  afterFinishInternal: function(effect) {
    new Effect.Move(effect.element,
      { x:  distance*2, y: 0, duration: split*2,  afterFinishInternal: function(effect) {
    new Effect.Move(effect.element,
      { x: -distance, y: 0, duration: split, afterFinishInternal: function(effect) {
        effect.element.undoPositioned().setStyle(oldStyle);
  }}) }}) }}) }}) }}) }});
};

Effect.SlideDown = function(element) {
  element = $(element).cleanWhitespace();
  // SlideDown need to have the content of the element wrapped in a container element with fixed height!
  var oldInnerBottom = element.down().getStyle('bottom');
  var elementDimensions = element.getDimensions();
  return new Effect.Scale(element, 100, Object.extend({ 
    scaleContent: false, 
    scaleX: false, 
    scaleFrom: window.opera ? 0 : 1,
    scaleMode: {originalHeight: elementDimensions.height, originalWidth: elementDimensions.width},
    restoreAfterFinish: true,
    afterSetup: function(effect) {
      effect.element.makePositioned();
      effect.element.down().makePositioned();
      if (window.opera) effect.element.setStyle({top: ''});
      effect.element.makeClipping().setStyle({height: '0px'}).show(); 
    },
    afterUpdateInternal: function(effect) {
      effect.element.down().setStyle({bottom:
        (effect.dims[0] - effect.element.clientHeight) + 'px' }); 
    },
    afterFinishInternal: function(effect) {
      effect.element.undoClipping().undoPositioned();
      effect.element.down().undoPositioned().setStyle({bottom: oldInnerBottom}); }
    }, arguments[1] || { })
  );
};

Effect.SlideUp = function(element) {
  element = $(element).cleanWhitespace();
  var oldInnerBottom = element.down().getStyle('bottom');
  var elementDimensions = element.getDimensions();
  return new Effect.Scale(element, window.opera ? 0 : 1,
   Object.extend({ scaleContent: false, 
    scaleX: false, 
    scaleMode: 'box',
    scaleFrom: 100,
    scaleMode: {originalHeight: elementDimensions.height, originalWidth: elementDimensions.width},
    restoreAfterFinish: true,
    afterSetup: function(effect) {
      effect.element.makePositioned();
      effect.element.down().makePositioned();
      if (window.opera) effect.element.setStyle({top: ''});
      effect.element.makeClipping().show();
    },  
    afterUpdateInternal: function(effect) {
      effect.element.down().setStyle({bottom:
        (effect.dims[0] - effect.element.clientHeight) + 'px' });
    },
    afterFinishInternal: function(effect) {
      effect.element.hide().undoClipping().undoPositioned();
      effect.element.down().undoPositioned().setStyle({bottom: oldInnerBottom});
    }
   }, arguments[1] || { })
  );
};

// Bug in opera makes the TD containing this element expand for a instance after finish 
Effect.Squish = function(element) {
  return new Effect.Scale(element, window.opera ? 1 : 0, { 
    restoreAfterFinish: true,
    beforeSetup: function(effect) {
      effect.element.makeClipping(); 
    },  
    afterFinishInternal: function(effect) {
      effect.element.hide().undoClipping(); 
    }
  });
};

Effect.Grow = function(element) {
  element = $(element);
  var options = Object.extend({
    direction: 'center',
    moveTransition: Effect.Transitions.sinoidal,
    scaleTransition: Effect.Transitions.sinoidal,
    opacityTransition: Effect.Transitions.full
  }, arguments[1] || { });
  var oldStyle = {
    top: element.style.top,
    left: element.style.left,
    height: element.style.height,
    width: element.style.width,
    opacity: element.getInlineOpacity() };

  var dims = element.getDimensions();    
  var initialMoveX, initialMoveY;
  var moveX, moveY;
  
  switch (options.direction) {
    case 'top-left':
      initialMoveX = initialMoveY = moveX = moveY = 0; 
      break;
    case 'top-right':
      initialMoveX = dims.width;
      initialMoveY = moveY = 0;
      moveX = -dims.width;
      break;
    case 'bottom-left':
      initialMoveX = moveX = 0;
      initialMoveY = dims.height;
      moveY = -dims.height;
      break;
    case 'bottom-right':
      initialMoveX = dims.width;
      initialMoveY = dims.height;
      moveX = -dims.width;
      moveY = -dims.height;
      break;
    case 'center':
      initialMoveX = dims.width / 2;
      initialMoveY = dims.height / 2;
      moveX = -dims.width / 2;
      moveY = -dims.height / 2;
      break;
  }
  
  return new Effect.Move(element, {
    x: initialMoveX,
    y: initialMoveY,
    duration: 0.01, 
    beforeSetup: function(effect) {
      effect.element.hide().makeClipping().makePositioned();
    },
    afterFinishInternal: function(effect) {
      new Effect.Parallel(
        [ new Effect.Opacity(effect.element, { sync: true, to: 1.0, from: 0.0, transition: options.opacityTransition }),
          new Effect.Move(effect.element, { x: moveX, y: moveY, sync: true, transition: options.moveTransition }),
          new Effect.Scale(effect.element, 100, {
            scaleMode: { originalHeight: dims.height, originalWidth: dims.width }, 
            sync: true, scaleFrom: window.opera ? 1 : 0, transition: options.scaleTransition, restoreAfterFinish: true})
        ], Object.extend({
             beforeSetup: function(effect) {
               effect.effects[0].element.setStyle({height: '0px'}).show(); 
             },
             afterFinishInternal: function(effect) {
               effect.effects[0].element.undoClipping().undoPositioned().setStyle(oldStyle); 
             }
           }, options)
      )
    }
  });
};

Effect.Shrink = function(element) {
  element = $(element);
  var options = Object.extend({
    direction: 'center',
    moveTransition: Effect.Transitions.sinoidal,
    scaleTransition: Effect.Transitions.sinoidal,
    opacityTransition: Effect.Transitions.none
  }, arguments[1] || { });
  var oldStyle = {
    top: element.style.top,
    left: element.style.left,
    height: element.style.height,
    width: element.style.width,
    opacity: element.getInlineOpacity() };

  var dims = element.getDimensions();
  var moveX, moveY;
  
  switch (options.direction) {
    case 'top-left':
      moveX = moveY = 0;
      break;
    case 'top-right':
      moveX = dims.width;
      moveY = 0;
      break;
    case 'bottom-left':
      moveX = 0;
      moveY = dims.height;
      break;
    case 'bottom-right':
      moveX = dims.width;
      moveY = dims.height;
      break;
    case 'center':  
      moveX = dims.width / 2;
      moveY = dims.height / 2;
      break;
  }
  
  return new Effect.Parallel(
    [ new Effect.Opacity(element, { sync: true, to: 0.0, from: 1.0, transition: options.opacityTransition }),
      new Effect.Scale(element, window.opera ? 1 : 0, { sync: true, transition: options.scaleTransition, restoreAfterFinish: true}),
      new Effect.Move(element, { x: moveX, y: moveY, sync: true, transition: options.moveTransition })
    ], Object.extend({            
         beforeStartInternal: function(effect) {
           effect.effects[0].element.makePositioned().makeClipping(); 
         },
         afterFinishInternal: function(effect) {
           effect.effects[0].element.hide().undoClipping().undoPositioned().setStyle(oldStyle); }
       }, options)
  );
};

Effect.Pulsate = function(element) {
  element = $(element);
  var options    = arguments[1] || { };
  var oldOpacity = element.getInlineOpacity();
  var transition = options.transition || Effect.Transitions.sinoidal;
  var reverser   = function(pos){ return transition(1-Effect.Transitions.pulse(pos, options.pulses)) };
  reverser.bind(transition);
  return new Effect.Opacity(element, 
    Object.extend(Object.extend({  duration: 2.0, from: 0,
      afterFinishInternal: function(effect) { effect.element.setStyle({opacity: oldOpacity}); }
    }, options), {transition: reverser}));
};

Effect.Fold = function(element) {
  element = $(element);
  var oldStyle = {
    top: element.style.top,
    left: element.style.left,
    width: element.style.width,
    height: element.style.height };
  element.makeClipping();
  return new Effect.Scale(element, 5, Object.extend({   
    scaleContent: false,
    scaleX: false,
    afterFinishInternal: function(effect) {
    new Effect.Scale(element, 1, { 
      scaleContent: false, 
      scaleY: false,
      afterFinishInternal: function(effect) {
        effect.element.hide().undoClipping().setStyle(oldStyle);
      } });
  }}, arguments[1] || { }));
};

Effect.Morph = Class.create(Effect.Base, {
  initialize: function(element) {
    this.element = $(element);
    if (!this.element) throw(Effect._elementDoesNotExistError);
    var options = Object.extend({
      style: { }
    }, arguments[1] || { });
    
    if (!Object.isString(options.style)) this.style = $H(options.style);
    else {
      if (options.style.include(':'))
        this.style = options.style.parseStyle();
      else {
        this.element.addClassName(options.style);
        this.style = $H(this.element.getStyles());
        this.element.removeClassName(options.style);
        var css = this.element.getStyles();
        this.style = this.style.reject(function(style) {
          return style.value == css[style.key];
        });
        options.afterFinishInternal = function(effect) {
          effect.element.addClassName(effect.options.style);
          effect.transforms.each(function(transform) {
            effect.element.style[transform.style] = '';
          });
        }
      }
    }
    this.start(options);
  },
  
  setup: function(){
    function parseColor(color){
      if (!color || ['rgba(0, 0, 0, 0)','transparent'].include(color)) color = '#ffffff';
      color = color.parseColor();
      return $R(0,2).map(function(i){
        return parseInt( color.slice(i*2+1,i*2+3), 16 ) 
      });
    }
    this.transforms = this.style.map(function(pair){
      var property = pair[0], value = pair[1], unit = null;

      if (value.parseColor('#zzzzzz') != '#zzzzzz') {
        value = value.parseColor();
        unit  = 'color';
      } else if (property == 'opacity') {
        value = parseFloat(value);
        if (Prototype.Browser.IE && (!this.element.currentStyle.hasLayout))
          this.element.setStyle({zoom: 1});
      } else if (Element.CSS_LENGTH.test(value)) {
          var components = value.match(/^([\+\-]?[0-9\.]+)(.*)$/);
          value = parseFloat(components[1]);
          unit = (components.length == 3) ? components[2] : null;
      }

      var originalValue = this.element.getStyle(property);
      return { 
        style: property.camelize(), 
        originalValue: unit=='color' ? parseColor(originalValue) : parseFloat(originalValue || 0), 
        targetValue: unit=='color' ? parseColor(value) : value,
        unit: unit
      };
    }.bind(this)).reject(function(transform){
      return (
        (transform.originalValue == transform.targetValue) ||
        (
          transform.unit != 'color' &&
          (isNaN(transform.originalValue) || isNaN(transform.targetValue))
        )
      )
    });
  },
  update: function(position) {
    var style = { }, transform, i = this.transforms.length;
    while(i--)
      style[(transform = this.transforms[i]).style] = 
        transform.unit=='color' ? '#'+
          (Math.round(transform.originalValue[0]+
            (transform.targetValue[0]-transform.originalValue[0])*position)).toColorPart() +
          (Math.round(transform.originalValue[1]+
            (transform.targetValue[1]-transform.originalValue[1])*position)).toColorPart() +
          (Math.round(transform.originalValue[2]+
            (transform.targetValue[2]-transform.originalValue[2])*position)).toColorPart() :
        (transform.originalValue +
          (transform.targetValue - transform.originalValue) * position).toFixed(3) + 
            (transform.unit === null ? '' : transform.unit);
    this.element.setStyle(style, true);
  }
});

Effect.Transform = Class.create({
  initialize: function(tracks){
    this.tracks  = [];
    this.options = arguments[1] || { };
    this.addTracks(tracks);
  },
  addTracks: function(tracks){
    tracks.each(function(track){
      track = $H(track);
      var data = track.values().first();
      this.tracks.push($H({
        ids:     track.keys().first(),
        effect:  Effect.Morph,
        options: { style: data }
      }));
    }.bind(this));
    return this;
  },
  play: function(){
    return new Effect.Parallel(
      this.tracks.map(function(track){
        var ids = track.get('ids'), effect = track.get('effect'), options = track.get('options');
        var elements = [$(ids) || $$(ids)].flatten();
        return elements.map(function(e){ return new effect(e, Object.extend({ sync:true }, options)) });
      }).flatten(),
      this.options
    );
  }
});

Element.CSS_PROPERTIES = $w(
  'backgroundColor backgroundPosition borderBottomColor borderBottomStyle ' + 
  'borderBottomWidth borderLeftColor borderLeftStyle borderLeftWidth ' +
  'borderRightColor borderRightStyle borderRightWidth borderSpacing ' +
  'borderTopColor borderTopStyle borderTopWidth bottom clip color ' +
  'fontSize fontWeight height left letterSpacing lineHeight ' +
  'marginBottom marginLeft marginRight marginTop markerOffset maxHeight '+
  'maxWidth minHeight minWidth opacity outlineColor outlineOffset ' +
  'outlineWidth paddingBottom paddingLeft paddingRight paddingTop ' +
  'right textIndent top width wordSpacing zIndex');
  
Element.CSS_LENGTH = /^(([\+\-]?[0-9\.]+)(em|ex|px|in|cm|mm|pt|pc|\%))|0$/;

String.__parseStyleElement = document.createElement('div');
String.prototype.parseStyle = function(){
  var style, styleRules = $H();
  if (Prototype.Browser.WebKit)
    style = new Element('div',{style:this}).style;
  else {
    String.__parseStyleElement.innerHTML = '<div style="' + this + '"></div>';
    style = String.__parseStyleElement.childNodes[0].style;
  }
  
  Element.CSS_PROPERTIES.each(function(property){
    if (style[property]) styleRules.set(property, style[property]); 
  });
  
  if (Prototype.Browser.IE && this.include('opacity'))
    styleRules.set('opacity', this.match(/opacity:\s*((?:0|1)?(?:\.\d*)?)/)[1]);

  return styleRules;
};

if (document.defaultView && document.defaultView.getComputedStyle) {
  Element.getStyles = function(element) {
    var css = document.defaultView.getComputedStyle($(element), null);
    return Element.CSS_PROPERTIES.inject({ }, function(styles, property) {
      styles[property] = css[property];
      return styles;
    });
  };
} else {
  Element.getStyles = function(element) {
    element = $(element);
    var css = element.currentStyle, styles;
    styles = Element.CSS_PROPERTIES.inject({ }, function(results, property) {
      results[property] = css[property];
      return results;
    });
    if (!styles.opacity) styles.opacity = element.getOpacity();
    return styles;
  };
};

Effect.Methods = {
  morph: function(element, style) {
    element = $(element);
    new Effect.Morph(element, Object.extend({ style: style }, arguments[2] || { }));
    return element;
  },
  visualEffect: function(element, effect, options) {
    element = $(element)
    var s = effect.dasherize().camelize(), klass = s.charAt(0).toUpperCase() + s.substring(1);
    new Effect[klass](element, options);
    return element;
  },
  highlight: function(element, options) {
    element = $(element);
    new Effect.Highlight(element, options);
    return element;
  }
};

$w('fade appear grow shrink fold blindUp blindDown slideUp slideDown '+
  'pulsate shake puff squish switchOff dropOut').each(
  function(effect) { 
    Effect.Methods[effect] = function(element, options){
      element = $(element);
      Effect[effect.charAt(0).toUpperCase() + effect.substring(1)](element, options);
      return element;
    }
  }
);

$w('getInlineOpacity forceRerendering setContentZoom collectTextNodes collectTextNodesIgnoreClass getStyles').each( 
  function(f) { Effect.Methods[f] = Element[f]; }
);

Element.addMethods(Effect.Methods);

/* ---- common ---- */
/****************************/
/* Common utility functions */
/****************************/
/* bugfix for IE6 - reloading background images on mouse over */
try
{	document.execCommand('BackgroundImageCache', false, true);
}
catch(ex)
{
}

function addLoadEvent(func) {
    var oldonload = window.onload;
    if (typeof window.onload != 'function') {
        window.onload = func;
    } else {
        window.onload = function() {
            if (oldonload) {
                oldonload();
            }
            func();
        }
    }
}

/* Find the position of a DOM Element */

function findPosition(obj) {
	var curleft = 0;
    var curtop = 0;
    if (obj.offsetParent) {
		curleft = obj.offsetLeft;
		curtop = obj.offsetTop;
		while (obj = obj.offsetParent) {
			curleft += obj.offsetLeft;
			curtop += obj.offsetTop;
		}
	}
	return [curleft, curtop];      
}

/* Get the scroll offset of the window */

function getScrollXY() {
    var scrOfX = 0, scrOfY = 0;
    if( typeof( window.pageYOffset ) == 'number' ) {
        //Netscape compliant
        scrOfY = window.pageYOffset;
        scrOfX = window.pageXOffset;
    } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
        //DOM compliant
        scrOfY = document.body.scrollTop;
        scrOfX = document.body.scrollLeft;
    } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
        //IE6 standards compliant mode
        scrOfY = document.documentElement.scrollTop;
        scrOfX = document.documentElement.scrollLeft;
    }
    return [ scrOfX, scrOfY ];
}

function cancelBubbling(e) {
  if (!e) {
    var e = window.event
  }
  e.cancelBubble = true
  if (e.stopPropagation) {
    e.stopPropagation()

  }
}


/**************************/
/* get translation for id */
/* condition: array translation_array is available for current language
/**************************/
function getTranslation(key, param1, param2)
{	
	try
	{	var tmpText = translation_array[key];
		var text;
		
		if (! tmpText)
		{	return key;
		}
		
		if (param1){
			var paramSymbol = /\{0\}/gi;
			text = tmpText.replace(paramSymbol, param1);
			// processing parameters
		}
		else
		{	text = tmpText;
		}
		
		if (param2){
			var paramSymbol2 = /\{1\}/gi;
			text = text.replace(paramSymbol2, param2);
		}
		
		return text;
	}
	catch(e)
	{	if (!key)
		{	return key;
		}	
		return "???Function getTranslation: key is not defined";
	}
}

/************************/
/* Global DOM Functions */
/************************/

/* Search for parents node by tagname */
function findParentNodeByTagName(obj, tagName) {

	do {
		obj = obj.parentNode;
	}  while (obj.nodeName != tagName && obj.nodeName != 'BODY')

	return (obj)
}

/* Search parents for element with specific classname */
function findParentNodeByClassName(obj, className) {

	do {
		obj = obj.parentNode;
	}  while ((obj.className.indexOf(className) == -1) && obj.nodeName != 'BODY')

	return (obj)
}

/* Search children for element(s) with specific classname */
function getElementsByClassName(obj, className, looseSearch) {
    /*
    obj = the object to search from;
    classname = the name of the class to search for;
    looseSearch = (default:true), if true then use indexOf ; if false then strict compare;
    */

    var list = new Array();
    var childrenList = obj.getElementsByTagName('DIV');
    var searchCriterium;

    if(!looseSearch)
        searchCriterium = "childrenList[i].className == className";
    else
        searchCriterium = "childrenList[i].className.indexOf(className) != -1";

    for(var i=0; i < childrenList.length; i++) {
        if(eval(searchCriterium)) {
            list.push(childrenList[i]);
            }
    }
    return (list);
}

/************************/
/* More specific Functions */
/************************/

function switchTab (theObj) {
    /* first check if object is already active */
    if(findParentNodeByTagName(theObj, 'LI').className.indexOf('active') != -1) {
        return false;
    }

    /* root element of the tabs container */
    var tabsContainerObj = findParentNodeByClassName(theObj, 'tabbedPanes');
    /* root element of the tabs group */
    var tabsObj = findParentNodeByClassName(theObj, 'tabs');

    /* list of tab buttons */
    var tabItems = findParentNodeByClassName(theObj, 'tabs').getElementsByTagName('A');
    /* list of tab panes */
    var paneList = getElementsByClassName(tabsContainerObj, 'pane', true);

    /* find out the order number of the selected tab */
    var position;

    for (var i=0; i < tabItems.length; i++ ) {
        if (tabItems[i] == theObj) {
            position = i;
            findParentNodeByTagName(tabItems[i], 'LI').className = 'active';
            //theObj.parentNode.className = 'active';
            //alert(theObj.nodeName + "==");
        }
        else {
            findParentNodeByTagName(tabItems[i], 'LI').className = '';
            //alert(theObj.nodeName);
        }
    }

    for (var i=0; i < paneList.length; i++ ) {
        if (i == position) {
            paneList[i].className += ' active';
        }
        else {
            paneList[i].className = paneList[i].className.replace(' active','');
        }
    }
}


function openPopUp (url,w,h,t,l,s,r) {
  myWindow = window.open(url, 'popupWindow', "width="+w+",height="+h+",left="+l+",top="+t+",scrollbars="+s+",resizable="+r+", status=no");
  myWindow.focus();
}


/* social bookmarks */
document.observe('dom:loaded', showBookmarksLink);


function showBookmarksLink() {
	links = document.getElementsByClassName('bookmark');
	if(links) {
		for (i=0; i < links.length; i++) {
			links[i].style.display = 'inline';
		}
	}
}


function showBookmarks() {
	$$('.ratingBox').each(Element.hide);
	$$('.recommendBox').each(Element.hide);
	bookmarkLi = $$('li.bookmark').first();
	$$('.socialBookmarkBox').each(function(el) {
		el.clonePosition(bookmarkLi, {setLeft:true,setTop:false,setWidth:false,setHeight:false,offsetLeft:-4});
	});
	var boxes = document.getElementsByClassName('socialBookmarkBox');
	if(boxes) {
		for (i=0; i < boxes.length; i++) {
			if(boxes[i].style.display != 'block') {
				boxes[i].style.display = 'block';
			} else {
				boxes[i].style.display = 'none';
			}
		}
	}
}

function hideBookmarks() {
	var boxes = document.getElementsByClassName('socialBookmarkBox');
	if(boxes) {
		for (i=0; i < boxes.length; i++) {
			boxes[i].style.display = 'none';
		}
	}
}

function toggleRecommends() {
	hideBookmarks();
	$$('.ratingBox').each(Element.hide);
	recommendLi = $$('li.recommendOption').first();
	$$('.recommendBox').each(function(el) {
		el.clonePosition(recommendLi, {setLeft:true,setTop:false,setWidth:false,setHeight:false,offsetLeft:-4});
		el.toggle();
	});
	return false;
}

function showRecommendEmailForm() {
	//getCounters('story_recommend'); 
	$$('.recommendBox').each(Element.hide);
	return showArticleInteraction('recommendEmail');
}

function showRecommendSmsForm() {
	//getCounters('story_recommend'); 
	$$('.recommendBox').each(Element.hide);
return showArticleInteraction('recommendSms');
}

//form file: comment.jsp
//create the validator at runtime due to the asynchronous determining of logged in user
var commentForm;
function showCommentForm(isApplPlattform)
{	try
	{	ssoUser = getSSOUserId();
		showCaptchaField = !isApplPlattform && ! ssoUser; 
	
		// hide captcha field if exist
		if (showCaptchaField == false)
		{	ssoCaptchaTag = document.getElementById("captchaWrapper");
			if (ssoCaptchaTag)
			{	ssoCaptchaTag.style.display = "none";
			}
		}
	
		// create validator
		commentForm = new CommentFormValidator(showCaptchaField);
	}
	catch (e)
	{	 // alert(e);
	}

	return showArticleInteraction('comment');
}

// form file: reportForm.tag
//create the validator at runtime due to the asynchronous determining of logged in user
var reportCommentForm;
function showReportCommentForm(commentId)
{	try
	{	ssoUser = getSSOUserId();
		showCaptchaField = ! ssoUser; 
		
		// hide captcha field for logged in users
		if (showCaptchaField == false)
		{	
			captchaTag = document.getElementById("reportFormCaptcha");
			if (captchaTag)
			{	captchaTag.style.display = "none";
			}
		}
	
		// create validator
		reportCommentForm = new ReportCommentSendFormValidator(showCaptchaField);
	}
	catch (e)
	{	// alert(e);
	}
	
    $$('.recommendBox').each(Element.hide);
    $("reportCommentTitle").innerHTML = $("commentUserName"+commentId).innerHTML;
    $("reportCommentMessage").innerHTML = $("commentMessage"+commentId).innerHTML;
    $("reportComment_form_commentId").value = commentId;
    return showArticleInteraction('reportComment');
}

/* change font size */
newFontSize = 100;
count = 0;

function setFontSize(fs) {
	if (fs < 0) {
		if (count > -3) {
			count -= 1;
			calcFontSize(fs);
		}
	} else {
		if (count < 3) {
			count += 1;
			calcFontSize(fs);
		}
	}
}

function calcFontSize(fs) {
	newFontSize = newFontSize + fs;
	cssFontSize = newFontSize + "%";
	//alert(n);
	document.getElementById('pageWrapper').style.fontSize = cssFontSize;
}


/* counter for comments */
// commentfield count signs function: wird in comment.jsp verwendet
function countSigns(field) {
	var fieldLengthMax = "1500"; // maximale Anzahl einzugebener Zeichen
	fieldLength = field.value.length; // momentane Anzahl eingebener Zeichen

	if (fieldLength>fieldLengthMax ) {
		// Es wird ein Warnhinweis ausgegeben, wieviel Zeichen zuviel eingegeben 
		// wurden. Der Submit wird blockiert. 
		var extraChar = fieldLength - fieldLengthMax;
		document.getElementById('signQuantity').style.color = "#990000";
		document.getElementById('signQuantity').firstChild.nodeValue = 
			getTranslation("extraChar.message",extraChar);
		
	} else {
		counter=fieldLengthMax-fieldLength;
	}
	// Die Anzahl der momentan eingegebenen Zeichen wird in das Element signQuantity 
	// ausgegeben.
	// Ist die momentane Anzahl von Zeichen kleiner als die maximale wird der
	// Warnhinweis im wieder entfernt.
	if (fieldLength<=fieldLengthMax) {
		document.getElementById('signQuantity').style.color = "#000000";
		document.getElementById('signQuantity').firstChild.nodeValue = 
			getTranslation("remainedChar.message",counter);
	}
}


// appends a parameter to a url
function appendUrlParameter( url, parameter, value )
{
    if( !url )
    {
        url = "";
    }
    var anchorPos = url.indexOf("#");
    var anchor = "";
    if( anchorPos >= 0 )
    {
        anchor = url.substr(anchorPos);
        url = url.substr(0, anchorPos);
    }
    if( url.indexOf("?") == -1 )
    {
        url += "?";
    }
    else
    {
        url += "&";
    }
    url = url + parameter + "=" + value + anchor;
    return url;
}

function setUrlAnchor( url, anchor )
{
    if( !url )
    {
        url = "";
    }
    var anchorPos = url.indexOf("#");
    if( anchorPos >= 0 )
    {
        url = url.substr(0, anchorPos);
    }
    url = url + "#" + anchor;
    return url;
}

// open print version page
function openPrintWindow(url)
{
    if(!url) {
        url = window.location.href;
    }
    url = appendUrlParameter(url, "print", "yes");
    url = setUrlAnchor(url, "reqdrucken");
    
    window.open( 
        url,
        'printversion',
        'menubar=no,toolbar=no,status=no,width=600,height=400,scrollbars=yes,resizable=yes'
        );
    
    return false;
} // openPrintWindow


/* ---- helper ---- */
var Helper = Class.create({
    se: new Array(
        new Array('www.google.de/search','q='),
        new Array('www.google.at/search','q='),
        new Array('www.google.ch/search','q='),
        new Array('search.live.com/results.aspx','q='),
        new Array('search.yahoo.com/search','p=')
    ),

    initialize: function() {
        this.domUpdateData = new Array();
        this.searchTerms = "";
    },
    
    getSearchTerms: function() {
    	return this.searchTerms;
    },
    
    /*
     * checks if the referrer set and is in array 'se'
     */
    checkReferrerFromSearch: function () {

		if (location.search.indexOf('query=') >= 0) {
			this.getUrlParams('query=', location.search);
			return true;
		}

        if (document.referrer.length != 0) {
    		//alert("---document.referrer.length: " + document.referrer.length);
    		//alert("document.referrer: " + document.referrer);
            this.referrerIsUtf8 = document.referrer.toLowerCase().indexOf('%c3') > 0;
            this.referrer = unescape(document.referrer);
            //alert("referrer: " + this.referrer);
            for(i=0; i < this.se.length; i++) {
        		//alert("se.length: " + this.se.length);
                if (this.referrer.indexOf(this.se[i][0]) > -1 ) {
                	//alert("ist referer im such array: " + (this.referrer.indexOf(this.se[i][0]) > -1 ));
                    if (this.getUrlParams(this.se[i][1], this.referrer)) {
                    	//alert("true case: " + this.getUrlParams(this.se[i][1]));
                        return true;
                    }
                    else {
                    	//alert("false case: " + this.getUrlParams(this.se[i][1]));
                        return false;
                    }
                }
            }
        }
        return false;
    },

    /*
     * parse the referrer, if referrer has paramter, search for the index of parameter query 
     * save the value of parameter query in this.searchTerms
     * return true if referrer has parameter 'query'
     */
    getUrlParams: function (query, params) {
    	hasParameterQuery = false;
        if (params.indexOf('?') > -1) {
        	//alert("index des ? im referrer: " + params.indexOf('?'));
            splittedReferrer = params.split('?')
		    if (splittedReferrer[1].indexOf('&') > -1) {
	        	//alert("splittet-referrer: " + splittedReferrer[1]);
                params = splittedReferrer[1].split('&');
            }
		    else {
                params = new Array(splittedReferrer[1]);
            }
            
            
            // parse query for search terms
            for (i=0; i<params.length; i++)
            {
	        	//alert("params[" + i + "]: " + params[i]);
	        	
                if (params[i].indexOf(query) > -1)
                {	//alert("index des query(" + query + ") in params[" + i + "]: " + params[i] + " ist: " + params[i].indexOf(query));
                    keys = params[i].substr(query.length);
    	        	//alert("keys: " + keys);
                    hasParameterQuery = true;
                    // stop condition
                    i = params.length;
                }
		    }

            this.searchTerms = new Array();
            // extract search terms if query was found
            if (hasParameterQuery)
		    {	var regex = new RegExp('"([0-9A-Za-z]*) ([0-9A-Za-z]*)"',"ig");
	            tmpkeys = keys.replace(regex, '$1 $2');
				tmpkeys = keys.replace('%20', '+');

	            if (this.referrerIsUtf8)
	            {	tmpkeys = UTF8.decode(tmpkeys);
	            }
	            
	            //alert("tmpkeys: " + tmpkeys);
	
	            // at least maximum of 8 search terms
	            var tmpSearchTerms = tmpkeys.split('+').slice(0,8);
	
	            for (i=0; i<tmpSearchTerms.length; i++)
	            {	currentElem = tmpSearchTerms[i];

	            	// ignore google search term like site:welt.de
	            	if (currentElem.indexOf("site:") < 0)
	            	{	this.searchTerms.push(currentElem);	            		
	            	}
	            }
		    }

//alert("this.searchTerms: " + this.searchTerms);            
            //alert("hasParameterQuery: " + hasParameterQuery);
        }
        
        return hasParameterQuery;
    },
    
    debug:function(text) {
        return;
        document.writeln(text + "<br/>");
    }

});

//+ Jonas Raoni Soares Silva
//@ http://jsfromhell.com/geral/utf-8 [v1.0]
UTF8 = {
    encode: function(s){
  		for(var c, i = -1, l = (s = s.split("")).length, o = String.fromCharCode; ++i < l;
  			s[i] = (c = s[i].charCodeAt(0)) >= 127 ? o(0xc0 | (c >>> 6)) + o(0x80 | (c & 0x3f)) : s[i]
  		);
  		return s.join("");
  	},
  	decode: function(s){
  		for(var a, b, i = -1, l = (s = s.split("")).length, o = String.fromCharCode, c = "charCodeAt"; ++i < l;
  			((a = s[i][c](0)) & 0x80) &&
  			(s[i] = (a & 0xfc) == 0xc0 && ((b = s[i + 1][c](0)) & 0xc0) == 0x80 ?
  			o(((a & 0x03) << 6) + (b & 0x3f)) : o(128), s[++i] = "")
  		);
  		return s.join("");
  	}
};


/* ---- cookies ---- */
/* STATUS: FINAL */

function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name + "=" + encodeURIComponent(value) + expires+"; path=/; domain=.welt.de";
}

/* NEW COOKIE FUNCTION
* all arguments are strings, excepting [days] (integer) and [secure] (boolean)
* unused arguments are set to 'null' as a placeholder
* example: setCookie('cookieName','cookieValue',30,null,'.welt.de')
*/

function setCookie (name,value,days,path,domain,secure) {
	var expires = "";
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		expires = "; expires="+date.toGMTString();
	}
	document.cookie = name + "=" + encodeURIComponent(value) + expires +  
		((path) ? "; path=" + path : "") +
		((domain) ? "; domain=" + domain : "") +
		((secure) ? "; secure" : "");
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	
	for (var i=0;i < ca.length;i++)
	{	var c = ca[i];
		
		while (c.charAt(0)==' ')
		{	c = c.substring(1,c.length);
		}
		
		if (c.indexOf(nameEQ) == 0)
		{	return decodeURIComponent(c.substring(nameEQ.length,c.length));
		}
	}
	
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

// WITHOUT DOMAIN
function createCookieDomainless(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name + "=" + encodeURIComponent(value) + expires+"; path=/";
}

function eraseCookieDomainless(name)
{	createCookieDomainless(name,"",-1);
}
/* ---- modules ---- */
/*
	This file contains all the javascripts that are used by the modules section in the welt online site
*/

/*
	Javascript functions for the poll form
*/
function ShowHide(id) {
	if ($("vote-"+id) != undefined) {
		$("vote-"+id).hide();
		$("voted-"+id).show();
	}
	if ($("vote-"+id+"-art") != undefined) {
		$("vote-"+id+"-art").hide();
		$("voted-"+id+"-art").show();
	}
	getCounters('index_poll');
	return false;
}

function pollSubmit(id) {
	getCounters('index_poll');
	var redirectTo = location.pathname + location.search + "#vote_" + id ;
	var pollForm = $('pollForm_' + id);	
	// Check how many boxes are checked
	var inputs = pollForm.getElementsByTagName ('input');
	var count = 0;
	var inputString = "";
    for (var i = 0; i < inputs.length; ++i) {
    	if ((inputs[i].type == 'checkbox' || inputs[i].type == 'radio') && inputs[i].name.substr(0, 4) == 'vote') {
    		if (inputs[i].checked == true) {
    			count++;
    		}
    	}
    	
    }
    // Return if no vote was checked
    if (count == 0) {
    	alert("Es wurde keine Stimme abgegeben.");
    	return false;
    }    
	pollForm.redirectTo.value = redirectTo;
	pollForm.submit();
	return false;	
}

function initialHide(id) {
  //alert('initialHide'+id);
  try {
  	//hide poll Q&A and show Results
  	var mentometer = readCookie("mentometer");
  	if (mentometer != null) {
  	  var mentometer_array = mentometer.split("M");
  	  for (i = 0; i < mentometer_array.length; i++) {
    		if (mentometer_array[i] != 'null') {
    		  if (mentometer_array[i].match(id)) {
    			  ShowHide(id);
    		  }
    		}
  	  }
  	}
  } catch (erm) {
	  //alert("displayPoll" + erm);
  }
}
/*
	end Javascript functions for the poll forum
*/



/*
 *	Start function for tabs
 */
var TabHash = {};

function showTab(tabgroup, panelid, islasttab)
{	//alert("showTab " + tabgroup + " " + panelid + " " + islasttab);
	/* First check if there's 1 tab selected (the first time its the 1st tab probably) */
	if (TabHash[tabgroup])
	{
		// If the selected tab is the lasttab 
		if (TabHash[tabgroup][2]){
			// Set a different deselect-style 
			TabHash[tabgroup][0].className="unselectedtablast";
		}else{
			// If not, set the default deselect-style
			TabHash[tabgroup][0].className="unselectedtab";
		}
		TabHash[tabgroup][1].style.display= "none";
	}
	/* Then remember the new selected Tab and remember if the tab is the lasttab (islasttab boolean) */
	TabHash[tabgroup] = [document.getElementById(tabgroup + "_" + panelid + "_tab"), document.getElementById(tabgroup + "_" + panelid), islasttab];
	TabHash[tabgroup][0].className="selected"; 
	TabHash[tabgroup][1].style.display = "block";
	/* If select tab is the last tab, show 'padding-line' */
    /* deactivated - not needed in new layout */

/*	if(islasttab){
		document.getElementById('tablastend').style.visibility = 'visible';
	}else{
		document.getElementById('tablastend').style.visibility = 'hidden';
	}
*/
}

/*
 *	End function for tabs
 */

/*
 *	Start function for slideshows
 */


var SlideHash = {};

function slide(slidegroup,delta)
{
	i=0;
	if (SlideHash[slidegroup])
	{
	if (!delta) return true;
		i = SlideHash[slidegroup][0] + (delta>0 ? 1 : -1);
		SlideHash[slidegroup][1].style.display="none";
	}

	//check to see if there if buttons still needs to be displayed
	if (document.getElementById(slidegroup + "_" + (i+1)) != null) document.getElementById(slidegroup + "_forward").style.visibility = "visible";
	else document.getElementById(slidegroup + "_forward").style.visibility = "hidden";

	if (document.getElementById(slidegroup + "_" + (i-1)) != null) document.getElementById(slidegroup + "_back").style.visibility = "visible";
	else document.getElementById(slidegroup + "_back").style.visibility = "hidden";

	SlideHash[slidegroup] = [i,document.getElementById(slidegroup + "_" + i)];
	SlideHash[slidegroup][1].style.display="block";
	return true;
}

/*
 *	End function for slideshows
 */


/*
 *	TabEx used for richmedia box
 */

var LOADED = false;
var hash_tab_ex = {};

function setTabInfo(groupid, nrtabs,cn_notsel, cn_sel, cn_first, cn_last, cn_stop, cn_stop_br)
{
        hash_tab_ex[groupid] = {};
        hash_tab_ex[groupid]["classNotSelected"] = cn_notsel;
        hash_tab_ex[groupid]["classSelected"] = cn_sel;
        hash_tab_ex[groupid]["classFirst"] = cn_first;
        hash_tab_ex[groupid]["classLast"] = cn_last;
        hash_tab_ex[groupid]["classStop"] = cn_stop;
        hash_tab_ex[groupid]["classStopBr"] = cn_stop_br;

        hash_tab_ex[groupid]["nrTabs"] = nrtabs;
        hash_tab_ex[groupid]["currentTabNr"] = -1;
}

function addFirstLast(groupid,sel)
{
        result = "";
        prefix = "";

        if (sel) prefix = hash_tab_ex[groupid]["classSelected"] + "_";
        else prefix = hash_tab_ex[groupid]["classNotSelected"] + "_";

        if (hash_tab_ex[groupid]["currentTabNr"] == 0) return " " + prefix + hash_tab_ex[groupid]["classFirst"];
        if (hash_tab_ex[groupid]["nrTabs"] == hash_tab_ex[groupid]["currentTabNr"]+1) return  " " + prefix + hash_tab_ex[groupid]["classLast"];
        return "";
}

function isLastTab(groupid)
{
        return (hash_tab_ex[groupid]["nrTabs"] == hash_tab_ex[groupid]["currentTabNr"]+1);
}

function showTabEx(groupid, tabid, tabnr)
{
        if (!document.getElementById(groupid+"_"+tabid)) return false;;
        if (!hash_tab_ex[groupid]) return false; // not initialized;

        if (hash_tab_ex[groupid]["currentTabNr"] != -1)
        {
                unsel = hash_tab_ex[groupid]["classNotSelected"] + addFirstLast(groupid,false);
                hash_tab_ex[groupid]["currentTabObj"].className = unsel;
                hash_tab_ex[groupid]["currentDivObj"].style.display = "none";
        }

        tabobj = document.getElementById(groupid + "_" + tabid + "_tab");
        divobj = document.getElementById(groupid + "_" + tabid);

        hash_tab_ex[groupid]["currentTabNr"]  = tabnr;
        hash_tab_ex[groupid]["currentTabObj"] = tabobj;
        hash_tab_ex[groupid]["currentDivObj"] = divobj;

        sel = hash_tab_ex[groupid]["classSelected"] + " " + addFirstLast(groupid,true);

        tabobj.className = sel;
        divobj.style.display = "block";

        stop = document.getElementById(groupid + "_stop_tab");
        if (stop) stop.className = (isLastTab(groupid) ? hash_tab_ex[groupid]["classStopBr"] : hash_tab_ex[groupid]["classStop"]);

        return false;
}

function startupRichmedia(divarray, tabhash)
{
	setTabInfo("rmtab", divarray.length, "tab", "tab_selected","firsttab","lasttab","rest","rest break");
	tabhash = tabhash.substring(1);
	var index = -1;

	//hide all tabs and find the id of the to be selected tab
	for (i=0; i<activeTabs.length; i++)
	{
		document.getElementById("rmtab_" + activeTabs[i]).style.display = "none";
		if (tabhash && (activeTabs[i] == tabhash)) index = i;
	}

	//select a tab
	showTabEx("rmtab",activeTabs[0],0);
	if (index >=0)
	{
		showTabEx("rmtab",tabhash,index);
	}
}

/*
 *	End of TabEx
 */

currElem = null;
currElem2 = null;
function showArticleInteraction(id)
{
	    if (currElem != null)
        {
                currElem.style.display = "none";
        }
        if (currElem2 != null)
        {
                currElem2.style.display = "none";
        }

        if (id == "comment")
        {
        	if (document.getElementById("readcomments")) {
			
                	currElem2 = document.getElementById("readcomments");
                	currElem2.style.display = "block";
                	
                	//mvo
                	//document.getElementById("articleBoxReadCommentButton1").style.display = "none";
                	document.getElementById("articleBoxReadCommentButton2").style.display = "none";
                }
        }

        if (id == "read_comments")
        {
			document.location.hash="#article_readcomments";
			return true;
        }
        
		if (!document.getElementById(id)) return true;

		if (captcha = document.getElementById(id + "_captcha_img"))
		{
			captcha.src="/captcha/captcha.jpg?no_cache=" + Math.floor(Math.random() * 9000000 + 1000000);
		}

        currElem = document.getElementById(id);
        currElem.style.display = "block";
        return true;
}

function popupImage(id)
{
        window.open("?service=ImagePopup&id=" + id, "", "scrollbars=no, addressbar=no,statusbar=no, menubar=no");
}


function initByHash()
{	if (document && document.location && document.location.hash)
	{
		switch (document.location.hash)
		{
			case "#article_recommendEmail":
				showArticleInteraction("recommendEmail");
				break;
			case "#article_recommendSms":
				showArticleInteraction("recommendSms");
				break;
			case "#article_comment":
				showArticleInteraction("comment");
				break;
			case "#read_comments":
				showArticleInteraction("comment");
				document.location.hash = "#article_readcomments";
				break;
			case "#msg_comment":
				$("xmsg_comment").show();
				break;
			case "#msg_recommendEmail":
				$("xmsg_recommendEmail").show();
				break;
			case "#msg_recommendSms":
				$("xmsg_recommendSms").show();
				$("xmsg_recommendSms").scrollTo();
				break;
					
			//Recommend E-Mail
			case "#article_recommendEmail_parameterError":
				showArticleInteraction("recommendEmail");
				recommendEmailForm.addNotification(getTranslation("wrongParameter"));
				document.location.hash = "#recommendEmail";
				break;
			case "#article_recommendEmail_parameterEmpty":
				showArticleInteraction("recommendEmail");
				recommendEmailForm.addNotification(getTranslation("wrongParameter"));
				document.location.hash = "#recommendEmail";
				break;
			case "#article_recommendEmail_captcha_failure":
				showArticleInteraction("recommendEmail");
				recommendEmailForm.addNotification(getTranslation("enterNewCode"));
				document.location.hash = "#recommendEmail";
				break;
			case "#article_recommendEmail_captcha_sessionError":
				showArticleInteraction("recommendEmail");
				recommendEmailForm.addNotification(getTranslation("timeOver"));
				document.location.hash = "#recommendEmail";
				break;

			// Recommend SMS
			case "#article_recommendSms_parameterError":
				showArticleInteraction("recommendSms");
				recommendSmsForm.addNotification(getTranslation("wrongParameter"));
				document.location.hash = "#recommendSms";
				break;
			case "#article_recommendSms_parameterEmpty":
				showArticleInteraction("recommendSms");
				recommendSmsForm.addNotification(getTranslation("wrongParameter"));
				document.location.hash = "#recommendSms";
				break;
			case "#article_recommendSms_captcha_failure":
				showArticleInteraction("recommendSms");
				recommendSmsForm.addNotification(getTranslation("enterNewCode"));
				document.location.hash = "#recommendSms";
				break;
			case "#article_recommendSms_captcha_sessionError":
				showArticleInteraction("recommendSms");
				recommendSmsForm.addNotification(getTranslation("timeOver"));
				document.location.hash = "#recommendSms";
				break;
					
			//Comment
			case "#article_kommentar_parameterError":
				showArticleInteraction("comment");
				postingForm.addNotification(getTranslation("checkSpelling"));
				document.location.hash = "#article_comment";
				break;
			case "#article_kommentar_captcha_failure":
				showArticleInteraction("comment");
				postingForm.addNotification(getTranslation("enterNewCode"));
				document.location.hash = "#article_comment";
				break;
			case "#article_kommentar_captcha_sessionError":
				showArticleInteraction("comment");
				postingForm.addNotification(getTranslation("timeOver"));
				document.location.hash = "#article_comment";
				break;
			case "#article_kommentar_forum_succes":
				showArticleInteraction("comment");
				postingForm.addNotification(getTranslation("commentSuccess"));
				document.location.hash="#article_readcomments";
				break;
			case "#article_kommentar_forum_error":
				showArticleInteraction("comment");
				postingForm.addNotification(getTranslation("checkSpelling"));
				document.location.hash="#article_comment";
				break;
			case "#article_kommentar_forum_cancel":
				showArticleInteraction("comment");
				postingForm.addNotification(getTranslation("cancelled"));
				document.location.hash="#article_comment";
				break;

		}
		//this is needed because when this is called the page has already scrolled to the anchor, and if the content after the page is smaller
		//after the anchor then the page it will scroll to the bottom, after which the div is displayed. The div will then not be scrolled
		//into view correctly. This will recall the anchor after the div is displayed, just like the links in the menu do.
		document.location.hash = document.location.hash;
	}

}

/*
 *	Following function is used to underline every element in a mouse over over en exklusiv item
 */



function styleChildren(obj, elem, val)
{
	par = obj;
	for (i=0; i<par.childNodes.length; i++)
	{
		v = "par.childNodes[i].style." + elem + " = \"" + val + "\"";
		try { eval(v); }
		catch (e) {}
	}
}


/*
 *	end underline styleChildren
 */

/* ---- validateForm ---- */
/* 
 * Unobstrusive Form validation class; use to validate a complete form. 
 * Form validation is automatically added.
 * For each field, validation stops at the first failed validator. 
 * 2008 R�diger Schulz (2rue.de) for ASMS
 */
var FormValidator = Class.create({
	cssError: "errorField",
	useAjax: false,
	
	initialize: function(formid) {
		this.form = $(formid);
		if (this.form == null) throw new Error("Form with id " + formid + " not in DOM (yet)");
		this.formid = formid;
		this.alertDiv = $(this.formid + "_alert");
		this.validators = new Array();
		this.errorFields = new Array();
		this.form.observe('submit', this.handleSubmit.bindAsEventListener(this));
  	},	
  	
  	/*
  	 * event listener function for submit.
  	 * validate and/or submit via AJAX 
  	 */
  	handleSubmit: function(event) {
		if (!this.validate()) {
			event.stop();
		}
		else if (this.useAjax) {
			this.executeAjax();
			event.stop();
		}
  	},
  	
  	// enable AJAX submitting; form action must be prepared for that
  	enableAjax: function() {
  		this.useAjax = true;
  	},
  	
  	// can be overridden
  	executeAjax: function() {
		new Ajax.Request(this.form.action, {
			method: this.form.method,
			parameters: this.form.serialize(true),
			evalJS: 'force'
		});
  	},
  	
  	// validate the form, return true if everything validates
  	validate: function() {
  		ret = true;
  		this.clearNotification();
  		this.errorFields = new Array();
  		for (i = 0; i < this.validators.length; i++) {
  			vd = this.validators[i];
  			if (this.errorFields.indexOf(vd.field.name) >= 0) {
  				continue; // only first error per field.
  			}
  			if (!vd.validate()) {
  				this.addNotification(vd.message());
  				vd.field.addClassName(this.cssError);
  				this.errorFields.push(vd.field.name);
  				ret = false;
  			}
  			else {
  				vd.field.removeClassName(this.cssError);
  			}
  		}
  		if (!ret) {
  			this.focusErrors();
  		}
  		return ret;
  	},
  	
  	focusErrors: function() {
		this.alertDiv.scrollTo();
  	},
  	
  	/* validate; if success, submit, else return false
  	 * required for javascript-submits, because those avoid auto-validation via event-handling
  	 */
  	submit: function() {
  		if (this.validate()) {
  			this.form.submit();
  	  		return true;
  		}
  		return false;
  	},

  	// add validators to the validators array
  	pushValidator: function(validator) {
  		this.validators.push(validator);
  	},
  	
  	// add dynamic validator
  	addValidator: function(validatorName, fieldName, description, arguments) {
  		//alert("adding validator " + validatorName + " on " + fieldName + " saying " + description + " using " + arguments);
  		if (arguments != null && !Object.isArray(arguments)) throw new Error("arguments must be an array, was " + arguments);
  		if (!this.form[fieldName]) throw new Error("Cannot validate unknown field named " + fieldName + " in form " + this.form.name);
  		js = 'validator = new ' + validatorName + '(this.form[fieldName], description';
  		if (arguments != null) {
  			for (i = 0; i < arguments.length; i++) {
  				js += ',' + arguments[i]; 
  			}
  		}
  		js += ');';
  		//alert ("evaling " + js);
  		eval(js);
  		this.pushValidator(validator);
  	},
  	
  	// F�gt Warnhinweise in das 'alert div' als <ul>-liste hinzu.
  	addNotification: function(notification) {
  		if (!this.alertDiv.firstChild) {//there is no UL yet
  			ul = document.createElement("ul"); 
  			this.alertDiv.appendChild(ul);
  		}
  		li = document.createElement("li");
  		li.innerHTML = notification;
  		this.alertDiv.firstChild.appendChild(li);
  	},

  	/*
  	 * Clear error notification div.
  	 */
  	clearNotification: function() {
  		this.alertDiv.innerHTML = '';
  	},
  	
  	/* 
  	 * convinient method for saying "this field is required"
  	 * @param requiredFields object with fieldnames as properties, and the field description as their value.
  	 */
  	require: function(requiredFields) {
  		for (fieldName in requiredFields) {
  	  		this.pushValidator(new RequiredValidator(this.form[fieldName], requiredFields[fieldName]));
  		}
  	}
});

/* OO-Validators hierarchy */
// basic interface
var IValidator = Class.create({
	initialize: function(field, description) {
		this.field = $(field); // prototypify!
		if (this.field == null) throw new Error("Unknow form field " + field);
		this.description = description;
	},
	validate: function() { return true; },
	message: function() { return '' }
});

/*
 * basic string validator for textfields, which auto-trims their value
 * subclasses shouldn't override validate(), but validateText(text) 
 */
var TextValidator = Class.create(IValidator, {
	validate: function() {
		this.field.value = this.field.value.strip();
		return this.validateText(this.field.value);
	},
	validateText: function(text) { return true; }
});

// check if field has any value except whitespace
var RequiredValidator = Class.create(IValidator, {
	initialize: function($super, field, description) {
		$super(field, description);
	},
	validate: function() {
		return !$F(this.field).blank();
	},
	message: function() {
		//return getTranslation("Das Feld '") + this.description + getTranslation("' darf nicht leer sein.");
		return getTranslation("emptyField", this.description);
	}
});

// check if field as maximum length
var MaxlengthValidator = Class.create(TextValidator, {
	initialize: function($super, field, description, maxlength) {
		$super(field, description);
		this.maxlength = maxlength;
//		alert("MaxlengthValidator with " + maxlength);
	},
	validateText: function(text) {
//		alert("comparing " + text.length + " with " + this.maxlength);
		return text.length < this.maxlength;
	},
	message: function() {
		return getTranslation("shortenText");
	}
});

// check if field is a german mobile number, by checking the the first 3 digits of the area code (015, 016, 017)
var MobileNumberValidator = Class.create(TextValidator, {
	initialize: function($super, field, description) {
		$super(field, description);
		this.validAreas = ['015', '016', '017'];
	},
	validateText: function(text) {
		text = text.substring(0, 3);
		return this.validAreas.indexOf(text) >= 0;
	},
	message: function() {
		//return getTranslation("Das Feld '") + this.description + getTranslation("' muss einer deutschen Mobilfunknummer entsprechen.");
		return getTranslation("germanMobileNum",this.description);
	}
});

/* generic validation methods */
ValidatorUtils = {
	
	/**
	 * Forces the value of the field to contain only numbers.
	 */
	forceOnlyNumbers: function(event) {
		field = event.element();
		field.value = Utils.removeNonDigits(field.value);
	}
}

/* generic helper functions */
Utils = {
	/**
	 * Remove all non-digits from text, return.
	 */
	removeNonDigits: function(text) {
		ret = '';
		for (i = 0; i < text.length; i++) {
			c = text.charAt(i);
			if (c >= '0' && c <= '9') {
				ret += c;
			}
		}
		return ret;
	}
}


/* ---- picturebars ---- */
/*
STATUS: CHECKED
TODO: maybe add some comments
*/


function imageObject() {
    this.htmlContent = "";
	/* htmlContent replaced by: */
    this.imageUrl = "";
    this.imageWidth = "";
    this.imageHeight = "";
    this.articleId = "";

    this.targetUrl = "";
	this.clickAction = "";
}

function setSelectedGallery(galleryID,numberOfImages){
	selectedGalleryID = galleryID
	n=0;
	populateGallery(selectedGalleryID,numberOfImages)
}

function nextFour(articleID,numberOfImages) {
	n+=numberOfImages
	populateGallery(articleID,numberOfImages);
}

function previousFour(articleID,numberOfImages) {
	if(n>0) {
		n-=numberOfImages
		populateGallery(articleID,numberOfImages);
	}
}

function populateGallery(articleID,numberOfImages){
    var imageObjectDOM;
    var compatabilityMode = false;

    for(var i=0;i<numberOfImages;i++){

        if(i == 0){
			if(n == 0){
				document.getElementById("picturebarBack").style.visibility="hidden";
			}else{
				document.getElementById("picturebarBack").style.visibility="visible";

			}
		}else if(i == (numberOfImages-1)){
			if((n+numberOfImages) < foto[articleID].length){
				document.getElementById("picturebarForward").style.visibility="visible";
			}else{
				document.getElementById("picturebarForward").style.visibility="hidden";
			}
		}

		try
		{

            if(foto[articleID][n+i].htmlContent == "false") {
                //Only used for frontpage
                //Get reference to image object inside the foto hyperlink

                imageObjectDOM = document.getElementById("foto" + i).getElementsByTagName("IMG")[0];

                imageObjectDOM.ImageObjectRef = foto[articleID][n+i]; // Copy reference from ImageObject to DOM IMG object
                imageObjectDOM.src = foto[articleID][n+i].imageUrl;
                imageObjectDOM.width = foto[articleID][n+i].imageWidth;
                imageObjectDOM.height = foto[articleID][n+i].imageHeight;

                //attach the mouse-events to the image object
                imageObjectDOM.onmouseover = function () { showTextPopup(this,'top','outside','medium',false,eval(this.ImageObjectRef.articleId)); };
                imageObjectDOM.onmouseout = function () { hideTextPopup(this); }

                compatabilityMode = false;
            }
            else  {
                //Backward compatibility with picturebar in premiumchannel
                document.getElementById("foto" + i).innerHTML = foto[articleID][n+i].htmlContent;
                compatabilityMode = true;
            }

            var clickActionVar = foto[articleID][n+i].clickAction;

			if(foto[articleID][n+i].targetUrl != ""){
				document.getElementById("fotoLink" + i).href = foto[articleID][n+i].targetUrl;
			}

            if(clickActionVar) {
                if (window.attachEvent) {
                     document.getElementById("fotoLink" + i).onclick = function attachedEvent() { eval(clickActionVar); };
                } else {
                    document.getElementById("fotoLink" + i).setAttribute("onClick", foto[articleID][n+i].clickAction);
                }
            }

            document.getElementById("fotoLink" + i).style.visibility = 'visible';

		}
		catch(err)
		{
            document.getElementById("fotoLink" + i).style.visibility = 'hidden';
            document.getElementById("fotoLink" + i).href = "";
            document.getElementById("fotoLink" + i).onclick = "";

            if(compatabilityMode)
                document.getElementById("foto" + i).innerHTML = "";

        }
	}
}

function switchTabsPictureBar(obj) {
	tabsObj = findParentNodeByTagName(obj, "UL");
	checkObj = findParentNodeByTagName(obj, "LI");

	for (i=0; i<tabsObj.childNodes.length; i++) {

		if(checkObj == tabsObj.childNodes[i])
			tabsObj.childNodes[i].className = 'selected';
		else
			tabsObj.childNodes[i].className = '';
	}
}
/* ---- mediabox ---- */
/*********************/
/* Mediabox functions */
/*********************/

/* Mediabox Object */
function MediaBox (DOMObject) {
	this.DOMObject = DOMObject;
	this.tabs = new Array(); // Array of DOM HTML Elements
	this.panes = new Array(); // Array of MediaBoxPane Objects
	this.activePane = 0;
	this.activeSlide = 0;
	this.previousButton = null; //DOM HTML Element
	this.nextButton = null; //DOM HTML Element
	this.initialized = false;
}

/* Pane Object inside Mediabox */
function MediaBoxPane (DOMObject) {
	this.DOMObject = DOMObject;
	this.slides = new Array();
}

/* Slide Object inside pane */
function MediaBoxPaneSlide (DOMObject) {
	this.DOMObject = DOMObject;
}

/* Initialize Mediabox (on page load) */
function mediaBoxInitialize(theObj) {
	/* If Mediabox was already initialized, skip this routine */

    if((theObj.mediaBoxObject != null) || (theObj.nodeName == "BODY"))
        return false;

    /* Make array of panes */
	try {
		// Instantiate new MediaBox object
		var theMediaBox = new MediaBox(theObj);
		theObj.mediaBoxObject = theMediaBox; // Make reference to Mediabox Object from DOMElement

		/* Add attribute "mediaBoxObject" to all child elements of the Mediabox */
		var allChildren = theObj.getElementsByTagName("*");
		for(var i in allChildren)
			allChildren[i].mediaBoxObject = theMediaBox;

		/* Get navigationbuttons (previous, next) */
		theMediaBox.previousButton = getElementsByClassName(theObj, "mediaBoxPaneNavigationPrevious", false)[0];
		theMediaBox.nextButton = getElementsByClassName(theObj, "mediaBoxPaneNavigationNext", false)[0];

		/* Get all the tabs */
		//Get container object of the tabs
		var tabContainer = getElementsByClassName(theObj, "mediaBoxTabStrip", true)[0];
		//Get List of items in that container
		var itemList = getElementsByClassName(tabContainer, "mediaBoxTab", true);
		for(i in itemList)
			theMediaBox.tabs[i] = itemList[i];

		/* Get all panes and slides */
		//Get container object with the picture panes(tabs) and slides
		var paneContainer = getElementsByClassName(theObj, "mediaBoxPaneContainer", true)[0];
		//Get List of items in that container
		var itemList = getElementsByClassName(paneContainer, "mediaBoxPane", true);

		var theMediaBoxPane;
		var theMediaBoxPaneSlide;

		for(i in itemList) {
			switch(itemList[i].className) {
				case "mediaBoxPane":
					theMediaBoxPane = theMediaBox.panes[theMediaBox.panes.length] = new MediaBoxPane(itemList[i]);
					break;

				case "mediaBoxPaneActive":
					theMediaBoxPane =  theMediaBox.panes[theMediaBox.panes.length] = new MediaBoxPane(itemList[i]);
					theMediaBox.activePane = theMediaBox.panes.length - 1;
					break;

				case "mediaBoxPaneSlide":
					theMediaBoxPaneSlide = theMediaBoxPane.slides[theMediaBoxPane.slides.length] = new MediaBoxPaneSlide(itemList[i]);
					break;

				case "mediaBoxPaneSlideActive":
					theMediaBoxPaneSlide = theMediaBoxPane.slides[theMediaBoxPane.slides.length] = new MediaBoxPaneSlide(itemList[i]);
					theMediaBox.activeSlide = theMediaBoxPane.slides.length - 1;
					break;
			}
		}
	}
	catch(error) {
		alert("Error during initialization of Mediabox [initializeMediaBox()]\n\nError Code: " + error);
		return false;
	}

	mediaBoxRefreshNavigation(theMediaBox);

	theMediaBox.initialized = true;
	return true;
}

function mediaBoxRefreshNavigation(mediaBox) {

    var maxActiveSlides = mediaBox.panes[mediaBox.activePane].slides.length;

	if(mediaBox.activeSlide == 0)
		mediaBox.previousButton.style.visibility = "hidden";
	else
		mediaBox.previousButton.style.visibility = "visible";

	if ((mediaBox.activeSlide + 1) == maxActiveSlides)
		mediaBox.nextButton.style.visibility = "hidden";
	else
		mediaBox.nextButton.style.visibility = "visible";

	return true;

}

/* Select the  pane corresponding to the tab  */
function mediaBoxPaneSelect(theObj) {
	var mediaBox;

    /* Instantiate Mediabox Object if needed */
	if(!theObj.mediaBoxObject) {
        // Find the mediaBox Rool element by moving up the DOM Tree
		mediaBox = theObj;
		do {
			mediaBox = findParentNodeByClassName(mediaBox, "mediaBox");
        }  while ((mediaBox.className != "mediaBox") && (mediaBox.className.indexOf("mediaBox ") == -1) && (mediaBox.nodeName != 'BODY')) //Find classnames exactly equal or in combination with other classname (i.e. class="Mediabox Frontpage")

        mediaBoxInitialize(mediaBox);
	}

	mediaBox = theObj.mediaBoxObject; // Use reference of the DOMElement to the Picturbar Object

	/* Find the right tab number */
	for(var i in mediaBox.tabs) {
		if(mediaBox.tabs[i].getElementsByTagName("A")[0] == theObj)
			break;
	}

	if(mediaBox.activePane == i) //If this pane is already active, exit function
		return false;

	/* Set active tab  */
	mediaBox.tabs[mediaBox.activePane].className = "mediaBoxTab";
	mediaBox.tabs[i].className = "mediaBoxTabActive";

	/* Set active pane and slide to normal */
	mediaBox.panes[mediaBox.activePane].DOMObject.className = "mediaBoxPane";
	mediaBox.panes[mediaBox.activePane].slides[mediaBox.activeSlide].DOMObject.className = "mediaBoxPaneSlide";

	/* Display chosen pane  and first slide  */
	mediaBox.panes[i].DOMObject.className = "mediaBoxPaneActive";
	mediaBox.panes[i].slides[0].DOMObject.className = "mediaBoxPaneSlideActive";
	mediaBox.activePane = i;
	mediaBox.activeSlide = 0;

	mediaBoxRefreshNavigation(mediaBox);
	return true;
}

function mediaBoxMoveSlide(theObj, direction) {
	var mediaBox;
	var currentPane;
	var maxpossibleSlides;
	var slideTest;

	/* Instantiate Mediabox Object if needed */
	if(!theObj.mediaBoxObject) {
		// Find the mediaBox Rool element by moving up the DOM Tree
		mediaBox = theObj;
		do {
			mediaBox = findParentNodeByClassName(mediaBox, "mediaBox");
        }  while ((mediaBox.className != "mediaBox") && (mediaBox.className.indexOf("mediaBox ") == -1) && (mediaBox.nodeName != 'BODY')) 
		//Find classnames exactly equal or in combination with other classname (i.e. class="Mediabox Frontpage")

		mediaBoxInitialize(mediaBox);
	}

	mediaBox = theObj.mediaBoxObject; // Use reference of the DOMElement to the Picturbar Object
	currentPane = mediaBox.panes[mediaBox.activePane];
	maxpossibleSlides = currentPane.slides.length;

	/*Test if it is possible to navigate */
	slideTest = mediaBox.activeSlide + direction;
	if((slideTest < 0) || (slideTest == maxpossibleSlides))
		return false;

	/* Hide current slide and show next */
	currentPane.slides[mediaBox.activeSlide].DOMObject.className = "mediaBoxPaneSlide";
	mediaBox.activeSlide += direction;
	currentPane.slides[mediaBox.activeSlide].DOMObject.className = "mediaBoxPaneSlideActive";

	mediaBoxRefreshNavigation(mediaBox)
	return true;
}
/* ---- popups ---- */
/********************************************/
/* Javascript textpopup with shaded corners */
/********************************************/

/* Declare some global variables, that will be persisted across the mouseover events */

var popupWindowObj; /* pointer to textPopupWindow object */
var textPopupActive = false;
var textPopupActiveOject = null;
var textPopupActiveValign;
var textPopupActivePosition;
var textPopupActiveSize;
var textPopupActiveFollow;

/*
    This function inserts the popup div at the end of the document and is then reused
*/

function insertTextPopupIntoDOM (){
    var textPopupHTML = new Array
    (
    '<div id="textPopupWindow" class="textPopup textPopupSizeSmall" onmouseover="showTextPopup(this,\'\',\'\',\'\');" onmouseout="hideTextPopup(this);">',
    '<iframe id="textPopupIframeBackground" scrolling="no" frameborder="0"'+ (navigator.userAgent.indexOf("MSIE") >= 0 ? ' style="block"' : '') +'></iframe>',
    '   <div id="textPopupWindowContent" class="textPopupcontent">',
    '       empty1',
    '   </div>',
    '   <div class="dropshadow">',
    '       <div class="top">',
    '           <span class="dropshadowtopleft"></span><span class="dropshadowtopright"></span><span class="dropshadowright"></span>',
    '       </div>',
    '       <div class="bottom">',
    '           <span class="dropshadowbottomleft"></span><span class="dropshadowbottom"></span><span class="dropshadowbottomright"></span>',
    '       </div>',
    '   </div>',
    '</div>'
    );

    for(var i=0; i<textPopupHTML.length; i++) {
        document.writeln(textPopupHTML[i]);
    }

    popupWindowObj = document.getElementById('textPopupWindow');
}

/*
    Mouseoverhandler for showing the textPopup
*/

function showTextPopup(theObj, valign, position, size, follow, textContent) {
    /* following parameters are possible: */
    /* valign = [top|bottom|], position = [inside|outside], size = [small|medium], follow = [true|false] */
    /* textContent = [String] for dynamic content */
    
    valign = valign.toUpperCase();
    position = position.toUpperCase();
    size = size.toUpperCase();

    //check if the popup-object exists
    if(!popupWindowObj)
        return(false);

    /*
    Execute this code only at the initial mouseover of a new button object that triggers the popup,
    textpopupActive will be set to true this time and will be set to false by the reallyHide function,
    when the popup is actually hidden
    */

    if(!textPopupActive) {   //Execute only at initial mouseover
        // Set the parameters of this mouseover to global variables that can be used by other event handlers
        textPopupActiveValign = valign;
        textPopupActivePosition = position;
        textPopupActiveSize = size;
        textPopupActiveFollow = follow;

        //Changes classname of the popupwindow according to the 'valign' and 'size' parameter
        //Check alignment
        if(valign == 'TOP') {
            popupWindowObj.className = 'textPopupTop';
        }
        else if(valign == 'BOTTOM') {
            popupWindowObj.className = 'textPopupBottom';
        }
        else  {
            popupWindowObj.className = 'textPopup';
        }

        //Check size
        if(size == 'SMALL') {
            popupWindowObj.className += ' textPopupSizeSmall';
        }
        else if(size == 'MEDIUM') {
            popupWindowObj.className += ' textPopupSizeMedium';
        }
        else {
            popupWindowObj.className += ' textPopupSizeMedium';
        }

        //Look for the HTML-content of the popup and copy that into the popupwindow
        if(textContent) {
           document.getElementById('textPopupWindowContent').innerHTML = textContent;
        }

        else {
            var goNext = true;
            var sibling = theObj.nextSibling;

            //First Look for child div with classname 'popupContent'
            for(var i=0; i < theObj.childNodes.length; i++) {

                if(theObj.childNodes[i].className) {
                    if(theObj.childNodes[i].className.indexOf('popupContent') != -1) {
                        document.getElementById('textPopupWindowContent').innerHTML = theObj.childNodes[i].innerHTML;
                        goNext = false;
                    }
                }
            }

            //If that fails:
            //Look for sibling div with classname 'popupContent'

            while(goNext && sibling) {
                if(sibling.className) {
                    if(sibling.className.indexOf('popupContent') != -1) {
                        document.getElementById('textPopupWindowContent').innerHTML = sibling.innerHTML;
                        goNext = false;
                    }
                }

                if(sibling.nextSibling) {
                    sibling = sibling.nextSibling;
                }

                else {
                    goNext = false;
                }
            }
        }

        //reset the popupwindow relative to the calling object
        coords = findPosition(theObj); //find position of popup-caller button
        popupWindowObj.style.left = coords[0] + (theObj.offsetWidth/2) + "px"; //horizontally center the popup over the caller-object
        popupWindowObj.style.top = coords[1] + "px"; //basic vertical realignment
        
		

        //Further adjust the vertical coordinate according to 'valign' and 'position' property
        if(valign == 'TOP' && position == 'INSIDE') {
            //popupWindowObj.style.top = ((coords[1] - popupWindowObj.offsetHeight) + (theObj.offsetHeight/1.5)) + "px";
            popupWindowObj.style.top = ((coords[1] - popupWindowObj.offsetHeight) + (theObj.offsetHeight)/2) + "px";
        }
        else if(valign == 'BOTTOM' && position == 'INSIDE') {
            //popupWindowObj.style.top = (coords[1] + (theObj.offsetHeight*2)) + "px";
            popupWindowObj.style.top = (coords[1] + (theObj.offsetHeight*0.5)) + "px";
        }
        else if(valign == 'TOP') {
            popupWindowObj.style.top = (coords[1] - popupWindowObj.offsetHeight) + "px";
        }
        else if(valign == 'BOTTOM') {
            popupWindowObj.style.top = (coords[1] + theObj.offsetHeight) + "px";
        }

        //Find the newly defined coords of the repositioned popupwindow
        coords = findPosition(popupWindowObj);

        //Check if it is positioned outside the TOP of the screen and reposition popup at bottom align
        if((findPosition(popupWindowObj)[1] < getScrollXY()[1]) && valign == 'TOP') {   // only trigger this event when valign == TOP!
            showTextPopup(theObj, 'bottom', position, size, follow); //recursively call this function again with 'valign = bottom'
            return;
        }

        //Check if the 'follow' parameter is set to true
        if(follow) {
            if(!theObj.onmousemove)
                theObj.onmousemove = followTextPopup;  //Set the mouseover eventhandler on the caller object

            if(window.event) //only needed for IE. directly readjust popup-position to the mouse pointer
               followTextPopup(window.event);  //used in IE only for smooth transition
        }

        //Important!
        textPopupActive = true; //this popup is now active and visible
        textPopupActiveOject = theObj; //the object that called the currently activated popupwindow

        //check if there is any valid content in the popup
        if(document.getElementById('textPopupWindowContent').innerHTML.length > 2) { //Content should at least contain more than 2 characters
            //Only show if there is any content
            document.getElementById("textPopupIframeBackground").style.width = (document.getElementById("textPopupWindowContent").offsetWidth - 0) + 'px';
            document.getElementById("textPopupIframeBackground").style.height = (document.getElementById("textPopupWindowContent").offsetHeight - 0) + 'px';

            popupWindowObj.style.visibility='visible'; //Finally display the window
        }
    }

    //Cancel the hide timeout if the popupwindow receives a mouseover again from the caller OR the popupwindow itself
    else if(popupWindowObj.getAttribute("timerId") && textPopupActiveFollow == false )  {  /* if the follow option is active, then don't do this */
        clearTimeout(popupWindowObj.getAttribute("timerId"));
        popupWindowObj.setAttribute ("timerId", null);
    }

    /*
    If a mouseover is generated by another caller, then immediately hide the current popupwindow without delay and
    and recursively call the showTextPop handler with the new caller
    */

    if(textPopupActiveOject != theObj && theObj != popupWindowObj) {
        reallyHideTextPopup(); //immmediately hide the current popup
        showTextPopup(theObj, valign, position, size, follow, textContent); //call a new popup window
    }

    return;
}

/*
    Mouseout handler for delayed hiding of the popup (exeption: follow parameter = true)
*/

function hideTextPopup(theObj) {
    /*
    Try to hide submenu in case of mouseout
    Can be cancelled by a mouseover
    */

    var timerId;
    var timeOutHandler = "reallyHideTextPopup()";
    var timeOut = 500; /* standard delay (milliseconds) of mouseout */

    //check if the popup-object exists
    if(!popupWindowObj)
        return(false);

    //If the follow property of the active popup is set to true, then immediately hide
    if(textPopupActiveFollow) {
        reallyHideTextPopup();
    }
    else {
        timerId = window.setTimeout(timeOutHandler, timeOut);
        popupWindowObj.setAttribute ("timerId", timerId);
    }

    return;
}

/*
    Actually hide the popup window
*/

function reallyHideTextPopup() {
    popupWindowObj.style.visibility='hidden';  //hide the window
    clearTimeout(popupWindowObj.getAttribute("timerId"));
    popupWindowObj.setAttribute ("timerId", null); //set the timer attribute of the popupwindow to null
    textPopupActive = false; //Important! set popupwindow inactive
}

/*
    Mousemove handler in case the 'follow(mouse)' option is selected
*/

function followTextPopup(e) {

    //check if the popup-object exists
    if(!popupWindowObj)
        return(false);

    //Do not execute when there is still a timer running !!!
    if(popupWindowObj.getAttribute("timerId"))  {
        return(false);
    }

    var posx = 0;
	var posy = 0;

    if (!e) var e = window.event;

    if (e.pageX || e.pageY) 	{
		posx = e.pageX;
		posy = e.pageY;
	}
	else if (e.clientX || e.clientY) 	{
		posx = e.clientX + document.body.scrollLeft
			+ document.documentElement.scrollLeft;
		posy = e.clientY + document.body.scrollTop
			+ document.documentElement.scrollTop;
	}

    //adjust the horizontal position of the popupwindow
    popupWindowObj.style.left = posx + "px";

    //adjust the vertical position according to the valign property relative to mousepointer
    if(textPopupActiveValign == 'TOP') {
        popupWindowObj.style.top = posy - popupWindowObj.offsetHeight - 10 + "px";
    }
    else if(textPopupActiveValign == 'BOTTOM') {
        popupWindowObj.style.top = posy + 15 + "px";
    }

    //find the newly coords of the repositioned popupwindow
    coords = findPosition(popupWindowObj);

    //check if it is positioned outside the TOP of the screen and reposition popup at bottom align
    if(findPosition(popupWindowObj)[1] < getScrollXY()[1]) {
        reallyHideTextPopup();
        showTextPopup(textPopupActiveOject, 'bottom', textPopupActivePosition, textPopupActiveSize, true);
        return;
    }
}

/*************************************************************/
/* Functions for displaying the ressort menustrip (mainmenu) */
/*************************************************************/

var mainMenu;
var iframeBackground;
var mainMenuActive = false;

function showMainMenu() {
    if(!mainMenuActive){
        mainMenu = document.getElementById('mainMenu');
        iframeBackground = document.getElementById('mainMenuIframeBackground');

        var coords = findPosition(mainMenu);

        iframeBackground.style.left = mainMenu.style.left;
        iframeBackground.style.top = mainMenu.style.right;
        iframeBackground.style.width = mainMenu.offsetWidth + 'px';
        iframeBackground.style.height = mainMenu.offsetHeight + 'px';

        mainMenu.style.visibility = 'visible';
        iframeBackground.style.visibility = 'visible';
        mainMenuActive = true;
    }
    //Cancel the hide timeout if the popupwindow receives a mouseover again from the caller OR the popupwindow itself
    else if(mainMenu.getAttribute("timerId"))  {
        clearTimeout(mainMenu.getAttribute("timerId"));
        mainMenu.setAttribute ("timerId", null);
    }

    return;
}

function hideMainMenu() {
    /*
    Try to hide submenu in case of mouseout
    Can be cancelled by a mouseover
    */

    var timerId;
    var timeOutHandler = "reallyHideMainMenu()";
    var timeOut = 400; /* standard delay (milliseconds) of mouseout */

    timerId = window.setTimeout(timeOutHandler, timeOut);
    mainMenu.setAttribute ("timerId", timerId);

    return;
}

/*
    Actually hide the popup window
*/

function reallyHideMainMenu() {
    mainMenu.style.visibility = 'hidden';
    iframeBackground.style.visibility = 'hidden'; //hide the window

    mainMenu.setAttribute ("timerId", null); //set the timer attribute of the popupwindow to null
    mainMenuActive = false; //Important! set popupwindow inactive
    return;
}

/* ---- print ---- */
/* STATUS:CHECKED Todo: should this file be integrated in a global js? */
var display_img_print = false;
function switchDisplayImages()
{
	//go through the domtree of article and set display to none
	divs = document.getElementById("contentContainer").getElementsByTagName("div");
	for (i=0; i<divs.length; i++)
	{
		switch (divs[i].className)
		{
			case "imageHeadline":
			case "imageLeft":
			case "imageCenter":
			case "imageRight":
			case "articleBox galleryBox clear":
			case "videoMainPlayer":
			case "inlineIMG_left inlineIMG_a":
			case "inlineIMG_left inlineIMG_b":
			case "inlineIMG_left inlineIMG_e":
			case "inlineIMG_left inlineIMG_g":
			case "inlineIMG_right inlineIMG_a":
			case "inlineIMG_right inlineIMG_b":
			case "inlineIMG_right inlineIMG_e":
			case "inlineIMG_right inlineIMG_g":
                divs[i].style.display = (display_img_print ? "block" : "none");

			default: break;
		}
	}
	display_img_print = !display_img_print;
}

function printArticle()
{
	window.print();
}
/* ---- search ---- */
function openWOAPopup (url) {
  myWindow = window.open(url, "WeltOnlineHilfe", "width=880,height=600,left=100,top=200,status=yes,scrollbars=yes,resizable=yes");
  myWindow.focus();
}

				function handleKeyUp(evt)
				{	
					evt = (evt) ? evt : ((event) ? event : null);
					id =  (evt.target) ? evt.target.id : evt.srcElement.id;
					value = (evt.target) ? evt.target.value : evt.srcElement.value;
					if (evt) {
						// check if return pressed
						if (evt.keyCode == 13) {
							
							deleteAllDefaultDateValues();
							form = document.getElementById("searchFormTop");
							form.submit();
						}
						else {
							syncDateField(id, value);
						}	
					}
					
				}	
				
				
				function readDates() {
					if (document.nf_period && document.nf_period.fromDate.value.length > 0) {
						document.searchFormTop.fromDate.value = document.nf_period.fromDate.value;
					}
					if (document.nf_period && document.nf_period.toDate.value.length > 0) {
						document.searchFormTop.toDate.value = document.nf_period.toDate.value;
					}
				}

				function readRessorts() {
				
					// read Ressorts wird durch die Umstellung der erweiterten Suche nun immer ausgefuehrt
					//if (document.getElementById('optionsDIV').style.display != 'none') {
						for (var x = 1; x < 14; x++) {
							if (document.getElementById('chk-1-'+x).checked == true) {
								document.searchFormTop.multiRessort.value += document.getElementById('chk-1-'+x).value + " ";
							}
						}
						if (document.searchFormTop.multiRessort.value.length > 0) {
							document.searchFormTop.multiRessort.value = document.searchFormTop.multiRessort.value.substring(0, document.searchFormTop.multiRessort.value.length - 1);
						}
					//}
				}

				function toggleRessort(group) {

					if (document.getElementById('chk-'+group).checked == true) {

						for (var x = 1; x < 20; x++) {
							if (document.getElementById('chk-'+group+'-'+x)) {
								document.getElementById('chk-'+group+'-'+x).setAttribute('checked', true);
								document.getElementById('chk-'+group+'-'+x).setAttribute('disabled', true);
							}
						}

					} else {

						for (var x = 1; x < 20; x++) {
							if (document.getElementById('chk-'+group+'-'+x)) {
								document.getElementById('chk-'+group+'-'+x).removeAttribute('checked');
								document.getElementById('chk-'+group+'-'+x).removeAttribute('disabled');
							}
						}									
					}
				}
				
				
				function selectRessort(group,check) {

					if (check == 'true') { 	// select all check boxes

						for (var x = 1; x < 20; x++) {
							if (document.getElementById('chk-'+group+'-'+x)) {
								document.getElementById('chk-'+group+'-'+x).checked = true;
							}
						}
						
						// deselect "Auswahl aufheben"
						document.getElementById('chk-2').checked = false;

					} else {				// deselect all check boxes

						for (var x = 1; x < 20; x++) {
							if (document.getElementById('chk-'+group+'-'+x)) {
								document.getElementById('chk-'+group+'-'+x).checked = false;
							}
						}
						
						// deselect "Alle auswaehlen"
						document.getElementById('chk-1').checked = false;
															
					}
				}
				
				function unselectMainCheckboxes() {
					document.getElementById('chk-1').checked = false;
					document.getElementById('chk-2').checked = false;
				}
				
				function switchToExtendedSearch() {
					// perform slide effect
					Effect.toggle('optionsDIV','slide', {duration:1}); 
					
					//toggle label simple search
					Element.toggle('labelSimpleSearch'); 
					
					//toggle label extended search
					Element.toggle('labelExtendedSearch');
					
					//deselect all ressorts 
					selectRessort(1,'false')
					
					//deselect timespane restrictions
					document.getElementById('chk-4-0').checked = true;
					
					//close all date picker windwows
					closeAllDatePickers ();

				
					
				}
				
				function switchToSimpleSearch() {
					// perform slide effect
					Effect.toggle('optionsDIV','slide', {duration:1});
					 
					//toggle label simple search 
					Element.toggle('labelSimpleSearch');
					
					//toggle label extended search 
					Element.toggle('labelExtendedSearch'); 
					
					//close all date picker windwows
					closeAllDatePickers ();
					
				}
				
				function closeAllDatePickersExceptFor (id) {

					var fromDate  = document.getElementById('datepicker-from_date');
					var toDate    = document.getElementById('datepicker-to_date');
					var fromDate2 = document.getElementById('datepicker-from_date2');
					var toDate2   = document.getElementById('datepicker-to_date2');
					
					if (fromDate  && id !='datepicker-from_date')  fromDate.style.display = 'none';
					if (toDate    && id !='datepicker-to_date')    toDate.style.display   = 'none';
					if (fromDate2 && id !='datepicker-from_date2') fromDate2.style.display= 'none';
					if (toDate2   && id !='datepicker-to_date2')   toDate2.style.display= 'none';
					
				}
				
				function closeAllDatePickers () {
					var fromDate  = document.getElementById('datepicker-from_date');
					var toDate    = document.getElementById('datepicker-to_date');
					var fromDate2 = document.getElementById('datepicker-from_date2');
					var toDate2   = document.getElementById('datepicker-to_date2');
					
					if (fromDate)  fromDate.style.display  = 'none';
					if (toDate)    toDate.style.display    = 'none';
					if (fromDate2) fromDate2.style.display = 'none';
					if (toDate2)   toDate2.style.display   = 'none';
				}
				
				
				function deleteDateInput(element) {
					if (element.value == 'tt.mm.jjjj') element.value = '';
				}
				
				
				function deleteAllDefaultDateValues() {
					if (document.getElementById('from_date') && document.getElementById('from_date').value  == 'tt.mm.jjjj') document.getElementById('from_date').value  = '';
					if (document.getElementById('to_date')	&& document.getElementById('to_date').value    == 'tt.mm.jjjj') document.getElementById('to_date').value    = '';
					if (document.getElementById('from_date2') && document.getElementById('from_date2').value == 'tt.mm.jjjj') document.getElementById('from_date2').value = '';
					if (document.getElementById('to_date2') && document.getElementById('to_date2').value   == 'tt.mm.jjjj') document.getElementById('to_date2').value   = '';
				}
				
				
				
				
				
				
				function moveDatePicker() {
					
					var anchor1 = document.getElementById('from_date2');
					var anchor2 = document.getElementById('to_date2');
					
					var newTopOffset1 = '';
					var newTopOffset2 = '';
					
					if (anchor1) newTopOffset1 = returnNewTopOffset(anchor1);
					if (anchor2) newTopOffset2 = returnNewTopOffset(anchor2);
					
					var datePicker1 = document.getElementById('datepicker-from_date2');
					var datePicker2 = document.getElementById('datepicker-to_date2');
					
					if (datePicker1) datePicker1.style.top = newTopOffset1 + 'px';
					if (datePicker2) datePicker2.style.top = newTopOffset2 + 'px';
					
					
  				
				}
				
				
				
				
				
				function returnNewTopOffset (obj, newTop) {
					var pos = {left:0, top:0};
					
					if(typeof obj.offsetLeft != 'undefined')
					{
					   while (obj)
					   {
					       pos.left += obj.offsetLeft;
					       pos.top += obj.offsetTop;
					       obj = obj.offsetParent;
					   }
					}
					else
					{
					   pos.left = obj.left ;
					   pos.top = obj.top ;
					}
					
					return pos.top;
				
				}
				

/*
Copyright (c) 2007, Yahoo! Inc. All rights reserved.
Code licensed under the BSD License:
http://developer.yahoo.net/yui/license.txt
version: 2.4.0
*/
if(typeof YAHOO=="undefined"||!YAHOO){var YAHOO={};}YAHOO.namespace=function(){var A=arguments,E=null,C,B,D;for(C=0;C<A.length;C=C+1){D=A[C].split(".");E=YAHOO;for(B=(D[0]=="YAHOO")?1:0;B<D.length;B=B+1){E[D[B]]=E[D[B]]||{};E=E[D[B]];}}return E;};YAHOO.log=function(D,A,C){var B=YAHOO.widget.Logger;if(B&&B.log){return B.log(D,A,C);}else{return false;}};YAHOO.register=function(A,E,D){var I=YAHOO.env.modules;if(!I[A]){I[A]={versions:[],builds:[]};}var B=I[A],H=D.version,G=D.build,F=YAHOO.env.listeners;B.name=A;B.version=H;B.build=G;B.versions.push(H);B.builds.push(G);B.mainClass=E;for(var C=0;C<F.length;C=C+1){F[C](B);}if(E){E.VERSION=H;E.BUILD=G;}else{YAHOO.log("mainClass is undefined for module "+A,"warn");}};YAHOO.env=YAHOO.env||{modules:[],listeners:[]};YAHOO.env.getVersion=function(A){return YAHOO.env.modules[A]||null;};YAHOO.env.ua=function(){var C={ie:0,opera:0,gecko:0,webkit:0,mobile:null};var B=navigator.userAgent,A;if((/KHTML/).test(B)){C.webkit=1;}A=B.match(/AppleWebKit\/([^\s]*)/);if(A&&A[1]){C.webkit=parseFloat(A[1]);if(/ Mobile\//.test(B)){C.mobile="Apple";}else{A=B.match(/NokiaN[^\/]*/);if(A){C.mobile=A[0];}}}if(!C.webkit){A=B.match(/Opera[\s\/]([^\s]*)/);if(A&&A[1]){C.opera=parseFloat(A[1]);A=B.match(/Opera Mini[^;]*/);if(A){C.mobile=A[0];}}else{A=B.match(/MSIE\s([^;]*)/);if(A&&A[1]){C.ie=parseFloat(A[1]);}else{A=B.match(/Gecko\/([^\s]*)/);if(A){C.gecko=1;A=B.match(/rv:([^\s\)]*)/);if(A&&A[1]){C.gecko=parseFloat(A[1]);}}}}}return C;}();(function(){YAHOO.namespace("util","widget","example");if("undefined"!==typeof YAHOO_config){var B=YAHOO_config.listener,A=YAHOO.env.listeners,D=true,C;if(B){for(C=0;C<A.length;C=C+1){if(A[C]==B){D=false;break;}}if(D){A.push(B);}}}})();YAHOO.lang=YAHOO.lang||{isArray:function(B){if(B){var A=YAHOO.lang;return A.isNumber(B.length)&&A.isFunction(B.splice);}return false;},isBoolean:function(A){return typeof A==="boolean";},isFunction:function(A){return typeof A==="function";},isNull:function(A){return A===null;},isNumber:function(A){return typeof A==="number"&&isFinite(A);},isObject:function(A){return(A&&(typeof A==="object"||YAHOO.lang.isFunction(A)))||false;},isString:function(A){return typeof A==="string";},isUndefined:function(A){return typeof A==="undefined";},hasOwnProperty:function(A,B){if(Object.prototype.hasOwnProperty){return A.hasOwnProperty(B);}return !YAHOO.lang.isUndefined(A[B])&&A.constructor.prototype[B]!==A[B];},_IEEnumFix:function(C,B){if(YAHOO.env.ua.ie){var E=["toString","valueOf"],A;for(A=0;A<E.length;A=A+1){var F=E[A],D=B[F];if(YAHOO.lang.isFunction(D)&&D!=Object.prototype[F]){C[F]=D;}}}},extend:function(D,E,C){if(!E||!D){throw new Error("YAHOO.lang.extend failed, please check that all dependencies are included.");}var B=function(){};B.prototype=E.prototype;D.prototype=new B();D.prototype.constructor=D;D.superclass=E.prototype;if(E.prototype.constructor==Object.prototype.constructor){E.prototype.constructor=E;}if(C){for(var A in C){D.prototype[A]=C[A];}YAHOO.lang._IEEnumFix(D.prototype,C);}},augmentObject:function(E,D){if(!D||!E){throw new Error("Absorb failed, verify dependencies.");}var A=arguments,C,F,B=A[2];if(B&&B!==true){for(C=2;C<A.length;C=C+1){E[A[C]]=D[A[C]];}}else{for(F in D){if(B||!E[F]){E[F]=D[F];}}YAHOO.lang._IEEnumFix(E,D);}},augmentProto:function(D,C){if(!C||!D){throw new Error("Augment failed, verify dependencies.");}var A=[D.prototype,C.prototype];for(var B=2;B<arguments.length;B=B+1){A.push(arguments[B]);}YAHOO.lang.augmentObject.apply(this,A);},dump:function(A,G){var C=YAHOO.lang,D,F,I=[],J="{...}",B="f(){...}",H=", ",E=" => ";if(!C.isObject(A)){return A+"";}else{if(A instanceof Date||("nodeType" in A&&"tagName" in A)){return A;}else{if(C.isFunction(A)){return B;}}}G=(C.isNumber(G))?G:3;if(C.isArray(A)){I.push("[");for(D=0,F=A.length;D<F;D=D+1){if(C.isObject(A[D])){I.push((G>0)?C.dump(A[D],G-1):J);}else{I.push(A[D]);}I.push(H);}if(I.length>1){I.pop();}I.push("]");}else{I.push("{");for(D in A){if(C.hasOwnProperty(A,D)){I.push(D+E);if(C.isObject(A[D])){I.push((G>0)?C.dump(A[D],G-1):J);}else{I.push(A[D]);}I.push(H);}}if(I.length>1){I.pop();}I.push("}");}return I.join("");},substitute:function(Q,B,J){var G,F,E,M,N,P,D=YAHOO.lang,L=[],C,H="dump",K=" ",A="{",O="}";for(;;){G=Q.lastIndexOf(A);if(G<0){break;}F=Q.indexOf(O,G);if(G+1>=F){break;}C=Q.substring(G+1,F);M=C;P=null;E=M.indexOf(K);if(E>-1){P=M.substring(E+1);M=M.substring(0,E);}N=B[M];if(J){N=J(M,N,P);}if(D.isObject(N)){if(D.isArray(N)){N=D.dump(N,parseInt(P,10));}else{P=P||"";var I=P.indexOf(H);if(I>-1){P=P.substring(4);}if(N.toString===Object.prototype.toString||I>-1){N=D.dump(N,parseInt(P,10));}else{N=N.toString();}}}else{if(!D.isString(N)&&!D.isNumber(N)){N="~-"+L.length+"-~";L[L.length]=C;}}Q=Q.substring(0,G)+N+Q.substring(F+1);}for(G=L.length-1;G>=0;G=G-1){Q=Q.replace(new RegExp("~-"+G+"-~"),"{"+L[G]+"}","g");}return Q;},trim:function(A){try{return A.replace(/^\s+|\s+$/g,"");}catch(B){return A;}},merge:function(){var D={},B=arguments;for(var C=0,A=B.length;C<A;C=C+1){YAHOO.lang.augmentObject(D,B[C],true);}return D;},later:function(H,B,I,D,E){H=H||0;B=B||{};var C=I,G=D,F,A;if(YAHOO.lang.isString(I)){C=B[I];}if(!C){throw new TypeError("method undefined");}if(!YAHOO.lang.isArray(G)){G=[D];}F=function(){C.apply(B,G);};A=(E)?setInterval(F,H):setTimeout(F,H);return{interval:E,cancel:function(){if(this.interval){clearInterval(A);}else{clearTimeout(A);}}};},isValue:function(B){var A=YAHOO.lang;return(A.isObject(B)||A.isString(B)||A.isNumber(B)||A.isBoolean(B));}};YAHOO.util.Lang=YAHOO.lang;YAHOO.lang.augment=YAHOO.lang.augmentProto;YAHOO.augment=YAHOO.lang.augmentProto;YAHOO.extend=YAHOO.lang.extend;YAHOO.register("yahoo",YAHOO,{version:"2.4.0",build:"733"});(function(){var B=YAHOO.util,L,J,H=0,K={},F={},N=window.document;var C=YAHOO.env.ua.opera,M=YAHOO.env.ua.webkit,A=YAHOO.env.ua.gecko,G=YAHOO.env.ua.ie;var E={HYPHEN:/(-[a-z])/i,ROOT_TAG:/^body|html$/i};var O=function(Q){if(!E.HYPHEN.test(Q)){return Q;}if(K[Q]){return K[Q];}var R=Q;while(E.HYPHEN.exec(R)){R=R.replace(RegExp.$1,RegExp.$1.substr(1).toUpperCase());}K[Q]=R;return R;};var P=function(R){var Q=F[R];if(!Q){Q=new RegExp("(?:^|\\s+)"+R+"(?:\\s+|$)");F[R]=Q;}return Q;};if(N.defaultView&&N.defaultView.getComputedStyle){L=function(Q,T){var S=null;if(T=="float"){T="cssFloat";}var R=N.defaultView.getComputedStyle(Q,"");if(R){S=R[O(T)];}return Q.style[T]||S;};}else{if(N.documentElement.currentStyle&&G){L=function(Q,S){switch(O(S)){case"opacity":var U=100;try{U=Q.filters["DXImageTransform.Microsoft.Alpha"].opacity;}catch(T){try{U=Q.filters("alpha").opacity;}catch(T){}}return U/100;case"float":S="styleFloat";default:var R=Q.currentStyle?Q.currentStyle[S]:null;return(Q.style[S]||R);}};}else{L=function(Q,R){return Q.style[R];};}}if(G){J=function(Q,R,S){switch(R){case"opacity":if(YAHOO.lang.isString(Q.style.filter)){Q.style.filter="alpha(opacity="+S*100+")";if(!Q.currentStyle||!Q.currentStyle.hasLayout){Q.style.zoom=1;}}break;case"float":R="styleFloat";default:Q.style[R]=S;}};}else{J=function(Q,R,S){if(R=="float"){R="cssFloat";}Q.style[R]=S;};}var D=function(Q,R){return Q&&Q.nodeType==1&&(!R||R(Q));};YAHOO.util.Dom={get:function(S){if(S&&(S.tagName||S.item)){return S;}if(YAHOO.lang.isString(S)||!S){return N.getElementById(S);}if(S.length!==undefined){var T=[];for(var R=0,Q=S.length;R<Q;++R){T[T.length]=B.Dom.get(S[R]);}return T;}return S;},getStyle:function(Q,S){S=O(S);var R=function(T){return L(T,S);};return B.Dom.batch(Q,R,B.Dom,true);},setStyle:function(Q,S,T){S=O(S);var R=function(U){J(U,S,T);};B.Dom.batch(Q,R,B.Dom,true);},getXY:function(Q){var R=function(S){if((S.parentNode===null||S.offsetParent===null||this.getStyle(S,"display")=="none")&&S!=S.ownerDocument.body){return false;}return I(S);};return B.Dom.batch(Q,R,B.Dom,true);},getX:function(Q){var R=function(S){return B.Dom.getXY(S)[0];};return B.Dom.batch(Q,R,B.Dom,true);},getY:function(Q){var R=function(S){return B.Dom.getXY(S)[1];};return B.Dom.batch(Q,R,B.Dom,true);},setXY:function(Q,T,S){var R=function(W){var V=this.getStyle(W,"position");if(V=="static"){this.setStyle(W,"position","relative");V="relative";}var Y=this.getXY(W);if(Y===false){return false;}var X=[parseInt(this.getStyle(W,"left"),10),parseInt(this.getStyle(W,"top"),10)];if(isNaN(X[0])){X[0]=(V=="relative")?0:W.offsetLeft;}if(isNaN(X[1])){X[1]=(V=="relative")?0:W.offsetTop;}if(T[0]!==null){W.style.left=T[0]-Y[0]+X[0]+"px";}if(T[1]!==null){W.style.top=T[1]-Y[1]+X[1]+"px";}if(!S){var U=this.getXY(W);if((T[0]!==null&&U[0]!=T[0])||(T[1]!==null&&U[1]!=T[1])){this.setXY(W,T,true);}}};B.Dom.batch(Q,R,B.Dom,true);},setX:function(R,Q){B.Dom.setXY(R,[Q,null]);},setY:function(Q,R){B.Dom.setXY(Q,[null,R]);},getRegion:function(Q){var R=function(S){if((S.parentNode===null||S.offsetParent===null||this.getStyle(S,"display")=="none")&&S!=N.body){return false;}var T=B.Region.getRegion(S);return T;};return B.Dom.batch(Q,R,B.Dom,true);},getClientWidth:function(){return B.Dom.getViewportWidth();},getClientHeight:function(){return B.Dom.getViewportHeight();},getElementsByClassName:function(U,Y,V,W){Y=Y||"*";V=(V)?B.Dom.get(V):null||N;if(!V){return[];}var R=[],Q=V.getElementsByTagName(Y),X=P(U);for(var S=0,T=Q.length;S<T;++S){if(X.test(Q[S].className)){R[R.length]=Q[S];if(W){W.call(Q[S],Q[S]);}}}return R;},hasClass:function(S,R){var Q=P(R);var T=function(U){return Q.test(U.className);};return B.Dom.batch(S,T,B.Dom,true);},addClass:function(R,Q){var S=function(T){if(this.hasClass(T,Q)){return false;}T.className=YAHOO.lang.trim([T.className,Q].join(" "));return true;};return B.Dom.batch(R,S,B.Dom,true);},removeClass:function(S,R){var Q=P(R);var T=function(U){if(!this.hasClass(U,R)){return false;}var V=U.className;U.className=V.replace(Q," ");if(this.hasClass(U,R)){this.removeClass(U,R);}U.className=YAHOO.lang.trim(U.className);return true;};return B.Dom.batch(S,T,B.Dom,true);},replaceClass:function(T,R,Q){if(!Q||R===Q){return false;}var S=P(R);var U=function(V){if(!this.hasClass(V,R)){this.addClass(V,Q);return true;}V.className=V.className.replace(S," "+Q+" ");if(this.hasClass(V,R)){this.replaceClass(V,R,Q);}V.className=YAHOO.lang.trim(V.className);return true;};return B.Dom.batch(T,U,B.Dom,true);},generateId:function(Q,S){S=S||"yui-gen";var R=function(T){if(T&&T.id){return T.id;}var U=S+H++;if(T){T.id=U;}return U;};return B.Dom.batch(Q,R,B.Dom,true)||R.apply(B.Dom,arguments);},isAncestor:function(Q,R){Q=B.Dom.get(Q);R=B.Dom.get(R);if(!Q||!R){return false;}if(Q.contains&&R.nodeType&&!M){return Q.contains(R);}else{if(Q.compareDocumentPosition&&R.nodeType){return !!(Q.compareDocumentPosition(R)&16);}else{if(R.nodeType){return !!this.getAncestorBy(R,function(S){return S==Q;});}}}return false;},inDocument:function(Q){return this.isAncestor(N.documentElement,Q);},getElementsBy:function(X,R,S,U){R=R||"*";S=(S)?B.Dom.get(S):null||N;if(!S){return[];}var T=[],W=S.getElementsByTagName(R);for(var V=0,Q=W.length;V<Q;++V){if(X(W[V])){T[T.length]=W[V];if(U){U(W[V]);}}}return T;},batch:function(U,X,W,S){U=(U&&(U.tagName||U.item))?U:B.Dom.get(U);if(!U||!X){return false;}var T=(S)?W:window;if(U.tagName||U.length===undefined){return X.call(T,U,W);}var V=[];for(var R=0,Q=U.length;R<Q;++R){V[V.length]=X.call(T,U[R],W);}return V;},getDocumentHeight:function(){var R=(N.compatMode!="CSS1Compat")?N.body.scrollHeight:N.documentElement.scrollHeight;var Q=Math.max(R,B.Dom.getViewportHeight());return Q;},getDocumentWidth:function(){var R=(N.compatMode!="CSS1Compat")?N.body.scrollWidth:N.documentElement.scrollWidth;var Q=Math.max(R,B.Dom.getViewportWidth());return Q;},getViewportHeight:function(){var Q=self.innerHeight;var R=N.compatMode;if((R||G)&&!C){Q=(R=="CSS1Compat")?N.documentElement.clientHeight:N.body.clientHeight;
}return Q;},getViewportWidth:function(){var Q=self.innerWidth;var R=N.compatMode;if(R||G){Q=(R=="CSS1Compat")?N.documentElement.clientWidth:N.body.clientWidth;}return Q;},getAncestorBy:function(Q,R){while(Q=Q.parentNode){if(D(Q,R)){return Q;}}return null;},getAncestorByClassName:function(R,Q){R=B.Dom.get(R);if(!R){return null;}var S=function(T){return B.Dom.hasClass(T,Q);};return B.Dom.getAncestorBy(R,S);},getAncestorByTagName:function(R,Q){R=B.Dom.get(R);if(!R){return null;}var S=function(T){return T.tagName&&T.tagName.toUpperCase()==Q.toUpperCase();};return B.Dom.getAncestorBy(R,S);},getPreviousSiblingBy:function(Q,R){while(Q){Q=Q.previousSibling;if(D(Q,R)){return Q;}}return null;},getPreviousSibling:function(Q){Q=B.Dom.get(Q);if(!Q){return null;}return B.Dom.getPreviousSiblingBy(Q);},getNextSiblingBy:function(Q,R){while(Q){Q=Q.nextSibling;if(D(Q,R)){return Q;}}return null;},getNextSibling:function(Q){Q=B.Dom.get(Q);if(!Q){return null;}return B.Dom.getNextSiblingBy(Q);},getFirstChildBy:function(Q,S){var R=(D(Q.firstChild,S))?Q.firstChild:null;return R||B.Dom.getNextSiblingBy(Q.firstChild,S);},getFirstChild:function(Q,R){Q=B.Dom.get(Q);if(!Q){return null;}return B.Dom.getFirstChildBy(Q);},getLastChildBy:function(Q,S){if(!Q){return null;}var R=(D(Q.lastChild,S))?Q.lastChild:null;return R||B.Dom.getPreviousSiblingBy(Q.lastChild,S);},getLastChild:function(Q){Q=B.Dom.get(Q);return B.Dom.getLastChildBy(Q);},getChildrenBy:function(R,T){var S=B.Dom.getFirstChildBy(R,T);var Q=S?[S]:[];B.Dom.getNextSiblingBy(S,function(U){if(!T||T(U)){Q[Q.length]=U;}return false;});return Q;},getChildren:function(Q){Q=B.Dom.get(Q);if(!Q){}return B.Dom.getChildrenBy(Q);},getDocumentScrollLeft:function(Q){Q=Q||N;return Math.max(Q.documentElement.scrollLeft,Q.body.scrollLeft);},getDocumentScrollTop:function(Q){Q=Q||N;return Math.max(Q.documentElement.scrollTop,Q.body.scrollTop);},insertBefore:function(R,Q){R=B.Dom.get(R);Q=B.Dom.get(Q);if(!R||!Q||!Q.parentNode){return null;}return Q.parentNode.insertBefore(R,Q);},insertAfter:function(R,Q){R=B.Dom.get(R);Q=B.Dom.get(Q);if(!R||!Q||!Q.parentNode){return null;}if(Q.nextSibling){return Q.parentNode.insertBefore(R,Q.nextSibling);}else{return Q.parentNode.appendChild(R);}},getClientRegion:function(){var S=B.Dom.getDocumentScrollTop(),R=B.Dom.getDocumentScrollLeft(),T=B.Dom.getViewportWidth()+R,Q=B.Dom.getViewportHeight()+S;return new B.Region(S,T,Q,R);}};var I=function(){if(N.documentElement.getBoundingClientRect){return function(R){var S=R.getBoundingClientRect();var Q=R.ownerDocument;return[S.left+B.Dom.getDocumentScrollLeft(Q),S.top+B.Dom.getDocumentScrollTop(Q)];};}else{return function(S){var T=[S.offsetLeft,S.offsetTop];var R=S.offsetParent;var Q=(M&&B.Dom.getStyle(S,"position")=="absolute"&&S.offsetParent==S.ownerDocument.body);if(R!=S){while(R){T[0]+=R.offsetLeft;T[1]+=R.offsetTop;if(!Q&&M&&B.Dom.getStyle(R,"position")=="absolute"){Q=true;}R=R.offsetParent;}}if(Q){T[0]-=S.ownerDocument.body.offsetLeft;T[1]-=S.ownerDocument.body.offsetTop;}R=S.parentNode;while(R.tagName&&!E.ROOT_TAG.test(R.tagName)){if(B.Dom.getStyle(R,"display").search(/^inline|table-row.*$/i)){T[0]-=R.scrollLeft;T[1]-=R.scrollTop;}R=R.parentNode;}return T;};}}();})();YAHOO.util.Region=function(C,D,A,B){this.top=C;this[1]=C;this.right=D;this.bottom=A;this.left=B;this[0]=B;};YAHOO.util.Region.prototype.contains=function(A){return(A.left>=this.left&&A.right<=this.right&&A.top>=this.top&&A.bottom<=this.bottom);};YAHOO.util.Region.prototype.getArea=function(){return((this.bottom-this.top)*(this.right-this.left));};YAHOO.util.Region.prototype.intersect=function(E){var C=Math.max(this.top,E.top);var D=Math.min(this.right,E.right);var A=Math.min(this.bottom,E.bottom);var B=Math.max(this.left,E.left);if(A>=C&&D>=B){return new YAHOO.util.Region(C,D,A,B);}else{return null;}};YAHOO.util.Region.prototype.union=function(E){var C=Math.min(this.top,E.top);var D=Math.max(this.right,E.right);var A=Math.max(this.bottom,E.bottom);var B=Math.min(this.left,E.left);return new YAHOO.util.Region(C,D,A,B);};YAHOO.util.Region.prototype.toString=function(){return("Region {top: "+this.top+", right: "+this.right+", bottom: "+this.bottom+", left: "+this.left+"}");};YAHOO.util.Region.getRegion=function(D){var F=YAHOO.util.Dom.getXY(D);var C=F[1];var E=F[0]+D.offsetWidth;var A=F[1]+D.offsetHeight;var B=F[0];return new YAHOO.util.Region(C,E,A,B);};YAHOO.util.Point=function(A,B){if(YAHOO.lang.isArray(A)){B=A[1];A=A[0];}this.x=this.right=this.left=this[0]=A;this.y=this.top=this.bottom=this[1]=B;};YAHOO.util.Point.prototype=new YAHOO.util.Region();YAHOO.register("dom",YAHOO.util.Dom,{version:"2.4.0",build:"733"});YAHOO.util.CustomEvent=function(D,B,C,A){this.type=D;this.scope=B||window;this.silent=C;this.signature=A||YAHOO.util.CustomEvent.LIST;this.subscribers=[];if(!this.silent){}var E="_YUICEOnSubscribe";if(D!==E){this.subscribeEvent=new YAHOO.util.CustomEvent(E,this,true);}this.lastError=null;};YAHOO.util.CustomEvent.LIST=0;YAHOO.util.CustomEvent.FLAT=1;YAHOO.util.CustomEvent.prototype={subscribe:function(B,C,A){if(!B){throw new Error("Invalid callback for subscriber to '"+this.type+"'");}if(this.subscribeEvent){this.subscribeEvent.fire(B,C,A);}this.subscribers.push(new YAHOO.util.Subscriber(B,C,A));},unsubscribe:function(D,F){if(!D){return this.unsubscribeAll();}var E=false;for(var B=0,A=this.subscribers.length;B<A;++B){var C=this.subscribers[B];if(C&&C.contains(D,F)){this._delete(B);E=true;}}return E;},fire:function(){var D=this.subscribers.length;if(!D&&this.silent){return true;}var G=[],F=true,C,H=false;for(C=0;C<arguments.length;++C){G.push(arguments[C]);}if(!this.silent){}for(C=0;C<D;++C){var K=this.subscribers[C];if(!K){H=true;}else{if(!this.silent){}var J=K.getScope(this.scope);if(this.signature==YAHOO.util.CustomEvent.FLAT){var A=null;if(G.length>0){A=G[0];}try{F=K.fn.call(J,A,K.obj);}catch(E){this.lastError=E;}}else{try{F=K.fn.call(J,this.type,G,K.obj);}catch(E){this.lastError=E;}}if(false===F){if(!this.silent){}return false;}}}if(H){var I=[],B=this.subscribers;for(C=0,D=B.length;C<D;C=C+1){I.push(B[C]);}this.subscribers=I;}return true;},unsubscribeAll:function(){for(var B=0,A=this.subscribers.length;B<A;++B){this._delete(A-1-B);}this.subscribers=[];return B;},_delete:function(A){var B=this.subscribers[A];if(B){delete B.fn;delete B.obj;}this.subscribers[A]=null;},toString:function(){return"CustomEvent: '"+this.type+"', scope: "+this.scope;}};YAHOO.util.Subscriber=function(B,C,A){this.fn=B;this.obj=YAHOO.lang.isUndefined(C)?null:C;this.override=A;};YAHOO.util.Subscriber.prototype.getScope=function(A){if(this.override){if(this.override===true){return this.obj;}else{return this.override;}}return A;};YAHOO.util.Subscriber.prototype.contains=function(A,B){if(B){return(this.fn==A&&this.obj==B);}else{return(this.fn==A);}};YAHOO.util.Subscriber.prototype.toString=function(){return"Subscriber { obj: "+this.obj+", override: "+(this.override||"no")+" }";};if(!YAHOO.util.Event){YAHOO.util.Event=function(){var H=false;var I=[];var J=[];var G=[];var E=[];var C=0;var F=[];var B=[];var A=0;var D={63232:38,63233:40,63234:37,63235:39,63276:33,63277:34,25:9};return{POLL_RETRYS:4000,POLL_INTERVAL:10,EL:0,TYPE:1,FN:2,WFN:3,UNLOAD_OBJ:3,ADJ_SCOPE:4,OBJ:5,OVERRIDE:6,lastError:null,isSafari:YAHOO.env.ua.webkit,webkit:YAHOO.env.ua.webkit,isIE:YAHOO.env.ua.ie,_interval:null,_dri:null,DOMReady:false,startInterval:function(){if(!this._interval){var K=this;var L=function(){K._tryPreloadAttach();};this._interval=setInterval(L,this.POLL_INTERVAL);}},onAvailable:function(P,M,Q,O,N){var K=(YAHOO.lang.isString(P))?[P]:P;for(var L=0;L<K.length;L=L+1){F.push({id:K[L],fn:M,obj:Q,override:O,checkReady:N});}C=this.POLL_RETRYS;this.startInterval();},onContentReady:function(M,K,N,L){this.onAvailable(M,K,N,L,true);},onDOMReady:function(K,M,L){if(this.DOMReady){setTimeout(function(){var N=window;if(L){if(L===true){N=M;}else{N=L;}}K.call(N,"DOMReady",[],M);},0);}else{this.DOMReadyEvent.subscribe(K,M,L);}},addListener:function(M,K,V,Q,L){if(!V||!V.call){return false;}if(this._isValidCollection(M)){var W=true;for(var R=0,T=M.length;R<T;++R){W=this.on(M[R],K,V,Q,L)&&W;}return W;}else{if(YAHOO.lang.isString(M)){var P=this.getEl(M);if(P){M=P;}else{this.onAvailable(M,function(){YAHOO.util.Event.on(M,K,V,Q,L);});return true;}}}if(!M){return false;}if("unload"==K&&Q!==this){J[J.length]=[M,K,V,Q,L];return true;}var Y=M;if(L){if(L===true){Y=Q;}else{Y=L;}}var N=function(Z){return V.call(Y,YAHOO.util.Event.getEvent(Z,M),Q);};var X=[M,K,V,N,Y,Q,L];var S=I.length;I[S]=X;if(this.useLegacyEvent(M,K)){var O=this.getLegacyIndex(M,K);if(O==-1||M!=G[O][0]){O=G.length;B[M.id+K]=O;G[O]=[M,K,M["on"+K]];E[O]=[];M["on"+K]=function(Z){YAHOO.util.Event.fireLegacyEvent(YAHOO.util.Event.getEvent(Z),O);};}E[O].push(X);}else{try{this._simpleAdd(M,K,N,false);}catch(U){this.lastError=U;this.removeListener(M,K,V);return false;}}return true;},fireLegacyEvent:function(O,M){var Q=true,K,S,R,T,P;S=E[M];for(var L=0,N=S.length;L<N;++L){R=S[L];if(R&&R[this.WFN]){T=R[this.ADJ_SCOPE];P=R[this.WFN].call(T,O);Q=(Q&&P);}}K=G[M];if(K&&K[2]){K[2](O);}return Q;},getLegacyIndex:function(L,M){var K=this.generateId(L)+M;if(typeof B[K]=="undefined"){return -1;}else{return B[K];}},useLegacyEvent:function(L,M){if(this.webkit&&("click"==M||"dblclick"==M)){var K=parseInt(this.webkit,10);if(!isNaN(K)&&K<418){return true;}}return false;},removeListener:function(L,K,T){var O,R,V;if(typeof L=="string"){L=this.getEl(L);}else{if(this._isValidCollection(L)){var U=true;for(O=0,R=L.length;O<R;++O){U=(this.removeListener(L[O],K,T)&&U);}return U;}}if(!T||!T.call){return this.purgeElement(L,false,K);}if("unload"==K){for(O=0,R=J.length;O<R;O++){V=J[O];if(V&&V[0]==L&&V[1]==K&&V[2]==T){J[O]=null;return true;}}return false;}var P=null;var Q=arguments[3];if("undefined"===typeof Q){Q=this._getCacheIndex(L,K,T);}if(Q>=0){P=I[Q];}if(!L||!P){return false;}if(this.useLegacyEvent(L,K)){var N=this.getLegacyIndex(L,K);var M=E[N];if(M){for(O=0,R=M.length;O<R;++O){V=M[O];if(V&&V[this.EL]==L&&V[this.TYPE]==K&&V[this.FN]==T){M[O]=null;break;}}}}else{try{this._simpleRemove(L,K,P[this.WFN],false);}catch(S){this.lastError=S;return false;}}delete I[Q][this.WFN];delete I[Q][this.FN];I[Q]=null;return true;},getTarget:function(M,L){var K=M.target||M.srcElement;return this.resolveTextNode(K);},resolveTextNode:function(K){if(K&&3==K.nodeType){return K.parentNode;}else{return K;}},getPageX:function(L){var K=L.pageX;if(!K&&0!==K){K=L.clientX||0;if(this.isIE){K+=this._getScrollLeft();}}return K;},getPageY:function(K){var L=K.pageY;if(!L&&0!==L){L=K.clientY||0;if(this.isIE){L+=this._getScrollTop();}}return L;},getXY:function(K){return[this.getPageX(K),this.getPageY(K)];
},getRelatedTarget:function(L){var K=L.relatedTarget;if(!K){if(L.type=="mouseout"){K=L.toElement;}else{if(L.type=="mouseover"){K=L.fromElement;}}}return this.resolveTextNode(K);},getTime:function(M){if(!M.time){var L=new Date().getTime();try{M.time=L;}catch(K){this.lastError=K;return L;}}return M.time;},stopEvent:function(K){this.stopPropagation(K);this.preventDefault(K);},stopPropagation:function(K){if(K.stopPropagation){K.stopPropagation();}else{K.cancelBubble=true;}},preventDefault:function(K){if(K.preventDefault){K.preventDefault();}else{K.returnValue=false;}},getEvent:function(M,K){var L=M||window.event;if(!L){var N=this.getEvent.caller;while(N){L=N.arguments[0];if(L&&Event==L.constructor){break;}N=N.caller;}}return L;},getCharCode:function(L){var K=L.keyCode||L.charCode||0;if(YAHOO.env.ua.webkit&&(K in D)){K=D[K];}return K;},_getCacheIndex:function(O,P,N){for(var M=0,L=I.length;M<L;++M){var K=I[M];if(K&&K[this.FN]==N&&K[this.EL]==O&&K[this.TYPE]==P){return M;}}return -1;},generateId:function(K){var L=K.id;if(!L){L="yuievtautoid-"+A;++A;K.id=L;}return L;},_isValidCollection:function(L){try{return(L&&typeof L!=="string"&&L.length&&!L.tagName&&!L.alert&&typeof L[0]!=="undefined");}catch(K){return false;}},elCache:{},getEl:function(K){return(typeof K==="string")?document.getElementById(K):K;},clearCache:function(){},DOMReadyEvent:new YAHOO.util.CustomEvent("DOMReady",this),_load:function(L){if(!H){H=true;var K=YAHOO.util.Event;K._ready();K._tryPreloadAttach();}},_ready:function(L){var K=YAHOO.util.Event;if(!K.DOMReady){K.DOMReady=true;K.DOMReadyEvent.fire();K._simpleRemove(document,"DOMContentLoaded",K._ready);}},_tryPreloadAttach:function(){if(this.locked){return false;}if(this.isIE){if(!this.DOMReady){this.startInterval();return false;}}this.locked=true;var P=!H;if(!P){P=(C>0);}var O=[];var Q=function(S,T){var R=S;if(T.override){if(T.override===true){R=T.obj;}else{R=T.override;}}T.fn.call(R,T.obj);};var L,K,N,M;for(L=0,K=F.length;L<K;++L){N=F[L];if(N&&!N.checkReady){M=this.getEl(N.id);if(M){Q(M,N);F[L]=null;}else{O.push(N);}}}for(L=0,K=F.length;L<K;++L){N=F[L];if(N&&N.checkReady){M=this.getEl(N.id);if(M){if(H||M.nextSibling){Q(M,N);F[L]=null;}}else{O.push(N);}}}C=(O.length===0)?0:C-1;if(P){this.startInterval();}else{clearInterval(this._interval);this._interval=null;}this.locked=false;return true;},purgeElement:function(O,P,R){var M=(YAHOO.lang.isString(O))?this.getEl(O):O;var Q=this.getListeners(M,R),N,K;if(Q){for(N=0,K=Q.length;N<K;++N){var L=Q[N];this.removeListener(M,L.type,L.fn,L.index);}}if(P&&M&&M.childNodes){for(N=0,K=M.childNodes.length;N<K;++N){this.purgeElement(M.childNodes[N],P,R);}}},getListeners:function(M,K){var P=[],L;if(!K){L=[I,J];}else{if(K==="unload"){L=[J];}else{L=[I];}}var R=(YAHOO.lang.isString(M))?this.getEl(M):M;for(var O=0;O<L.length;O=O+1){var T=L[O];if(T&&T.length>0){for(var Q=0,S=T.length;Q<S;++Q){var N=T[Q];if(N&&N[this.EL]===R&&(!K||K===N[this.TYPE])){P.push({type:N[this.TYPE],fn:N[this.FN],obj:N[this.OBJ],adjust:N[this.OVERRIDE],scope:N[this.ADJ_SCOPE],index:Q});}}}}return(P.length)?P:null;},_unload:function(R){var Q=YAHOO.util.Event,O,N,L,K,M;for(O=0,K=J.length;O<K;++O){L=J[O];if(L){var P=window;if(L[Q.ADJ_SCOPE]){if(L[Q.ADJ_SCOPE]===true){P=L[Q.UNLOAD_OBJ];}else{P=L[Q.ADJ_SCOPE];}}L[Q.FN].call(P,Q.getEvent(R,L[Q.EL]),L[Q.UNLOAD_OBJ]);J[O]=null;L=null;P=null;}}J=null;if(YAHOO.env.ua.IE&&I&&I.length>0){N=I.length;while(N){M=N-1;L=I[M];if(L){L[Q.EL].clearAttributes();}N=N-1;}L=null;}G=null;Q._simpleRemove(window,"unload",Q._unload);},_getScrollLeft:function(){return this._getScroll()[1];},_getScrollTop:function(){return this._getScroll()[0];},_getScroll:function(){var K=document.documentElement,L=document.body;if(K&&(K.scrollTop||K.scrollLeft)){return[K.scrollTop,K.scrollLeft];}else{if(L){return[L.scrollTop,L.scrollLeft];}else{return[0,0];}}},regCE:function(){},_simpleAdd:function(){if(window.addEventListener){return function(M,N,L,K){M.addEventListener(N,L,(K));};}else{if(window.attachEvent){return function(M,N,L,K){M.attachEvent("on"+N,L);};}else{return function(){};}}}(),_simpleRemove:function(){if(window.removeEventListener){return function(M,N,L,K){M.removeEventListener(N,L,(K));};}else{if(window.detachEvent){return function(L,M,K){L.detachEvent("on"+M,K);};}else{return function(){};}}}()};}();(function(){var A=YAHOO.util.Event;A.on=A.addListener;if(A.isIE){YAHOO.util.Event.onDOMReady(YAHOO.util.Event._tryPreloadAttach,YAHOO.util.Event,true);A._dri=setInterval(function(){var C=document.createElement("p");try{C.doScroll("left");clearInterval(A._dri);A._dri=null;A._ready();C=null;}catch(B){C=null;}},A.POLL_INTERVAL);}else{if(A.webkit){A._dri=setInterval(function(){var B=document.readyState;if("loaded"==B||"complete"==B){clearInterval(A._dri);A._dri=null;A._ready();}},A.POLL_INTERVAL);}else{A._simpleAdd(document,"DOMContentLoaded",A._ready);}}A._simpleAdd(window,"load",A._load);A._simpleAdd(window,"unload",A._unload);A._tryPreloadAttach();})();}YAHOO.util.EventProvider=function(){};YAHOO.util.EventProvider.prototype={__yui_events:null,__yui_subscribers:null,subscribe:function(A,C,F,E){this.__yui_events=this.__yui_events||{};var D=this.__yui_events[A];if(D){D.subscribe(C,F,E);}else{this.__yui_subscribers=this.__yui_subscribers||{};var B=this.__yui_subscribers;if(!B[A]){B[A]=[];}B[A].push({fn:C,obj:F,override:E});}},unsubscribe:function(C,E,G){this.__yui_events=this.__yui_events||{};var A=this.__yui_events;if(C){var F=A[C];if(F){return F.unsubscribe(E,G);}}else{var B=true;for(var D in A){if(YAHOO.lang.hasOwnProperty(A,D)){B=B&&A[D].unsubscribe(E,G);}}return B;}return false;},unsubscribeAll:function(A){return this.unsubscribe(A);},createEvent:function(G,D){this.__yui_events=this.__yui_events||{};var A=D||{};var I=this.__yui_events;if(I[G]){}else{var H=A.scope||this;var E=(A.silent);var B=new YAHOO.util.CustomEvent(G,H,E,YAHOO.util.CustomEvent.FLAT);I[G]=B;if(A.onSubscribeCallback){B.subscribeEvent.subscribe(A.onSubscribeCallback);}this.__yui_subscribers=this.__yui_subscribers||{};
var F=this.__yui_subscribers[G];if(F){for(var C=0;C<F.length;++C){B.subscribe(F[C].fn,F[C].obj,F[C].override);}}}return I[G];},fireEvent:function(E,D,A,C){this.__yui_events=this.__yui_events||{};var G=this.__yui_events[E];if(!G){return null;}var B=[];for(var F=1;F<arguments.length;++F){B.push(arguments[F]);}return G.fire.apply(G,B);},hasEvent:function(A){if(this.__yui_events){if(this.__yui_events[A]){return true;}}return false;}};YAHOO.util.KeyListener=function(A,F,B,C){if(!A){}else{if(!F){}else{if(!B){}}}if(!C){C=YAHOO.util.KeyListener.KEYDOWN;}var D=new YAHOO.util.CustomEvent("keyPressed");this.enabledEvent=new YAHOO.util.CustomEvent("enabled");this.disabledEvent=new YAHOO.util.CustomEvent("disabled");if(typeof A=="string"){A=document.getElementById(A);}if(typeof B=="function"){D.subscribe(B);}else{D.subscribe(B.fn,B.scope,B.correctScope);}function E(J,I){if(!F.shift){F.shift=false;}if(!F.alt){F.alt=false;}if(!F.ctrl){F.ctrl=false;}if(J.shiftKey==F.shift&&J.altKey==F.alt&&J.ctrlKey==F.ctrl){var G;if(F.keys instanceof Array){for(var H=0;H<F.keys.length;H++){G=F.keys[H];if(G==J.charCode){D.fire(J.charCode,J);break;}else{if(G==J.keyCode){D.fire(J.keyCode,J);break;}}}}else{G=F.keys;if(G==J.charCode){D.fire(J.charCode,J);}else{if(G==J.keyCode){D.fire(J.keyCode,J);}}}}}this.enable=function(){if(!this.enabled){YAHOO.util.Event.addListener(A,C,E);this.enabledEvent.fire(F);}this.enabled=true;};this.disable=function(){if(this.enabled){YAHOO.util.Event.removeListener(A,C,E);this.disabledEvent.fire(F);}this.enabled=false;};this.toString=function(){return"KeyListener ["+F.keys+"] "+A.tagName+(A.id?"["+A.id+"]":"");};};YAHOO.util.KeyListener.KEYDOWN="keydown";YAHOO.util.KeyListener.KEYUP="keyup";YAHOO.util.KeyListener.KEY={ALT:18,BACK_SPACE:8,CAPS_LOCK:20,CONTROL:17,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,LEFT:37,META:224,NUM_LOCK:144,PAGE_DOWN:34,PAGE_UP:33,PAUSE:19,PRINTSCREEN:44,RIGHT:39,SCROLL_LOCK:145,SHIFT:16,SPACE:32,TAB:9,UP:38};YAHOO.register("event",YAHOO.util.Event,{version:"2.4.0",build:"733"});YAHOO.register("yahoo-dom-event", YAHOO, {version: "2.4.0", build: "733"});
/*
Copyright (c) 2007, Yahoo! Inc. All rights reserved.
Code licensed under the BSD License:
http://developer.yahoo.net/yui/license.txt
version: 2.4.0
*/
YAHOO.util.Get=function(){var I={},H=0,B=0,O=false,A=YAHOO.env.ua,D=YAHOO.lang;var Q=function(U,R,V){var S=V||window,W=S.document,X=W.createElement(U);for(var T in R){if(R[T]&&YAHOO.lang.hasOwnProperty(R,T)){X.setAttribute(T,R[T]);}}return X;};var N=function(R,S){return Q("link",{"id":"yui__dyn_"+(B++),"type":"text/css","rel":"stylesheet","href":R},S);};var M=function(R,S){return Q("script",{"id":"yui__dyn_"+(B++),"type":"text/javascript","src":R},S);};var K=function(R){return{tId:R.tId,win:R.win,data:R.data,nodes:R.nodes,purge:function(){J(this.tId);}};};var P=function(T){var R=I[T];if(R.onFailure){var S=R.scope||R.win;R.onFailure.call(S,K(R));}};var F=function(T){var R=I[T];R.finished=true;if(R.aborted){P(T);return ;}if(R.onSuccess){var S=R.scope||R.win;R.onSuccess.call(S,K(R));}};var E=function(T,W){var S=I[T];if(S.aborted){P(T);return ;}if(W){S.url.shift();if(S.varName){S.varName.shift();}}else{S.url=(D.isString(S.url))?[S.url]:S.url;if(S.varName){S.varName=(D.isString(S.varName))?[S.varName]:S.varName;}}var Z=S.win,Y=Z.document,X=Y.getElementsByTagName("head")[0],U;if(S.url.length===0){if(S.type==="script"&&A.webkit&&A.webkit<420&&!S.finalpass&&!S.varName){var V=M(null,S.win);V.innerHTML="YAHOO.util.Get._finalize(\""+T+"\");";S.nodes.push(V);X.appendChild(V);}else{F(T);}return ;}var R=S.url[0];if(S.type==="script"){U=M(R,Z);}else{U=N(R,Z);}G(S.type,U,T,R,Z,S.url.length);S.nodes.push(U);X.appendChild(U);if((A.webkit||A.gecko)&&S.type==="css"){E(T,R);}};var C=function(){if(O){return ;}O=true;for(var R in I){var S=I[R];if(S.autopurge&&S.finished){J(S.tId);}}O=false;};var J=function(X){var U=I[X];if(U){var W=U.nodes,R=W.length,V=U.win.document,T=V.getElementsByTagName("head")[0];for(var S=0;S<R;S=S+1){T.removeChild(W[S]);}}};var L=function(S,R,T){var V="q"+(H++);T=T||{};if(H%YAHOO.util.Get.PURGE_THRESH===0){C();}I[V]=D.merge(T,{tId:V,type:S,url:R,finished:false,nodes:[]});var U=I[V];U.win=U.win||window;U.scope=U.scope||U.win;U.autopurge=("autopurge" in U)?U.autopurge:(S==="script")?true:false;D.later(0,U,E,V);return{tId:V};};var G=function(a,V,U,S,W,X,Z){var Y=Z||E;if(A.ie){V.onreadystatechange=function(){var b=this.readyState;if("loaded"===b||"complete"===b){Y(U,S);}};}else{if(A.webkit){if(a==="script"){if(A.webkit>419){V.addEventListener("load",function(){Y(U,S);});}else{var R=I[U];if(R.varName){var T=YAHOO.util.Get.POLL_FREQ;R.maxattempts=YAHOO.util.Get.TIMEOUT/T;R.attempts=0;R._cache=R.varName[0].split(".");R.timer=D.later(T,R,function(f){var d=this._cache,c=d.length,b=this.win,e;for(e=0;e<c;e=e+1){b=b[d[e]];if(!b){this.attempts++;if(this.attempts++>this.maxattempts){R.timer.cancel();P(U);}else{}return ;}}R.timer.cancel();Y(U,S);},null,true);}else{D.later(YAHOO.util.Get.POLL_FREQ,null,Y,[U,S]);}}}}else{V.onload=function(){Y(U,S);};}}};return{POLL_FREQ:10,PURGE_THRESH:20,TIMEOUT:2000,_finalize:function(R){D.later(0,null,F,R);},abort:function(S){var T=(D.isString(S))?S:S.tId;var R=I[T];if(R){R.aborted=true;}},script:function(R,S){return L("script",R,S);},css:function(R,S){return L("css",R,S);}};}();YAHOO.register("get",YAHOO.util.Get,{version:"2.4.0",build:"733"});/*
Copyright (c) 2007, Yahoo! Inc. All rights reserved.
Code licensed under the BSD License:
http://developer.yahoo.net/yui/license.txt
version: 2.4.0
*/
YAHOO.widget.AutoComplete=function(G,B,J,D){if(G&&B&&J){if(J instanceof YAHOO.widget.DataSource){this.dataSource=J;}else{return ;}if(YAHOO.util.Dom.inDocument(G)){if(YAHOO.lang.isString(G)){this._sName="instance"+YAHOO.widget.AutoComplete._nIndex+" "+G;this._oTextbox=document.getElementById(G);}else{this._sName=(G.id)?"instance"+YAHOO.widget.AutoComplete._nIndex+" "+G.id:"instance"+YAHOO.widget.AutoComplete._nIndex;this._oTextbox=G;}YAHOO.util.Dom.addClass(this._oTextbox,"yui-ac-input");}else{return ;}if(YAHOO.util.Dom.inDocument(B)){if(YAHOO.lang.isString(B)){this._oContainer=document.getElementById(B);}else{this._oContainer=B;}if(this._oContainer.style.display=="none"){}var E=this._oContainer.parentNode;var A=E.tagName.toLowerCase();if(A=="div"){YAHOO.util.Dom.addClass(E,"yui-ac");}else{}}else{return ;}if(D&&(D.constructor==Object)){for(var I in D){if(I){this[I]=D[I];}}}this._initContainer();this._initProps();this._initList();this._initContainerHelpers();var H=this;var F=this._oTextbox;var C=this._oContainer._oContent;YAHOO.util.Event.addListener(F,"keyup",H._onTextboxKeyUp,H);YAHOO.util.Event.addListener(F,"keydown",H._onTextboxKeyDown,H);YAHOO.util.Event.addListener(F,"focus",H._onTextboxFocus,H);YAHOO.util.Event.addListener(F,"blur",H._onTextboxBlur,H);YAHOO.util.Event.addListener(C,"mouseover",H._onContainerMouseover,H);YAHOO.util.Event.addListener(C,"mouseout",H._onContainerMouseout,H);YAHOO.util.Event.addListener(C,"scroll",H._onContainerScroll,H);YAHOO.util.Event.addListener(C,"resize",H._onContainerResize,H);YAHOO.util.Event.addListener(F,"keypress",H._onTextboxKeyPress,H);YAHOO.util.Event.addListener(window,"unload",H._onWindowUnload,H);this.textboxFocusEvent=new YAHOO.util.CustomEvent("textboxFocus",this);this.textboxKeyEvent=new YAHOO.util.CustomEvent("textboxKey",this);this.dataRequestEvent=new YAHOO.util.CustomEvent("dataRequest",this);this.dataReturnEvent=new YAHOO.util.CustomEvent("dataReturn",this);this.dataErrorEvent=new YAHOO.util.CustomEvent("dataError",this);this.containerExpandEvent=new YAHOO.util.CustomEvent("containerExpand",this);this.typeAheadEvent=new YAHOO.util.CustomEvent("typeAhead",this);this.itemMouseOverEvent=new YAHOO.util.CustomEvent("itemMouseOver",this);this.itemMouseOutEvent=new YAHOO.util.CustomEvent("itemMouseOut",this);this.itemArrowToEvent=new YAHOO.util.CustomEvent("itemArrowTo",this);this.itemArrowFromEvent=new YAHOO.util.CustomEvent("itemArrowFrom",this);this.itemSelectEvent=new YAHOO.util.CustomEvent("itemSelect",this);this.unmatchedItemSelectEvent=new YAHOO.util.CustomEvent("unmatchedItemSelect",this);this.selectionEnforceEvent=new YAHOO.util.CustomEvent("selectionEnforce",this);this.containerCollapseEvent=new YAHOO.util.CustomEvent("containerCollapse",this);this.textboxBlurEvent=new YAHOO.util.CustomEvent("textboxBlur",this);F.setAttribute("autocomplete","off");YAHOO.widget.AutoComplete._nIndex++;}else{}};YAHOO.widget.AutoComplete.prototype.dataSource=null;YAHOO.widget.AutoComplete.prototype.minQueryLength=1;YAHOO.widget.AutoComplete.prototype.maxResultsDisplayed=10;YAHOO.widget.AutoComplete.prototype.queryDelay=0.2;YAHOO.widget.AutoComplete.prototype.highlightClassName="yui-ac-highlight";YAHOO.widget.AutoComplete.prototype.prehighlightClassName=null;YAHOO.widget.AutoComplete.prototype.delimChar=null;YAHOO.widget.AutoComplete.prototype.autoHighlight=true;YAHOO.widget.AutoComplete.prototype.typeAhead=false;YAHOO.widget.AutoComplete.prototype.animHoriz=false;YAHOO.widget.AutoComplete.prototype.animVert=true;YAHOO.widget.AutoComplete.prototype.animSpeed=0.3;YAHOO.widget.AutoComplete.prototype.forceSelection=false;YAHOO.widget.AutoComplete.prototype.allowBrowserAutocomplete=true;YAHOO.widget.AutoComplete.prototype.alwaysShowContainer=false;YAHOO.widget.AutoComplete.prototype.useIFrame=false;YAHOO.widget.AutoComplete.prototype.useShadow=false;YAHOO.widget.AutoComplete.prototype.toString=function(){return"AutoComplete "+this._sName;};YAHOO.widget.AutoComplete.prototype.isContainerOpen=function(){return this._bContainerOpen;};YAHOO.widget.AutoComplete.prototype.getListItems=function(){return this._aListItems;};YAHOO.widget.AutoComplete.prototype.getListItemData=function(A){if(A._oResultData){return A._oResultData;}else{return false;}};YAHOO.widget.AutoComplete.prototype.setHeader=function(A){if(A){if(this._oContainer._oContent._oHeader){this._oContainer._oContent._oHeader.innerHTML=A;this._oContainer._oContent._oHeader.style.display="block";}}else{this._oContainer._oContent._oHeader.innerHTML="";this._oContainer._oContent._oHeader.style.display="none";}};YAHOO.widget.AutoComplete.prototype.setFooter=function(A){if(A){if(this._oContainer._oContent._oFooter){this._oContainer._oContent._oFooter.innerHTML=A;this._oContainer._oContent._oFooter.style.display="block";}}else{this._oContainer._oContent._oFooter.innerHTML="";this._oContainer._oContent._oFooter.style.display="none";}};YAHOO.widget.AutoComplete.prototype.setBody=function(A){if(A){if(this._oContainer._oContent._oBody){this._oContainer._oContent._oBody.innerHTML=A;this._oContainer._oContent._oBody.style.display="block";this._oContainer._oContent.style.display="block";}}else{this._oContainer._oContent._oBody.innerHTML="";this._oContainer._oContent.style.display="none";}this._maxResultsDisplayed=0;};YAHOO.widget.AutoComplete.prototype.formatResult=function(B,C){var A=B[0];if(A){return A;}else{return"";}};YAHOO.widget.AutoComplete.prototype.doBeforeExpandContainer=function(A,B,D,C){return true;};YAHOO.widget.AutoComplete.prototype.sendQuery=function(A){this._sendQuery(A);};YAHOO.widget.AutoComplete.prototype.doBeforeSendQuery=function(A){return A;};YAHOO.widget.AutoComplete.prototype.destroy=function(){var B=this.toString();var A=this._oTextbox;var D=this._oContainer;this.textboxFocusEvent.unsubscribe();this.textboxKeyEvent.unsubscribe();this.dataRequestEvent.unsubscribe();this.dataReturnEvent.unsubscribe();this.dataErrorEvent.unsubscribe();this.containerExpandEvent.unsubscribe();this.typeAheadEvent.unsubscribe();
this.itemMouseOverEvent.unsubscribe();this.itemMouseOutEvent.unsubscribe();this.itemArrowToEvent.unsubscribe();this.itemArrowFromEvent.unsubscribe();this.itemSelectEvent.unsubscribe();this.unmatchedItemSelectEvent.unsubscribe();this.selectionEnforceEvent.unsubscribe();this.containerCollapseEvent.unsubscribe();this.textboxBlurEvent.unsubscribe();YAHOO.util.Event.purgeElement(A,true);YAHOO.util.Event.purgeElement(D,true);D.innerHTML="";for(var C in this){if(YAHOO.lang.hasOwnProperty(this,C)){this[C]=null;}}};YAHOO.widget.AutoComplete.prototype.textboxFocusEvent=null;YAHOO.widget.AutoComplete.prototype.textboxKeyEvent=null;YAHOO.widget.AutoComplete.prototype.dataRequestEvent=null;YAHOO.widget.AutoComplete.prototype.dataReturnEvent=null;YAHOO.widget.AutoComplete.prototype.dataErrorEvent=null;YAHOO.widget.AutoComplete.prototype.containerExpandEvent=null;YAHOO.widget.AutoComplete.prototype.typeAheadEvent=null;YAHOO.widget.AutoComplete.prototype.itemMouseOverEvent=null;YAHOO.widget.AutoComplete.prototype.itemMouseOutEvent=null;YAHOO.widget.AutoComplete.prototype.itemArrowToEvent=null;YAHOO.widget.AutoComplete.prototype.itemArrowFromEvent=null;YAHOO.widget.AutoComplete.prototype.itemSelectEvent=null;YAHOO.widget.AutoComplete.prototype.unmatchedItemSelectEvent=null;YAHOO.widget.AutoComplete.prototype.selectionEnforceEvent=null;YAHOO.widget.AutoComplete.prototype.containerCollapseEvent=null;YAHOO.widget.AutoComplete.prototype.textboxBlurEvent=null;YAHOO.widget.AutoComplete._nIndex=0;YAHOO.widget.AutoComplete.prototype._sName=null;YAHOO.widget.AutoComplete.prototype._oTextbox=null;YAHOO.widget.AutoComplete.prototype._bFocused=true;YAHOO.widget.AutoComplete.prototype._oAnim=null;YAHOO.widget.AutoComplete.prototype._oContainer=null;YAHOO.widget.AutoComplete.prototype._bContainerOpen=false;YAHOO.widget.AutoComplete.prototype._bOverContainer=false;YAHOO.widget.AutoComplete.prototype._aListItems=null;YAHOO.widget.AutoComplete.prototype._nDisplayedItems=0;YAHOO.widget.AutoComplete.prototype._maxResultsDisplayed=0;YAHOO.widget.AutoComplete.prototype._sCurQuery=null;YAHOO.widget.AutoComplete.prototype._sSavedQuery=null;YAHOO.widget.AutoComplete.prototype._oCurItem=null;YAHOO.widget.AutoComplete.prototype._bItemSelected=false;YAHOO.widget.AutoComplete.prototype._nKeyCode=null;YAHOO.widget.AutoComplete.prototype._nDelayID=-1;YAHOO.widget.AutoComplete.prototype._iFrameSrc="javascript:false;";YAHOO.widget.AutoComplete.prototype._queryInterval=null;YAHOO.widget.AutoComplete.prototype._sLastTextboxValue=null;YAHOO.widget.AutoComplete.prototype._initProps=function(){var B=this.minQueryLength;if(!YAHOO.lang.isNumber(B)){this.minQueryLength=1;}var D=this.maxResultsDisplayed;if(!YAHOO.lang.isNumber(D)||(D<1)){this.maxResultsDisplayed=10;}var E=this.queryDelay;if(!YAHOO.lang.isNumber(E)||(E<0)){this.queryDelay=0.2;}var A=this.delimChar;if(YAHOO.lang.isString(A)&&(A.length>0)){this.delimChar=[A];}else{if(!YAHOO.lang.isArray(A)){this.delimChar=null;}}var C=this.animSpeed;if((this.animHoriz||this.animVert)&&YAHOO.util.Anim){if(!YAHOO.lang.isNumber(C)||(C<0)){this.animSpeed=0.3;}if(!this._oAnim){this._oAnim=new YAHOO.util.Anim(this._oContainer._oContent,{},this.animSpeed);}else{this._oAnim.duration=this.animSpeed;}}if(this.forceSelection&&A){}};YAHOO.widget.AutoComplete.prototype._initContainerHelpers=function(){if(this.useShadow&&!this._oContainer._oShadow){var B=document.createElement("div");B.className="yui-ac-shadow";this._oContainer._oShadow=this._oContainer.appendChild(B);}if(this.useIFrame&&!this._oContainer._oIFrame){var A=document.createElement("iframe");A.src=this._iFrameSrc;A.frameBorder=0;A.scrolling="no";A.style.position="absolute";A.style.width="100%";A.style.height="100%";A.tabIndex=-1;this._oContainer._oIFrame=this._oContainer.appendChild(A);}};YAHOO.widget.AutoComplete.prototype._initContainer=function(){YAHOO.util.Dom.addClass(this._oContainer,"yui-ac-container");if(!this._oContainer._oContent){var D=document.createElement("div");D.className="yui-ac-content";D.style.display="none";this._oContainer._oContent=this._oContainer.appendChild(D);var B=document.createElement("div");B.className="yui-ac-hd";B.style.display="none";this._oContainer._oContent._oHeader=this._oContainer._oContent.appendChild(B);var C=document.createElement("div");C.className="yui-ac-bd";this._oContainer._oContent._oBody=this._oContainer._oContent.appendChild(C);var A=document.createElement("div");A.className="yui-ac-ft";A.style.display="none";this._oContainer._oContent._oFooter=this._oContainer._oContent.appendChild(A);}else{}};YAHOO.widget.AutoComplete.prototype._initList=function(){this._aListItems=[];while(this._oContainer._oContent._oBody.hasChildNodes()){var B=this.getListItems();if(B){for(var A=B.length-1;A>=0;A--){B[A]=null;}}this._oContainer._oContent._oBody.innerHTML="";}var E=document.createElement("ul");E=this._oContainer._oContent._oBody.appendChild(E);for(var C=0;C<this.maxResultsDisplayed;C++){var D=document.createElement("li");D=E.appendChild(D);this._aListItems[C]=D;this._initListItem(D,C);}this._maxResultsDisplayed=this.maxResultsDisplayed;};YAHOO.widget.AutoComplete.prototype._initListItem=function(C,B){var A=this;C.style.display="none";C._nItemIndex=B;C.mouseover=C.mouseout=C.onclick=null;YAHOO.util.Event.addListener(C,"mouseover",A._onItemMouseover,A);YAHOO.util.Event.addListener(C,"mouseout",A._onItemMouseout,A);YAHOO.util.Event.addListener(C,"click",A._onItemMouseclick,A);};YAHOO.widget.AutoComplete.prototype._onIMEDetected=function(A){A._enableIntervalDetection();};YAHOO.widget.AutoComplete.prototype._enableIntervalDetection=function(){var A=this._oTextbox.value;var B=this._sLastTextboxValue;if(A!=B){this._sLastTextboxValue=A;this._sendQuery(A);}};YAHOO.widget.AutoComplete.prototype._cancelIntervalDetection=function(A){if(A._queryInterval){clearInterval(A._queryInterval);}};YAHOO.widget.AutoComplete.prototype._isIgnoreKey=function(A){if((A==9)||(A==13)||(A==16)||(A==17)||(A>=18&&A<=20)||(A==27)||(A>=33&&A<=35)||(A>=36&&A<=40)||(A>=44&&A<=45)){return true;
}return false;};YAHOO.widget.AutoComplete.prototype._sendQuery=function(G){if(this.minQueryLength==-1){this._toggleContainer(false);return ;}var C=(this.delimChar)?this.delimChar:null;if(C){var E=-1;for(var B=C.length-1;B>=0;B--){var F=G.lastIndexOf(C[B]);if(F>E){E=F;}}if(C[B]==" "){for(var A=C.length-1;A>=0;A--){if(G[E-1]==C[A]){E--;break;}}}if(E>-1){var D=E+1;while(G.charAt(D)==" "){D+=1;}this._sSavedQuery=G.substring(0,D);G=G.substr(D);}else{if(G.indexOf(this._sSavedQuery)<0){this._sSavedQuery=null;}}}if((G&&(G.length<this.minQueryLength))||(!G&&this.minQueryLength>0)){if(this._nDelayID!=-1){clearTimeout(this._nDelayID);}this._toggleContainer(false);return ;}G=encodeURIComponent(G);this._nDelayID=-1;G=this.doBeforeSendQuery(G);this.dataRequestEvent.fire(this,G);this.dataSource.getResults(this._populateList,G,this);};YAHOO.widget.AutoComplete.prototype._populateList=function(K,L,I){if(L===null){I.dataErrorEvent.fire(I,K);}if(!I._bFocused||!L){return ;}var A=(navigator.userAgent.toLowerCase().indexOf("opera")!=-1);var O=I._oContainer._oContent.style;O.width=(!A)?null:"";O.height=(!A)?null:"";var H=decodeURIComponent(K);I._sCurQuery=H;I._bItemSelected=false;if(I._maxResultsDisplayed!=I.maxResultsDisplayed){I._initList();}var C=Math.min(L.length,I.maxResultsDisplayed);I._nDisplayedItems=C;if(C>0){I._initContainerHelpers();var D=I._aListItems;for(var G=C-1;G>=0;G--){var N=D[G];var B=L[G];N.innerHTML=I.formatResult(B,H);N.style.display="list-item";N._sResultKey=B[0];N._oResultData=B;}for(var F=D.length-1;F>=C;F--){var M=D[F];M.innerHTML=null;M.style.display="none";M._sResultKey=null;M._oResultData=null;}var J=I.doBeforeExpandContainer(I._oTextbox,I._oContainer,K,L);I._toggleContainer(J);if(I.autoHighlight){var E=D[0];I._toggleHighlight(E,"to");I.itemArrowToEvent.fire(I,E);I._typeAhead(E,K);}else{I._oCurItem=null;}}else{I._toggleContainer(false);}I.dataReturnEvent.fire(I,K,L);};YAHOO.widget.AutoComplete.prototype._clearSelection=function(){var C=this._oTextbox.value;var B=(this.delimChar)?this.delimChar[0]:null;var A=(B)?C.lastIndexOf(B,C.length-2):-1;if(A>-1){this._oTextbox.value=C.substring(0,A);}else{this._oTextbox.value="";}this._sSavedQuery=this._oTextbox.value;this.selectionEnforceEvent.fire(this);};YAHOO.widget.AutoComplete.prototype._textMatchesOption=function(){var D=null;for(var A=this._nDisplayedItems-1;A>=0;A--){var C=this._aListItems[A];var B=C._sResultKey.toLowerCase();if(B==this._sCurQuery.toLowerCase()){D=C;break;}}return(D);};YAHOO.widget.AutoComplete.prototype._typeAhead=function(E,G){if(!this.typeAhead||(this._nKeyCode==8)){return ;}var B=this._oTextbox;var F=this._oTextbox.value;if(!B.setSelectionRange&&!B.createTextRange){return ;}var C=F.length;this._updateValue(E);var D=B.value.length;this._selectText(B,C,D);var A=B.value.substr(C,D);this.typeAheadEvent.fire(this,G,A);};YAHOO.widget.AutoComplete.prototype._selectText=function(A,B,C){if(A.setSelectionRange){A.setSelectionRange(B,C);}else{if(A.createTextRange){var D=A.createTextRange();D.moveStart("character",B);D.moveEnd("character",C-A.value.length);D.select();}else{A.select();}}};YAHOO.widget.AutoComplete.prototype._toggleContainerHelpers=function(B){var D=false;var C=this._oContainer._oContent.offsetWidth+"px";var A=this._oContainer._oContent.offsetHeight+"px";if(this.useIFrame&&this._oContainer._oIFrame){D=true;if(B){this._oContainer._oIFrame.style.width=C;this._oContainer._oIFrame.style.height=A;}else{this._oContainer._oIFrame.style.width=0;this._oContainer._oIFrame.style.height=0;}}if(this.useShadow&&this._oContainer._oShadow){D=true;if(B){this._oContainer._oShadow.style.width=C;this._oContainer._oShadow.style.height=A;}else{this._oContainer._oShadow.style.width=0;this._oContainer._oShadow.style.height=0;}}};YAHOO.widget.AutoComplete.prototype._toggleContainer=function(J){var L=this._oContainer;if(this.alwaysShowContainer&&this._bContainerOpen){return ;}if(!J){this._oContainer._oContent.scrollTop=0;var C=this._aListItems;if(C&&(C.length>0)){for(var G=C.length-1;G>=0;G--){C[G].style.display="none";}}if(this._oCurItem){this._toggleHighlight(this._oCurItem,"from");}this._oCurItem=null;this._nDisplayedItems=0;this._sCurQuery=null;}if(!J&&!this._bContainerOpen){L._oContent.style.display="none";return ;}var B=this._oAnim;if(B&&B.getEl()&&(this.animHoriz||this.animVert)){if(!J){this._toggleContainerHelpers(J);}if(B.isAnimated()){B.stop();}var H=L._oContent.cloneNode(true);L.appendChild(H);H.style.top="-9000px";H.style.display="block";var F=H.offsetWidth;var D=H.offsetHeight;var A=(this.animHoriz)?0:F;var E=(this.animVert)?0:D;B.attributes=(J)?{width:{to:F},height:{to:D}}:{width:{to:A},height:{to:E}};if(J&&!this._bContainerOpen){L._oContent.style.width=A+"px";L._oContent.style.height=E+"px";}else{L._oContent.style.width=F+"px";L._oContent.style.height=D+"px";}L.removeChild(H);H=null;var I=this;var K=function(){B.onComplete.unsubscribeAll();if(J){I.containerExpandEvent.fire(I);}else{L._oContent.style.display="none";I.containerCollapseEvent.fire(I);}I._toggleContainerHelpers(J);};L._oContent.style.display="block";B.onComplete.subscribe(K);B.animate();this._bContainerOpen=J;}else{if(J){L._oContent.style.display="block";this.containerExpandEvent.fire(this);}else{L._oContent.style.display="none";this.containerCollapseEvent.fire(this);}this._toggleContainerHelpers(J);this._bContainerOpen=J;}};YAHOO.widget.AutoComplete.prototype._toggleHighlight=function(A,C){var B=this.highlightClassName;if(this._oCurItem){YAHOO.util.Dom.removeClass(this._oCurItem,B);}if((C=="to")&&B){YAHOO.util.Dom.addClass(A,B);this._oCurItem=A;}};YAHOO.widget.AutoComplete.prototype._togglePrehighlight=function(A,C){if(A==this._oCurItem){return ;}var B=this.prehighlightClassName;if((C=="mouseover")&&B){YAHOO.util.Dom.addClass(A,B);}else{YAHOO.util.Dom.removeClass(A,B);}};YAHOO.widget.AutoComplete.prototype._updateValue=function(F){var C=this._oTextbox;var E=(this.delimChar)?(this.delimChar[0]||this.delimChar):null;var B=this._sSavedQuery;var D=F._sResultKey;C.focus();
C.value="";if(E){if(B){C.value=B;}C.value+=D+E;if(E!=" "){C.value+=" ";}}else{C.value=D;}if(C.type=="textarea"){C.scrollTop=C.scrollHeight;}var A=C.value.length;this._selectText(C,A,A);this._oCurItem=F;};YAHOO.widget.AutoComplete.prototype._selectItem=function(A){this._bItemSelected=true;this._updateValue(A);this._cancelIntervalDetection(this);this.itemSelectEvent.fire(this,A,A._oResultData);this._toggleContainer(false);};YAHOO.widget.AutoComplete.prototype._jumpSelection=function(){if(this._oCurItem){this._selectItem(this._oCurItem);}else{this._toggleContainer(false);}};YAHOO.widget.AutoComplete.prototype._moveSelection=function(G){if(this._bContainerOpen){var D=this._oCurItem;var F=-1;if(D){F=D._nItemIndex;}var C=(G==40)?(F+1):(F-1);if(C<-2||C>=this._nDisplayedItems){return ;}if(D){this._toggleHighlight(D,"from");this.itemArrowFromEvent.fire(this,D);}if(C==-1){if(this.delimChar&&this._sSavedQuery){if(!this._textMatchesOption()){this._oTextbox.value=this._sSavedQuery;}else{this._oTextbox.value=this._sSavedQuery+this._sCurQuery;}}else{this._oTextbox.value=this._sCurQuery;}this._oCurItem=null;return ;}if(C==-2){this._toggleContainer(false);return ;}var B=this._aListItems[C];var E=this._oContainer._oContent;var A=((YAHOO.util.Dom.getStyle(E,"overflow")=="auto")||(YAHOO.util.Dom.getStyle(E,"overflowY")=="auto"));if(A&&(C>-1)&&(C<this._nDisplayedItems)){if(G==40){if((B.offsetTop+B.offsetHeight)>(E.scrollTop+E.offsetHeight)){E.scrollTop=(B.offsetTop+B.offsetHeight)-E.offsetHeight;}else{if((B.offsetTop+B.offsetHeight)<E.scrollTop){E.scrollTop=B.offsetTop;}}}else{if(B.offsetTop<E.scrollTop){this._oContainer._oContent.scrollTop=B.offsetTop;}else{if(B.offsetTop>(E.scrollTop+E.offsetHeight)){this._oContainer._oContent.scrollTop=(B.offsetTop+B.offsetHeight)-E.offsetHeight;}}}}this._toggleHighlight(B,"to");this.itemArrowToEvent.fire(this,B);if(this.typeAhead){this._updateValue(B);}}};YAHOO.widget.AutoComplete.prototype._onItemMouseover=function(A,B){if(B.prehighlightClassName){B._togglePrehighlight(this,"mouseover");}else{B._toggleHighlight(this,"to");}B.itemMouseOverEvent.fire(B,this);};YAHOO.widget.AutoComplete.prototype._onItemMouseout=function(A,B){if(B.prehighlightClassName){B._togglePrehighlight(this,"mouseout");}else{B._toggleHighlight(this,"from");}B.itemMouseOutEvent.fire(B,this);};YAHOO.widget.AutoComplete.prototype._onItemMouseclick=function(A,B){B._toggleHighlight(this,"to");B._selectItem(this);};YAHOO.widget.AutoComplete.prototype._onContainerMouseover=function(A,B){B._bOverContainer=true;};YAHOO.widget.AutoComplete.prototype._onContainerMouseout=function(A,B){B._bOverContainer=false;if(B._oCurItem){B._toggleHighlight(B._oCurItem,"to");}};YAHOO.widget.AutoComplete.prototype._onContainerScroll=function(A,B){B._oTextbox.focus();};YAHOO.widget.AutoComplete.prototype._onContainerResize=function(A,B){B._toggleContainerHelpers(B._bContainerOpen);};YAHOO.widget.AutoComplete.prototype._onTextboxKeyDown=function(A,C){var D=A.keyCode;switch(D){case 9:if(C._oCurItem){if(C.delimChar&&(C._nKeyCode!=D)){if(C._bContainerOpen){YAHOO.util.Event.stopEvent(A);}}C._selectItem(C._oCurItem);}else{C._toggleContainer(false);}break;case 13:var B=(navigator.userAgent.toLowerCase().indexOf("mac")!=-1);if(!B){if(C._oCurItem){if(C._nKeyCode!=D){if(C._bContainerOpen){YAHOO.util.Event.stopEvent(A);}}C._selectItem(C._oCurItem);}else{C._toggleContainer(false);}}break;case 27:C._toggleContainer(false);return ;case 39:C._jumpSelection();break;case 38:YAHOO.util.Event.stopEvent(A);C._moveSelection(D);break;case 40:YAHOO.util.Event.stopEvent(A);C._moveSelection(D);break;default:break;}};YAHOO.widget.AutoComplete.prototype._onTextboxKeyPress=function(A,C){var D=A.keyCode;var B=(navigator.userAgent.toLowerCase().indexOf("mac")!=-1);if(B){switch(D){case 9:if(C._oCurItem){if(C.delimChar&&(C._nKeyCode!=D)){YAHOO.util.Event.stopEvent(A);}}break;case 13:if(C._oCurItem){if(C._nKeyCode!=D){if(C._bContainerOpen){YAHOO.util.Event.stopEvent(A);}}C._selectItem(C._oCurItem);}else{C._toggleContainer(false);}break;case 38:case 40:YAHOO.util.Event.stopEvent(A);break;default:break;}}else{if(D==229){C._queryInterval=setInterval(function(){C._onIMEDetected(C);},500);}}};YAHOO.widget.AutoComplete.prototype._onTextboxKeyUp=function(B,D){D._initProps();var E=B.keyCode;D._nKeyCode=E;var C=this.value;if(D._isIgnoreKey(E)||(C.toLowerCase()==D._sCurQuery)){return ;}else{D._bItemSelected=false;YAHOO.util.Dom.removeClass(D._oCurItem,D.highlightClassName);D._oCurItem=null;D.textboxKeyEvent.fire(D,E);}if(D.queryDelay>0){var A=setTimeout(function(){D._sendQuery(C);},(D.queryDelay*1000));if(D._nDelayID!=-1){clearTimeout(D._nDelayID);}D._nDelayID=A;}else{D._sendQuery(C);}};YAHOO.widget.AutoComplete.prototype._onTextboxFocus=function(A,B){B._oTextbox.setAttribute("autocomplete","off");B._bFocused=true;if(!B._bItemSelected){B.textboxFocusEvent.fire(B);}};YAHOO.widget.AutoComplete.prototype._onTextboxBlur=function(A,B){if(!B._bOverContainer||(B._nKeyCode==9)){if(!B._bItemSelected){var C=B._textMatchesOption();if(!B._bContainerOpen||(B._bContainerOpen&&(C===null))){if(B.forceSelection){B._clearSelection();}else{B.unmatchedItemSelectEvent.fire(B);}}else{if(B.forceSelection){B._selectItem(C);}}}if(B._bContainerOpen){B._toggleContainer(false);}B._cancelIntervalDetection(B);B._bFocused=false;B.textboxBlurEvent.fire(B);}};YAHOO.widget.AutoComplete.prototype._onWindowUnload=function(A,B){if(B&&B._oTextbox&&B.allowBrowserAutocomplete){B._oTextbox.setAttribute("autocomplete","on");}};YAHOO.widget.DataSource=function(){};YAHOO.widget.DataSource.ERROR_DATANULL="Response data was null";YAHOO.widget.DataSource.ERROR_DATAPARSE="Response data could not be parsed";YAHOO.widget.DataSource.prototype.maxCacheEntries=15;YAHOO.widget.DataSource.prototype.queryMatchContains=false;YAHOO.widget.DataSource.prototype.queryMatchSubset=false;YAHOO.widget.DataSource.prototype.queryMatchCase=false;YAHOO.widget.DataSource.prototype.toString=function(){return"DataSource "+this._sName;};YAHOO.widget.DataSource.prototype.getResults=function(A,D,B){var C=this._doQueryCache(A,D,B);
if(C.length===0){this.queryEvent.fire(this,B,D);this.doQuery(A,D,B);}};YAHOO.widget.DataSource.prototype.doQuery=function(A,C,B){};YAHOO.widget.DataSource.prototype.flushCache=function(){if(this._aCache){this._aCache=[];}if(this._aCacheHelper){this._aCacheHelper=[];}this.cacheFlushEvent.fire(this);};YAHOO.widget.DataSource.prototype.queryEvent=null;YAHOO.widget.DataSource.prototype.cacheQueryEvent=null;YAHOO.widget.DataSource.prototype.getResultsEvent=null;YAHOO.widget.DataSource.prototype.getCachedResultsEvent=null;YAHOO.widget.DataSource.prototype.dataErrorEvent=null;YAHOO.widget.DataSource.prototype.cacheFlushEvent=null;YAHOO.widget.DataSource._nIndex=0;YAHOO.widget.DataSource.prototype._sName=null;YAHOO.widget.DataSource.prototype._aCache=null;YAHOO.widget.DataSource.prototype._init=function(){var A=this.maxCacheEntries;if(!YAHOO.lang.isNumber(A)||(A<0)){A=0;}if(A>0&&!this._aCache){this._aCache=[];}this._sName="instance"+YAHOO.widget.DataSource._nIndex;YAHOO.widget.DataSource._nIndex++;this.queryEvent=new YAHOO.util.CustomEvent("query",this);this.cacheQueryEvent=new YAHOO.util.CustomEvent("cacheQuery",this);this.getResultsEvent=new YAHOO.util.CustomEvent("getResults",this);this.getCachedResultsEvent=new YAHOO.util.CustomEvent("getCachedResults",this);this.dataErrorEvent=new YAHOO.util.CustomEvent("dataError",this);this.cacheFlushEvent=new YAHOO.util.CustomEvent("cacheFlush",this);};YAHOO.widget.DataSource.prototype._addCacheElem=function(B){var A=this._aCache;if(!A||!B||!B.query||!B.results){return ;}if(A.length>=this.maxCacheEntries){A.shift();}A.push(B);};YAHOO.widget.DataSource.prototype._doQueryCache=function(A,I,N){var H=[];var G=false;var J=this._aCache;var F=(J)?J.length:0;var K=this.queryMatchContains;var D;if((this.maxCacheEntries>0)&&J&&(F>0)){this.cacheQueryEvent.fire(this,N,I);if(!this.queryMatchCase){D=I;I=I.toLowerCase();}for(var P=F-1;P>=0;P--){var E=J[P];var B=E.results;var C=(!this.queryMatchCase)?encodeURIComponent(E.query).toLowerCase():encodeURIComponent(E.query);if(C==I){G=true;H=B;if(P!=F-1){J.splice(P,1);this._addCacheElem(E);}break;}else{if(this.queryMatchSubset){for(var O=I.length-1;O>=0;O--){var R=I.substr(0,O);if(C==R){G=true;for(var M=B.length-1;M>=0;M--){var Q=B[M];var L=(this.queryMatchCase)?encodeURIComponent(Q[0]).indexOf(I):encodeURIComponent(Q[0]).toLowerCase().indexOf(I);if((!K&&(L===0))||(K&&(L>-1))){H.unshift(Q);}}E={};E.query=I;E.results=H;this._addCacheElem(E);break;}}if(G){break;}}}}if(G){this.getCachedResultsEvent.fire(this,N,D,H);A(D,H,N);}}return H;};YAHOO.widget.DS_XHR=function(C,A,D){if(D&&(D.constructor==Object)){for(var B in D){this[B]=D[B];}}if(!YAHOO.lang.isArray(A)||!YAHOO.lang.isString(C)){return ;}this.schema=A;this.scriptURI=C;this._init();};YAHOO.widget.DS_XHR.prototype=new YAHOO.widget.DataSource();YAHOO.widget.DS_XHR.TYPE_JSON=0;YAHOO.widget.DS_XHR.TYPE_XML=1;YAHOO.widget.DS_XHR.TYPE_FLAT=2;YAHOO.widget.DS_XHR.ERROR_DATAXHR="XHR response failed";YAHOO.widget.DS_XHR.prototype.connMgr=YAHOO.util.Connect;YAHOO.widget.DS_XHR.prototype.connTimeout=0;YAHOO.widget.DS_XHR.prototype.scriptURI=null;YAHOO.widget.DS_XHR.prototype.scriptQueryParam="query";YAHOO.widget.DS_XHR.prototype.scriptQueryAppend="";YAHOO.widget.DS_XHR.prototype.responseType=YAHOO.widget.DS_XHR.TYPE_JSON;YAHOO.widget.DS_XHR.prototype.responseStripAfter="\n<!-";YAHOO.widget.DS_XHR.prototype.doQuery=function(E,G,B){var J=(this.responseType==YAHOO.widget.DS_XHR.TYPE_XML);var D=this.scriptURI+"?"+this.scriptQueryParam+"="+G;if(this.scriptQueryAppend.length>0){D+="&"+this.scriptQueryAppend;}var C=null;var F=this;var I=function(K){if(!F._oConn||(K.tId!=F._oConn.tId)){F.dataErrorEvent.fire(F,B,G,YAHOO.widget.DataSource.ERROR_DATANULL);return ;}for(var N in K){}if(!J){K=K.responseText;}else{K=K.responseXML;}if(K===null){F.dataErrorEvent.fire(F,B,G,YAHOO.widget.DataSource.ERROR_DATANULL);return ;}var M=F.parseResponse(G,K,B);var L={};L.query=decodeURIComponent(G);L.results=M;if(M===null){F.dataErrorEvent.fire(F,B,G,YAHOO.widget.DataSource.ERROR_DATAPARSE);M=[];}else{F.getResultsEvent.fire(F,B,G,M);F._addCacheElem(L);}E(G,M,B);};var A=function(K){F.dataErrorEvent.fire(F,B,G,YAHOO.widget.DS_XHR.ERROR_DATAXHR);return ;};var H={success:I,failure:A};if(YAHOO.lang.isNumber(this.connTimeout)&&(this.connTimeout>0)){H.timeout=this.connTimeout;}if(this._oConn){this.connMgr.abort(this._oConn);}F._oConn=this.connMgr.asyncRequest("GET",D,H,null);};YAHOO.widget.DS_XHR.prototype.parseResponse=function(sQuery,oResponse,oParent){var aSchema=this.schema;var aResults=[];var bError=false;var nEnd=((this.responseStripAfter!=="")&&(oResponse.indexOf))?oResponse.indexOf(this.responseStripAfter):-1;if(nEnd!=-1){oResponse=oResponse.substring(0,nEnd);}switch(this.responseType){case YAHOO.widget.DS_XHR.TYPE_JSON:var jsonList,jsonObjParsed;var isNotMac=(navigator.userAgent.toLowerCase().indexOf("khtml")==-1);if(oResponse.parseJSON&&isNotMac){jsonObjParsed=oResponse.parseJSON();if(!jsonObjParsed){bError=true;}else{try{jsonList=eval("jsonObjParsed."+aSchema[0]);}catch(e){bError=true;break;}}}else{if(YAHOO.lang.JSON&&isNotMac){jsonObjParsed=YAHOO.lang.JSON.parse(oResponse);if(!jsonObjParsed){bError=true;break;}else{try{jsonList=eval("jsonObjParsed."+aSchema[0]);}catch(e){bError=true;break;}}}else{if(window.JSON&&isNotMac){jsonObjParsed=JSON.parse(oResponse);if(!jsonObjParsed){bError=true;break;}else{try{jsonList=eval("jsonObjParsed."+aSchema[0]);}catch(e){bError=true;break;}}}else{try{while(oResponse.substring(0,1)==" "){oResponse=oResponse.substring(1,oResponse.length);}if(oResponse.indexOf("{")<0){bError=true;break;}if(oResponse.indexOf("{}")===0){break;}var jsonObjRaw=eval("("+oResponse+")");if(!jsonObjRaw){bError=true;break;}jsonList=eval("(jsonObjRaw."+aSchema[0]+")");}catch(e){bError=true;break;}}}}if(!jsonList){bError=true;break;}if(!YAHOO.lang.isArray(jsonList)){jsonList=[jsonList];}for(var i=jsonList.length-1;i>=0;i--){var aResultItem=[];var jsonResult=jsonList[i];for(var j=aSchema.length-1;j>=1;j--){var dataFieldValue=jsonResult[aSchema[j]];
if(!dataFieldValue){dataFieldValue="";}aResultItem.unshift(dataFieldValue);}if(aResultItem.length==1){aResultItem.push(jsonResult);}aResults.unshift(aResultItem);}break;case YAHOO.widget.DS_XHR.TYPE_XML:var xmlList=oResponse.getElementsByTagName(aSchema[0]);if(!xmlList){bError=true;break;}for(var k=xmlList.length-1;k>=0;k--){var result=xmlList.item(k);var aFieldSet=[];for(var m=aSchema.length-1;m>=1;m--){var sValue=null;var xmlAttr=result.attributes.getNamedItem(aSchema[m]);if(xmlAttr){sValue=xmlAttr.value;}else{var xmlNode=result.getElementsByTagName(aSchema[m]);if(xmlNode&&xmlNode.item(0)&&xmlNode.item(0).firstChild){sValue=xmlNode.item(0).firstChild.nodeValue;}else{sValue="";}}aFieldSet.unshift(sValue);}aResults.unshift(aFieldSet);}break;case YAHOO.widget.DS_XHR.TYPE_FLAT:if(oResponse.length>0){var newLength=oResponse.length-aSchema[0].length;if(oResponse.substr(newLength)==aSchema[0]){oResponse=oResponse.substr(0,newLength);}var aRecords=oResponse.split(aSchema[0]);for(var n=aRecords.length-1;n>=0;n--){aResults[n]=aRecords[n].split(aSchema[1]);}}break;default:break;}sQuery=null;oResponse=null;oParent=null;if(bError){return null;}else{return aResults;}};YAHOO.widget.DS_XHR.prototype._oConn=null;YAHOO.widget.DS_ScriptNode=function(D,A,C){if(C&&(C.constructor==Object)){for(var B in C){this[B]=C[B];}}if(!YAHOO.lang.isArray(A)||!YAHOO.lang.isString(D)){return ;}this.schema=A;this.scriptURI=D;this._init();};YAHOO.widget.DS_ScriptNode.prototype=new YAHOO.widget.DataSource();YAHOO.widget.DS_ScriptNode.prototype.getUtility=YAHOO.util.Get;YAHOO.widget.DS_ScriptNode.prototype.scriptURI=null;YAHOO.widget.DS_ScriptNode.prototype.scriptQueryParam="query";YAHOO.widget.DS_ScriptNode.prototype.asyncMode="allowAll";YAHOO.widget.DS_ScriptNode.prototype.scriptCallbackParam="callback";YAHOO.widget.DS_ScriptNode.callbacks=[];YAHOO.widget.DS_ScriptNode._nId=0;YAHOO.widget.DS_ScriptNode._nPending=0;YAHOO.widget.DS_ScriptNode.prototype.doQuery=function(A,F,C){var B=this;if(YAHOO.widget.DS_ScriptNode._nPending===0){YAHOO.widget.DS_ScriptNode.callbacks=[];YAHOO.widget.DS_ScriptNode._nId=0;}var E=YAHOO.widget.DS_ScriptNode._nId;YAHOO.widget.DS_ScriptNode._nId++;YAHOO.widget.DS_ScriptNode.callbacks[E]=function(G){if((B.asyncMode!=="ignoreStaleResponses")||(E===YAHOO.widget.DS_ScriptNode.callbacks.length-1)){B.handleResponse(G,A,F,C);}else{}delete YAHOO.widget.DS_ScriptNode.callbacks[E];};YAHOO.widget.DS_ScriptNode._nPending++;var D=this.scriptURI+"&"+this.scriptQueryParam+"="+F+"&"+this.scriptCallbackParam+"=YAHOO.widget.DS_ScriptNode.callbacks["+E+"]";this.getUtility.script(D,{autopurge:true,onsuccess:YAHOO.widget.DS_ScriptNode._bumpPendingDown,onfail:YAHOO.widget.DS_ScriptNode._bumpPendingDown});};YAHOO.widget.DS_ScriptNode.prototype.handleResponse=function(oResponse,oCallbackFn,sQuery,oParent){var aSchema=this.schema;var aResults=[];var bError=false;var jsonList,jsonObjParsed;try{jsonList=eval("(oResponse."+aSchema[0]+")");}catch(e){bError=true;}if(!jsonList){bError=true;jsonList=[];}else{if(!YAHOO.lang.isArray(jsonList)){jsonList=[jsonList];}}for(var i=jsonList.length-1;i>=0;i--){var aResultItem=[];var jsonResult=jsonList[i];for(var j=aSchema.length-1;j>=1;j--){var dataFieldValue=jsonResult[aSchema[j]];if(!dataFieldValue){dataFieldValue="";}aResultItem.unshift(dataFieldValue);}if(aResultItem.length==1){aResultItem.push(jsonResult);}aResults.unshift(aResultItem);}if(bError){aResults=null;}if(aResults===null){this.dataErrorEvent.fire(this,oParent,sQuery,YAHOO.widget.DataSource.ERROR_DATAPARSE);aResults=[];}else{var resultObj={};resultObj.query=decodeURIComponent(sQuery);resultObj.results=aResults;this._addCacheElem(resultObj);this.getResultsEvent.fire(this,oParent,sQuery,aResults);}oCallbackFn(sQuery,aResults,oParent);};YAHOO.widget.DS_ScriptNode._bumpPendingDown=function(){YAHOO.widget.DS_ScriptNode._nPending--;};YAHOO.widget.DS_JSFunction=function(A,C){if(C&&(C.constructor==Object)){for(var B in C){this[B]=C[B];}}if(!YAHOO.lang.isFunction(A)){return ;}else{this.dataFunction=A;this._init();}};YAHOO.widget.DS_JSFunction.prototype=new YAHOO.widget.DataSource();YAHOO.widget.DS_JSFunction.prototype.dataFunction=null;YAHOO.widget.DS_JSFunction.prototype.doQuery=function(C,F,D){var B=this.dataFunction;var E=[];E=B(F);if(E===null){this.dataErrorEvent.fire(this,D,F,YAHOO.widget.DataSource.ERROR_DATANULL);return ;}var A={};A.query=decodeURIComponent(F);A.results=E;this._addCacheElem(A);this.getResultsEvent.fire(this,D,F,E);C(F,E,D);return ;};YAHOO.widget.DS_JSArray=function(A,C){if(C&&(C.constructor==Object)){for(var B in C){this[B]=C[B];}}if(!YAHOO.lang.isArray(A)){return ;}else{this.data=A;this._init();}};YAHOO.widget.DS_JSArray.prototype=new YAHOO.widget.DataSource();YAHOO.widget.DS_JSArray.prototype.data=null;YAHOO.widget.DS_JSArray.prototype.doQuery=function(E,I,A){var F;var C=this.data;var J=[];var D=false;var B=this.queryMatchContains;if(I){if(!this.queryMatchCase){I=I.toLowerCase();}for(F=C.length-1;F>=0;F--){var H=[];if(YAHOO.lang.isString(C[F])){H[0]=C[F];}else{if(YAHOO.lang.isArray(C[F])){H=C[F];}}if(YAHOO.lang.isString(H[0])){var G=(this.queryMatchCase)?encodeURIComponent(H[0]).indexOf(I):encodeURIComponent(H[0]).toLowerCase().indexOf(I);if((!B&&(G===0))||(B&&(G>-1))){J.unshift(H);}}}}else{for(F=C.length-1;F>=0;F--){if(YAHOO.lang.isString(C[F])){J.unshift([C[F]]);}else{if(YAHOO.lang.isArray(C[F])){J.unshift(C[F]);}}}}this.getResultsEvent.fire(this,A,I,J);E(I,J,A);};YAHOO.register("autocomplete",YAHOO.widget.AutoComplete,{version:"2.4.0",build:"733"});
 
/* ---- mp3Player ---- */
function startMp3Player(mp3FilePath,divName,startPlaying){
	mp3Player.addParam("allowScriptAccess", "sameDomain");
	mp3Player.addParam("menu", "false");
	mp3Player.addParam("quality", "high");
	mp3Player.addParam("align", "middle");
	mp3Player.addParam("wmode", "opaque");
	mp3Player.addVariable("beginPlaying", startPlaying);
	mp3Player.addVariable("mp3Path", mp3FilePath);
	mp3Player.write(divName);
}
function ivwLogMp3Player(){
	getCounters();
}
/* ---- video ---- */
if (!videoPrototype) {
    var videoPrototype = new Object();

    videoPrototype.TeaserViewer = function() {
        this.teaserWidth = null;
        this.teasersVisible = null;
        this.teasersTotal = null;
        this.teaserListId = null;
        this.teasersLeftButtonId = null;
        this.teasersRightButtonId = null;
        this.scrollFactor = null;
        this.inlinePlayerController = null;
        this.inlinePlayerId = null;
        this.teaserPosition = 0;
        this.teaserOffset = 0;
        this.teaserTargetOffset = 0;
    }

    videoPrototype.TeaserViewer.prototype = {
        setTeaserWidth: function(teaserWidth) {
            this.teaserWidth = teaserWidth;
        },
        setTeasersVisible: function(teasersVisible) {
            this.teasersVisible = teasersVisible;
        },
        setTeasersTotal: function(teasersTotal) {
            this.teasersTotal = teasersTotal;
        },
        setTeaserListId: function(teaserListId) {
            this.teaserListId = teaserListId;
        },
        setTeasersLeftButtonId: function(teasersLeftButtonId) {
            this.teasersLeftButtonId = teasersLeftButtonId;
        },
        setTeasersRightButtonId: function(teasersRightButtonId) {
            this.teasersRightButtonId = teasersRightButtonId;
        },
        setScrollFactor: function(scrollFactor) {
            this.scrollFactor = scrollFactor;
        },
        setInlinePlayerController: function(inlinePlayerController) {
            this.inlinePlayerController = inlinePlayerController;
        },
        setInlinePlayerId: function(inlinePlayerId) {
            this.inlinePlayerId = inlinePlayerId;
        },
        init: function() {
            this.updateButtons();
        },
        next: function() {
            if (this.teaserPosition < this.teasersTotal - this.teasersVisible) {
                this.teaserPosition++;
                this.teaserTargetOffset += this.teaserWidth;
                this.startScroll();
                this.updateButtons();
            }
        },
        previous: function() {
            if (this.teaserPosition > 0) {
                this.teaserPosition--;
                this.teaserTargetOffset -= this.teaserWidth;
                this.startScroll();
                this.updateButtons();
            }
        },
        playVideo: function(videoClipUrl, videoArticleUrl, channelName, sectionName, sectionUniqueName, adChannelName) {
            if (this.inlinePlayerController) {
                this.inlinePlayerController.playVideo(videoClipUrl, videoArticleUrl, channelName, sectionName, sectionUniqueName, adChannelName);
            }
        },
        startScroll: function() {
            setTimeout(this.delegate(this, this.step), 40);
        },
        step: function() {
            var doNextStep = false;

            if (Math.abs(this.teaserTargetOffset - this.teaserOffset) > 1) {
                this.teaserOffset += (this.teaserTargetOffset - this.teaserOffset) * this.scrollFactor;
                doNextStep = true;
            } else {
                this.teaserOffset = this.teaserTargetOffset;
            }

            var teaserListElement = document.getElementById(this.teaserListId);
            teaserListElement.style.marginLeft = "" + (0 - Math.round(this.teaserOffset)) + "px";

            if (doNextStep) {
                setTimeout(this.delegate(this, this.step), 40);
            }
        },
        updateButtons: function() {
            var leftButtonElement = document.getElementById(this.teasersLeftButtonId);
            if (this.teaserPosition > 0) {
                leftButtonElement.className = "videoTeaserButtonActive";
            } else {
                leftButtonElement.className = "videoTeaserButtonInactive";
            }

            var rightButtonElement = document.getElementById(this.teasersRightButtonId);
            if (this.teaserPosition < this.teasersTotal - this.teasersVisible) {
                rightButtonElement.className = "videoTeaserButtonActive";
            } else {
                rightButtonElement.className = "videoTeaserButtonInactive";
            }
        },
        delegate: function(obj, objMethod) {
            return function() {
                return objMethod.call(obj, arguments);
            }
        }
    }

    videoPrototype.SectionBox = function() {
        this.sectionBoxItems = new Array();
        this.selectedSectionId = null;
        this.targetElementId = null;
    }

    videoPrototype.SectionBox.prototype = {
        setTargetElementId: function(targetElementId) {
            this.targetElementId = targetElementId;
        },
        addItem: function(elementId, sectionId, sectionUrl, sectionElementId, isSelectedSection) {
            var sectionBoxItem = new videoPrototype.SectionBoxItem(elementId, sectionId, sectionUrl, sectionElementId);
            this.sectionBoxItems.push(sectionBoxItem);
            if (isSelectedSection) this.selectedSectionId = sectionId;
        },
        openSection: function(sectionId) {
            if (sectionId != this.selectedSectionId) {
                for (var i = 0; i < this.sectionBoxItems.length; i++) {
                    var sectionBoxItem = this.sectionBoxItems[i];
                    var element = document.getElementById(sectionBoxItem.elementId);
                    element.className = element.className.replace("videoSectionBoxItemSelected", "videoSectionBoxItemNotSelected");
                    element.className = element.className.replace("videoSectionBoxSubItemSelected", "videoSectionBoxSubItemNotSelected");

                    var sectionElement = document.getElementById(sectionBoxItem.sectionElementId);
                    sectionElement.className = sectionElement.className.replace(" videoSectionBoxItemOpen", "");
                }

                for (var i = 0; i < this.sectionBoxItems.length; i++) {
                    var sectionBoxItem = this.sectionBoxItems[i];
                    var element = document.getElementById(sectionBoxItem.elementId);

                    if (sectionBoxItem.sectionId == sectionId) {
                        element.className = element.className.replace("videoSectionBoxItemNotSelected", "videoSectionBoxItemSelected");
                        element.className = element.className.replace("videoSectionBoxSubItemNotSelected", "videoSectionBoxSubItemSelected");
                        ajaxLoad(sectionBoxItem.sectionUrl + '?service=Ajax&type=videoSectionBrowse', this.targetElementId);
                    }

                    var sectionElement = document.getElementById(sectionBoxItem.sectionElementId);

                    if (sectionBoxItem.sectionId == sectionId) {
                        sectionElement.className += " videoSectionBoxItemOpen";
                    }
                }
                this.selectedSectionId = sectionId;

                getIVW();
            }
        }
    }

    videoPrototype.SectionBoxItem = function(elementId, sectionId, sectionUrl, sectionElementId) {
        this.elementId = elementId;
        this.sectionId = sectionId;
        this.sectionUrl = sectionUrl;
        this.sectionElementId = sectionElementId;
    }

    videoPrototype.TabController = function() {
        this.tabs = new Object();
        this.activeTab = null;
        this.dropDownBoxElementId = null;
        this.dropDownAreaElementId = null;
        this.dropDownContentElementId = null;
        this.contentHeight = null;
        this.scrollFactor = null;
        this.contentOffset = null;
        this.targetOffset = null;
        this.notifiedElementIds = new Array();
    }

    videoPrototype.TabController.prototype = {
        setTab: function(name, elementId) {
            if (this.tabs[name] == null) {
                var tabItem = new videoPrototype.TabItem(name);
                tabItem.tabElementId = elementId;
                this.tabs[name] = tabItem;
            } else {
                this.tabs[name].tabElementId = elementId;
            }
        },
        setTabExtension: function(name, elementId) {
            if (this.tabs[name] == null) {
                var tabItem = new videoPrototype.TabItem(name);
                tabItem.tabExtensionElementId = elementId;
                this.tabs[name] = tabItem;
            } else {
                this.tabs[name].tabExtensionElementId = elementId;
            }
        },
        setTabContent: function(name, elementId) {
            if (this.tabs[name] == null) {
                var tabItem = new videoPrototype.TabItem(name);
                tabItem.tabContentElementId = elementId;
                this.tabs[name] = tabItem;
            } else {
                this.tabs[name].tabContentElementId = elementId;
            }
            document.getElementById(elementId).style.display = "none";
        },
        setTabOnOpenFunction: function(name, onOpenFunction) {
            if (this.tabs[name] == null) {
                var tabItem = new videoPrototype.TabItem(name);
                tabItem.tabOnOpenFunction = onOpenFunction;
                this.tabs[name] = tabItem;
            } else {
                this.tabs[name].tabOnOpenFunction = onOpenFunction;
            }
        },
        setTabOnCloseFunction: function(name, onCloseFunction) {
            if (this.tabs[name] == null) {
                var tabItem = new videoPrototype.TabItem(name);
                tabItem.tabOnCloseFunction = onCloseFunction;
                this.tabs[name] = tabItem;
            } else {
                this.tabs[name].tabOnCloseFunction = onCloseFunction;
            }
        },
        setDropDownBox: function(elementId) {
            this.dropDownBoxElementId = elementId;
        },
        setDropDownArea: function(elementId) {
            this.dropDownAreaElementId = elementId;
        },
        setDropDownContent: function(elementId) {
            this.dropDownContentElementId = elementId;
        },
        setContentHeight: function(contentHeight) {
            this.contentHeight = contentHeight;
            this.contentOffset = 0;
        },
        setScrollFactor: function(scrollFactor) {
            this.scrollFactor = scrollFactor;
        },
        addNotifiedElementId: function(elementId) {
            this.notifiedElementIds.push(elementId);
        },
        open: function(name) {
            var targetTab = this.tabs[name];
            if (targetTab != null) {
                if (targetTab != this.activeTab) {

                    if (this.activeTab != null) {
                        this.deactivateTabs();
                    }

                    var tabElement = document.getElementById(targetTab.tabElementId);
                    if (tabElement != null) {
                        tabElement.className = tabElement.className.replace(" videoLinkTabInactive", " videoLinkTabActive");
                    }

                    var tabExtensionElement = document.getElementById(targetTab.tabExtensionElementId);
                    if (tabExtensionElement != null) {
                        tabExtensionElement.className = tabExtensionElement.className.replace(" videoDropDownTabInactive", " videoDropDownTabActive");
                    }

                    var tabContentElement = document.getElementById(targetTab.tabContentElementId);
                    if (tabContentElement != null) {
                        tabContentElement.style.display = "block";
                    }

                    if (targetTab.tabOnOpenFunction != null) {
                        targetTab.tabOnOpenFunction();
                    }

                    this.targetOffset = this.contentHeight;
                    this.startScroll();

                    this.activeTab = targetTab;
                    getIVW();
                } else {
                    this.close();
                }
            }
        },
        close: function() {
            this.activeTab = null;

            this.targetOffset = 0;
            this.startScroll();
            
            getIVW();
        },
        deactivateTabs: function() {
            for (var tabName in this.tabs) {
                var tab = this.tabs[tabName];

                var tabElement = document.getElementById(tab.tabElementId);
                if (tabElement != null) {
                    tabElement.className = tabElement.className.replace(" videoLinkTabActive", " videoLinkTabInactive");
                }

                var tabExtensionElement = document.getElementById(tab.tabExtensionElementId);
                if (tabExtensionElement != null) {
                    tabExtensionElement.className = tabExtensionElement.className.replace(" videoDropDownTabActive", " videoDropDownTabInactive");
                }

                var tabContentElement = document.getElementById(tab.tabContentElementId);
                if (tabContentElement != null) {
                    tabContentElement.style.display = "none";
                }

                if (tab.tabOnCloseFunction != null) {
                    tab.tabOnCloseFunction();
                }
            }
        },
        startScroll: function() {
            for (var i = 0; i < this.notifiedElementIds.length; i++) {
                var notifiedElementId = this.notifiedElementIds[i];
                var notifiedElement = document.getElementById(notifiedElementId);
                if (notifiedElement != null) {
                    notifiedElement.className += " videoDropDownScroll";
                }
            }

            setTimeout(this.delegate(this, this.step), 40);
        },
        step: function() {
            var doNextStep = false;

            if (Math.abs((this.targetOffset - this.contentOffset) * this.scrollFactor) > 1) {
                this.contentOffset += (this.targetOffset - this.contentOffset) * this.scrollFactor;
                doNextStep = true;
            } else {
                for (var i = 0; i < this.notifiedElementIds.length; i++) {
                    var notifiedElementId = this.notifiedElementIds[i];
                    var notifiedElement = document.getElementById(notifiedElementId);
                    if (notifiedElement != null) {
                        notifiedElement.className = notifiedElement.className.replace(" videoDropDownScroll", "");
                    }
                }

                this.contentOffset = this.targetOffset;
            }

            var dropDownBoxElement = document.getElementById(this.dropDownBoxElementId);
            if (this.contentOffset == 0) {
                dropDownBoxElement.style.display = "none";
                this.deactivateTabs();
            } else {
                dropDownBoxElement.style.display = "block";
            }

            var dropDownContentElement = document.getElementById(this.dropDownContentElementId);
            dropDownContentElement.style.marginTop = "" + (Math.round(this.contentOffset) - this.contentHeight) + "px";

            var dropDownAreaElement = document.getElementById(this.dropDownAreaElementId);
            dropDownAreaElement.style.height = "" + (Math.round(this.contentOffset)) + "px";

            if (doNextStep) {
                setTimeout(this.delegate(this, this.step), 40);
            }
        },
        delegate: function(obj, objMethod) {
            return function() {
                return objMethod.call(obj, arguments);
            }
        }
    }

    videoPrototype.TabItem = function(name) {
        this.name = name;
        this.tabElementId = null;
        this.tabExtensionElementId = null;
        this.tabContentElementId = null;
        this.tabOnOpenFunction = null;
        this.tabOnCloseFunction = null;
    }

    videoPrototype.VideoComment = function() {
        this.formElementId = null;
        this.boxElementId = null;
        this.messageElementId = null;
        this.bodyElementId = null;
        this.nameElementId = null;
        this.emailElementId = null;
        this.captchaElementId = null;
        this.targetElementId = null;
        this.captchaImageId = null;
    }

    videoPrototype.VideoComment.prototype = {
        setFormElementId: function(formElementId) {
            this.formElementId = formElementId;
        },
        setBoxElementId: function(boxElementId) {
            this.boxElementId = boxElementId;
        },
        setMessageElementId: function(messageElementId) {
            this.messageElementId = messageElementId;
        },
        setBodyElementId: function(bodyElementId) {
            this.bodyElementId = bodyElementId;
        },
        setNameElementId: function(nameElementId) {
            this.nameElementId = nameElementId;
        },
        setEmailElementId: function(emailElementId) {
            this.emailElementId = emailElementId;
        },
        setCaptchaElementId: function(captchaElementId) {
            this.captchaElementId = captchaElementId;
        },
        setTargetElementId: function(targetElementId) {
            this.targetElementId = targetElementId;
        },
        setCaptchaImageId: function(captchaImageId) {
            this.captchaImageId = captchaImageId;
        },
        sendCommentForm: function() {
            var result = true;

            this.clearErrors();

            try
            {
                var errorMessageActive = false;

                if (document.getElementById(this.bodyElementId).value == "") {
                    document.getElementById(this.bodyElementId).className += " videoFormError";
                    document.getElementById(this.messageElementId).innerHTML += getTranslation("video.comment.fill") + "<br/>";
                    errorMessageActive = true;
                    result = false;
                } else {
                    this.setCookie("commentBody", document.getElementById(this.bodyElementId).value);
                }

                if (document.getElementById(this.nameElementId).value == "") {
                    document.getElementById(this.nameElementId).className += " videoFormError";
                    document.getElementById(this.messageElementId).innerHTML += getTranslation("video.fill.yourname") + "<br/>";
                    errorMessageActive = true;
                    result = false;
                } else {
                    this.setCookie("commentName", document.getElementById(this.nameElementId).value);
                }

                if (document.getElementById(this.emailElementId).value == "") {
                    document.getElementById(this.emailElementId).className += " videoFormError";
                    document.getElementById(this.messageElementId).innerHTML += getTranslation("video.fill.youremail") + "<br/>";
                    errorMessageActive = true;
                    result = false;
                }
                else if (!this.checkEmail(document.getElementById(this.emailElementId).value)) {
                    document.getElementById(this.emailElementId).className += " videoFormError";
                    document.getElementById(this.messageElementId).innerHTML += getTranslation("video.your.wrongemail") + "<br/>";
                    errorMessageActive = true;
                    result = false;
                } else {
                    this.setCookie("commentEmail", document.getElementById(this.emailElementId).value);
                }

                // HM: added check for SSO-USER
                if (document.getElementById(this.captchaElementId).value == "" && !getSSOUserId())
                {
                    document.getElementById(this.captchaElementId).className += " videoFormError";
                    document.getElementById(this.messageElementId).innerHTML += getTranslation("video.fill.code") + "<br/>";
                    errorMessageActive = true;
                    result = false;
                }

                if (errorMessageActive) {
                    document.getElementById(this.messageElementId).className += " videoPostMessagesError";
                    document.getElementById(this.boxElementId).className += " videoFormShowMessage";
                }
            }
            catch(e) {}

            if (result) {
                var data = "";

                var formElement = document.getElementById(this.formElementId);
                for (var i = 0; i < formElement.elements.length; i++) {
                    var field = formElement.elements[i];
                    if (i > 0) data += "&";
                    data += field.name + "=" + encodeURIComponent(field.value);
                }

                var postUrl = formElement.action;

                ajaxLoad(postUrl, this.targetElementId, "POST", data, this.delegate(this, this.checkResult));
            }
        },
        checkResult: function() {
            var showMessage = false;
            var isError = false;
            var captchaError = false;
            var resultMessage = "";

            var result = document.getElementById(this.messageElementId).innerHTML;
            if (result == "kommentarSuccess") {
                showMessage = true;
                resultMessage = getTranslation("video.comment.success");

                getIVW();
            } else if (result == "kommentarError") {
                showMessage = true;
                isError = true;
                resultMessage = getTranslation("video.comment.error");
            } else if (result == "kommentarCodeExpired") {
                showMessage = true;
                isError = true;
                captchaError = true;
                resultMessage = getTranslation("video.invalid.code");
            } else if (result == "kommentarCodeFalsch") {
                showMessage = true;
                isError = true;
                captchaError = true;
                resultMessage = getTranslation("video.wrong.code");
            }

            this.clearErrors();
            document.getElementById(this.messageElementId).innerHTML = resultMessage;
            if (showMessage) document.getElementById(this.boxElementId).className += " videoFormShowMessage";
            if (isError) document.getElementById(this.messageElementId).className += " videoPostMessagesError";
            if (captchaError) document.getElementById(this.captchaElementId).className += " videoFormError";

            if (isError)
            {	this.restoreFields();
            }
            else
            {	// show user values again if sso user
            	var isSSOUser = getSSOUserId()
            	var userName;
            	var email;
            	
            	if (isSSOUser)
            	{	userName = this.getCookie("commentName");
            		email = this.getCookie("commentEmail");
            	}
            	
            	this.clearFields();
            
            	if (isSSOUser)
            	{	videoCommentUserData(userName, email);            	
            	}
            }

            var imageElement = document.getElementById(this.captchaImageId);
            if (imageElement) {
                imageElement.src = "/captcha/captcha.jpg?nocache=" + Math.floor(Math.random() * 9000000 + 1000000);
            }
        },
        clearErrors: function() {
            document.getElementById(this.boxElementId).className = document.getElementById(this.boxElementId).className.replace(" videoFormShowMessage", "");
            document.getElementById(this.messageElementId).innerHTML = "";
            document.getElementById(this.messageElementId).className = document.getElementById(this.messageElementId).className.replace(" videoPostMessagesError", "");
            document.getElementById(this.bodyElementId).className = document.getElementById(this.bodyElementId).className.replace(" videoFormError", "");
            document.getElementById(this.nameElementId).className = document.getElementById(this.nameElementId).className.replace(" videoFormError", "");
            document.getElementById(this.emailElementId).className = document.getElementById(this.emailElementId).className.replace(" videoFormError", "");
            document.getElementById(this.captchaElementId).className = document.getElementById(this.captchaElementId).className.replace(" videoFormError", "");
        },
        restoreFields: function() {
            document.getElementById(this.bodyElementId).value = this.getCookie("commentBody");
            document.getElementById(this.nameElementId).value = this.getCookie("commentName");
            document.getElementById(this.emailElementId).value = this.getCookie("commentEmail");
        },
        clearFields: function() {
            document.getElementById(this.bodyElementId).value = "";
            document.getElementById(this.nameElementId).value = "";
            document.getElementById(this.emailElementId).value = "";
            this.setCookie("commentBody", "");
            this.setCookie("commentName", "");
            this.setCookie("commentEmail", "");
        },
        clearForm: function() {
            this.clearFields();
            this.clearErrors();
        },
        onOpen: function() {
            var imageElement = document.getElementById(this.captchaImageId);
            if (imageElement) {
                imageElement.src = "/captcha/captcha.jpg?nocache=" + Math.floor(Math.random() * 9000000 + 1000000);
            }
        },
        delegate: function(obj, objMethod) {
            return function() {
                return objMethod.call(obj, arguments);
            }
        },
        setCookie: function(cookieName, value) {
            document.cookie = cookieName + "=" + encodeURIComponent(value);
        },
        getCookie: function(cookieName) {
            if (document.cookie.length > 0) {
                var cookieStart = document.cookie.indexOf(cookieName + "=");
                if (cookieStart != -1) {
                    cookieStart = cookieStart + cookieName.length + 1;
                    var cookieEnd = document.cookie.indexOf(";", cookieStart);
                    if (cookieEnd == -1) cookieEnd = document.cookie.length;
                    return decodeURIComponent(document.cookie.substring(cookieStart, cookieEnd));
                }
            }
            return "";
        },
        checkEmail: function(emailAddress) {
            var at = "@";
            var dot = ".";
            var lat = emailAddress.indexOf(at);
            var lstr = emailAddress.length;
            var ldot = emailAddress.indexOf(dot);

            if (emailAddress.indexOf(at) == -1) return false;
            if (emailAddress.indexOf(at) == -1 || emailAddress.indexOf(at) == 0 || emailAddress.indexOf(at) == lstr) return false;
            if (emailAddress.indexOf(dot) == -1 || emailAddress.indexOf(dot) == 0 || emailAddress.indexOf(dot) == lstr) return false;
            if (emailAddress.indexOf(at, (lat + 1)) != -1) return false;
            if (emailAddress.substring(lat - 1, lat) == dot || emailAddress.substring(lat + 1, lat + 2) == dot) return false;
            if (emailAddress.indexOf(dot, (lat + 2)) == -1) return false;
            if (emailAddress.indexOf(" ") != -1) return false;

            return true;
        }
    }

    videoPrototype.VideoSend = function() {
        this.formElementId = null;
        this.boxElementId = null;
        this.previewElementId = null;
        this.messageElementId = null;
        this.addressesElementId = null;
        this.nameElementId = null;
        this.emailElementId = null;
        this.captchaElementId = null;
        this.targetElementId = null;
        this.captchaImageId = null;
        this.topic = null;
        this.title = null;
        this.intro = null;
        this.permaLink = null;
    }

    videoPrototype.VideoSend.prototype = {
        setFormElementId: function(formElementId) {
            this.formElementId = formElementId;
        },
        setBoxElementId: function(boxElementId) {
            this.boxElementId = boxElementId;
        },
        setPreviewElementId: function(previewElementId) {
            this.previewElementId = previewElementId;
        },
        setMessageElementId: function(messageElementId) {
            this.messageElementId = messageElementId;
        },
        setAddressesElementId: function(addressesElementId) {
            this.addressesElementId = addressesElementId;
        },
        setNameElementId: function(nameElementId) {
            this.nameElementId = nameElementId;
        },
        setEmailElementId: function(emailElementId) {
            this.emailElementId = emailElementId;
        },
        setCaptchaElementId: function(captchaElementId) {
            this.captchaElementId = captchaElementId;
        },
        setTargetElementId: function(targetElementId) {
            this.targetElementId = targetElementId;
        },
        setCaptchaImageId: function(captchaImageId) {
            this.captchaImageId = captchaImageId;
        },
        setTopic: function(topic) {
            this.topic = topic;
        },
        setTitle: function(title) {
            this.title = title;
        },
        setIntro: function(intro) {
            this.intro = intro;
        },
        setPermaLink: function(permaLink) {
            this.permaLink = permaLink;
        },
        sendForm: function() {
            var result = true;

            this.clearErrors();

            try
            {
                var errorMessageActive = false;

                if (document.getElementById(this.addressesElementId).value == "") {
                    document.getElementById(this.addressesElementId).className += " videoFormError";
                    //document.getElementById(this.messageElementId).innerHTML += "F&uuml;llen Sie bitte das Feld 'Dieses Video senden an' aus.<br />";
                    document.getElementById(this.messageElementId).innerHTML += getTranslation("video.fillreceipent.email");
                    errorMessageActive = true;
                    result = false;
                } else {
                    var addressesError = false;
                    var addresses = document.getElementById(this.addressesElementId).value.split(",");
                    for (var i = 0; i < addresses.length; i++) {
                        var address = addresses[i].replace(/^\s+|\s+$/g, "");
                        if (!this.checkEmail(address)) {
                            document.getElementById(this.addressesElementId).className += " videoFormError";
                            //document.getElementById(this.messageElementId).innerHTML += "Die Empf&auml;nger-Email ist nicht g&uuml;ltig.<br />";
                            document.getElementById(this.messageElementId).innerHTML += getTranslation("video.wrongreceipent.email");
                            errorMessageActive = true;
                            break;
                        }
                    }

                    if (!addressesError) {
                        this.setCookie("sendAddresses", document.getElementById(this.addressesElementId).value);
                    }
                }

                if (document.getElementById(this.nameElementId).value == "") {
                    document.getElementById(this.nameElementId).className += " videoFormError";
                    //document.getElementById(this.messageElementId).innerHTML += "F&uuml;llen Sie bitte das Feld 'Ihr Name' aus.<br />";
                    document.getElementById(this.messageElementId).innerHTML += getTranslation("video.fill.yourname");
                    errorMessageActive = true;
                    result = false;
                } else {
                    this.setCookie("sendName", document.getElementById(this.nameElementId).value);
                }

                if (document.getElementById(this.emailElementId).value == "") {
                    document.getElementById(this.emailElementId).className += " videoFormError";
                    //document.getElementById(this.messageElementId).innerHTML += "F&uuml;llen Sie bitte das Feld 'Ihre E-mail' aus.<br />";
                    document.getElementById(this.messageElementId).innerHTML += getTranslation("video.fill.youremail");
                    errorMessageActive = true;
                    result = false;
                }
                else if (!this.checkEmail(document.getElementById(this.emailElementId).value)) {
                    document.getElementById(this.emailElementId).className += " videoFormError";
                    //document.getElementById(this.messageElementId).innerHTML += "Ihre E-mail is nicht g&uuml;ltig.<br />";
                    document.getElementById(this.messageElementId).innerHTML += getTranslation("video.your.wrongemail");
                    errorMessageActive = true;
                    result = false;
                } else {
                    this.setCookie("sendEmail", document.getElementById(this.emailElementId).value);
                }

                if (document.getElementById(this.captchaElementId).value == "") {
                    document.getElementById(this.captchaElementId).className += " videoFormError";
                    //document.getElementById(this.messageElementId).innerHTML += "F&uuml;llen Sie bitte das Feld 'Code' aus.<br />";
                    document.getElementById(this.messageElementId).innerHTML += getTranslation("video.fill.code");
                    errorMessageActive = true;
                    result = false;
                }

                if (errorMessageActive) {
                    document.getElementById(this.messageElementId).className += " videoPostMessagesError";
                    document.getElementById(this.boxElementId).className += " videoFormShowMessage";
                }
            }
            catch(e) {}

            if (result) {
                var data = "";

                var formElement = document.getElementById(this.formElementId);
                for (var i = 0; i < formElement.elements.length; i++) {
                    var field = formElement.elements[i];
                    if (i > 0) data += "&";
                    data += field.name + "=" + encodeURIComponent(field.value);
                }

                var postUrl = formElement.action;

                ajaxLoad(postUrl, this.targetElementId, "POST", data, this.delegate(this, this.checkResult));
            }
        },
        checkResult: function() {
            var showMessage = false;
            var isError = false;
            var captchaError = false;
            var resultMessage = "";

            var result = document.getElementById(this.messageElementId).innerHTML;
            if (result == "recommendation_success") {
                showMessage = true;
                resultMessage = getTranslation("video.sent");

                getIVW();
            } else if (result == "recommendation_failure") {
                showMessage = true;
                isError = true;
                resultMessage = getTranslation("form.video.notSend");
            } else if (result == "recommendationCodeExpired") {
                showMessage = true;
                isError = true;
                captchaError = true;
                resultMessage = getTranslation("video.invalid.code");
            } else if (result == "recommendationCodeFalsch") {
                showMessage = true;
                isError = true;
                captchaError = true;
                resultMessage = getTranslation("video.wrong.code");
            }

            this.clearErrors();
            document.getElementById(this.messageElementId).innerHTML = resultMessage;
            if (showMessage) document.getElementById(this.boxElementId).className += " videoFormShowMessage";
            if (isError) document.getElementById(this.messageElementId).className += " videoPostMessagesError";
            if (captchaError) document.getElementById(this.captchaElementId).className += " videoFormError";

            if (isError) this.restoreFields();
            else this.clearFields();

            var imageElement = document.getElementById(this.captchaImageId);
            if (imageElement) {
                imageElement.src = "/captcha/captcha.jpg?nocache=" + Math.floor(Math.random() * 9000000 + 1000000);
            }
            this.updatePreview();
        },
        clearErrors: function() {
            document.getElementById(this.boxElementId).className = document.getElementById(this.boxElementId).className.replace(" videoFormShowMessage", "");
            document.getElementById(this.messageElementId).innerHTML = "";
            document.getElementById(this.messageElementId).className = document.getElementById(this.messageElementId).className.replace(" videoPostMessagesError", "");
            document.getElementById(this.addressesElementId).className = document.getElementById(this.addressesElementId).className.replace(" videoFormError", "");
            document.getElementById(this.nameElementId).className = document.getElementById(this.nameElementId).className.replace(" videoFormError", "");
            document.getElementById(this.emailElementId).className = document.getElementById(this.emailElementId).className.replace(" videoFormError", "");
            document.getElementById(this.captchaElementId).className = document.getElementById(this.captchaElementId).className.replace(" videoFormError", "");
        },
        restoreFields: function() {
            document.getElementById(this.addressesElementId).value = this.getCookie("sendAddresses");
            document.getElementById(this.nameElementId).value = this.getCookie("sendName");
            document.getElementById(this.emailElementId).value = this.getCookie("sendEmail");
        },
        clearFields: function() {
            document.getElementById(this.addressesElementId).value = "";
            document.getElementById(this.nameElementId).value = "";
            document.getElementById(this.emailElementId).value = "";
            this.setCookie("sendAddresses", "");
            this.setCookie("sendName", "");
            this.setCookie("sendEmail", "");
        },
        clearForm: function() {
            this.clearFields();
            this.clearErrors();
        },
        updatePreview: function() {
            var name = document.getElementById(this.nameElementId).value;
            if (name == "") name = getTranslation("video.yourname");

            var email = document.getElementById(this.emailElementId).value;
            if (email == "") email = getTranslation("video.youremail");

            var preview = name + " (" + email + ") " + getTranslation("video.received.videolink") + "<br />";
            preview += "------------------------------------------------<br />";
            if (this.topic != "") preview += this.topic + "<br /><br />";
            if (this.title != "") preview += this.title + "<br /><br />";
            if (this.intro != "") preview += this.intro + "<br /><br />";
            preview += getTranslation("video.see.here") + "<br />";

            var permaLinkChunk = this.permaLink;
            while (permaLinkChunk.length > 50) {
                var index = permaLinkChunk.lastIndexOf("/", 50);
                if (index == -1) {
                    break;
                } else {
                    preview += permaLinkChunk.substr(0, index + 1) + "<br />";
                    permaLinkChunk = permaLinkChunk.substr(index + 1);
                }
            }
            preview += permaLinkChunk + "<br />";

            preview += "<br />";
            preview += getTranslation("video.data.notsaved") + "<br/>";
            preview += "------------------------------------------------<br />";
            preview += getTranslation("video.misuse");

            document.getElementById(this.previewElementId).innerHTML = preview;
        },
        wrapAddresses: function() {
            document.getElementById(this.addressesElementId).value = document.getElementById(this.addressesElementId).value.replace(/,\s*/g, ", ");
        },
        onOpen: function() {
            var imageElement = document.getElementById(this.captchaImageId);
            if (imageElement) {
                imageElement.src = "/captcha/captcha.jpg?nocache=" + Math.floor(Math.random() * 9000000 + 1000000);
            }
            this.updatePreview();
        },
        delegate: function(obj, objMethod) {
            return function() {
                return objMethod.call(obj, arguments);
            }
        },
        setCookie: function(cookieName, value) {
            document.cookie = cookieName + "=" + encodeURIComponent(value);
        },
        getCookie: function(cookieName) {
            if (document.cookie.length > 0) {
                var cookieStart = document.cookie.indexOf(cookieName + "=");
                if (cookieStart != -1) {
                    cookieStart = cookieStart + cookieName.length + 1;
                    var cookieEnd = document.cookie.indexOf(";", cookieStart);
                    if (cookieEnd == -1) cookieEnd = document.cookie.length;
                    return decodeURIComponent(document.cookie.substring(cookieStart, cookieEnd));
                }
            }
            return "";
        },
        checkEmail: function(emailAddress) {
            var at = "@";
            var dot = ".";
            var lat = emailAddress.indexOf(at);
            var lstr = emailAddress.length;
            var ldot = emailAddress.indexOf(dot);

            if (emailAddress.indexOf(at) == -1) return false;
            if (emailAddress.indexOf(at) == -1 || emailAddress.indexOf(at) == 0 || emailAddress.indexOf(at) == lstr) return false;
            if (emailAddress.indexOf(dot) == -1 || emailAddress.indexOf(dot) == 0 || emailAddress.indexOf(dot) == lstr) return false;
            if (emailAddress.indexOf(at, (lat + 1)) != -1) return false;
            if (emailAddress.substring(lat - 1, lat) == dot || emailAddress.substring(lat + 1, lat + 2) == dot) return false;
            if (emailAddress.indexOf(dot, (lat + 2)) == -1) return false;
            if (emailAddress.indexOf(" ") != -1) return false;

            return true;
        }
    }

    videoPrototype.VideoInline = function() {
        this.swfObject = null;
        this.inlinePlayerDivId = null;
        this.inlinePlayerId = null;
    }

    videoPrototype.VideoInline.prototype = {
        setSwfObject: function(swfObject) {
            this.swfObject = swfObject;
        },
        setInlinePlayerDivId: function(inlinePlayerDivId) {
            this.inlinePlayerDivId = inlinePlayerDivId;
        },
        setInlinePlayerId: function(inlinePlayerId) {
            this.inlinePlayerId = inlinePlayerId;
        },
        playVideo: function(videoClipUrl, videoArticleUrl, channelName, sectionName, sectionUniqueName, adChannelName) {
            var inlinePlayerElement = document.getElementById(this.inlinePlayerId);
            if (inlinePlayerElement) {
                inlinePlayerElement.setAutoPlay("true");
                inlinePlayerElement.playClip(videoClipUrl, videoArticleUrl, channelName, sectionName, sectionUniqueName, adChannelName);
            } else {
                this.swfObject.addVariable("mediaclip", escape(videoClipUrl));
                this.swfObject.addVariable("deeplinkUrl", escape(videoArticleUrl));
                this.swfObject.addVariable("channelName", escape(channelName));
                this.swfObject.addVariable("sectionName", escape(sectionName));
                this.swfObject.addVariable("sectionUniqueName", escape(sectionUniqueName));
                this.swfObject.addVariable("adChannelName", escape(adChannelName));
                this.swfObject.addVariable("autoPlay", "true");
                this.swfObject.write(this.inlinePlayerDivId);
            }
        }
    }

    videoPrototype.VideoStats = function() {
    }

    videoPrototype.VideoStats.prototype = {
        logStats: function(parameters) {
            var ivwUrl = "http://welt.ivwbox.de/cgi-bin/ivw/CP/" + parameters.channelName;
            ivwUrl += "?r=" + escape(location.href) + "&d=" + (Math.random() * 100000);

            var videoIVWStats = document.getElementById("videoIVWStats");
            if (videoIVWStats == null) {
                videoIVWStats = document.createElement("img");
            }
            videoIVWStats.src = ivwUrl;

            var subChannel = "";
            if (parameters.sectionName != parameters.channelName) subChannel = parameters.sectionName;

            var dateString = "";
            if (parameters.pubdate != null) {
                dateString = parameters.pubdate.substr(8, 2);
                dateString += "." + parameters.pubdate.substr(5, 2);
                dateString += "." + parameters.pubdate.substr(0, 4);
            }

            s.pageName = escape(parameters.sectionName) + ":" + parameters.id + ":" + escape(parameters.title);
            s.server = "";
            s.pageURL = escape(parameters.deeplinkUrl);
            s.referrer = escape(location.href);
            s.channel = escape(parameters.channelName);
            s.pageType = "";
            s.prop1 = subChannel;
            s.prop2 = "welt";
            s.prop3 = parameters.id;
            s.prop4 = dateString;
            s.prop5 = "story";
            s.prop6 = "";
            s.prop7 = "image";
            s.prop8 = "";
            s.prop9 = "";
            s.campaign = "";
            s.state = "";
            s.zip = "";
            s.events = "";
            s.products = "";
            s.purchaseID = "";
            s.eVar1 = "";
            s.eVar2 = "";
            s.eVar3 = "";
            s.eVar4 = "";
            s.eVar5 = "";
            var s_code=s.t();
            if (s_code) document.write(s_code);
        }
    }

    videoPrototype.VideoEmbed = function() {
        this.playerId = null;
        this.instanceName = null;
        this.siteUrl = null;
        this.publicationName = "welt";
        this.adFunctionBody = "";
        this.embedPlayer = null;
        this.teaserViewer = null;
        this.videoInline = null;
        this.videos = new Array();
    }

    videoPrototype.VideoEmbed.prototype = {
        setPlayerId: function(playerId) {
            this.playerId = playerId;
        },
        setInstanceName: function(instanceName) {
            this.instanceName = instanceName;
        },
        setSiteUrl: function(siteUrl) {
            this.siteUrl = siteUrl;
        },
        addVideo: function(videoId, title, deeplinkUrl, thumbnailUrl, channelName, sectionName, sectionUniqueName, adChannelName) {
            var videoObject = new Object();
            videoObject.videoId = videoId;
            videoObject.title = title;
            videoObject.deeplinkUrl = deeplinkUrl;
            videoObject.thumbnailUrl = thumbnailUrl;
            videoObject.channelName = channelName;
            videoObject.sectionName = sectionName;
            videoObject.sectionUniqueName = sectionUniqueName;
            videoObject.adChannelName = adChannelName;
            this.videos.push(videoObject);
        },
        playVideo: function(videoClipUrl, videoArticleUrl, channelName, sectionName, sectionUniqueName, adChannelName) {
            this.teaserViewer.playVideo(videoClipUrl, videoArticleUrl, channelName, sectionName, sectionUniqueName, adChannelName);
        },
        previous: function() {
            if (this.teaserViewer != null) this.teaserViewer.previous();
        },
        next: function() {
            if (this.teaserViewer != null) this.teaserViewer.next();
        },
        render: function() {
            if (this.videos.length > 0) {
                var videoObject = this.videos[0];

                document.write("<div id=\"" + this.playerId + "Tracking\" class=\"videoHidden\"><img id=\"" + this.playerId + "IVW\" src=\"\" width=\"1\" height=\"1\" alt=\"\" class=\"countPixel\"/></div>");
                document.write("<div id=\"" + this.playerId + "Div\" class=\"videoPlayer\"></div>");
                eval("function " + this.playerId + "GetPrerollTag(channelName) { try { " + this.adFunctionBody + " } catch (e) {} return \"\"; }");
                eval("function " + this.playerId + "Tracking() { var ivwPixel = document.getElementById(\"" + this.playerId + "IVW\"); var IVW = \"http://welt.ivwbox.de/cgi-bin/ivw/CP/" + videoObject.uniqueSectionName + "\"; ivwPixel.src = IVW + \"?r=\" + escape(document.referrer) + \"&d=\" + (Math.random()*100000); if (omniture) omniture(); }");

                this.embedPlayer = new SWFObject(this.siteUrl + "flash/welt_main.swf", this.playerId + "Id", "468", "387", "8", "#FFFFFF");
                this.embedPlayer.addParam("allowScriptAccess", "sameDomain");
                this.embedPlayer.addParam("menu", "false");
                this.embedPlayer.addParam("quality", "high");
                this.embedPlayer.addParam("id", this.playerId + "Id");
                this.embedPlayer.addParam("quality", "middle");
                this.embedPlayer.addParam("allowFullScreen", "true");
                this.embedPlayer.addVariable("publication", escape(this.publicationName));
                this.embedPlayer.addVariable("mediaclip", escape(this.siteUrl + "media/query.do?clip=" + videoObject.videoId));
                this.embedPlayer.addVariable("autoPlay", escape("false"));
                this.embedPlayer.addVariable("sectionName", escape(videoObject.sectionName));
                this.embedPlayer.addVariable("adChannelName", escape(videoObject.adChannelName));
                this.embedPlayer.addVariable("prerollTagFunction", this.playerId + "GetPrerollTag");
                this.embedPlayer.addVariable("statsFunction", "videoLogStats");
                this.embedPlayer.addVariable("bwCheckUrl", escape("http://media.boreus.de/video/bwcheck.flv"));
                this.embedPlayer.addVariable("defaultMediaAssetPath", escape("http://media.boreus.de/video"));
                this.embedPlayer.write(this.playerId + "Div");

                if (this.videos.length > 1) {
                    this.videoInline = new VideoInline();
                    this.videoInline.setSwfObject(this.embedPlayer);
                    this.videoInline.setInlinePlayerDivId(this.playerId + "Div");
                    this.videoInline.setInlinePlayerId(this.playerId + "Id");

                    this.teaserViewer = new TeaserViewer();
                    this.teaserViewer.setTeaserWidth(102);
                    this.teaserViewer.setTeasersVisible(4);
                    this.teaserViewer.setTeasersTotal(this.videos.length - 1);
                    this.teaserViewer.setTeaserListId(this.playerId + "TeaserList");
                    this.teaserViewer.setTeasersLeftButtonId(this.playerId + "TeasersLeftButton");
                    this.teaserViewer.setTeasersRightButtonId(this.playerId + "TeasersRightButton");
                    this.teaserViewer.setScrollFactor(0.25);
                    this.teaserViewer.setInlinePlayerController(this.videoInline);

                    document.write("<div class=\"videoEmbedTeasers\">");
                    document.write("<div class=\"videoEmbedTeaserButton videoTeaserButtonLeft\"><div id=\"" + this.playerId + "TeasersLeftButton\" onclick=\"" + this.instanceName + ".previous();\" class=\"videoTeaserButtonInactive\"></div></div>");
                    document.write("<div class=\"videoEmbedTeaserView\"><div id=\"" + this.playerId + "TeaserList\" class=\"videoTeaserList\">");

                    for (var i = 1; i < this.videos.length; i++) {
                        videoObject = this.videos[i];
                        document.write("<div class=\"videoEmbedTeaser\">");
                        document.write("<div class=\"videoEmbedTeaserImage\"><a href=\"" + videoObject.deeplinkUrl + "#autoplay\" onclick=\"" + this.instanceName + ".playVideo('" + this.siteUrl + "media/query.do?clip=" + videoObject.videoId + "', '" + videoObject.deeplinkUrl + "', '" + videoObject.channelName + "', '" + videoObject.sectionName + "', '" + videoObject.uniqueSectionName + "', '" + videoObject.adChannelName + "'); return false;\"><img src=\"" + videoObject.thumbnailUrl + "\" alt=\"\" width=\"94\" height=\"62\"/></a></div>");
                        document.write("<div class=\"videoEmbedTeaserTitle\"><a href=\"" + videoObject.deeplinkUrl + "#autoplay\" onclick=\"" + this.instanceName + ".playVideo('" + this.siteUrl + "media/query.do?clip=" + videoObject.videoId + "', '" + videoObject.deeplinkUrl + "', '" + videoObject.channelName + "', '" + videoObject.sectionName + "', '" + videoObject.uniqueSectionName + "', '" + videoObject.adChannelName + "'); return false;\">" + videoObject.title + "</a></div>");
                        document.write("</div>");
                    }

                    document.write("</div></div>");
                    document.write("<div class=\"videoEmbedTeaserButton videoTeaserButtonRight\"><div id=\"" + this.playerId + "TeasersRightButton\" onclick=\"" + this.instanceName + ".next();\" class=\"videoTeaserButtonInactive\"></div></div>");
                    document.write("</div>");

                    this.teaserViewer.init();
                }
            }
        }
    }

}

var TeaserViewer = videoPrototype.TeaserViewer;
var SectionBox = videoPrototype.SectionBox;
var TabController = videoPrototype.TabController;
var VideoComment = videoPrototype.VideoComment;
var VideoSend = videoPrototype.VideoSend;
var VideoInline = videoPrototype.VideoInline;
var VideoStats = videoPrototype.VideoStats;
var VideoEmbed = videoPrototype.VideoEmbed;
window.videoFirstPlayed = false;

function videoLogStats(parameters) {
    if (window.videoFirstPlayed || (location.href.substring(location.href.length - 9) != "#autoplay")) {
        var videoStats = new VideoStats();
        videoStats.logStats(parameters);
    }
    window.videoFirstPlayed = true;
}

/* ---- gallery ---- */
var displayAd = 0;
var displayAdReload = 0;
var loadAd = new Array (0,0,0,0);
var galleryClick = new Array(0,0,0,0);
var galleryChannel = new Array();
var adCount = new Array();
var adFrame = new Array('adFrame','textGalleryAdFrame','adRelaodFrame','textGalleryAdRelaodFrame');
var adSrc = new Array('adGallery','adGallery','adReload','adReload');

function openImageGalleryPopup(Url)
{
	window.open(Url,'picture_gallery','menubar=no, toolbar=no, status=no, width=665, height=565, scrollbars=no, resizable=no');
}

function setLoadAd(status,galleryType)
{
	
  if (status == 1)
  {
    loadAd[galleryType] = 2;
  }
}

function getGalleryAd(galleryType)
{
	galleryClick[galleryType] = galleryClick[galleryType] + 1;

	if (galleryClick[galleryType] == adCount[galleryType])
	{
		galleryClick[galleryType] = -1;
		
		var stampAdURL = new Date();
		
		var adURL = "/"+adSrc[galleryType]+".html?r="+galleryChannel[galleryType]+"&t="+galleryType+"&s="+stampAdURL.getTime();
		document.getElementById(adFrame[galleryType]).setAttribute("src",adURL);
		loadAd[galleryType] = 1;
		return 0;
	} 

	if (loadAd[galleryType] == 2)
	{
		loadAd[galleryType] = 0;
		return 1;
	}

	return 0;
}

function viewImage(linkElement, number)
{
	if (hideAds == false)
	{
		displayAd = getGalleryAd(0);
	}
	
	if (hideAdReload == false)
	{
		displayAdReload = getGalleryAd(2);
	}
	
	if (displayAd == 1 && document.getElementById("adFrame").contentWindow.document.getElementById("banner").innerHTML.indexOf("display:none;") == -1 && showAdBanner == true)
	{
		document.getElementById("galleryBanner").innerHTML = "<div class=\"advertising\">Anzeige</div>" + document.getElementById("adFrame").contentWindow.document.getElementById("banner").innerHTML;
		document.getElementById('galleryBanner').style.display = 'block';
	}
		
	if (displayAd == 1 && document.getElementById("adFrame").contentWindow.document.getElementById("ad").innerHTML.indexOf("display:none;") != -1)
	{
		displayAd = 0;
	}
	
	if (displayAd == 1 && hideAds == false )
	{

	imgNode = document.getElementById("imagebox");
	imgNode.innerHTML = document.getElementById("adFrame").contentWindow.document.getElementById("ad").innerHTML;	
	document.getElementById("fullimage_copy").innerHTML = '';
	document.getElementById("fullimage_intro").innerHTML = '';
	document.getElementById("galleryIndex").innerHTML = 'Werbung';
	galleryClick[0] = 0;

	} else 
	{
	
		if (number != null) {
			imageId = number;
		} else {
			imageId = linkElement.name;
		}
		curr = images[imageId];

		imgNode = document.getElementById("fullimage");
		if (imgNode == null)
		{
		  document.getElementById("imagebox").innerHTML = '<a id="fullimage_forward_imagebutton" name="'+curr[10]+'" onclick="viewImage(this);return false;"><div id="fullimage"></div></a>';
		  imgNode = document.getElementById("fullimage");
		}
		
		imgNode.innerHTML = '<img src="' + curr[0] + '" alt="' + decodeURI(curr[4]) + '" title="' + decodeURI(curr[4]) + '" height="' + curr[12] + '" width="' + curr[13] + '" style="height:' + curr[14] + 'em; width:' + curr[15] + 'em;" />';

		if (document.getElementById("fullimage_index") == null)
		{
		  document.getElementById("galleryIndex").innerHTML = galleryIndexHTML;
		}
		
	// document.getElementById("fullimage_author").innerHTML = curr[11];
	// copyright: picture source declaration
		cursrclabel = "";
		if (typeof(curr[3]) != 'undefined') cursrclabel = curr[3];
		document.getElementById("fullimage_copy").innerHTML = cursrclabel;
		// document.getElementById("fullimage_headline").innerHTML = curr[1];
		document.getElementById("fullimage_intro").innerHTML = unescape(curr[2]);
		document.getElementById("fullimage_index").innerHTML = curr[8];
		document.getElementById("fullimage_back").name= curr[9];
		document.getElementById("fullimage_forward").name = curr[10];
		document.getElementById("fullimage_forward_imagebutton").name = curr[10];

		getCounters(getCountersCodePageType,getCountersCodePageName,getAgofCode, '', getPageId, getContentdId, getContentGroup_1, getContentGroup_2, getContentGroup_3, getContentGroup_4, getCustomParameter_2);

	}
	
	if (displayAdReload == 1 &&  hideAdReload == false )
	{
		document.getElementById("ad2").innerHTML = document.getElementById("adRelaodFrame").contentWindow.document.getElementById("bannerAd").innerHTML;
		document.getElementById("ad2").style.height = '90px';
		document.getElementById("ad3").innerHTML = document.getElementById("adRelaodFrame").contentWindow.document.getElementById("scyscraperAd").innerHTML;
		document.getElementById("banner_1").innerHTML = "<div class=\"advertising\">Anzeige</div>" + document.getElementById("adRelaodFrame").contentWindow.document.getElementById("rectangleAd").innerHTML;
	}

}
/* ---- swfobject ---- */
/* STATUS:FINAL */

/**
 * SWFObject v1.4: Flash Player detection and embed - http://blog.deconcept.com/swfobject/
 *
 * SWFObject is (c) 2006 Geoff Stearns and is released under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 *
 * **SWFObject is the SWF embed script formerly known as FlashObject. The name was changed for
 *   legal reasons.
 */
try
{ 

if(typeof deconcept=="undefined"){var deconcept=new Object();}
if(typeof deconcept.util=="undefined"){deconcept.util=new Object();}
if(typeof deconcept.SWFObjectUtil=="undefined"){deconcept.SWFObjectUtil=new Object();}

deconcept.SWFObject=function(_1,id,w,h,_5,c,_7,_8,_9,_a,_b)
{
	if(!document.createElement||!document.getElementById){return;}
	
	this.DETECT_KEY=_b?_b:"detectflash";
	this.skipDetect=deconcept.util.getRequestParameter(this.DETECT_KEY);
	this.params=new Object();
	this.variables=new Object();
	this.attributes=new Array();
	
	if(_1){this.setAttribute("swf",_1);}
	if(id){this.setAttribute("id",id);}
	if(w){this.setAttribute("width",w);}
	if(h){this.setAttribute("height",h);}
	if(_5){this.setAttribute("version",new deconcept.PlayerVersion(_5.toString().split(".")));}

	this.installedVer=deconcept.SWFObjectUtil.getPlayerVersion(this.getAttribute("version"),_7);
	if(c){this.addParam("bgcolor",c);}
	
	var q=_8?_8:"high";
	this.addParam("quality",q);
	this.setAttribute("useExpressInstall",_7);
	this.setAttribute("doExpressInstall",false);
	var _d=(_9)?_9:window.location;
	
	this.setAttribute("xiRedirectUrl",_d);
	this.setAttribute("redirectUrl","");
	if(_a){this.setAttribute("redirectUrl",_a);}
};
deconcept.SWFObject.prototype={setAttribute:function(_e,_f){
this.attributes[_e]=_f;
},getAttribute:function(_10){
return this.attributes[_10];
},addParam:function(_11,_12){
this.params[_11]=_12;
},getParams:function(){
return this.params;
},addVariable:function(_13,_14){
this.variables[_13]=_14;
},getVariable:function(_15){
return this.variables[_15];
},getVariables:function(){
return this.variables;
},getVariablePairs:function(){
var _16=new Array();
var key;
var _18=this.getVariables();
for(key in _18){
_16.push(key+"="+_18[key]);}
return _16;
},getSWFHTML:function(){
var _19="";
if(navigator.plugins&&navigator.mimeTypes&&navigator.mimeTypes.length){
if(this.getAttribute("doExpressInstall")){this.addVariable("MMplayerType","PlugIn");}
_19="<embed type=\"application/x-shockwave-flash\" src=\""+this.getAttribute("swf")+"\" width=\""+this.getAttribute("width")+"\" height=\""+this.getAttribute("height")+"\"";
_19+=" id=\""+this.getAttribute("id")+"\" name=\""+this.getAttribute("id")+"\" ";
var _1a=this.getParams();
for(var key in _1a){_19+=[key]+"=\""+_1a[key]+"\" ";}
var _1c=this.getVariablePairs().join("&");
if(_1c.length>0){_19+="flashvars=\""+_1c+"\"";}
_19+="/>";
}else{
if(this.getAttribute("doExpressInstall")){this.addVariable("MMplayerType","ActiveX");}
_19="<object id=\""+this.getAttribute("id")+"\" classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" width=\""+this.getAttribute("width")+"\" height=\""+this.getAttribute("height")+"\">";
_19+="<param name=\"movie\" value=\""+this.getAttribute("swf")+"\" />";
var _1d=this.getParams();
for(var key in _1d){_19+="<param name=\""+key+"\" value=\""+_1d[key]+"\" />";}
var _1f=this.getVariablePairs().join("&");
if(_1f.length>0){_19+="<param name=\"flashvars\" value=\""+_1f+"\" />";}
_19+="</object>";}
return _19;
},write:function(_20){
if(this.getAttribute("useExpressInstall")){
var _21=new deconcept.PlayerVersion([6,0,65]);
if(this.installedVer.versionIsValid(_21)&&!this.installedVer.versionIsValid(this.getAttribute("version"))){
this.setAttribute("doExpressInstall",true);
this.addVariable("MMredirectURL",escape(this.getAttribute("xiRedirectUrl")));
document.title=document.title.slice(0,47)+" - Flash Player Installation";
this.addVariable("MMdoctitle",document.title);}}
if(this.skipDetect||this.getAttribute("doExpressInstall")||this.installedVer.versionIsValid(this.getAttribute("version"))){
	var n=(typeof _20=="string")?document.getElementById(_20):_20;
	if (n)
		n.innerHTML=this.getSWFHTML();
	return true;
}else{
if(this.getAttribute("redirectUrl")!=""){document.location.replace(this.getAttribute("redirectUrl"));}}
return false;}};
deconcept.SWFObjectUtil.getPlayerVersion=function(_23,_24){
var _25=new deconcept.PlayerVersion([0,0,0]);
if(navigator.plugins&&navigator.mimeTypes.length){
var x=navigator.plugins["Shockwave Flash"];
if(x&&x.description){_25=new deconcept.PlayerVersion(x.description.replace(/([a-z]|[A-Z]|\s)+/,"").replace(/(\s+r|\s+b[0-9]+)/,".").split("."));}
}else{try{
var axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
for(var i=3;axo!=null;i++){
axo=new ActiveXObject("ShockwaveFlash.ShockwaveFlash."+i);
_25=new deconcept.PlayerVersion([i,0,0]);}}
catch(e){}
if(_23&&_25.major>_23.major){return _25;}
if(!_23||((_23.minor!=0||_23.rev!=0)&&_25.major==_23.major)||_25.major!=6||_24){
try{_25=new deconcept.PlayerVersion(axo.GetVariable("$version").split(" ")[1].split(","));}
catch(e){}}}
return _25;};
deconcept.PlayerVersion=function(_29){
this.major=parseInt(_29[0])!=null?parseInt(_29[0]):0;
this.minor=parseInt(_29[1])||0;
this.rev=parseInt(_29[2])||0;};
deconcept.PlayerVersion.prototype.versionIsValid=function(fv){
if(this.major<fv.major){return false;}
if(this.major>fv.major){return true;}
if(this.minor<fv.minor){return false;}
if(this.minor>fv.minor){return true;}
if(this.rev<fv.rev){return false;}return true;};
deconcept.util={getRequestParameter:function(_2b){
var q=document.location.search||document.location.hash;
if(q){
var _2d=q.indexOf(_2b+"=");
var _2e=(q.indexOf("&",_2d)>-1)?q.indexOf("&",_2d):q.length;
if(q.length>1&&_2d>-1){
return q.substring(q.indexOf("=",_2d)+1,_2e);
}}return "";}};
if(Array.prototype.push==null){
Array.prototype.push=function(_2f){
this[this.length]=_2f;
return this.length;};}
var getQueryParamValue=deconcept.util.getRequestParameter;
var FlashObject=deconcept.SWFObject; // for backwards compatibility
var SWFObject=deconcept.SWFObject;

}
catch(e)
{	
}
/* ---- control.tabs ---- */
/**
 * @author Ryan Johnson <ryan@livepipe.net>
 * @copyright 2007 LivePipe LLC
 * @package Control.Tabs
 * @license MIT
 * @url http://livepipe.net/projects/control_tabs/
 * @version 2.1.1
 */

if(typeof(Control) == 'undefined')
	var Control = {};
Control.Tabs = Class.create();
Object.extend(Control.Tabs,{
	instances: [],
	findByTabId: function(id){
		return Control.Tabs.instances.find(function(tab){
			return tab.links.find(function(link){
				return link.key == id;
			});
		});
	}
});
Object.extend(Control.Tabs.prototype,{
	initialize: function(tab_list_container,options){
		this.activeContainer = false;
		this.activeLink = false;
		this.containers = $H({});
		this.links = [];
		Control.Tabs.instances.push(this);
		this.options = {
			beforeChange: Prototype.emptyFunction,
			afterChange: Prototype.emptyFunction,
			hover: false,
			linkSelector: 'li a',
			setClassOnContainer: false,
			activeClassName: 'active',
			defaultTab: 'first',
			autoLinkExternal: true,
			targetRegExp: /#(.+)$/,
			showFunction: Element.show,
			hideFunction: Element.hide
		};
		Object.extend(this.options,options || {});
		(typeof(this.options.linkSelector == 'string')
			? $(tab_list_container).getElementsBySelector(this.options.linkSelector)
			: this.options.linkSelector($(tab_list_container))
		).findAll(function(link){
			return (/^#/).exec(link.href.replace(window.location.href.split('#')[0],''));
		}).each(function(link){
			this.addTab(link);
		}.bind(this));
		this.containers.values().each(this.options.hideFunction);
		if(this.options.defaultTab == 'first')
			this.setActiveTab(this.links.first());
		else if(this.options.defaultTab == 'last')
			this.setActiveTab(this.links.last());
		else
			this.setActiveTab(this.options.defaultTab);
		var targets = this.options.targetRegExp.exec(window.location);
		if(targets && targets[1]){
			targets[1].split(',').each(function(target){
				this.links.each(function(target,link){
					if(link.key == target){
						this.setActiveTab(link);
						throw $break;
					}
				}.bind(this,target));
			}.bind(this));
		}
		if(this.options.autoLinkExternal){
			$A(document.getElementsByTagName('a')).each(function(a){
				if(!this.links.include(a)){
					var clean_href = a.href.replace(window.location.href.split('#')[0],'');
					if(clean_href.substring(0,1) == '#'){
						if(this.containers.keys().include(clean_href.substring(1))){
							$(a).observe('click',function(event,clean_href){
								this.setActiveTab(clean_href.substring(1));
							}.bindAsEventListener(this,clean_href));
						}
					}
				}
			}.bind(this));
		}
	},
	addTab: function(link){
		this.links.push(link);
		link.key = link.getAttribute('href').replace(window.location.href.split('#')[0],'').split('/').last().replace(/#/,'');
		this.containers[link.key] = $(link.key);
		link[this.options.hover ? 'onmouseover' : 'onclick'] = function(link){
			if(window.event)
				Event.stop(window.event);
			this.setActiveTab(link);
			return false;
		}.bind(this,link);
	},
	setActiveTab: function(link){
		if(!link)
			return;
		if(typeof(link) == 'string'){
			this.links.each(function(_link){
				if(_link.key == link){
					this.setActiveTab(_link);
					throw $break;
				}
			}.bind(this));
		}else{
			this.notify('beforeChange',this.activeContainer);
			if(this.activeContainer)
				this.options.hideFunction(this.activeContainer);
			this.links.each(function(item){
				(this.options.setClassOnContainer ? $(item.parentNode) : item).removeClassName(this.options.activeClassName);
			}.bind(this));
			(this.options.setClassOnContainer ? $(link.parentNode) : link).addClassName(this.options.activeClassName);
			this.activeContainer = this.containers[link.key];
			this.activeLink = link;
			this.options.showFunction(this.containers[link.key]);
			this.notify('afterChange',this.containers[link.key]);
		}
	},
	next: function(){
		this.links.each(function(link,i){
			if(this.activeLink == link && this.links[i + 1]){
				this.setActiveTab(this.links[i + 1]);
				throw $break;
			}
		}.bind(this));
		return false;
	},
	previous: function(){
		this.links.each(function(link,i){
			if(this.activeLink == link && this.links[i - 1]){
				this.setActiveTab(this.links[i - 1]);
				throw $break;
			}
		}.bind(this));
		return false;
	},
	first: function(){
		this.setActiveTab(this.links.first());
		return false;
	},
	last: function(){
		this.setActiveTab(this.links.last());
		return false;
	},
	notify: function(event_name){
		try{
			if(this.options[event_name])
				return [this.options[event_name].apply(this.options[event_name],$A(arguments).slice(1))];
		}catch(e){
			if(e != $break)
				throw e;
			else
				return false;
		}
	}
});
if(typeof(Object.Event) != 'undefined')
	Object.Event.extend(Control.Tabs);

/* ---- control.modal Lightbox Clone ---- */
/**
 * @author Ryan Johnson <ryan@livepipe.net>
 * @copyright 2007 LivePipe LLC
 * @package Control.Modal
 * @license MIT
 * @url http://livepipe.net/projects/control_modal/
 * @version 2.2.2
 */

if(typeof(Control) == "undefined")
	Control = {};
Control.Modal = Class.create();
Object.extend(Control.Modal,{
	loaded: false,
	loading: false,
	loadingTimeout: false,
	overlay: false,
	container: false,
	current: false,
	ie: false,
	effects: {
		containerFade: false,
		containerAppear: false,
		overlayFade: false,
		overlayAppear: false
	},
	targetRegexp: /#(.+)$/,
	imgRegexp: /\.(jpe?g|gif|png|tiff?)$/,
	overlayStyles: {
		position: 'fixed',
		top: 0,
		left: 0,
		width: '100%',
		height: '100%',
		zIndex: 9998
	},
	overlayIEStyles: {
		position: 'absolute',
		top: 0,
		left: 0,
		zIndex: 9998
	},
	disableHoverClose: false,
	load: function(){
		if(!Control.Modal.loaded){
			Control.Modal.loaded = true;
			Control.Modal.ie = !(typeof document.body.style.maxHeight != 'undefined');
			Control.Modal.overlay = $(document.createElement('div'));
			Control.Modal.overlay.id = 'modal_overlay';
			Object.extend(Control.Modal.overlay.style,Control.Modal['overlay' + (Control.Modal.ie ? 'IE' : '') + 'Styles']);
			Control.Modal.overlay.hide();
			Control.Modal.container = $(document.createElement('div'));
			Control.Modal.container.id = 'modal_container';
			Control.Modal.container.hide();
			Control.Modal.loading = $(document.createElement('div'));
			Control.Modal.loading.id = 'modal_loading';
			Control.Modal.loading.hide();
			var body_tag = document.getElementsByTagName('body')[0];
			body_tag.appendChild(Control.Modal.overlay);
			body_tag.appendChild(Control.Modal.container);
			body_tag.appendChild(Control.Modal.loading);
			Control.Modal.container.observe('mouseout',function(event){
				if(!Control.Modal.disableHoverClose && Control.Modal.current && Control.Modal.current.options.hover && !Position.within(Control.Modal.container,Event.pointerX(event),Event.pointerY(event)))
					Control.Modal.close();
			});
		}
	},
	open: function(contents,options){
		options = options || {};
		if(!options.contents)
			options.contents = contents;
		var modal_instance = new Control.Modal(false,options);
		modal_instance.open();
		return modal_instance;
	},
	close: function(force){
		if(typeof(force) != 'boolean')
			force = false;
		if(Control.Modal.current)
			Control.Modal.current.close(force);
	},
//	attachEvents: function(){
//		Event.observe(window,'load',Control.Modal.load);
//		Event.observe(window,'unload',Event.unloadCache,false);
//	},
	
	attachEvents: function(){
	document.observe('contentloaded', Control.Modal.load);  
	},
	
	
	center: function(element){
		if(!element._absolutized){
			element.setStyle({
				position: 'absolute'
			}); 
			element._absolutized = true;
		}
		var dimensions = element.getDimensions();
		Position.prepare();
		var offset_left = (Position.deltaX + Math.floor((Control.Modal.getWindowWidth() - dimensions.width) / 2));
		var offset_top = (Position.deltaY + ((Control.Modal.getWindowHeight() > dimensions.height) ? Math.floor((Control.Modal.getWindowHeight() - dimensions.height) / 2) : 0));
		element.setStyle({
			top: ((dimensions.height <= Control.Modal.getDocumentHeight()) ? ((offset_top != null && offset_top > 0) ? offset_top : '0') + 'px' : 0),
			left: ((dimensions.width <= Control.Modal.getDocumentWidth()) ? ((offset_left != null && offset_left > 0) ? offset_left : '0') + 'px' : 0)
		});
	},
	getWindowWidth: function(){
		return (self.innerWidth || document.documentElement.clientWidth || document.body.clientWidth || 0);
	},
	getWindowHeight: function(){
		return (self.innerHeight ||  document.documentElement.clientHeight || document.body.clientHeight || 0);
	},
	getDocumentWidth: function(){
		return Math.min(document.body.scrollWidth,Control.Modal.getWindowWidth());
	},
	getDocumentHeight: function(){
		return Math.max(document.body.scrollHeight,Control.Modal.getWindowHeight());
	},
	onKeyDown: function(event){
		if(event.keyCode == Event.KEY_ESC)
			Control.Modal.close();
	}
});
Object.extend(Control.Modal.prototype,{
	mode: '',
	html: false,
	href: '',
	element: false,
	src: false,
	imageLoaded: false,
	ajaxRequest: false,
	initialize: function(element,options){
		this.element = $(element);
		this.options = {
			beforeOpen: Prototype.emptyFunction,
			afterOpen: Prototype.emptyFunction,
			beforeClose: Prototype.emptyFunction,
			afterClose: Prototype.emptyFunction,
			onSuccess: Prototype.emptyFunction,
			onFailure: Prototype.emptyFunction,
			onException: Prototype.emptyFunction,
			beforeImageLoad: Prototype.emptyFunction,
			afterImageLoad: Prototype.emptyFunction,
			autoOpenIfLinked: true,
			contents: false,
			loading: false, //display loading indicator
			fade: false,
			fadeDuration: 0.75,
			image: false,
			imageCloseOnClick: true,
			hover: false,
			iframe: false,
			iframeTemplate: new Template('<iframe src="#{href}" width="100%" height="100%" frameborder="0" id="#{id}"></iframe>'),
			evalScripts: true, //for Ajax, define here instead of in requestOptions
			requestOptions: {}, //for Ajax.Request
			overlayDisplay: true,
			overlayClassName: '',
			overlayCloseOnClick: true,
			containerClassName: '',
			opacity: 0.3,
			zIndex: 9998,
			width: null,
			height: null,
			offsetLeft: 0, //for use with 'relative'
			offsetTop: 0, //for use with 'relative'
			position: 'absolute' //'absolute' or 'relative'
		};
		Object.extend(this.options,options || {});
		var target_match = false;
		var image_match = false;
		if(this.element){
			target_match = Control.Modal.targetRegexp.exec(this.element.href);
			image_match = Control.Modal.imgRegexp.exec(this.element.href);
		}
		if(this.options.position == 'mouse')
			this.options.hover = true;
		if(this.options.contents){
			this.mode = 'contents';
		}else if(this.options.image || image_match){
			this.mode = 'image';
			this.src = this.element.href;
		}else if(target_match){
			this.mode = 'named';
			var x = $(target_match[1]);
			this.html = x.innerHTML;
			x.remove();
			this.href = target_match[1];
		}else{
			this.mode = (this.options.iframe) ? 'iframe' : 'ajax';
			this.href = this.element.href;
		}
		if(this.element){
			if(this.options.hover){
				this.element.observe('mouseover',this.open.bind(this));
				this.element.observe('mouseout',function(event){
					if(!Position.within(Control.Modal.container,Event.pointerX(event),Event.pointerY(event)))
						this.close();
				}.bindAsEventListener(this));
			}else{
				this.element.onclick = function(event){
					this.open();
					Event.stop(event);
					return false;
				}.bindAsEventListener(this);
			}
		}
		var targets = Control.Modal.targetRegexp.exec(window.location);
		this.position = function(event){
			if(this.options.position == 'absolute')
				Control.Modal.center(Control.Modal.container);
			else{
				var xy = (event && this.options.position == 'mouse' ? [Event.pointerX(event),Event.pointerY(event)] : Position.cumulativeOffset(this.element));
				Control.Modal.container.setStyle({
					position: 'absolute',
					top: xy[1] + (typeof(this.options.offsetTop) == 'function' ? this.options.offsetTop() : this.options.offsetTop) + 'px',
					left: xy[0] + (typeof(this.options.offsetLeft) == 'function' ? this.options.offsetLeft() : this.options.offsetLeft) + 'px'
				});
			}
			if(Control.Modal.ie){
				Control.Modal.overlay.setStyle({
					height: Control.Modal.getDocumentHeight() + 'px',
					width: Control.Modal.getDocumentWidth() + 'px'
				});
			}
		}.bind(this);
		if(this.mode == 'named' && this.options.autoOpenIfLinked && targets && targets[1] && targets[1] == this.href)
			this.open();
	},
	showLoadingIndicator: function(){
		if(this.options.loading){
			Control.Modal.loadingTimeout = window.setTimeout(function(){
				var modal_image = $('modal_image');
				if(modal_image)
					modal_image.hide();
				Control.Modal.loading.style.zIndex = this.options.zIndex + 1;
				Control.Modal.loading.update('<img id="modal_loading" src="' + this.options.loading + '"/>');
				Control.Modal.loading.show();
				Control.Modal.center(Control.Modal.loading);
			}.bind(this),250);
		}
	},
	hideLoadingIndicator: function(){
		if(this.options.loading){
			if(Control.Modal.loadingTimeout)
				window.clearTimeout(Control.Modal.loadingTimeout);
			var modal_image = $('modal_image');
			if(modal_image)
				modal_image.show();
			Control.Modal.loading.hide();
		}
	},
	open: function(force){
		if(!force && this.notify('beforeOpen') === false)
			return;
		if(!Control.Modal.loaded)
			Control.Modal.load();
		Control.Modal.close();
		if(!this.options.hover)
			Event.observe($(document.getElementsByTagName('body')[0]),'keydown',Control.Modal.onKeyDown);
		Control.Modal.current = this;
		if(!this.options.hover)
			Control.Modal.overlay.setStyle({
				zIndex: this.options.zIndex,
				opacity: this.options.opacity
			});
		Control.Modal.container.setStyle({
			zIndex: this.options.zIndex + 1,
			width: (this.options.width ? (typeof(this.options.width) == 'function' ? this.options.width() : this.options.width) + 'px' : null),
			height: (this.options.height ? (typeof(this.options.height) == 'function' ? this.options.height() : this.options.height) + 'px' : null)
		});
		if(Control.Modal.ie && !this.options.hover){
			$A(document.getElementsByTagName('select')).each(function(select){
				select.style.visibility = 'hidden';
			});
		}
		Control.Modal.overlay.addClassName(this.options.overlayClassName);
		Control.Modal.container.addClassName(this.options.containerClassName);
		switch(this.mode){
			case 'image':
				this.imageLoaded = false;
				this.notify('beforeImageLoad');
				this.showLoadingIndicator();
				var img = document.createElement('img');
				img.onload = function(img){
					this.hideLoadingIndicator();
					this.update([img]);
					if(this.options.imageCloseOnClick)
						$(img).observe('click',Control.Modal.close);
					this.position();
					this.notify('afterImageLoad');
					img.onload = null;
				}.bind(this,img);
				img.src = this.src;
				img.id = 'modal_image';
				break;
			case 'ajax':
				this.notify('beforeLoad');
				var options = {
					method: 'post',
					onSuccess: function(request){
						this.hideLoadingIndicator();
						this.update(request.responseText);
						this.notify('onSuccess',request);
						this.ajaxRequest = false;
					}.bind(this),
					onFailure: function(){
						this.notify('onFailure');
					}.bind(this),
					onException: function(){
						this.notify('onException');
					}.bind(this)
				};
				Object.extend(options,this.options.requestOptions);
				this.showLoadingIndicator();
				this.ajaxRequest = new Ajax.Request(this.href,options);
				break;
			case 'iframe':
				this.update(this.options.iframeTemplate.evaluate({href: this.href, id: 'modal_iframe'}));
				break;
			case 'contents':
				this.update((typeof(this.options.contents) == 'function' ? this.options.contents() : this.options.contents));
				break;
			case 'named':
				this.update(this.html);
				break;
		}
		if(!this.options.hover){
			if(this.options.overlayCloseOnClick && this.options.overlayDisplay)
				Control.Modal.overlay.observe('click',Control.Modal.close);
			if(this.options.overlayDisplay){
				if(this.options.fade){
					if(Control.Modal.effects.overlayFade)
						Control.Modal.effects.overlayFade.cancel();
					Control.Modal.effects.overlayAppear = new Effect.Appear(Control.Modal.overlay,{
						queue: {
							position: 'front',
							scope: 'Control.Modal'
						},
						to: this.options.opacity,
						duration: this.options.fadeDuration / 2
					});
				}else
					Control.Modal.overlay.show();
			}
		}
		if(this.options.position == 'mouse'){
			this.mouseHoverListener = this.position.bindAsEventListener(this);
			this.element.observe('mousemove',this.mouseHoverListener);
		}
		this.notify('afterOpen');
	},
	update: function(html){
		if(typeof(html) == 'string')
			Control.Modal.container.update(html);
		else{
			Control.Modal.container.update('');
			(html.each) ? html.each(function(node){
				Control.Modal.container.appendChild(node);
			}) : Control.Modal.container.appendChild(node);
		}
		if(this.options.fade){
			if(Control.Modal.effects.containerFade)
				Control.Modal.effects.containerFade.cancel();
			Control.Modal.effects.containerAppear = new Effect.Appear(Control.Modal.container,{
				queue: {
					position: 'end',
					scope: 'Control.Modal'
				},
				to: 1,
				duration: this.options.fadeDuration / 2
			});
		}else
			Control.Modal.container.show();
		this.position();
		Event.observe(window,'resize',this.position,false);
		Event.observe(window,'scroll',this.position,false);
	},
	close: function(force){
		if(!force && this.notify('beforeClose') === false)
			return;
		if(this.ajaxRequest)
			this.ajaxRequest.transport.abort();
		this.hideLoadingIndicator();	
		if(this.mode == 'image'){
			var modal_image = $('modal_image');
			if(this.options.imageCloseOnClick && modal_image)
				modal_image.stopObserving('click',Control.Modal.close);
		}
		if(Control.Modal.ie && !this.options.hover){
			$A(document.getElementsByTagName('select')).each(function(select){
				select.style.visibility = 'visible';
			});			
		}
		if(!this.options.hover)
			Event.stopObserving(window,'keyup',Control.Modal.onKeyDown);
		Control.Modal.current = false;
		Event.stopObserving(window,'resize',this.position,false);
		Event.stopObserving(window,'scroll',this.position,false);
		if(!this.options.hover){
			if(this.options.overlayCloseOnClick && this.options.overlayDisplay)
				Control.Modal.overlay.stopObserving('click',Control.Modal.close);
			if(this.options.overlayDisplay){
				if(this.options.fade){
					if(Control.Modal.effects.overlayAppear)
						Control.Modal.effects.overlayAppear.cancel();
					Control.Modal.effects.overlayFade = new Effect.Fade(Control.Modal.overlay,{
						queue: {
							position: 'end',
							scope: 'Control.Modal'
						},
						from: this.options.opacity,
						to: 0,
						duration: this.options.fadeDuration / 2
					});
				}else
					Control.Modal.overlay.hide();
			}
		}
		if(this.options.fade){
			if(Control.Modal.effects.containerAppear)
				Control.Modal.effects.containerAppear.cancel();
			Control.Modal.effects.containerFade = new Effect.Fade(Control.Modal.container,{
				queue: {
					position: 'front',
					scope: 'Control.Modal'
				},
				from: 1,
				to: 0,
				duration: this.options.fadeDuration / 2,
				afterFinish: function(){
					Control.Modal.container.update('');
					this.resetClassNameAndStyles();
				}.bind(this)
			});
		}else{
			Control.Modal.container.hide();
			Control.Modal.container.update('');
			this.resetClassNameAndStyles();
		}
		if(this.options.position == 'mouse')
			this.element.stopObserving('mousemove',this.mouseHoverListener);
		this.notify('afterClose');
	},
	resetClassNameAndStyles: function(){
		Control.Modal.overlay.removeClassName(this.options.overlayClassName);
		Control.Modal.container.removeClassName(this.options.containerClassName);
		Control.Modal.container.setStyle({
			height: null,
			width: null,
			top: null,
			left: null
		});
	},
	notify: function(event_name){
		try{
			if(this.options[event_name])
				return [this.options[event_name].apply(this.options[event_name],$A(arguments).slice(1))];
		}catch(e){
			if(e != $break)
				throw e;
			else
				return false;
		}
	}
});
if(typeof(Object.Event) != 'undefined')
	Object.Event.extend(Control.Modal);
Control.Modal.attachEvents();
/* ---- ajax ---- */
function ajaxLoad(url, targetElementId, method, data, onLoadFunction) {
	if (!method) method = "get";
	if (!onLoadFunction) onLoadFunction = function() {};
	new Ajax.Updater(targetElementId, url, {
		method: method.toLowerCase(),
		postBody: data,
		onComplete: onLoadFunction,
        encoding: null
    });
}



/* ---- rating ---- */
/*
 * GUI control class for module articleRating. 
 * 2008 Rüdiger Schulz (2rue.de) for ASMS
 */
var Rating = Class.create({
  COOKIE_NAME: 'RATING',	// see RatingService.COOKIE_NAME
  COOKIE_SEPA: '|',			// see RatingService.COOKIE_SEPA
  hasRated: false,
  guiLoaded: false,
  baseurl: '',
  articleId: 0,
  
  initialize: function(articleId, ajaxBaseurl) {
	this.articleId = articleId;
    this.baseurl = ajaxBaseurl;
    //this.baseurl = "rate.js?foo=bar";
    this.checkCookie();
  },
  getBaseUrl: function(ajaxType) {
	  return this.baseurl + "&now=" + new Date().getTime() + "&type=" + ajaxType;
  },
  rate: function(upordown) {
    var idClass = ".ratingBox"+this.articleId+" .ratings"; 
    $$(idClass+" .voteButtons, "+idClass+" .totalRatings").each(function(voteButton) {
        voteButton.fade({
            duration:0.5,
            afterFinish:function() {
                $$(idClass+" .ratingResultBorder, "+idClass+" .totalRatings, .ratingResultPercent").each(function(ratingResult) {
                    ratingResult.appear({duration:0.5});
                });
            }
        });
    });
    url = this.getBaseUrl('ratingVote') + "&upordown=" + upordown;
    new Ajax.Request(url, {
        method: 'get',
        evalJS: 'force' // force is required, as the server always sends text/html
    });
	//getCounters('rating');
  },
  toggle: function(buttonId) {
        var id = this.articleId+(buttonId?'_'+buttonId:'');
		var ratingBox = $("ratingBox"+id);
        var ratingLi = $("rating"+id);
        ratingBox.clonePosition(ratingLi,
            {setLeft:true,setTop:true,setWidth:false,setHeight:false,offsetLeft:-4,offsetTop:16});
        if (!this.guiLoaded) {
            this.loadGui();
        }
	    var isVisible = ratingBox.visible();
		hideBookmarks();	// defined in common.js
        Rating.hideBox();
        if(!isVisible) {
            ratingBox.show();
        }
		$$('.recommendBox').each(Element.hide);
		//getCounters('rating');
		return false;
  },
  loadGui: function() {
	  var r = this;
	  url = this.getBaseUrl("ratingShowGui") + "&hasRated=" + this.hasRated;
	  new Ajax.Request(url, {
		  method: 'get',
		  onSuccess: function(transport) {
			  $$('.ratingBox'+r.articleId+' .ratings .guiSpace').each(function(el) {
				  el.innerHTML = transport.responseText;
			  });
			  r.guiLoaded = true;
	  	  }
	  });
  },
  checkCookie: function() {
	var ratedIds = new Array();
	var cookies = document.cookie.split('; ');
	for (var i = 0; i < cookies.length; i++) {
		var crumb = cookies[i].split('=');
		if (crumb[0] == this.COOKIE_NAME) {
			ratedIds = crumb[1].split(this.COOKIE_SEPA);
			break;
		}
	}
	for (var j = 0; j < ratedIds.length; j++) {
		//alert("you rated for #" + ratedIds[j]);
		if (ratedIds[j] == this.articleId) {
			this.hasRated = true;
			break;
		}
	}
	//alert("Because articleId=" + this.articleId + ", hasRated is now " + this.hasRated);
  }
});
Rating.showBox = function() {
	  $$('.ratingBox').each(Element.show);
};
Rating.hideBox = function() {
	  $$('.ratingBox').each(Element.hide);
};


/* ---- highlight search keywords ---- */
var KeywordTextHighlighter = Class.create({
    keywordExtractRegex: new RegExp("(\\|\\d\\|)([^\\|]*)(\\|\\d\\|)"),
    se: new Array(
        new Array('suche.welt.de','query='),
        new Array('www.google.de/search','q='),
        new Array('www.google.at/search','q='),
        new Array('www.google.ch/search','q='),
        new Array('search.live.com/results.aspx','q='),
        new Array('search.yahoo.com/search','p=')
    ),

    initialize: function() {
        this.domUpdateData = new Array();
        var helper = new Helper();
        this.isFromSearch = helper.checkReferrerFromSearch(); 
        //alert("isFromSearch: " + this.isFromSearch);
        this.searchTerms = helper.getSearchTerms();
        //this.isFromSearch = this.checkReferrerFromSearch(); 
        //alert("searchTerms: " + this.searchTerms);
        if (this.isFromSearch) {
            keywordTextHighlighter = this;
            Event.observe(window, "load", function(ev) {
                keywordTextHighlighter.matchSearchTerms();
            });
        }
    },

    /*
     * print info above article
     */
    printSearchTerms: function () {
    	//alert("printSearchTerms-1 this.isFromSearch: " + this.isFromSearch);
        if (this.isFromSearch) {
	        document.writeln('<div id="yourSearchLine">');
	        document.writeln('    <div id="yourSearch">');
	        document.writeln('<span class=\"intro\">' + getTranslation("searchedWords") + ': </span>');
	        for (i=0; i<this.searchTerms.length; i++) {
	            document.writeln("<span class=\"highlighted term" + i + "\">" + this.searchTerms[i] + "</span>" + " ");
	        }
	        document.writeln('    </div>');
	        document.writeln('    <div><a href="javascript:location.href=location.href">' + getTranslation("disableHighlighting") + '</a></div>');
	        document.writeln('</div>');
        }
    },

    matchSearchTerms: function () {
        
        elements = $$('div.articleBox');
        for (eix=0; eix<elements.length; eix++) {
            this.treeReplace(elements[eix]);
        }
        this.updateDom();
    },

    treeReplace: function (node) {
        if (node.nodeType == 3) {
            this.highlightTextNode(node);
        }
        else if (node.hasChildNodes()) {
            childNodes = node.childNodes;
            $A(childNodes).each(this.treeReplace, this);
        }
    },

    highlightTextNode: function (element) {
        var newNodes = new Array();
        var tempNodeValue = ' ' + element.nodeValue + ' ';
        for (index=0, len = this.searchTerms.length; index<len && index < 8; ++index) {
            this.debug("marking term #" + index + ": " + this.searchTerms[index] + " out of " + len);
            var regex = new RegExp("([^\-|a-zA-Z0-9][\s]?)("+this.searchTerms[index]+")([^\-|a-zA-Z0-9][\s]?)",'ig');
            tempNodeValue = tempNodeValue.replace(regex, '$1|'+index+'|$2|'+index+'|$3');
        }
        this.highLight(element, tempNodeValue, newNodes);
        nodeSize = newNodes.size();
        this.debug("nodeSize: " + nodeSize  + " newNodes[0]: " + newNodes[0]);
        if (nodeSize == 0 || (nodeSize == 1 && newNodes[0].nodeType == 3)) {
            this.debug("<em>no highlights in " + element + "</em>");
            return; // nothing really changed
        }
        this.domUpdateData.push({
            textNode: element,
            newNodes: newNodes
        });
        this.debug(this.domUpdateData.length + " dom updates so far.");
    },

    highLight: function(textNode, markedText, newNodes) {
//        markedText = markedText.strip();
        this.debug("highlighting in " + textNode.nodeValue + " with " + markedText);
        i = 0;
        while (!markedText.empty() && i < 500) {
            // extract text until next keyword
            ks = markedText.indexOf('|');
            if (ks < 0) { // no more keywords
                ks = markedText.length;
            }
            normalText = markedText.substring(0, ks);
            this.addTextNode(newNodes, normalText);
            markedText = markedText.substring(ks);
            
            // extract keyword
            parts = this.keywordExtractRegex.exec(markedText);
            if (parts == null) {
                break;
            }
            markedText = markedText.substring(parts[0].length);
            newNodes.push(this.createHighlightNode(parts[2], parts[1].charAt(1)));
            
            i++;    // prevent endless loop
        }
    },
 
    addTextNode: function(newNodes, text) {
        this.debug("creatingTextNode: " + text);
//        if (text.blank())   return;
        newNodes.push(document.createTextNode(text));
        //this.newText += text;
    },
    
    createHighlightNode: function(text, colour) {
        this.debug("createHighlightNode(" + colour + ") " + text.strip());
        hnode = document.createElement('span');
        hnode.className = 'term' + colour;
        hnode.appendChild(document.createTextNode(text.strip()));
        //this.newText += '<span class="term' + colour + '">' + text + '</span>';
        return hnode;
    },
    
    updateDom: function() {
        this.debug("<hr/>");
        this.domUpdateData.each(function(dud) {
            this.debug("updating in " + dud.textNode.parentNode);
            textNode = dud.textNode;
            dud.newNodes.each(function(node) {
                this.debug("inserting " + node + " before " + textNode);
                try {
                    textNode.parentNode.insertBefore(node, textNode);
                } catch(err) {
                    this.debug(err);
                }
            }, this);
            textNode.parentNode.removeChild(textNode);
/*
*/
        }, this);
    },
    
    debug:function(text) {
        return;
        document.writeln(text + "<br/>");
    }

});



/* ---- show more search results ---- */
var MoreSearchResults = Class.create({

	initialize: function(ajaxBaseurl) {
	    this.domUpdateData = new Array();
	    this.baseUrl = ajaxBaseurl;
	    var helper = new Helper();
	    this.isFromSearch = helper.checkReferrerFromSearch();
        this.searchTerms = helper.getSearchTerms();
        if( this.searchTerms && this.searchTerms != "" )
            this.searchTerms = this.searchTerms.join(",");
        else
            this.searchTerms = "";
	    this.loadMoreSearchResults();
	},

	/*
	 * get the search query
	 */
	getSearchTerms: function() {
		return this.searchTerms;
	},
	
	/*
	 * comes from search engine
	 */
	isFromSearch: function() {
		return this.isFromSearch;
	},

	/*
	 * get the base url and append 
	 */
	getBaseUrl: function(ajaxType) {
		return this.baseUrl + "&type=" + ajaxType;
	},
	
	/*
	 * build new http request to show more search articles in right column
	 */
	loadMoreSearchResults: function() {
	    if (this.isFromSearch) {
		  var r = this;
		  // append url parameter to define which jsp or tag are get from wfAjax
		  url = this.getBaseUrl("moreSearchArticlesRight");
		  // javascript alert maybe a problem in asynchronous processes the box will not shown
		  new Ajax.Request(url, {
			  method: 'get',
			  parameters: 'searchQuery='+this.searchTerms+"&isInRightColumn=true",
			  onSuccess: function(transport) {
				  $$('#moreSearchResultRight').each(function(el) {
					  el.innerHTML = transport.responseText;
					  //alert("el.innerHTML.length: " + el.innerHTML.length + "\nel.innerHTML: " + el.innerHTML);
					  $(el).setStyle({display: 'block'});
					  //alert(transport.status);	
				  });
		  	  }
		  });
		  // append url parameter to define which jsp or tag are get from wfAjax
		  url = this.getBaseUrl("moreSearchArticles");
		  // javascript alert maybe a problem in asynchronous processes the box will not shown
		  new Ajax.Request(url, {
			  method: 'get',
			  parameters: 'searchQuery='+this.searchTerms+"&isInRightColumn=false",
			  onSuccess: function(transport) {
				  $$('#moreSearchResult').each(function(el) {
					  el.innerHTML = transport.responseText;
					  //alert("el.innerHTML.length: " + el.innerHTML.length + "\nel.innerHTML: " + el.innerHTML);
					  $(el).setStyle({display: 'block'});
					  //alert(transport.status);	
//					  if (el.innerHTML && el.innerHTML.length > 0) {
//						  $(el).setStyle({display: 'block'});
//					  }
				  });
				  $$('#relatedArticles').each(function(el) {
					  $(el).setStyle({display: 'none'});
				  });
		  	  }
		  });
	    }
	}

});

/* ---- recommend via SMS or E-Mail ---- */
var EmailSendFormValidator = Class.create(FormValidator, {
    initialize: function($super) {
        $super('recommendEmail_form');
        this.require({
            'receiverName':  getTranslation("article.form.nameRecipient"),
            'receiverEmail': getTranslation("article.form.mailRecipient"),
            'senderName':    getTranslation("article.form.name"),
            'senderEmail':   getTranslation("article.form.mail"),
            'comment':       getTranslation("article.form.remark"),
            'captchafield':  getTranslation("article.form.code")
        });
        
        this.enableAjax();
        //alert("email:function initialize");
    },
    // this is called by sendMail.jsp respond()
    processResult: function(anchor) {
        //alert("EMAIL processing " + anchor);
        if (anchor.indexOf('captcha_failure') >= 0) {
            this.addNotification(getTranslation('video.wrong.code'));
            this.clearCaptcha();
        } else if (anchor.indexOf('captcha_sessionError') >= 0) {
            this.addNotification(getTranslation('video.invalid.code'));
            this.clearCaptcha();
        } else if (anchor.indexOf('internalError') >= 0) {
            this.addNotification(getTranslation('internalError'));
            this.clearCaptcha();
        } else if (anchor.indexOf('parameterError') >= 0) {
            this.addNotification(getTranslation('wrongParameter'));
            this.clearCaptcha();
        } else if (anchor.indexOf('parameterEmpty') >= 0) {
            this.addNotification(getTranslation('wrongParameter'));
            this.clearCaptcha();
        } else if (anchor.indexOf('error') >= 0) {
            location.href = location.pathname + anchor;
            initByHash();
        } else {
        	//alert('send email ..else ');
            $('recommendEmail').hide();
            this.updateSuccessMessage();
            this.form.getInputs('text').invoke('clear');
            $('recommendEmail_comment').value = '';
            location.href = location.pathname + anchor;
            initByHash();
        }
    },
    clearCaptcha: function() {
        this.focusErrors();
        $('recommendEmail_captcha_img').src="/captcha/captcha.jpg?no_cache=" + Math.floor(Math.random() * 9000000 + 1000000);
        $('recommendEmail_code_field').value = '';
        $('recommendEmail_code_field').focus();
    },
    updateSuccessMessage: function() {
        $('xmsg_recommendEmail').scrollTo();
    }
});

var SmsSendFormValidator = Class.create(FormValidator, {
    initialize: function($super) {
        $super('recommendSms_form');
        this.require({
          'receiverName':   getTranslation("article.form.nameRecipient"),
          'receiverNumber': getTranslation("article.form.mobileRecipient"),
          'senderName':     getTranslation("article.form.name"),
//          'senderNumber':   getTranslation("article.form.mobile"),
          'captchafield':   getTranslation("article.form.code")
        });
        //alert("sms:function initialize111");
        this.addValidator('MobileNumberValidator', 'receiverNumber', getTranslation("article.form.mobileRecipient"));
        this.enableAjax();
    },
    processResult: function(anchor) {
         //alert("Sms processing " + anchor);
        if (anchor.indexOf('captcha_failure') >= 0) {
            this.addNotification(getTranslation('video.wrong.code'));
            this.clearCaptcha();
        }
        else if (anchor.indexOf('captcha_sessionError') >= 0) {
            this.addNotification(getTranslation('video.invalid.code'));
            this.clearCaptcha();
        }
        else if (anchor.indexOf('internalError') >= 0) {
            this.addNotification(getTranslation('internalError'));
            this.clearCaptcha();
        }
        else if (anchor.indexOf('error') >= 0) {
            location.href = location.pathname + anchor;
            initByHash();
        }        
        else  {
        	//alert("Success Processing");
            $('recommendSms').hide();
            this.updateSuccessMessage();
            this.form.getInputs('text').invoke('clear');
            location.href = location.pathname + anchor;
            initByHash();
        }
    },
    clearCaptcha: function() {
        this.focusErrors();
        $('recommendSms_captcha_img').src="/captcha/captcha.jpg?no_cache=" + Math.floor(Math.random() * 9000000 + 1000000);
        $('recommendSms_code_field').value = '';
        $('recommendSms_code_field').focus();
    },
    updateSuccessMessage: function() {
    	$('xmsg_recommendSms').innerHTML = '<div>' +
            getTranslation('article.recommenSmsSuccess', $('recommendSms_receiverNumber').getValue()) +
            '</div>';
    }
});

var CommentFormValidator = Class.create(FormValidator,
{	initialize: function($super, showCaptchaField)
	{	$super('com.escenic.forum.struts.presentation.PostingForm');
		this.require({
				'field(comment)': getTranslation("general.comment"),
			     'field(name)':    getTranslation("article.form.name"),
			    'field(email)':   getTranslation("article.form.mail")
			});
		if (showCaptchaField)
		{	this.require({
				'captchafield':   getTranslation("article.form.code")
			});
		}
		this.addValidator('MaxlengthValidator', 'field(comment)', getTranslation("general.comment"), [1500]); 
       	this.enableAjax();
	},
	processResult: function(anchor)
	{	if (anchor.indexOf('article_kommentar_captcha_sessionError') >= 0)
		{	this.addNotification(getTranslation('video.wrong.code'));
            this.clearCaptcha();
        }
        else if (anchor.indexOf('article_kommentar_captcha_failure') >= 0)
        {	this.addNotification(getTranslation('video.invalid.code'));
            this.clearCaptcha();
        }
        else if (anchor.indexOf('article_kommentar_forum_error') >= 0)
        {	this.addNotification(getTranslation('internalError'));
            this.clearCaptcha();
        }
       	else if (anchor.indexOf('error') >= 0)
       	{	location.href = location.pathname + anchor;
            initByHash();
        }
        // success submitted by doPostForum.jsp
        else // #xmsg_comment
        {	// empty text fields and hide comment box fields
        	
        	// store email and username for SSO users which are displayed by default
        	var username;
        	var email;
        	
        	if (getSSOUserId())
        	{	username = $('nameBox').value;
        		email = $('mailBox').value;
        	}
        
        	this.form.getInputs('text').invoke('clear');
        	
        	if (username)
        	{	$('nameBox').value = username;
        	}
        	if (email)
        	{	$('mailBox').value = email;        		
        	}
        
        	$('comment_box').value = '';
        	$('comment').hide();
        	
        	// show "Kommentar schreiben" - button
        	document.getElementById('articleBoxReadCommentButton2').style.display = "block";
        	
        	location.href = location.pathname + anchor;
            initByHash();
            
            this.updateSuccessMessage();
        }
        //alert("processResult " + anchor);
	},
	clearCaptcha: function()
	{	this.focusErrors();
		// could be that there is no captcha is needed, therefore catch exceptions
       	try
       	{	$('comment_captcha_img').src="/captcha/captcha.jpg?no_cache=" + Math.floor(Math.random() * 9000000 + 1000000);
        	$('comment_code_field').value = '';
        	$('comment_code_field').focus();
        }
        catch(e) {};
    },
    updateSuccessMessage: function()
    {	document.getElementById('xmsg_comment').style.display = "block";
		$('xmsg_comment').scrollTo();
    }
});

var ReportCommentSendFormValidator = Class.create(FormValidator, {
    initialize: function($super, showCaptchaField) {
        $super('reportComment_form');
        this.require({
            'comment':       getTranslation("article.form.remark")
        });
        
        if (showCaptchaField)
		{	this.require({
				'captchafield':   getTranslation("article.form.code")
			});
		}
        
        this.enableAjax();
        //alert("email:function initialize");
    },
    // this is called by sendMail.jsp respond()
    processResult: function(anchor) {
        //alert("EMAIL processing " + anchor);
        if (anchor.indexOf('captcha_failure') >= 0) {
            this.addNotification(getTranslation('video.wrong.code'));
            this.clearCaptcha();
        } else if (anchor.indexOf('captcha_sessionError') >= 0) {
            this.addNotification(getTranslation('video.invalid.code'));
            this.clearCaptcha();
        } else if (anchor.indexOf('internalError') >= 0) {
            this.addNotification(getTranslation('internalError'));
            this.clearCaptcha();
        } else if (anchor.indexOf('parameterError') >= 0) {
            this.addNotification(getTranslation('wrongParameter'));
            this.clearCaptcha();
        } else if (anchor.indexOf('parameterEmpty') >= 0) {
            this.addNotification(getTranslation('wrongParameter'));
            this.clearCaptcha();
        } else if (anchor.indexOf('error') >= 0) {
            location.href = location.pathname + anchor;
            initByHash();
        } else {
        	//alert('send email ..else ');
            //$('recommendEmail').hide();
            this.updateSuccessMessage();
            this.form.getInputs('text').invoke('clear');
            $('reportComment_comment').value = '';
            location.href = location.pathname + anchor;
            initByHash();
        }
    },
    clearCaptcha: function() {
        this.focusErrors();
        $('reportComment_captcha_img').src="/captcha/captcha.jpg?no_cache=" + Math.floor(Math.random() * 9000000 + 1000000);
        $('reportComment_code_field').value = '';
        $('reportComment_code_field').focus();
    },
    updateSuccessMessage: function() {
        $('xmsg_reportComment').show();
        $('xmsg_reportComment').scrollTo();
    }
});


/* ---- content ticker ---- */
var Ticker = {};
Ticker.toggleContent = function(contentDivId) {
    $(contentDivId).toggle();
    if (window.getCounters) getCounters();
    return false;
}

/* ---- omniture ---- */
/* SiteCatalyst code version: H.4.
Copyright 1997-2006 Omniture, Inc. More info available at
http://www.omniture.com */
/* Specify the Report Suite ID(s) to track here */
var s_account="axelspringerweltde"
var s=s_gi(s_account)
/************************** CONFIG SECTION **************************/
/* You may add or alter any code config here. */
s.charSet="ISO-8859-1"
/* Link Tracking Config */
s.trackDownloadLinks=true
s.trackExternalLinks=true
s.trackInlineStats=true
s.linkDownloadFileTypes="exe,zip,wav,mp3,mov,mpg,avi,wmv,doc,pdf,xls"
s.linkInternalFilters="javascript:,.welt.de,//welt.de,welt.parship.de,newsstand.de,welt-kompakt.de,welt.ariva.de,welt.personalmarkt.de,wams.parship.de,//wams.de,.wams.de"
s.linkLeaveQueryString=false
s.linkTrackVars="None"
s.linkTrackEvents="None"
/* Visitor-Sampling Config */
s.visitorSampling=50 /* % (0.01 - 100) */
s.visitorSamplingGroup="1"

/* Plugin Config */
s.usePlugins=true
function s_doPlugins(s) {
	/* Add calls to plugins here */
	s.setupDynamicObjectIDs();
}
s.doPlugins=s_doPlugins

/************************** PLUGINS SECTION *************************/
/*
 * Custom Plugin v0.6: Setup Dynamic Object IDs based on URL
 */
s.setupDynamicObjectIDs=new Function(""
+"var s=this;if(!s.doi){s.doi=1;if(s.apv>3&&(!s.isie||!s.ismac||s.apv"
+">=5)){if(s.wd.attachEvent)s.wd.attachEvent('onload',s.setOIDs);else"
+" if(s.wd.addEventListener)s.wd.addEventListener('load',s.setOIDs,fa"
+"lse);else{s.doiol=s.wd.onload;s.wd.onload=s.setOIDs}}s.wd.s_semapho"
+"re=1}");s.setOIDs=new Function("e",""
+"var s=s_c_il["+s._in+"],b=s.eh(s.wd,'onload'),o='onclick',l,u,c,x,i"
+",a=new Array;if(s.doiol){if(b)s[b]=s.wd[b];s.doiol(e)}if(s.d.links)"
+"{for(i=0;i<s.d.links.length;i++){l=s.d.links[i];c=l[o]?''+l[o]:'';b"
+"=s.eh(l,o);z=l[b]?''+l[b]:'';u=s.getObjectID(l);if(u&&c.indexOf('s_"
+"objectID')<0&&z.indexOf('s_objectID')<0){u=s.rep(u,'\"','').substri"
+"ng(0,97);l.s_oc=l[o];a[u]=a[u]?a[u]+1:1;x='s_objectID=\"'+u+'_'+a[u"
+"]+'\";return this.s_oc?this.s_oc(e):true';if(s.isns&&s.apv>=5)l.set"
+"Attribute(o,x);l[o]=new Function('e',x)}}}s.wd.s_semaphore=0;return"
+" true");s.getObjectID=new Function("o","var h=o.href;return h");

/* WARNING: Changing the visitor namespace will cause drastic changes
to how your visitor data is collected.  Changes should only be made
when instructed to do so by your account manager.*/
s.visitorNamespace="axelspringer"

/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
var s_objectID;function s_c2fe(f){var x='',s=0,e,a,b,c;while(1){e=
f.indexOf('"',s);b=f.indexOf('\\',s);c=f.indexOf("\n",s);if(e<0||(b>=
0&&b<e))e=b;if(e<0||(c>=0&&c<e))e=c;if(e>=0){x+=(e>s?f.substring(s,e):
'')+(e==c?'\\n':'\\'+f.substring(e,e+1));s=e+1}else return x
+f.substring(s)}return f}function s_c2fa(f){var s=f.indexOf('(')+1,e=
f.indexOf(')'),a='',c;while(s>=0&&s<e){c=f.substring(s,s+1);if(c==',')
a+='","';else if(("\n\r\t ").indexOf(c)<0)a+=c;s++}return a?'"'+a+'"':
a}function s_c2f(cc){cc=''+cc;var fc='var f=new Function(',s=
cc.indexOf(';',cc.indexOf('{')),e=cc.lastIndexOf('}'),o,a,d,q,c,f,h,x
fc+=s_c2fa(cc)+',"var s=new Object;';c=cc.substring(s+1,e);s=
c.indexOf('function');while(s>=0){d=1;q='';x=0;f=c.substring(s);a=
s_c2fa(f);e=o=c.indexOf('{',s);e++;while(d>0){h=c.substring(e,e+1);if(
q){if(h==q&&!x)q='';if(h=='\\')x=x?0:1;else x=0}else{if(h=='"'||h=="'"
)q=h;if(h=='{')d++;if(h=='}')d--}if(d>0)e++}c=c.substring(0,s)
+'new Function('+(a?a+',':'')+'"'+s_c2fe(c.substring(o+1,e))+'")'
+c.substring(e+1);s=c.indexOf('function')}fc+=s_c2fe(c)+';return s");'
eval(fc);return f}function s_gi(un,pg,ss){var c="function s_c(un,pg,s"
+"s){var s=this;s.wd=window;if(!s.wd.s_c_in){s.wd.s_c_il=new Array;s."
+"wd.s_c_in=0;}s._il=s.wd.s_c_il;s._in=s.wd.s_c_in;s._il[s._in]=s;s.w"
+"d.s_c_in++;s.m=function(m){return (''+m).indexOf('{')<0};s.fl=funct"
+"ion(x,l){return x?(''+x).substring(0,l):x};s.co=function(o){if(!o)r"
+"eturn o;var n=new Object,x;for(x in o)if(x.indexOf('select')<0&&x.i"
+"ndexOf('filter')<0)n[x]=o[x];return n};s.num=function(x){x=''+x;for"
+"(var p=0;p<x.length;p++)if(('0123456789').indexOf(x.substring(p,p+1"
+"))<0)return 0;return 1};s.rep=function(x,o,n){var i=x.indexOf(o),l="
+"n.length>0?n.length:1;while(x&&i>=0){x=x.substring(0,i)+n+x.substri"
+"ng(i+o.length);i=x.indexOf(o,i+l)}return x};s.ape=function(x){var s"
+"=this,i;x=x?s.rep(escape(''+x),'+','%2B'):x;if(x&&s.charSet&&s.em=="
+"1&&x.indexOf('%u')<0&&x.indexOf('%U')<0){i=x.indexOf('%');while(i>="
+"0){i++;if(('89ABCDEFabcdef').indexOf(x.substring(i,i+1))>=0)return "
+"x.substring(0,i)+'u00'+x.substring(i);i=x.indexOf('%',i)}}return x}"
+";s.epa=function(x){var s=this;return x?unescape(s.rep(''+x,'+',' ')"
+"):x};s.pt=function(x,d,f,a){var s=this,t=x,z=0,y,r;while(t){y=t.ind"
+"exOf(d);y=y<0?t.length:y;t=t.substring(0,y);r=s.m(f)?s[f](t,a):f(t,"
+"a);if(r)return r;z+=y+d.length;t=x.substring(z,x.length);t=z<x.leng"
+"th?t:''}return ''};s.isf=function(t,a){var c=a.indexOf(':');if(c>=0"
+")a=a.substring(0,c);if(t.substring(0,2)=='s_')t=t.substring(2);retu"
+"rn (t!=''&&t==a)};s.fsf=function(t,a){var s=this;if(s.pt(a,',','isf"
+"',t))s.fsg+=(s.fsg!=''?',':'')+t;return 0};s.fs=function(x,f){var s"
+"=this;s.fsg='';s.pt(x,',','fsf',f);return s.fsg};s.c_d='';s.c_gdf=f"
+"unction(t,a){var s=this;if(!s.num(t))return 1;return 0};s.c_gd=func"
+"tion(){var s=this,d=s.wd.location.hostname,n=s.fpCookieDomainPeriod"
+"s,p;if(!n)n=s.cookieDomainPeriods;if(d&&!s.c_d){n=n?parseInt(n):2;n"
+"=n>2?n:2;p=d.lastIndexOf('.');while(p>=0&&n>1){p=d.lastIndexOf('.',"
+"p-1);n--}s.c_d=p>0&&s.pt(d,'.','c_gdf',0)?d.substring(p):''}return "
+"s.c_d};s.c_r=function(k){var s=this;k=s.ape(k);var c=' '+s.d.cookie"
+",i=c.indexOf(' '+k+'='),e=i<0?i:c.indexOf(';',i),v=i<0?'':s.epa(c.s"
+"ubstring(i+2+k.length,e<0?c.length:e));return v!='[[B]]'?v:''};s.c_"
+"w=function(k,v,e){var s=this,d=s.c_gd(),l=s.cookieLifetime,t;v=''+v"
+";l=l?(''+l).toUpperCase():'';if(e&&l!='SESSION'&&l!='NONE'){t=(v!='"
+"'?parseInt(l?l:0):-60);if(t){e=new Date;e.setTime(e.getTime()+(t*10"
+"00))}}if(k&&l!='NONE'){s.d.cookie=k+'='+s.ape(v!=''?v:'[[B]]')+'; p"
+"ath=/;'+(e&&l!='SESSION'?' expires='+e.toGMTString()+';':'')+(d?' d"
+"omain='+d+';':'');return s.c_r(k)==v}return 0};s.eh=function(o,e,r,"
+"f){var s=this,b='s_'+e+'_'+s._in,n=-1,l,i,x;if(!s.ehl)s.ehl=new Arr"
+"ay;l=s.ehl;for(i=0;i<l.length&&n<0;i++){if(l[i].o==o&&l[i].e==e)n=i"
+"}if(n<0){n=i;l[n]=new Object}x=l[n];x.o=o;x.e=e;f=r?x.b:f;if(r||f){"
+"x.b=r?0:o[e];x.o[e]=f}if(x.b){x.o[b]=x.b;return b}return 0};s.cet=f"
+"unction(f,a,t,o,b){var s=this,r;if(s.apv>=5&&(!s.isopera||s.apv>=7)"
+")eval('try{r=s.m(f)?s[f](a):f(a)}catch(e){r=s.m(t)?s[t](e):t(e)}');"
+"else{if(s.ismac&&s.u.indexOf('MSIE 4')>=0)r=s.m(b)?s[b](a):b(a);els"
+"e{s.eh(s.wd,'onerror',0,o);r=s.m(f)?s[f](a):f(a);s.eh(s.wd,'onerror"
+"',1)}}return r};s.gtfset=function(e){var s=this;return s.tfs};s.gtf"
+"soe=new Function('e','var s=s_c_il['+s._in+'];s.eh(window,\"onerror"
+"\",1);s.etfs=1;var c=s.t();if(c)s.d.write(c);s.etfs=0;return true')"
+";s.gtfsfb=function(a){return window};s.gtfsf=function(w){var s=this"
+",p=w.parent,l=w.location;s.tfs=w;if(p&&p.location!=l&&p.location.ho"
+"st==l.host){s.tfs=p;return s.gtfsf(s.tfs)}return s.tfs};s.gtfs=func"
+"tion(){var s=this;if(!s.tfs){s.tfs=s.wd;if(!s.etfs)s.tfs=s.cet('gtf"
+"sf',s.tfs,'gtfset',s.gtfsoe,'gtfsfb')}return s.tfs};s.ca=function()"
+"{var s=this,imn='s_i_'+s.fun;if(s.d.images&&s.apv>=3&&(!s.isopera||"
+"s.apv>=7)&&(s.ns6<0||s.apv>=6.1)){s.ios=1;if(!s.d.images[imn]&&(!s."
+"isns||(s.apv<4||s.apv>=5))){s.d.write('<im'+'g name=\"'+imn+'\" hei"
+"ght=1 width=1 border=0 alt=\"\">');if(!s.d.images[imn])s.ios=0}}};s"
+".mr=function(sess,q,ta){var s=this,ns=s.visitorNamespace,unc=s.rep("
+"s.fun,'_','-'),imn='s_i_'+s.fun,im,b,e,rs='http'+(s.ssl?'s':'')+':/"
+"/'+(ns?ns:(s.ssl?'102':unc))+'.122.2O7.net/b/ss/'+s.un+'/1/H.4-pdv-"
+"2/'+sess+'?[AQB]&ndh=1'+(q?q:'')+(s.q?s.q:'')+'&[AQE]';if(s.isie&&!"
+"s.ismac){if(s.apv>5.5)rs=s.fl(rs,4095);else rs=s.fl(rs,2047)}if(s.i"
+"os){if (!s.ss)s.ca();im=s.wd[imn]?s.wd[imn]:s.d.images[imn];if(!im)"
+"im=s.wd[imn]=new Image;im.src=rs;if(rs.indexOf('&pe=')>=0&&(!ta||ta"
+"=='_self'||ta=='_top'||(s.wd.name&&ta==s.wd.name))){b=e=new Date;wh"
+"ile(e.getTime()-b.getTime()<500)e=new Date}return ''}return '<im'+'"
+"g sr'+'c=\"'+rs+'\" width=1 height=1 border=0 alt=\"\">'};s.gg=func"
+"tion(v){var s=this;return s.wd['s_'+v]};s.glf=function(t,a){if(t.su"
+"bstring(0,2)=='s_')t=t.substring(2);var s=this,v=s.gg(t);if(v)s[t]="
+"v};s.gl=function(v){var s=this;s.pt(v,',','glf',0)};s.gv=function(v"
+"){var s=this;return s['vpm_'+v]?s['vpv_'+v]:(s[v]?s[v]:'')};s.havf="
+"function(t,a){var s=this,b=t.substring(0,4),x=t.substring(4),n=pars"
+"eInt(x),k='g_'+t,m='vpm_'+t,q=t,v=s.linkTrackVars,e=s.linkTrackEven"
+"ts;s[k]=s.gv(t);if(s.lnk||s.eo){v=v?v+','+s.vl_l:'';if(v&&!s.pt(v,'"
+",','isf',t))s[k]='';if(t=='events'&&e)s[k]=s.fs(s[k],e)}s[m]=0;if(t"
+"=='pageURL')q='g';else if(t=='referrer')q='r';else if(t=='vmk')q='v"
+"mt';else if(t=='charSet'){q='ce';if(s[k]&&s.em==2)s[k]='UTF-8'}else"
+" if(t=='visitorNamespace')q='ns';else if(t=='cookieDomainPeriods')q"
+"='cdp';else if(t=='cookieLifetime')q='cl';else if(t=='variableProvi"
+"der')q='vvp';else if(t=='currencyCode')q='cc';else if(t=='channel')"
+"q='ch';else if(t=='campaign')q='v0';else if(s.num(x)) {if(b=='prop'"
+")q='c'+n;else if(b=='eVar')q='v'+n;else if(b=='hier'){q='h'+n;s[k]="
+"s.fl(s[k],255)}}if(s[k]&&t!='linkName'&&t!='linkType')s.qav+='&'+q+"
+"'='+s.ape(s[k]);return ''};s.hav=function(){var s=this;s.qav='';s.p"
+"t(s.vl_t,',','havf',0);return s.qav};s.lnf=function(t,h){t=t?t.toLo"
+"werCase():'';h=h?h.toLowerCase():'';var te=t.indexOf('=');if(t&&te>"
+"0&&h.indexOf(t.substring(te+1))>=0)return t.substring(0,te);return "
+"''};s.ln=function(h){var s=this,n=s.linkNames;if(n)return s.pt(n,',"
+"','lnf',h);return ''};s.ltdf=function(t,h){t=t?t.toLowerCase():'';h"
+"=h?h.toLowerCase():'';var qi=h.indexOf('?');h=qi>=0?h.substring(0,q"
+"i):h;if(t&&h.substring(h.length-(t.length+1))=='.'+t)return 1;retur"
+"n 0};s.ltef=function(t,h){t=t?t.toLowerCase():'';h=h?h.toLowerCase("
+"):'';if(t&&h.indexOf(t)>=0)return 1;return 0};s.lt=function(h){var "
+"s=this,lft=s.linkDownloadFileTypes,lef=s.linkExternalFilters,lif=s."
+"linkInternalFilters;lif=lif?lif:s.wd.location.hostname;h=h.toLowerC"
+"ase();if(s.trackDownloadLinks&&lft&&s.pt(lft,',','ltdf',h))return '"
+"d';if(s.trackExternalLinks&&(lef||lif)&&(!lef||s.pt(lef,',','ltef',"
+"h))&&(!lif||!s.pt(lif,',','ltef',h)))return 'e';return ''};s.lc=new"
+" Function('e','var s=s_c_il['+s._in+'],b=s.eh(this,\"onclick\");s.l"
+"nk=s.co(this);s.t();s.lnk=0;if(b)return this[b](e);return true');s."
+"bc=new Function('e','var s=s_c_il['+s._in+'],f;if(s.d&&s.d.all&&s.d"
+".all.cppXYctnr)return;s.eo=e.srcElement?e.srcElement:e.target;eval("
+"\"try{if(s.eo&&(s.eo.tagName||s.eo.parentElement||s.eo.parentNode))"
+"s.t()}catch(f){}\");s.eo=0');s.ot=function(o){var a=o.type,b=o.tagN"
+"ame;return (a&&a.toUpperCase?a:b&&b.toUpperCase?b:o.href?'A':'').to"
+"UpperCase()};s.oid=function(o){var s=this,t=s.ot(o),p=o.protocol,c="
+"o.onclick,n='',x=0;if(!o.s_oid){if(o.href&&(t=='A'||t=='AREA')&&(!c"
+"||!p||p.toLowerCase().indexOf('javascript')<0))n=o.href;else if(c){"
+"n=s.rep(s.rep(s.rep(s.rep(''+c,\"\\r\",''),\"\\n\",''),\"\\t\",''),"
+"' ','');x=2}else if(o.value&&(t=='INPUT'||t=='SUBMIT')){n=o.value;x"
+"=3}else if(o.src&&t=='IMAGE')n=o.src;if(n){o.s_oid=s.fl(n,100);o.s_"
+"oidt=x}}return o.s_oid};s.rqf=function(t,un){var s=this,e=t.indexOf"
+"('='),u=e>=0?','+t.substring(0,e)+',':'';return u&&u.indexOf(','+un"
+"+',')>=0?s.epa(t.substring(e+1)):''};s.rq=function(un){var s=this,c"
+"=un.indexOf(','),v=s.c_r('s_sq'),q='';if(c<0)return s.pt(v,'&','rqf"
+"',un);return s.pt(un,',','rq',0)};s.sqp=function(t,a){var s=this,e="
+"t.indexOf('='),q=e<0?'':s.epa(t.substring(e+1));s.sqq[q]='';if(e>=0"
+")s.pt(t.substring(0,e),',','sqs',q);return 0};s.sqs=function(un,q){"
+"var s=this;s.squ[un]=q;return 0};s.sq=function(q){var s=this,k='s_s"
+"q',v=s.c_r(k),x,c=0;s.sqq=new Object;s.squ=new Object;s.sqq[q]='';s"
+".pt(v,'&','sqp',0);s.pt(s.un,',','sqs',q);v='';for(x in s.squ)s.sqq"
+"[s.squ[x]]+=(s.sqq[s.squ[x]]?',':'')+x;for(x in s.sqq)if(x&&s.sqq[x"
+"]&&(x==q||c<2)){v+=(v?'&':'')+s.sqq[x]+'='+s.ape(x);c++}return s.c_"
+"w(k,v,0)};s.wdl=new Function('e','var s=s_c_il['+s._in+'],r=true,b="
+"s.eh(s.wd,\"onload\"),i,o,oc;if(b)r=this[b](e);for(i=0;i<s.d.links."
+"length;i++){o=s.d.links[i];oc=o.onclick?\"\"+o.onclick:\"\";if((oc."
+"indexOf(\"s_gs(\")<0||oc.indexOf(\".s_oc(\")>=0)&&oc.indexOf(\".tl("
+"\")<0)s.eh(o,\"onclick\",0,s.lc);}return r');s.wds=function(){var s"
+"=this;if(s.apv>3&&(!s.isie||!s.ismac||s.apv>=5)){if(s.b&&s.b.attach"
+"Event)s.b.attachEvent('onclick',s.bc);else if(s.b&&s.b.addEventList"
+"ener)s.b.addEventListener('click',s.bc,false);else s.eh(s.wd,'onloa"
+"d',0,s.wdl)}};s.vs=function(x){var s=this,v=s.visitorSampling,g=s.v"
+"isitorSamplingGroup,k='s_vsn_'+s.un+(g?'_'+g:''),n=s.c_r(k),e=new D"
+"ate,y=e.getYear();e.setYear(y+10+(y<1900?1900:0));if(v){v*=100;if(!"
+"n){if(!s.c_w(k,x,e))return 0;n=x}if(n%10000>v)return 0}return 1};s."
+"dyasmf=function(t,m){if(t&&m&&m.indexOf(t)>=0)return 1;return 0};s."
+"dyasf=function(t,m){var s=this,i=t?t.indexOf('='):-1,n,x;if(i>=0&&m"
+"){var n=t.substring(0,i),x=t.substring(i+1);if(s.pt(x,',','dyasmf',"
+"m))return n}return 0};s.uns=function(){var s=this,x=s.dynamicAccoun"
+"tSelection,l=s.dynamicAccountList,m=s.dynamicAccountMatch,n,i;s.un."
+"toLowerCase();if(x&&l){if(!m)m=s.wd.location.host;if(!m.toLowerCase"
+")m=''+m;l=l.toLowerCase();m=m.toLowerCase();n=s.pt(l,';','dyasf',m)"
+";if(n)s.un=n}i=s.un.indexOf(',');s.fun=i<0?s.un:s.un.substring(0,i)"
+"};s.t=function(){var s=this,trk=1,tm=new Date,sed=Math&&Math.random"
+"?Math.floor(Math.random()*10000000000000):tm.getTime(),sess='s'+Mat"
+"h.floor(tm.getTime()/10800000)%10+sed,yr=tm.getYear(),vt=tm.getDate"
+"()+'/'+tm.getMonth()+'/'+(yr<1900?yr+1900:yr)+' '+tm.getHours()+':'"
+"+tm.getMinutes()+':'+tm.getSeconds()+' '+tm.getDay()+' '+tm.getTime"
+"zoneOffset(),tfs=s.gtfs(),ta='',q='',qs='';s.uns();if(!s.q){var tl="
+"tfs.location,x='',c='',v='',p='',bw='',bh='',j='1.0',k=s.c_w('s_cc'"
+",'true',0)?'Y':'N',hp='',ct='',pn=0,ps;if(s.apv>=4)x=screen.width+'"
+"x'+screen.height;if(s.isns||s.isopera){if(s.apv>=3){j='1.1';v=s.n.j"
+"avaEnabled()?'Y':'N';if(s.apv>=4){j='1.2';c=screen.pixelDepth;bw=s."
+"wd.innerWidth;bh=s.wd.innerHeight;if(s.apv>=4.06)j='1.3'}}s.pl=s.n."
+"plugins}else if(s.isie){if(s.apv>=4){v=s.n.javaEnabled()?'Y':'N';j="
+"'1.2';c=screen.colorDepth;if(s.apv>=5){bw=s.d.documentElement.offse"
+"tWidth;bh=s.d.documentElement.offsetHeight;j='1.3';if(!s.ismac&&s.b"
+"){s.b.addBehavior('#default#homePage');hp=s.b.isHomePage(tl)?\"Y\":"
+"\"N\";s.b.addBehavior('#default#clientCaps');ct=s.b.connectionType}"
+"}}else r=''}if(s.pl)while(pn<s.pl.length&&pn<30){ps=s.fl(s.pl[pn].n"
+"ame,100)+';';if(p.indexOf(ps)<0)p+=ps;pn++}s.q=(x?'&s='+s.ape(x):''"
+")+(c?'&c='+s.ape(c):'')+(j?'&j='+j:'')+(v?'&v='+v:'')+(k?'&k='+k:''"
+")+(bw?'&bw='+bw:'')+(bh?'&bh='+bh:'')+(ct?'&ct='+s.ape(ct):'')+(hp?"
+"'&hp='+hp:'')+(p?'&p='+s.ape(p):'')}if(s.usePlugins)s.doPlugins(s);"
+"var l=s.wd.location,r=tfs.document.referrer;if(!s.pageURL)s.pageURL"
+"=s.fl(l?l:'',255);if(!s.referrer)s.referrer=s.fl(r?r:'',255);if(s.l"
+"nk||s.eo){var o=s.eo?s.eo:s.lnk;if(!o)return '';var p=s.gv('pageNam"
+"e'),w=1,t=s.ot(o),n=s.oid(o),x=o.s_oidt,h,l,i,oc;if(s.eo&&o==s.eo){"
+"while(o&&!n&&t!='BODY'){o=o.parentElement?o.parentElement:o.parentN"
+"ode;if(!o)return '';t=s.ot(o);n=s.oid(o);x=o.s_oidt}oc=o.onclick?''"
+"+o.onclick:'';if((oc.indexOf(\"s_gs(\")>=0&&oc.indexOf(\".s_oc(\")<"
+"0)||oc.indexOf(\".tl(\")>=0)return ''}ta=o.target;h=o.href?o.href:'"
+"';i=h.indexOf('?');h=s.linkLeaveQueryString||i<0?h:h.substring(0,i)"
+";l=s.linkName?s.linkName:s.ln(h);t=s.linkType?s.linkType.toLowerCas"
+"e():s.lt(h);if(t&&(h||l))q+='&pe=lnk_'+(t=='d'||t=='e'?s.ape(t):'o'"
+")+(h?'&pev1='+s.ape(h):'')+(l?'&pev2='+s.ape(l):'');else trk=0;if(s"
+".trackInlineStats){if(!p){p=s.gv('pageURL');w=0}t=s.ot(o);i=o.sourc"
+"eIndex;if(s.gg('objectID')){n=s.gg('objectID');x=1;i=1}if(p&&n&&t)q"
+"s='&pid='+s.ape(s.fl(p,255))+(w?'&pidt='+w:'')+'&oid='+s.ape(s.fl(n"
+",100))+(x?'&oidt='+x:'')+'&ot='+s.ape(t)+(i?'&oi='+i:'')}}if(!trk&&"
+"!qs)return '';var code='';if(trk&&s.vs(sed))code=s.mr(sess,(vt?'&t="
+"'+s.ape(vt):'')+s.hav()+q+(qs?qs:s.rq(s.un)),ta);s.sq(trk?'':qs);s."
+"lnk=s.eo=s.linkName=s.linkType=s.wd.s_objectID='';return code};s.tl"
+"=function(o,t,n){var s=this;s.lnk=s.co(o);s.linkType=t;s.linkName=n"
+";s.t()};s.ssl=(s.wd.location.protocol.toLowerCase().indexOf('https'"
+")>=0);s.d=document;s.b=s.d.body;s.n=navigator;s.u=s.n.userAgent;s.n"
+"s6=s.u.indexOf('Netscape6/');var apn=s.n.appName,v=s.n.appVersion,i"
+"e=v.indexOf('MSIE '),o=s.u.indexOf('Opera '),i;if(v.indexOf('Opera'"
+")>=0||o>0)apn='Opera';s.isie=(apn=='Microsoft Internet Explorer');s"
+".isns=(apn=='Netscape');s.isopera=(apn=='Opera');s.ismac=(s.u.index"
+"Of('Mac')>=0);if(o>0)s.apv=parseFloat(s.u.substring(o+6));else if(i"
+"e>0){s.apv=parseInt(i=v.substring(ie+5));if(s.apv>3)s.apv=parseFloa"
+"t(i)}else if(s.ns6>0)s.apv=parseFloat(s.u.substring(s.ns6+10));else"
+" s.apv=parseFloat(v);s.em=0;if(String.fromCharCode){i=escape(String"
+".fromCharCode(256)).toUpperCase();s.em=(i=='%C4%80'?2:(i=='%U0100'?"
+"1:0))}s.un=un;s.uns();s.vl_l='vmk,charSet,visitorNamespace,cookieDo"
+"mainPeriods,cookieLifetime,pageName,pageURL,referrer,currencyCode,p"
+"urchaseID';s.vl_t=s.vl_l+',variableProvider,channel,server,pageType"
+",campaign,state,zip,events,products,linkName,linkType';for(var n=1;"
+"n<51;n++)s.vl_t+=',prop'+n+',eVar'+n+',hier'+n;s.vl_g=s.vl_t+',trac"
+"kDownloadLinks,trackExternalLinks,trackInlineStats,linkLeaveQuerySt"
+"ring,linkDownloadFileTypes,linkExternalFilters,linkInternalFilters,"
+"linkNames';if(pg)s.gl(s.vl_g);s.ss=ss;if(!ss){s.wds();s.ca()}}",
l=window.s_c_il,n=navigator,u=n.userAgent,v=n.appVersion,e=v.indexOf(
'MSIE '),m=u.indexOf('Netscape6/'),a,i,s;if(l)for(i=0;i<l.length;i++){
s=l[i];s.uns();if(s.un==un)return s;else if(s.pt(s.un,',','isf',un)){
s=s.co(s);s.un=un;s.uns();return s}}if(e>0){a=parseInt(i=v.substring(e
+5));if(a>3)a=parseFloat(i)}else if(m>0)a=parseFloat(u.substring(m+10)
);else a=parseFloat(v);if(a>=5&&v.indexOf('Opera')<0&&u.indexOf(
'Opera')<0){eval(c);return new s_c(un,pg,ss)}else s=s_c2f(c);return s(
un,pg,ss)}


/* ---- sso ---- */
/* general functions to split an url
 * http://www.eggheadcafe.com/articles/20020107.asp
*/
function PageQuery(q)
{	if (q.length > 1) 
	{	this.q = q.substring(1, q.length);
	}
	else
	{	this.q = null;
	}

	this.keyValuePairs = new Array();
	if (q)
	{	for (var i=0; i < this.q.split("&").length; i++)
		{	this.keyValuePairs[i] = this.q.split("&")[i];
		}
	}
	
	this.getKeyValuePairs = function()
	{	return this.keyValuePairs;
	}

	this.getValue = function(s)
	{	for (var j=0; j < this.keyValuePairs.length; j++)
		{	if (this.keyValuePairs[j].split("=")[0] == s)
			{	return this.keyValuePairs[j].split("=")[1];
			}
		}
		return false;
	}

	this.getParameters = function()
	{	var a = new Array(this.getLength());
		
		for (var j=0; j < this.keyValuePairs.length; j++)
		{	a[j] = this.keyValuePairs[j].split("=")[0];
		}
		return a;
	}
	
	this.getLength = function()
	{	return this.keyValuePairs.length;
	}
}

/*
 * Functions concerned to SSO (Single Sign On)
*/
function clearSSO()
{	eraseCookieDomainless("SSOSession");
	eraseCookieDomainless("SSOId");
}

var globaleSsoShowCurrentSSOUserInited = false;
var globalSsoUser;
var globalSsoMail;
var gloablSsoNewsLetter;

function ssoInit()
{	showCurrentSSOUser(globalSsoUser, globalSsoMail);

	// news article comments
	newsCommentUserData(globalSsoUser, globalSsoMail);
	
	// video comments
	videoCommentUserData(globalSsoUser, globalSsoMail);
}

// get infos from SSO using session id cookie
// if the session is expired in sso the return string is empty and the session cookie is deleted in won
// see http://www.prototypejs.org/learn/json
// see http://www.prototypejs.org/learn/introduction-to-ajax
function getSSOInfo()
{	var query = new PageQuery(window.location.search);
	var value = query.getValue("sessionid");
	var sessionId;

	// is there a query parameter ?sessionid=....
	// the user was redirected from SSO after login
	if (value != false)
	{	sessionId = value; 
		clearSSO();
		createCookieDomainless("SSOSession", sessionId);
	}
	else
	{	sessionId = readCookie("SSOSession");
	}

	if (sessionId)
	{	// route to wfAjax.jsp by type=...&service=Ajax
		var url = "?type=getCurrentSSOInfo&id=" + sessionId + "&service=Ajax";

		SSOUser = undefined;
		SSOEmail = undefined;
	
		new Ajax.Request(url,
		{	method: 'get',
			onSuccess: function(transport)
						{ 	var response = transport.responseText;
							var ok = false;
//alert("Response " + response);
							
							if (response && response.length > 0)
							{	try
								{	// true - parse strong, checks the string for possible malicious attempts
									var data = response.evalJSON(true);
//alert(response + " => username: " + data.username + ", email: " + data.email + ", id: " + data.id + " newsletter: " + data.newsLetterURL);
									SSOUser = data.username;
									SSOEmail = data.email;
									SSOId = data.id;
									SSONewsletter = data.newsLetterURL;
									
									ok = SSOUser != undefined && SSOEmail != undefined
											&& SSOId != undefined
											&& SSOUser.length > 0 && SSOEmail.length > 0
											&& SSOId.length > 0;

								}
								catch(e)
								{	// alert(e);
								}
							}

							if (ok)
							{	// the SSO id is later on used (for storing user id within comments)
								// so we rely on the fact 
								// that this asynchronous thread is finished before posting a comment
								createCookieDomainless("SSOId", SSOId);
								
								globalSsoUser = SSOUser;
								globalSsoMail = SSOEmail;
								gloablSsoNewsLetter = SSONewsletter;
								
//alert("SSO Session " + readCookie("SSOSession"));
								// navigation - logon/logoff
								showCurrentSSOUser(SSOUser, SSONewsletter);
							}
							else
							{	showCurrentSSOUser();
								clearSSO();
							}
						},
			onFailure: function()
						{ 	clearSSO();
							showCurrentSSOUser();
							//alert("Fehler");
						}
		});
	}
	else
	{	showCurrentSSOUser();
	}
}

function getSSOUserId()
{	return readCookie("SSOId");	
}

// show login if not logged in
// show logout if logged in
function showCurrentSSOUser(ssoUsername, ssoNewsletter)
{	
	if (globaleSsoShowCurrentSSOUserInited)
	{	return;
	}
	
	// HM: don't check existence of tags
	// => exception: we know the "fast" load (/welt/template/ver1-3/navigation/header.jsp) did not work
	//	second try: the lazy load (after DOM was constructed completely) should work
	try
	{	ssoLoginTag = document.getElementById("ssoLogin");
		ssoLoggedInTag = document.getElementById("ssoLoggedIn");
		ssoUsernameTag = document.getElementById("ssoCurrentUser");
		ssoNewsLetterTag = document.getElementById("ssoNewsLetter");
		
		if (ssoUsername) // already logged in
		{	ssoUsernameTag.innerHTML = ssoUsername;
			ssoLoginTag.style.display = "none";
			ssoLoggedInTag.style.display = "inline"; // due to Safari
		}
		else // not logged  in
		{	ssoLoginTag.style.display = "inline"; // due to Safari
			ssoLoggedInTag.style.display = "none";
		}
		
		if (ssoNewsletter)
		{	ssoNewsLetterTag.href = ssoNewsletter;
		}
		
		globaleSsoShowCurrentSSOUserInited = true;
	}
	catch(e)
	{	//alert(e);
	}
}

// show user name and mail for logged in user in article comments
function newsCommentUserData(ssoUsername, ssoMail)
{	try
	{	if (ssoUsername)
		{	// user name
			ssoUserTag = document.getElementById("nameBox");
			if (ssoUserTag)
			{	ssoUserTag.value = ssoUsername;
				ssoUserTag.readOnly = true;	
			}
		
			// mail box
			ssoMailTag = document.getElementById("mailBox");
			if (ssoMail && ssoMailTag)
			{	ssoMailTag.value = ssoMail;
				ssoMailTag.readOnly = true;	
			}
			
			// hide captcha
			ssoCaptchaTag = document.getElementById("captchaWrapper");
			if (ssoCaptchaTag)
			{	ssoCaptchaTag.style.display = "none";
			}
		}
	}
	catch (e)
	{	//alert(e);
	}
}

function videoCommentUserData(ssoUsername, ssoMail)
{	try
	{	if (ssoUsername)
		{	// user name
			ssoUserTag = document.getElementById("videoUserName");
			if (ssoUserTag)
			{	ssoUserTag.value = ssoUsername;
				ssoUserTag.readOnly = true;	
			}
		
			// mail box
			ssoMailTag = document.getElementById("videoUserMail");
			if (ssoMail && ssoMailTag)
			{	ssoMailTag.value = ssoMail;
				ssoMailTag.readOnly = true;	
			}
			
			ssoCaptchaBox = document.getElementById("videoCaptchaBox");
			if (ssoCaptchaBox)
			{	ssoCaptchaBox.style.display = "none";				
			}
		}
	}
	catch (e)
	{	//alert(e);
	}
}

/* ASMS AJAX */

function AsmsAjax() {}
AsmsAjax.xmlHttp = new Array();
AsmsAjax.xmlHttpScript = new Array();
AsmsAjax.requestScheduledId = new Array();
AsmsAjax.requestScheduledScript = new Array();
AsmsAjax.requestScheduled = new Array();


/*
Run an Ajax request.
dstdiv: the div-id to place result in. if it starts with "js_"-prefix, only the given JavaScript is processed
url: the request url.
script: the script to run after result, default: none
method: GET or POST, default: GET
*/ 
AsmsAjax.runRequest = function( dstdiv, url, script, method )
{
  if( typeof(AsmsAjax.xmlHttp[dstdiv]) != "undefined" && AsmsAjax.xmlHttp[dstdiv] !== null )
    return;
  if( AsmsAjax.requestScheduled[dstdiv] === true )
  {
    AsmsAjax.requestScheduled[dstdiv] = false;
    script = AsmsAjax.requestScheduledScript[dstdiv];
  }

  AsmsAjax.xmlHttp[dstdiv] = AsmsAjax.GetXmlHttpObject();
  if( AsmsAjax.xmlHttp[dstdiv] == null )
    return;
  AsmsAjax.xmlHttpScript[dstdiv] = script;
  
  if( url.indexOf('service=') == -1 )
	url = url + (url.indexOf('?') == -1 ? "?" : "&") + "service=Ajax";

  var params = null;
  if( method && method.search(/post/i) != -1 )
  {
    params = url.split('?',2);
    if( params )
    {
      url = params[0];
      params = params.length > 1 ? params[1] : "";
    }
  }
	
  AsmsAjax.xmlHttp[dstdiv].onreadystatechange = function(){AsmsAjax.showRequest(dstdiv)};
  AsmsAjax.xmlHttp[dstdiv].open( (method ? method : "GET"), url, true );

  if( params !== null )
  {
    AsmsAjax.xmlHttp[dstdiv].setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    AsmsAjax.xmlHttp[dstdiv].setRequestHeader("Content-length", params.length);
    AsmsAjax.xmlHttp[dstdiv].setRequestHeader("Connection", "close");
  }
  
  AsmsAjax.xmlHttp[dstdiv].send( params !== null ? params : null );
} // runRequest



/*
Run an Ajax request after a duration and automatically start again
dstdiv: see runRequest()
url: see runRequest()
duration: time in millsec
*/
AsmsAjax.runRefresh = function( dstdiv, url, duration )
{
    AsmsAjax.scheduleRequest( dstdiv, url, duration, "runRefresh('"+dstdiv+"','"+url+"',"+duration+")" );
} // runRefresh



/*
Run an Ajax request after a duration
dstdiv: see runRequest()
url: see runRequest()
duration: time in millsec
script: see runRequest()
*/
AsmsAjax.scheduleRequest = function( dstdiv, url, duration, script )
{
  if( AsmsAjax.requestScheduled[dstdiv] === true )
    return;
  AsmsAjax.requestScheduled[dstdiv] = true;
  AsmsAjax.requestScheduledScript[dstdiv] = script;
  AsmsAjax.requestScheduledId[dstdiv] = setTimeout( "runRequest('"+dstdiv+"','"+url+"')", duration );
} // scheduleRequest



/*
Stop a scheduled Ajax request
dstdiv: see runRequest()
script: set the script to run if a request has already been sent, default: no script is run
*/
AsmsAjax.stopRequest = function( dstdiv, script )
{
  if( AsmsAjax.requestScheduled[dstdiv] === true )
    AsmsAjax.requestScheduledScript[dstdiv] = script;
  if( AsmsAjax.xmlHttp[dstdiv] )
    AsmsAjax.xmlHttpScript[dstdiv] = script;
} // scheduleRequest



/*
Post a form via Ajax
dstdiv: see runRequest()
formEl: the element of the form
script: see runRequest()
*/
AsmsAjax.runFormRequest = function( dstdiv, formEl, script )
{
	var urlAttr = formEl.attributes.getNamedItem('action');
	var methodAttr = formEl.attributes.getNamedItem('method');
	var url = (urlAttr ? urlAttr.value : "");
	var method = (methodAttr ? methodAttr.value : "");
	var params = "";
	for( var childElNo = 0; childElNo < formEl.elements.length; childElNo++ )
	{
		childEl = formEl.elements[childElNo];
		if( childEl.name )
		{
			var value = "";
			if( childEl.type == 'radio' )
			{
				if( childEl.checked )
					value = childEl.value;
				else
					value = null;
			}
			else
			if( childEl.type == 'checkbox' )
			{
				if( childEl.checked )
					value = childEl.value;
			}
			else
			{
				value = childEl.value;
			}
			
			if( value !== null )
			{
				params += (params.length == 0 ? "?" : "&");
				params += encodeURIComponent( childEl.name ) + "=" + encodeURIComponent( value );
			}
		}
	}
	url += params;
	AsmsAjax.runRequest( dstdiv, url, script, method );
} // runFormRequest



/* processes the result of an Ajax request */
AsmsAjax.showRequest = function( dstdiv )
{
  if( AsmsAjax.xmlHttp[dstdiv] == null )
    return;
    
  if( AsmsAjax.xmlHttp[dstdiv].readyState==4 || AsmsAjax.xmlHttp[dstdiv].readyState=="complete" )
  {
	if( AsmsAjax.xmlHttp[dstdiv].status == 200 )
  	{
	  	var responseText = AsmsAjax.xmlHttp[dstdiv].responseText;
	  	if( responseText !== null && dstdiv.indexOf("js_") !== 0 && !AsmsAjax.xmlHttp[dstdiv].responseIsFailure )
	    	document.getElementById( dstdiv ).innerHTML = AsmsAjax.xmlHttp[dstdiv].responseText;
	    script = AsmsAjax.xmlHttpScript[dstdiv];
	    if( typeof(script) != "undefined" && script != null )
	      eval( script );
	}
    AsmsAjax.xmlHttp[dstdiv] = null;
  }
} // showRequest



/* creates an Ajax request object */
AsmsAjax.GetXmlHttpObject = function()
{
  var objXMLHttp=null
  
  if (window.XMLHttpRequest)
  {
    objXMLHttp=new XMLHttpRequest()
  }
  else if (window.ActiveXObject)
  {
    objXMLHttp=new ActiveXObject("Microsoft.XMLHTTP")
  }
  return objXMLHttp
}


/* newsticker module functions */
function NewsTickerModule(newstickerId, pageSize) {
    if( newstickerId != undefined ) {
        this._initialize(newstickerId, pageSize);
        NewsTickerModule._objects[newstickerId] = this;
    }
}

NewsTickerModule.next = function(newstickerId) {
    NewsTickerModule._objects[newstickerId].nextPage();
}

NewsTickerModule.previous = function(newstickerId) {
    NewsTickerModule._objects[newstickerId].previousPage();
}

NewsTickerModule._objects = new Array(); // array of newsticker modules

NewsTickerModule.prototype = {
    _newstickerId: null,
    _pageSize: 6,
    _currentPosition: 0,

    _initialize: function(newstickerId, pageSize) {
        this._newstickerId = newstickerId;
        this._pageSize = pageSize;
    },
    
    _getItem: function(position) {
        return document.getElementById("newsticker" + this._newstickerId + "_" + position);
    },
    
    _update: function() {
        for(var pos = 0; pos < 1000; pos++) {
            var item = this._getItem(pos);
            if(item == null) {
                break;
            } else {
                var display = (pos >= this._currentPosition && pos < this._currentPosition + this._pageSize ? "block" : "none");
                item.style.display = display;
            }
        }
    },
    
    nextPage: function() {
        var newPos = this._currentPosition + this._pageSize;
        var isValidItem = this._getItem(newPos) != null;
        if(isValidItem) {
            this._currentPosition = newPos;
            this._update();
            getCounters("newsticker_next");
        }
    },
    
    previousPage: function() {
        var newPos = this._currentPosition - this._pageSize;
        if(newPos >= 0) {
            this._currentPosition = newPos;
            this._update();
            getCounters("newsticker_previous");
        }
    }
}


/* newsticker overview functions */
NewsTickerOverview = {
    _calendarPosition: 0,           // scrolling position of the calendar
    _calendarSelected: 0,           // index of selected calendar day
    _calendarDate: null,            // the date parameter for selected calendar day
    _timeStamp: null,               // defined timestamp for refreshed contents
    _articleUrl: null,              // url of the currently shown news article 
    _articleId: 0,                  // id of the currently shown news article
    _overviewUrl: null,             // url to the overview article
    _articlePage: 0,                // page number in current news article
    _pageHeight: 0,                 // height of the news article page
    _pageCount: 0,                  // number of pages in the current news article
    _pageMargin: 15,                // margin for paging, to repeat last line in article
    _articleHeight: '280px',        // initial height of the article view
    _contentHeight: '250px',        // initial height of the arrticle content
    _articlePaginationActive: false,
    
    _counterTypeLoadArticle: "newsticker_article_show",
    _counterTypeSelectNewsticker: "newsticker_select",
    _counterTypeCalendarSelect: "newsticker_calendar",
    _counterTypeRefresh: "newsticker_refresh",
    _counterTypeSearch: "newsticker_search",
    _counterTypePreviousArticlePage: "newsticker_article_previous",
    _counterTypeNextArticlePage: "newsticker_article_next",

    // must be called on startup
    initialize: function(overviewUrl, articleUrl, articleId, articlePaginationActive) {
        this._articleUrl = articleUrl;
        this._articleId = articleId;
        this._overviewUrl = overviewUrl;
        this._articlePaginationActive = articlePaginationActive;
        this._initializeArticlePage();
    },

    // loads a new news article into view
    loadArticle: function(articleUrl, articleId) {
        this._articleUrl = articleUrl;
        this._articleId = articleId;
        AsmsAjax.runRequest("nt_article", articleUrl, "NewsTickerOverview._initializeArticlePage('"+this._counterTypeLoadArticle+"')");
        return false;
    },

    // loads the selected newsticker into view
    selectNewsticker: function() {
        this._updateNewsticker(this._counterTypeSelectNewsticker);
        return false;
    },
    
    // updates the current newsticker view
    _updateNewsticker: function(counterType) {
        var url = document.getElementById("nt_select").value;
        if(this._calendarDate) {
            url = appendUrlParameter(url, "date", this._calendarDate);
        }
        if(this._timeStamp) {
            url = appendUrlParameter(url, "ts", this._timeStamp);
        }
        AsmsAjax.runRequest("nt_mainbox", url, "NewsTickerOverview._initializeArticlePage('"+counterType+"')");
    },
    
    // scrolls the calendar to the next day
    calendarNext: function() {
        var newPos = this._calendarPosition - 1;
        if(newPos >= 0) {
            this._calendarPosition = newPos;
            this._updateCalendar();
        }
        return false;
    },
    
    // scrolls the calendar to the previous day
    calendarPrevious: function() {
        var newPos = this._calendarPosition + 1;
        if(newPos <= 23) {
            this._calendarPosition = newPos;
            this._updateCalendar();
        }
        return false;
    },
    
    // updates the calendar view with the selected day
    _updateCalendar: function() {
        var start = this._calendarPosition > 0 ? this._calendarPosition-1 : 0;
        var end = this._calendarPosition < 23 ? this._calendarPosition+7 : 29;
        for(var pos = start; pos <= end; pos++) {
            var item = document.getElementById("nt_cday" + pos);
            if(item != null) {
                var display = (pos >= this._calendarPosition && pos < this._calendarPosition + 7 ? "block" : "none");
                item.style.display = display;
            }
        }
    },
    
    // selects the given day index in the calendar
    calendarSelect: function(day, date) {
        this._updateCalendarSelection(this._calendarSelected, day);
        this._calendarSelected = day;
        this._calendarDate = date;
        this._updateNewsticker(this._counterTypeCalendarSelect);
        return false;
    },
    
    // updates the given calendar selection
    _updateCalendarSelection: function(oldDay, newDay) {
        document.getElementById("nt_cday" + oldDay).className = "nt-cnorm";
        document.getElementById("nt_cday" + newDay).className = "nt-csel";
    },
    
    // reloads the current newsticker view with a timestamp to overide cache
    refresh: function() {
        var date = new Date();
        this._timeStamp = date.getTime();
        this._updateNewsticker(this._counterTypeRefresh);
        return false;
    },
    
    // opens the print window for the current news article
    print: function() {
        openPrintWindow(this._articleUrl);
        return false;
    },
    
    // opens a popup window with this overview
    popup: function() {
        window.open( 
            this._overviewUrl + "?service=NewstickerPopup",
            'newsticker',
            'menubar=no,toolbar=no,status=no,width=496,height=690,scrollbars=no,resizable=no'
            );
        return false;
    },

    // performs a user search
    search: function(form) {
        AsmsAjax.runFormRequest("nt_mainbox", form, "NewsTickerOverview._initializeArticlePage('"+this._counterTypeSearch+"')");
        return false;
    },
    
    // shows the previous page of the current news article
    previousArticlePage: function() {
        this.setArticlePage(this._articlePage - 1, this._counterTypePreviousArticlePage);
        return false;
    },
    
    // shows the next page of the current news article
    nextArticlePage: function() {
        this.setArticlePage(this._articlePage + 1, this._counterTypeNextArticlePage);
        return false;
    },
    
    // sets the page of the current news article
    setArticlePage: function(page, counterType) {
        if(page >= 0 && page < this._pageCount) {
            this._articlePage = page;
            var pos = page * (this._pageHeight - this._pageMargin);
            $("nt_articlecontent").setStyle({top:-pos+"px"});
            $("nt_articlepagecurrent").innerHTML = page + 1;
            if(counterType) {
                getCounters(counterType);
            }
        }
    },
    
    // initializes paging and image popup functionality for the current news article
    _initializeArticlePage: function(counterType) {
        var articleContainer = $("nt_articlecontainer");
        if(articleContainer) {
            // initialize modal layer popup for image
            var imageLink = "modal_link_" + this._articleId;
            if(document.getElementById(imageLink)) {
                new Control.Modal(imageLink, 
                    {'width':494, afterOpen: function(){getCounters('articleZoomImage','articleID_'+this._articleId)} });
            }
            
            if (this._articlePaginationActive) {
                // initialize page height
                $("nt_article").setStyle({height:this._articleHeight});
                $("nt_articlecontainer").setStyle({height:this._contentHeight});
                // initialize pagination
                this._pageHeight = articleContainer.getHeight();
                var articleHeight = $("nt_articlecontent").getHeight();
                if(this._pageHeight >= articleHeight) {
                    this._pageCount = 1;
                } else {
                    this._pageCount = parseInt((articleHeight / (this._pageHeight - this._pageMargin)) + 1);
                }
                if(this._pageCount > 1) {
                    $("nt_articlepagination").show();
                    $("nt_articlepagecount").innerHTML = this._pageCount;
                    this.setArticlePage(0);
                } else {
                    $("nt_articlepagination").hide();
                }
            }
        }
        if(counterType) {
            getCounters(counterType);
        }
    }
}

/* Themen-Alarm */
function openThemenAlarmLogin(url) {
    window.open( 
        url,
        'themenalarm',
        'menubar=no,toolbar=no,status=no,width=805,height=565,scrollbars=yes,resizable=no'
        );
    
    return false;
}


/* ---- asd.tv video ---- */
/*
 * jQuery JavaScript Library v1.3.2
 * http://jquery.com/
 *
 * Copyright (c) 2009 John Resig
 * Dual licensed under the MIT and GPL licenses.
 * http://docs.jquery.com/License
 *
 * Date: 2009-02-19 17:34:21 -0500 (Thu, 19 Feb 2009)
 * Revision: 6246
 */
(function(){var l=this,g,y=l.jQuery,p=l.$,o=l.jQuery=l.$=function(E,F){return new o.fn.init(E,F)},D=/^[^<]*(<(.|\s)+>)[^>]*$|^#([\w-]+)$/,f=/^.[^:#\[\.,]*$/;o.fn=o.prototype={init:function(E,H){E=E||document;if(E.nodeType){this[0]=E;this.length=1;this.context=E;return this}if(typeof E==="string"){var G=D.exec(E);if(G&&(G[1]||!H)){if(G[1]){E=o.clean([G[1]],H)}else{var I=document.getElementById(G[3]);if(I&&I.id!=G[3]){return o().find(E)}var F=o(I||[]);F.context=document;F.selector=E;return F}}else{return o(H).find(E)}}else{if(o.isFunction(E)){return o(document).ready(E)}}if(E.selector&&E.context){this.selector=E.selector;this.context=E.context}return this.setArray(o.isArray(E)?E:o.makeArray(E))},selector:"",jquery:"1.3.2",size:function(){return this.length},get:function(E){return E===g?Array.prototype.slice.call(this):this[E]},pushStack:function(F,H,E){var G=o(F);G.prevObject=this;G.context=this.context;if(H==="find"){G.selector=this.selector+(this.selector?" ":"")+E}else{if(H){G.selector=this.selector+"."+H+"("+E+")"}}return G},setArray:function(E){this.length=0;Array.prototype.push.apply(this,E);return this},each:function(F,E){return o.each(this,F,E)},index:function(E){return o.inArray(E&&E.jquery?E[0]:E,this)},attr:function(F,H,G){var E=F;if(typeof F==="string"){if(H===g){return this[0]&&o[G||"attr"](this[0],F)}else{E={};E[F]=H}}return this.each(function(I){for(F in E){o.attr(G?this.style:this,F,o.prop(this,E[F],G,I,F))}})},css:function(E,F){if((E=="width"||E=="height")&&parseFloat(F)<0){F=g}return this.attr(E,F,"curCSS")},text:function(F){if(typeof F!=="object"&&F!=null){return this.empty().append((this[0]&&this[0].ownerDocument||document).createTextNode(F))}var E="";o.each(F||this,function(){o.each(this.childNodes,function(){if(this.nodeType!=8){E+=this.nodeType!=1?this.nodeValue:o.fn.text([this])}})});return E},wrapAll:function(E){if(this[0]){var F=o(E,this[0].ownerDocument).clone();if(this[0].parentNode){F.insertBefore(this[0])}F.map(function(){var G=this;while(G.firstChild){G=G.firstChild}return G}).append(this)}return this},wrapInner:function(E){return this.each(function(){o(this).contents().wrapAll(E)})},wrap:function(E){return this.each(function(){o(this).wrapAll(E)})},append:function(){return this.domManip(arguments,true,function(E){if(this.nodeType==1){this.appendChild(E)}})},prepend:function(){return this.domManip(arguments,true,function(E){if(this.nodeType==1){this.insertBefore(E,this.firstChild)}})},before:function(){return this.domManip(arguments,false,function(E){this.parentNode.insertBefore(E,this)})},after:function(){return this.domManip(arguments,false,function(E){this.parentNode.insertBefore(E,this.nextSibling)})},end:function(){return this.prevObject||o([])},push:[].push,sort:[].sort,splice:[].splice,find:function(E){if(this.length===1){var F=this.pushStack([],"find",E);F.length=0;o.find(E,this[0],F);return F}else{return this.pushStack(o.unique(o.map(this,function(G){return o.find(E,G)})),"find",E)}},clone:function(G){var E=this.map(function(){if(!o.support.noCloneEvent&&!o.isXMLDoc(this)){var I=this.outerHTML;if(!I){var J=this.ownerDocument.createElement("div");J.appendChild(this.cloneNode(true));I=J.innerHTML}return o.clean([I.replace(/ jQuery\d+="(?:\d+|null)"/g,"").replace(/^\s*/,"")])[0]}else{return this.cloneNode(true)}});if(G===true){var H=this.find("*").andSelf(),F=0;E.find("*").andSelf().each(function(){if(this.nodeName!==H[F].nodeName){return}var I=o.data(H[F],"events");for(var K in I){for(var J in I[K]){o.event.add(this,K,I[K][J],I[K][J].data)}}F++})}return E},filter:function(E){return this.pushStack(o.isFunction(E)&&o.grep(this,function(G,F){return E.call(G,F)})||o.multiFilter(E,o.grep(this,function(F){return F.nodeType===1})),"filter",E)},closest:function(E){var G=o.expr.match.POS.test(E)?o(E):null,F=0;return this.map(function(){var H=this;while(H&&H.ownerDocument){if(G?G.index(H)>-1:o(H).is(E)){o.data(H,"closest",F);return H}H=H.parentNode;F++}})},not:function(E){if(typeof E==="string"){if(f.test(E)){return this.pushStack(o.multiFilter(E,this,true),"not",E)}else{E=o.multiFilter(E,this)}}var F=E.length&&E[E.length-1]!==g&&!E.nodeType;return this.filter(function(){return F?o.inArray(this,E)<0:this!=E})},add:function(E){return this.pushStack(o.unique(o.merge(this.get(),typeof E==="string"?o(E):o.makeArray(E))))},is:function(E){return !!E&&o.multiFilter(E,this).length>0},hasClass:function(E){return !!E&&this.is("."+E)},val:function(K){if(K===g){var E=this[0];if(E){if(o.nodeName(E,"option")){return(E.attributes.value||{}).specified?E.value:E.text}if(o.nodeName(E,"select")){var I=E.selectedIndex,L=[],M=E.options,H=E.type=="select-one";if(I<0){return null}for(var F=H?I:0,J=H?I+1:M.length;F<J;F++){var G=M[F];if(G.selected){K=o(G).val();if(H){return K}L.push(K)}}return L}return(E.value||"").replace(/\r/g,"")}return g}if(typeof K==="number"){K+=""}return this.each(function(){if(this.nodeType!=1){return}if(o.isArray(K)&&/radio|checkbox/.test(this.type)){this.checked=(o.inArray(this.value,K)>=0||o.inArray(this.name,K)>=0)}else{if(o.nodeName(this,"select")){var N=o.makeArray(K);o("option",this).each(function(){this.selected=(o.inArray(this.value,N)>=0||o.inArray(this.text,N)>=0)});if(!N.length){this.selectedIndex=-1}}else{this.value=K}}})},html:function(E){return E===g?(this[0]?this[0].innerHTML.replace(/ jQuery\d+="(?:\d+|null)"/g,""):null):this.empty().append(E)},replaceWith:function(E){return this.after(E).remove()},eq:function(E){return this.slice(E,+E+1)},slice:function(){return this.pushStack(Array.prototype.slice.apply(this,arguments),"slice",Array.prototype.slice.call(arguments).join(","))},map:function(E){return this.pushStack(o.map(this,function(G,F){return E.call(G,F,G)}))},andSelf:function(){return this.add(this.prevObject)},domManip:function(J,M,L){if(this[0]){var I=(this[0].ownerDocument||this[0]).createDocumentFragment(),F=o.clean(J,(this[0].ownerDocument||this[0]),I),H=I.firstChild;if(H){for(var G=0,E=this.length;G<E;G++){L.call(K(this[G],H),this.length>1||G>0?I.cloneNode(true):I)}}if(F){o.each(F,z)}}return this;function K(N,O){return M&&o.nodeName(N,"table")&&o.nodeName(O,"tr")?(N.getElementsByTagName("tbody")[0]||N.appendChild(N.ownerDocument.createElement("tbody"))):N}}};o.fn.init.prototype=o.fn;function z(E,F){if(F.src){o.ajax({url:F.src,async:false,dataType:"script"})}else{o.globalEval(F.text||F.textContent||F.innerHTML||"")}if(F.parentNode){F.parentNode.removeChild(F)}}function e(){return +new Date}o.extend=o.fn.extend=function(){var J=arguments[0]||{},H=1,I=arguments.length,E=false,G;if(typeof J==="boolean"){E=J;J=arguments[1]||{};H=2}if(typeof J!=="object"&&!o.isFunction(J)){J={}}if(I==H){J=this;--H}for(;H<I;H++){if((G=arguments[H])!=null){for(var F in G){var K=J[F],L=G[F];if(J===L){continue}if(E&&L&&typeof L==="object"&&!L.nodeType){J[F]=o.extend(E,K||(L.length!=null?[]:{}),L)}else{if(L!==g){J[F]=L}}}}}return J};var b=/z-?index|font-?weight|opacity|zoom|line-?height/i,q=document.defaultView||{},s=Object.prototype.toString;o.extend({noConflict:function(E){l.$=p;if(E){l.jQuery=y}return o},isFunction:function(E){return s.call(E)==="[object Function]"},isArray:function(E){return s.call(E)==="[object Array]"},isXMLDoc:function(E){return E.nodeType===9&&E.documentElement.nodeName!=="HTML"||!!E.ownerDocument&&o.isXMLDoc(E.ownerDocument)},globalEval:function(G){if(G&&/\S/.test(G)){var F=document.getElementsByTagName("head")[0]||document.documentElement,E=document.createElement("script");E.type="text/javascript";if(o.support.scriptEval){E.appendChild(document.createTextNode(G))}else{E.text=G}F.insertBefore(E,F.firstChild);F.removeChild(E)}},nodeName:function(F,E){return F.nodeName&&F.nodeName.toUpperCase()==E.toUpperCase()},each:function(G,K,F){var E,H=0,I=G.length;if(F){if(I===g){for(E in G){if(K.apply(G[E],F)===false){break}}}else{for(;H<I;){if(K.apply(G[H++],F)===false){break}}}}else{if(I===g){for(E in G){if(K.call(G[E],E,G[E])===false){break}}}else{for(var J=G[0];H<I&&K.call(J,H,J)!==false;J=G[++H]){}}}return G},prop:function(H,I,G,F,E){if(o.isFunction(I)){I=I.call(H,F)}return typeof I==="number"&&G=="curCSS"&&!b.test(E)?I+"px":I},className:{add:function(E,F){o.each((F||"").split(/\s+/),function(G,H){if(E.nodeType==1&&!o.className.has(E.className,H)){E.className+=(E.className?" ":"")+H}})},remove:function(E,F){if(E.nodeType==1){E.className=F!==g?o.grep(E.className.split(/\s+/),function(G){return !o.className.has(F,G)}).join(" "):""}},has:function(F,E){return F&&o.inArray(E,(F.className||F).toString().split(/\s+/))>-1}},swap:function(H,G,I){var E={};for(var F in G){E[F]=H.style[F];H.style[F]=G[F]}I.call(H);for(var F in G){H.style[F]=E[F]}},css:function(H,F,J,E){if(F=="width"||F=="height"){var L,G={position:"absolute",visibility:"hidden",display:"block"},K=F=="width"?["Left","Right"]:["Top","Bottom"];function I(){L=F=="width"?H.offsetWidth:H.offsetHeight;if(E==="border"){return}o.each(K,function(){if(!E){L-=parseFloat(o.curCSS(H,"padding"+this,true))||0}if(E==="margin"){L+=parseFloat(o.curCSS(H,"margin"+this,true))||0}else{L-=parseFloat(o.curCSS(H,"border"+this+"Width",true))||0}})}if(H.offsetWidth!==0){I()}else{o.swap(H,G,I)}return Math.max(0,Math.round(L))}return o.curCSS(H,F,J)},curCSS:function(I,F,G){var L,E=I.style;if(F=="opacity"&&!o.support.opacity){L=o.attr(E,"opacity");return L==""?"1":L}if(F.match(/float/i)){F=w}if(!G&&E&&E[F]){L=E[F]}else{if(q.getComputedStyle){if(F.match(/float/i)){F="float"}F=F.replace(/([A-Z])/g,"-$1").toLowerCase();var M=q.getComputedStyle(I,null);if(M){L=M.getPropertyValue(F)}if(F=="opacity"&&L==""){L="1"}}else{if(I.currentStyle){var J=F.replace(/\-(\w)/g,function(N,O){return O.toUpperCase()});L=I.currentStyle[F]||I.currentStyle[J];if(!/^\d+(px)?$/i.test(L)&&/^\d/.test(L)){var H=E.left,K=I.runtimeStyle.left;I.runtimeStyle.left=I.currentStyle.left;E.left=L||0;L=E.pixelLeft+"px";E.left=H;I.runtimeStyle.left=K}}}}return L},clean:function(F,K,I){K=K||document;if(typeof K.createElement==="undefined"){K=K.ownerDocument||K[0]&&K[0].ownerDocument||document}if(!I&&F.length===1&&typeof F[0]==="string"){var H=/^<(\w+)\s*\/?>$/.exec(F[0]);if(H){return[K.createElement(H[1])]}}var G=[],E=[],L=K.createElement("div");o.each(F,function(P,S){if(typeof S==="number"){S+=""}if(!S){return}if(typeof S==="string"){S=S.replace(/(<(\w+)[^>]*?)\/>/g,function(U,V,T){return T.match(/^(abbr|br|col|img|input|link|meta|param|hr|area|embed)$/i)?U:V+"></"+T+">"});var O=S.replace(/^\s+/,"").substring(0,10).toLowerCase();var Q=!O.indexOf("<opt")&&[1,"<select multiple='multiple'>","</select>"]||!O.indexOf("<leg")&&[1,"<fieldset>","</fieldset>"]||O.match(/^<(thead|tbody|tfoot|colg|cap)/)&&[1,"<table>","</table>"]||!O.indexOf("<tr")&&[2,"<table><tbody>","</tbody></table>"]||(!O.indexOf("<td")||!O.indexOf("<th"))&&[3,"<table><tbody><tr>","</tr></tbody></table>"]||!O.indexOf("<col")&&[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"]||!o.support.htmlSerialize&&[1,"div<div>","</div>"]||[0,"",""];L.innerHTML=Q[1]+S+Q[2];while(Q[0]--){L=L.lastChild}if(!o.support.tbody){var R=/<tbody/i.test(S),N=!O.indexOf("<table")&&!R?L.firstChild&&L.firstChild.childNodes:Q[1]=="<table>"&&!R?L.childNodes:[];for(var M=N.length-1;M>=0;--M){if(o.nodeName(N[M],"tbody")&&!N[M].childNodes.length){N[M].parentNode.removeChild(N[M])}}}if(!o.support.leadingWhitespace&&/^\s/.test(S)){L.insertBefore(K.createTextNode(S.match(/^\s*/)[0]),L.firstChild)}S=o.makeArray(L.childNodes)}if(S.nodeType){G.push(S)}else{G=o.merge(G,S)}});if(I){for(var J=0;G[J];J++){if(o.nodeName(G[J],"script")&&(!G[J].type||G[J].type.toLowerCase()==="text/javascript")){E.push(G[J].parentNode?G[J].parentNode.removeChild(G[J]):G[J])}else{if(G[J].nodeType===1){G.splice.apply(G,[J+1,0].concat(o.makeArray(G[J].getElementsByTagName("script"))))}I.appendChild(G[J])}}return E}return G},attr:function(J,G,K){if(!J||J.nodeType==3||J.nodeType==8){return g}var H=!o.isXMLDoc(J),L=K!==g;G=H&&o.props[G]||G;if(J.tagName){var F=/href|src|style/.test(G);if(G=="selected"&&J.parentNode){J.parentNode.selectedIndex}if(G in J&&H&&!F){if(L){if(G=="type"&&o.nodeName(J,"input")&&J.parentNode){throw"type property can't be changed"}J[G]=K}if(o.nodeName(J,"form")&&J.getAttributeNode(G)){return J.getAttributeNode(G).nodeValue}if(G=="tabIndex"){var I=J.getAttributeNode("tabIndex");return I&&I.specified?I.value:J.nodeName.match(/(button|input|object|select|textarea)/i)?0:J.nodeName.match(/^(a|area)$/i)&&J.href?0:g}return J[G]}if(!o.support.style&&H&&G=="style"){return o.attr(J.style,"cssText",K)}if(L){J.setAttribute(G,""+K)}var E=!o.support.hrefNormalized&&H&&F?J.getAttribute(G,2):J.getAttribute(G);return E===null?g:E}if(!o.support.opacity&&G=="opacity"){if(L){J.zoom=1;J.filter=(J.filter||"").replace(/alpha\([^)]*\)/,"")+(parseInt(K)+""=="NaN"?"":"alpha(opacity="+K*100+")")}return J.filter&&J.filter.indexOf("opacity=")>=0?(parseFloat(J.filter.match(/opacity=([^)]*)/)[1])/100)+"":""}G=G.replace(/-([a-z])/ig,function(M,N){return N.toUpperCase()});if(L){J[G]=K}return J[G]},trim:function(E){return(E||"").replace(/^\s+|\s+$/g,"")},makeArray:function(G){var E=[];if(G!=null){var F=G.length;if(F==null||typeof G==="string"||o.isFunction(G)||G.setInterval){E[0]=G}else{while(F){E[--F]=G[F]}}}return E},inArray:function(G,H){for(var E=0,F=H.length;E<F;E++){if(H[E]===G){return E}}return -1},merge:function(H,E){var F=0,G,I=H.length;if(!o.support.getAll){while((G=E[F++])!=null){if(G.nodeType!=8){H[I++]=G}}}else{while((G=E[F++])!=null){H[I++]=G}}return H},unique:function(K){var F=[],E={};try{for(var G=0,H=K.length;G<H;G++){var J=o.data(K[G]);if(!E[J]){E[J]=true;F.push(K[G])}}}catch(I){F=K}return F},grep:function(F,J,E){var G=[];for(var H=0,I=F.length;H<I;H++){if(!E!=!J(F[H],H)){G.push(F[H])}}return G},map:function(E,J){var F=[];for(var G=0,H=E.length;G<H;G++){var I=J(E[G],G);if(I!=null){F[F.length]=I}}return F.concat.apply([],F)}});var C=navigator.userAgent.toLowerCase();o.browser={version:(C.match(/.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/)||[0,"0"])[1],safari:/webkit/.test(C),opera:/opera/.test(C),msie:/msie/.test(C)&&!/opera/.test(C),mozilla:/mozilla/.test(C)&&!/(compatible|webkit)/.test(C)};o.each({parent:function(E){return E.parentNode},parents:function(E){return o.dir(E,"parentNode")},next:function(E){return o.nth(E,2,"nextSibling")},prev:function(E){return o.nth(E,2,"previousSibling")},nextAll:function(E){return o.dir(E,"nextSibling")},prevAll:function(E){return o.dir(E,"previousSibling")},siblings:function(E){return o.sibling(E.parentNode.firstChild,E)},children:function(E){return o.sibling(E.firstChild)},contents:function(E){return o.nodeName(E,"iframe")?E.contentDocument||E.contentWindow.document:o.makeArray(E.childNodes)}},function(E,F){o.fn[E]=function(G){var H=o.map(this,F);if(G&&typeof G=="string"){H=o.multiFilter(G,H)}return this.pushStack(o.unique(H),E,G)}});o.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(E,F){o.fn[E]=function(G){var J=[],L=o(G);for(var K=0,H=L.length;K<H;K++){var I=(K>0?this.clone(true):this).get();o.fn[F].apply(o(L[K]),I);J=J.concat(I)}return this.pushStack(J,E,G)}});o.each({removeAttr:function(E){o.attr(this,E,"");if(this.nodeType==1){this.removeAttribute(E)}},addClass:function(E){o.className.add(this,E)},removeClass:function(E){o.className.remove(this,E)},toggleClass:function(F,E){if(typeof E!=="boolean"){E=!o.className.has(this,F)}o.className[E?"add":"remove"](this,F)},remove:function(E){if(!E||o.filter(E,[this]).length){o("*",this).add([this]).each(function(){o.event.remove(this);o.removeData(this)});if(this.parentNode){this.parentNode.removeChild(this)}}},empty:function(){o(this).children().remove();while(this.firstChild){this.removeChild(this.firstChild)}}},function(E,F){o.fn[E]=function(){return this.each(F,arguments)}});function j(E,F){return E[0]&&parseInt(o.curCSS(E[0],F,true),10)||0}var h="jQuery"+e(),v=0,A={};o.extend({cache:{},data:function(F,E,G){F=F==l?A:F;var H=F[h];if(!H){H=F[h]=++v}if(E&&!o.cache[H]){o.cache[H]={}}if(G!==g){o.cache[H][E]=G}return E?o.cache[H][E]:H},removeData:function(F,E){F=F==l?A:F;var H=F[h];if(E){if(o.cache[H]){delete o.cache[H][E];E="";for(E in o.cache[H]){break}if(!E){o.removeData(F)}}}else{try{delete F[h]}catch(G){if(F.removeAttribute){F.removeAttribute(h)}}delete o.cache[H]}},queue:function(F,E,H){if(F){E=(E||"fx")+"queue";var G=o.data(F,E);if(!G||o.isArray(H)){G=o.data(F,E,o.makeArray(H))}else{if(H){G.push(H)}}}return G},dequeue:function(H,G){var E=o.queue(H,G),F=E.shift();if(!G||G==="fx"){F=E[0]}if(F!==g){F.call(H)}}});o.fn.extend({data:function(E,G){var H=E.split(".");H[1]=H[1]?"."+H[1]:"";if(G===g){var F=this.triggerHandler("getData"+H[1]+"!",[H[0]]);if(F===g&&this.length){F=o.data(this[0],E)}return F===g&&H[1]?this.data(H[0]):F}else{return this.trigger("setData"+H[1]+"!",[H[0],G]).each(function(){o.data(this,E,G)})}},removeData:function(E){return this.each(function(){o.removeData(this,E)})},queue:function(E,F){if(typeof E!=="string"){F=E;E="fx"}if(F===g){return o.queue(this[0],E)}return this.each(function(){var G=o.queue(this,E,F);if(E=="fx"&&G.length==1){G[0].call(this)}})},dequeue:function(E){return this.each(function(){o.dequeue(this,E)})}});
/*
 * Sizzle CSS Selector Engine - v0.9.3
 *  Copyright 2009, The Dojo Foundation
 *  Released under the MIT, BSD, and GPL Licenses.
 *  More information: http://sizzlejs.com/
 */
(function(){var R=/((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^[\]]*\]|['"][^'"]*['"]|[^[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?/g,L=0,H=Object.prototype.toString;var F=function(Y,U,ab,ac){ab=ab||[];U=U||document;if(U.nodeType!==1&&U.nodeType!==9){return[]}if(!Y||typeof Y!=="string"){return ab}var Z=[],W,af,ai,T,ad,V,X=true;R.lastIndex=0;while((W=R.exec(Y))!==null){Z.push(W[1]);if(W[2]){V=RegExp.rightContext;break}}if(Z.length>1&&M.exec(Y)){if(Z.length===2&&I.relative[Z[0]]){af=J(Z[0]+Z[1],U)}else{af=I.relative[Z[0]]?[U]:F(Z.shift(),U);while(Z.length){Y=Z.shift();if(I.relative[Y]){Y+=Z.shift()}af=J(Y,af)}}}else{var ae=ac?{expr:Z.pop(),set:E(ac)}:F.find(Z.pop(),Z.length===1&&U.parentNode?U.parentNode:U,Q(U));af=F.filter(ae.expr,ae.set);if(Z.length>0){ai=E(af)}else{X=false}while(Z.length){var ah=Z.pop(),ag=ah;if(!I.relative[ah]){ah=""}else{ag=Z.pop()}if(ag==null){ag=U}I.relative[ah](ai,ag,Q(U))}}if(!ai){ai=af}if(!ai){throw"Syntax error, unrecognized expression: "+(ah||Y)}if(H.call(ai)==="[object Array]"){if(!X){ab.push.apply(ab,ai)}else{if(U.nodeType===1){for(var aa=0;ai[aa]!=null;aa++){if(ai[aa]&&(ai[aa]===true||ai[aa].nodeType===1&&K(U,ai[aa]))){ab.push(af[aa])}}}else{for(var aa=0;ai[aa]!=null;aa++){if(ai[aa]&&ai[aa].nodeType===1){ab.push(af[aa])}}}}}else{E(ai,ab)}if(V){F(V,U,ab,ac);if(G){hasDuplicate=false;ab.sort(G);if(hasDuplicate){for(var aa=1;aa<ab.length;aa++){if(ab[aa]===ab[aa-1]){ab.splice(aa--,1)}}}}}return ab};F.matches=function(T,U){return F(T,null,null,U)};F.find=function(aa,T,ab){var Z,X;if(!aa){return[]}for(var W=0,V=I.order.length;W<V;W++){var Y=I.order[W],X;if((X=I.match[Y].exec(aa))){var U=RegExp.leftContext;if(U.substr(U.length-1)!=="\\"){X[1]=(X[1]||"").replace(/\\/g,"");Z=I.find[Y](X,T,ab);if(Z!=null){aa=aa.replace(I.match[Y],"");break}}}}if(!Z){Z=T.getElementsByTagName("*")}return{set:Z,expr:aa}};F.filter=function(ad,ac,ag,W){var V=ad,ai=[],aa=ac,Y,T,Z=ac&&ac[0]&&Q(ac[0]);while(ad&&ac.length){for(var ab in I.filter){if((Y=I.match[ab].exec(ad))!=null){var U=I.filter[ab],ah,af;T=false;if(aa==ai){ai=[]}if(I.preFilter[ab]){Y=I.preFilter[ab](Y,aa,ag,ai,W,Z);if(!Y){T=ah=true}else{if(Y===true){continue}}}if(Y){for(var X=0;(af=aa[X])!=null;X++){if(af){ah=U(af,Y,X,aa);var ae=W^!!ah;if(ag&&ah!=null){if(ae){T=true}else{aa[X]=false}}else{if(ae){ai.push(af);T=true}}}}}if(ah!==g){if(!ag){aa=ai}ad=ad.replace(I.match[ab],"");if(!T){return[]}break}}}if(ad==V){if(T==null){throw"Syntax error, unrecognized expression: "+ad}else{break}}V=ad}return aa};var I=F.selectors={order:["ID","NAME","TAG"],match:{ID:/#((?:[\w\u00c0-\uFFFF_-]|\\.)+)/,CLASS:/\.((?:[\w\u00c0-\uFFFF_-]|\\.)+)/,NAME:/\[name=['"]*((?:[\w\u00c0-\uFFFF_-]|\\.)+)['"]*\]/,ATTR:/\[\s*((?:[\w\u00c0-\uFFFF_-]|\\.)+)\s*(?:(\S?=)\s*(['"]*)(.*?)\3|)\s*\]/,TAG:/^((?:[\w\u00c0-\uFFFF\*_-]|\\.)+)/,CHILD:/:(only|nth|last|first)-child(?:\((even|odd|[\dn+-]*)\))?/,POS:/:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^-]|$)/,PSEUDO:/:((?:[\w\u00c0-\uFFFF_-]|\\.)+)(?:\((['"]*)((?:\([^\)]+\)|[^\2\(\)]*)+)\2\))?/},attrMap:{"class":"className","for":"htmlFor"},attrHandle:{href:function(T){return T.getAttribute("href")}},relative:{"+":function(aa,T,Z){var X=typeof T==="string",ab=X&&!/\W/.test(T),Y=X&&!ab;if(ab&&!Z){T=T.toUpperCase()}for(var W=0,V=aa.length,U;W<V;W++){if((U=aa[W])){while((U=U.previousSibling)&&U.nodeType!==1){}aa[W]=Y||U&&U.nodeName===T?U||false:U===T}}if(Y){F.filter(T,aa,true)}},">":function(Z,U,aa){var X=typeof U==="string";if(X&&!/\W/.test(U)){U=aa?U:U.toUpperCase();for(var V=0,T=Z.length;V<T;V++){var Y=Z[V];if(Y){var W=Y.parentNode;Z[V]=W.nodeName===U?W:false}}}else{for(var V=0,T=Z.length;V<T;V++){var Y=Z[V];if(Y){Z[V]=X?Y.parentNode:Y.parentNode===U}}if(X){F.filter(U,Z,true)}}},"":function(W,U,Y){var V=L++,T=S;if(!U.match(/\W/)){var X=U=Y?U:U.toUpperCase();T=P}T("parentNode",U,V,W,X,Y)},"~":function(W,U,Y){var V=L++,T=S;if(typeof U==="string"&&!U.match(/\W/)){var X=U=Y?U:U.toUpperCase();T=P}T("previousSibling",U,V,W,X,Y)}},find:{ID:function(U,V,W){if(typeof V.getElementById!=="undefined"&&!W){var T=V.getElementById(U[1]);return T?[T]:[]}},NAME:function(V,Y,Z){if(typeof Y.getElementsByName!=="undefined"){var U=[],X=Y.getElementsByName(V[1]);for(var W=0,T=X.length;W<T;W++){if(X[W].getAttribute("name")===V[1]){U.push(X[W])}}return U.length===0?null:U}},TAG:function(T,U){return U.getElementsByTagName(T[1])}},preFilter:{CLASS:function(W,U,V,T,Z,aa){W=" "+W[1].replace(/\\/g,"")+" ";if(aa){return W}for(var X=0,Y;(Y=U[X])!=null;X++){if(Y){if(Z^(Y.className&&(" "+Y.className+" ").indexOf(W)>=0)){if(!V){T.push(Y)}}else{if(V){U[X]=false}}}}return false},ID:function(T){return T[1].replace(/\\/g,"")},TAG:function(U,T){for(var V=0;T[V]===false;V++){}return T[V]&&Q(T[V])?U[1]:U[1].toUpperCase()},CHILD:function(T){if(T[1]=="nth"){var U=/(-?)(\d*)n((?:\+|-)?\d*)/.exec(T[2]=="even"&&"2n"||T[2]=="odd"&&"2n+1"||!/\D/.test(T[2])&&"0n+"+T[2]||T[2]);T[2]=(U[1]+(U[2]||1))-0;T[3]=U[3]-0}T[0]=L++;return T},ATTR:function(X,U,V,T,Y,Z){var W=X[1].replace(/\\/g,"");if(!Z&&I.attrMap[W]){X[1]=I.attrMap[W]}if(X[2]==="~="){X[4]=" "+X[4]+" "}return X},PSEUDO:function(X,U,V,T,Y){if(X[1]==="not"){if(X[3].match(R).length>1||/^\w/.test(X[3])){X[3]=F(X[3],null,null,U)}else{var W=F.filter(X[3],U,V,true^Y);if(!V){T.push.apply(T,W)}return false}}else{if(I.match.POS.test(X[0])||I.match.CHILD.test(X[0])){return true}}return X},POS:function(T){T.unshift(true);return T}},filters:{enabled:function(T){return T.disabled===false&&T.type!=="hidden"},disabled:function(T){return T.disabled===true},checked:function(T){return T.checked===true},selected:function(T){T.parentNode.selectedIndex;return T.selected===true},parent:function(T){return !!T.firstChild},empty:function(T){return !T.firstChild},has:function(V,U,T){return !!F(T[3],V).length},header:function(T){return/h\d/i.test(T.nodeName)},text:function(T){return"text"===T.type},radio:function(T){return"radio"===T.type},checkbox:function(T){return"checkbox"===T.type},file:function(T){return"file"===T.type},password:function(T){return"password"===T.type},submit:function(T){return"submit"===T.type},image:function(T){return"image"===T.type},reset:function(T){return"reset"===T.type},button:function(T){return"button"===T.type||T.nodeName.toUpperCase()==="BUTTON"},input:function(T){return/input|select|textarea|button/i.test(T.nodeName)}},setFilters:{first:function(U,T){return T===0},last:function(V,U,T,W){return U===W.length-1},even:function(U,T){return T%2===0},odd:function(U,T){return T%2===1},lt:function(V,U,T){return U<T[3]-0},gt:function(V,U,T){return U>T[3]-0},nth:function(V,U,T){return T[3]-0==U},eq:function(V,U,T){return T[3]-0==U}},filter:{PSEUDO:function(Z,V,W,aa){var U=V[1],X=I.filters[U];if(X){return X(Z,W,V,aa)}else{if(U==="contains"){return(Z.textContent||Z.innerText||"").indexOf(V[3])>=0}else{if(U==="not"){var Y=V[3];for(var W=0,T=Y.length;W<T;W++){if(Y[W]===Z){return false}}return true}}}},CHILD:function(T,W){var Z=W[1],U=T;switch(Z){case"only":case"first":while(U=U.previousSibling){if(U.nodeType===1){return false}}if(Z=="first"){return true}U=T;case"last":while(U=U.nextSibling){if(U.nodeType===1){return false}}return true;case"nth":var V=W[2],ac=W[3];if(V==1&&ac==0){return true}var Y=W[0],ab=T.parentNode;if(ab&&(ab.sizcache!==Y||!T.nodeIndex)){var X=0;for(U=ab.firstChild;U;U=U.nextSibling){if(U.nodeType===1){U.nodeIndex=++X}}ab.sizcache=Y}var aa=T.nodeIndex-ac;if(V==0){return aa==0}else{return(aa%V==0&&aa/V>=0)}}},ID:function(U,T){return U.nodeType===1&&U.getAttribute("id")===T},TAG:function(U,T){return(T==="*"&&U.nodeType===1)||U.nodeName===T},CLASS:function(U,T){return(" "+(U.className||U.getAttribute("class"))+" ").indexOf(T)>-1},ATTR:function(Y,W){var V=W[1],T=I.attrHandle[V]?I.attrHandle[V](Y):Y[V]!=null?Y[V]:Y.getAttribute(V),Z=T+"",X=W[2],U=W[4];return T==null?X==="!=":X==="="?Z===U:X==="*="?Z.indexOf(U)>=0:X==="~="?(" "+Z+" ").indexOf(U)>=0:!U?Z&&T!==false:X==="!="?Z!=U:X==="^="?Z.indexOf(U)===0:X==="$="?Z.substr(Z.length-U.length)===U:X==="|="?Z===U||Z.substr(0,U.length+1)===U+"-":false},POS:function(X,U,V,Y){var T=U[2],W=I.setFilters[T];if(W){return W(X,V,U,Y)}}}};var M=I.match.POS;for(var O in I.match){I.match[O]=RegExp(I.match[O].source+/(?![^\[]*\])(?![^\(]*\))/.source)}var E=function(U,T){U=Array.prototype.slice.call(U);if(T){T.push.apply(T,U);return T}return U};try{Array.prototype.slice.call(document.documentElement.childNodes)}catch(N){E=function(X,W){var U=W||[];if(H.call(X)==="[object Array]"){Array.prototype.push.apply(U,X)}else{if(typeof X.length==="number"){for(var V=0,T=X.length;V<T;V++){U.push(X[V])}}else{for(var V=0;X[V];V++){U.push(X[V])}}}return U}}var G;if(document.documentElement.compareDocumentPosition){G=function(U,T){var V=U.compareDocumentPosition(T)&4?-1:U===T?0:1;if(V===0){hasDuplicate=true}return V}}else{if("sourceIndex" in document.documentElement){G=function(U,T){var V=U.sourceIndex-T.sourceIndex;if(V===0){hasDuplicate=true}return V}}else{if(document.createRange){G=function(W,U){var V=W.ownerDocument.createRange(),T=U.ownerDocument.createRange();V.selectNode(W);V.collapse(true);T.selectNode(U);T.collapse(true);var X=V.compareBoundaryPoints(Range.START_TO_END,T);if(X===0){hasDuplicate=true}return X}}}}(function(){var U=document.createElement("form"),V="script"+(new Date).getTime();U.innerHTML="<input name='"+V+"'/>";var T=document.documentElement;T.insertBefore(U,T.firstChild);if(!!document.getElementById(V)){I.find.ID=function(X,Y,Z){if(typeof Y.getElementById!=="undefined"&&!Z){var W=Y.getElementById(X[1]);return W?W.id===X[1]||typeof W.getAttributeNode!=="undefined"&&W.getAttributeNode("id").nodeValue===X[1]?[W]:g:[]}};I.filter.ID=function(Y,W){var X=typeof Y.getAttributeNode!=="undefined"&&Y.getAttributeNode("id");return Y.nodeType===1&&X&&X.nodeValue===W}}T.removeChild(U)})();(function(){var T=document.createElement("div");T.appendChild(document.createComment(""));if(T.getElementsByTagName("*").length>0){I.find.TAG=function(U,Y){var X=Y.getElementsByTagName(U[1]);if(U[1]==="*"){var W=[];for(var V=0;X[V];V++){if(X[V].nodeType===1){W.push(X[V])}}X=W}return X}}T.innerHTML="<a href='#'></a>";if(T.firstChild&&typeof T.firstChild.getAttribute!=="undefined"&&T.firstChild.getAttribute("href")!=="#"){I.attrHandle.href=function(U){return U.getAttribute("href",2)}}})();if(document.querySelectorAll){(function(){var T=F,U=document.createElement("div");U.innerHTML="<p class='TEST'></p>";if(U.querySelectorAll&&U.querySelectorAll(".TEST").length===0){return}F=function(Y,X,V,W){X=X||document;if(!W&&X.nodeType===9&&!Q(X)){try{return E(X.querySelectorAll(Y),V)}catch(Z){}}return T(Y,X,V,W)};F.find=T.find;F.filter=T.filter;F.selectors=T.selectors;F.matches=T.matches})()}if(document.getElementsByClassName&&document.documentElement.getElementsByClassName){(function(){var T=document.createElement("div");T.innerHTML="<div class='test e'></div><div class='test'></div>";if(T.getElementsByClassName("e").length===0){return}T.lastChild.className="e";if(T.getElementsByClassName("e").length===1){return}I.order.splice(1,0,"CLASS");I.find.CLASS=function(U,V,W){if(typeof V.getElementsByClassName!=="undefined"&&!W){return V.getElementsByClassName(U[1])}}})()}function P(U,Z,Y,ad,aa,ac){var ab=U=="previousSibling"&&!ac;for(var W=0,V=ad.length;W<V;W++){var T=ad[W];if(T){if(ab&&T.nodeType===1){T.sizcache=Y;T.sizset=W}T=T[U];var X=false;while(T){if(T.sizcache===Y){X=ad[T.sizset];break}if(T.nodeType===1&&!ac){T.sizcache=Y;T.sizset=W}if(T.nodeName===Z){X=T;break}T=T[U]}ad[W]=X}}}function S(U,Z,Y,ad,aa,ac){var ab=U=="previousSibling"&&!ac;for(var W=0,V=ad.length;W<V;W++){var T=ad[W];if(T){if(ab&&T.nodeType===1){T.sizcache=Y;T.sizset=W}T=T[U];var X=false;while(T){if(T.sizcache===Y){X=ad[T.sizset];break}if(T.nodeType===1){if(!ac){T.sizcache=Y;T.sizset=W}if(typeof Z!=="string"){if(T===Z){X=true;break}}else{if(F.filter(Z,[T]).length>0){X=T;break}}}T=T[U]}ad[W]=X}}}var K=document.compareDocumentPosition?function(U,T){return U.compareDocumentPosition(T)&16}:function(U,T){return U!==T&&(U.contains?U.contains(T):true)};var Q=function(T){return T.nodeType===9&&T.documentElement.nodeName!=="HTML"||!!T.ownerDocument&&Q(T.ownerDocument)};var J=function(T,aa){var W=[],X="",Y,V=aa.nodeType?[aa]:aa;while((Y=I.match.PSEUDO.exec(T))){X+=Y[0];T=T.replace(I.match.PSEUDO,"")}T=I.relative[T]?T+"*":T;for(var Z=0,U=V.length;Z<U;Z++){F(T,V[Z],W)}return F.filter(X,W)};o.find=F;o.filter=F.filter;o.expr=F.selectors;o.expr[":"]=o.expr.filters;F.selectors.filters.hidden=function(T){return T.offsetWidth===0||T.offsetHeight===0};F.selectors.filters.visible=function(T){return T.offsetWidth>0||T.offsetHeight>0};F.selectors.filters.animated=function(T){return o.grep(o.timers,function(U){return T===U.elem}).length};o.multiFilter=function(V,T,U){if(U){V=":not("+V+")"}return F.matches(V,T)};o.dir=function(V,U){var T=[],W=V[U];while(W&&W!=document){if(W.nodeType==1){T.push(W)}W=W[U]}return T};o.nth=function(X,T,V,W){T=T||1;var U=0;for(;X;X=X[V]){if(X.nodeType==1&&++U==T){break}}return X};o.sibling=function(V,U){var T=[];for(;V;V=V.nextSibling){if(V.nodeType==1&&V!=U){T.push(V)}}return T};return;l.Sizzle=F})();o.event={add:function(I,F,H,K){if(I.nodeType==3||I.nodeType==8){return}if(I.setInterval&&I!=l){I=l}if(!H.guid){H.guid=this.guid++}if(K!==g){var G=H;H=this.proxy(G);H.data=K}var E=o.data(I,"events")||o.data(I,"events",{}),J=o.data(I,"handle")||o.data(I,"handle",function(){return typeof o!=="undefined"&&!o.event.triggered?o.event.handle.apply(arguments.callee.elem,arguments):g});J.elem=I;o.each(F.split(/\s+/),function(M,N){var O=N.split(".");N=O.shift();H.type=O.slice().sort().join(".");var L=E[N];if(o.event.specialAll[N]){o.event.specialAll[N].setup.call(I,K,O)}if(!L){L=E[N]={};if(!o.event.special[N]||o.event.special[N].setup.call(I,K,O)===false){if(I.addEventListener){I.addEventListener(N,J,false)}else{if(I.attachEvent){I.attachEvent("on"+N,J)}}}}L[H.guid]=H;o.event.global[N]=true});I=null},guid:1,global:{},remove:function(K,H,J){if(K.nodeType==3||K.nodeType==8){return}var G=o.data(K,"events"),F,E;if(G){if(H===g||(typeof H==="string"&&H.charAt(0)==".")){for(var I in G){this.remove(K,I+(H||""))}}else{if(H.type){J=H.handler;H=H.type}o.each(H.split(/\s+/),function(M,O){var Q=O.split(".");O=Q.shift();var N=RegExp("(^|\\.)"+Q.slice().sort().join(".*\\.")+"(\\.|$)");if(G[O]){if(J){delete G[O][J.guid]}else{for(var P in G[O]){if(N.test(G[O][P].type)){delete G[O][P]}}}if(o.event.specialAll[O]){o.event.specialAll[O].teardown.call(K,Q)}for(F in G[O]){break}if(!F){if(!o.event.special[O]||o.event.special[O].teardown.call(K,Q)===false){if(K.removeEventListener){K.removeEventListener(O,o.data(K,"handle"),false)}else{if(K.detachEvent){K.detachEvent("on"+O,o.data(K,"handle"))}}}F=null;delete G[O]}}})}for(F in G){break}if(!F){var L=o.data(K,"handle");if(L){L.elem=null}o.removeData(K,"events");o.removeData(K,"handle")}}},trigger:function(I,K,H,E){var G=I.type||I;if(!E){I=typeof I==="object"?I[h]?I:o.extend(o.Event(G),I):o.Event(G);if(G.indexOf("!")>=0){I.type=G=G.slice(0,-1);I.exclusive=true}if(!H){I.stopPropagation();if(this.global[G]){o.each(o.cache,function(){if(this.events&&this.events[G]){o.event.trigger(I,K,this.handle.elem)}})}}if(!H||H.nodeType==3||H.nodeType==8){return g}I.result=g;I.target=H;K=o.makeArray(K);K.unshift(I)}I.currentTarget=H;var J=o.data(H,"handle");if(J){J.apply(H,K)}if((!H[G]||(o.nodeName(H,"a")&&G=="click"))&&H["on"+G]&&H["on"+G].apply(H,K)===false){I.result=false}if(!E&&H[G]&&!I.isDefaultPrevented()&&!(o.nodeName(H,"a")&&G=="click")){this.triggered=true;try{H[G]()}catch(L){}}this.triggered=false;if(!I.isPropagationStopped()){var F=H.parentNode||H.ownerDocument;if(F){o.event.trigger(I,K,F,true)}}},handle:function(K){var J,E;K=arguments[0]=o.event.fix(K||l.event);K.currentTarget=this;var L=K.type.split(".");K.type=L.shift();J=!L.length&&!K.exclusive;var I=RegExp("(^|\\.)"+L.slice().sort().join(".*\\.")+"(\\.|$)");E=(o.data(this,"events")||{})[K.type];for(var G in E){var H=E[G];if(J||I.test(H.type)){K.handler=H;K.data=H.data;var F=H.apply(this,arguments);if(F!==g){K.result=F;if(F===false){K.preventDefault();K.stopPropagation()}}if(K.isImmediatePropagationStopped()){break}}}},props:"altKey attrChange attrName bubbles button cancelable charCode clientX clientY ctrlKey currentTarget data detail eventPhase fromElement handler keyCode metaKey newValue originalTarget pageX pageY prevValue relatedNode relatedTarget screenX screenY shiftKey srcElement target toElement view wheelDelta which".split(" "),fix:function(H){if(H[h]){return H}var F=H;H=o.Event(F);for(var G=this.props.length,J;G;){J=this.props[--G];H[J]=F[J]}if(!H.target){H.target=H.srcElement||document}if(H.target.nodeType==3){H.target=H.target.parentNode}if(!H.relatedTarget&&H.fromElement){H.relatedTarget=H.fromElement==H.target?H.toElement:H.fromElement}if(H.pageX==null&&H.clientX!=null){var I=document.documentElement,E=document.body;H.pageX=H.clientX+(I&&I.scrollLeft||E&&E.scrollLeft||0)-(I.clientLeft||0);H.pageY=H.clientY+(I&&I.scrollTop||E&&E.scrollTop||0)-(I.clientTop||0)}if(!H.which&&((H.charCode||H.charCode===0)?H.charCode:H.keyCode)){H.which=H.charCode||H.keyCode}if(!H.metaKey&&H.ctrlKey){H.metaKey=H.ctrlKey}if(!H.which&&H.button){H.which=(H.button&1?1:(H.button&2?3:(H.button&4?2:0)))}return H},proxy:function(F,E){E=E||function(){return F.apply(this,arguments)};E.guid=F.guid=F.guid||E.guid||this.guid++;return E},special:{ready:{setup:B,teardown:function(){}}},specialAll:{live:{setup:function(E,F){o.event.add(this,F[0],c)},teardown:function(G){if(G.length){var E=0,F=RegExp("(^|\\.)"+G[0]+"(\\.|$)");o.each((o.data(this,"events").live||{}),function(){if(F.test(this.type)){E++}});if(E<1){o.event.remove(this,G[0],c)}}}}}};o.Event=function(E){if(!this.preventDefault){return new o.Event(E)}if(E&&E.type){this.originalEvent=E;this.type=E.type}else{this.type=E}this.timeStamp=e();this[h]=true};function k(){return false}function u(){return true}o.Event.prototype={preventDefault:function(){this.isDefaultPrevented=u;var E=this.originalEvent;if(!E){return}if(E.preventDefault){E.preventDefault()}E.returnValue=false},stopPropagation:function(){this.isPropagationStopped=u;var E=this.originalEvent;if(!E){return}if(E.stopPropagation){E.stopPropagation()}E.cancelBubble=true},stopImmediatePropagation:function(){this.isImmediatePropagationStopped=u;this.stopPropagation()},isDefaultPrevented:k,isPropagationStopped:k,isImmediatePropagationStopped:k};var a=function(F){var E=F.relatedTarget;while(E&&E!=this){try{E=E.parentNode}catch(G){E=this}}if(E!=this){F.type=F.data;o.event.handle.apply(this,arguments)}};o.each({mouseover:"mouseenter",mouseout:"mouseleave"},function(F,E){o.event.special[E]={setup:function(){o.event.add(this,F,a,E)},teardown:function(){o.event.remove(this,F,a)}}});o.fn.extend({bind:function(F,G,E){return F=="unload"?this.one(F,G,E):this.each(function(){o.event.add(this,F,E||G,E&&G)})},one:function(G,H,F){var E=o.event.proxy(F||H,function(I){o(this).unbind(I,E);return(F||H).apply(this,arguments)});return this.each(function(){o.event.add(this,G,E,F&&H)})},unbind:function(F,E){return this.each(function(){o.event.remove(this,F,E)})},trigger:function(E,F){return this.each(function(){o.event.trigger(E,F,this)})},triggerHandler:function(E,G){if(this[0]){var F=o.Event(E);F.preventDefault();F.stopPropagation();o.event.trigger(F,G,this[0]);return F.result}},toggle:function(G){var E=arguments,F=1;while(F<E.length){o.event.proxy(G,E[F++])}return this.click(o.event.proxy(G,function(H){this.lastToggle=(this.lastToggle||0)%F;H.preventDefault();return E[this.lastToggle++].apply(this,arguments)||false}))},hover:function(E,F){return this.mouseenter(E).mouseleave(F)},ready:function(E){B();if(o.isReady){E.call(document,o)}else{o.readyList.push(E)}return this},live:function(G,F){var E=o.event.proxy(F);E.guid+=this.selector+G;o(document).bind(i(G,this.selector),this.selector,E);return this},die:function(F,E){o(document).unbind(i(F,this.selector),E?{guid:E.guid+this.selector+F}:null);return this}});function c(H){var E=RegExp("(^|\\.)"+H.type+"(\\.|$)"),G=true,F=[];o.each(o.data(this,"events").live||[],function(I,J){if(E.test(J.type)){var K=o(H.target).closest(J.data)[0];if(K){F.push({elem:K,fn:J})}}});F.sort(function(J,I){return o.data(J.elem,"closest")-o.data(I.elem,"closest")});o.each(F,function(){if(this.fn.call(this.elem,H,this.fn.data)===false){return(G=false)}});return G}function i(F,E){return["live",F,E.replace(/\./g,"`").replace(/ /g,"|")].join(".")}o.extend({isReady:false,readyList:[],ready:function(){if(!o.isReady){o.isReady=true;if(o.readyList){o.each(o.readyList,function(){this.call(document,o)});o.readyList=null}o(document).triggerHandler("ready")}}});var x=false;function B(){if(x){return}x=true;if(document.addEventListener){document.addEventListener("DOMContentLoaded",function(){document.removeEventListener("DOMContentLoaded",arguments.callee,false);o.ready()},false)}else{if(document.attachEvent){document.attachEvent("onreadystatechange",function(){if(document.readyState==="complete"){document.detachEvent("onreadystatechange",arguments.callee);o.ready()}});if(document.documentElement.doScroll&&l==l.top){(function(){if(o.isReady){return}try{document.documentElement.doScroll("left")}catch(E){setTimeout(arguments.callee,0);return}o.ready()})()}}}o.event.add(l,"load",o.ready)}o.each(("blur,focus,load,resize,scroll,unload,click,dblclick,mousedown,mouseup,mousemove,mouseover,mouseout,mouseenter,mouseleave,change,select,submit,keydown,keypress,keyup,error").split(","),function(F,E){o.fn[E]=function(G){return G?this.bind(E,G):this.trigger(E)}});o(l).bind("unload",function(){for(var E in o.cache){if(E!=1&&o.cache[E].handle){o.event.remove(o.cache[E].handle.elem)}}});(function(){o.support={};var F=document.documentElement,G=document.createElement("script"),K=document.createElement("div"),J="script"+(new Date).getTime();K.style.display="none";K.innerHTML='   <link/><table></table><a href="/a" style="color:red;float:left;opacity:.5;">a</a><select><option>text</option></select><object><param/></object>';var H=K.getElementsByTagName("*"),E=K.getElementsByTagName("a")[0];if(!H||!H.length||!E){return}o.support={leadingWhitespace:K.firstChild.nodeType==3,tbody:!K.getElementsByTagName("tbody").length,objectAll:!!K.getElementsByTagName("object")[0].getElementsByTagName("*").length,htmlSerialize:!!K.getElementsByTagName("link").length,style:/red/.test(E.getAttribute("style")),hrefNormalized:E.getAttribute("href")==="/a",opacity:E.style.opacity==="0.5",cssFloat:!!E.style.cssFloat,scriptEval:false,noCloneEvent:true,boxModel:null};G.type="text/javascript";try{G.appendChild(document.createTextNode("window."+J+"=1;"))}catch(I){}F.insertBefore(G,F.firstChild);if(l[J]){o.support.scriptEval=true;delete l[J]}F.removeChild(G);if(K.attachEvent&&K.fireEvent){K.attachEvent("onclick",function(){o.support.noCloneEvent=false;K.detachEvent("onclick",arguments.callee)});K.cloneNode(true).fireEvent("onclick")}o(function(){var L=document.createElement("div");L.style.width=L.style.paddingLeft="1px";document.body.appendChild(L);o.boxModel=o.support.boxModel=L.offsetWidth===2;document.body.removeChild(L).style.display="none"})})();var w=o.support.cssFloat?"cssFloat":"styleFloat";o.props={"for":"htmlFor","class":"className","float":w,cssFloat:w,styleFloat:w,readonly:"readOnly",maxlength:"maxLength",cellspacing:"cellSpacing",rowspan:"rowSpan",tabindex:"tabIndex"};o.fn.extend({_load:o.fn.load,load:function(G,J,K){if(typeof G!=="string"){return this._load(G)}var I=G.indexOf(" ");if(I>=0){var E=G.slice(I,G.length);G=G.slice(0,I)}var H="GET";if(J){if(o.isFunction(J)){K=J;J=null}else{if(typeof J==="object"){J=o.param(J);H="POST"}}}var F=this;o.ajax({url:G,type:H,dataType:"html",data:J,complete:function(M,L){if(L=="success"||L=="notmodified"){F.html(E?o("<div/>").append(M.responseText.replace(/<script(.|\s)*?\/script>/g,"")).find(E):M.responseText)}if(K){F.each(K,[M.responseText,L,M])}}});return this},serialize:function(){return o.param(this.serializeArray())},serializeArray:function(){return this.map(function(){return this.elements?o.makeArray(this.elements):this}).filter(function(){return this.name&&!this.disabled&&(this.checked||/select|textarea/i.test(this.nodeName)||/text|hidden|password|search/i.test(this.type))}).map(function(E,F){var G=o(this).val();return G==null?null:o.isArray(G)?o.map(G,function(I,H){return{name:F.name,value:I}}):{name:F.name,value:G}}).get()}});o.each("ajaxStart,ajaxStop,ajaxComplete,ajaxError,ajaxSuccess,ajaxSend".split(","),function(E,F){o.fn[F]=function(G){return this.bind(F,G)}});var r=e();o.extend({get:function(E,G,H,F){if(o.isFunction(G)){H=G;G=null}return o.ajax({type:"GET",url:E,data:G,success:H,dataType:F})},getScript:function(E,F){return o.get(E,null,F,"script")},getJSON:function(E,F,G){return o.get(E,F,G,"json")},post:function(E,G,H,F){if(o.isFunction(G)){H=G;G={}}return o.ajax({type:"POST",url:E,data:G,success:H,dataType:F})},ajaxSetup:function(E){o.extend(o.ajaxSettings,E)},ajaxSettings:{url:location.href,global:true,type:"GET",contentType:"application/x-www-form-urlencoded",processData:true,async:true,xhr:function(){return l.ActiveXObject?new ActiveXObject("Microsoft.XMLHTTP"):new XMLHttpRequest()},accepts:{xml:"application/xml, text/xml",html:"text/html",script:"text/javascript, application/javascript",json:"application/json, text/javascript",text:"text/plain",_default:"*/*"}},lastModified:{},ajax:function(M){M=o.extend(true,M,o.extend(true,{},o.ajaxSettings,M));var W,F=/=\?(&|$)/g,R,V,G=M.type.toUpperCase();if(M.data&&M.processData&&typeof M.data!=="string"){M.data=o.param(M.data)}if(M.dataType=="jsonp"){if(G=="GET"){if(!M.url.match(F)){M.url+=(M.url.match(/\?/)?"&":"?")+(M.jsonp||"callback")+"=?"}}else{if(!M.data||!M.data.match(F)){M.data=(M.data?M.data+"&":"")+(M.jsonp||"callback")+"=?"}}M.dataType="json"}if(M.dataType=="json"&&(M.data&&M.data.match(F)||M.url.match(F))){W="jsonp"+r++;if(M.data){M.data=(M.data+"").replace(F,"="+W+"$1")}M.url=M.url.replace(F,"="+W+"$1");M.dataType="script";l[W]=function(X){V=X;I();L();l[W]=g;try{delete l[W]}catch(Y){}if(H){H.removeChild(T)}}}if(M.dataType=="script"&&M.cache==null){M.cache=false}if(M.cache===false&&G=="GET"){var E=e();var U=M.url.replace(/(\?|&)_=.*?(&|$)/,"$1_="+E+"$2");M.url=U+((U==M.url)?(M.url.match(/\?/)?"&":"?")+"_="+E:"")}if(M.data&&G=="GET"){M.url+=(M.url.match(/\?/)?"&":"?")+M.data;M.data=null}if(M.global&&!o.active++){o.event.trigger("ajaxStart")}var Q=/^(\w+:)?\/\/([^\/?#]+)/.exec(M.url);if(M.dataType=="script"&&G=="GET"&&Q&&(Q[1]&&Q[1]!=location.protocol||Q[2]!=location.host)){var H=document.getElementsByTagName("head")[0];var T=document.createElement("script");T.src=M.url;if(M.scriptCharset){T.charset=M.scriptCharset}if(!W){var O=false;T.onload=T.onreadystatechange=function(){if(!O&&(!this.readyState||this.readyState=="loaded"||this.readyState=="complete")){O=true;I();L();T.onload=T.onreadystatechange=null;H.removeChild(T)}}}H.appendChild(T);return g}var K=false;var J=M.xhr();if(M.username){J.open(G,M.url,M.async,M.username,M.password)}else{J.open(G,M.url,M.async)}try{if(M.data){J.setRequestHeader("Content-Type",M.contentType)}if(M.ifModified){J.setRequestHeader("If-Modified-Since",o.lastModified[M.url]||"Thu, 01 Jan 1970 00:00:00 GMT")}J.setRequestHeader("X-Requested-With","XMLHttpRequest");J.setRequestHeader("Accept",M.dataType&&M.accepts[M.dataType]?M.accepts[M.dataType]+", */*":M.accepts._default)}catch(S){}if(M.beforeSend&&M.beforeSend(J,M)===false){if(M.global&&!--o.active){o.event.trigger("ajaxStop")}J.abort();return false}if(M.global){o.event.trigger("ajaxSend",[J,M])}var N=function(X){if(J.readyState==0){if(P){clearInterval(P);P=null;if(M.global&&!--o.active){o.event.trigger("ajaxStop")}}}else{if(!K&&J&&(J.readyState==4||X=="timeout")){K=true;if(P){clearInterval(P);P=null}R=X=="timeout"?"timeout":!o.httpSuccess(J)?"error":M.ifModified&&o.httpNotModified(J,M.url)?"notmodified":"success";if(R=="success"){try{V=o.httpData(J,M.dataType,M)}catch(Z){R="parsererror"}}if(R=="success"){var Y;try{Y=J.getResponseHeader("Last-Modified")}catch(Z){}if(M.ifModified&&Y){o.lastModified[M.url]=Y}if(!W){I()}}else{o.handleError(M,J,R)}L();if(X){J.abort()}if(M.async){J=null}}}};if(M.async){var P=setInterval(N,13);if(M.timeout>0){setTimeout(function(){if(J&&!K){N("timeout")}},M.timeout)}}try{J.send(M.data)}catch(S){o.handleError(M,J,null,S)}if(!M.async){N()}function I(){if(M.success){M.success(V,R)}if(M.global){o.event.trigger("ajaxSuccess",[J,M])}}function L(){if(M.complete){M.complete(J,R)}if(M.global){o.event.trigger("ajaxComplete",[J,M])}if(M.global&&!--o.active){o.event.trigger("ajaxStop")}}return J},handleError:function(F,H,E,G){if(F.error){F.error(H,E,G)}if(F.global){o.event.trigger("ajaxError",[H,F,G])}},active:0,httpSuccess:function(F){try{return !F.status&&location.protocol=="file:"||(F.status>=200&&F.status<300)||F.status==304||F.status==1223}catch(E){}return false},httpNotModified:function(G,E){try{var H=G.getResponseHeader("Last-Modified");return G.status==304||H==o.lastModified[E]}catch(F){}return false},httpData:function(J,H,G){var F=J.getResponseHeader("content-type"),E=H=="xml"||!H&&F&&F.indexOf("xml")>=0,I=E?J.responseXML:J.responseText;if(E&&I.documentElement.tagName=="parsererror"){throw"parsererror"}if(G&&G.dataFilter){I=G.dataFilter(I,H)}if(typeof I==="string"){if(H=="script"){o.globalEval(I)}if(H=="json"){I=l["eval"]("("+I+")")}}return I},param:function(E){var G=[];function H(I,J){G[G.length]=encodeURIComponent(I)+"="+encodeURIComponent(J)}if(o.isArray(E)||E.jquery){o.each(E,function(){H(this.name,this.value)})}else{for(var F in E){if(o.isArray(E[F])){o.each(E[F],function(){H(F,this)})}else{H(F,o.isFunction(E[F])?E[F]():E[F])}}}return G.join("&").replace(/%20/g,"+")}});var m={},n,d=[["height","marginTop","marginBottom","paddingTop","paddingBottom"],["width","marginLeft","marginRight","paddingLeft","paddingRight"],["opacity"]];function t(F,E){var G={};o.each(d.concat.apply([],d.slice(0,E)),function(){G[this]=F});return G}o.fn.extend({show:function(J,L){if(J){return this.animate(t("show",3),J,L)}else{for(var H=0,F=this.length;H<F;H++){var E=o.data(this[H],"olddisplay");this[H].style.display=E||"";if(o.css(this[H],"display")==="none"){var G=this[H].tagName,K;if(m[G]){K=m[G]}else{var I=o("<"+G+" />").appendTo("body");K=I.css("display");if(K==="none"){K="block"}I.remove();m[G]=K}o.data(this[H],"olddisplay",K)}}for(var H=0,F=this.length;H<F;H++){this[H].style.display=o.data(this[H],"olddisplay")||""}return this}},hide:function(H,I){if(H){return this.animate(t("hide",3),H,I)}else{for(var G=0,F=this.length;G<F;G++){var E=o.data(this[G],"olddisplay");if(!E&&E!=="none"){o.data(this[G],"olddisplay",o.css(this[G],"display"))}}for(var G=0,F=this.length;G<F;G++){this[G].style.display="none"}return this}},_toggle:o.fn.toggle,toggle:function(G,F){var E=typeof G==="boolean";return o.isFunction(G)&&o.isFunction(F)?this._toggle.apply(this,arguments):G==null||E?this.each(function(){var H=E?G:o(this).is(":hidden");o(this)[H?"show":"hide"]()}):this.animate(t("toggle",3),G,F)},fadeTo:function(E,G,F){return this.animate({opacity:G},E,F)},animate:function(I,F,H,G){var E=o.speed(F,H,G);return this[E.queue===false?"each":"queue"](function(){var K=o.extend({},E),M,L=this.nodeType==1&&o(this).is(":hidden"),J=this;for(M in I){if(I[M]=="hide"&&L||I[M]=="show"&&!L){return K.complete.call(this)}if((M=="height"||M=="width")&&this.style){K.display=o.css(this,"display");K.overflow=this.style.overflow}}if(K.overflow!=null){this.style.overflow="hidden"}K.curAnim=o.extend({},I);o.each(I,function(O,S){var R=new o.fx(J,K,O);if(/toggle|show|hide/.test(S)){R[S=="toggle"?L?"show":"hide":S](I)}else{var Q=S.toString().match(/^([+-]=)?([\d+-.]+)(.*)$/),T=R.cur(true)||0;if(Q){var N=parseFloat(Q[2]),P=Q[3]||"px";if(P!="px"){J.style[O]=(N||1)+P;T=((N||1)/R.cur(true))*T;J.style[O]=T+P}if(Q[1]){N=((Q[1]=="-="?-1:1)*N)+T}R.custom(T,N,P)}else{R.custom(T,S,"")}}});return true})},stop:function(F,E){var G=o.timers;if(F){this.queue([])}this.each(function(){for(var H=G.length-1;H>=0;H--){if(G[H].elem==this){if(E){G[H](true)}G.splice(H,1)}}});if(!E){this.dequeue()}return this}});o.each({slideDown:t("show",1),slideUp:t("hide",1),slideToggle:t("toggle",1),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"}},function(E,F){o.fn[E]=function(G,H){return this.animate(F,G,H)}});o.extend({speed:function(G,H,F){var E=typeof G==="object"?G:{complete:F||!F&&H||o.isFunction(G)&&G,duration:G,easing:F&&H||H&&!o.isFunction(H)&&H};E.duration=o.fx.off?0:typeof E.duration==="number"?E.duration:o.fx.speeds[E.duration]||o.fx.speeds._default;E.old=E.complete;E.complete=function(){if(E.queue!==false){o(this).dequeue()}if(o.isFunction(E.old)){E.old.call(this)}};return E},easing:{linear:function(G,H,E,F){return E+F*G},swing:function(G,H,E,F){return((-Math.cos(G*Math.PI)/2)+0.5)*F+E}},timers:[],fx:function(F,E,G){this.options=E;this.elem=F;this.prop=G;if(!E.orig){E.orig={}}}});o.fx.prototype={update:function(){if(this.options.step){this.options.step.call(this.elem,this.now,this)}(o.fx.step[this.prop]||o.fx.step._default)(this);if((this.prop=="height"||this.prop=="width")&&this.elem.style){this.elem.style.display="block"}},cur:function(F){if(this.elem[this.prop]!=null&&(!this.elem.style||this.elem.style[this.prop]==null)){return this.elem[this.prop]}var E=parseFloat(o.css(this.elem,this.prop,F));return E&&E>-10000?E:parseFloat(o.curCSS(this.elem,this.prop))||0},custom:function(I,H,G){this.startTime=e();this.start=I;this.end=H;this.unit=G||this.unit||"px";this.now=this.start;this.pos=this.state=0;var E=this;function F(J){return E.step(J)}F.elem=this.elem;if(F()&&o.timers.push(F)&&!n){n=setInterval(function(){var K=o.timers;for(var J=0;J<K.length;J++){if(!K[J]()){K.splice(J--,1)}}if(!K.length){clearInterval(n);n=g}},13)}},show:function(){this.options.orig[this.prop]=o.attr(this.elem.style,this.prop);this.options.show=true;this.custom(this.prop=="width"||this.prop=="height"?1:0,this.cur());o(this.elem).show()},hide:function(){this.options.orig[this.prop]=o.attr(this.elem.style,this.prop);this.options.hide=true;this.custom(this.cur(),0)},step:function(H){var G=e();if(H||G>=this.options.duration+this.startTime){this.now=this.end;this.pos=this.state=1;this.update();this.options.curAnim[this.prop]=true;var E=true;for(var F in this.options.curAnim){if(this.options.curAnim[F]!==true){E=false}}if(E){if(this.options.display!=null){this.elem.style.overflow=this.options.overflow;this.elem.style.display=this.options.display;if(o.css(this.elem,"display")=="none"){this.elem.style.display="block"}}if(this.options.hide){o(this.elem).hide()}if(this.options.hide||this.options.show){for(var I in this.options.curAnim){o.attr(this.elem.style,I,this.options.orig[I])}}this.options.complete.call(this.elem)}return false}else{var J=G-this.startTime;this.state=J/this.options.duration;this.pos=o.easing[this.options.easing||(o.easing.swing?"swing":"linear")](this.state,J,0,1,this.options.duration);this.now=this.start+((this.end-this.start)*this.pos);this.update()}return true}};o.extend(o.fx,{speeds:{slow:600,fast:200,_default:400},step:{opacity:function(E){o.attr(E.elem.style,"opacity",E.now)},_default:function(E){if(E.elem.style&&E.elem.style[E.prop]!=null){E.elem.style[E.prop]=E.now+E.unit}else{E.elem[E.prop]=E.now}}}});if(document.documentElement.getBoundingClientRect){o.fn.offset=function(){if(!this[0]){return{top:0,left:0}}if(this[0]===this[0].ownerDocument.body){return o.offset.bodyOffset(this[0])}var G=this[0].getBoundingClientRect(),J=this[0].ownerDocument,F=J.body,E=J.documentElement,L=E.clientTop||F.clientTop||0,K=E.clientLeft||F.clientLeft||0,I=G.top+(self.pageYOffset||o.boxModel&&E.scrollTop||F.scrollTop)-L,H=G.left+(self.pageXOffset||o.boxModel&&E.scrollLeft||F.scrollLeft)-K;return{top:I,left:H}}}else{o.fn.offset=function(){if(!this[0]){return{top:0,left:0}}if(this[0]===this[0].ownerDocument.body){return o.offset.bodyOffset(this[0])}o.offset.initialized||o.offset.initialize();var J=this[0],G=J.offsetParent,F=J,O=J.ownerDocument,M,H=O.documentElement,K=O.body,L=O.defaultView,E=L.getComputedStyle(J,null),N=J.offsetTop,I=J.offsetLeft;while((J=J.parentNode)&&J!==K&&J!==H){M=L.getComputedStyle(J,null);N-=J.scrollTop,I-=J.scrollLeft;if(J===G){N+=J.offsetTop,I+=J.offsetLeft;if(o.offset.doesNotAddBorder&&!(o.offset.doesAddBorderForTableAndCells&&/^t(able|d|h)$/i.test(J.tagName))){N+=parseInt(M.borderTopWidth,10)||0,I+=parseInt(M.borderLeftWidth,10)||0}F=G,G=J.offsetParent}if(o.offset.subtractsBorderForOverflowNotVisible&&M.overflow!=="visible"){N+=parseInt(M.borderTopWidth,10)||0,I+=parseInt(M.borderLeftWidth,10)||0}E=M}if(E.position==="relative"||E.position==="static"){N+=K.offsetTop,I+=K.offsetLeft}if(E.position==="fixed"){N+=Math.max(H.scrollTop,K.scrollTop),I+=Math.max(H.scrollLeft,K.scrollLeft)}return{top:N,left:I}}}o.offset={initialize:function(){if(this.initialized){return}var L=document.body,F=document.createElement("div"),H,G,N,I,M,E,J=L.style.marginTop,K='<div style="position:absolute;top:0;left:0;margin:0;border:5px solid #000;padding:0;width:1px;height:1px;"><div></div></div><table style="position:absolute;top:0;left:0;margin:0;border:5px solid #000;padding:0;width:1px;height:1px;" cellpadding="0" cellspacing="0"><tr><td></td></tr></table>';M={position:"absolute",top:0,left:0,margin:0,border:0,width:"1px",height:"1px",visibility:"hidden"};for(E in M){F.style[E]=M[E]}F.innerHTML=K;L.insertBefore(F,L.firstChild);H=F.firstChild,G=H.firstChild,I=H.nextSibling.firstChild.firstChild;this.doesNotAddBorder=(G.offsetTop!==5);this.doesAddBorderForTableAndCells=(I.offsetTop===5);H.style.overflow="hidden",H.style.position="relative";this.subtractsBorderForOverflowNotVisible=(G.offsetTop===-5);L.style.marginTop="1px";this.doesNotIncludeMarginInBodyOffset=(L.offsetTop===0);L.style.marginTop=J;L.removeChild(F);this.initialized=true},bodyOffset:function(E){o.offset.initialized||o.offset.initialize();var G=E.offsetTop,F=E.offsetLeft;if(o.offset.doesNotIncludeMarginInBodyOffset){G+=parseInt(o.curCSS(E,"marginTop",true),10)||0,F+=parseInt(o.curCSS(E,"marginLeft",true),10)||0}return{top:G,left:F}}};o.fn.extend({position:function(){var I=0,H=0,F;if(this[0]){var G=this.offsetParent(),J=this.offset(),E=/^body|html$/i.test(G[0].tagName)?{top:0,left:0}:G.offset();J.top-=j(this,"marginTop");J.left-=j(this,"marginLeft");E.top+=j(G,"borderTopWidth");E.left+=j(G,"borderLeftWidth");F={top:J.top-E.top,left:J.left-E.left}}return F},offsetParent:function(){var E=this[0].offsetParent||document.body;while(E&&(!/^body|html$/i.test(E.tagName)&&o.css(E,"position")=="static")){E=E.offsetParent}return o(E)}});o.each(["Left","Top"],function(F,E){var G="scroll"+E;o.fn[G]=function(H){if(!this[0]){return null}return H!==g?this.each(function(){this==l||this==document?l.scrollTo(!F?H:o(l).scrollLeft(),F?H:o(l).scrollTop()):this[G]=H}):this[0]==l||this[0]==document?self[F?"pageYOffset":"pageXOffset"]||o.boxModel&&document.documentElement[G]||document.body[G]:this[0][G]}});o.each(["Height","Width"],function(I,G){var E=I?"Left":"Top",H=I?"Right":"Bottom",F=G.toLowerCase();o.fn["inner"+G]=function(){return this[0]?o.css(this[0],F,false,"padding"):null};o.fn["outer"+G]=function(K){return this[0]?o.css(this[0],F,false,K?"margin":"border"):null};var J=G.toLowerCase();o.fn[J]=function(K){return this[0]==l?document.compatMode=="CSS1Compat"&&document.documentElement["client"+G]||document.body["client"+G]:this[0]==document?Math.max(document.documentElement["client"+G],document.body["scroll"+G],document.documentElement["scroll"+G],document.body["offset"+G],document.documentElement["offset"+G]):K===g?(this.length?o.css(this[0],J):null):this.css(J,typeof K==="string"?K:K+"px")}})})();
jQuery.noConflict();
/**
 * Flash (http://jquery.lukelutman.com/plugins/flash)
 * A jQuery plugin for embedding Flash movies.
 * 
 * Version 1.0
 * November 9th, 2006
 *
 * Copyright (c) 2006 Luke Lutman (http://www.lukelutman.com)
 * Dual licensed under the MIT and GPL licenses.
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.opensource.org/licenses/gpl-license.php
 * 
 * Inspired by:
 * SWFObject (http://blog.deconcept.com/swfobject/)
 * UFO (http://www.bobbyvandersluis.com/ufo/)
 * sIFR (http://www.mikeindustries.com/sifr/)
 * 
 * IMPORTANT: 
 * The packed version of jQuery breaks ActiveX control
 * activation in Internet Explorer. Use JSMin to minifiy
 * jQuery (see: http://jquery.lukelutman.com/plugins/flash#activex).
 *
 **/ 
;(function(){
	
var $$;

/**
 * 
 * @desc Replace matching elements with a flash movie.
 * @author Luke Lutman
 * @version 1.0.1
 *
 * @name flash
 * @param Hash htmlOptions Options for the embed/object tag.
 * @param Hash pluginOptions Options for detecting/updating the Flash plugin (optional).
 * @param Function replace Custom block called for each matched element if flash is installed (optional).
 * @param Function update Custom block called for each matched if flash isn't installed (optional).
 * @type jQuery
 *
 * @cat plugins/flash
 * 
 * @example $('#hello').flash({ src: 'hello.swf' });
 * @desc Embed a Flash movie.
 *
 * @example $('#hello').flash({ src: 'hello.swf' }, { version: 8 });
 * @desc Embed a Flash 8 movie.
 *
 * @example $('#hello').flash({ src: 'hello.swf' }, { expressInstall: true });
 * @desc Embed a Flash movie using Express Install if flash isn't installed.
 *
 * @example $('#hello').flash({ src: 'hello.swf' }, { update: false });
 * @desc Embed a Flash movie, don't show an update message if Flash isn't installed.
 *
**/
$$ = jQuery.fn.flash = function(htmlOptions, pluginOptions, replace, update) {
	
	// Set the default block.
	var block = replace || $$.replace;
	
	// Merge the default and passed plugin options.
	pluginOptions = $$.copy($$.pluginOptions, pluginOptions);
	
	// Detect Flash.
	if(!$$.hasFlash(pluginOptions.version)) {
		// Use Express Install (if specified and Flash plugin 6,0,65 or higher is installed).
		if(pluginOptions.expressInstall && $$.hasFlash(6,0,65)) {
			// Add the necessary flashvars (merged later).
			var expressInstallOptions = {
				flashvars: {  	
					MMredirectURL: location,
					MMplayerType: 'PlugIn',
					MMdoctitle: jQuery('title').text() 
				}					
			};
		// Ask the user to update (if specified).
		} else if (pluginOptions.update) {
			// Change the block to insert the update message instead of the flash movie.
			block = update || $$.update;
		// Fail
		} else {
			// The required version of flash isn't installed.
			// Express Install is turned off, or flash 6,0,65 isn't installed.
			// Update is turned off.
			// Return without doing anything.
			return this;
		}
	}
	
	// Merge the default, express install and passed html options.
	htmlOptions = $$.copy($$.htmlOptions, expressInstallOptions, htmlOptions);
	
	// Invoke $block (with a copy of the merged html options) for each element.
	return this.each(function(){
		block.call(this, $$.copy(htmlOptions));
	});
	
};
/**
 *
 * @name flash.copy
 * @desc Copy an arbitrary number of objects into a new object.
 * @type Object
 * 
 * @example $$.copy({ foo: 1 }, { bar: 2 });
 * @result { foo: 1, bar: 2 };
 *
**/
$$.copy = function() {
	var options = {}, flashvars = {};
	for(var i = 0; i < arguments.length; i++) {
		var arg = arguments[i];
		if(arg == undefined) continue;
		jQuery.extend(options, arg);
		// don't clobber one flash vars object with another
		// merge them instead
		if(arg.flashvars == undefined) continue;
		jQuery.extend(flashvars, arg.flashvars);
	}
	options.flashvars = flashvars;
	return options;
};
/*
 * @name flash.hasFlash
 * @desc Check if a specific version of the Flash plugin is installed
 * @type Boolean
 *
**/
$$.hasFlash = function() {
	// look for a flag in the query string to bypass flash detection
	if(/hasFlash\=true/.test(location)) return true;
	if(/hasFlash\=false/.test(location)) return false;
	var pv = $$.hasFlash.playerVersion().match(/\d+/g);
	var rv = String([arguments[0], arguments[1], arguments[2]]).match(/\d+/g) || String($$.pluginOptions.version).match(/\d+/g);
	for(var i = 0; i < 3; i++) {
		pv[i] = parseInt(pv[i] || 0);
		rv[i] = parseInt(rv[i] || 0);
		// player is less than required
		if(pv[i] < rv[i]) return false;
		// player is greater than required
		if(pv[i] > rv[i]) return true;
	}
	// major version, minor version and revision match exactly
	return true;
};
/**
 *
 * @name flash.hasFlash.playerVersion
 * @desc Get the version of the installed Flash plugin.
 * @type String
 *
**/
$$.hasFlash.playerVersion = function() {
	// ie
	try {
		try {
			// avoid fp6 minor version lookup issues
			// see: http://blog.deconcept.com/2006/01/11/getvariable-setvariable-crash-internet-explorer-flash-6/
			var axo = new ActiveXObject('ShockwaveFlash.ShockwaveFlash.6');
			try { axo.AllowScriptAccess = 'always';	} 
			catch(e) { return '6,0,0'; }				
		} catch(e) {}
		return new ActiveXObject('ShockwaveFlash.ShockwaveFlash').GetVariable('$version').replace(/\D+/g, ',').match(/^,?(.+),?$/)[1];
	// other browsers
	} catch(e) {
		try {
			if(navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin){
				return (navigator.plugins["Shockwave Flash 2.0"] || navigator.plugins["Shockwave Flash"]).description.replace(/\D+/g, ",").match(/^,?(.+),?$/)[1];
			}
		} catch(e) {}		
	}
	return '0,0,0';
};
/**
 *
 * @name flash.htmlOptions
 * @desc The default set of options for the object or embed tag.
 *
**/
$$.htmlOptions = {
	height: 240,
	flashvars: {},
	pluginspage: 'http://www.adobe.com/go/getflashplayer',
	src: '#',
	type: 'application/x-shockwave-flash',
	width: 320		
};
/**
 *
 * @name flash.pluginOptions
 * @desc The default set of options for checking/updating the flash Plugin.
 *
**/
$$.pluginOptions = {
	expressInstall: false,
	update: true,
	version: '6.0.65'
};
/**
 *
 * @name flash.replace
 * @desc The default method for replacing an element with a Flash movie.
 *
**/
$$.replace = function(htmlOptions) {
	this.innerHTML = '<div class="alt">'+this.innerHTML+'</div>';
	jQuery(this)
		.addClass('flash-replaced')
		.prepend($$.transform(htmlOptions));
};
/**
 *
 * @name flash.update
 * @desc The default method for replacing an element with an update message.
 *
**/
$$.update = function(htmlOptions) {
	var url = String(location).split('?');
	url.splice(1,0,'?hasFlash=true&');
	url = url.join('');
	var msg = '<p>This content requires the Flash Player. <a href="http://www.adobe.com/go/getflashplayer">Download Flash Player</a>. Already have Flash Player? <a href="'+url+'">Click here.</a></p>';
	this.innerHTML = '<span class="alt">'+this.innerHTML+'</span>';
	jQuery(this)
		.addClass('flash-update')
		.prepend(msg);
};
/**
 *
 * @desc Convert a hash of html options to a string of attributes, using Function.apply(). 
 * @example toAttributeString.apply(htmlOptions)
 * @result foo="bar" foo="bar"
 *
**/
function toAttributeString() {
	var s = '';
	for(var key in this)
		if(typeof this[key] != 'function')
			s += key+'="'+this[key]+'" ';
	return s;		
};
/**
 *
 * @desc Convert a hash of flashvars to a url-encoded string, using Function.apply(). 
 * @example toFlashvarsString.apply(flashvarsObject)
 * @result foo=bar&foo=bar
 *
**/
function toFlashvarsString() {
	var s = '';
	for(var key in this)
		if(typeof this[key] != 'function')
			s += key+'='+encodeURIComponent(this[key])+'&';
	return s.replace(/&$/, '');		
};
/**
 *
 * @name flash.transform
 * @desc Transform a set of html options into an embed tag.
 * @type String 
 *
 * @example $$.transform(htmlOptions)
 * @result <embed src="foo.swf" ... />
 *
 * Note: The embed tag is NOT standards-compliant, but it 
 * works in all current browsers. flash.transform can be
 * overwritten with a custom function to generate more 
 * standards-compliant markup.
 *
**/
$$.transform = function(htmlOptions) {
	htmlOptions.toString = toAttributeString;
	if(htmlOptions.flashvars) htmlOptions.flashvars.toString = toFlashvarsString;
	return '<embed ' + String(htmlOptions) + '/>';		
};

/**
 *
 * Flash Player 9 Fix (http://blog.deconcept.com/2006/07/28/swfobject-143-released/)
 *
**/
if (window.attachEvent) {
	window.attachEvent("onbeforeunload", function(){
		__flash_unloadHandler = function() {};
		__flash_savedUnloadHandler = function() {};
	});
}
	
})();
(function(a){a.fn.asdtv_slider=function(h){var e=a(this),f=this,b=e.width();this.params=a.extend({offsetLeft:23,knob:false,next:"",prev:"",PerPage:4,Pages:1,Length:1,view:""},h);this.params.knob=a(this.params.knob,e);this.params.next=a(this.params.next,e);this.params.prev=a(this.params.prev,e);var c=this.params.knob.width();this.params.knob.css({left:this.params.offsetLeft});this.params.view.css({position:"relative"});var g=0;this.update=function(i){i=f.knobPosition(i);if(!i){return}i=i.viewLeft;if(f.inProgress){f.params.view.stop().css({left:"-"+i+"px"})}else{f.params.view.stop().animate({left:"-"+i+"px"})}};this.knobPosition=function(l){if(!l){return false}var k=(l)-f.params.offsetLeft;var m=b-(c)-f.params.offsetLeft*2;var j=(k/(m/100));var i=f.params.view.find("li");var l=((i.eq(0).width()*i.size())/100)*j;return{knob:k,view:m,viewLeft:l,pct:j,lis:i}};this.updateKnob=function(k,j){if(k.pageX){k=k.pageX}j=j||1;var i=parseInt(k)-f.params.offsetLeft;if(i<f.params.offsetLeft){i=f.params.offsetLeft}else{if(i>=b-f.params.offsetLeft-c){i=b-f.params.offsetLeft-c}}f.params.knob.stop().animate({left:i},j,function(){});f.update(parseInt(i))};var d=false;e.hover(function(i){},function(){d=false;f.inProgress=false;f.update()}).mousemove(function(i){if(!d){return}i.pageX=i.pageX-e.offset().left;f.inProgress=true;f.updateKnob(i,false)}).mousedown(function(){d=true}).mouseup(function(){f.inProgress=false;d=false}).click(function(i){if(i&&i.target&&(a(i.target).hasClass("asdtv_prev")||a(i.target).hasClass("asdtv_next"))){return false}f.inProgress=false;d=false;i.pageX=i.pageX-e.offset().left;f.updateKnob(i.pageX,500)});f.params.knob.attr("unselectable","on").css("MozUserSelect","none");this.params.next.click(function(){var o=f.params.knob.offset().left-e.offset().left+f.params.offsetLeft;var m=f.knobPosition(o);var n=m.lis.eq(0).width();var k=n*m.lis.size();var j=(n/(k/100))*(f.params.PerPage-1);var i=((e.width()-f.params.offsetLeft*2)/100);o+=j*i;f.updateKnob(o,500);f.inProgress=false;d=false;return false});this.params.prev.click(function(){var o=f.params.knob.offset().left-e.offset().left+f.params.offsetLeft;var m=f.knobPosition(o);var n=m.lis.eq(0).width();var k=n*m.lis.size();var j=(n/(k/100))*(f.params.PerPage-1);var i=((e.width()-f.params.offsetLeft*2)/100);o-=j*i;f.updateKnob(o,500);f.inProgress=false;d=false;return false});return this}})(jQuery);(function(d){d.fn.asdtv_carousel=function(h){h=d.extend({btnPrev:null,btnNext:null,btnGo:null,mouseWheel:false,auto:null,speed:200,easing:null,vertical:false,circular:true,visible:3,start:0,scroll:1,beforeStart:null,afterEnd:null,disabledClass:"disabled",enabledClass:"enabled"},h||{});var i=h.scroll,g=false,f=h.visible;return this.each(function(){var s=false,q=(h.vertical?"top":"left"),k=(h.vertical?"height":"width"),j=d(this),u=d("ul:first",j),m=d(">li",u),z=m.size(),y=h.visible;if(m.size()>h.visible){if(h.circular){u.prepend(m.slice(z-y-1+1).clone()).append(m.slice(0,y).clone());h.start+=y}}if(((m.size()/2)<=h.scroll)&&(h.scroll>1)){h.scroll--}var x=d(">li",u),t=x.size(),A=h.start;j.css("visibility","visible");x.css({overflow:"hidden","float":(h.vertical?"none":"left")});u.css({margin:"0",padding:"0",position:"relative","list-style-type":"none","z-index":"1",left:"0px",width:"0px"});j.css({overflow:"hidden",position:"relative","z-index":"2",left:"0px",display:"block"});var p=h.vertical?a(x):c(x);var w=p*t;var r=p*y;x.css({width:x.width(),height:x.height()});u.css(k,w+"px").css(q,-(A*p));j.css(k,r+"px");if(m.size()>h.visible){d(h.btnPrev).removeClass(h.disabledClass).addClass(h.enabledClass);d(h.btnNext).removeClass(h.disabledClass).addClass(h.enabledClass);if(h.btnPrev){d(h.btnPrev).unbind("click").click(function(){return o(A-h.scroll)})}if(h.btnNext){d(h.btnNext).unbind("click").click(function(){return o(A+h.scroll)})}if(h.btnGo){d.each(h.btnGo,function(v,B){d(B).click(function(){return o(h.circular?h.visible+v:v)})})}if(h.mouseWheel&&j.mousewheel){j.mousewheel(function(v,B){return B>0?o(A-h.scroll):o(A+h.scroll)})}if(h.auto){setInterval(function(){o(A+h.scroll)},h.auto+h.speed)}}else{d(h.btnPrev).addClass(h.disabledClass).removeClass(h.enabledClass);d(h.btnNext).addClass(h.disabledClass).removeClass(h.enabledClass)}function l(){return x.slice(A).slice(0,y)}function o(B){if(!s){if(h.beforeStart){h.beforeStart.call(this,l())}var v=A;if(h.circular){if(B<=h.start-y-1){u.css(q,-((t-(y*2))*p)+"px");A=B==h.start-y-1?t-(y*2)-1:t-(y*2)-h.scroll}else{if(B>=t-y+1){u.css(q,-((y)*p)+"px");A=B==t-y+1?y+1:y+h.scroll}else{A=B}}}else{if(B<0||B>t-y){return}else{A=B}}n(v,A);s=true;u.animate(q=="left"?{left:-(A*p)}:{top:-(A*p)},h.speed,h.easing,function(){if(h.afterEnd){h.afterEnd.call(this,l())}s=false});if(!h.circular){d(h.btnPrev).removeClass(h.disabledClass).addClass(h.enabledClass);d(h.btnNext).removeClass(h.disabledClass).addClass(h.enabledClass);if(A-h.scroll<0){d(h.btnPrev).addClass(h.disabledClass).removeClass(h.enabledClass)}if(A+h.scroll>(t-y)){h.btnNext.addClass(h.disabledClass).remvoveClass(h.enabledClass)}}if(g==true){asdtv_brightcove.ivwcount()}else{g=true}}return false}function n(B,v){x.asdtv_activateImages(v,v+f)}n(0,i)})};d.fn.asdtv_activateImages=function(g,f){g=g||0;f=f||10000;this.find("img").slice(g,f).each(function(){var h=d(this);if(h.attr("data")&&!h.attr("src")){h.attr("src",h.attr("data"))}});return this};function b(f,g){if(!f||!f[0]){return 0}return parseInt(d.css(f[0],g))||0}function c(f){return(!f||!f[0]?0:f[0].offsetWidth+b(f,"marginLeft")+b(f,"marginRight"))}function a(f){return(!f||!f[0]?0:f[0].offsetHeight+b(f,"marginTop")+b(f,"marginBottom"))}var e=d('<div class="asdtv_overlay"></div>');d.fn.asdtv_overlay=function(f){f=f==true?true:false;return this.each(function(g,h){h=d(h);if(f==true){h.parent().hover(function(){e.insertBefore(h).css({width:h.width(),height:h.height(),position:"absolute",display:"block"})},function(){e.css("display","none")})}else{e.clone().insertBefore(h).css({width:h.width(),height:h.height(),position:"absolute",display:"block"})}})}})(jQuery);(function(p){var d=p;var u="http://api.brightcove.com/services/library",n=[],g={find_related_videos:false};var r=function(){};window.asdtv_brightcove=r;r.__revision__="$Revision: 282 $";r.api=function(e){this.token=e;n.push(this)};r.api.get=function(e){e=e||(n.length-1);return n[e]};r.api.canUse=function(j,e){if(g[j]!=undefined){if(e!=undefined){g[j]=e==true?true:false}else{return g[j]==true?true:false}}};r.api.prototype.data=function(y,x){var E="";if(y.who){E=y.who+".";delete y.who}y=r.hook(E+"brightcove.params."+y.command,y);var F=[],C=this,z=(y.all_pages==true)?true:false;delete y.all_pages;p.each(y,function(H,G){if(H!=undefined&&G!=undefined){F.push(C.encode(H)+"="+C.encode(G))}});F.push("token="+this.token);var e={items:[],videos:[]};var A=true,w=0,B=0;function D(){p.ajax({url:u,cache:true,data:F.join("&")+"&page_number="+B,dataType:"jsonp",contentType:"utf-8",scriptCharset:"utf-8",success:function(G){j(G)},error:x})}function j(G){if(G&&G.error==undefined){if(z==true){B++;if(A==true){A=false;w=Math.ceil(G.total_count/G.page_size);e=d.extend(e,G)}else{if(G.items){jQuery.each(G.items,function(H,I){e.items=e.items||[];e.items.push(I)})}if(G.videos){jQuery.each(G.videos,function(H,I){e.videos=e.videos||[];e.videos.push(I)})}}if(B>=w){x(r.hook(E+"brightcove.data."+y.command,e))}else{D()}}else{x(r.hook(E+"brightcove.data."+y.command,G))}}}D()};r.api.prototype.encode=function(e){switch(typeof(e)){case"array":return e.join(",");break;case"string":default:return encodeURIComponent(e);break}};r.api.prototype.playlists=function(j,e,w){if(!j||j.length==0){return this.allplaylists(e,w)}this.data(p.extend({command:"find_playlists_by_ids",playlist_ids:j,playlist_fields:"id,name,filterTags,videos,playlistType"},w||{}),e)};r.api.prototype.playlist=function(w,e,j){this.data(p.extend({command:"find_playlist_by_id",playlist_id:w,playlist_fields:"id,name,filterTags,videos,playlistType"},j||{}),e)};r.api.prototype.allplaylists=function(e,j){this.data(p.extend({command:"find_all_playlists",page_size:"25"},j||{}),e)};r.api.prototype.video=function(w,e,j){this.data(p.extend({command:"find_video_by_id",video_id:w,video_fields:"id,name,shortDescription,linkURL,thumbnailURL,TAGS"},j||{}),e)};r.api.prototype.related=function(w,e,j){if(typeof(w)=="number"||parseInt(w)==w&&r.api.canUse("find_related_videos")){this.data(d.extend({command:"find_related_videos",video_id:w,page_size:25},j||{}),e)}else{this.data(d.extend({command:"find_related_videos",reference_id:w,page_size:25},j||{}),e)}};r.api.prototype.tags=function(j,e,w){and_tags=[];or_tags=[];if(typeof(j)=="array"){or_tags=j}else{and_tags=undefined;or_tags=j.replace("/[+]/gmi","").split(",")}d.each(or_tags,function(x,y){if(y.indexOf("+")>-1){if(!and_tags){and_tags=[]}and_tags=y.split("+");delete or_tags[x]}});this.data(d.extend({command:"find_videos_by_tags",get_item_count:"true",and_tags:and_tags,or_tags:or_tags,page_size:25},w||{}),e)};r.api.prototype.search=function(w,e,j){this.data(d.extend({command:"find_videos_by_text",text:w,page_size:25,video_fields:"id,name,shortDescription,linkURL,thumbnailURL,publishedDate,TAGS"},j||{}),function(x){x.items=r.filter_video_by_tags(x.items);e(x)})};r.api.prototype.byPlayer=function(w,e,j){this.data(d.extend({command:"find_playlists_for_player_id",get_item_count:"true",player_id:w,page_size:25,fields:"id,name,videos",video_fields:"id,name,shortDescription,linkURL,thumbnailURL"},j||{}),e)};var s=1,v={allowScriptAccess:"sameDomain",menu:"false",quality:"high",align:"middle",allowFullScreen:"true",wmode:"opaque"},i=["allowScriptAccess","menu","quality","flashvars","allowFullScreen","wmode","width","height","src","align"],m={};r.player=function(y,e){y.playerID=y.playerID?y.playerID:m.playerID;if(y.playerID&&parseInt(y.playerID)==y.playerID&&window.brightcove!=undefined){s++;y=d.extend(m,y);y.linkBaseURL=y.itemURL;y["@videoPlayer"]=y.videoId;var D=brightcove.createElement("object");D.id="asdtvBrightcove"+s;var B;for(var z in y){if(!y[z]||y[z]==undefined){continue}B=brightcove.createElement("param");B.name=z;B.value=y[z];D.appendChild(B)}e=d(e).empty().get(0);brightcove.createExperience(D,e,true)}else{s++;var C=r.utilities.copyObject(d.extend(v,y));C.src=C.src?C.src:C.playerID;C["@videoPlayer"]=y.videoId;var w=d.extend(C,C.flashvars||{}),A={};if(w.flashvars){delete (w.flashvars)}if(!C.width){C.width=e.width()}if(!C.height){C.height=e.height()}w.deeplinkUrl=y.itemURL;if(w.mediaclip){if(w.deeplinkUrl){var E=w.deeplinkUrl.match(/([\d]+)/gmi);w.mediaclip=w.mediaclip+E}else{w.mediaclip=w.mediaclip+y.videoId}}else{w.mediaclip=y.videoId}if(w.autoStart){w.autoPlay=w.autoStart}else{if(w.autoPlay){w.autoStart=w.autoPlay}}for(var x in C){if(r.utilities.iN(x,i)){A[x]=C[x]}}var j={};for(var x in w){if(!r.utilities.iN(x,i)){j[x]=w[x]}}A.flashvars=j;e.empty().flash(A,{version:8,expressInstall:true})}};r.player.setDefaultParams=function(e){v=e};r.player.setDefaultBrightcoveParams=function(e){m=e};var a=[],h=false,c=(new Date).getTime(),o=1;r.ivwcount=function(){if(h==false){return}var e=(new Date).getTime();if(Math.round(c/o)==Math.round(e/o)){return}c=e;d.each(a,function(j,w){if(w.apply){w.apply(window,arguments)}})};r.ivwcount.addCounter=function(e){a.push(e)};r.ivwcount.clean=function(){a=[]};r.ivwcount.enable=function(){h=true;c=(new Date).getTime()};r.ivwcount.disable=function(){h=false};r.enabled_tag=false;r.filter_video_by_tags=function(j){if(!r.enabled_tag){return j}var e=[];d.each(j,function(x,w){if(w.tags){if(r.utilities.contains(w.tags,r.enabled_tag)){e.push(w)}}else{e.push(w)}});return e};var f={};r.register_hook=function(e,j){if(!f[e]){f[e]=[]}f[e].push(j);return r};r.unregister_hook=function(j,w){if(!w){delete f[j]}else{if(f[j]){var e;for(e=0;e<f[j].length;e++){if(f[j][e]==fn){delete f[j][e]}}}}return r};r.hook=function(x,j){if(!f[x]){return j}var w;for(w=0;w<f[x].length;w++){try{j=f[x][w].apply(r,[j])}catch(y){}}return j};r.staticPicture="./lib/img/spacer.png";var b={teaserview:function(e){e.thumbnailURL=e.thumbnailURL||r.staticPicture;if(e.linkURL&&e.linkURL.indexOf("javascript:")==-1&&e.linkURL.indexOf("#autoplay")==-1){e.linkURL+="#autoplay"}return d('<li class="asdtv_videoteaser"><a href="'+e.linkURL+'"><img data="'+e.thumbnailURL+'" /></a><span class="asdtv_teasertextlink"><a href="'+e.linkURL+'#autoplay">'+r.utilities.ie6.decode(e.name)+"</a></span></li>").data(r.css.data,e)},sidebar_vertical_teaserview:function(j){j.thumbnailURL=j.thumbnailURL||r.staticPicture;j.shortDescription=j.shortDescription.slice(0,25);if(j.linkURL&&j.linkURL.indexOf("javascript:")==-1&&j.linkURL.indexOf("#autoplay")==-1){j.linkURL+="#autoplay"}var e="";e+="<li>";e+='	<a href="'+j.linkURL+'" title="">';e+='		<span class="asdtv_teaserimg asdtv_floatleft">';e+='			<img class="asdtv_teaserimagelink asdtv_floatleft" data="'+j.thumbnailURL+'" />';e+="		</span>";e+='		<span class="asdtv_category">'+r.utilities.ie6.decode(j.shortDescription)+"</span>";e+='		<span class="asdtv_teasertextlink">'+r.utilities.ie6.decode(j.name)+"</span>";e+="	</a>";e+="</li>";return d(e).data(r.css.data,j)},tab:function(e){return d('<li class="asdtv_tab"><a href="#" title="">'+r.utilities.ie6.decode(e.name)+"</a></li>").data(r.css.data,e)},category_list_item:function(e){return d('<li class="asdtv_category"><a href="#" title="">'+r.utilities.ie6.decode(e.name)+"</a></li>").data(r.css.data,e)},"2row_item":function(e){e.shortDescription=e.shortDescription.slice(0,25);e.thumbnailURL=e.thumbnailURL||r.staticPicture;if(e.linkURL&&e.linkURL.indexOf("javascript:")==-1&&e.linkURL.indexOf("#autoplay")==-1){e.linkURL+="#autoplay"}return d('<li class="asdtv_videoteaser"><div class="asdtv_videoteasercontent"><a href="'+e.linkURL+'"><img src="'+e.thumbnailURL+'" /></a><span class="asdtv_teasertextlink"><a href="'+e.linkURL+'"><strong>'+r.utilities.ie6.decode(e.shortDescription)+"</strong><br/>"+r.utilities.ie6.decode(e.name)+"</a></span></div></li>").data(r.css.data,e)},"1row_item":function(e){e.shortDescription=e.shortDescription.slice(0,25);e.thumbnailURL=e.thumbnailURL||r.staticPicture;if(e.linkURL&&e.linkURL.indexOf("javascript:")==-1&&e.linkURL.indexOf("#autoplay")==-1){e.linkURL+="#autoplay"}return d('<li class="asdtv_videoteaser"><div class="asdtv_videoteasercontent"><a href="'+e.linkURL+'"><img src="'+e.thumbnailURL+'" /></a><span class="asdtv_teasertextlink"><a href="'+e.linkURL+'"><strong>'+r.utilities.ie6.decode(e.shortDescription)+"</strong><br/>"+r.utilities.ie6.decode(e.name)+"</a></span></div></li>").data(r.css.data,e)},videobuehne_pagination:function(e){return d('<li class="asdtv_page"><a href="#videobuehne/list:'+e.listID+"/page:"+e.page+'">'+e.page+"</a></li>").data(r.css.data,e)},videobuehne:function(e){return d('<ul class="asdtv_videoteaserlist"></ul>').data(r.css.data,e)},videobuehne_coverflow:function(j){var e="";e+='<h3 class="asdtv_videoteaserheadline"></h3>';e+='<ul class="asdtv_videoteaserlist"></ul>';e+='<div class="asdtv_videoteaserslider asdtv_clear"><ul><li class="asdtv_prev"></li><li></li><li class="asdtv_next"></li></ul><div class="asdtv_slidercontroller"></div></div>';return d(e).data(r.css.data,j)},videoplayer:function(y){var x=(new Date(parseInt(y.publishedDate)));var j=r.utilities.twoChars(x.getDate())+"."+r.utilities.twoChars(x.getMonth())+"."+r.utilities.twoChars(x.getFullYear())+", "+r.utilities.twoChars(x.getHours())+":"+r.utilities.twoChars(x.getMinutes());var e="";jQuery.each(y.tags||[],function(A,z){if(z.indexOf("source=")!==-1){e=", Quelle: "+z.replace("source=","")}});y.shortDescription=y.shortDescription.slice(0,25);if(y.container){y.container.find(".asdtv_time").html(j+" Uhr"+r.utilities.ie6.decode(e));y.container.find(".asdtv_videoteaserheadline").html(r.utilities.ie6.decode(y.name));y.container.find(".asdtv_videoteaserdescription").html(r.utilities.ie6.decode(y.longDescription))}else{var w="";w+='<div class="asdtv_moduleheader"><h3>&nbsp;</h3></div>';w+='<div class="asdtv_modulebody"><span class="asdtv_time">'+j+" Uhr"+r.utilities.ie6.decode(e)+'</span><h4 class="asdtv_videoteaserheadline">'+r.utilities.ie6.decode(y.name)+'</h4><p class="asdtv_videoteaserdescription">'+r.utilities.ie6.decode(y.longDescription)+'</p><span class="asdtv_videosource"></span><div class="asdtv_videoteasers"><ul class="asdtv_videoteaserlist"><li class="asdtv_videoteaser asdtv_clear"><div class="stage"></div></li></ul></div></div>'}return d(w).data(r.css.data,y)},searchresult:function(j){var e='<div class="asdtv_searchresult">Ihre Suche nach <span class="asdtv_searchhighlight">"'+j.value+'"</span> hat folgende Ergebnisse gebracht:</div>';return d(e).data(r.css.data,j)}};r.template=function(){};r.template.preprocess=function(e){return e};r.template.render=function(w,j){if(j.items){j.items=r.hook(w,j.items);var e=d("<div></div>");d.each(j.items,function(y,x){e.append(b[w](r.template.preprocess(x)))});return e.children()}else{j=r.hook(w,j);return b[w](r.template.preprocess(j))}};r.css={loading:"asdtv_loading",prev:"asdtv_prev",next:"asdtv_next",active:"asdtv_active",inactive:"asdtv_inactive",data:"asdtv_data",page:"asdtv_page",teasersubnavi:"asdtv_videoteasersubnavi",teaserheadline:"asdtv_videoteaserheadline",teaserlist:"asdtv_videoteaserlist",slider:"asdtv_videoteaserslider",ivw_counter:"asdtv_ivw_counter"};var k={verwandt:"Verwandt"};r.translate=function(e){return k[e]};r.modules=function(){};r.utilities=function(){};r.utilities.unique=function(e){var w=[],x,j=e.length;for(x=0;x<j;x++){if(r.utilities.contains(w,e[x])==false){w.push(e[x])}}return w};r.utilities.contains=function(e,j){for(var w=0;w<e.length;w++){if(e[w]==j){return true}}return false};r.utilities.iN=function(j,e){j=j.toLowerCase();for(var w=0;w<e.length;w++){if(String(e[w]).toLowerCase()==j){return true}}return false};r.utilities.twoChars=function(e){e=String(e);return e.length<2?"0"+e:e};r.utilities.copyObject=function(j){var e={};d.each(j,function(x,w){e[x]=w});return e};var l=false;if(jQuery.browser.msie){try{d("<div></div>").get(0).style.display="table-cell";l=true}catch(q){}}r.utilities.ie6={decode:function(e){return r.utilities.utf8String.decode(e)}};var t={150:"&mdash;",132:"&mdash;"};r.utilities.utf8String={decode:function(x){x=x?String(x):"";var w=0,j="",e="",y=0;while(w<x.length){y=x.charCodeAt(w);if(t[y]!=undefined){x=x.substring(0,w)+t[y]+x.substring(w+1,x.length)}w++}return x}};r.utilities.cookie=function(E,C,j){if(C==undefined){var z,e,C,B=document.cookie.split(";"),F={},A;for(A=0;A<B.length;A=A+1){z=B[A].split("=");e=z[0].replace(/^\s*/,"").replace(/\s*$/,"");C=decodeURIComponent(z[1]);F[e]=C}if(E){return F[E]}return F}else{j=j||{};var D=[],y,x={expires:24*30,path:"/",domain:null,secure:false},w={expires:function(H){if(!H||H=="0"){return"Thu, 01-Jan-1970 00:00:01 GMT"}var G=new Date;G.setTime(G.getTime()+(H*60*60*1000));return G.toGMTString()},secure:function(G){return G?1:0}};for(y in x){if(j[y]){D.push(y+"="+(w[y]?w[y](j[y]):j[y]))}else{if(x[y]){D.push(y+"="+(w[y]?w[y](x[y]):x[y]))}}}document.cookie=E+"="+encodeURIComponent(C)+(D.length==0?"":";")+D.join(";");return r.utilities.cookie}};window.asdtv_brightcove=r})(jQuery);(function(b,c){var e=jQuery,f="content1";b.content1=function(a,h){var g=this;this.params=e.extend({tags:false,carousel:false,relatedVideoID:false,teaserTemplate:"teaserview",onClick:function(){return true},ammount:false},h||{});this.data={items:[]};this.container=e(a);if(this.container.size()==0){return}this.initialized=false;this.api=c.api.get();this.view=this.container.find(".asdtv_videoteasers").unbind("click");this.viewList=this.view.find(".asdtv_videoteaserlist").unbind("click");this.viewBody=this.view.find(".asdtv_videoteaserview").unbind("click");this.tabBody=this.container.find(".asdtv_tabnav").unbind("click");this.container.addClass(c.css.loading);this.loaded=false;this.relatedLoaded=false;this.playlistsLoaded=false;if(this.params.tags){e.each(this.params.tags,function(k,j){var l={};l.id=j;l.name=j;l.type="tag";g.data.items.unshift(l);g.renderTabs()})}else{this.api.playlists(this.params.playlistID,function(i){if(!g.params.relatedVideoID){g.loaded=true}else{if(g.relatedLoaded){g.loaded=true}}g.container.find("."+c.css.loading).removeClass(c.css.loading);if(g.params.ammount){e.each(i.items,function(l,j){if(!i.items[l]){return}i.items[l].type="playlist";if(j.videos!=undefined){i.items[l].videos=j.videos.slice(0,g.params.ammount)}})}g.data.items=e.merge(g.data.items,i.items);g.renderTabs(g.params.relatedVideoID?false:true);g.playlistsLoaded=true},{playlist_fields:"id,name",who:f})}if(this.params.relatedVideoID){this.api.related(this.params.relatedVideoID,function(i){g.relatedLoaded=true;if(g.playlistsLoaded||!g.params.playlistID){g.loaded=true}i.name=asdtv_brightcove.translate("verwandt");i.id=g.params.relatedVideoID;i.videos=i.items;if(i.videos==undefined){return false}if(g.params.ammount){i.videos=i.videos.slice(0,g.params.ammount)}g.data.items.unshift(i);g.renderTabs(true);g.container.find("."+c.css.loading).removeClass(c.css.loading)},{page_size:g.params.ammount||20,page_number:0,sort_by:"PLAYS_TRAILING_WEEK",sort_order:"DESC",who:f})}};b.content1.prototype.renderTabs=function(g){this.container.removeClass(c.css.loading);var h=this;var a=c.template.render("tab",this.data).appendTo(this.tabBody.empty()).click(function(){if(this.isRunning==true){return}var i=e(this);var j=i.data(c.css.data);if(j.videos!=undefined){h.showVideos(j)}else{h.load(j.id,function(k){h.showVideos(k)},j.type)}a.removeClass(c.css.active);i.addClass(c.css.active);return false});if(h.loaded||g==true){a.eq(0).click()}};var d={};b.content1.prototype.load=function(i,a,g){if(this.isRunning==true){return}this.view.addClass(c.css.loading);this.viewList.empty();if(d[i]!=undefined&&d[i].videos!=undefined){a(d[i]);this.view.removeClass(c.css.loading)}else{var h=this;this.isRunning=true;if(g=="playlist"){this.api.playlists(i,function(j){j=j.items[0];if(h.params.ammount){if(j.items!=undefined){j.videos=j.items.slice(0,h.params.ammount)}}d[i]=j;a(j);h.view.removeClass(c.css.loading);h.isRunning=false})}else{h.api.tags(i,function(j){h.container.find("."+c.css.loading).removeClass(c.css.loading);j.id=i;j.name=i;j.videos=j.items;d[i]=j;a(j);h.view.removeClass(c.css.loading);h.isRunning=false},{page_size:h.params.ammount,sort_by:"PLAYS_TRAILING_WEEK",sort_order:"DESC",who:f})}}};b.content1.prototype.carousel=function(){if(this.params.carousel){this.viewBody.asdtv_carousel({scroll:4,visible:4,btnPrev:this.container.find("."+c.css.prev),btnNext:this.container.find("."+c.css.next),disabledClass:c.css.inactive,enabledClass:c.css.active})}else{this.viewList.scrollTop(0);this.viewList.asdtv_activateImages()}};b.content1.prototype.showVideos=function(h){if(this.loaded){if(this.initialized==true){asdtv_brightcove.ivwcount()}else{this.initialized=true}}var g=this;var a=c.template.render(this.params.teaserTemplate,{items:h.videos}).appendTo(this.viewList.empty()).click(function(){a.removeClass(c.css.active);return g.params.onClick(e(this).addClass(c.css.active).data(c.css.data))});a.eq(0).click();g.carousel()}})(asdtv_brightcove.modules,asdtv_brightcove);(function(b,c){var d=jQuery,e="sidebar1";b.sidebar1=function(a,g){var f=this;this.params=d.extend({ammount:false},g||{});this.container=d(a);if(this.container.size()==0){return}this.initialized=false;this.api=c.api.get();this.view=this.container.find(".asdtv_videoteasers").unbind("click");this.viewList=this.view.find(".asdtv_videoteaserlist").unbind("click");this.viewBody=this.view.find(".asdtv_videoteaserview").unbind("click");this.videoContainer=this.container.find(".asdtv_videomodule").unbind("click");this.container.addClass(c.css.loading);this.api.related(this.params.relatedVideoID,function(j){if(f.params.ammount){j.items=j.items.slice(0,f.params.ammount)}f.container.find("."+c.css.loading).removeClass(c.css.loading);var h=c.template.render("teaserview",j).appendTo(f.viewList).click(function(){var k=d(this).data(c.css.data);f.showVideo(k.id,{itemURL:k.itemURL||k.linkURL,videoId:k.videoId});return false});var i=h.eq(0).data(c.css.data);f.showVideo(i.id,{autoStart:"false",itemURL:i.itemURL||i.linkURL,videoId:i.videoId});f.carousel();f.container.removeClass(c.css.loading)},{page_size:f.params.ammount||20,page_number:0})};b.sidebar1.prototype.carousel=function(){this.viewBody.asdtv_carousel({scroll:2,visible:2,btnPrev:this.container.find("."+c.css.prev),btnNext:this.container.find("."+c.css.next),disabledClass:c.css.inactive,enabledClass:c.css.active})};b.sidebar1.prototype.showVideo=function(f,a){if(this.initialized==true){asdtv_brightcove.ivwcount()}else{this.initialized=true}this.videoContainer.removeClass(c.css.loading);a=d.extend({playerID:this.params.playerID,videoId:f,autoStart:"true",isVid:"true"},a||{});new asdtv_brightcove.player(a,this.videoContainer)}})(asdtv_brightcove.modules,asdtv_brightcove);(function(b,c){var b=asdtv_brightcove.modules;var c=asdtv_brightcove;var d=jQuery,e="sidebar_topvideo";b.sidebar_topvideo=function(a,g){var f=this;this.params=d.extend({ammount:false},g||{});this.container=d(a);if(this.container.size()==0){return}this.initialized=false;this.api=c.api.get();this.view=this.container.find(".asdtv_videoteasers").unbind("click");this.viewList=this.view.find(".asdtv_videoteaserlist").unbind("click");this.viewBody=this.view.find(".asdtv_videoteaserview").unbind("click");this.videoContainer=d("<div></div>");this.api.related(this.params.relatedVideoID,function(i){if(f.params.ammount){i.items=i.items.slice(0,f.params.ammount)}f.container.removeClass(c.css.loading).find("."+c.css.loading).removeClass(c.css.loading);var h=c.template.render("teaserview",i).appendTo(f.viewList).click(function(){var j=d(this);var k=j.data(c.css.data);f.showVideo(k.id,{itemURL:k.itemURL||k.linkURL,videoId:k.videoId,who:e},j);return false});h.find("img").asdtv_overlay();f.carousel()},{page_size:f.params.ammount||20,page_number:0,sort_by:"PLAYS_TRAILING_WEEK",sort_order:"DESC",who:e})};b.sidebar_topvideo.prototype.carousel=function(){var a=this;this.viewBody.asdtv_carousel({scroll:1,visible:1,btnPrev:this.container.find("."+c.css.prev),btnNext:this.container.find("."+c.css.next),disabledClass:c.css.inactive,enabledClass:c.css.active,beforeStart:function(){a.hideVideo()}})};b.sidebar_topvideo.prototype.showVideo=function(g,f,a){if(this.initialized==true){c.ivwcount()}else{this.initialized=true}this.videoContainer.removeClass(c.css.loading);f=d.extend({playerID:this.params.playerID,videoId:g,autoStart:"true",isVid:"true"},f||{});this.videoContainer.css({height:a.height(),width:a.width()}).prependTo(a);new c.player(f,this.videoContainer)};b.sidebar_topvideo.prototype.hideVideo=function(){this.videoContainer.remove()}})(asdtv_brightcove.modules,asdtv_brightcove);(function(b,c){var e=jQuery,d={playlists:[]},f="videobuehne";b.videobuehne1=function(a,i){var h=this;this.params=e.extend({onChangeChannel:function(){},onStartChannel:null,searchForm:false,carousel:false,playerID:false,search_page_size:50,category_page_size:25,onClick:function(){return true},videosOnARow:{"2row":8,"1row":4},cssKlassen:{"2row":"2row","1row":"1row"}},i||{});this.currentView="2row";this.data={items:[]};this.currentData={};this.container=e(a);if(this.container.size()==0){return}this.initialized=false;this.api=c.api.get();this.category_navi=this.container.find(".asdtv_category-navi>ul:first").unbind("click");this.view=this.container.find(".asdtv_videoteaserview").unbind("click");this.pagination_navi=this.container.find(".asdtv_pagination").unbind("click");this.moduleBody=this.container.find(".asdtv_modulebody").unbind("click");this.view_switch_navigation=this.container.find(".asdtv_view_switch").unbind("click");this.isRunning=false;this.getCategories();var g=this.view_switch_navigation.find("li").unbind("click").click(function(l){if(l&&l.preventDefault){l.preventDefault()}if(h.isRunning==true){return false}var k=e(this);g.find("a").removeClass(c.css.active);if(e(this).hasClass(c.css.active)){return false}g.removeClass(c.css.active);e("a",k).addClass(c.css.active);var j="1row";if(e(this).addClass(c.css.active).hasClass("asdtv_2rows_view")){j="2row"}h.currentView=j;h.renderView();return false});if(this.params.searchForm&&this.params.searchForm.size&&this.params.searchForm.size()>0){this.params.searchForm.find("form").submit(function(){h.search();return false});this.params.searchForm.find("input[type=text]").attr("value","").keydown(function(j){if(j.keyCode==13){h.search();return false}})}};b.videobuehne1.prototype.search=function(){if(this.isRunning==true){return false}var h=e.trim(this.params.searchForm.find("input[type=text]").attr("value"));if(h.length<2){return false}var a=this,g="search:"+h;a.view.empty();this.pagination_navi.empty();a.moduleBody.addClass(c.css.loading);a.category_navi.find("."+c.css.active).removeClass(c.css.active);if(d.playlists[g]!=undefined){a.moduleBody.removeClass(c.css.loading);a.isRunning=false;a.currentData=d.playlists[g];a.renderView();c.template.render("searchresult",{value:h,data:d.playlists[g]}).prependTo(a.view)}else{this.api.search(h,function(i){i.items=i.items.sort(function(k,j){return k.publishedDate<j.publishedDate});i.videos=i.items;d.playlists[g]=i;a.moduleBody.removeClass(c.css.loading);a.isRunning=false;a.currentData=i;a.renderView();c.template.render("searchresult",{value:h,data:i}).prependTo(a.view)},{page_size:a.params.search_page_size,who:f})}this.params.searchForm.find("input[type=text]").attr("value","")};b.videobuehne1.prototype.getCategories=function(){var a=this;this.moduleBody.addClass(c.css.loading);this.api.byPlayer(this.params.playerID,function(h){var g=c.template.render("category_list_item",h).appendTo(a.category_navi.empty()).click(function(){if(a.isRunning==true){return false}var i=e(this);var j=i.data(c.css.data);g.removeClass(c.css.active);i.addClass(c.css.active);a.videoByCatgeory(j.id,function(k){a.currentData=k;a.renderView();a.params.onChangeChannel(k.id)});return false});if(a.params.onStartChannel){a.params.onStartChannel(g,h)}else{g.eq(0).click()}},{page_size:a.params.category_page_size,all_pages:true,fields:"id,name",who:f})};b.videobuehne1.prototype.videoByCatgeory=function(h,a){this.isRunning=true;var g=this;g.view.empty();this.pagination_navi.empty();g.moduleBody.addClass(c.css.loading);if(d.playlists[h]!=undefined){g.moduleBody.removeClass(c.css.loading);g.isRunning=false;a(d.playlists[h])}else{this.api.playlists(h,function(i){g.moduleBody.removeClass(c.css.loading);d.playlists[h]=i.items[0];g.isRunning=false;a(i.items[0])},{video_fields:"id,name,shortDescription,linkURL,thumbnailURL",playlist_fields:"id,videos",who:f})}};b.videobuehne1.prototype.renderCategoryNavigation=function(){var g=this;var a=c.template.render("category_list_item",this.data).appendTo(this.category_navi.empty()).click(function(){var h=e(this);var i=h.data(c.css.data);a.removeClass(c.css.active);h.addClass(c.css.active);g.currentData=i;g.renderView();return false});a.eq(0).click()};b.videobuehne1.prototype.renderView=function(){this.pagination_navi.empty();this.view.empty();this.container.removeClass(this.params.cssKlassen["2row"]);this.container.removeClass(this.params.cssKlassen["1row"]);this.container.addClass(this.params.cssKlassen[this.currentView]);this["renderView_"+this.currentView]()};b.videobuehne1.prototype.getDataPage=function(g,h){var i=this.currentData.videos;var j=((h-1)*g);var a=h*g;i=i.length?i:[];return i.slice(j,a)};b.videobuehne1.prototype.renderView_2row=function(){var l=this;var m=this.currentData;if(!m||!m.videos){return}var g=this.params.videosOnARow["2row"];var k=Math.ceil(m.videos.length/g);var a=c.template.render("videobuehne",{}).appendTo(this.view);for(var j=0;j<k;j++){c.template.render("videobuehne_pagination",{page:(j+1),listID:m.id}).appendTo(this.pagination_navi)}var h=this.pagination_navi.find("."+c.css.page).click(function(){if(l.isRunning==true){return false}if(l.initialized==true){asdtv_brightcove.ivwcount()}else{l.initialized=true}var p=e(this);h.removeClass(c.css.active);var o=p.addClass(c.css.active).data(c.css.data).page;var n=l.getDataPage(g,o);var i=c.template.render("2row_item",{items:n}).appendTo(a.empty()).click(l.params.onClick);return false});h.eq(0).click()};b.videobuehne1.prototype.renderView_1row=function(){var l=this;var m=this.currentData;if(!m||!m.videos){return}var a=c.template.render("videobuehne_coverflow",{}).appendTo(this.view.empty());var k=this.view.find("."+c.css.teasersubnavi);var j=this.view.find("."+c.css.teaserheadline);var h=this.view.find("."+c.css.teaserlist);var i=this.view.find("."+c.css.slider).asdtv_slider({PerPage:this.params.videosOnARow["1row"],Length:m.videos.length,Pages:Math.ceil(m.videos.length/this.params.videosOnARow["1row"]),next:".asdtv_next",prev:".asdtv_prev",knob:".asdtv_slidercontroller",view:h});j.html(c.utilities.ie6.decode(m.name));var g=c.template.render("1row_item",{items:m.videos}).appendTo(h.empty()).click(l.params.onClick);g.eq(0).addClass("asdtv_active");if(this.initialized==true){asdtv_brightcove.ivwcount()}else{this.initialized=true}}})(asdtv_brightcove.modules,asdtv_brightcove);(function(b,c,d){var e="videoplayer";b.videoplayer=function(a,g){var f=this;this.data=false;this.params=d.extend({videoId:false,autoplay:undefined,isReady:function(){},notRenderVideo:false},g||{});this.container=d(a);if(this.container.size()==0){return this.ready()}this.api=c.api.get();if(this.params.videoId==false){this.getParams()}if(this.params.videoId==false&&this.params.playlistID==undefined){return this.ready()}this.container.addClass(c.css.loading);if(this.params.playlistID&&this.params.videoId==false){this.api.playlist(this.params.playlistID,function(h){f.data=h.videos[0];f.render()},{video_fields:"id,name,shortDescription,longDescription,linkURL,thumbnailURL,PUBLISHEDDATE,tags",playlist_fields:"id,videos",page_size:"1",who:e})}else{this.api.video(this.params.videoId,function(h){f.data=h;f.render()},{video_fields:"id,name,shortDescription,longDescription,linkURL,thumbnailURL,PUBLISHEDDATE,tags",who:e})}};b.videoplayer.prototype.ready=function(){if(this.params.isReady){this.params.isReady(this.data)}};b.videoplayer.prototype.getParams=function(){var g=window.location.search,f={};var a=String(String(g).split("?")[1]).split("&");d.each(a,function(h,k){var j=k.split("=");if(j[0]&&j[1]!=undefined&&j[0]!="?"){f[j[0]]=String(decodeURIComponent(j[1])).replace(/[\+]/gmi," ")}});if(f.vid){this.params.videoId=parseInt(f.vid)}if(String(location.hash).indexOf("autoplay")>-1){this.params.autoplay=true}};b.videoplayer.prototype.render=function(){this.ready();this.container.removeClass(c.css.loading);if(this.params.notRenderVideo&&this.data&&this.data.id&&this.data.name){this.data.container=this.container;c.template.render("videoplayer",this.data);return}if(this.data&&this.data.id&&this.data.name){this.data.notRenderVideo=this.params.notRenderVideo;var a=c.template.render("videoplayer",this.data).appendTo(this.container.empty());var f={videoId:this.data.id,autoStart:this.params.autoplay==true?"true":"false",isVid:"true"};new asdtv_brightcove.player(f,a.find(".stage"))}else{this.container.empty().html("Ooops here is an error occured, the video is not valid.")}}})(asdtv_brightcove.modules,asdtv_brightcove,jQuery);(function(k,c){var d=c,h=false,f=false,l,g=true;var b=function(j,a){if(!window.token&&h==false){return false}if(!h){h=new asdtv_brightcove.api(window.token)}f=false;var e=new asdtv_brightcove.modules.videoplayer(d(".asdtv_modulecontainer.asdtv_video_player"),{videoId:(j||false),notRenderVideo:(j&&a?true:false),playlistID:window.playerInitPlaylistID,autoplay:(j&&!a?true:false),isReady:function(r){l=r&&r.id?r.id:false;if(window.playlistIDs){var u=window.ammountPlaylistVideos||false;var v=window.playlistIDs.split(",");var x=r.id||window.relatedVideo;if(window.playlistInitID){v.unshift(playlistInitID)}v=asdtv_brightcove.utilities.unique(v);if(v.length==0){return false}if(!window.related=="true"||!x){x=false}if(window.playerParams!=undefined){asdtv_brightcove.player.setDefaultParams(playerParams)}if(window.BrightcovePlayerParams!=undefined){asdtv_brightcove.player.setDefaultBrightcoveParams(BrightcovePlayerParams)}new asdtv_brightcove.modules.content1(".asdtv_playlist_content",{playlistID:v,relatedVideoID:x,carousel:true,ammount:u});new asdtv_brightcove.modules.content1(".asdtv_playlist_sidebar_vertical",{playlistID:v,relatedVideoID:x,tags:(window.Tags!=undefined?String(window.Tags).split(","):false),carousel:false,teaserTemplate:"sidebar_vertical_teaserview",ammount:u});if(window.sidebar=="true"){new asdtv_brightcove.modules.sidebar1(".asdtv_playlist_sidebar.bmo",{relatedVideoID:x,ammount:u});new asdtv_brightcove.modules.sidebar_topvideo(".asdtv_playlist_sidebar_topvideos",{relatedVideoID:x,ammount:u});new asdtv_brightcove.modules.sidebar1(".asdtv_playlist_sidebar.won",{relatedVideoID:x,ammount:u})}}if(window.videoBuehnePlayerID!=undefined){var n=d(".asdtv_modulecontainer.videostage_bottom.won_videostage_bottom");var o=d(".asdtv_modulecontainer.videostage_bottom.bmo_videostage_bottom");var t=d(".asdtv_modulecontainer.videostage_bottom.hao_videostage_bottom");if(n.size()>0){asdtv_brightcove.enabled_tag="publisher=www.welt.de";asdtv_brightcove.ivwcount.addCounter(function(){var z=d("."+asdtv_brightcove.css.ivw_counter),y="http://welt.ivwbox.de/cgi-bin/ivw/CP/Videos?r="+escape(document.referrer)+"&d="+(new Date).getTime();if(z.size()){z.attr("src",y)}else{d('<img class="'+asdtv_brightcove.css.ivw_counter+'" src="'+y+'"/>').appendTo(d("body"))}});new asdtv_brightcove.modules.videobuehne1(n,{searchForm:c(".asdtv_modulecontainer.videostage_searchengine"),playerID:window.videoBuehnePlayerID,cssKlassen:{"2row":"won_videostage_bottom","1row":"won_videostage_bottom_coverflow"}})}else{if(o.size()>0){asdtv_brightcove.enabled_tag="publisher=www.morgenpost.de";new asdtv_brightcove.modules.videobuehne1(o,{searchForm:c(".asdtv_modulecontainer.videostage_searchengine"),playerID:window.videoBuehnePlayerID,cssKlassen:{"2row":"bmo_videostage_bottom","1row":"bmo_videostage_bottom_coverflow"}})}else{if(t.size()>0){var w=function(y){y.video_fields=y.video_fields.replace("thumbnailURL","videoStillURL");return y};var p=function(A){var y,z;for(y=0;y<A.items.length;y++){for(z=0;z<A.items[y].videos.length;z++){A.items[y].videos[z]["thumbnailURL"]="./abendblatt/image.php?image="+escape(A.items[y].videos[z]["videoStillURL"])+"&size=140,105"}}return A};var s=function(A){var y,z;for(y=0;y<A.items.length;y++){A.items[y]["thumbnailURL"]="./abendblatt/image.php?image="+escape(A.items[y]["videoStillURL"])+"&size=140,105"}return A};asdtv_brightcove.register_hook("videobuehne.brightcove.params.find_playlists_by_ids",w);asdtv_brightcove.register_hook("videobuehne.brightcove.data.find_playlists_by_ids",p);asdtv_brightcove.register_hook("videobuehne.brightcove.params.find_videos_by_text",w);asdtv_brightcove.register_hook("videobuehne.brightcove.data.find_videos_by_text",s);var q=asdtv_brightcove.utilities.cookie("hao.asdtv.videobuehne1.current");new asdtv_brightcove.modules.videobuehne1(t,{searchForm:c(".videostage_searchengine"),playerID:window.videoBuehnePlayerID,cssKlassen:{"2row":"hao_videostage_bottom","1row":"hao_videostage_bottom_coverflow"},videosOnARow:{"2row":9,"1row":3},onClick:function(){return true},onChangeChannel:function(y){if(y!==asdtv_brightcove.utilities.cookie("hao.asdtv.videobuehne1.current")){asdtv_brightcove.utilities.cookie("hao.asdtv.videobuehne1.current",y)}},onStartChannel:function(z,A){if(!q){z.eq(0).click()}else{var y=false;z.each(function(B,D){if(y==false){var C=d(D).data(asdtv_brightcove.css.data);if(C.id==q){d(D).click();y=true}}})}}});window.adstv_linkbridge=function(y){if(y&&y!==r.id){b(y,false)}};asdtv_brightcove.template.preprocess=function(y){if(y.linkURL!=undefined||y.id!=undefined){y.linkURL="javascript: adstv_linkbridge( '"+y.id+"' );"}return y}}}}window.setTimeout(asdtv_brightcove.ivwcount.enable,200)}else{asdtv_brightcove.ivwcount.enable()}f=true;g=false}})};try{if(!APIModules.VIDEO_PLAYER||!BCMediaEvent.CHANGE){throw ("BC Modules are not loaded")}var m=window.onTemplateLoaded||function(){};window.onTemplateLoaded=function(e){m(e);var j=brightcove.getExperience(e);var a=j.getModule(APIModules.VIDEO_PLAYER);a.addEventListener(BCMediaEvent.CHANGE,function(n){if(!f){return}if(n.media.id!==l){try{b(n.media.id,true)}catch(o){}}})}}catch(i){}c().ready(function(){try{b()}catch(a){}})})(asdtv_brightcove,jQuery);(function(){var b;b=jQuery.fn.flash=function(g,f,d,i){var h=d||b.replace;f=b.copy(b.pluginOptions,f);if(!b.hasFlash(f.version)){if(f.expressInstall&&b.hasFlash(6,0,65)){var e={flashvars:{MMredirectURL:location,MMplayerType:"PlugIn",MMdoctitle:jQuery("title").text()}}}else{if(f.update){h=i||b.update}else{return this}}}g=b.copy(b.htmlOptions,e,g);return this.each(function(){h.call(this,b.copy(g))})};b.copy=function(){var f={},e={};for(var g=0;g<arguments.length;g++){var d=arguments[g];if(d==undefined){continue}jQuery.extend(f,d);if(d.flashvars==undefined){continue}jQuery.extend(e,d.flashvars)}f.flashvars=e;return f};b.hasFlash=function(){if(/hasFlash\=true/.test(location)){return true}if(/hasFlash\=false/.test(location)){return false}var e=b.hasFlash.playerVersion().match(/\d+/g);var f=String([arguments[0],arguments[1],arguments[2]]).match(/\d+/g)||String(b.pluginOptions.version).match(/\d+/g);for(var d=0;d<3;d++){e[d]=parseInt(e[d]||0);f[d]=parseInt(f[d]||0);if(e[d]<f[d]){return false}if(e[d]>f[d]){return true}}return true};b.hasFlash.playerVersion=function(){try{try{var d=new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");try{d.AllowScriptAccess="always"}catch(f){return"6,0,0"}}catch(f){}return new ActiveXObject("ShockwaveFlash.ShockwaveFlash").GetVariable("$version").replace(/\D+/g,",").match(/^,?(.+),?$/)[1]}catch(f){try{if(navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin){return(navigator.plugins["Shockwave Flash 2.0"]||navigator.plugins["Shockwave Flash"]).description.replace(/\D+/g,",").match(/^,?(.+),?$/)[1]}}catch(f){}}return"0,0,0"};b.htmlOptions={height:240,flashvars:{},pluginspage:"http://www.adobe.com/go/getflashplayer",src:"#",type:"application/x-shockwave-flash",width:320};b.pluginOptions={expressInstall:false,update:true,version:"6.0.65"};b.replace=function(d){this.innerHTML='<div class="alt">'+this.innerHTML+"</div>";jQuery(this).addClass("flash-replaced").prepend(b.transform(d))};b.update=function(e){var d=String(location).split("?");d.splice(1,0,"?hasFlash=true&");d=d.join("");var f='<p>This content requires the Flash Player. <a href="http://www.adobe.com/go/getflashplayer">Download Flash Player</a>. Already have Flash Player? <a href="'+d+'">Click here.</a></p>';this.innerHTML='<span class="alt">'+this.innerHTML+"</span>";jQuery(this).addClass("flash-update").prepend(f)};function a(){var e="";for(var d in this){if(typeof this[d]!="function"){e+=d+'="'+this[d]+'" '}}return e}function c(){var e="";for(var d in this){if(typeof this[d]!="function"){e+=d+"="+encodeURIComponent(this[d])+"&"}}return e.replace(/&$/,"")}b.transform=function(d){d.toString=a;if(d.flashvars){d.flashvars.toString=c}return"<embed "+String(d)+"/>"};if(window.attachEvent){window.attachEvent("onbeforeunload",function(){__flash_unloadHandler=function(){};__flash_savedUnloadHandler=function(){}})}})();
// jpa: special JS for asdtv players

// called when template loads, we use this to store a reference to the player and modules
// and add event listeners for the video load (when the user clicks on a video)
function onTemplateLoaded(pExperience) {
    var player = brightcove.getExperience(pExperience);
    var adModule = player.getModule(APIModules.ADVERTISING);
    var adPolicy = adModule.getAdPolicy();
    adPolicy.adServerURL = adPolicy.adServerURL + AsdtvVideo.getChannelName(pExperience);
    adModule.setAdPolicy(adPolicy);
}

function AsdtvVideo( videoPlayerId, defaultVideoId, defaultArticleUrl, commercialUrl, channelName )
{
    if( videoPlayerId != undefined )
    {
        var newVideo = new AsdtvVideo();
        newVideo.videoPlayerId = videoPlayerId;
        newVideo.defaultArticleUrl = defaultArticleUrl;
        newVideo.defaultVideoId = defaultVideoId;
        newVideo.commercialUrl = commercialUrl;
        newVideo.channelName = channelName;
        AsdtvVideo._objects[videoPlayerId] = newVideo;
    }
}

AsdtvVideo.play = function( videoPlayerId, videoId, channelName )
{
    if( channelName != undefined )
    {
        AsdtvVideo._objects[videoPlayerId].channelName = channelName;
    }
    AsdtvVideo._objects[videoPlayerId]._playVideo( videoId );
    return false;
}

AsdtvVideo.setChannelName = function( videoPlayerId, channelName )
{
    AsdtvVideo._channelName[videoPlayerId] = channelName;
}

AsdtvVideo.getChannelName = function( videoPlayerId )
{
    return AsdtvVideo._channelName[videoPlayerId];
}

AsdtvVideo._channelName = new Array();
AsdtvVideo._objects = new Array(); // array of video players
AsdtvVideo._activeVideo = null; // the currently actived video

AsdtvVideo.prototype =
{
    videoPlayerId: null,
    commercialUrl: "",
    defaultArticleUrl: "",
    defaultVideoId: "",
    channelName: "",
    player: null,
    content: null,
    video: null,
    bgcolor: "#FFFFFF",
    width: "248",
    height: "208",
    playerID: "18178112001",
    publisherID: "17907421001",
    isSlim: "true",
    autoStart: "true",

    _initPlayer:function( videoId )
    {
        videoId = videoId ? videoId : this.defaultVideoId;
    
        var params = {};
        params.bgcolor=this.bgcolor;
        params.width=this.width;
        params.height=this.height;
        params.playerID=this.playerID;
        params.publisherID=this.publisherID;
        params.isSlim=this.isSlim;
        params['@videoPlayer']="ref:" + videoId;
        params.linkBaseURL=this.defaultArticleUrl;
        params.adServerURL=this.commercialUrl;
        params.autoStart=this.autoStart;
        
        this.player = brightcove.createElement("object");
        this.player.id = this.videoPlayerId+"Player";
        AsdtvVideo.setChannelName(this.player.id,this.channelName);
        
        var parameter;
        for( var i in params )
        {
             parameter = brightcove.createElement("param");
             parameter.name = i;
             parameter.value = params[i];
             this.player.appendChild(parameter);
        }
        var playerContainer = document.getElementById(this.videoPlayerId);
        playerContainer.innerHTML = "";
        brightcove.createExperience(this.player, playerContainer, true);
    },
    
    _playVideo:function( videoId )
    {
        this._initPlayer( videoId );
    }
    
}

AsdtvVideo.onVideoLoad = function(event)
{
    AsdtvVideo._activeVideo.loadVideo(event.video.id);
}

// jpa: some bugs removed for IE
brightcove.createExperiences = function(pEvent, pElementID) {
      brightcove.removeListeners();
      var pDefaultParam = {
         };
      pDefaultParam.width = '100%';
      pDefaultParam.height = '100%';
      var pDefaultFParam = {
         };
      pDefaultFParam.allowScriptAccess = 'always';
      pDefaultFParam.allowFullScreen = 'true';
      pDefaultFParam.seamlessTabbing = false;
      pDefaultFParam.swliveconnect = true;
      pDefaultFParam.wmode = 'window';
      pDefaultFParam.quality = 'high';
      pDefaultFParam.bgcolor = '#999999';
      var isIE = (window.ActiveXObject != undefined);
      var pMajorVersion = 0;
      var pMinorRevision = 0;
      var pVersions;
      var pNoFlash = false;
      if(typeof navigator.plugins != 'undefined' && navigator.plugins.length > 0) {
         if(navigator.plugins["Shockwave Flash 2.0"] || navigator.plugins["Shockwave Flash"]) {
            var pSWFVersion = navigator.plugins["Shockwave Flash 2.0"] ? " 2.0" : "";
            var pDescription = navigator.plugins["Shockwave Flash" + pSWFVersion].description;
            pVersions = pDescription.split(" ");
            pMajorVersion = pVersions[2].split(".")[0];
            pMinorRevision = pVersions[3];
            if(pMinorRevision == "") {
               pMinorRevision = pVersions[4];
               }
            if(pMinorRevision[0] == "d") {
               pMinorRevision = pMinorRevision.substring(1);
               }
            else if(pMinorRevision[0] == "r") {
               pMinorRevision = pMinorRevision.substring(1);
               if(pMinorRevision.indexOf("d") > 0) {
                  pMinorRevision = pMinorRevision.substring(0, pMinorRevision.indexOf("d"));
                  }
               }
            }
         }
      else if(isIE) {
         try {
            var pFlash = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7");
            pVersions = / ([0-9]+),[0-9],([0-9]+),/.exec(pFlash.GetVariable('$version'));
            pMajorVersion = pVersions[1];
            pMinorRevision = pVersions[2];
            }
         catch(e) {
            pNoFlash = true;
            }
         }
      else {
         pNoFlash = true;
         }
      var pExperiences = [];
      if(pElementID != null) {
         pExperiences.push(document.getElementById(pElementID));
         }
      else {
         var pAllObjects = document.getElementsByTagName('object');
         var pNumObjects = pAllObjects.length;
         for(var i = 0; i < pNumObjects; i++) {
            if(/\bBrightcoveExperience\b/.test(pAllObjects[i].className)){pExperiences.push(pAllObjects[i]);
            }
         }
      }
   if(isIE) {
      var pParams = document.getElementsByTagName('param');
      }
   var pExperience;
   var pPlayerID = brightcove.getParameter("bcpid");
   var pTitleID = brightcove.getParameter("bctid");
   var pLineupID = brightcove.getParameter("bclid");
   var pAutoStart = brightcove.getParameter("autoStart");
   var pNumExperiences = pExperiences.length;
   var pRequestedMinorRevision;
   var pRequestedMajorVersion;
   for(var i = 0; i < pNumExperiences; i++) {
      pExperience = pExperiences[i];
      if(!pExperience.params)pExperience.params = {
         };
      if(!pExperience.fParams)pExperience.fParams = {
         };
      for(var j in pDefaultParam) {
         pExperience.params[j] = pDefaultParam[j];
         }
      for(var j in pDefaultFParam) {
         pExperience.fParams[j] = pDefaultFParam[j];
         }
      if(pExperience.id.length > 0) {
         pExperience.params.flashID = pExperience.id;
         }
      else {
         pExperience.id = pExperience.params.flashID = 'bcExperienceObj' + (brightcove.experienceNum++);
         }
      if(!isIE) {
         var pParams = pExperience.getElementsByTagName('param');
         }
      var pNumParams = pParams.length;
      var pParam;
      for(var j = 0; j < pNumParams; j++) {
         pParam = pParams[j];
         if(isIE && pParam.parentNode.id != pExperience.id) {
            continue;
            }
         pExperience.params[pParam.name] = pParam.value;
         }
      if(pExperience.params.majorVersion != undefined) {
         pRequestedMajorVersion = parseInt(pExperience.params.majorVersion);
         }
      else {
         pRequestedMajorVersion = brightcove.majorVersion;
         }
      if(pExperience.params.minorRevision != undefined) {
         pRequestedMinorRevision = parseInt(pExperience.params.minorRevision);
         }
      else {
         pRequestedMinorRevision = brightcove.minorRevision;
         }
      var pUseInstaller = false;
      if(pMajorVersion < pRequestedMajorVersion || (pMajorVersion == pRequestedMajorVersion && pMinorRevision < pRequestedMinorRevision)) {
         pUseInstaller = true;
         }
      if(pExperience.params.bgcolor != undefined)pExperience.fParams.bgcolor = pExperience.params.bgcolor;
      if(pExperience.params.wmode != undefined)pExperience.fParams.wmode = pExperience.params.wmode;
      if(pPlayerID.length < 1 || (pPlayerID == pExperience.params.playerID)) {
         if(pPlayerID != pExperience.params.playerID && pPlayerID.length > 0) {
            pExperience.params.playerID = pPlayerID;
            }
         if(pTitleID.length > 0) {
            pExperience.params.videoID = pTitleID;
            pExperience.params.autoStart = (pExperience.params.autoStart != "false" && pAutoStart != "false");
            }
         if(pLineupID.length > 0) {
            pExperience.params.lineupID = pLineupID;
            }
         }
      var pFile;
      if(pUseInstaller) {
         pFile = brightcove.cdnURL + "/viewer/playerProductInstall.swf";
         var MMPlayerType = isIE ? "ActiveX" : "PlugIn";
         document.title = document.title.slice(0, 47) + " - Flash Player Installation";
         var MMdoctitle = document.title;
         pFile += "?&MMredirectURL=" + window.location + '&MMplayerType=' + MMPlayerType + '&MMdoctitle=' + MMdoctitle;
         }
      else {
         if(pExperience.params.secureConnections == "true") {
            pFile = brightcove.getPubURL(brightcove.secureServicesURL, brightcove.pubSecureHost, pExperience.params.pubCode);
            }
         else {
            pFile = brightcove.getPubURL(brightcove.servicesURL, brightcove.pubHost, pExperience.params.pubCode);
            }
         pFile += ('/viewer/federated_f9?' + brightcove.getOverrides());
         for(var pConfig in pExperience.params) {
            pFile += '&' + encodeURIComponent(pConfig) + '=' + encodeURIComponent(pExperience.params[pConfig]);
            }
         }
      var pExperienceElement;
      if(pNoFlash) {
         var pContainer = document.createElement('span');
         if(pExperience.params.height.charAt(pExperience.params.height.length - 1) == "%") {
            pContainer.style.display = 'block';
            }
         else {
            pContainer.style.display = 'inline-block';
            }
         pContainer.id = '_container' + i;
         var pLinkHTML = "<a href='http://www.adobe.com/go/getflash/' target='_blank'><img src='" + brightcove.cdnURL + "/viewer/upgrade_flash_player2.gif' alt='Get Flash Player' width='314' height='200' border='0'></a>";
         pExperience.parentNode.replaceChild(pContainer, pExperience);
         document.getElementById('_container' + i).innerHTML = pLinkHTML;
         }
      else {
         if(isIE) {
            var pContainer = document.createElement('span');
            if(pExperience.params.height.charAt(pExperience.params.height.length - 1) == "%") {
               pContainer.style.display = 'block';
               }
            else {
               pContainer.style.display = 'inline-block';
               }
            pContainer.id = '_container' + i;
            pExperience.fParams.movie = pFile;
            var pOptions = '';
            for(var pOption in pExperience.fParams) {
               pOptions += '<param name="' + pOption + '" value="' + pExperience.fParams[pOption] + '" />';
               }
            var pProtocol = (pExperience.params.secureConnections == "true") ? "https" : "http";
            var pExperienceHTML = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"' + ' codebase="' + pProtocol + '://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=' + brightcove.majorVersion + ',' + brightcove.majorRevision + ',' + brightcove.minorRevision + ',0"' + ' id="' + pExperience.id + '"' + ' width="' + pExperience.params.width + '"' + ' height="' + pExperience.params.height + '"' + ' class="BrightcoveExperience">' + pOptions + '</object>';

            // jpa: BUG!
            var expParent = pExperience.parentNode;
            expParent.removeChild(pExperience);
            expParent.appendChild(pContainer);
            pContainer.innerHTML = pExperienceHTML;

            //pExperience.parentNode.replaceChild(pContainer, pExperience);
            //document.getElementById('_container' + i).innerHTML = pExperienceHTML;
            pExperience.experience = document.getElementById(pExperience.id);
            brightcove.experiences[pExperience.id] = pContainer;
            }
         else {
            var pExperienceElement = document.createElementNS('http://www.w3.org/1999/xhtml', 'object');
            pExperienceElement.type = 'application/x-shockwave-flash';
            pExperienceElement.data = pFile;
            pExperienceElement.id = pExperience.params.flashID;
            pExperienceElement.width = pExperience.params.width;
            pExperienceElement.height = pExperience.params.height;
            pExperienceElement.className = pExperience.className;
            var pTempParam;
            for(var pConfig in pExperience.fParams) {
               pTempParam = document.createElementNS('http://www.w3.org/1999/xhtml', 'param');
               pTempParam.name = pConfig;
               pTempParam.value = pExperience.fParams[pConfig];
               pExperienceElement.appendChild(pTempParam);
               }
            pExperience.parentNode.replaceChild(pExperienceElement, pExperience);
            brightcove.experiences[pExperience.id] = pExperienceElement;
            }
         }
      }
};



// jpa: settings for won asdtv video
AsdtvVideo.prototype.width = "308";
AsdtvVideo.prototype.height = "253";
AsdtvVideo.prototype.playerID = "17325656001";
AsdtvVideo.prototype.publisherID = "8385562001";


/* ---- google AdSense ---- */
var google_adnum = 0;

function google_ad_request_done(google_ads) {
/*
* This function is required and is used to display
* the ads that are returned from the JavaScript
* request. You should modify the document.write
* commands so that the HTML they write out fits
* with your desired ad layout.
*/
var s = '';
var i;

/*
* Verify that there are actually ads to display.
*/
	if (google_ads.length == 0) {
		return;
	}

/*
* If an image or flash ad is returned, display that ad.
* Otherwise, build a string containing all of the ads and
* then use a document.write() command to print that string.
*/

	if (google_ads[0].type == "flash") {

		s += '<a href=\"' +
		google_info.feedback_url + '\" style="color:000000">Ads by Google</a><br>' +
		'<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"' +
		' codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0" WIDTH="' +
		google_ad.image_width + '" HEIGHT="' +
		google_ad.image_height + '"> <PARAM NAME="movie" VALUE="' +
		google_ad.image_url + '">' +
		'<PARAM NAME="quality" VALUE="high">' +
		'<PARAM NAME="AllowScriptAccess" VALUE="never">' +
		'<EMBED src="' +
		google_ad.image_url + '" WIDTH="' +
		google_ad.image_width + '" HEIGHT="' +
		google_ad.image_height +
		'" TYPE="application/x-shockwave-flash"' +
		' AllowScriptAccess="never" ' +
		' PLUGINSPAGE="http://www.macromedia.com/go/getflashplayer"></EMBED></OBJECT>';

	} else if (google_ads[0].type == "image") {

		s += '<a href=\"' +
		google_info.feedback_url + '\" style="color:000000">Ads by Google</a><br> <a href="' +
		google_ads[0].url + '" target="_top" title="go to ' +
		google_ads[0].visible_url + '" onmouseout="window.status=\'\'" onmouseover="window.status=\'go to ' +
		google_ads[0].visible_url + '\';return true"><img border="0" src="' +
		google_ads[0].image_url + '"width="' +
		google_ads[0].image_width + '"height="' +
		google_ads[0].image_height + '"></a>';

	} else if (google_ads[0].type == "html") {

		s += google_ads[0].snippet;

	} else {

		if (google_ads.length == 1) {
/*
* Partners should adjust text sizes
* so ads occupy the majority of ad space.
*/
			s += '<a href=\"' +
			google_info.feedback_url + '\" class="adsByGoogle" target="_blank">Google Anzeige</a><br><br><a href="' +
			google_ads[0].url + '" onmouseout="window.status=\'\'" onmouseover="window.status=\'go to ' +
			google_ads[0].visible_url + '\';return true" class="headline" target="_blank">' +
			google_ads[0].line1 + '</a><br><span>' +
			google_ads[0].line2 + ' ' +
			google_ads[0].line3 + '<br></span> <a href="' +
			google_ads[0].url + '" onmouseout="window.status=\'\'" onmouseover="window.status=\'go to ' +
			google_ads[0].visible_url + '\';return true" class="link" target="_blank">' +
			google_ads[0].visible_url + '</a>';
			
			if (google_ads[0].bidtype == "CPC") {
				google_adnum = google_adnum + google_ads.length;
			}

		} else if (google_ads.length > 1) {

			s += '<a href=\"' + google_info.feedback_url + '\" class="adsByGoogle" target="_blank">Google Anzeigen</a><br>'

  /*
  * For text ads, append each ad to the string.
  */

			for(i = 0; i < google_ads.length; ++i) {

				s += '<br><a href="' +
				google_ads[i].url + '" onmouseout="window.status=\'\'" onmouseover="window.status=\'go to ' +
				google_ads[i].visible_url + '\';return true" class="headline" target="_blank">' +
				google_ads[i].line1 + '</a><br><span>' +
				google_ads[i].line2 + ' ' +
				google_ads[i].line3 + '<br></span> <a href="' +
				google_ads[i].url + '" onmouseout="window.status=\'\'" onmouseover="window.status=\'go to ' +
				google_ads[i].visible_url + '\';return true" class="link" target="_blank">' +
				google_ads[i].visible_url + '</a><br>';
			}
   
			if (google_ads[0].bidtype == "CPC") {
				google_adnum = google_adnum + google_ads.length;
			}
   
		}
    }

    document.write(s);
    return;
}

google_ad_client = 'pub-9638257500739576'; // substitute your client_id (pub-#)
google_ad_channel = '2805017551'; // In-Text-Ads Print-WELT
google_ad_output = 'js';
google_max_num_ads = '1';
google_ad_type = 'text';
google_image_size = '200x200';
google_feedback = 'on';
google_language = 'de';
