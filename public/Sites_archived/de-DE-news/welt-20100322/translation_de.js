/***
 * Default German Language.
 * translation key >> text...REMEMBER if you want to add variable then write \{0\} in the text...if you have 2 variables in the text then \{0\} and \{1\} 
 ****/

/*
 * ATTENTION: THIS FILE MUST BE IN ISO-8859-1
 * and MUST NOT contain HTML-escapes like &auml;
 */
var translation_array= new Array();
translation_array["general.comment"]="Kommentar";

translation_array["twingly_language"] = "german";

translation_array["form.video.notSend"]="Leider konnte das Video nicht an \{0\} versendet werden.";
translation_array["video.fillreceipent.email"]="F�llen Sie bitte das Feld 'Dieses Video senden an' aus.";
translation_array["video.fill.youremail"]="F�llen Sie bitte das Feld 'Ihre E-Mail' aus.";
translation_array["video.wrongreceipent.email"]="Die Empf�nger-E-Mail-Adresse ist nicht g�ltig.";
translation_array["video.fill.yourname"]="F�llen Sie bitte das Feld 'Ihr Name' aus.";
translation_array["video.your.wrongemail"]="Ihre E-Mail-Addresse is nicht g&uuml;ltig.";
translation_array["video.fill.code"]="F�llen Sie bitte das Feld 'Code' aus.";
translation_array["video.wrong.code"]="Der eingegebene Code ist falsch. Bitte geben Sie den Code erneut ein.";
translation_array["video.invalid.code"]="Der Code ist nicht mehr g�ltig. Bitte geben Sie den Code erneut ein.";
translation_array["video.sent"]="Dieses Video wurde erfolgreich verschickt!";
translation_array["video.comment.error"]="Leider konnte Ihr Kommentar nicht verarbeitet werden.";
translation_array["video.comment.success"]="Vielen Dank f�r Ihren Kommentar. Er wird in wenigen Minuten unter dem Artikel erscheinen.";
translation_array["video.comment.fill"]="F&uuml;llen Sie bitte das Feld 'Ihr Kommentar' aus.";
translation_array["video.comment.fillleserbrief"]="Das Feld 'Leserbrief' muss ausgef�llt werden.";
translation_array["video.yourname"]="[Ihr Name]";
translation_array["video.youremail"]="[Ihre E-mail]";
translation_array["video.received.videolink"]="hat Ihnen ein Video von \"Welt Online\"; empfohlen.";
translation_array["video.see.here"]="Das Video k�nnen Sie hier sehen:";
translation_array["video.data.notsaved"]="Ihre Daten wurden nicht gespeichert und wurden ausschlie�lich zum Versenden dieser Mail verwendet.";
translation_array["video.misuse"]="Wenn der Service missbraucht wurde, schicken Sie bitte diese Mail an leserbriefe@welt.de";
translation_array["extraChar.message"]="Sie haben \{0\} Zeichen zuviel eingegeben.";
translation_array["remainedChar.message"]="Sie k�nnen noch \{0\} Zeichen eingeben.";
translation_array["wrongParameter"]="Falscher Parameter";
translation_array["enterNewCode"]="Bitte geben Sie den Code erneut ein";
translation_array["timeOver"]="Zeitlimit abgelaufen";
translation_array["checkSpelling"]="Bitte �berpr�fen Sie die Schreibweise Ihrer Eingaben.";
translation_array["commentSuccess"]="Vielen Dank f�r Ihren Kommentar. Er wird in wenigen Minuten unter dem Artikel erscheinen.";
translation_array["cancelled"]="Abgebrochen";
translation_array["searchedWords"]="Hervorgehobene Suchbegriffe";
translation_array["disableHighlighting"]="Artikel ohne Hervorhebung anzeigen";
translation_array["enterSearchTerm"]="Bitte geben sie einen Suchbegriff ein.";
translation_array["emptyField"]="Das Feld '\{0\}' darf nicht leer sein.";
translation_array["shortenText"]="Sie haben zuviele Zeichen eingegeben. Bitte k�rzen Sie den Text";
translation_array["germanMobileNum"]="'\{0\}' muss einer deutschen Mobilfunknummer entsprechen.";
translation_array["internalError"]="Es ist ein interner Fehler aufgetreten. Bitte versuchen Sie es sp�ter noch einmal.";
translation_array["article.recommenSmsSuccess"]="Der Artikel wurde erfolgreich an \{0\} verschickt.";
translation_array["article.form.nameRecipient"]="Name Empf�nger";
translation_array["article.form.mobileRecipient"]="E-Mail Empf�nger";
translation_array["article.form.name"]="Ihr Name";
translation_array["article.form.mobile"]="Ihre Mobilfunknummer";
translation_array["article.form.code"]="Code";
translation_array["article.form.mobileRecipient"]="Mobilfunknummer Empf�nger";
translation_array["article.form.mailRecipient"]="E-Mail Empf�nger";
translation_array["article.form.mail"]="Ihre E-Mail";
translation_array["article.form.remark"]="Bemerkung";
