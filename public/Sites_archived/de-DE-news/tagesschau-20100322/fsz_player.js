
/**
 * $Id$
 */

function getMPlayerById(mplayer_id)
{
    if (navigator.appName.indexOf("Microsoft") != -1) return window[mplayer_id];
    else return document[mplayer_id];
}

function loadAndPlayFile(mplayer_id, file, title, link, image, id, start)
{
    var  mplayer = getMPlayerById(mplayer_id);
    mplayer.loadFile(file, title, link,  image, id, start);
    mplayer.sendEvent('playitem', 0);
}

var FSZ4eq1 = null;

// {{{ setupFSZPlayer()
function setupFSZPlayer(fsz_player_id, file, id, start, stop, title, image, more_streams, mplayer, mplayer_pholder)
{
    var css = null;
    var txt = null;
    var tbl = null;
    var tbd = null;
    var tr = null;
    var td = null;
    var a = null;
    var img = null;
 
 
    var viewportwidth;
    var viewportheight;
 
    if (typeof window.innerWidth != 'undefined')
    {
	 viewportwidth = window.innerWidth,
	 viewportheight = window.innerHeight
    }
    else if (typeof document.documentElement != 'undefined'  && typeof document.documentElement.clientWidth != 'undefined' && document.documentElement.clientWidth != 0)
    {
	  viewportwidth = document.documentElement.clientWidth,
	  viewportheight = document.documentElement.clientHeight
    }
    else
    {
	  viewportwidth = document.getElementsByTagName('body')[0].clientWidth,
	  viewportheight = document.getElementsByTagName('body')[0].clientHeight
    }

    var mplayer_height = 308;
    var mplayer_width = 512;
    var mplayer_top = (viewportheight-mplayer_height)/2;
    var mplayer_left = (viewportwidth-mplayer_width)/2;;

    FSZ4eq1 = new Object();
    FSZ4eq1.flv_id = fsz_player_id;
    FSZ4eq1.flvState = null;
    FSZ4eq1.flvTimePos = 0;
    FSZ4eq1.openerMplayer = mplayer;
    FSZ4eq1.openerParent = mplayer.parentNode;
    FSZ4eq1.openerMplayerPholder = mplayer_pholder;

    // {{{ setFLV()
    FSZ4eq1.setFLV = function (callback)
    {
        FSZ4eq1.flv = null;

        var _flv = null;

        var _t = setInterval(
            function ()
            {
                _flv = document.getElementById(FSZ4eq1.flv_id);

                if (_flv)
                {
                    FSZ4eq1.flv = _flv;

                    FSZ4eq1.flv.style.display = 'block';
                    if (callback != undefined) callback();
                    clearInterval(_t);
                }
            },
            200
        );
    }
    // }}} setFLV()

    FSZ4eq1.setFLV();

    // {{{ open()
    FSZ4eq1.open = function ()
    {
        FSZ4eq1.openerMplayerPholder.style.display = 'block';
        FSZ4eq1.openerMplayer.style.display = 'none';
	FSZ4eq1.openerMplayer.sendEvent('playpause');
	
        // FSZ4eq1.openerParent.removeChild(FSZ4eq1.openerMplayer);

        if (document.all)
        {
            FSZ4eq1.openerMplayerPholder.style.top = -13;
            var selboxElements = document.getElementsByTagName('select');
            for (var i = 0; i < selboxElements.length; i++) {
                selboxElements[i].style.visibility = 'hidden';
            }
        }

        FSZ4eq1.container.style.display  = 'block';
    }
    // }}} open()


    // {{{ play()
    FSZ4eq1.play = function (start)
    {
        FSZ4eq1.sendEvent('playpause', start);
    }
    // }}} play()

    // {{{ sendEvent()
    FSZ4eq1.sendEvent = function (evt, arg)
    {
        if (FSZ4eq1.flv != null)
        {
            try {FSZ4eq1.flv.sendEvent(evt, arg);} catch(e){}
            return;
        }

        FSZ4eq1.setFLV(
            function ()
            {
		try {FSZ4eq1.sendEvent(evt, arg);} catch(e){}
            }
        );
    }
    // }}} sendEvent()

    // {{{ close()
    FSZ4eq1.close = function ()
    {
        if (document.all) {
            FSZ4eq1.container.style.display  = 'none';
            var selboxElements = document.getElementsByTagName('select');
            for (var i = 0; i < selboxElements.length; i++) {
                selboxElements[i].style.visibility = 'visible';
            }
        }
        else {
            FSZ4eq1.container.style.visibility  = 'hidden';
        }

        FSZ4eq1.sendEvent('stop');
        FSZ4eq1.openerMplayerPholder.style.display = 'none';        
        FSZ4eq1.openerMplayer.style.display = 'block';
	
	// We don�t recreate the player-flash-object anymore,
	// we just hide/unhide the calling one�s parent-container.
        // FIXME: get the player id from params
        // UFO.create(FO, mplayer.id + '_div');
        

        for (var mm in FSZ4eq1.flv) {
            if (typeof FSZ4eq1.flv[mm] == 'function') {
                if (!document.all || document.all===undefined) {
		    delete FSZ4eq1.flv[mm];
		}
                else {
		    FSZ4eq1.flv[mm] = null;
		}
            }
        }

        FSZ4eq1.container.parentNode.removeChild(FSZ4eq1.container); 
        delete FSZ4eq1;
        FSZ4eq1 = null;
    }
    // }}} close()

    // {{{ hide()
    FSZ4eq1.hide = function ()
    {
        //if (FSZ4eq1.flvState > 0) FSZ4eq1.sendEvent('playpause');
    }
    // }}} hide()

    // {{{ reveal()
    FSZ4eq1.reveal = function ()
    {
        //if (FSZ4eq1.flvState == 0) FSZ4eq1.sendEvent('playpause');
    }
    // }}} reveal()

    // {{{ more()
    FSZ4eq1.more = function ()
    {
        FSZ4eq1.sendEvent('stop');
        window.open(more_streams, 'multimedia', '');
    }
    // }}} more()

    // {{{ startDrag()
    FSZ4eq1.startDrag = function (evt)
    {
        if (!evt) evt = window.event;

        FSZ4eq1.initMouseX = evt.clientX;
        FSZ4eq1.initMouseY = evt.clientY;

        var p = FSZ4eq1.container.pane;
        var sh = FSZ4eq1.container.shadow;

        FSZ4eq1.initPaneX = parseInt(p.style.left);
        FSZ4eq1.initPaneY = parseInt(p.style.top);

        FSZ4eq1.hide();

        var body = document.getElementsByTagName('body')[0];

        body.onmousemove = function (evt)
        {
            if (!evt) evt = window.event;

            if (evt.preventDefault) evt.preventDefault();
            else evt.returnValue = false;

            var cX = evt.clientX;
            var cY = evt.clientY;

            var dX = cX - FSZ4eq1.initMouseX;
            var dY = cY - FSZ4eq1.initMouseY;

            FSZ4eq1.initMouseX = cX;
            FSZ4eq1.initMouseY = cY;

            var pX = parseInt(p.style.left);
            var pY = parseInt(p.style.top);

            p.style.top  = (pY + dY) + 'px';
            p.style.left = (pX + dX) + 'px';

            //sh.style.left = (pX + 10) + 'px';
            //sh.style.top  = (pY + 10) + 'px';
        }

        body.onmouseup = function ()
        {
            body.onmousemove = null;
            body.onmouseup = null;
            FSZ4eq1.reveal();
        }
    }
    // }}} startDrag()

    // {{{ getUpdate()
    FSZ4eq1.getUpdate = function (type, pr1, pr2)
    {
        if (type == 'state')
        {
            FSZ4eq1.flvState = pr1;
            return;
        }

        if (type == 'time')
        {
            FSZ4eq1.flvTimePos = pr1;
            return;
        }
    }
    // }}} getUpdate()

    FSZ4eq1.container = document.createElement('div');

    // {{{ shadow
    FSZ4eq1.container.shadow = document.createElement('div');
    css = FSZ4eq1.container.shadow.style;
    css.top = '0px';
    css.left = '0px';
    css.height = '100%';
    css.width = '100%';
	css.background = 'url("/image/videoplayer/blau_t.png")';

    css.position = 'absolute';
    //css.opacity = '0.7';
    css.zIndex = '999998';

    FSZ4eq1.container.appendChild(FSZ4eq1.container.shadow);
    // }}} shadow

    // {{{ pane
    FSZ4eq1.container.pane = document.createElement('div');
    css = FSZ4eq1.container.pane.style;
    css.top = mplayer_top + 'px';
    css.left = mplayer_left + 'px';
    css.height = (mplayer_height + 60) +'px';
    css.width = (mplayer_width) + 'px';
    css.border = '#fff solid 1px';
    css.background = '#D5E0F7';
    css.position = 'absolute';
    css.zIndex = '999999';

    FSZ4eq1.container.appendChild(FSZ4eq1.container.pane);
    // }}} pane

    // {{{ titleBar
    FSZ4eq1.container.pane.titleBar = document.createElement('div');
    var ttb = FSZ4eq1.container.pane.titleBar;

    ttb.onmousedown = function (evt)
    {
        FSZ4eq1.startDrag(evt);
    }


    css = ttb.style;
    css.height = '20px';
    css.background = '#00044B';
    if (!document.all || document.all===undefined) {
	css.cursor = 'move';
    }
    else {
	css.cursor = 'url(move.cur), hand';
    }

    var tbl = document.createElement('table');
    tbl.style.width = mplayer_width + 'px';
    tbl.style.height = '20px';

    ttb.appendChild(tbl);

    tbd = document.createElement('tbody');
    tbl.appendChild(tbd);

    tr = document.createElement('tr');
    tbd.appendChild(tr);

    td = document.createElement('td');
    td.style.textAlign = 'left';
    td.style.padding = '0px 5px';
    td.style.fontSize = '12px';
    td.style.fontWeight = 'bold';
    td.style.color = '#D5E0F7';

    tr.appendChild(td);

    txt = document.createTextNode(title);
    td.appendChild(txt);

    td = document.createElement('td');
    td.align = 'right';
    td.style.width = '15px';
    td.style.padding = '0px 5px';
    td.style.textAlign = 'right';

    tr.appendChild(td);

    a = document.createElement('a');
    a.href = 'javascript:';
    a.style.display = 'block';
    a.style.width = '10px';
    a.style.border = '0px solid #fff';
    a.style.textAlign = 'right';
    a.onclick = function (evt)
    {
        if (!evt) evt = window.event;

        this.blur();
        FSZ4eq1.close();

        if (evt.preventDefault) evt.preventDefault();
        else evt.returnValue = false;
    }

    td.appendChild(a);

    img = document.createElement('img');
    img.alt = '';
    img.style.display = 'block';
    img.style.border = '0px solid #fff';
    img.src = '/image/videoplayer/close.gif';

    a.appendChild(img);

    FSZ4eq1.container.pane.appendChild(FSZ4eq1.container.pane.titleBar);
    // }}} titleBar

    // {{{ mplayer
    FSZ4eq1.container.pane.mplayer = document.createElement('div');
    FSZ4eq1.container.pane.mplayer.setAttribute('id', 'fsz_player_4eq1');
    FSZ4eq1.container.pane.mplayer.style.zIndex = '9999999';

    FSZ4eq1.container.pane.appendChild(FSZ4eq1.container.pane.mplayer);
    // }}} mplayer

    // {{{ bbar
    FSZ4eq1.container.pane.bbar = document.createElement('table');
    css = FSZ4eq1.container.pane.bbar.style;
    css.height = '40px';
    css.width = '100%';

    tbd = document.createElement('tbody');
    FSZ4eq1.container.pane.bbar.appendChild(tbd);

    tr = document.createElement('tr');
    tbd.appendChild(tr);

    td = document.createElement('td');
    td.style.textAlign = 'left';
    td.style.padding = '0px 10px';

    tr.appendChild(td);

    // {{{ more
    FSZ4eq1.container.pane.bbar.more = document.createElement('a');
    var more_lnk = FSZ4eq1.container.pane.bbar.more;
    more_lnk.style.fontSize = '11px';
    more_lnk.style.color = '#00044B';
    more_lnk.style.fontWeight = 'bold';
    more_lnk.style.background = 'url(/image/icon_video_d5e0f7.gif) no-repeat';
    more_lnk.style.paddingLeft = '20px';
    more_lnk.setAttribute('href', more_streams);
    more_lnk.setAttribute('target', '_blank');

    more_lnk.onclick = function (evt)
    {
        if (!evt) evt = window.event;

        more_lnk.blur();
        FSZ4eq1.more();
        FSZ4eq1.close();

        if (evt.preventDefault) evt.preventDefault();
        else evt.returnValue = false;
    };

    txt = document.createTextNode('weitere Sendungen');
    more_lnk.appendChild(txt);

    td.appendChild(FSZ4eq1.container.pane.bbar.more);
    // }}} more

    td = document.createElement('td');
    td.style.textAlign = 'right';
    td.style.padding = '0px 10px';

    tr.appendChild(td);

    // {{{ close
    FSZ4eq1.container.pane.bbar.close = document.createElement('a');
    var close_lnk = FSZ4eq1.container.pane.bbar.close;
    close_lnk.style.fontSize = '11px';
    close_lnk.style.color = '#00044B';
    close_lnk.style.fontWeight = 'bold';
    close_lnk.style.background = 'url(/image/icon_intern_d5e0f7.gif) no-repeat';
    close_lnk.style.paddingLeft = '18px';

    close_lnk.setAttribute('href', 'javascript:');
    close_lnk.onclick = function (evt)
    {
        if (!evt) evt = window.event;

        close_lnk.blur();
        FSZ4eq1.close();

        if (evt.preventDefault) evt.preventDefault();
        else evt.returnValue = false;
    };

    txt = document.createTextNode('schlie�en');
    close_lnk.appendChild(txt);

    td.appendChild(FSZ4eq1.container.pane.bbar.close);
    // }}} close

    FSZ4eq1.container.pane.appendChild(FSZ4eq1.container.pane.bbar);
    // }}} bbar

    document.getElementsByTagName('body')[0].appendChild(FSZ4eq1.container);

    // {{{ UFO
    var FSZ_OBJ =
    {
        movie: "/resources/flash/mediaplayer21.swf",
        id: FSZ4eq1.flv_id,
        name: FSZ4eq1.flv_id,
        width: mplayer_width,
        height: mplayer_height,
        majorversion: "8",
        build: "0",
        bgcolor: "#FFFFFF",
        allowfullscreen: "true",
        flashvars:
            "file=" + file +
            "&id=" + id +
            "&start=" + start +
            "&stop=" + stop +
            "&image=" + image +
            "&displayheight=288" +
            "&shuffle=false" +
            "&autostart=true" +
            "&enablejs=true" +
            "&showfsbutton=false" +
            "&showdigits=true" +
            "&frontcolor=0x00044B" +
            "&lightcolor=0xFFFFFF" +
            "&backcolor=0xD5E0F7" +
            "&linuxhook=true" +
            "&usefullscreen=true"
    };

    UFO.create(FSZ_OBJ, FSZ4eq1.container.pane.mplayer.getAttribute('id'));
    // }}} UFO

}
// }}} setupFSZPlayer()

// {{{ openFullSizeView()
function openFullSizeView(mplayer_id, file, id, start, stop, title, image, more_streams)
{
    if (!document.getElementById) return;
    if (FSZ4eq1!==null) return;
    
    var mplayer = getMPlayerById(mplayer_id);
    mplayer.sendEvent('playpause');

    var mplayer_pholder_id = mplayer_id + '_pholder';
    var mplayer_pholder = document.getElementById(mplayer_pholder_id);

    if (FSZ4eq1 == null) {
        setupFSZPlayer('fsz_player_flv', file, id, start, stop, title, image, more_streams, mplayer, mplayer_pholder);
    }

    FSZ4eq1.open();
}
// }}} openFullSizeView()

function getUpdate(type, pr1, pr2)
{
    if (FSZ4eq1 != null) FSZ4eq1.getUpdate(type, pr1, pr2);
};

