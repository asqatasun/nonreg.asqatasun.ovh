function openMultimediaWindow(site, width, height) {
	var Ausdruck = /^\/sendungen/;
	var i = Ausdruck.exec(site);
	if (i) {
	   windowOpen(site,width,height);
	} else {
	    location.href = site;
	}
}
function windowOpen(url, width, height, scrollbars, location) {
	if (scrollbars != 'no') var scrollbars = 'yes';
	if (location == 1) {
		popupWindow = window.open(url, "popup", "height="+height+",width="+width+",locationbar=yes,location=yes,menubar=yes,toolbar=yes,scrollbars="+scrollbars+",resizable=yes");
	} else {
		popupWindow = window.open(url, "popup", "height="+height+",width="+width+",locationbar=no,location=no,menubar=no,toolbar=no,scrollbars="+scrollbars+",resizable=yes");
	}
	popupWindow.focus();
}
function winOpen(name, url, width, height, scrollbars) {

	if (scrollbars != 'no') var scrollbars = 'yes';
	if (name == '') var name = 'popup';
	if (popupWindow != null && popupWindow.closed == false) popupWindow.close();
	
	popupWindow = window.open(url, name, "height="+height+",width="+width+",locationbar=no,menubar=no,scrollbars="+scrollbars+",resizable=yes");
	popupWindow.focus();
}
var popupWindow = null;


// neue Fenster ankuendigen
function popupLinkTitleAnpassen() {
    if (document.getElementById) {
        var anker = '';
        for(var i=0; i<document.links.length; i++) {
            anker += document.links[i].className;
            var blank = ' ';
            if (document.links[i].title == '') {
                blank = '';
            }
//            if (document.links[i].className.indexOf('multimediaFenster') != -1 || document.links[i].className.indexOf('lupe') != -1) {
//                document.links[i].title = document.links[i].title + blank + '[neues Fenster]';
//            }
        }
    }
}

temp = window.onload;
counter22 = 0;
window.onload = function() {
if(temp != null && counter22 == 0) {
counter22++;
temp();
}
popupLinkTitleAnpassen();
}
