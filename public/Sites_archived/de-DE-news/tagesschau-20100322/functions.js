/*	Aktuelle XP Revision: $Revision$
	Letzte XP Aenderung am: $Date$ GMT */
/* for the IE only workarounds */
var ie = (document.all) ? true : false;
/* to get rid of the pre-text in inputs */
/* needs two functions, if a ...... user enters the default Value... */
function onFocusCheckEntry(which, cont, colorChange) {
	if(cont !=  "dummy" && which.value == cont) {
		which.value = "";
	}
}

function onBlurCheckEntry(which, cont, colorChange) {
	if(cont !=  "dummy" && which.value == '') {
		which.value = cont;
	}
}

/*	function for the View of the title-Attribute beside the icons */
function tiVi(linkObj) {
	if(document.getElementsByTagName) {
		text = " ";
		spanObj = linkObj.parentNode.parentNode.parentNode.getElementsByTagName("span");
		titleViewObj = spanObj[spanObj.length-1].firstChild;
		if(titleViewObj.nodeValue != linkObj.getAttribute("title")) text = linkObj.getAttribute("title");
		titleViewObj.nodeValue = text;
	}
}

function alerts() {
}
		
/*function init() {
	if(typeof changeTitleAndTarget != "undefined") changeTitleAndTarget();
	if(typeof bilderstreckeVars != "undefined") bilderstreckeVars();
	if(typeof eilScroll != "undefined") eilScroll();
}

window.onload = function() {
					if(typeof window.onload == "function")window.onload;
					bilderstreckeVars();
				}*/