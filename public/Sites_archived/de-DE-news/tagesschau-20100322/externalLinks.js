var typeOfLink = new Array(2);
for(i = 0; i < typeOfLink.length; i++) {
	typeOfLink[i] = new Array(2);
}
//WICHTIG: 0 und gerade = extern, ungerade = ard-intern
typeOfLink[0][0] = "Externer Link";
typeOfLink[0][1] = " [neues Fenster]";

typeOfLink[1][0] = "Mehr bei";
typeOfLink[1][1] = " [neues Fenster]";

function changeTitleAndTarget() {
	if(!document.getElementsByTagName) return;
	var links = document.getElementsByTagName("a");
	var buttons = document.getElementsByTagName("button");
	var inputs = document.getElementsByTagName("input");
	var areas = document.getElementsByTagName("area");
	if(buttons.length != 0 || areas.length != 0) {
		var elemsArr = new Array();
		for(i = 0; i < links.length; i++) elemsArr[i] = links[i];
		for(i = 0; i < buttons.length; i++) elemsArr[elemsArr.length] = buttons[i];
		for(i = 0; i < inputs.length; i++) elemsArr[elemsArr.length] = inputs[i];
		for(i = 0; i < areas.length; i++) elemsArr[elemsArr.length] = areas[i];
		links = elemsArr;
	}

	for(i = 0; i < links.length; i++) {
		changed = false;
		var singleLink = links[i];
		tOld = singleLink.getAttribute("title");
		if(tOld) {
			for(j = 0; j < typeOfLink.length; j++) {
				if((tOld.indexOf(typeOfLink[j][0]) != -1)) {
					if(tOld.indexOf(typeOfLink[j][1]) != -1) break;
					if(j%2 == 0) {
						titleAddition = tOld.substring(typeOfLink[j][0].length, tOld.length);
						singleLink.setAttribute("title",typeOfLink[j][0] + typeOfLink[j][1] + titleAddition);
						changed = true;
					} else {
						tFront = tBack = "";
						if(tOld.indexOf(":") != -1) {
							tFront = tOld.substring(typeOfLink[j][0].length, tOld.indexOf(":"));
							tBack = tOld.substring(tOld.indexOf(":"), tOld.length);
						} else {
							tFront = tOld.substring(typeOfLink[j][0].length, tOld.length);
							tBack = "";
						}
						singleLink.setAttribute("title",typeOfLink[j][0] + tFront + typeOfLink[j][1] + tBack);
						changed = true;
					}
					if(changed) {
						hasOnClick = singleLink.getAttribute("onclick");
						if(hasOnClick == null) {
							singleLink.target = "_blank";						    
						    // handelt es sich um einen Button und gibt es ein zugehoeriges Form?
						    bForm = singleLink.form;
						    if(bForm) bForm.target = "_blank";
						}
					}
					break;
				}
			}
		}
	}
}
	
	var counter = 0;
	tempExternal = window.onload;
	window.onload = function() {
						if(counter == 0) {
							if(typeof tempExternal == "function") tempExternal();
							changeTitleAndTarget();
							counter++;
						}
					}
