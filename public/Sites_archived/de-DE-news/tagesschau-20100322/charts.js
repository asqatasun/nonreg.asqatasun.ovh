/*	Aktuelle XP Revision: $Revision$
	Letzte XP Aenderung am: $Date$ GMT */
	slidersArr = new Array("rateNsd","rateDow","rateEst","rateDax");
	oldLi = "rateDax";
	agentOpera = !(navigator.userAgent.indexOf("Opera") == -1);
	
	function chartsVars() {
		if(document.getElementById(slidersArr[0]) && !agentOpera) {
			for(i = 0; i < slidersArr.length; i++) {
				document.getElementById(slidersArr[i]).getElementsByTagName("div")[0].getElementsByTagName("a")[0].onclick = changeChart;
				if(slidersArr[i] != oldLi) {
					document.getElementById(slidersArr[i]).getElementsByTagName("div")[1].getElementsByTagName("img")[0].style.visibility = "hidden";
				}
			}
		}
	}
	
	function changeChart(e) {
		clickedLiObj = (document.all) ? window.event.srcElement.parentNode.parentNode.parentNode : e.target.parentNode.parentNode.parentNode.parentNode;
		oldLiObj = document.getElementById(oldLi);
		oldLiObj.getElementsByTagName("div")[0].style.borderBottomColor = "#00044B";
		clickedLiObj.getElementsByTagName("div")[0].style.borderBottomColor = "#EEF";
		oldLiObj.getElementsByTagName("div")[0].style.backgroundColor = "#FFF";
		clickedLiObj.getElementsByTagName("div")[0].style.backgroundColor = "#EEF";
		oldLiObj.getElementsByTagName("div")[1].getElementsByTagName("img")[0].style.visibility = "hidden";
		clickedLiObj.getElementsByTagName("div")[1].getElementsByTagName("img")[0].style.visibility = "visible";
		oldLi = clickedLiObj.id;
		return false;
	}
	
	var counter = 0;
	tempCharts = window.onload;
	window.onload = function() {
						if(counter == 0) {
							if(typeof tempCharts == "function") tempCharts();
							chartsVars();
							counter++;
						}
					}