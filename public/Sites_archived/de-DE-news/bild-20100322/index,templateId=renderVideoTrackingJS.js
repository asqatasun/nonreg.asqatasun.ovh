
function videoTrackingEvent(filmName, eventBezeichnung, zeit, prozent, useCookie, searchterm, documentName, durationSec, withAdsString, layout, description, firstPublicationDate, videoKeyword)
{

	try
	{
		var webtrekkAction
		
		switch(eventBezeichnung)
		{
			case 'playClick':
				webtrekkAction = "init"
				zeit = 0 
				break
			case 'playStart':
				webtrekkAction = "play"
				break
			case 'pause':
				webtrekkAction = "pause"
				break
			case 'playComplete':
				webtrekkAction = "eof"
				break
			case '""':
				webtrekkAction = "pos"
				break
			default:
				break
		}
		if (webtrekkAction)
		{
			var url = window.location.pathname
			if (zeit == '""')
			{   
				zeit = 0
			}
			var parString = "ck1=" + withAdsString + ";ck2=" + layout + ";ck3=" + url + ";ck4=" + description + ";ck8=" + withAdsString + "/" + videoKeyword + ";ck9=" + firstPublicationDate
			wt_sendinfo_media(documentName, webtrekkAction, zeit, durationSec, parString, "", "", "")
		}
 	}
	catch (e)
	{
	}


} // videoTrackingEvent
