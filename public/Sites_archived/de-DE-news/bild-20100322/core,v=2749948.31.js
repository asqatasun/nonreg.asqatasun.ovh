/*core:2749948.31*/
// Zaehlen von SEM-Kampagnen
if (window.location.search.indexOf("src=epro") >= 0)
{
  var expires = new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 30);
  document.cookie = "epro=1; path=/; expires=" + expires.toGMTString() +";";
}
function videoPopup(ziel) {
document.getElementById('iframeVideo').src = ziel;
}

function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      oldonload();
      func();
    }
  }
}

function setFrameName(pname) {
window.name=pname;
}

function FramesetLaden(url) {
window.name="btobody";
}

function FramesetLaden2(url)
{
if (self==top)
{
if (top.location.replace)
{
   top.location.replace(url);
}
else
{
   top.location.href=url;
}
}
}

function openSendWindow(url)
{
window.open(url,"SendImage","width=390,height=410,resizable=no,scrollbars=no,menubar=no,directories=no,screenX=200,screenY=150,Left=200,Top=150").focus();
}

function openBookmarkWindow(url)
{
window.open(url,"Bookmarken","width=390,height=282,resizable=no,scrollbars=no,menubar=no,directories=no").focus();
}

var PAID_CAMPAIGN=1;
var DEFAULT_CAMPAIGN=2;
var SYSTEM_DEFAULT=3;

// Ueberpruefen, ob Falk ein Werbebanner ausgeliefert hat
function isFalkAdShown()
{
  if (typeof(Ads_status) == "undefined")
    return false;

    for (var i in Ads_status)
    {
      if (Ads_status[i][0] != PAID_CAMPAIGN)
        return false;
    } // for

  return true;
} // isFalkAdShown


// Hoehe der ausgelieferten  Werbung ermitteln
function getFalkAdHeight()
{
  if (typeof(Ads_status) == "undefined")
    return 0;

  for (var i in Ads_status)
  {
    if (Ads_status[i][0] == PAID_CAMPAIGN)
      return Ads_status[i][2];
  } // for

  return 0;
} // getFalkAdHeight


// Breite der ausgelieferten  Werbung ermitteln
function getFalkAdWidth()
{
  if  (typeof(Ads_status) == "undefined")
    return 0;

  for (var i in Ads_status)
  {
    if (Ads_status[i][0] == PAID_CAMPAIGN)
      return Ads_status[i][1];
  } // for

  return 0;
} // getFalkAdWidth


function followLink(pUrl, pTarget, pCounter)
{
    loadRedirectPixel(pCounter);

    if (pTarget == "btobody")
        setBtobody(pUrl);
    else
        window.open(pUrl, "_blank");
}

function followLink2(pUrl, pTarget, pAttrib, pCounter)
{
    loadRedirectPixel(pCounter);
    if (pTarget == "btobody")
        setBtobody(pUrl);
    else
        window.open(pUrl, pTarget, pAttrib);
}

function setBtobody (pUrl)
{
    if (top.opener == null)
    {
        if (parent.btobody == null)
            location.href = pUrl;
        else
            parent.btobody.location.href = pUrl;
    }
    else
    {
        if (top.opener.parent.btobody == null) {
            if (parent.btobody == null) {
                location.href = pUrl;
            }
            else {
                parent.btobody.location.href = pUrl;
            }
        }
        else {
            top.opener.parent.btobody.location.href = pUrl;
        }
    }
}

function loadRedirectPixel(pCounter)
{
    if ((pCounter == null) || (pCounter.length == 0))
        return;
    
    pixel = new Image();
    pixel.src = "http://dyn.bild.de/toolkit/ClientServlet.jsp?komponenten_id=5&server_action=0&pixel=" + pCounter;
    pixel.seconds = 5;
    pixel.wait = waitForPixel;
    pixel.interval = setInterval("pixel.wait()", 1000);
    pixel.wait();
}

function waitForPixel()
{
    this.seconds--;
    if (document.layers || this.complete || (this.seconds <= 0))
        clearInterval(this.interval);
}

function openWindowAndCount(pLink, pName, pSize, pCounter)
{
    loadRedirectPixel(pCounter);
    window.open(pLink, pName, pSize);
}

function callLinkTrackerPixel(pCounter) {
    pixel = new Image();
    pixel.src = "http://dyn.bild.de/toolkit/ClientServlet.jsp?komponenten_id=5&server_action=0&pixel=" + pCounter;
    pixel.seconds = 5;
    pixel.wait = waitForPixel;
    pixel.interval = setInterval("pixel.wait()", 1000);
    pixel.wait();
}

function gc(n){var a=document.cookie.split("; ");var i;var c=new Array();for (i=0;i<a.length;i++){var p=a[i].indexOf("=");if(p>=0){c[a[i].substring(0,p)]=a[i].substring(p+1);}}return c[n];}

function winOpen(url,name,breite,hoehe)
{
/* Errechnet Koordinaten, um das Popup zentriert zu platzieren */
links=(screen.width/2)-(breite/2);
oben=(screen.height/2)-(hoehe/2);
window.open(url,name,"height="+hoehe+",width="+breite+",status=no,toolbar=no,menubar=no,location=no,resizable=no,titlebar=no,scrollbars=no,fullscreen=no,top="+oben+",left="+links);
}

function getArg(n)
{
var r="";
var p=window.location.search;
if (p.length>2)
{
	var s=p.substring(1,p.length).split("&");
	var t={};
	for (var i=0;i<s.length;i++)
	{
	var c=s[i].split("=");
	t[c[0]]=c[1];
	}
	
	if (t[n])
	{
	r=t[n];
	}
}

return r;
}

function getArgO()
{
var argO=getArg("o");
if (argO.length>0)
{
	argO="/"+argO;
}
else
{
	argO=getArg("site");
	if (argO.length>0)
	{
	argO="/"+argO;
	}
}

return argO;
}

function ivw(p)
{
ivwpixel=new Image();
ivwpixel.src=p+"?r="+escape(document.referrer)+"&zeit="+new Date().getTime();
}

if (window.adgroupid==undefined) window.adgroupid=Math.round(Math.random()*1000);

var google_adnum = 0;
function checkFullBanner()
{
var fullBanner = document.getElementById("fullBanner");
if (fullBanner != null && fullBanner.offsetHeight <= 2)
    fullBanner.className = "hide";
}
function callTeaserPage(classId, segmentList){
    segmentList.each(function(item){
        if(item == classId){
            $$('div.'+item).each( function(element) {
                element.className = item + ' unhide ';
         $(item).className='pagActive';
            });
        }else{
            $$('div.'+item).each( function(element) {
                element.className = item + ' hide';
         $(item).className='';
            });
        }
    });
}
