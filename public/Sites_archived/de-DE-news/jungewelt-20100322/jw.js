/**
 * @created 2006-01-20
 * @updated 2006-04-15
 * @updated 2006-08-22
 * @copyright warenform.net
 */

// search

function checkSearchField(id) {
	var e = document.getElementById(id);
	if (e.value == e.defaultValue) {
		window.location.href = '/archiv/';
		return false;
	}
	return true;
}

// event calendar

var cName = 'jw-plz_';

function setPlz(plz) {
	if (plz.match(/^\d$/)) {
		setCookie(cName, plz, 7001, '/');
		eventShow(getPlzFromCookie());
	}
}

function setCookie(name, value, expires, path) {
	var today = new Date();
	today.setTime(today.getTime());
	if (expires) {
		expires = expires * 1000 * 60 * 60 * 24;
	}
	var expires_date = new Date(today.getTime() + expires);
	
	document.cookie = name + "=" + escape(value) +
	((expires) ? ";expires=" + expires_date.toGMTString() : "") + 
	((path) ? ";path=" + path : "");
}

function getCookie(name) {
	var start = document.cookie.indexOf( name + "=" );
	var len = start + name.length + 1;
	if ((!start) && (name != document.cookie.substring(0, name.length))) {
		return null;
	}
	if (start == -1) return null;
	var end = document.cookie.indexOf( ";", len );
	if (end == -1) end = document.cookie.length;
	return unescape(document.cookie.substring(len, end));
}

function getPlzFromCookie() {
	var plz = -1;
	plz = getCookie(cName);
	if (plz==null || plz < 0 || plz > 9) {
		return -1;
		//plz = Math.floor(Math.random() * 9);
	}
	return plz;
}

function eventShow(plz) {
	if (plz==null || plz < 0 || plz > 9) {
		return;
	}
	var idExists = false;
	var e = document.getElementById('ID_CalendarUl');
	for (i=0; i<e.childNodes.length; i++) {
		if (e.childNodes[i].tagName == 'LI') {
			if (e.childNodes[i].id != 'ID_Calendar_PLZ_' + plz) {
				e.childNodes[i].style.display = 'none';
			} else {
				e.childNodes[i].style.display = 'block';
				idExists = true;
			}
		}
	}
	document.getElementById('ID_Calendar_None').innerHTML = (idExists ? '' : 'Zur Zeit kein Termin im PLZ-Bereich ' + plz + '.');
}
