
function layer_move(x_pos,y_pos){
 var obj=this.style;
 if(this.NS){obj.moveTo(x_pos,y_pos);}
 else{obj.top=y_pos;obj.left=x_pos;}
}

function layer_size(width,height){
 var obj=this.style;
 if(this.NS){obj.clip.width=width;obj.clip.height=height;}
 else{
  if(this.MZ){obj.width=width;obj.height=height;}
  else{obj.clip='rect(auto '+(width)+' '+(height)+' auto)';}
 }
}

function layer_clear(){
 if(this.NS){
  this.document.open();
  this.document.write('');
  this.document.close();
 }
 else{
  this.layer.innerHTML='';
 }
}

function layer_write(htmlCode){
 if(this.NS){
  this.document.write(htmlCode);
  this.document.close();
 }
 else{
  this.layer.innerHTML+=htmlCode;
 }
}

function layer_setVisible(){this.style.visibility=this.SHOW;}
function layer_setInvisible(){this.style.visibility=this.HIDE;}
function layer_isVisible(){return(this.style.visibility==this.SHOW);}
function layer_getWidth(){return((this.NS)?this.style.clip.width:(this.MZ)?parseInt(this.style.width):this.style.pixelWidth);}
function layer_getLeft(){return((this.NS)?this.style.left:(this.MZ)?parseInt(this.style.left):this.style.pixelLeft);}
function layer_getTop(){return((this.NS)?this.style.top:(this.MZ)?parseInt(this.style.top):this.style.pixelTop);}
function layer_getHeight(){return((this.NS)?this.style.clip.height:(this.MZ)?parseInt(this.style.height):this.style.pixelHeight);}
function layer_setWidth(width){this.size(width,this.getHeight());}
function layer_setLeft(left){this.move(left,this.getTop());}
function layer_setTop(top){this.move(this.getLeft(),top);}
function layer_setHeight(height){this.size(this.getWidth(),height);}

var tab_layer = new Array();

function Layer(layerName){
 var d=document;
 this.IE = (document.all)?1:0;
 this.NS = (document.layers)?1:0;
 this.MZ = ((navigator.appName=='Netscape')&&(parseInt(navigator.appVersion)>=5))?1:0;
 this.SHOW = (this.NS)?'show':'visible';
 this.HIDE = (this.NS)?'hide':'hidden';
 this.name = layerName;
 this.layer = (this.NS)?d.layers[layerName]:(this.MZ)?(d.getElementById(layerName))?d.getElementById(layerName):d.getElementByName(layerName):d.all[layerName];
 this.style = (this.NS)?this.layer:this.layer.style;
 this.document = (this.NS)?this.layer.document:d;
 this.getWidth = layer_getWidth;
 this.getLeft = layer_getLeft;
 this.getTop = layer_getTop;
 this.getHeight = layer_getHeight;
 this.setWidth = layer_setWidth;
 this.setLeft = layer_setLeft;
 this.setTop = layer_setTop;
 this.setHeight = layer_setHeight;
 this.isVisible = layer_isVisible;
 this.setVisible = layer_setVisible;
 this.setInvisible = layer_setInvisible;
 this.move = layer_move;
 this.size = layer_size;
 this.write = layer_write;
 this.clear = layer_clear;
 this.index = tab_layer.length;
 tab_layer[this.index] = this;
}
