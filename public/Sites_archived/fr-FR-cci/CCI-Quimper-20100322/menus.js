NS = (document.layers);
MZ = ((navigator.appName=='Netscape')&&(parseInt(navigator.appVersion)>=5));
IE = (document.all);
mg_bas = null;
mg_haut = null;

function AnimeMenu(n,offset){
 working=1;
 if(n<(nmenus+1)){
  var i=n+1;
  for(;i<=nmenus;i++){
   var old_top = menus[i].menu.getTop();
   menus[i].menu.move(mainLeft,old_top+offset);
  }
 }
 mg_bas.move(mainLeft,mg_bas.getTop()+offset);
 menus[n].sousmenu.setVisible();
 mh = menus[n].varHeight;
 mh += offset;
 menus[n].varHeight = mh;
 menus[n].sousmenu.size(199,menus[n].varHeight);
 if(!MZ){
  if(((menus[n].varHeight<(menus[n].fullHeight-1))&&(offset>0))||((menus[n].varHeight>0)&&(offset<0))){setTimeout('AnimeMenu('+n+','+offset+')',40);}
  else{
   working=0;
   if (offset<0)
    menus[n].sousmenu.setInvisible(); 
  }
 }
 else{
  working=0;
  if (offset<0)
   menus[n].sousmenu.setInvisible(); 
 }
 if(!NS)
  document.spacerMenuGauche.height = mg_bas.getTop() - mg_haut.getTop() + mg_bas.getHeight();
  if(MZ)
   document.spacerMenuGauche.height+=2;
}

function Menu(n){
 this.menu = null;
 this.sousmenu = null;
 this.fullHeight = 0;
 this.varHeight = 0;
}

function ShowMenu(n){
 if(working){return;}
 if(curmenu){
  if(MZ)
   AnimeMenu(curmenu,-menus[curmenu].fullHeight);
  else
   AnimeMenu(curmenu,-16);
  menus[curmenu].menu.document["MTI"+curmenu].src = '/images/spacer.gif';
  if (curmenu!=n)
   tid = setInterval('ShowMenu2('+n+')',100);
  else
   curmenu = 0;
  return;
 }
 menus[n].menu.document["MTI"+n].src = '/images/menu-selected.gif';
 if(MZ)
  AnimeMenu(n,menus[n].fullHeight);
 else
  AnimeMenu(n,16);
 curmenu=n;
}

function ShowMenu2(n){
 if(tid)clearInterval(tid);
 if(working)
  tid=setInterval('ShowMenu2('+n+')',10)
 else
 {
  menus[n].menu.document["MTI"+n].src = '/images/menu-selected.gif';
  if (MZ)
   AnimeMenu(n,menus[n].fullHeight);
  else
   AnimeMenu(n,16);
  curmenu = n;
 }
}

function InitMenus(nombre_total_menus){
 menus[0] = null;
 var i=1;
 if (!nombre_total_menus){nombre_total_menus=13;}
 while (i<nombre_total_menus){
  menus[i] = new Menu(i);
  menus[i].menu = new Layer("MT"+i);
  menus[i].sousmenu = new Layer("MC"+i);
  menus[i].fullHeight = menus[i].sousmenu.getHeight();
  menus[i].sousmenu.size(199,0);
  menus[i].sousmenu.setInvisible();
  i++;
 }
 mg_bas = new Layer("MTB");
 mg_haut = new Layer("MTH");
}

function writeMenu(){
 var a = writeMenu.arguments;
 var i = 0;
 var dd=(NS)?document.layers[a[0]].document:document;
 switch(a[0].charAt(1)){
 case 'C': // menu de gauche "Contenu"
  var num = a[0].substring(2,a[0].length);
  dd.write('<TABLE WIDTH=199 HEIGHT=15 BORDER=0 CELLSPACING=0 CELLPADDING=0>');
  for (;i<((a.length-1)/3);i++){
   dd.write('<TR>');
   dd.write('<TD CLASS="mg2" background="/images/fond-gauche-'+(1+a[3*i+3])+'.jpg"><A HREF="'+a[3*i+2]+'?menu='+num+'"><img src="/images/spacer.gif" width='+(15+a[3*i+3]*12)+' height=14 border=0><font color="#'+((a[3*i+3]==1)?'6F6F6F':'757575')+'">'+a[3*i+1]+'</font></A></TD>');
   dd.write('</TR>');
  }
  break;
 case 'T': // menu de gauche "Titre"
  dd.write('<TABLE WIDTH=199 HEIGHT=17 BORDER=0 CELLSPACING=0 CELLPADDING=0>');
  dd.write('<TR>');
  dd.write('<TD CLASS="mg" background="/images/fond-gauche-1.jpg"><A HREF="Javascript:ShowMenu('+a[1]+');"><img name="MTI'+a[1]+'" src="/images/spacer.gif" width=20 height=16 border=0><font color="#1D1D1D">'+a[2]+'</font></A></TD>');
  dd.write('</TR>');
  break;
 default:
  return;
 }
 dd.write('</TABLE>');
}

function writeMenuHaut() {
 var a = writeMenuHaut.arguments;
 var l = a.length-3;
 var i = 1;
 var dd=(NS)?document.layers[a[0]].document:document;
 dd.write('<tr><td class="mhc" background="/images/fond-haut-2.jpg"><img src="/images/spacer.gif" width=15 border=0 height=14><font color="#1D1D1D">'+a[1]+'</font></td></tr>');
 for (;i<=(l/2);i++){
  dd.write('<tr><td class="mhc" background="/images/fond-haut-1.jpg"><A HREF="'+a[i*2+2]+'"><img src="/images/spacer.gif" width=25 border=0 height=14><font color="#6F6F6F">'+a[i*2+1]+'</font></A></td></tr>');
 }
}

function rollMH(objLayer){
 var obj = eval(objLayer);
 if(mh1.isVisible()&&(obj.name=='MH2')){
  mh1.setInvisible();
  mh2.setVisible();
 }
 if(mh2.isVisible()&&(obj.name=='MH1')){
  mh2.setInvisible();
  mh1.setVisible();
 }
}

function toggleMH(objLayer){
 var obj = eval(objLayer);
 (obj.isVisible())?obj.setInvisible():obj.setVisible();
}