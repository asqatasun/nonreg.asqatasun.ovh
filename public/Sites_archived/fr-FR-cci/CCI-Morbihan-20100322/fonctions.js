

/*****************  gestion dynamique de la taille de police  *****************/
/*****************                 ylc 08/2007                *****************/

var MinFontSize = 0.8;
var MaxFontSize = 2.5;
var ActualFontSize = 1;

function modFontSize(step) {
    ActualFontSize += (step/10);
    if (ActualFontSize > MaxFontSize) ActualFontSize = MaxFontSize;
    if (ActualFontSize < MinFontSize) ActualFontSize = MinFontSize;
    document.getElementById('corps').style.fontSize = ActualFontSize+'em';
}