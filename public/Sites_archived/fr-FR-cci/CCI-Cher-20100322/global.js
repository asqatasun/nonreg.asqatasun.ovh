// JavaScript Document

// fonctionnement du menu
	function ouvrirFermerMenu(menu,lien,actif,inactif)
	{
		nbMenus = menus.length;
		
		for (i=0;i<nbMenus;i++)
		{
			if(menus[i]==menu)
			{
				if(document.getElementById(menu).style.display == 'none')
				{
					document.getElementById(menu).style.display = 'block';
					document.getElementById(lien).className = actif;
					
				}
				else if(document.getElementById(menu).style.display == 'block')
				{
					document.getElementById(menu).style.display = 'none';
				}
			}
			else
			{
				if(document.getElementById(menus[i]).style.display == 'block')
				{
					document.getElementById(menus[i]).style.display = 'none';
					if(document.getElementById(liens[i]).className == 'niv1Actif niv1BleuActif')
					{
						document.getElementById(liens[i]).className = 'niv1 niv1Bleu';
					}
					if(document.getElementById(liens[i]).className == 'niv1Actif niv1OrangeActif')
					{
						document.getElementById(liens[i]).className = 'niv1 niv1Orange';
					}
					if(document.getElementById(liens[i]).className == 'niv1Actif niv1VertActif')
					{
						document.getElementById(liens[i]).className = 'niv1 niv1Vert';
					}
				}
			}
		}
	}
	
	function survolMenu(lien,menu,actif)
	{
		document.getElementById(lien).className = actif;
	}
	
	function quitteSurvolMenu(lien,menu,actif,inactif)
	{
		if(menu!=0)
		{
			if(document.getElementById(menu).style.display == 'none')
			{
				document.getElementById(lien).className = inactif;
			}
			else if(document.getElementById(menu).style.display == 'block')
			{
				document.getElementById(lien).className = actif;
			}
		}
		else
		{
			document.getElementById(lien).className = inactif;
		}
	}