<HTML>
<HEAD>
<TITLE>CCI eure et loir menu_haut</TITLE>
<meta name="keywords" content="commerce ext�rieur, export, exportations, formalit�s export, conventions d'affaire, Journ�es pays, International, formation, subvention, �conomie, �conomique, environnement, ISO 14000, ISO 9000, management environnemental, industrie, r�glementation industrielle, installation class�e, transcommerce, commerce, commer�ants prestataires de services, industriels, concours, cuisine, restauration, euro, monnaie europ�enne, kit boutique euro, entreprendre, cr�ation d'entreprise, reprise d'entreprise, fichiers, centre de formalit� des entreprises, stages, passeport en France, Club des cr�ateurs, aides, CFE, enseignement sup�rieur, �cole de commerce, emploi, march� �conomique, �tudes, alternance, cartographie, d�partement, SIG, �quipement commercial, documentation �conomique, information �conomique, Eure-et-Loir, Centre, Chartres, Nogent le Rotrou, Dreux, Ch�teaudun, organisme �conomique, chambre, technologie, innovation, fili�re automobile, constructeurs automobiles, qualit�, propri�t� industrielle, carnet ATA, Chambre, CCI, Chambre de Commerce et d'Industrie, 28, immatriculation, projet, conseil, financement, entreprise, entrepreneur, activit� �conomique, commer�ants non s�dentaires, chefs d'entreprises, tourisme, UCIA, Comit� Technique Local, Foires, sous-traitance, La lettre de la CCI, Communication �conomique, zone euro, Am�nagement du territoire, �tudes �conomiques, d�veloppement local, CDEC, Syst�me d'Information G�ographique, R�seau EGC, Etudes commerciales, Marketing, Gestion, BTS Commerce International, BTS Force de vente, BTS Assistant de Direction, Bac Pro Commerce, Contrat de qualification, Formation en alternance, Campus de Chartres, visas export, pollution, gestion des d�chets, gestion de l'eau, importation, certificats d'origine, imprim�s douaniers, hygi�ne alimentaire, zone urbaine sensible, NTIC, Intelligence �conomique, ESCEMESGC">
<meta name="description" content="La mission de la CCI est de d�velopper l'activit� des 11 000 commer�ants, industriels et prestataires de services d'Eure-et-Loir et � l'implantation de nouvelles entreprises dans le d�partement. Vous trouvez sur le site une pr�sentation de l'�conomie du d�partement et de la R�gion Centre, de l'annuaire des �lus de la Chambre et toute une gamme des services aux entreprises : aides � la cr�ation - reprise d'entreprise, formation professionnelle et initiale, d�veloppement international, �tudes de march�, services � l'industrie (environnement, qualit�, technologie-innovation, sous-traitance, NTIC), indices �conomiques du d�partement, revue de presse locale, agenda des manifestations, � Vous avez �galement la possibilit� d'�tre conseill� et de commander en ligne des �tudes �conomiques de la CCI.">
<meta name="robots" content="all">
<meta name="revisit" content="30 days">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<script language="JavaScript">
<!--
function chargeUrl(framea,urla,frameb,urlb){
	top.frames[framea].location.href=urla
 	top.frames[frameb].location.href=urlb}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>

</HEAD>
<BODY BGCOLOR=#FFFFFF ONLOAD="MM_preloadImages('images/menu_haut11.gif','images/menu_haut22.gif','images/menu_haut33.gif','images/menu_haut44.gif','images/menu_haut55.gif','images/menu_haut66.gif','images/menu_haut88.gif','images/menu_haut100.gif','images/menu_haut113.gif','images/menu_haut115.gif')" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background="menu_haut_fond.gif">
<table border="0" cellspacing="0" cellpadding="0" width="600">
  <tr> 
    <td colspan="2"><a href="javascript:chargeUrl('gauche','menu.php','bas','menu_central.php');" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('haut1','','images/menu_haut11.gif',1)"><img src="menu_haut1.gif" width="67" height="18" name="haut1" border="0"></a></td>
    <td><a href="http://www.eureetloir.cci.fr/contact1.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('haut2','','images/menu_haut22.gif',1)" target="bas"><img src="menu_haut2.gif" width="70" height="18" name="haut2" border="0"></a></td>
    <td colspan="2"><a href="http://www.eureetloir.cci.fr/agenda.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('haut3','','images/menu_haut33.gif',1)" target="bas"><img src="menu_haut3.gif" width="60" height="18" name="haut3" border="0"></a></td>
    <td colspan="3"><a href="http://www.eureetloir.cci.fr/informations/actu.php?action=actu" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('haut4','','images/menu_haut44.gif',1)" target="bas"><img src="menu_haut4.gif" width="81" height="18" name="haut4" border="0"></a></td>
    <td colspan="2"><a href="http://www.eureetloir.cci.fr/Ident.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('haut5','','images/menu_haut55.gif',1)" target="bas"><img src="menu_haut5.gif" width="83" height="18" name="haut5" border="0"></a></td>
    <td align="right"><a href="http://www.eureetloir.cci.fr/traduction.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('haut6','','images/menu_haut66.gif',1)" target="bas"><img src="menu_haut6.gif" width="40" height="30" name="haut6" border="0"></a></td>
  </tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="600">
  <tr> 
    <td colspan="2"><img src="menu_haut.gif" width="67" height="41"></td>
    <td colspan="2"><a href="http://www.eureetloir.cci.fr/presentez-vous.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('haut71','','images/menu_haut88.gif',1)" target="bas"><img src="menu_haut8.gif" width="118" height="15" name="haut71" border="0"></a></td>
    <td align="right"><img src="menu_haut9.gif" width="85" height="15"></td>
    <td valign="top"><a href="http://www.eureetloir.cci.fr/commander.php" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('haut81','','images/menu_haut100.gif',1)" target="bas"><img src="menu_haut10.gif" width="36" height="32" name="haut81" border="0"></a></td>
    <td align="right" colspan="2"><img src="menu_haut111.gif" width="81" height="15"></td>
    <td valign="top" colspan="2"><a href="http://www.eureetloir.cci.fr/recherche/recherche.asp" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('haut91','','images/menu_haut113.gif',1)" target="bas"><img src="menu_haut112.gif" width="37" height="32" name="haut91" border="0"></a> 
    </td>
    <td valign="top" align="right"><a href="http://www.eureetloir.cci.fr/traduction.php#espagnol" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('haut101','','images/menu_haut115.gif',1)" target="bas"><img src="menu_haut114.gif" width="40" height="32" name="haut101" border="0"></a></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</BODY>
</HTML>