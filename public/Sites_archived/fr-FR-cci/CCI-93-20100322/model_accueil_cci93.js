var letterexp = /[a-z]/i;
 function isValid(pattern, str)
{
	return pattern.test(str);
}
function hasLetter(str)
{
	return letterexp.test(str);
}

function checkform(form)
{
	if (!hasLetter(form.qu.value))
	{
		alert("Veuillez remplir le champs de recherche");
		form.qu.focus();
		return false
	}

	this.document.query.submit();
}


//onSubmit="checkform(this);"
