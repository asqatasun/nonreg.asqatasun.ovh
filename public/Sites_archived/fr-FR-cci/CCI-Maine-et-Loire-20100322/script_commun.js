//*****************************************************************************
// AGIIR Network /
// date de création: 04/02 VPN
// version: APV2.1.1
//*****************************************************************************
// Dernière Modification: 01/05/03 MCN
//				29/03/06 FDX
//*****************************************************************************

function supp_file(nom_fichier)
{
	document.getElementById(nom_fichier).value="";
	alert("La suppression a été prise en compte.\nLe fichier sera supprimé lors de l'enregistrement des modifications...");
}

function createCookie(name,value)
{
	days=5;
	if (days)
	{
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name)
{
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++)
	{
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name)
{
	createCookie(name,"",-1);
}



function Lancer(act)
{
    if (act=="quit.php")  if (!confirm("Souhaitez-vous vraiment quitter l'application ?")) return ;
	Lancerspecial(act,'');
}

/* *********VPN  MIS EN COMMENTAIRE LE 15 02 2008 ENLEVER SI ELLE SERT 
function Lancerajax(act,categpasse)
{
	if (act=="bve_recherche.php")
	{
        with(document.lanceur)
        {
			rubbve.value=document.recherche.trouv.value;
        }
	}
	
	document.lanceur.modeappel.value=categpasse;
	document.lanceur.pkcateg.value=categpasse;
    	document.lanceur.action=act;
	var donnees=document.getElementById('bvecoeur');
	ajax(document.lanceur, act, 'bve_coeur',donnees,reload_ajax);

    
}
*/

function Lancerspecial(act,categpasse)
{

//if (document.lanceur.pk_menu.value!=js_pkmenu) 
	Lancerspecial_changement_menu(act,categpasse);
//else Lancerajax(act,categpasse);

}

function Lancerspecial_changement_menu(act,categpasse)
{

	if (act=="bve_recherche.php")
	{
        with(document.lanceur)
        {
			rubbve.value=document.recherche.trouv.value;
        }
	}
	
	document.lanceur.modeappel.value=categpasse;
	document.lanceur.pkcateg.value=categpasse;
    with(document.lanceur)
    {
        action=act;
        submit();
    }
}


/* *********VPN  MIS EN COMMENTAIRE LE 15 02 2008 ENLEVER SI ELLE SERT 
function reload_ajax()
{
	
div_menu=document.getElementById("menu_fils2");
div_menu2=document.getElementById("menu_ajax");
div_menu.innerHTML=div_menu2.innerHTML;


}
*/

function Lancerspecialkey(act,categpasse,key)
{
	document.lanceur.modeappel.value=categpasse;
	document.lanceur.pkkey.value=key;
	document.lanceur.pkcateg.value=categpasse;
    with(document.lanceur)
    {
        action=act;
        submit();
    }
}

function Lancerspecialdoc(act,categpasse,key)
{
	document.lanceur.modeappel.value=categpasse;
	document.lanceur.pkdoc.value=key;
	document.lanceur.pkcateg.value=categpasse;
    with(document.lanceur)
    {
        action=act;
        submit();
    }
}

function Lancerspecialcateg(act,mode,categ)
{
	document.lanceur.modeappel.value=mode;
	document.lanceur.pkcateg.value=categ;
    with(document.lanceur)
    {
        action=act;
        submit();
    }
}


function Lancerged(act,categpasse)
{
	document.lanceur.pkcateg.value=categpasse;
    with(document.lanceur)
	{
		action=act;
		document.location="ged.php?pk_orga="+js_pkorga+"&pk_menu_fils="+categpasse;
	}
}

function Lancerged_ancre(act,categpasse,docpasse)
{
	document.lanceur.pkcateg.value=categpasse;
    with(document.lanceur)
	{
		action=act;
		document.location="ged.php?pk_orga="+js_pkorga+"&pkcateg="+categpasse+"#xml"+docpasse;
	}
}

function Lancergeddoc(act,categpasse,docpass)
{
    document.lanceur.pkcateg.value=categpasse;
    document.lanceur.pkkey.value=docpass;
    document.lanceur.modeappel.value="afficher_doc";
    with(document.lanceur)
                {
                        action=act;
			//method="get";
			document.location="ged.php?pk_orga="+js_pkorga+"&pkcateg="+categpasse+"&pkkey="+docpass+"&modeappel=afficher_doc"+"&loc=portail";
                        //submit();
                }
}

function Lancerged_dashboard(act,categpasse,menupere,menufils)
{
document.lanceur.pkcateg.value=categpasse;
    with(document.lanceur)
                {
                        action=act;
			//method="get";
			document.location="ged.php?pk_orga="+js_pkorga+"&pkcateg="+categpasse+"&modeappel=actu_intra_dashboard"+"&pk_menu_fils="+menufils+"&pk_menu="+menupere;
                        //submit();
                }
}


function reloadparam()
{
    with(document.lanceur)
                {
                        target="_self";
                }
}

/* Fonction de comparaison de dates, si datedeb < datefin ...
  * datedeb et datefin sont deux dates dans le format jj-mm-aaaa
  * Retourne true si OK false sinon
*/
function compare_dates(datedeb, datefin)
{
	var regex = /^([0-9]{2})-([0-9]{2})-([0-9]{4})$/; // Expression régulière
	var tabdeb = regex.exec(datedeb); // Séparation des champs des dates
	var tabfin = regex.exec(datefin);

	ddeb = new Date(tabdeb[3], tabdeb[2]-1, tabdeb[1]);
	dfin = new Date(tabfin[3], tabfin[2]-1, tabfin[1]);
	// Récupération de la date en millisecondes écoulées depuis le 1er janvier 1970
	timedeb = ddeb.getTime();
	timefin = dfin.getTime();
	if (timedeb <= timefin) return true;
	else return false;
}

/* Fonction de comparaison d'heures
  * heuredeb et heurefin sont deux heures dans le format hh:mm
  * Retourne true si OK false sinon
*/
function compare_heures(heuredeb, heurefin)
{
	var regex = /^([0-9]{1,2}):([0-9]{1,2})$/;
	var tabdeb = regex.exec(heuredeb); 
	var tabfin = regex.exec(heurefin);
	if(tabdeb[1]<10) tabdeb[1] = 0+tabdeb[1];
	if(tabfin[1]<10) tabfin[1] = 0+tabfin[1];
	if(tabdeb[1] > tabfin[1]) return false;
	else if(tabdeb[1]==tabfin[1])
	{	
		if(tabdeb[2]>tabfin[2]) return false;
		else return true;
	}
	else return true;
}

// Fonction pour cocher ou décocher un ensemble de checkbox dans le formulaire passé en paramètre
function checkAll(form, champs, valeur)
{
	for (i = 0; i < form.elements.length; i++){
		if(form.elements[i].name == champs)
			form.elements[i].checked = valeur;
	}
}

// Fonction de vérification qu'un checkbox au moins est coché dans le fom passé en paramètres
function verifCheckForm(form, champs)
{
	var ok = false;
	var i = 0;
	while (i < form.elements.length && ok == false) {
		if(form.elements[i].name == champs && form.elements[i].checked == 1) ok = true;
		i++;
	}
	return ok;
}

// Fonction d'impression
/*
function PSR_imprimer()
        {
			
			var PSR_f1 = null; 
            var PSR_content=document.getElementById('PSR_print').parentNode.innerHTML; 
            var PSR_title=document.getElementsByTagName('title')[0].innerText; 
            if (PSR_f1) {if(!PSR_f1.closed) PSR_f1.close();} 
            PSR_f1 = window.open ('',"PSR_f1", "height=600,width=700,menubar=yes,scrollbars=yes,resizable=yes,,left=10,top=10"); ; 
            PSR_f1.document.open(); 
            PSR_f1.document.write("<html><head><title>Impression de page</"+"title>"); 
            PSR_f1.document.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"./µREPµ/style/µSTYLEBUREAUµ\" />"); 
            PSR_f1.document.write("<link rel=\"StyleSheet\" href=\"./µREPµjscript/nlstree/nlstree.css\" type=\"text/css\" />"); 
            PSR_f1.document.write("<link rel=\"shortcut icon\" type=\"image/ico\" href=\"./µREPµ/images/favicon.ico\" />"); 
            PSR_f1.document.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />"); 
            PSR_f1.document.write("</"+"head>");
            PSR_f1.document.write("<body>"+PSR_content+"</"+"body></"+"html>");
            PSR_f1.document.close(); 
            PSR_f1.print();
            PSR_f1.focus(); 
        }
		*/
        
// Vidage des zones

	function vide_recherche()
	{
		document.recherche.mot_rech.value="";
 	} 

	function vide_login()
	{
		document.MonForm.login.value="";
 	}
	function vide_pass()
	{
		document.MonForm.password.value="";
 	} 	
    function validation()
    {
        with (document.MonForm)
        {
            var message_alert="Vous devez saisir :\n";
            if (login.value=="") message_alert+="- votre login\n";
            if (password.value=="") message_alert+="- votre mot de passe\n";
            if (message_alert!="Vous devez saisir :\n") 
            {
                alert(message_alert);
                return false;
            }
            else return true;
        }
    }
/*
// Fonction d'impression pour la ged
	function PSR_imprimer_ged(id)
    {
        var PSR_f1 = null; 
        var PSR_content=document.getElementById(id).parentNode.innerHTML; 
        var PSR_title=document.getElementsByTagName('title')[0].innerText; 
        if (PSR_f1) {if(!PSR_f1.closed) PSR_f1.close();} 
        PSR_f1 = window.open ('',"PSR_f1", "height=600,width=700,menubar=yes,scrollbars=yes,resizable=yes,,left=10,top=10"); ; 
        PSR_f1.document.open(); 
        PSR_f1.document.write("<html><head><title>Impression de page</"+"title>"); 
        PSR_f1.document.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"./µREPµ/style/µSTYLEBUREAUµ\" />"); 
        PSR_f1.document.write("<link rel=\"StyleSheet\" href=\"./µREPµjscript/nlstree/nlstree.css\" type=\"text/css\" />"); 
        PSR_f1.document.write("<link rel=\"shortcut icon\" type=\"image/ico\" href=\"./µREPµ/images/favicon.ico\" />"); 
        PSR_f1.document.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />"); 
        PSR_f1.document.write("</"+"head>");
        PSR_f1.document.write("<body>"+PSR_content+"</"+"body></"+"html>");
        PSR_f1.document.close(); 
        PSR_f1.print();
        PSR_f1.focus(); 
    }
*/
function curseur()
	{
		document.recherche.mot_rech.focus();
	}


/*
	Fonction permetant le requètage HTTP dynamique(AJAX) (YDE) FDX, VPN
*/
function lancer_ajax(form, action, tpl, item, fct, modeap, key)
{
	inputs=form.getElementsByTagName("input"); //.value=modeap;
	for(var i = 0; i < inputs.length; i++)
	{
		if (inputs[i].name=="modeappel") 
		inputs[i].value=modeap;
		if (inputs[i].name=="pkkey") inputs[i].value=key;
		if (inputs[i].name=="pk_key") inputs[i].value=key;
	}
	ajax(form, action, tpl, item, fct);
}

/*
	Fonction permetant le requètage HTTP dynamique(AJAX) (YDE)
*/

var AJAX_DEFAULT_MESSAGE = '<span class="libelle" style="vertical-align: center;"><img src="default_produit/images/ajax-loader.gif"/><span>';
var AJAX_VOID_MESSAGE = 'void';

function ajax(form, action, tpl, item, fct, message)
{
	var xhr = null;
	
	if(window.XMLHttpRequest) // Firefox et autres
	{
		xhr = new XMLHttpRequest();
	}
	else if(window.ActiveXObject) // Internet Explorer
	{
		try
		{
			xhr = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch(e)
		{
			xhr = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	else // XMLHttpRequest non supporté par le navigateur
	{
		alert("Votre navigateur ne prend pas en charge cette fonctionalité");
		xhr = false;
	}
	
	if(!message)
	{
		message = AJAX_DEFAULT_MESSAGE;
	}
	else if(message == AJAX_VOID_MESSAGE)
	{
		message = '';
	}
	
	xhr.onreadystatechange = function()
	{
		if(xhr.readyState == 4)
		{
			if(item !== null)
			{
				item.innerHTML = xhr.responseText;
			}
			try
			{
				fct();
			}
			catch(e)
			{
			}
		}
		else if(item && message != '')
		{
			item.innerHTML = message;
		}
	}
	
	var vars = "";
	var inputs = form.getElementsByTagName('input');
	var textareas = form.getElementsByTagName('textarea');
	var file = form.getElementsByTagName('file');
	var selects = form.getElementsByTagName('select');
	
	for(var i = 0; i < inputs.length; i++)
	{
		if(inputs[i].type == 'radio' || inputs[i].type == 'checkbox')
		{
			if(inputs[i].checked)
			{
				vars += inputs[i].name + "=" + encodeURIComponent(inputs[i].value) + "&";
			}
		}
		else vars += inputs[i].name + "=" + encodeURIComponent(inputs[i].value) + "&";
	}

	for(i = 0; i < textareas.length; i++)
	{
		if (textareas[i].className == 'mceEditor') textareas[i].value = tinyMCE.getContent(textareas[i].name);
		vars += textareas[i].name + "=" + encodeURIComponent(textareas[i].value) + "&";
	}

	for(i = 0; i < selects.length; i++)
	{
		if(selects[i].options.length > 0)
		{
			vars += selects[i].name + "=" + encodeURIComponent(selects[i].options[selects[i].selectedIndex].value) + "&";
		}
	}

	vars += "type_sortie=TYPE_SORTIE_SPECIFIQUE&tpl_ajax=" + tpl;
	xhr.open("POST", action, true);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=ISO-8859-1");
	xhr.send(vars);
}

/*
	Fonction AJAX simple permettant de lancer un traitement et retournant une valeur
*/
	function simple_ajax(form, action, modeap, fct, sortie)
	{
		var vars = "";
		var inputs = form.getElementsByTagName('input');
		var textareas = form.getElementsByTagName('textarea');
		var file = form.getElementsByTagName('file');
		var selects = form.getElementsByTagName('select');
		for(var i = 0; i < inputs.length; i++)
		{
			if (inputs[i].name=="modeappel") inputs[i].value=modeap;
			if(inputs[i].type == 'radio' || inputs[i].type == 'checkbox')
			{
				if(inputs[i].checked)
				{
					vars += inputs[i].name + "=" + encodeURIComponent(inputs[i].value) + "&";
				}
			}
			else 
			{
				vars += inputs[i].name + "=" + encodeURIComponent(inputs[i].value) + "&";
			}
		}
		for(var i = 0; i < textareas.length; i++)
		{
			if (textareas[i].className == 'mceEditor') textareas[i].value = tinyMCE.getContent(textareas[i].name);
			vars += textareas[i].name + "=" + encodeURIComponent(textareas[i].value) + "&";
		}
		for(i = 0; i < selects.length; i++)
		{
			vars += selects[i].name + "=" + encodeURIComponent(selects[i].options[selects[i].selectedIndex].value) + "&";
		}
		vars += "type_sortie=TYPE_SORTIE_VIDE";

		var xhr = null;
		if(sortie == 'xml')
		{
			if(window.ActiveXObject) {
                xhr = new ActiveXObject("Microsoft.XMLDOM");
                xhr.onreadystatechange = function() {
                        if(xhr.readyState == 4) {
                                fct(xhr);
                        }
                };
				xhr.load(action +"?" + vars);
	        }
	        else if(document.implementation && document.implementation.createDocument) 
			{
				xhr = document.implementation.createDocument("", "", null);
				xhr.async = false;
				var loaded = xhr.load(action +"?" + vars);
				setTimeout("fct(xhr)" , 1000);
				if (loaded) 
				{
					fct(xhr); 
				}
	        }       
	        else 
			{
				alert("Votre navigateur ne gère pas l'importation de fichiers XML");
				return;
	        } 
		}
		else
		{
			if(window.XMLHttpRequest) // Firefox et autres
			{
				xhr = new XMLHttpRequest();
			}
			else if(window.ActiveXObject) // Internet Explorer
			{
				try
				{
					xhr = new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch(e)
				{
					xhr = new ActiveXObject("Microsoft.XMLHTTP");
				}
			}
			else // XMLHttpRequest non supporté par le navigateur
			{
				alert("Votre navigateur ne prend pas en charge cette fonctionalité");
				xhr = false;
			}
			xhr.onreadystatechange = function()
			{
				if(xhr.readyState == 4)
				{
					if (fct != '')
					{
						try
						{
							fct(xhr.responseText);
						}
						catch(e)
						{
						}
					}
				}
			}
			xhr.open("POST", action, true);
			xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=ISO-8859-1");
			xhr.send(vars);
		}
	}

	function fuiuisdf()
	{
	   alert("bip");
	}
/*
	Fonctions rendant un tableau selectionable (YDE)
*/

function make_selectable(table, col_number, fct, row_start)
{
	for(var i = (row_start == null ? 1 : 0); i < table.rows.length; i++)
	{
		if(col_number >= 0)
		{
			table.rows[i].cells[col_number].style.cursor = "arrow";
			add_events_actions(table, table.rows[i].cells[col_number], fct);
		}
		else
		{
			table.rows[i].style.cursor = "arrow";
			add_events_actions(table, table.rows[i], fct);
		}
	}
}

function add_events_actions(table, item, fct)
{
	item.oncontextmenu = function(e)
	{
		unselect_all(table);
		item.onmousedown();
		try
		{
			fct(item);
		}
		catch(e)
		{
		}
		return false
	};
	item.onmousedown = function()
	{
		if(item.cells !== undefined)
		{
			var regexp = new RegExp("(selected)", "g")
			for(var i = 0; i < item.cells.length; i++)
			{
				if(item.cells[i].className.match(regexp))
				{
					item.cells[i].className = item.cells[i].className.replace(regexp,"");
				}
				else
				{
					item.cells[i].className += " selected";
				}
			}
		}
		else
		{
			var regexp = new RegExp("(selected)", "g")
			if(item.className.match(regexp))
			{
				item.className = item.className.replace(regexp,"");
			}
			else
			{
				item.className += " selected";
			}
		}
	}
}

function get_selected(table)
{
	var selected = new Array();
	var regexp = new RegExp("(selected)", "g")
	for(var i = 0; i < table.rows.length; i++)
	{
		if(table.rows[i].className.match(regexp))
		{
			selected.push(table.rows[i]);
		}
		else
		{
			for(var j = 0; j < table.rows[i].cells.length; j++)
			{
				if(table.rows[i].cells[j].className.match(regexp))
				{
					selected.push(table.rows[i].cells[j]);
				}
			}
		}
	}
	return selected;
}

function unselect_all(table)
{
	var regexp = new RegExp("(selected)", "g")
	for(var i = 0; i < table.rows.length; i++)
	{
		for(var j = 0; j < table.rows[i].cells.length; j++)
		{
			if(table.rows[i].cells[j].className.match(regexp))
			{
				table.rows[i].cells[j].className = table.rows[i].cells[j].className.replace(regexp,"")
			}
		}
	}
}


// FONCTION DE CONTROLE A UTILISER LORS DES FORMULAIRES DE SAISIE VPN 20 12 2007

var Reg_Exp = new Array();

Reg_Exp['Url'] = "(http|https)\://+\.[a-zA-Z]{2,3}\.([A-Za-z0-9])";
Reg_Exp['Mail'] = "/^[a-z0-9._-]+@[a-z0-9.-]{2,}[.][a-z]{2,3}$/";
Reg_Exp['Heure'] = "/^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/";
Reg_Exp['Telephone'] = "/^[0-9]{10}$/";
Reg_Exp['Code_Postal'] = "/^[0-9]{5}$/";
Reg_Exp['Alpha_Numerique'] = "/^[A-Za-z]+$/";
Reg_Exp['Code_Postal_FR'] = "/^(0[1-9]|[1-9][0-9])[0-9]{3}$/";
Reg_Exp['Code_Postal_BE'] = "/^(F-[0-9]{4,5}|B-[0-9]{4})$/";


//Fonction de vérification de saisie code postal
function verif_cp(cp)
{
	var reg_cp = Reg_Exp['Code_Postal'];
	if (!(reg_cp.exec(cp)!=null))
	{
		alert("Le code postal saisie n'est pas au format valide !");
		return(false);
	}
	return(true);
}

//Fonction de vérification de saisie code postal
function controle_cp(cp)
{
	var reg_cp = Reg_Exp['Code_Postal'];
	if (!(reg_cp.exec(cp)!=null))
	{
		message = "Le code postal saisie n'est pas au format valide !";
		return(message);
	}
	return(true);
}


//Fonction de vérification chaine de caratere sans chiffres
function verif_alphab (champ,saisie)
{
	var reg_alphab = Reg_Exp['Alpha_Numerique'];
	if (!(reg_alphab.exec(saisie)!=null))
	{
		alert("Le champ saisie n'est pas au format valide !");
		champ.focus();
	}
}

//Fonction de vérification saisie numérique (chiffres)
function verif_numeric(nb_saisie,champnum)
{
	var nb=nb_saisie.value;
	var rfocu=champnum;
	if ((isNaN(nb)))
	{
		alert("Le nombre saisi n'est pas valide !");
		return(false);
	}
	return(true);
}





//Fonction de vérification de saisie du numéro de téléphone français
function verif_tel(tel)
{
	if (tel!='')
	{
		var reg_tel = Reg_Exp['Telephone'];
		if (!(reg_tel.exec(tel)!=null))
		{
			alert("Le numero de tel saisie n'est pas au format valide !");
			return(false);
		}
	}
	return(true);
}

//Fonction de vérification de saisie d'heure
function verif_heure(champform,valeur)
{
	var ctl = Reg_Exp['Heure'];
	if (!(ctl.exec(valeur)!=null))
	{
		alert("L'heure saisie n'est pas au format valide !");
		champform.focus()
	}
}


//Fonction de vérification de saisie d'email au bon format
function verif_mail(email)
{
	if (email!='')
	{
		var reg_mail = Reg_Exp['Mail'];
	
		if (!(reg_mail.exec(email)!=null))
		{
			alert("L'adresse email saisie n'est pas au format valide !");
			return(false);
		}
	}
	return(true);
}

//Fonction de vérification de saisie d'url au bon format
function verif_url(url)
{
	if (url!='')
	{
		var reg_url = new RegExp(Reg_Exp['Url']); 
		if (!(reg_url.exec(url)!=null))
		{
			alert("L'URL saisie n'est pas au format valide !");
			return(false);
		}
	}
	return(true);
}





//fonction utilisant la vérification de la saisie au bon format d'une date et récupération de focus
function verifdate_blur(datesaisie,champdat)
{
	var dateaverif=datesaisie.value;
	var rfocu=champdat;
	// rangement de la date dans des variables
	if (dateaverif!="")
	{
		if (!verifdate(dateaverif)) 
		{
			alert("Attention soit la date n'est pas correcte, soit elle n'est pas au format JJ/MM/AAAA");
      DonnerFocus(rfocu);return false;
		}	
	}
	return true;
}


//fonction vérifiant la saisie au bon format d'une date
function verifdate(d) 
{
	var dateaverifier=d
	// rangement de la date dans des variables
	if (dateaverifier.substring(0,1)=="0")
	{
		var j=parseInt(dateaverifier.substring(1,2));
	}
	else 
	{
		var j=parseInt(dateaverifier.substring(0,2));
	}
	if (dateaverifier.substring(3,4)=="0")
	{
		var m=parseInt(dateaverifier.substring(4,5));
	}
	else 
	{
		var m=parseInt(dateaverifier.substring(3,5));
	}
	
	var a=parseInt(dateaverifier.substring(6,10));
	//si la longueur est différent de 10 , problème
	if (dateaverifier.length != 10) 
	{
		return false;
	}
	//les caratères / ne sont pas aux endroits attendus
	else 
	{
		if((dateaverifier.charAt(2) != '/') && (dateaverifier.charAt(5) != '/')) 
		{
			return false;
		}
	}

	//l'année n'est pa un chiffre
	if (isNaN(a))
	{
		return false;
	}
	
	//le mois n'est pas un chiffre ou n'est pas compris entre 0 et12
	if ((isNaN(m))||(m<1)||(m>12))
	{
		return false;
	}
	
	//test si il s'agit d'une année bissextile pour accepter le 29/02
	if (((a % 4)==0 && (a % 100)!=0) || (a % 400)==0)
	{
		if ((isNaN(j)) || ((m!=2) && ((j<1)||(j>31))) || ((m==2) && ((j<1)||(j>29))))
		{
			return false;
		}
	}
	else 
	{
		if ((isNaN(j)) || ((m!=2) && ((j<1)||(j>31))) || ((m==2) && ((j<1)||(j>28))))
		{
			return false;
		}
	}
	return true;
}


function Lancer_xslt(key,fic_xslt)
{
	//var Left=window.screen.width/2-175; 
	var Left=150; 		
	var Top=50;
	//var Top=window.screen.height/2-175; 
	//document.frm_newsscateg.target="dossier";
	open("","dossier","width=920,scrollbars=yes,resizable=yes,height=850,left="+Left+",top="+Top);
	with(document.lanceur)
	{
		target="dossier";
		modeappel.value="affiche_xsl_doc";
		ficxslt.value=fic_xslt;
		pkcateg.value=key;
		nomuti.value=js_nomuti;
		prenomuti.value=js_prenomuti;
		pkuti.value=js_pkuti;
		login.value=js_login;
		pkgroupe.value=js_pkgroupe;			
		action='sscateg.php';
		submit();
	}
	setTimeout("reinit_lanceur()",1000);
}

function reinit_lanceur()
{
	with(document.lanceur)
	{
		target='';
		modeappel.value='';
		ficxslt.value='';
		pkcateg.value='';		
		action='';
	}
}

// ALS 29/04/2009 : Retourne la liste des elements de classe searchClass qui soient fils de l'element node (si null ou omis, le document) et de type tag (si null ou omis, tous les types)
function getElementsByClass(searchClass,node,tag)
{
	var classElements = new Array();
	if ( node == null ){node = document;}
	if ( tag == null ){tag = '*';}
	var els = node.getElementsByTagName(tag);
	var elsLen = els.length;
	var pattern = new RegExp("(^|\\s)"+searchClass+"(\\s|$)");
	for (i = 0, j = 0; i < elsLen; i++)
	{
		if ( pattern.test(els[i].className) )
		{
			classElements[j] = els[i];
			j++;
		}
	}
	return classElements;
}

	function getPosition(element)
	{
	    var left = 0;
	    var top = 0;
	    /*On récupère l'élément*/
	    var e = document.getElementById(element);
	    /*Tant que l'on a un élément parent*/
	    while (e.offsetParent != undefined && e.offsetParent != null)
	    {
	        /*On ajoute la position de l'élément parent*/
	        left += e.offsetLeft + (e.clientLeft != null ? e.clientLeft : 0);
	        top += e.offsetTop + (e.clientTop != null ? e.clientTop : 0);
	        e = e.offsetParent;
	    }
	    return new Array(left,top);
	}	
	
// ALS 30/04/2009 :	affiche la page numero num_pg du formulaire de pk pk_form. form_ajax est le formulaire de la page a valider par ajax qui doit contenir un champ de name numpage. apercu definit si l'affichage du formulaire est en mode apercu (1) ou en mode saisie (0)
function affiche_page(form_ajax, num_pg, pk_form, apercu, mode_appel, pk_key)
{
	with(form_ajax)
	{
		numpage.value = num_pg;
		if(apercu == 1)
		{
			// en mode apercu
			pkkey.value = pk_form;
			modeappel.value = 'apercu';
			ajax(form_ajax, 'admin_formulaires.php', 'apercu', document.getElementById('affiche_form'), null, AJAX_VOID_MESSAGE);
		}
		else
		{
			// en mode saisie du formulaire
			modeappel.value = mode_appel;
			change_page.value = 1;
			pkkey.value=pk_key;
			pk_menu_fils.value=js_pkmenufils;
			pk_menu.value=js_pkmenu;
			login.value=js_login;
			submit();
		}
	}
}

// ALS 04/06/09 : change la page d'affichage d'un document. pk_doc est le pk du document, num_page est le numero de la page a afficher
function changement_page(pk_doc, num_page)
{
	for(var i = 1; document.getElementById('pg' + i + '-doc' + pk_doc) != null; i++)
	{
		document.getElementById('pg' + i + '-doc' + pk_doc).style.display = "none";
	}
	document.getElementById('pg' + num_page + '-doc' + pk_doc).style.display = "block";
}