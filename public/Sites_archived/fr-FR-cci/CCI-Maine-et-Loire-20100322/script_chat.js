//*****************************************************************************
// GLT Code javascript spécifique au chat
//*****************************************************************************

//*****************************************************************************
// Raccourcis clavier
//*****************************************************************************
var myms_tem_touch=false; // temoin d'utilisation des touches clavier
var myms_alt=false; // temoin d'utilisation de la touche Alt

function chatEvt(pEvent) {
myms_tem_touch = true;
var pEvent = pEvent;

	if((pEvent.keyCode == 13)&&(myms_a_fait==true)) {
		myms_tem_touch=false;
		chatValMes();
	}
}


//*****************************************************************************
// Couleurs, icônes, mise en forme
//*****************************************************************************
function chatSmiley(text) {
	text = ' ' + text + ' ';
	chatTraite(text);
}
//traitement du texte et des couleurs
var myms_tag=""; // tag peut prendre les valeur "#hexa" ou  "b" "i" "s" etc .... ou "zzz" quand chekform ferme les balises ouvertes
var myms_cat=""; // cat peut prendre la valeur "txt" ou "col"
var myms_tem=""; // tem permet de savoir si et quel bbcode est deja  ouvert et dans quel ordre "c" => color, "t" => txt
var myms_memtxt=""; // memorise le dernier tag txt
var myms_memcol=""; // memorise le dernier tag color
var myms_etat_txt= false; // une balise txt est elle ouverte? false => non, true => oui
var myms_etat_col= false; // une balise color est elle ouverte? false => non, true => oui
var myms_fbal=""; // fermeture des bbcode en dehors du processus normal en cas de depacement de max caracteres

function chatBBCode(myms_tag,myms_cat) {
var myms_bal=""; // le bbcode qui s'affichera dans MSmessage
var myms_ftxt="**";
var myms_fcol="[]";
	if(myms_cat=="txt") {
		if(myms_memtxt!=""){document.getElementById('Chatbout' + myms_memtxt).style.border="outset #cccccc 1px";}
		if((myms_tag==myms_memtxt)||(myms_tag=="zzz")){
			myms_etat_txt=true;
		}else{
			myms_etat_txt=false;
		}
		if(myms_etat_txt==false){
			myms_memtxt= myms_tag;
			var myms_dtag= "*" + myms_tag + "*";
			switch(myms_tem){
				case "c" : myms_bal= myms_dtag;
						   myms_tem= "ct";
				break;
				case "ct": myms_bal= myms_ftxt + myms_dtag;
				break;
				case "t" : myms_bal= myms_ftxt + myms_dtag;
				break;
				case ""  : myms_bal= myms_dtag;
						   myms_tem= "t";
				break;
			}
			document.getElementById('Chatbout' + myms_tag).style.border= "inset #cccccc 1px";
			myms_etat_txt=true;
		} else{
			myms_bal= myms_ftxt;
			myms_fbal= myms_alertmax==true? myms_ftxt + myms_fbal : myms_fbal;
			myms_tem= myms_tem=="ct"? "c" : myms_tem ;
			myms_tem= myms_tem=="t"? "" : myms_tem ;
			myms_etat_txt= false;
			myms_memtxt="";
		}
	} else {
		//if(myms_memcol!=""){document.getElementById('pal').style.background= "transparent";}
		if((myms_tag==myms_memcol)||(myms_tag=="zzz")){
			myms_etat_col=true;
		}else{
			myms_etat_col=false;
		}
		if(myms_etat_col==false){
		  myms_memcol= myms_tag;
		  var myms_dtag="[" + myms_tag + "]";
			switch(myms_tem){
				case "c" : myms_bal= myms_fcol + myms_dtag;
				break;
				case "ct": myms_bal= myms_ftxt + myms_fcol + myms_dtag + "*" + myms_memtxt + "*";
					   myms_tem= "ct";
				break;
				case "t" : myms_bal= myms_ftxt + myms_dtag + "*" + myms_memtxt + "*";
					   myms_tem= "ct";
				break;
				case ""  : myms_bal= myms_dtag;
					   myms_tem= "c";
				break;
			}
		  myms_etat_col=true;
		}else{
		  myms_bal= myms_fcol; // pas de depacement nb char on ferme normalement dans message
		  myms_fbal= myms_alertmax==true? myms_fcol + myms_fbal : myms_fbal;
		  myms_tem=myms_tem=="ct"? "t" : myms_tem ;
		  myms_tem=myms_tem=="c"? "" : myms_tem ;
		  myms_etat_col=false;
		}
	}
	chatTraite(myms_bal);
}

function chatTraite(val) {
	var txtarea = document.lancechat.message;
	if (txtarea.createTextRange && txtarea.caretPos) {
		var caretPos = txtarea.caretPos;
		caretPos.val = caretPos.val.charAt(caretPos.val.length - 1) == ' ' ? val + ' ' : val;
		txtarea.focus();
	} else {
		txtarea.value  += val;
		txtarea.focus();
	}
}

//*****************************************************************************
// Prévisualisation du message
//*****************************************************************************
function chatTimer() {
	setTimeout("chatTimer()",1000);
	chatCompteur(document.lancechat.message);
	chatVisu(); // Aperçu du message
}

var myms_a_fait = false; // pret à  detecter l'ecriture du message
var myms_alertmax = false; // pret pour alert max caracteres

function chatCompteur(mess) {
var myms_min = 0;
var myms_txt = mess.value;
var myms_nb = myms_txt.length;
	// Vérification du nombre de caractères
	if ((myms_nb >= 200) && (myms_alertmax == false)) {
		// Affichage d'une alerte seulement la première fois
		myms_alertmax = true;
		alert("Attention! Le message ne doit pas etre superieur a " + 200 + " caracteres...");
	}
	if (myms_nb >= 200) {
		mess.value = myms_txt.substring(myms_min, 200);
		myms_nb = 200;
	}
	if ((myms_nb>myms_min)&&(myms_nb<=200)&&(myms_a_fait==false)&&(myms_tem_touch==true)) {
		myms_a_fait=true; // Pour envoyer une seule fois la requete
	}
	document.lancechat.nbcar.value = 200 - myms_nb;
}

var fin_txt=""; // variable qui faire d'avance la balise bbcode (visualisation)
var fin_col=""; // pareil mais pour les couleurs
var myms_texte="";

function chatVisu() {
	fin_txt=myms_etat_txt==true?" **":"";
	fin_col=myms_etat_col==true?" []":"";
	
	var myms_t=document.lancechat.message.value + fin_col + fin_txt;
	myms_t = chatCodeHtml (myms_t);
	if (document.getElementById) document.getElementById("ap_message").innerHTML=myms_t ;
}

function chatCodeHtml(myms_t) {
	// balise Gras
	myms_t = chatSupCar(/(\*\*)/g,myms_t);
	myms_t = chatRemplace(/\*(b|u|i|s|big|small|sup)\*(.+)\*\*/g,'<$1>$2</$1>',myms_t);
	myms_t = chatAddCar(myms_t);
	// balise Color hexa
	myms_t = chatSupCar(/(\[\])/g,myms_t);
	myms_t = chatRemplace(/\[(#[a-fA-F0-9]{6})\](.+)\[\]/g,'<font color="$1">$2</font>',myms_t);
	myms_t = chatAddCar(myms_t);
	return myms_t;
}
function chatSupCar(reg,myms_t) {
	myms_texte = new String(myms_t);
	return myms_texte.replace(reg,'$1\n');
}
function chatAddCar(myms_t) {
	myms_texte=new String(myms_t);
	return myms_texte.replace(/\n/g,'');
}
function chatRemplace(reg,rep,myms_t) {
	myms_texte=new String(myms_t) ;
	return myms_texte.replace(reg,rep);
}

//*****************************************************************************
// Code pour la prévisualisation du message
//*****************************************************************************
function chatRefresh(){
	setInterval('chatActualise()', vitesse_refresh);
}

function chatActualise() {
	// Si il y a plusieurs connectés, rechargement des messages
	if(nb_connect>0) {
		chatLanceur("./lance_chat.php?pk_projet="+js_pkprojet+"&modeappel=reload_chat&tpl_ajax=tous_messages", "zone_mess");
	}
	// Actualisation du compteur en permanance
	chatLanceur("./lance_chat.php?pk_projet="+js_pkprojet+"&modeappel=compteur&tpl_ajax=compteur", "count");
}


//*****************************************************************************
// Code pour le rechargement automatique des messages
//*****************************************************************************

// retourne un objet xmlHttpRequest.
// méthode compatible entre tous les navigateurs (IE/Firefox/Opera)
function getXMLHTTP(){
  var xhr=null;
  if(window.XMLHttpRequest) // Firefox et autres
  xhr = new XMLHttpRequest();
  else if(window.ActiveXObject){ // Internet Explorer
    try {
      xhr = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
      try {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (e1) {
        xhr = null;
      }
    }
  }
  else { // XMLHttpRequest non supporté par le navigateur
    alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
  }
  return xhr;
}

var url='';
var cadre='';

function chatLanceur(url, cadre) {
  var cadreRetour = document.getElementById(cadre);
  var xmlHttp = null; //l'objet xmlHttpRequest utilisé pour contacter le serveur
  xmlHttp = getXMLHTTP();
  
  if(xmlHttp){
    //appel à l'url distante
    xmlHttp.open("GET", url, true);
    xmlHttp.onreadystatechange = function() {
      //if(xmlHttp.readyState < 4) cadreRetour.style.cursor="wait"; // Pour montrer le rechargement ...
	  // Retour requête serveur
	  if(xmlHttp.readyState==4) {
		//cadreRetour.style.cursor="default";
		cadreRetour.innerHTML = xmlHttp.responseText;
      }
    };
    // envoi de la requête
    xmlHttp.send(null)
  }
}


//*****************************************************************************
// Test du message et envoi de celui-ci pour l'enregistrement
//*****************************************************************************
function chatValMes() {
	if (document.lancechat.message.value.length < 1) {
		alert("Vous devez saisir un message avant de valider...");
		return false;
	}
	
	if(myms_etat_col==true) chatBBCode("zzz","col");
	if(myms_etat_txt==true) chatBBCode("zzz","txt");
	// Enregistrement du message
	chatLanceur("./lance_chat.php?pk_projet="+js_pkprojet+"&message="+document.lancechat.message.value+"&modeappel=sauver_message&tpl_ajax=tous_messages", "zone_mess");

	// RAZ
	myms_a_fait = false;
	myms_fbal = "";
	myms_alertmax = false;
	document.lancechat.nbcar.value='';
	document.lancechat.message.value='';
	document.lancechat.message.focus();
}