//*********************************************************************
// 07-07-06  GLT  Javascript menu_expander
//*********************************************************************

// Fonction d'affichage d'une div
function affiche_id(id, etat){
	if(etat=='oui') {
		this.document.getElementById(id).style.display=''
	}
	else{
		this.document.getElementById(id).style.display='none'
	}
}

// Fonction d'affichage d'un paramètre du dashboard
function affiche_param_dshbrd(id){
	if(this.document.getElementById(id).style.display=='none') {
		this.document.getElementById(id).style.display=''
	}
	else{
		this.document.getElementById(id).style.display='none'
	}
}


// Fonction pour afficher ou non le menu de gauche
function affMenuGauche(id) {

	if(this.document.getElementById(id).style.display=='none'){
		this.document.getElementById(id).style.display='inline';

		setCookie('affMenuGauche','true',30,'/','','');
		var show = getCookie('affMenuGauche');
		document['affMenu'].src = 'ActivePortail/Commun/images/menu_hide.gif';
		document['affMenu'].title = 'Cacher le menu';
	} else {
		this.document.getElementById(id).style.display='none';

		setCookie('affMenuGauche','false',30,'/','','');
		var show = getCookie('affMenuGauche');
		document['affMenu'].src = 'ActivePortail/Commun/images/menu_show.gif';
		document['affMenu'].title = 'Afficher le menu';
	}
}

// Fonction poser cookie et valeurs
function setCookie( name, value, expires, path, domain, secure ) 
{
	// setTime en millisecondes
	var today = new Date();
	today.setTime( today.getTime() );

	/*
	if the expires variable is set, make the correct 
	expires time, the current script below will set 
	it for x number of days, to make it for hours, 
	delete * 24, for minutes, delete * 60 * 24
	*/
	if ( expires )
	{
		expires = expires * 1000 * 60 * 60 * 24;
	}
	var expires_date = new Date( today.getTime() + (expires) );

	document.cookie = name + "=" +escape( value ) +
						( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) + 
						( ( path ) ? ";path=" + path : "" ) + 
						( ( domain ) ? ";domain=" + domain : "" ) +
						( ( secure ) ? ";secure" : "" );
}

// Fonction obtenir cookie et valeurs
function getCookie(name) {
	var start = document.cookie.indexOf(name + '=');
	var len = start + name.length + 1;
	
	if ((!start) && (name != document.cookie.substring(0,name.length))) return null;
	if (start == -1) return null;
	
	var end = document.cookie.indexOf(';',len);
	if (end == -1) end = document.cookie.length;
	
	return unescape(document.cookie.substring(len,end));
}
