var mootabs = new Class({
 
	Implements: [Options],
 
	options: {
		width: '300px',
		height: '200px',
		changeTransition: Fx.Transitions.Bounce.easeOut,
		duration: 1000,
		mouseOverClass: 'over',
		activateOnLoad: 'first',
		useAjax: false,
		ajaxUrl: '',
		ajaxOptions: {
			method:'get',
			evalScripts: true
		},
		ajaxLoadingText: 'Loading...'
	},
 
	initialize: function(element, options) {
		if(options) this.setOptions(options);
 
		this.el = $(element);
		this.elid = element;
 
		this.el.setStyles({
			height: this.options.height,
			width: this.options.width
		});
 
		this.titles = $$('#' + this.elid + ' ul li');
 
		this.panelHeight = this.el.getSize().y - (this.titles[0].getSize().y + 4);
		this.panel = new Element('div', {
			'class': 'mootabs_panel',
			'styles': {
				'height': this.panelHeight + 'px'
			}
		}).inject(this.el.getFirst(), 'after');
 
		this.attach(this.titles);
 
		if(this.options.activateOnLoad != 'none') {
			if(this.options.activateOnLoad == 'first') {
				this.activate(this.titles[0]);
			} else {
				this.activate(this.options.activateOnLoad);
			}
		}
	},
 
	attach: function(elements) {
		$$(elements).each(function(element) {
			var enter = element.retrieve('tab:enter', this.elementEnter.bindWithEvent(this, element));
			var leave = element.retrieve('tab:leave', this.elementLeave.bindWithEvent(this, element));
			var mouseclick = element.retrieve('tab:click', this.elementClick.bindWithEvent(this, element));
			element.addEvents({mouseenter: enter, mouseleave: leave, click: mouseclick});
			var el = $(element.get('title'));
			element.store('panel:html', el.get('html'));
			element.store('panel:id', el.id);
			var elementDispose = $(element.get('title')).dispose();
		}, this);
		return this;
	},
 
	detach: function(elements) {
		$$(elements).each(function(element){
			element.removeEvent('mouseenter', element.retrieve('tab:enter') || $empty);
			element.removeEvent('mouseleave', element.retrieve('tab:leave') || $empty);
			element.removeEvent('mouseclick', element.retrieve('tab:click') || $empty);
			element.eliminate('tab:enter').eliminate('tab:leave').eliminate('tab:click').eliminate('panel:html').eliminate('panel:id');
			var elementDispose = element.dispose();
		});
		return this;
	},
 
	activate: function(tab) {
		if($type(tab) == 'string') {
			myTab = $$('#' + this.elid + ' ul li').filter('[title=' + tab + ']')[0];
			tab = myTab;
		}
 
		if($type(tab) == 'element') {
			this.panel.empty();
			var html = tab.retrieve('panel:html');
			this.panel.id = tab.retrieve('panel:id');
			if (html) this.fill(this.panel, html);
 
			this.titles.removeClass('active');
 
			tab.addClass('active');
 
			if(this.options.changeTransition != 'none') this.changeEffect();
 
			this.activeTitle = tab;
 
			if(this.options.useAjax) {
				this.getContent();
			}
		}
	},
 
	elementEnter: function(event, element) {
		if(element != this.activeTitle) {
			element.addClass(this.options.mouseOverClass);
		}
	},
 
	elementLeave: function(event, element) {
		if(element != this.activeTitle) {
			element.removeClass(this.options.mouseOverClass);
		}
	},
 
	elementClick: function(event, element) {
		if(element != this.activeTitle) {
			element.removeClass(this.options.mouseOverClass);
			this.activate(element);
		}
	},
 
	fill: function(element, contents) {
		($type(contents) == 'string') ? element.set('html', contents) : element.adopt(contents);
	},
 
	getContent: function() {
		this.panel.set('html',this.options.ajaxLoadingText);
		var newOptions = {
			url: this.options.ajaxUrl + '?tab=' + this.activeTitle.getProperty('title'),
			update: this.panel
		};
		this.options.ajaxOptions = $merge(this.options.ajaxOptions, newOptions);
		var tabRequest = new Request.HTML(this.options.ajaxOptions);
		tabRequest.send();
	},
 
	changeEffect: function() {
		this.panel.setStyle('height', 0);
		var changeEffect = new Fx.Elements(this.panel, {duration: this.options.duration, transition: this.options.changeTransition});
		changeEffect.start({
			'0': {
				'height': [0, this.panelHeight]
			}
		});
	},
 
	addTab: function(title, label, content) {
		var newTitle = new Element('li', {
			'title': title
		});
		newTitle.appendText(label);
		this.titles.include(newTitle);
		$$('#' + this.elid + ' ul').adopt(newTitle);
		var newPanel = new Element('div', {
			'id': title,
			'class': 'mootabs_panel'
		});
		if(!this.options.useAjax) {
			newPanel.set('html',content);
		}
		this.el.adopt(newPanel);
		this.attach(newTitle);
	},
 
	removeTab: function(title){
		if(this.activeTitle.title == title) {
			this.activate(this.titles[0]);
		}
		var tab = $$('#' + this.elid + ' ul li').filter('[title=' + title + ']')[0];
		this.detach(tab);
	},
 
	next: function() {
		var nextTab = this.activeTitle.getNext();
		if(!nextTab) {
			nextTab = this.titles[0];
		}
		this.activate(nextTab);
	},
 
	previous: function() {
		var previousTab = this.activeTitle.getPrevious();
		if(!previousTab) {
			previousTab = this.titles[this.titles.length - 1];
		}
		this.activate(previousTab);
	}
});