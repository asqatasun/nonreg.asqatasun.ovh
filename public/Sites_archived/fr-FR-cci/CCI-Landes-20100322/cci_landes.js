function div_cache_entreprise(nomdiv)
{

 var style = document.getElementById(nomdiv).style.visibility;
 var display  = "block";
 
 if (style == "hidden")
 {
  style ="visible";
 }
 else
 {
  style ="hidden";
 }
 document.getElementById(nomdiv).style.visibility= style;
}
function div_cache(nomdiv)
{

 var style = document.getElementById(nomdiv).style.visibility;
 var display  = "block";
 
 if (style == "hidden")
 {
  style ="visible";
  display = "block";
 }
 else
 {
  style ="hidden";
  display = "none";
 }
 //alert("div_cache"+style+" "+display);
 
 document.getElementById(nomdiv).style.visibility= style;
 document.getElementById(nomdiv).style.display = display; 
 if ((nomdiv == "identification")&&(style == "hidden"))
 {
    document.getElementById("num_adherent_content").style.visibility= "hidden";
    document.getElementById("num_adherent_content").style.display = "none";
 }
 else
 {
  //change_type_objet();
 }
}
function register()
{
	if (document.Register.elements['ezcoa-340_type_user_0'].checked)
	{
		
		document.Register.elements['ezcoa-340_type_user_0'].checked =true;
		document.Register.elements['ezcoa-358_entreprise'].value ="";
		div_cache_entreprise("entreprise");
	}
	else
	{
		document.Register.elements['ezcoa-340_type_user_1'].checked =true;
		document.Register.elements['ezcoa-358_entreprise'].value ="Particulier";
		div_cache_entreprise("entreprise");
	}
}
function communes(radio)
{
	if (radio[0].checked)
	{
		div_cache("commune_dept");
		div_cache("commune_autre");
		document.Register.elements['ezcoa-404_commune_dept'].value="";
	}
	else
	{
		div_cache("commune_dept");
		div_cache("commune_autre");
		document.Register.elements['ezcoa-405_commune_autre'].value="";
	}
	


}
function type_users(radio)
{
	//alert("info ");
	if (radio[0].checked)
	{
		div_cache("entreprise");
		div_cache("particulier");
	}
	else
	{
		if(confirm('Etes vous sûr de vouloir changer (vous risquez de perdre les informations) ?'))
		{
			document.editform.elements["ezcoa-358_entreprise"].value="";
			document.editform.elements["ezcoa-359_siege_etablissement"].value="";
			document.editform.elements["ezcoa-403_domaine_activites_entreprise"].value="";
			document.editform.elements["ezcoa-364_annee_creation"].value="";
			document.editform.elements["ezcoa-365_siret"].value="";
			document.editform.elements["ezcoa-366_code_ape"].value="";
			document.editform.elements["ezcoa-367_site_internet"].value="";
			document.editform.elements["ContentObjectAttribute_data_enumid_2170[]"].value="";
			div_cache("entreprise");
			div_cache("particulier");
		}
		else
		{
			document.editform.type_user[0].checked=true;
		}
	}
}
function type_annuaire(radio)
{
	
	if (radio[0].checked)
	{
	
		document.editsearch.elements["type_service_rech"].value="entreprise";
	}
	else
	{	
		document.editsearch.elements["type_service_rech"].value="personne";
	}
//		alert("TEST "+document.editsearch.elements["type_service_rech"].value);
	document.editsearch.submit();
}
function type_bourse(radio)
{
	if (radio[0].checked)
	{
		document.editsearch.elements["type_rech"].value="offre";
	}
	else
	{	
		document.editsearch.elements["type_rech"].value="demande";
	}
	
	document.editsearch.submit();
}

function vide_select_zone(name,form)
  {
    var select_name = document.forms[form].elements[name];
    var nbr_elmt = 0;
    var nbr_elmt = select_name.options.length; 

    /* On vide le tableau des utilisateurs */
    var i = 0;
    while (i < nbr_elmt)
    {
        select_name.options[0]=null;
        i++;
    }  
 
  }
 function change_dom(form,sous_dom)
{
    /* On vide les tableau des sous_domaines */
    vide_select_zone("sous_domaine_act",form);
    var sous_domaine_act = document.forms[form].elements['sous_domaine_act'];
    
    var domaine_act = document.forms[form].elements['domaine_act'].options[document.forms[form].elements['domaine_act'].selectedIndex].value;
    var nbr_sous_dom = tab_list_sous_dom.length;
    var $id_sous_domaine_act = document.forms[form].elements["id_sous_domaine_act"].value;
   
    if (nbr_sous_dom!=null )
    {
    	var newEntry = new Option('Indifférent','',false,false);
        sous_domaine_act.options[0] = newEntry;
    	
      var i = 0;
      var nbr_elmt = 1;
       var old_dom="";
     while (i < nbr_sous_dom)
      {
      	
        if ((tab_list_sous_dom[i][2]== domaine_act)&&(old_dom!=tab_list_sous_dom[i][3]))
        {  
          var check=false;
          if ($id_sous_domaine_act == tab_list_sous_dom[i][1]) 
           {check =true;}
           var newEntry = new Option(tab_list_sous_dom[i][3], tab_list_sous_dom[i][1],false,check);
           sous_domaine_act.options[nbr_elmt] = newEntry;
           nbr_elmt++;
           old_dom=tab_list_sous_dom[i][3];
        }
        
        i++;
      }
      if (nbr_elmt==0)
      {
      	   var newEntry = new Option('Indifférent','',false,false);
           sous_domaine_act.options[0] = newEntry;
      }
      if (sous_dom=="oui")
      {
      	change_sous_dom(form);
      }
    }

}
function change_sous_dom(form)
{
    /* On vide les tableau des sous_domaines */
    vide_select_zone("sous_sous_domaine_act",form);
    var sous_sous_domaine_act = document.forms[form].elements['sous_sous_domaine_act'];
    var sous_domaine_act = document.forms[form].elements['sous_domaine_act'].options[document.forms[form].elements['sous_domaine_act'].selectedIndex].text;
    var id_sous_domaine_act = document.forms[form].elements['sous_domaine_act'].options[document.forms[form].elements['sous_domaine_act'].selectedIndex].value;

    var nbr_sous_sous_dom = tab_list_sous_sous_dom.length;
    var id_sous_sous_domaine_act = document.forms[form].elements["id_sous_sous_domaine_act"].value;
   
    if (nbr_sous_sous_dom!=null )
    {
      var i = 0;
      var nbr_elmt = 1;
      var newEntry = new Option('Indifférent','',false,false);
      sous_sous_domaine_act.options[0] = newEntry;
      while (i < nbr_sous_sous_dom)
      {
      	//alert("TEST :"+tab_list_sous_sous_dom[i][2]+" "+tab_list_sous_sous_dom[i][3]+" "+id_sous_domaine_act+" "+nbr_elmt);
        if ((tab_list_sous_sous_dom[i][2]== id_sous_domaine_act) )
        {  
        	var check=false;
          	if (id_sous_sous_domaine_act == tab_list_sous_sous_dom[i][1])
           		{check =true;}
           	var newEntry = new Option(tab_list_sous_sous_dom[i][3], tab_list_sous_sous_dom[i][1],false,check);
           	sous_sous_domaine_act.options[nbr_elmt] = newEntry;
           	nbr_elmt++;
        }
        i++;
      }
    }
    document.forms[form].elements['libelle_sous_domaine_act'].value = document.forms[form].elements['sous_domaine_act'].options[document.forms[form].elements['sous_domaine_act'].selectedIndex].text;
    
}

function checked_table(choixcases,elements,value) 
{
    var i = 0;
    var choix = document.forms[0].elements[choixcases];
    var elts = document.forms[0].elements[elements];
    var result=elts.value;
    //alert("TEST "+choixcases+" "+choix.checked+" "+elts.value);
    if (choix.checked == true)
        {
          result += value+";";
          //alert("TEST checked ");
        }
    else
        {
           var pos=0;
           var tag = value+";";
           pos = result.indexOf(tag);
           //alert("TEST dechecked ");
           result = result.substring(0,pos)+result.substring(pos+tag.length,result.length);
        }
    //result = result.slice(0,result.length-1); //delete the last  
    //alert("TEST final "+result);
    elts.value = result;
}
