/******************************************************************************
* albRuntimeJs.js
*******************************************************************************

*******************************************************************************
*                                                                             *
* Copyright 2006									                          *
*                                                                             *
******************************************************************************/

//	-------------------------------------------------------------------------
//	Recovery of effective width and height maintaining ratio
//	-------------------------------------------------------------------------
function albGetEffectiveWidth(width, height, expectedWidth, expectedHeight)
{
	width = parseInt(width);
	height = parseInt(height);
	expectedWidth = parseInt(expectedWidth);
	expectedHeight = parseInt(expectedHeight);
	if(isNaN(expectedWidth) || expectedWidth == 0) {
		if(isNaN(expectedHeight) || expectedHeight == 0) return width;
		if(expectedHeight > height) return width;
		return Math.floor(width * expectedHeight / height);
	} else if(isNaN(expectedHeight) || expectedHeight == 0) {
		if(width < expectedWidth) return width;
		return expectedWidth;
	}
	if(width * expectedHeight > height * expectedWidth) {
		if(width < expectedWidth) return width;
		return expectedWidth;
	}
	if(height < expectedHeight) return width;
	return Math.floor(width * expectedHeight / height);
}

function albGetEffectiveHeight(width, height, expectedWidth, expectedHeight)
{
	width = parseInt(width);
	height = parseInt(height);
	expectedWidth = parseInt(expectedWidth);
	expectedHeight = parseInt(expectedHeight);
	if(isNaN(expectedHeight)|| expectedHeight == 0) {
		if(isNaN(expectedWidth) || expectedWidth == 0) return height;
		if(expectedWidth > width) return height;
		return Math.floor(height * expectedWidth / width);
	} else if(isNaN(expectedWidth) || expectedWidth == 0) {
		if(height < expectedHeight) return height;
		return expectedHeight;
	}
	if(height * expectedWidth > width * expectedHeight) {
		if(height < expectedHeight) return height;
		return expectedHeight;
	}
	if(width < expectedWidth) return height;
	return Math.floor(height * expectedWidth / width);
}