/******************************************************************************
* dynlibJsThesaurus.js
*******************************************************************************
Acces au thesaurus de l'application en Javascript
*******************************************************************************
*                                                                             *
* Copyright 2000-2002								                          *
*                                                                             *
******************************************************************************/
function isoThesaurus(thesaurus)
{
	this.lang = "fr" ; // default language use
	this.thesaurus = thesaurus;
}

isoThesaurus.prototype.setLanguage = function ( lang )
{
	// on pourrait ici tester les langues valides
	this.lang = lang ;
}

//--------------------------------------------------------------
// args est optionnel :
//	- n'est pas present si aucune inclusion dans le message
//	- est un string si le message comporte une inclusion
//	- est un array si le message comporte plusieurs inclusions
//--------------------------------------------------------------
isoThesaurus.prototype.translate = function ( item, args )
{
	var strMsg = null;
	var langArray = this.thesaurus[this.lang];
	if (langArray != null)
		strMsg = langArray[item];
	if (strMsg == null)
		strMsg = item+" (lang="+this.lang+")";
	else
	{
		if (args != null) {
			if ( typeof(args) == "string" ) {
				strMsg = strMsg.replace(/%1/g, args ) ;
			}
			else {
				var argSearch ;
				var indice ;
				for ( var i = 0; i < args.length; i++ ) {
					indice = i + 1 ;
					var re = new RegExp("%" + indice,"g");
					strMsg = strMsg.replace(re, args[ i ] ) ;
				}
			}
		}
	}
	return strMsg ;
}

/////////////////////////////////////////////////////////////////
//
var objThesaurus = new isoThesaurus(thesaurus) ;
objThesaurus.setLanguage( lang ) ;
