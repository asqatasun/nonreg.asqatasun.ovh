/* Gestionnaire d'�v�nements */
function addEvent( obj, type, fn )
{
	if (obj.addEventListener) {
		obj.addEventListener( type, fn, false );
	} else if (obj.attachEvent) {
		obj["e"+type+fn] = fn;
		obj[type+fn] = function() { obj["e"+type+fn]( window.event ); }
		obj.attachEvent( "on"+type, obj[type+fn] );
	}
}

function removeEvent( obj, type, fn )
{
	if (obj.removeEventListener) {
		obj.removeEventListener( type, fn, false );
	} else if (obj.detachEvent) {
		obj.detachEvent( "on"+type, obj[type+fn] );
		obj[type+fn] = null;
		obj["e"+type+fn] = null;
	}
}

/* Fonction lanc�e au chargement complet de la page */
function launch_page(obj) {
  var anchors = document.getElementById(obj).getElementsByTagName('a');

/*  var txt = '<select id="new'+obj+'"><option>'+document.getElementsByTagName('h6')[0].innerHTML+'</option>';  */
var txt = '<select id="new'+obj+'" class=listederoulante><option>'+document.getElementsByTagName('h6')[0].innerHTML+'</option>';


  for(var i=0; i < anchors.length; i++) {
    txt += '<option value="'+anchors[i]+'">'+anchors[i].innerHTML+'</option>';
  }

  txt += '</select>';

  document.getElementById(obj).innerHTML = txt;

  addEvent(document.getElementById('new'+obj),'change', function() { window.location = document.getElementById('new'+obj).value; } );
}

/* Appel de la fonction principale */
/*    mis dans fichiers specifiques (listeAlaUne.js ,listeAgenda.js ,listeZoom.js)
	addEvent(window,'load',function() { launch_page('listeAlaUne'); });
*/
