//================================================================================
// Librairie de fonctions communes
//================================================================================
var valDefault;
var valType;
var valRequis;
var valDefaut;

//--------------------------------------------------------------------------------
// Fonction D'extraction des attributs propri�taires des input
function funcExtract(varElement,varTyp)
	{
	if (varElement.alt != '')
		{
		if (varElement.alt)
			switch (varTyp)
				{
				case 'nom' :
						return varElement.alt.substr(4 , varElement.alt.indexOf('|')-4);
				case 'type' :
					return varElement.alt.substr(varElement.alt.indexOf('|Type:',0)+6 , varElement.alt.indexOf('|Requis:',0)-(varElement.alt.indexOf('|Type:',0)+6));
				case 'requis' :
					return varElement.alt.substr(varElement.alt.indexOf('|Requis:')+8 , 3);
				case 'defaut' :
					return varElement.alt.substr(varElement.alt.indexOf('|Defaut:')+8 , varElement.alt.length-(varElement.alt.indexOf('|Defaut:')+8));
				}
		else
			switch (varTyp)
				{
				case 'nom' :
						return varElement.id.substr(4 , varElement.id.indexOf('|')-4);
				case 'type' :
					return varElement.id.substr(varElement.id.indexOf('|Type:',0)+6 , varElement.id.indexOf('|Requis:',0)-(varElement.id.indexOf('|Type:',0)+6));
				case 'requis' :
					return varElement.id.substr(varElement.id.indexOf('|Requis:')+8 , 3);
				case 'defaut' :
					return varElement.id.substr(varElement.id.indexOf('|Defaut:')+8 , varElement.id.length-(varElement.id.indexOf('|Defaut:')+8));
				}

		}
	else
		return '';
	}

//--------------------------------------------------------------------------------
// Fonction d'initialisation d'un formulaire
varInit = false;
function funcFormInit(varForm)
	{
	if (varInit == false)
		{
		//--- Affectation des valeurs par d�faut
		for (var i = 0; i < varForm.length; i++)
			{
			valDefault = funcExtract(varForm.elements[i],'defaut');
			if ((varForm.elements[i].value == "") && (valDefault != 'undefined'))
				{
				varForm.elements[i].value = valDefault;
				}
			valType = funcExtract(varForm.elements[i],'type');	
 			  if ((valType == "Dat") && (varForm.elements[i].value  == "")) 
				{
				strDate = new Date();
				strDay = strDate.getDate().toString();
				if (strDay.length == 1) {strDay = "0"+ strDay;}
				strMonth = (strDate.getMonth()+1).toString();
				if (strMonth.length == 1) {strMonth = "0"+ strMonth;}
				strYear = strDate.getYear();
				varForm.elements[i].value = strDay +"/"+ strMonth +"/"+ strYear;
				}
			}
		}
	varInit = true;
	}

//--------------------------------------------------------------------------------
// Fonction de contr�le de saisie d'un formulaire
var varPwdCtrl = "";

function funcCtrlSaisie(varForm)
	{
	var StatutValid = 'OK';
	var StatutSelect = 'NON';
	var strMessage = '';
	var strCheckbox = '';
	//--- R�initlisation de l'aspect des champs (s'ils ont �t� pr�c�demment en erreur de saisie)
	for (var i = 0; i < varForm.length; i++)
		{
		funcInitField(varForm.elements[i]);
		}

/*	for (i = 0; i < varForm.length; i++)
		{
		if (varForm.elements[i].name)
			alert(varForm.elements[i].name + ' - ' + varForm.elements[i].type);
		}*/
for (var i = 0; i < varForm.length; i++)
		{
			
			
		//--- Contr�le des champs obligatoires
		if (varForm.elements[i].name)
			{
			if (varForm.elements[i].name.toLowerCase() == 'rmcontactuid')
				{
				if (varForm.elements[i].value != '')
					{
					if (!funcCtrlLogin(varForm.RmContactId.value, varForm.elements[i].value, varForm.SysWebId.value))
						{
						strMessage = strMessage + "Ce login est d�j� utilis�. Choisissez-en un autre.\n";
						funcErrorField(varForm.elements[i]);
						StatutValid = ""
						}
					}
				}
				
			if (varForm.elements[i].name.toLowerCase() == 'rmcontactemail') 
				{
//alert('rmcontactemail : ' + varForm.elements[i].name + ' - ' + varForm.elements[i].type);					
				if ((varForm.elements[i].value != '') && (varForm.elements[i+1].name == varForm.elements[i].name + '_ctl'))
//					{
//					alert('ctlemail 1');						
//					if (varForm.elements[i+1].name == varForm.elements[i].name + '_ctl')
					{
					if (varForm.elements[i].value != varForm.elements[i+1].value)
						{
						strMessage = strMessage + "La 2�me adresse Email saisie pour confirmation est diff�rente.\n Veuillez recommencer.";
						varForm.elements[i].value = "";
						varForm.elements[i+1].value = "";					
						varPwdCtrl = "";
						StatutValid = "";
						break
						}
	//					alert(varForm.RmContactId.value + ' - ' + varForm.elements[i].value + ' - ' + varForm.SysWebId.value);
					if (!funcCtrlEmail(varForm.RmContactId.value, varForm.elements[i].value, varForm.SysWebId.value))
						{
						strMessage = strMessage + "\nCette adresse e-mail est d�j� utilis�e.\nVous devez certainement poss�der un compte.\nIdentifiez-vous.\n\n";
						funcErrorField(varForm.elements[i]);
						StatutValid = "NON"
						}
					}
				}

//		if (varForm.elements[i].valoblig == "Oui")

			if (funcExtract(varForm.elements[i],'requis') == 'Oui')
				{
//alert('Txt : ' + varForm.elements[i].name + ' - ' + varForm.elements[i].type);
				switch (varForm.elements[i].type)
					{
					
					case 'checkbox' :
					  if (strCheckbox != funcExtract(varForm.elements[i],'nom'))
							{
							strCheckbox = funcExtract(varForm.elements[i],'nom');
							statutSelect = "NON";
						  for (var j=i;j<varForm.elements.length;j++)
								{
								if (strCheckbox == funcExtract(varForm.elements[i],'nom'))
									{
									if (varForm.elements[j].checked)
										statutSelect = 'OK';		
									}
								}
							if (statutSelect != 'OK')
								{
								strMessage = strMessage + "Le champ \"" + funcExtract(varForm.elements[i],'nom') + "\" est obligatoire,\n vous devez le remplir \n"
								funcErrorField(varForm.elements[i-1]);
								StatutValid = "NON"
								}	
							}
						break;	
					
					case 'select-one' :
						statutSelect = "NON";
						for (var j=0;j<varForm.elements[i].options.length;j++)
							{
							if (varForm.elements[i].options[j].selected)
								statutSelect = 'OK';
							}
						if (statutSelect != 'OK')
							{
							strMessage = strMessage + "Le champ \"" + funcExtract(varForm.elements[i],'nom') + "\" est obligatoire,\n vous devez le remplir \n"
							funcErrorField(varForm.elements[i]);
							StatutValid = "NON"
							}
						break;					

					case 'text' :
						if ((varForm.elements[i].value.length < 1) || (varForm.elements[i].value == "VIDE"))
							{
							strMessage = strMessage + "Le champ \"" + funcExtract(varForm.elements[i],'nom') + "\" est obligatoire,\n vous devez le remplir \n"
							funcErrorField(varForm.elements[i]);
							StatutValid = ""
							}
						else
							if (varForm.elements[i].value.length > varForm.elements[i].maxlength)
								{
								strMessage = strMessage + "Le champ \"" + funcExtract(varForm.elements[i],'nom') + "\" est trop long (" + varForm.elements[i].value.length + " caract�res).\nIl ne doit pas d�passer " + varForm.elements[i].maxlength + " caract�res.\n"
								funcErrorField(varForm.elements[i]);
								StatutValid = ""
								}
							else
								{
								if ((funcExtract(varForm.elements[i],'type') == "Mail") || (funcExtract(varForm.elements[i],'type') == "Web"))
									var strCar = new Array('http://');
								else
									var strCar = new Array('www', 'http://');
								for (j = 0; j < strCar.length; j++)
									{
									if (varForm.elements[i].value.toLowerCase().indexOf(strCar[j]) > -1)
										{
										strMessage = strMessage + "Le champ \"" + funcExtract(varForm.elements[i],'nom') + "\" ne peut contenir pas contenir les caract�res suivants \"" + strCar[j] + "\"\n"
										funcErrorField(varForm.elements[i]);
										StatutValid = "NON";
										break
										}
									}
								}
						break
						
					case 'textarea' :
						if ((varForm.elements[i].value.length < 1) || (varForm.elements[i].value == "VIDE"))
							{
							strMessage = strMessage + "Le champ \"" + funcExtract(varForm.elements[i],'nom') + "\" est obligatoire,\n vous devez le remplir \n"
							funcErrorField(varForm.elements[i]);
							StatutValid = "NON"
							}
						else
							if (varForm.elements[i].value.length > varForm.elements[i].maxlength)
							{
							strMessage = strMessage + "Le champ \"" + funcExtract(varForm.elements[i],'nom') + "\" est trop long (" + varForm.elements[i].value.length + " caract�res).\nIl ne doit pas d�passer " + varForm.elements[i].maxlength + " caract�res.\n"
							funcErrorField(varForm.elements[i]);
							StatutValid = "NON"
							}
							if (funcExtract(varForm.elements[i],'type') == "Mail")
								var strCar = new Array('http://');
							else if (funcExtract(varForm.elements[i],'type') == "Web")
								var strCar = new Array('@', 'http://');
							else
								var strCar = new Array('@', 'www', 'http://');
							for (j = 0; j < strCar.length; j++)
								{
								if (varForm.elements[i].value.toLowerCase().indexOf(strCar[j]) > -1)
									{
									strMessage = strMessage + "Le champ \"" + funcExtract(varForm.elements[i],'nom') + "\" ne peut contenir pas contenir les caract�res suivants \"" + strCar[j] + "\"\n"
									funcErrorField(varForm.elements[i]);
									StatutValid = "NON";
									break
									}
								}
						break			

/*					case 'password' :
						if ((varForm.elements[i].value.length < 1) || (varForm.elements[i].value == "VIDE"))
							{
							strMessage = strMessage + "Le champ \"" + funcExtract(varForm.elements[i],'nom') + "\" est obligatoire,\n vous devez le remplir \n"
							funcErrorField(varForm.elements[i]);
							StatutValid = "NON"
							}
						break	*/
					}
				}

			//--- Contr�le des champs Upload Image
			if (funcExtract(varForm.elements[i],'type') == "Img" && varForm.elements[i].value.length > 0)
				{
//alert('Img : ' + varForm.elements[i].name + ' - ' + varForm.elements[i].type);					
				var strCar = ".GIF, .JPG, .JPEG, .PNG"
				var strExt = varForm.elements[i].value.substring(varForm.elements[i].value.length-4,varForm.elements[i].value.length).toUpperCase()
				if (strCar.indexOf(strExt) < 0)
					{
					strMessage = strMessage + "Le champ \"" + funcExtract(varForm.elements[i],'nom') + "\" doit �tre un fichier du type \"" + strCar + "\"\n"
					funcErrorField(varForm.elements[i]);
					StatutValid = "";
					}
				}

			//--- Contr�le des champs Num�riques
			if (funcExtract(varForm.elements[i],'type') == "Num" && varForm.elements[i].value.length > 0)
				{
//alert('Num : ' + varForm.elements[i].name + ' - ' + varForm.elements[i].type);					
				var strCar = "0123456789.,"
				for (j = 0; j < varForm.elements[i].value.length; j++)
					{
					if (strCar.indexOf(varForm.elements[i].value.charAt(j)) < 0)
						{
						strMessage = strMessage + "Le champ \"" + funcExtract(varForm.elements[i],'nom') + "\" ne peut contenir que les caract�res suivants \"" + strCar + "\"\n"
						funcErrorField(varForm.elements[i]);
						StatutValid = "";
						break
						}
					}
				}
//alert('email');
			//--- Contr�le des champs E-mail
//			if (funcExtract(varForm.elements[i],'type') == "Mail" && varForm.elements[i].value.length > 0)
			if (funcExtract(varForm.elements[i],'type') == "Mail" && varForm.elements[i].value.length > 0)			
				{
//alert('mail : ' + varForm.elements[i].name + ' - ' + varForm.elements[i].type);					
				if (varForm.elements[i].value.length < 6 || varForm.elements[i].value.indexOf("@") < 0 || varForm.elements[i].value.indexOf(".") < 0)
					{
					strMessage = strMessage + "Vous devez saisir une adresse E-mail valide\n"
					funcErrorField(varForm.elements[i]);
					StatutValid = "";
					}
				else
					{
					var strCar = "0123456789abcdefghijklmnopqrstuvwxyz_-.@"
					for (j = 0; j < varForm.elements[i].value.length; j++)
						{
						if (strCar.toLowerCase().indexOf(varForm.elements[i].value.charAt(j).toLowerCase()) < 0)
							{
							strMessage = strMessage + "Le champ \"" + funcExtract(varForm.elements[i],'nom') + "\" ne peut contenir que les caract�res suivants \"" + strCar + "\"\n"
							funcErrorField(varForm.elements[i]);
							StatutValid = "";
							break
							}
						}
					}
				}

//alert('date');
			//--- Contr�le des champs Date sur 10 ou 19 caract�res
			if (funcExtract(varForm.elements[i],'type') == "Dat" && varForm.elements[i].value.length > 0)
				{
//alert('Date : ' + varForm.elements[i].name + ' - ' + varForm.elements[i].type);					
				if (((varForm.elements[i].value.length != 10) && (varForm.elements[i].value.length != 19)) || (varForm.elements[i].value.charAt(2) != "/") || (varForm.elements[i].value.length.charAt(5) != "/"))
					{
					strMessage = strMessage + "Le champ \"" + funcExtract(varForm.elements[i],'nom') + "\" doit respecter le format JJ/MM/AAAA HH:MM:SS\n"
					funcErrorField(varForm.elements[i]);
					StatutValid = "";
					}
				var strCar = "0123456789/ :";
				for (j = 0; j < varForm.elements[i].value.length; j++)
					{
					if (strCar.indexOf(varForm.elements[i].value.charAt(j)) < 0)
						{
						strMessage = strMessage + "Le champ \"" + funcExtract(varForm.elements[i],'nom') + "\" ne peut contenir que les caract�res suivants \"" + strCar + "\"\n"
						funcErrorField(varForm.elements[i]);
						StatutValid = "";
						break
						}
					}
				}
//alert('password');
			//--- Contr�le des champs Password
			if ((funcExtract(varForm.elements[i],'type') == "Pwd") && (varForm.elements[i].name.toLowerCase().indexOf('_ctl') == -1))
				{
//alert('Pwd : ' + varForm.elements[i].name + ' - ' + varForm.elements[i].type);					
				if ((varForm.elements[i].value.length < 1) || (varForm.elements[i].value == "VIDE"))
					{
					strMessage = strMessage + "Le champ \"" + funcExtract(varForm.elements[i],'nom') + "\" est obligatoire,\n vous devez le remplir \n"
					funcErrorField(varForm.elements[i]);
					StatutValid = "";
					varForm.elements[i+1].value = "";					
					varPwdCtrl = "";
					}
					
					
				if (varForm.elements[i+1].name == varForm.elements[i].name + '_ctl')
					{
					if (varForm.elements[i].value != varForm.elements[i+1].value)
						{
						strMessage = strMessage + "Le 2�me mot de passe saisi pour confirmation est diff�rent.\n Veuillez recommencer.\n";
						funcErrorField(varForm.elements[i]);
						funcErrorField(varForm.elements[i+1]);
						varForm.elements[i].value = "";
						varForm.elements[i+1].value = "";					
						varPwdCtrl = "";
						StatutValid = "";
						}
	
					}
				else
					if (varPwdCtrl == "")
						{
						varPwdCtrl = varForm.elements[i].value;
						varForm.elements[i].value = "";
						strMessage = strMessage + "Veuillez saisir � nouveau le mot de passe pour confirmation.\n";
						funcErrorField(varForm.elements[i+1]);
						varForm.elements[i].select();
						StatutValid = "";
						}
					else
						{
						if (varPwdCtrl != varForm.elements[i].value)
							{
							strMessage = strMessage + "Le 2�me mot de passe saisi pour confirmation est diff�rent.\n Veuillez recommencer.\n";
							funcErrorField(varForm.elements[i]);
							funcErrorField(varForm.elements[i+1]);
							varForm.elements[i].value = "";
							varPwdCtrl = "";
							StatutValid = "";
							}
						}
					}
//alert(varForm.elements[i].value + ' - ' + strMessage + ' - ' + StatutValid);					
			}
		//--- retour du contr�le de saisie
		}
	if (StatutValid != "OK")
		{
		alert(strMessage);
		return false;
		}
	else
		{
		return true;
		}
	}

//--------------------------------------------------------------------------------
// Fonction de mise en �vidence d'un champ en erreur de saisie

function funcErrorField(varField)
	{
	varField.style.borderColor = "red";
	varField.style.borderWidth = "2px";
	varField.style.borderStyle = "solid";
	if (document.getElementById('Alert' + varField.name) == null)
		{
		varField.outerHTML = "<img id='Alert"+ varField.name +"' src='../images/_charte/alert.gif' align='top'>"+ varField.outerHTML;
		}
	}

//--------------------------------------------------------------------------------
// Fonction de r�initialisation d'un champ pr�c�demment en erreur de saisie

function funcInitField(varField)
	{
	varField.style.borderColor = "";
	varField.style.borderWidth = "";
//	varField.style.borderStyle = "";
	if (document.getElementById('Alert' + varField.name))
		{
		document.getElementById('Alert' + varField.name).outerHTML = "";
		}
	}


var varPassword2 = "";

function funcCtrlLogin(varId, varLogin, varWebId)
	{
	var StatutValid = "OK";	
	var oXmlHttp = null;
	if(window.XMLHttpRequest) // Firefox
		{
		oXmlHttp = new XMLHttpRequest();
		oXmlHttp.open("POST", "../_inc/XML_funcCtrlLogin.asp", false);
		oXmlHttp.onreadystatechange = function()
			{
			if(oXmlHttp.readyState == 4)
			eval(oXmlHttp.responseText);
			}
		oXmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		var data = "strId=" + varId + "&strLogin="+ varLogin + "&strWebId=" + varWebId;
		oXmlHttp.send(data);
		if (oXmlHttp.responseText != '')
			return false;
		else
			return true;
		}
	else if(window.ActiveXObject) // Internet Explorer
		{
		oXmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		oXmlHttp.open("POST", "../_inc/XML_funcCtrlLogin.asp?strId=" + varId + "&strLogin="+ varLogin + "&strWebId=" + varWebId, false);		
		oXmlHttp.send();
//alert(oXmlHttp.responseText)
		if (oXmlHttp.responseText != '')
			return false;
		else
			return true;
		}
	else
		{ // XMLHttpRequest non support� par le navigateur
		alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
		return;
		}
	}

//**** Controle de l'Email si d�j� existant
function funcCtrlEmail(varId, varEmail, varWebId)
	{
	var StatutValid = "OK";	
	var oXmlHttp = null;
	if(window.XMLHttpRequest) // Firefox
		{
		oXmlHttp = new XMLHttpRequest();
		oXmlHttp.open("POST", "../_inc/XML_funcCtrlEmail.asp", false);
		oXmlHttp.onreadystatechange = function()
			{
			if(oXmlHttp.readyState == 4)
			eval(oXmlHttp.responseText);
			}
		oXmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		var data = "strId=" + varId + "&strEmail="+ varEmail + "&strWebId=" + varWebId;
		oXmlHttp.send(data);
		if (oXmlHttp.responseText != '')
			return false;
		else
			return true;
		}
	else if(window.ActiveXObject) // Internet Explorer
		{
		oXmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		oXmlHttp.open("POST", "../_inc/XML_funcCtrlEmail.asp?strId=" + varId + "&strEmail="+ varEmail + "&strWebId=" + varWebId, false);		
		oXmlHttp.send();		
		if (oXmlHttp.responseText != '')
			return false;
		else
			return true;
		}
	else
		{ // XMLHttpRequest non support� par le navigateur
		alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
		return;
		}
	}