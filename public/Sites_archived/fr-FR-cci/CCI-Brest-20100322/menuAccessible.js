/* Adapt� du menu : iubito's menu - http://iubito.free.fr/prog/menu.php - configuration du javascript */


/* true = le menu sera vertical, � gauche.
   false = le menu sera horizontal, en haut. - Desactiv� depuis la modification du code- */
var vertical = true;

/* TRES IMPORTANT !
	Il faut mettre ici le nombre de menus, le script n'est pas capable de compter tout
	seul ! :-p Donc si votre code va jusqu'� <p id="menu5"...> il faut mettre 5. */
// Variables definies par le PHP de maniere a rendre la construction du menu dynamique.
//var nbmenu = 10;
//var nbmetamenu = 2;


/* Centrer le menu ? (true/false).
	Centre horizontalement ou verticalement suivant le mode choisi. */
var centrer_menu = false;

/* On est oblig� de d�finir une largeur pour les menus.
	Pour mettre des menus de largeurs diff�rentes :
	var largeur_menu = new Array(largeur menu1, largeur menu2, largeur menu3...)
	Il faut faire attention � mettre autant de valeurs que de nombre de menu !
	Attention, si vous �tes en menu vertical, mettez une largeur fixe (pas de Array) !
	*/
var largeur_metamenu = 165;
var largeur_menu = 165;
var largeur_marchespublics = 150;

/* En mode vertical, on a besoin de conna�tre la hauteur de chaque menu.
	M�me si les "cases" ne sont pas dimensionn�es en hauteur.
	Ajustez cette variable si les menus sont trop rapproch�s ou espac�s en vertical.
	Pour mettre des menus de hauteurs diff�rentes :
	var hauteur_menu = new Array(hauteur menu1, hauteur menu2, hauteur menu3...)
	Il faut faire attention � mettre autant de valeurs que de nombre de menu !
	Attention, si vous �tes en menu horizontal, mettez une largeur fixe (pas de Array) !
	*/
var hauteur_menu = 19;
var hauteur_metamenu = 18;


/* En mode horizontal.
	Largeur des sous-menus, pour IE uniquement, les autres navigateurs respectent la largeur
	auto. Mettez "auto" uniquement si vous �tes s�r d'avoir mis des &nbsp; � la place des
	espace dans les items !
	Pour mettre des sous-menus de largeurs diff�rentes :
	var largeur_sous_menu = new Array(largeur1, largeur2...).
	Il faut faire attention � mettre autant de valeurs que de menus.
	Si un menu n'a pas de sous-menus, il faut mettre quand m�me quelque chose !
	Il est possible de mettre "auto" dans certaines colonnes, � condition de respecter la
	consigne ci-dessus.
	*/
var largeur_sous_menu = 170;

/* Pour les navigateurs connaissant la largeur automatique (s'adapte au contenu), cette
	option (active par d�faut) permet d'avoir une largeur automatique. En cas contraire
	(false), les sous menus auront la largeur largeur_sous_menu. */
var largeur_auto_ssmenu = true;

/* ... pour mettre un peu d'espace entre les menus ! */
var espace_entre_menus = 0;


/* position du menu par rapport au haut de l'�cran ou de la page.
	0 = le menu est tout en haut. en px */
var top_menu = 120;
/* En version horizontale.
	position des sous-menus par rapport au haut de l'�cran ou de la page. Il faut pr�voir
	la hauteur des menus, donc ne pas mettre 0 et faire "� t�ton". en px */
var top_ssmenu = top_menu + 28;

/* Position gauche du menu, en px. */
var left_menu = 0;
/* En version verticale.
	Position des sous-menus par rapport au bord gauche de l'�cran. */
var left_ssmenu = left_menu+largeur_menu+2;

/* Quand la souris quitte un sous-menu, si le sous-menu disparait imm�diatement,
	cela g�ne l'utilisateur. Alors on peut mettre un d�lai avant disparition du sous-menu.
	500 ms c'est bien :-) */
var delai = 650; // en milliseconde

/* En version horizontale.
	Comme le menu peut se superposer avec le texte de la page, il est possible de faire
	descendre un peu la page (on augmente la marge du haut) pour a�rer un peu la page,
	une quarantaine de pixel c'est pas mal. en px*/
var marge_en_haut_de_page = top_menu + 40;
/* En version verticale.
	On d�cale le document � droite pour pas que le menu le superpose. */
var marge_a_gauche_de_la_page = 0;


/* Mettez � true si vous souhaitez que le menu soit toujours visible.
	Mettez false si vous ne le souhaitez pas, dans ce cas le menu "dispara�tra" quand vous
	descendrez dans la page. */
var suivre_le_scroll=true;

/* Pour IE uniquement, les balises <select> passent toujours au-dessus du menu, donc
	par d�faut on cache les listes d�roulantes quand le menu est ouvert, puis on les fait
	r�appara�tre � la fermeture du menu. Pour emp�cher �a, mettre � false. */
var cacher_les_select=false;


var timeout; //ne pas toucher, c'est pour d�clarer la variable
var agt = navigator.userAgent.toLowerCase();
var isMac = (agt.indexOf('mac') != -1);
var isOpera = (agt.indexOf("opera") != -1);
var IEver = parseInt(agt.substring(agt.indexOf('msie ') + 5));
var isIE = ((agt.indexOf('msie')!=-1 && !isOpera && (agt.indexOf('webtv')==-1)) && !isMac);
var isIE5win = (isIE && IEver == 5);
var isIE5mac = ((agt.indexOf("msie") != -1) && isMac);


var blnOk=true;
//pour enlever les "px" pour faire des calculs...
var reg = new RegExp("px", "g");

// onScroll pour Internet Explorer, le position:fixed fait ce boulot pour les autres navigateurs
// qui respectent les normes CSS...
window.onscroll = function()
{
	var kstart = 1;
	if (blnOk && suivre_le_scroll && (isIE || isIE5mac))
	{
		if (isIE5mac) document.getElementById("conteneurmenu").style.visibility="hidden";
		var cumul=0;
		for(i=1;i<=nbmetamenu;i++)
		{
			document.getElementById("metamenu"+i).style.top = document.body.scrollTop
						+(((i-1)*espace_entre_menus)+cumul+1+top_menu)+"px";
			cumul += isFinite(hauteur_metamenu)?hauteur_metamenu:hauteur_metamenu[i-1];
			
			var kend = (kstart==1?decoupage_menu[i-1]:decoupage_menu[i-1]+kstart-1);
			for(k=kstart;k<=kend;k++)
			{
				document.getElementById("menu"+k).style.top = document.body.scrollTop
							+(((k-1)*espace_entre_menus)+cumul+1+top_menu)+"px";
				if (document.getElementById("ssmenu"+k))//undefined
					document.getElementById("ssmenu"+k).style.top = document.body.scrollTop
							+(((k-1)*espace_entre_menus)+cumul+1+top_menu)+"px";
				cumul += isFinite(hauteur_menu)?hauteur_menu:hauteur_menu[k-1];
				kstart = k+1;
			}
		}
		document.getElementById("marchespublics").style.top = document.body.scrollTop
			+(((i-1)*espace_entre_menus)+cumul+1+top_menu)+"px";

		if (isIE5mac) document.getElementById("conteneurmenu").style.visibility="visible";
	}
}

function preChargement()
{
	if (document.getElementById("conteneurmenu"))
	{
		document.getElementById("conteneurmenu").style.visibility="hidden";
		//IE5 mac a un bug : quand un texte est dans un �l�ment de style float, il n'apparait pas.
		/*if (isIE5mac)
		{
			document.getElementById("conteneurmenu").style="";
		}*/
	}
}

function Chargement() {
	if (!blnOk) {
		if(document.body.style.backgroundColor!="") { blnOk=false; }
		if(document.body.style.color!="") { blnOk=false; }
		if(document.body.style.marginTop!="") { blnOk=false; }
		if(document.getElementById) {
			with(document.getElementById("conteneurmenu").style) {
				if(position!="" || top!="" || left!=""
						|| width!="" || height!="" || zIndex!=""
						|| margin!="" || visibility!="") {
					blnOk=false;
				}
			}
		}
		else{
			blnOk=false;
		}
	}

	if(blnOk)
	{
		document.getElementById("conteneurmenu").style.visibility="hidden";
		
		trimespaces();
		
		with(document.body.style) {
			marginLeft=marge_a_gauche_de_la_page+"px";
		}
		
		placerMenu();
		CacherMenus();
	}

	// comme on a �vit� le clignotement, maintenant on fait appara�tre le menu ;-)
	document.getElementById("conteneurmenu").style.visibility='';
}

// Code plac� dans agenda.html
//window.onresize = Chargement;

/*
 * Place les �l�ments du menu correctement, au chargement, au scroll, au redimensionnement
 * de la fen�tre
 */
function positionne() {
	var largeur_fenetre = (isIE?document.body.clientWidth:window.innerWidth);
	var hauteur_fenetre = (isIE?document.body.clientHeight:window.innerHeight);
	if (centrer_menu) {
		var hauteur_totale = espace_entre_menus * ((nbmenu-1) + (nbmetamenu-1));
		if (isFinite(hauteur_metamenu))
			hauteur_totale += hauteur_metamenu * nbmetamenu;
		else {
			for (i = 1; i <= nbmetamenu; i++)
				hauteur_totale += hauteur_metamenu[i-1];
		}
		if (isFinite(hauteur_menu))
			hauteur_totale += hauteur_menu * nbmenu;
		else {
			for (i = 1; i <= nbmenu; i++)
				hauteur_totale += hauteur_menu[i-1];
		}
		top_menu = (hauteur_fenetre - hauteur_totale)/2;
	}
	
	
	//Meta Menus
	var cumul = 0;
	var kstart = 1;
	for(i=1;i<=nbmetamenu;i++) {
		with(document.getElementById("metamenu"+i).style) {
			top=(((i-1)*espace_entre_menus)+cumul+1+top_menu)+"px";
			left=left_menu+"px";

			if (!suivre_le_scroll || isIE || isIE5mac)
				position="absolute";
			else position="fixed";

			margin="0";
			zIndex="2";
			if (vertical || isFinite(largeur_menu))
				width=largeur_menu+"px";
			else
				width=largeur_menu[i-1]+"px";
			if ((vertical && isFinite(hauteur_metamenu))) {
				cumul += hauteur_metamenu;
			}
			else {
				cumul += hauteur_metamenu[i-1];
				height=hauteur_metamenu[i-1]+"px";
			}
			
			// Positionne les menus du metamenu[i]
			var kend = (kstart==1?decoupage_menu[i-1]:decoupage_menu[i-1]+kstart-1);
			for(k=kstart;k<=kend;k++) {
				with(document.getElementById("menu"+k).style) {
					top=(((k-1)*espace_entre_menus)+cumul+1+top_menu)+"px";
					left=left_menu+"px";

					if (!suivre_le_scroll || isIE || isIE5mac)
						position="absolute";
					else position="fixed";

					margin="0";
					zIndex="2";
					if (vertical || isFinite(largeur_menu))
						width=largeur_menu+"px";
					else
						width=largeur_menu[k-1]+"px";
						
						
					// Positionne le sous menu associ�
					if (document.getElementById("ssmenu"+k))//undefined
					{
						with(document.getElementById("ssmenu"+k).style) {
							if (!suivre_le_scroll || isIE || isIE5mac)
								position="absolute";
							else position="fixed";

							left=left_ssmenu+"px";
							top=(((k-1)*espace_entre_menus)+cumul+1+top_menu)+"px";
	
							if (isIE || isOpera || isIE5mac || !largeur_auto_ssmenu) {
								if (isFinite(largeur_sous_menu))
									width = largeur_sous_menu+(largeur_sous_menu!="auto"?"px":"");
								else
									width = largeur_sous_menu[k-1]+(largeur_sous_menu[k-1]!="auto"?"px":"");
							}
							else width = "auto";
							margin="0";
							zIndex="3";
						}
					}
					// Fin positionnement du sous menu associ�
					
					if ((vertical && isFinite(hauteur_menu))) {
						cumul += hauteur_menu;
					}
					else {
						cumul += hauteur_menu[k-1];
						height=hauteur_menu[k-1]+"px";
					}
				}
				kstart = k+1;
			}

		}
	}
	
	// Placement de l'icone des Marches publics
	with(document.getElementById("marchespublics").style) {
		if (!suivre_le_scroll || isIE || isIE5mac)
			position="absolute";
		else position="fixed";

		left=(left_menu + Math.round((largeur_metamenu - largeur_marchespublics)/2) )+"px";
		top=(((k-1)*espace_entre_menus)+cumul+1+top_menu)+"px";

		if (vertical)
				width=largeur_marchespublics+"px";

		margin="0";
		zIndex="3";
	}

	
}


function MontrerMenu(strMenu) {
	if(blnOk) {
		AnnulerCacher();
		CacherMenus();
		if (document.getElementById(strMenu))//undefined
			with (document.getElementById(strMenu).style)
				visibility="visible";
	}
	SelectVisible("hidden",document.getElementsByTagName('select'));
}

function CacherDelai() {
	if (blnOk) {
		timeout = setTimeout('CacherMenus()',delai);
	}
}
function AnnulerCacher() {
	if (blnOk && timeout) {
		clearTimeout(timeout);
	}
}
function CacherMenus() {
	if(blnOk) {
		for(i=1;i<=nbmenu;i++) {
			if (document.getElementById("ssmenu"+i))//undefined
				with(document.getElementById("ssmenu"+i).style)
					visibility="hidden";
		}
	}
	SelectVisible("visible",document.getElementsByTagName('select'));
}

function trimespaces() {
	//Contourne un bug d'IE5/win... il ne capte pas bien les css pour les <li>, donc on les vire !
	if(blnOk&&isIE5win) {
		for(i=1;i<=nbmenu;i++) {
			if (document.getElementById("ssmenu"+i))//undefined
				with(document.getElementById("ssmenu"+i))
					innerHTML = innerHTML.replace(/<LI>|<\/LI>/g,"");
		}
	}
}

function SelectVisible(v,elem) {
	if (blnOk && cacher_les_select && (isIE||isIE5win))
		for (var i=0;i<elem.length;i++) elem[i].style.visibility=v;
}


// Fonctions ATL
function getPosX() {
	var posX = (document.body['clientWidth']-990)/2 + 16;
	//alert(document.body['clientWidth']);
		//alert(posX);
	return (posX < 18? 18:posX);
}

function placerMenu() {
	//alert(document.body['clientWidth']);
	left_menu = getPosX();
	left_ssmenu = left_menu+largeur_menu+2;
	positionne();
}
   
