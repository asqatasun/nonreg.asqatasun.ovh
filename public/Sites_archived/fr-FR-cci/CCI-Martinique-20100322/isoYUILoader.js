var isoYUILoader = function() {
	var loaded = false;
	var loadEvent = new YAHOO.util.CustomEvent("ready");
	return {
		load:function(base,require) {
			var loader = new YAHOO.util.YUILoader({base: base, require: require, loadOptional: true, onSuccess: function() {
				loaded = true;
				loadEvent.fire();
			}});
			loader.insert();
		},
		onReady:function(fn,scope) {
			if (loaded) fn.call(scope);
			else loadEvent.subscribe(fn,scope);
		}
	}
}();