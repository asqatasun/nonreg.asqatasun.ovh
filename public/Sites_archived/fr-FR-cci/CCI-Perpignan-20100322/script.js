function popup(page,tx,ty,nom_fenetre,barre_status,barre_outils,barre_scroll)
{
	if (parseInt(navigator.appVersion) > 3)
	{
		x = parseInt((screen.availWidth - tx) / 2);
		y = parseInt((screen.availHeight - ty) / 2);
	}
	else
	{
		x = 50;
		y = 50;
	}
	
	window.open(page,nom_fenetre,"status=" + barre_status + ",toolbar=" + barre_outils + ",resizable=1,scrollbars=" + barre_scroll + ",width=" + tx + ",height=" + ty + ",left=" + x  + ",top=" + y);
	return false;
}

function popupFlash(page,tx,ty,nom_fenetre,barre_status,barre_outils,barre_scroll)
{
	popup(page,tx,ty,nom_fenetre,barre_status,barre_outils,barre_scroll);
}

function imprime()
{
	if (window.print) window.print();
	else alert("Pour imprimer la page, cliquer sur le bouton 'Imprimer' de votre navigateur.");
	
	return false;
}

function popPrint(version,id)
{
	tx = 640;
	ty = 400;
	
	if (parseInt(navigator.appVersion) > 3)
	{
		x = parseInt((screen.availWidth - tx) / 2);
		y = parseInt((screen.availHeight - ty) / 2);
	}
	else
	{
		x = 50;
		y = 50;
	}
	
	var temp = document.location.href;
	var nom_page = temp.substring(temp.lastIndexOf("/")+1,temp.length);

	if (nom_page.indexOf("isens_stats") <  0)
		window.open("isens_print.php?id=" + id + "&lg=" + version + "&page=" + escape(nom_page),"imprimer","status=1,toolbar=0,resizable=1,scrollbars=1,width=" + tx + ",height=" + ty + ",left=" + x  + ",top=" + y);
}

function fermer()
{
	window.close();
}

function domaine(x)
{
	var i=x.host.indexOf(":");
	return (i>=0)?x.host.substring(0,i):x.host;
}

function goLinks(obj,lnks,i)
{
	var retour = true;
	if (lnks.li[i]) retour = lnks.li[i].oc();

	var img = new Image();
	var na = (navigator.appName == "Netscape");
			
	lien = 'isens_marker.php?externe=1&page=' + escape(obj.href);
	if (parseFloat(navigator.appVersion) >= 4)
	{
		s = screen;
		lien += '&screen=' + s.width + 'x' + s.height + 'x' + s.colorDepth;
	}
	
	var today = new Date();
	lien += "&time=" + today.getTime();
			
	if (na) setTimeout("img.src = lien;",1);
	else img.src = lien;

	return retour;
}

function isFileInterne(lien, domaine_interne)
{
	if ((lien.indexOf(domaine_interne) > 0) && (lien.indexOf("/files/") > 0)) return true;
	else return false;
}

function doLinks()
{
	temp = document.location.href;
	if (temp.indexOf("isens_stats.php") < 0)
	{
		lnks = new Object();
		var ln = document.links.length;
		lnks.li = new Array(ln);
		var domaine_courant = domaine(location);
		for (var i=0; i<ln; i++)
		{
			var buff = document.links[i] + "";
			if ((buff.indexOf("javascript:") == -1) && (document.links[i] != "#"))
			{
				if ((domaine_courant != domaine(document.links[i])) || isFileInterne(document.links[i].href, domaine_courant))
				{
					if(document.links[i].onclick)
					{
						lnks.li[i] = document.links[i];
						lnks.li[i].oc = document.links[i].onclick;
					}
					eval("document.links[i].onclick=function(){return goLinks(this,lnks,i);}");
				}
			}
		}
	}
}

window.onload = doLinks;