domMenu_data.set('domMenu_main', new Hash(
    1, new Hash(
        'contents', 'La vie de l entreprise',
        'uri', 'http://www.perdu.com',
        'statusText', 'la vie de l entreprise',
        1, new Hash(
            'contents', 'bli',
            'uri', 'www.perdu.com',
            'statusText', 'bli'
        ),
        2, new Hash(
            'contents', 'bla',
            'uri', 'www.perdu.com',
            'statusText', 'bla'
        )
    ),
    2, new Hash(
        'contents', 'Aménagement & urbanisme',
        'uri', 'www.perdu.com',
        'statusText', 'Aménagement & urbanisme',
        1, new Hash(
            'contents', 'plic',
            'uri', 'www.perdu.com',
            'statusText', 'plic'
        ),
        2, new Hash(
            'contents', 'ploc',
            'uri', 'www.perdu.com',
            'statusText', 'ploc'
        )
    ),
    3, new Hash(
        'contents', 'Compétences & formation',
        'uri', 'www.perdu.com',
        'statusText', '^^'
        ),
    4, new Hash(
        'contents', 'Les atouts du sud Alsace',
        'uri', 'www.perdu.com',
        'statusText', 'MDR'
    )
));

// }}}
// {{{ domMenu_vertical: settings

domMenu_settings.set('domMenu_main', new Hash(
    'axis', 'vertical',
    'subMenuWidthCorrection', -1,
    'verticalSubMenuOffsetX', -1,
    'verticalSubMenuOffsetY', -1,
    'horizontalSubMenuOffsetX', 0,
    'horizontalSubMenuOffsetY', 0
  //  'expandMenuArrowUrl', 'arrow.gif'
));

