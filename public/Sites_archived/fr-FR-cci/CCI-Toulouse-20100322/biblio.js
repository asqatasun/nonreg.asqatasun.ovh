// Evénements
function onDomReady(f){ //(C)webreflection.blogspot.com + fixe actengo.com
	var a=onDomReady,b=navigator.userAgent,d=document,w=window,c="onDomReady",e="addEventListener",o="opera",r="readyState",
	s="<scr".concat("ipt defer src='//:' on",r,"change='if(this.",r,"==\"complete\"){this.parentNode.removeChild(this);",c,".",c,"()}'></scr","ipt>");
	a[c]="";a[c]=(function(o){return function(){a[c]=function(){};for(a=arguments.callee;!a.done;a.done=1)f(o?o():o)}})(a[c]);
	if(d[e])d[e]("DOMContentLoaded",a[c],false);
	if(/WebKit|Khtml/i.test(b)||(w[o]&&parseInt(w[o].version())<9))(function(){/loaded|complete/.test(d[r])?a[c]():setTimeout(arguments.callee,1)})();
	else if(/MSIE/i.test(b))d.write(s);
}
function addEvents(oElem, sEvent, fn) {
	if (oElem.addEventListener)
		return oElem.addEventListener(sEvent, fn, false);
	else
		if (oElem.attachEvent) { // ie + fixe pb mem
			window.attachEvent('onunload', function() {
				oElem.detachEvent('on' + sEvent, fn);
				window.detachEvent('onunload', arguments.callee);
			});
			return oElem.attachEvent('on' + sEvent, fn);
		}
		else 
			return oElem['on' + sEvent] = fn;
}
function removeEvents(oElem, sEvent, fn) {
	return oElem.removeEventListener?
      oElem.removeEventListener(sEvent, fn, false):
      oElem.detachEvent?
         oElem.detachEvent('on' + sEvent, fn):
         oElem['on' + sEvent] = fn;
}
function getEvent(e) { return e || window.event; }

// insertion d'un lien imprimer
// iPos = la position d'un LI à rajouter si le conteneur est un ul
function windowPrint(oElem, sText, iPos) {
	if (window.print && document.getElementById(oElem) && sText != '') {
		var oEl = document.getElementById(oElem);
		var oA = document.createElement('a');
		oA.setAttribute('href', '#');
		oA.appendChild(document.createTextNode(sText));
		oA.onclick = function() { window.print(); return false; };

		var bInsert = false, cpt = 0; i = 0;
		if (oEl.tagName == "UL" || oEl.tagName == "OL") {
			if (oEl.childNodes || oEl.childNodes.length>0) {
				var oLi = document.createElement('li');
				oLi.appendChild(oA);
				while(i<oEl.childNodes.length && !bInsert) {
					if (oEl.childNodes[i].nodeName == "LI") {
						if(++cpt==iPos)	{
							oEl.insertBefore(oLi,oEl.childNodes[i]);
							bInsert = true;
						}
					}
					i++;
				}
			}
			if(!bInsert) oEl.appendChild(oLi);
		}
		else
			oEl.appendChild(oA);
	}
};

// Langue
function langue(sMot, aRemplace) {
	if(typeof( langueJS ) != "undefined") {
		var s = langueJS[sMot];
		if(aRemplace)
			for(var i = 0;i<aRemplace.length;i++)
				s = s.replace(aRemplace[i][0], aRemplace[i][1]);
		return s;
	}
	else
		return "Erreur traduction : " + sMot;
};

// Util
document.getElementsByClass = function (nomClass, node, tag) {
	var i, j = 0, classElements = [];
    if ( node == null ) node = document;
    if ( tag == null ) tag = '*';
    var els = node.getElementsByTagName(tag);
    var elsLen = els.length;
    var pattern = new RegExp("(^|\\s)" + nomClass + "(\\s|$)");

	for (i = 0; i < elsLen; i++) {
        if ( pattern.test(els[i].className) )
            classElements[j++] = els[i];
    }
    return classElements;
};

// trim
String.prototype.trim = function() { return this.replace(/(^\s*)|(\s*$)/g, ""); };

// Classe ajax
function Ajax() {
	var datas = "";

	this.init = function(settings) {
		this.settings = settings;

		this.def("parametres", null); 					// paramètres passés à la fonction appelée en retour
		this.def("prechargement", null); 				// fonction, nom d'id, ou objet pour afficher le message de chargement en cours
		this.def("prechargement_complementaire", null); // fonction d'exécution complémentaire au préchargement
		this.def("prechargement_message", "<p>Chargement en cours...<\/p>");	// message de chargement
		this.def("async", true);			// mode de synchronisation (asynchrone par defaut)
		this.def("alerte", true);			// si true, affiche les erreurs et n'execute pas la fonction de récupération
		this.def("debug", false);			// si true exécute la fonction debug
	};

	this.def = function(key, def_val) {
		this.settings[key] = this.getParam(key, def_val);
	};

	this.getParam = function(name, def_val) {
		var v = (typeof(this.settings[name]) == "undefined") ? def_val : this.settings[name];
		return (v == "true" || v == "false") ? (v == "true"): v;
	};

	this.debug = function(s) {
		if(typeof this.settings["debug"] == "object") {
			this.settings["debug"].value = s;
			this.settings["debug"].innerHTML = s;
		}
		else
			alert(s);
	};

	this.prechargement = function() {
		switch (typeof this.settings["prechargement"]) {
			case null:
				break;
			case "function" :
				this.settings["prechargement"]();
				break;
			case "object" :
				if (this.settings["prechargement"] != null)
					this.settings["prechargement"].innerHTML = this.settings["prechargement_message"];
				break;
			case "string" :
				document.getElementById(this.settings["prechargement"]).innerHTML = this.settings["prechargement_message"];
		}
	};

	this.prechargement_complementaire = function() {
		if (typeof this.settings["prechargement_complementaire"] == "function")
			this.settings["prechargement_complementaire"]();
	};

	this.charger = function( sMethod, sUrl, funct ) {
		var xhr, oReturn = new Object();
		var parametres = this.settings["parametres"];
		var alerte = this.settings["alerte"];
		sMethod = sMethod.toUpperCase(); // en majuscules comme spécifié par la norme HTTP

		// objet à utiliser selon navigateur
		if (window.XMLHttpRequest)
			xhr = new XMLHttpRequest();
		else if (window.ActiveXObject) // IE/Windows ActiveX version
			xhr = new ActiveXObject("Microsoft.XMLHTTP");

		if (xhr) {
			this.prechargement_complementaire();
			this.prechargement();

			if(datas.length > 0 && sMethod == "GET") {
				(sUrl.indexOf("?") == -1) ? sUrl += "?" : sUrl += "&";
				sUrl += datas;
			}
			xhr.open(sMethod, sUrl, this.settings["async"]);

			// le script appelé doit savoir que c'est un XMLHttpRequest
			xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");

			// s'assurer que le navigateur envoie le bon en-tête
			if ( xhr.overrideMimeType )
				xhr.setRequestHeader("Connection", "close");

			xhr.onreadystatechange = function() {
				if (xhr.readyState == 4) {
					if(xhr.status == 200) {
						oReturn.erreur = false;
						oReturn.responseText = xhr.responseText;
						oReturn.responseXML = xhr.responseXML;
					}
					else {
						oReturn.erreur = true;
						oReturn.message = "Un problème est survenu lors de la réception de données XML :\n" + xhr.statusText + " (" + xhr.status + ")";
					}

					// libére la mémoire
					xhr.onreadystatechange = function(){};
					xhr = null;

					if (alerte && oReturn.erreur)
						alert(oReturn.message);
					else
						funct(oReturn, parametres);
				}
			};
			if (sMethod == "POST") {
				xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				xhr.send(datas);
			}
			else
				xhr.send(null);
		}
		else {
			oReturn.erreur = true;
			oReturn.message = "Impossible d'obtenir des données XML.";
			if (alerte)
				alert(oReturn.message);
			else
				funct(oReturn, parametres);
		}
		if (this.settings["debug"]) this.debug("url: " + sUrl + "\n données: " + datas );
	};

	this.donnees = function(o) {
		var s = [];
		for ( var j in o )
			s.push( j + "=" + encodeURIComponent(o[j]) );
		datas = s.join("&");
	};

	this.effaceDonnees = function() {
		datas = "";
	};
};


/* en cours */
/*
addEvents(window,"load",lienPopup);

function getStandardEvent(e) { // IE fixe
	if (e == null && window.event) e = window.event;
	if (e.target == null && e.srcElement) e.target = e.srcElement;
	if (! e.preventDefault ) { e.preventDefault = function() { this.returnValue = false; } }

	return e;
}

function openLienPopup(e) {
	e = getStandardEvent(e);
	var liens = e.target;
	var href = '';

	if (liens.tagName=='IMG') {
		href = liens.parentNode;
		popup_image(href);
	}
	else {
		href = liens.getAttribute('href');
		window.open(href);
	}

	e.preventDefault();
	return false;
}

function lienPopup() {
	var oL, liens = document.getElementsByClass('lien_ext', null, 'a');
	for(var i=0; i<liens.length; i++) {
		liens[i].title = 'S\'ouvre dans une nouvelle fenêtre';
		addEvents(liens[i], 'click', openLienPopup);
	}
}*/
/* ---- */