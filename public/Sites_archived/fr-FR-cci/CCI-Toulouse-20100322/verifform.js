onDomReady(countCar);

function MM_findObj(n,d){var p,i,x;if(!d)d=document;if((p=n.indexOf("?"))>0&&parent.frames.length){d=parent.frames[n.substring(p+1)].document;n=n.substring(0,p);}if(!(x=d[n])&&d.all)x=d.all[n];for(i=0;!x&&i<d.forms.length;i++)x=d.forms[i][n];for(i=0;!x&&d.layers&&i<d.layers.length;i++)x=MM_findObj(n,d.layers[i].document);if(!x&&d.getElementById)x=d.getElementById(n);return x;}

// if (Est.email(v))
var Est = {
	sM : "[a-z0-9_]+((-[a-z0-9_]+)|([.][a-z0-9_]+)|(_[a-z0-9_]+))*@[a-z0-9]+(([.]|-)[a-z0-9]+)*[.][a-z]{2,}",
	sF : "(^[/(/)a-z0-9_-]+[.])",

	colorHexa: 	function(v) { return new RegExp(/^#+[\dA-F]{6}$|^$/i).test(v); },
	folder : 	function(v) { return new RegExp(/^[a-z\d_-]*$/i).test(v); },
	suppr :	 	function(v) { return new RegExp(/^(oui)$/i).test(v); },
	fileImg : 	function(v) { return new RegExp(eval("/" + this.sF + "(gif|jpg|jpeg|png)$/i")).test(v); },
	fileJPG : 	function(v) { return new RegExp(eval("/" + this.sF + "(jpg|jpeg)$/i")).test(v); },
	fileAll : 	function(v) { return new RegExp(eval("/" + this.sF + "[a-z\d]{3,4}$/i")).test(v); },
	heure : 	function(v) { return new RegExp(/^(([0-1]\d)|(2[0-3]))(:[0-5]\d){2}$/).test(v); },
	decimal : 	function(v) { return new RegExp(/^\d+[,.]?\d*$/i).test(v); },
	entier : 	function(v) { return new RegExp(/^\d+$/).test(v); },
	mailing :	function(v) { return new RegExp(eval("/^((" + this.sM + ");?){1,}$/i")).test(v); },
	email : 	function(v) { return new RegExp(eval("/^" + this.sM + "$/i")).test(v); },
	vide :		function(v) { return (v == "" || typeof(v) == "null"); },
	date :		function(v) {
					var err = '';
					if(!new RegExp(/^\d{2}\/\d{2}\/\d{4}$/).test(v))
						err = 'format non valide';
					else {
						var oD = vf_split_date(v);
						var aMois = [31, vf_nb_jour_fevrier(oD.an), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

						if (oD.mois>12 || oD.mois<1)
							err = 'mois non valide';
						else if (oD.jour>31 || oD.jour<1)
							err = 'jour non valide';
						else {
							if ((oD.an * 10000 + oD.mois * 100 + oD.jour) < 15821220) // date < au lundi 20 decembre 1582
								err = 'periode non valide';
							else if (aMois[--oD.mois] < oD.jour)
								err = 'date non valide';
						}
					}
					return err;
				},
	compare : 	function(v, v2, m) {
					switch (m) {
						case "<":
							return ( v < v2 );
							break;
						case ">":
							return ( v > v2 );
							break;
					}
					return false;
				},
	comprisEntre : function(v, min, max) {	return (v >= min && v <= max); }
};

function vf_mid(v, dtag, ftag) {
	var d = v.indexOf(dtag) + dtag.length;
	return ftag ? v.substring(d, v.indexOf(ftag)) : v.substring(d);
}
function vf_url2fichier(v) {
	var a = v.split(/\\|\//);
	return a[a.length-1];
}
function vf_split_date(d) {
	return {
		jour: parseInt(d.substring(0,2).replace(/(0)(\d)/gi,"$2")),
		mois: parseInt(d.substring(3,5).replace(/(0)(\d)/gi,"$2")),
		an: parseInt(d.substring(6,10))
	};
}
function vf_nb_jour_fevrier(an) { return ((an % 100 != 0 && an % 4 == 0) || (an % 400 == 0)) ? 29 : 28; }

function MM_validateForm() { // 4 arguments
	function Cond(c, v) {
		return (v.indexOf(c)!=-1);
	}

	var groupe, bCheck, j, i, err='', min, max;
	var args = MM_validateForm.arguments;
	for(i=0; i<(args.length-3); i+=4) {
		var v = (MM_validateForm.form) ? MM_validateForm.form.elements[args[i]] : MM_findObj(args[i]); // valeur
		var c = args[i+2]; 	 // condition
		var lbl = args[i+3]; // label

		if (v) {
			v = v.value;
			if (Cond('Trim', c)) v = v.replace(/(^\s*)|(\s*$)/g, "");

			if (!Est.vide(v) || v == "null") {
				if (Cond('isColorHexa', c)) if (!Est.colorHexa(v)) err += langue("isColorHexa", [["%1",lbl]]) + "\n";
				if (Cond('isFolder', c)) if (!Est.folder(v)) err += langue("isFolder", [["%1",lbl]]) + "\n";
				if (Cond('isSuppr', c)) if (!Est.suppr(v)) err += langue("isSuppr", [["%1",lbl]]) + "\n";
				if (Cond('isEmail', c)) if (!Est.email(v)) err += langue("isEmail", [["%1",lbl]]) + "\n";
				if (Cond('isMailing', c)) if (!Est.mailing(v)) err += langue("isMailing", [["%1",lbl]]) + "\n";
				if (Cond('isSelect', c)) if (v == "null" && c.charAt(0) == 'R') err += langue("isSelect", [["%1",lbl]]) + "\n";

				if (Cond('isHeure', c)) if (!Est.heure(v)) err += langue("isHeure", [["%1",lbl]]) + "\n";
				if (Cond('isDate', c) || Cond('isADate', c)) {
					if ( !(Cond('isADate', c) && v == "Toujours visible") ) {
						var sDate = Est.date(v);
						switch (sDate) {
							case 'format non valide':
								err += langue("isDate", [["%1",lbl]]) + "\n";
								break;
							case 'mois non valide':
								err += langue("isDateMois", [["%1",lbl]]) + "\n";
								break;
							case 'jour non valide':
								err += langue("isDateJour", [["%1",lbl]]) + "\n";
								break;
							case 'periode non valide':
								err += langue("isDatePeriode", [["%1",lbl]]) + "\n";
								break;
							case 'date non valide':
								var oD = vf_split_date(v); oD.mois--;
								var aMois = [ [langue("jan"), 31], [langue("fev"), vf_nb_jour_fevrier(oD.an)], [langue("mar"), 31], [langue("avr"), 30], [langue("mai"), 31], [langue("juin"), 30], [langue("jui"), 31], [langue("aou"), 31], [langue("sep"), 30], [langue("oct"), 31], [langue("nov"), 30], [langue("dec"), 31] ];
								err += langue("isDateNbJour", [["%1",oD.jour], ["%2",aMois[oD.mois][0]], ["%3",oD.an], ["%4",aMois[oD.mois][1]]]) + "\n";
						}
					}
		    	}

				if (Cond('isEntier', c)) if (!Est.entier(v)) err += langue("isEntier", [["%1",lbl]]) + "\n";
				if (Cond('isEntierRange', c)) {
					min = parseInt(vf_mid(c, ':', 'to'));
					max = parseInt(vf_mid(c, 'to'));
					if(!Est.comprisEntre(v, min, max)) err += langue("isEntierRange", [["%1",lbl], ["%2",min], ["%3",max]]) + "\n";
				}
				if (Cond('isDecimal', c)) if (!Est.decimal(v)) err += langue("isDecimal", [["%1",lbl]]) + "\n";
				if (Cond('isDecimalRange', c)) {
					min = parseFloat(vf_mid(c, ':', 'to'));
					max = parseFloat(vf_mid(c, 'to'));
					if(!Est.comprisEntre(v, min, max)) err += langue("isDecimalRange", [["%1",lbl], ["%2",min], ["%3",max]]) + "\n";
				}
				if (Cond('isMin', c)) {
					min = parseInt(vf_mid(c, '<', ':'));
					if (Est.compare(v.length, min, "<")) err += langue("isMin", [["%1",lbl], ["%2",min]]) + "\n";
				}
				if (Cond('isNombreMax', c)) if (Est.compare(v, parseFloat(vf_mid(c,'>',':')), ">")) err += langue("isNombreMax", [["%1",lbl]]) + "\n";

				if (Cond('isFileImg', c)) if (!Est.fileImg( vf_url2fichier(v) )) err += langue("isFileImg", [["%1",lbl], ["%2",v]]) + "\n";
				if (Cond('isFileJPG', c)) if (!Est.fileJPG( vf_url2fichier(v) )) err += langue("isFileJPG", [["%1",lbl]]) + "\n";
				if (Cond('isFileAll', c)) {
					v = vf_url2fichier(v);
					var ext = v.substr(v.lastIndexOf(".") + 1).toLowerCase();
					if (!Est.fileAll(v) || ext=="exe" || ext=="com" || ext=="bat") err += langue("isFileAll", [["%1",lbl], ["%2",v]]) + "\n";
			    }

				if (Cond('isCheckboxGForm', c)) { // spec géné de form
					bCheck = false;
					var oFgp = document.forms['ff_' + document.getElementById(args[i]).value];
					for(j=0;j<oFgp.length;j++) {
						groupe = oFgp.elements[j];
						if (groupe.type != null) {
							if (groupe.type.toLowerCase() == "checkbox" && groupe.name.indexOf(args[i] + "_") != -1)
								if(groupe.checked) bCheck = true;
						}
					}
					if (!bCheck) err += langue("isObligatoire", [["%1",lbl]]) + "\n";
				}
				else {
					if (Cond('isRadio', c) || Cond('isCheckbox', c)) {
						groupe = document.getElementsByName(args[i]);
						bCheck = false;
						for(j=0;j<groupe.length;j++) {
							if(groupe[j].checked) {
								if( Cond('isRadioChecked', c) || Cond('isCheckboxChecked', c) )
									bCheck = !groupe[j].disabled;
								else
									bCheck = true;
							}
							if (bCheck) break;
						}
						if (!bCheck) err += langue("isObligatoire", [["%1",lbl]]) + "\n";
					}
				}
			} else if (c.charAt(0) == 'R') err += langue("isObligatoire", [["%1",lbl]]) + "\n";
		}
	}

	MM_validateForm.form = null;
	if (err) alert(langue("error") + "\n\n" + err);
	document.MM_returnValue = (err == '');
}


function alertForm( sMod ) {
	switch (sMod) {
		case "rechercher" :
			alert(langue("alertRechercher"));
			break;
		default :
			alert(langue("alertDefaut"));
	}
}

function countCar() {
	var counts = document.getElementsByClass('maxlength', null, null);
	var i, count, matches, countHolder;

	for (i=0; i<counts.length; i++)	{
		count = counts[i];
		matches = count.className.match(/max_(\d+)/);
		count.maxVal = RegExp.$1;
		count.holder = document.getElementById(count.id + 'Count');
		if (count.holder) {
			count.onkeyup = function() { chkCountCar(this); };
			//count.onfocus = function() { chkCountCar(this); };
			//count.onblur = function() { chkCountCar(this); };
			chkCountCar(document.getElementById(count.id));
		}
	}
}
function chkCountCar(obj) {
	var len = obj.value.length;
	var s = obj.value.replace(/\r\n/g,'--').replace(/\n/g,'--'); // Cause BUG FF
	t = s.length - len;
	if ((len + t) > obj.maxVal) obj.value = obj.value.substring(0, obj.maxVal - t);
	var iLng = obj.maxVal - obj.value.length - t;
	obj.holder.innerHTML = langue("caractMaxi", [["%1",iLng],["%2",((iLng > 1) ? "(s)" : "")]]);
}

function disableForm() {
	var oEl, i, ii, aForm = document.forms;

	for (ii=0;ii<aForm.length;ii++) { // tous les formulaires de la page
		for (i=0;i<aForm[ii].length;i++) {
			oEl = aForm[ii].elements[i];
			if (oEl.type != null)
				switch (oEl.type.toLowerCase()) {
					case "submit":case "button":case "reset":case "image":
						oEl.disabled = true;
				}
		}
	}
}

// test 2 dates
function DateToNumber(oFormName, oDebName, oFinName, oNbdebName, oNbfinName) {
	var oForm = document.forms[oFormName];
	var aElem = [[oNbdebName, oDebName],[oNbfinName, oFinName]];
	for(var i = 0;i<aElem.length;i++)
		oForm.elements[aElem[i][0]].value = oForm.elements[aElem[i][1]].value.replace(/(\d{2})\/(\d{2})\/(\d{4})/gi,"$3$2$1");
}