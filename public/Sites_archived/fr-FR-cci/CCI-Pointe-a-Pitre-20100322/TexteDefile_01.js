var TextesDefilants=new Array();
function TexteDefilant(tmpPasDefil,TempsAffichage,tmpLargeur,tmpHauteur,tmpDirection,tmpIndexDeDepart,tmpControleTitre,tmpvitesseDefile)
{
//D�finit et initialise les vaiables pass�es en param�tre
this.PasDefil = 1
this.TimerDefile = null;
this.TableauTextes = new Array()
this.TableauTitres = new Array()
this.IndexEnCours = 0;
this.IndexEnPrecedent = -1;
this.Largeur = "100%";
this.Hauteur = "100%";
this.Direction = "UP";
this.ControleTitre = tmpControleTitre
this.PosLeft = 0;
this.PosTop = 0;
this.VitesseDefile = 10;
if(tmpvitesseDefile)
{
this.VitesseDefile = tmpvitesseDefile;
}
if(tmpPasDefil)
{
this.PasDefil = tmpPasDefil;
}
if(tmpLargeur)
{
this.Largeur = tmpLargeur;
}
if(tmpHauteur)
{
this.Hauteur = tmpHauteur;
}
if(tmpIndexDeDepart)
{
this.IndexEnCours = tmpIndexDeDepart;
}
if(tmpDirection)
{
this.Direction = tmpDirection
}
this.PosH=this.Hauteur;
//D�termine le nouveau num�ro d'index de l'objet
this.Index=TextesDefilants.length;
//Ins�re l'objet en cours dans le tableau des objets
TextesDefilants[this.Index]=this;
//Ajoute la chaine pass�e en param�tre dans les textes � faire d�filer
this.AddTexte = function(strChaine,strTitre)
{
var TmpId = this.TableauTextes.length;
this.TableauTextes[TmpId] = strChaine;
//Dessine le div ki va contenir le texte
//overflow:hidden, c pour masquer les barres de d�filement si le texte > taille du DIV
tmpDiv = "<DIV id='Div_TexteDefilant"+this.Index+"_"+TmpId+"' nowrap=true style='overflow:hidden;display:none;position:relative;left:"+this.PosLeft+";top:"+this.PosTop+";";
tmpDiv += "width:"+this.Largeur+"px;height:"+this.Hauteur+"px;z-index:100;'>";
document.write(tmpDiv);
document.write(this.TableauTextes[TmpId]);
document.write("</DIV>");
if(strTitre)
{
this.TableauTextes[TmpId] = strTitre;
}
else
{
this.TableauTextes[TmpId] = "";
}
}
this.Previous = function()
{
this.IndexEnPrecedent = this.IndexEnCours
this.IndexEnCours -=1
if (this.IndexEnCours>=this.TableauTextes.length)
{
this.IndexEnCours = 0;
}
else if (this.IndexEnCours<0)
{
this.IndexEnCours = (this.TableauTextes.length-1);
}
eval('TextesDefilants[' + this.Index + '].ChangeTexte()');
}
this.Next = function()
{
this.IndexEnPrecedent = this.IndexEnCours
this.IndexEnCours +=1
if (this.IndexEnCours>=this.TableauTextes.length)
{
this.IndexEnCours = 0;
}
else if (this.IndexEnCours<0)
{
this.IndexEnCours = (this.TableauTextes.length-1);
}
eval('TextesDefilants[' + this.Index + '].ChangeTexte(1)');
}
//Change le texte � faire d�filer
this.ChangeTexte = function()
{
if(document.getElementById("Div_TexteDefilant"+this.Index+"_"+this.IndexEnPrecedent) )
{
document.getElementById("Div_TexteDefilant"+this.Index+"_"+this.IndexEnPrecedent).style.display= "none";
}
this.IndexEnCours = this.IndexEnCours;
document.getElementById("Div_TexteDefilant"+this.Index+"_"+this.IndexEnCours).style.display= "";
if(this.Direction == "UP")
{
this.PosH=this.Hauteur;
this.PosTop =this.Hauteur;
this.PosLeft =0;
}
else if(this.Direction == "LEFT")
{
this.PosL=this.Largeur;
this.PosTop =0;
this.PosLeft =0;
}
else if(this.Direction == "DOWN")
{
this.PosH=1;
this.PosTop =0;
this.PosLeft =0;
}
else if(this.Direction == "RIGHT")
{
this.PosL=1;
this.PosTop =0;
this.PosLeft =0;
}
eval('TextesDefilants[' + this.Index + '].Defile()');
if(this.TableauTextes[this.IndexEnCours] != "")
{
//document.getElementById(this.ControleTitre).innerHTML = this.TableauTextes[this.IndexEnCours]
this.PosL = this.PosL;
}
}
//Arret d�filement
this.Stop = function()
{
if (this.TimerDefile!= null)
{
clearTimeout(this.TimerDefile);
this.TimerDefile=null;
}
}
//d�filement
this.Start=function ()
{
eval('TextesDefilants[' + this.Index + '].ChangeTexte()');
}
//Action du d�filement
this.Defile= function ()
{
clearTimeout(this.TimerDefile);
if(this.Direction == "UP")
{
this.PosH-=this.PasDefil;
this.PosTop-=this.PasDefil;
if (this.PosH<=0)
{
this.Stop();
this.TimerDefile = window.setTimeout("TextesDefilants[" + this.Index + "].Next()",TempsAffichage)
}
else
{
document.getElementById("Div_TexteDefilant"+this.Index+"_"+this.IndexEnCours).style.height = parseInt(this.Hauteur - this.PosH);
this.TimerDefile = window.setTimeout("TextesDefilants[" + this.Index + "].Defile()",this.VitesseDefile)
}
}
else if(this.Direction == "DOWN")
{
this.PosH+=this.PasDefil;
if (this.PosH>this.Hauteur)
{
this.Stop();
this.TimerDefile = window.setTimeout("TextesDefilants[" + this.Index + "].Next()",TempsAffichage)
}
else
{
document.getElementById("Div_TexteDefilant"+this.Index+"_"+this.IndexEnCours).style.height = parseInt(this.PosH);
this.TimerDefile = window.setTimeout("TextesDefilants[" + this.Index + "].Defile()",this.VitesseDefile)
}
}
else if(this.Direction == "LEFT")
{
this.PosL-=this.PasDefil;
if (this.PosL<=0)
{
this.Stop();
this.TimerDefile = window.setTimeout("TextesDefilants[" + this.Index + "].Next()",TempsAffichage)
}
else
{
document.getElementById("Div_TexteDefilant"+this.Index+"_"+this.IndexEnCours).style.width = parseInt(this.Largeur - this.PosL);
this.TimerDefile = window.setTimeout("TextesDefilants[" + this.Index + "].Defile()",this.VitesseDefile)
}
}
else if(this.Direction == "RIGHT")
{
this.PosL+=this.PasDefil;
if (this.PosL>this.Largeur)
{
this.Stop();
this.TimerDefile = window.setTimeout("TextesDefilants[" + this.Index + "].Next()",TempsAffichage)
}
else
{
document.getElementById("Div_TexteDefilant"+this.Index+"_"+this.IndexEnCours).style.width = parseInt(this.PosL);
this.TimerDefile = window.setTimeout("TextesDefilants[" + this.Index + "].Defile()",this.VitesseDefile)
}
}
document.getElementById("Div_TexteDefilant"+this.Index+"_"+this.IndexEnCours).style.top = parseInt(this.PosTop) + "px";
document.getElementById("Div_TexteDefilant"+this.Index+"_"+this.IndexEnCours).style.left = parseInt(this.PosLeft)+"px";
}
} 