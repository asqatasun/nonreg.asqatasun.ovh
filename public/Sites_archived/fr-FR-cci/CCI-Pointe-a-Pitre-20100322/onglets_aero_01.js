function hide_all_onglet_and_text_aero()
{
	document.getElementById('onglet_site_aeroport').className = 'class_site_aeroport_off';
	document.getElementById('onglet_facilite_proposees').className = 'class_facilite_proposees_off';
	document.getElementById('onglet_diffusion_info').className = 'class_diffusion_info_off';
	document.getElementById('onglet_support_comm').className = 'class_support_comm_off';
	document.getElementById('onglet_presta_proposes').className = 'class_presta_proposes_off';
	document.getElementById('onglet_espace_chef_entrp').className = 'class_espace_chef_entrp_off';
	
	document.getElementById('text_site_aeroport').style.display = 'none';
	document.getElementById('text_facilite_proposees').style.display= 'none';
	document.getElementById('text_diffusion_info').style.display= 'none';
	document.getElementById('text_support_comm').style.display= 'none';
	document.getElementById('text_presta_proposes').style.display= 'none';
	document.getElementById('text_espace_chef_entrp').style.display= 'none';
}

function change_onglet_aero(oBject)
{
	hide_all_onglet_and_text_aero();
	switch (oBject.id) {
		case 'onglet_site_aeroport':
			oBject.className = oBject.className=='class_site_aeroport_on'?'class_site_aeroport_off':'class_site_aeroport_on';
			document.getElementById('text_site_aeroport').style.display = 'block';
			break;
		case 'onglet_facilite_proposees':
			oBject.className = oBject.className=='class_facilite_proposees_off'?'class_facilite_proposees_on':'class_facilite_proposees_off';
			document.getElementById('text_facilite_proposees').style.display= 'block';
			break;
		case 'onglet_diffusion_info':
			oBject.className = oBject.className=='class_diffusion_info_off'?'class_diffusion_info_on':'class_diffusion_info_off';
			document.getElementById('text_diffusion_info').style.display= 'block';
			break;
		case 'onglet_support_comm':
			oBject.className = oBject.className=='class_support_comm_off'?'class_support_comm_on':'class_support_comm_off';
			document.getElementById('text_support_comm').style.display= 'block';
			break;
		case 'onglet_presta_proposes':
			oBject.className = oBject.className=='class_presta_proposes_off'?'class_presta_proposes_on':'class_presta_proposes_off';
			document.getElementById('text_presta_proposes').style.display= 'block';
			break;
		case 'onglet_espace_chef_entrp':
			oBject.className = oBject.className=='class_espace_chef_entrp_off'?'class_espace_chef_entrp_on':'class_espace_chef_entrp_off';
			document.getElementById('text_espace_chef_entrp').style.display= 'block';
			break;
	}
}