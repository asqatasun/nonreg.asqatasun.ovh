
<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<title>CCI LITTORAL NORMAND-PICARD : Chambre de Commerce et d'Industrie du Littoral Normand Picard</title>

<meta name="keywords" content="Somme, Picardie Maritime, CCI, Cr�ation entreprise, Cession reprise transmission, M�tallurgie, Sous-traitant, Robinetier, Serrure, Quincaillerie, Baie de Somme, Marquenterre, Ponthieu, Vimeu, Locaux industriels bureaux">

<meta name="description" content="la CCI LITTORAL NORMAND-PICARD informe, conseille, aide les entreprises et initie des actions favorisant le d�veloppement d�activit�s en baie de Somme.">

<meta name="abstract" content="CCI LITTORAL NORMAND-PICARD : la chambre de commerce et d'industrie du littoral Normand Picard pour le d�veloppement d�activit�s en Picardie Maritime">

<meta name="robots" CONTENT="all">

<meta name="rating" content=" General">

<meta name="robots" content="INDEX,FOLLOW">

<meta name="copyright" content="Netdivision">





<script language="javascript" src="http://littoral-normand-picard.cci.fr/Scripts/js/scripts.js"></script>

<link href="http://littoral-normand-picard.cci.fr/Scripts/css/style.css" rel="stylesheet" type="text/css">



<script src="http://littoral-normand-picard.cci.fr/Scripts/Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
</head>

<body class="fondBlanc">

<div id="Titre" style="position:absolute; width:732px; height:115px; z-index:1; visibility: hidden;">

  <h1>CCI LITTORAL NORMAND-PICARD <a href="http://littoral-normand-picard.cci.fr/Scripts/services_entreprise.php">au service des entreprises</a>  </h1>

  <h2>Aide &agrave; la cr&eacute;ation, <a href="http://littoral-normand-picard.cci.fr/Scripts/creation_reprise_entreprise.php">reprise et cession d'entreprises</a>, am&eacute;nagement parcs d&rsquo;activit&eacute;s, <a href="http://littoral-normand-picard.cci.fr/Scripts/decouvrez_picardie.php">d&eacute;veloppement tourisme</a>, la CCI informe, conseille, aide les entreprises et initie des actions favorisant le <a href="http://littoral-normand-picard.cci.fr/Scripts/developpez_entreprise.php">d&eacute;veloppement d&rsquo;activit&eacute;s</a>.</h2>

</div>

<table width="760" border="0" cellpadding="0" cellspacing="0" class="cadreMain" align="center" bgcolor="#FEFFFF">

  <tr>

    <td><TABLE width="758" border="0" cellpadding="0" cellspacing="0">

  <TR>

	<TD rowspan="2" valign="top"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/logo_entete_home.jpg" width="188" height="172" /></TD>

	<TD valign="top"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/visu_entete_home.jpg" width="570" height="140" /></TD>
  </TR>

  <TR>

	<TD height="32" background="http://littoral-normand-picard.cci.fr/Scripts/img/fond_recherche.jpg"><TABLE width="570" border="0" cellspacing="0" cellpadding="0">

<FORM name="form1" method="post" action="AC_RunActiveContent.js">

	  <TR>

		<TD><IMG src="http://littoral-normand-picard.cci.fr/Scripts/img/recherche2.gif" width="225" height="25"></TD>

		<TD valign="middle">

		  <SELECT name="menu" onChange="MM_jumpMenuDirect('sousmenu',this,0)" class="selectbox">

		    <OPTION value="rub1">Votre CCI</OPTION>

		    <OPTION value="rub2">La Picardie Maritime</OPTION>

		    <OPTION value="rub3">Implantation d'entreprise</OPTION>

		    <OPTION value="rub4">Cr&eacute;ez votre entreprise</OPTION>

		    <OPTION value="rub5">D&eacute;veloppez votre entreprise</OPTION>

		    <OPTION value="rub6">Transmission d'entreprise</OPTION>

		    <OPTION value="rub7">Services aux entreprises</OPTION>

		    <OPTION value="rub8">Actualit&eacute;s</OPTION>

		    <OPTION value="rub9">Supports de communication</OPTION>

		  </SELECT>

		</TD>

		<TD width="200" valign="middle">

	  <IFRAME src="http://littoral-normand-picard.cci.fr/Scripts/sous_menu_rub1.php" name="sousmenu" width="200" marginwidth="0" height="22" marginheight="0" scrolling="no" frameborder="0" hspace="0" vspace="0" id="iframe_corps" allowtransparency="true">

	  D�sol�, votre navigateur ne peut pas afficher correctement le contenu de 

	  ce site, pensez � le mettre � jour !</IFRAME></TD>

	</TR>

</FORM>

</TABLE></TD>
  </TR>

</TABLE>
</td>

  </tr>

  <tr>

    <td><table width="758" border="0" cellspacing="0" cellpadding="0">

      <tr>

        <td width="188" valign="top"><table width="188" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td><img src="http://littoral-normand-picard.cci.fr/Scripts/img/home_nav_haut.jpg" width="188" height="15"></td>

          </tr>

        </table><SCRIPT type="text/javascript" language="JavaScript1.2" src="http://littoral-normand-picard.cci.fr/Scripts/stm31.js"></SCRIPT>
<LINK href="http://littoral-normand-picard.cci.fr/Scripts/css/style.css" rel="stylesheet" type="text/css">
<SCRIPT type="text/javascript" language="JavaScript1.2" src="http://littoral-normand-picard.cci.fr/Scripts/stm31.js"></SCRIPT>
<TABLE width="188" border="0" cellpadding="0" cellspacing="0" style="background:url(http://littoral-normand-picard.cci.fr/Scripts/img/fond_nav.jpg) repeat-x">
  <TR>
    <TD height="350" valign="top">
<SCRIPT ID="Sothink Widgets:navigation.pgt" type="text/javascript" language="JavaScript1.2">
<!--
stm_bm(["menu007c",400,"","blank.gif",0,"","",0,0,150,0,150,1,0,0]);
stm_bp("p0",[1,4,0,0,0,0,0,0,100,"",-2,"",-2,50,0,0,"#fffff7","transparent","",3,0,0,"#000000"]);
stm_ai("p0i0",[2,"","img/menu/item1.gif","img/menu/item1_on.gif",188,30,0,"votre_cci_sommaire.php","_self","votre_cci_sommaire.php","","","",0,0,0,"","",0,0,0,0,0,"#fffff7",1,"#b5bed6",1,"","",3,3,0,0,"#fffff7","#000000","#000000","#000000","8pt Verdana","8pt Verdana",0,0]);
stm_bpx("p1","p0",[1,4,28,0,0,0,0,0,100,"progid:DXImageTransform.Microsoft.Wipe(GradientSize=1.0,wipeStyle=1,motion=forward,enabled=0,Duration=0.40)",5,"progid:DXImageTransform.Microsoft.Wipe(GradientSize=1.0,wipeStyle=1,motion=reverse,enabled=0,Duration=0.40)",4,70,0,0,"#fffff7","#fffff7","",3,1,1]);
stm_aix("p1i0","p0i0",[1,"<TABLE width=191 border=0 cellpadding=0 cellspacing=0>\r\n  <TR>\r\n    <TD valign=top bgcolor=#003366><TABLE width=191 border=0 cellpadding=0 cellspacing=0>\r\n      <TR>\r\n        <TD width=11 height=6 valign=top><IMG src=img/_dot.gif width=9 height=6></TD>\r\n        <TD width=226 height=6></TD>\r\n        <TD width=10 height=6></TD>\r\n      </TR>\r\n      <TR>\r\n        <TD width=11>&nbsp;</TD>\r\n        <TD width=226 class=blanc10><A href=votre_cci_strategie.php class=blanc10lien>&gt;Notre strat&eacute;gie</A></TD>\r\n        <TD width=10>&nbsp;</TD>\r\n      </TR>\r\n      <TR>\r\n        <TD width=11 height=1 valign=top><IMG src=img/_dot.gif width=9 height=1></TD>\r\n        <TD height=1 valign=top background=img/menu/pointille_menu.gif class=marineclair10></TD>\r\n        <TD width=10 height=1></TD>\r\n      </TR>\r\n      <TR>\r\n        <TD width=11>&nbsp;</TD>\r\n        <TD width=226 class=blanc10><A href=les_elus.php class=blanc10lien>&gt;Les &eacute;lus</TD>\r\n        <TD width=10>&nbsp;</TD>\r\n      </TR>\r\n      <TR>\r\n        <TD width=11 height=1 valign=top><IMG src=img/_dot.gif width=9 height=1></TD>\r\n        <TD height=1 valign=top background=img/menu/pointille_menu.gif class=marineclair10></TD>\r\n        <TD width=10 height=1></TD>\r\n      </TR>\r\n      <TR>\r\n        <TD width=11>&nbsp;</TD>\r\n        <TD width=226 class=blanc10><A href=nos_metiers.php class=blanc10lien>&gt;Nos m&eacute;tiers</A></TD>\r\n        <TD width=10>&nbsp;</TD>\r\n      </TR>\r\n      <TR>\r\n        <TD width=11 height=1 valign=top><IMG src=img/_dot.gif width=9 height=1></TD>\r\n        <TD height=1 valign=top background=img/menu/pointille_menu.gif class=marineclair10></TD>\r\n        <TD width=10 height=1></TD>\r\n      </TR>\r\n      <TR>\r\n        <TD width=11>&nbsp;</TD>\r\n        <TD width=226 class=blanc10><A href=quifaitquoi.php class=blanc10lien>&gt;Qui fait quoi &agrave; la CCI ?</A></TD>\r\n        <TD width=10>&nbsp;</TD>\r\n      </TR>\r\n      <TR>\r\n        <TD width=11 height=1 valign=top><IMG src=img/_dot.gif width=9 height=1></TD>\r\n        <TD height=1 valign=top background=img/menu/pointille_menu.gif class=marineclair10></TD>\r\n        <TD width=10 height=1></TD>\r\n      </TR>\r\n      <TR>\r\n        <TD>&nbsp;</TD>\r\n        <TD class=blanc10lien><A href=\"ou_nous_trouver.php\" class=\"blanc10lien\">&gt;O&ugrave; nous trouver ?</A></TD>\r\n        <TD>&nbsp;</TD>\r\n      </TR>\r\n	  <TR>\r\n        <TD width=11 height=1 valign=top><IMG src=img/_dot.gif width=9 height=1></TD>\r\n        <TD height=1 valign=top background=img/menu/pointille_menu.gif class=marineclair10></TD>\r\n        <TD width=10 height=1></TD>\r\n      </TR>\r\n	  <TR>\r\n        <TD>&nbsp;</TD>\r\n        <TD class=blanc10><A href=\"ecrivez_nous.php\" class=\"blanc10lien\">&gt;Ecrivez-nous</A></TD>\r\n        <TD>&nbsp;</TD>\r\n      </TR>\r\n      <TR>\r\n        <TD width=11 height=6><IMG src=img/_dot.gif width=9 height=6></TD>\r\n        <TD width=226 height=6></TD>\r\n        <TD width=10 height=6></TD>\r\n      </TR>\r\n    </TABLE></TD>\r\n  </TR>\r\n</TABLE>","","",-1,-1,0,"","_self","","","","",0,0,0,"","",0,0,0,0,1]);
stm_ep();
stm_aix("p0i1","p0i0",[2,"","img/menu/item2.gif","img/menu/item2_on.gif",188,30,0,"decouvrez_picardie.php","_self","decouvrez_picardie.php"]);
stm_bpx("p2","p1",[]);
stm_aix("p2i0","p1i0",[1,"<TABLE width=191 border=0 cellpadding=0 cellspacing=0>\r\n  <TR>\r\n    <TD valign=top bgcolor=#003366><TABLE width=191 border=0 cellpadding=0 cellspacing=0>\r\n      <TR>\r\n        <TD width=11 height=6 valign=top><IMG src=img/_dot.gif width=9 height=6></TD>\r\n        <TD width=226 height=6></TD>\r\n        <TD width=10 height=6></TD>\r\n      </TR>\r\n      <TR>\r\n        <TD width=11>&nbsp;</TD>\r\n        <TD width=226><A href=implantez_entreprise.php class=blanc10lien></A><SPAN class=blanc10lien><A href=decouvrez_picardie.php class=blanc10lien>&gt;Atouts</A></SPAN> </TD>\r\n        <TD width=10>&nbsp;</TD>\r\n      </TR>\r\n      <TR>\r\n        <TD width=11 height=1 valign=top><IMG src=img/_dot.gif width=9 height=1></TD>\r\n        <TD height=1 valign=top background=img/menu/pointille_menu.gif class=marineclair10></TD>\r\n        <TD width=10 height=1></TD>\r\n      </TR>\r\n      <TR>\r\n        <TD width=11>&nbsp;</TD>\r\n        <TD width=226 class=blanc10><A href=javascript:popupSansScroll('maison_picarde.php',745,500) class=blanc10lien>&gt;M&eacute;tiers et savoir-faire industriels</A></TD>\r\n    <TD width=10>&nbsp;</TD>\r\n      </TR>\r\n      <TR>\r\n        <TD width=11 height=6><IMG src=img/_dot.gif width=9 height=6></TD>\r\n        <TD width=226 height=6></TD>\r\n        <TD width=10 height=6></TD>\r\n      </TR>\r\n    </TABLE></TD>\r\n  </TR>\r\n</TABLE>\r\n"]);
stm_ep();
stm_aix("p0i2","p0i0",[2,"","img/menu/item3.gif","img/menu/item3_on.gif",188,30,0,"implantez_entreprise.php","_self","implantez_entreprise.php"]);
stm_aix("p0i3","p0i0",[2,"","img/menu/item4.gif","img/menu/item4_on.gif",188,30,0,"creation_reprise_entreprise.php","_self","creation_reprise_entreprise.php"]);
stm_bpx("p3","p1",[]);
stm_aix("p3i0","p1i0",[1,"<TABLE width=191 border=0 cellpadding=0 cellspacing=0>\r\n  <TR>\r\n    <TD valign=top bgcolor=#003366><TABLE width=191 border=0 cellpadding=0 cellspacing=0>\r\n      <TR>\r\n        <TD width=11 height=6 valign=top><IMG src=img/_dot.gif width=9 height=6></TD>\r\n        <TD width=226 height=6></TD>\r\n        <TD width=10 height=6></TD>\r\n      </TR>\r\n      <TR>\r\n        <TD width=11>&nbsp;</TD>\r\n        <TD width=226 class=\"blanc10lien\"><A href=\"creation_reprise_entreprise.php\" class=\"blanc10lien\">&gt;Cr&eacute;ation / reprise d\'entreprise</A></TD>\r\n        <TD width=10>&nbsp;</TD>\r\n      </TR>\r\n      <TR>\r\n        <TD width=11 height=1 valign=top><IMG src=img/_dot.gif width=9 height=1></TD>\r\n        <TD height=1 valign=top background=img/menu/pointille_menu.gif class=marineclair10></TD>\r\n        <TD width=10 height=1></TD>\r\n      </TR>\r\n      <TR>\r\n        <TD>&nbsp;</TD>\r\n        <TD class=blanc10><A href=aide_createurs.php class=blanc10lien>&gt;Le dispositif d\'aide aux cr&eacute;ateurs</A></TD>\r\n        <TD>&nbsp;</TD>\r\n      </TR>\r\n      <TR>\r\n        <TD width=11 height=6><IMG src=img/_dot.gif width=9 height=6></TD>\r\n        <TD width=226 height=6></TD>\r\n        <TD width=10 height=6></TD>\r\n      </TR>\r\n    </TABLE></TD>\r\n  </TR>\r\n</TABLE>"]);
stm_ep();
stm_aix("p0i4","p0i0",[2,"","img/menu/item5.gif","img/menu/item5_on.gif",188,30,0,"developpez_entreprise.php","_self","developpez_entreprise.php"]);
stm_bpx("p4","p1",[1,4,28,0,0,0,0,0,100,"progid:DXImageTransform.Microsoft.Wipe(GradientSize=1.0,wipeStyle=1,motion=forward,enabled=0,Duration=0.60)",5,"progid:DXImageTransform.Microsoft.Wipe(GradientSize=1.0,wipeStyle=1,motion=reverse,enabled=0,Duration=0.60)",4,50]);
stm_aix("p4i0","p1i0",[1,"<TABLE width=191 border=0 cellpadding=0 cellspacing=0>\r\n  <TR>\r\n    <TD valign=top bgcolor=#003366><TABLE width=191 border=0 cellpadding=0 cellspacing=0>\r\n      <TR>\r\n        <TD width=11 height=6 valign=top><IMG src=img/_dot.gif width=9 height=6></TD>\r\n        <TD width=226 height=6></TD>\r\n        <TD width=10 height=6></TD>\r\n      </TR>\r\n      <TR>\r\n        <TD width=11>&nbsp;</TD>\r\n        <TD width=226><A href=implantez_entreprise.php class=blanc10lien></A><SPAN class=blanc10lien><A href=develop_entrep_industrie.php class=blanc10lien>&gt;Industrie</A></SPAN> </TD>\r\n        <TD width=10>&nbsp;</TD>\r\n      </TR>\r\n      <TR>\r\n        <TD width=11 height=1 valign=top><IMG src=img/_dot.gif width=9 height=1></TD>\r\n        <TD height=1 valign=top background=img/menu/pointille_menu.gif class=marineclair10></TD>\r\n        <TD width=10 height=1></TD>\r\n      </TR>\r\n      <TR>\r\n        <TD>&nbsp;</TD>\r\n        <TD class=blanc10><A href=develop_entrep_commerce.php class=blanc10lien>&gt;Commerce &amp; services</A></TD>\r\n        <TD>&nbsp;</TD>\r\n      </TR>\r\n      <TR>\r\n        <TD height=1 valign=top><IMG src=img/_dot.gif width=9 height=1></TD>\r\n        <TD height=1 valign=top background=img/menu/pointille_menu.gif class=marineclair10></TD>\r\n        <TD height=1></TD>\r\n      </TR>\r\n      <TR>\r\n        <TD>&nbsp;</TD>\r\n        <TD class=blanc10><A href=develop_entrep_tourisme.php class=blanc10lien>&gt;Tourisme</A></TD>\r\n        <TD>&nbsp;</TD>\r\n      </TR>\r\n      <TR>\r\n        <TD height=1 valign=top><IMG src=img/_dot.gif width=9 height=1></TD>\r\n        <TD height=1 valign=top background=img/menu/pointille_menu.gif class=marineclair10></TD>\r\n        <TD height=1></TD>\r\n      </TR>\r\n      <TR>\r\n        <TD>&nbsp;</TD>\r\n        <TD class=blanc10><A href=\"develop_entrep_formation.php\" class=\"blanc10lien\">&gt;Formation</A></TD>\r\n        <TD>&nbsp;</TD>\r\n      </TR>\r\n      <TR>\r\n        <TD height=1 valign=top><IMG src=img/_dot.gif width=9 height=1></TD>\r\n        <TD height=1 valign=top background=img/menu/pointille_menu.gif class=marineclair10></TD>\r\n        <TD height=1></TD>\r\n      </TR>\r\n      <TR>\r\n        <TD width=11>&nbsp;</TD>\r\n        <TD width=226 class=blanc10><A href=\"develop_entrep_developpement.php\" class=\"blanc10lien\">&gt;D&eacute;veloppement durable &amp;<BR>\r\n&nbsp;&nbsp;environnement</A></TD>\r\n        <TD width=10>&nbsp;</TD>\r\n      </TR>\r\n      <TR>\r\n        <TD width=11 height=6><IMG src=img/_dot.gif width=9 height=6></TD>\r\n        <TD width=226 height=6></TD>\r\n        <TD width=10 height=6></TD>\r\n      </TR>\r\n    </TABLE></TD>\r\n  </TR>\r\n</TABLE>"]);
stm_ep();
stm_aix("p0i5","p0i0",[2,"","img/menu/item6.gif","img/menu/item6_on.gif",188,30,0,"transmettez_entreprise.php","_self","transmettez_entreprise.php"]);
stm_aix("p0i6","p0i0",[2,"","img/menu/item7.gif","img/menu/item7_on.gif",188,30,0,"services_entreprise.php","_self","services_entreprise.php"]);
stm_bpx("p5","p0",[1,4,28,0,0,0,0,0,100,"progid:DXImageTransform.Microsoft.Wipe(GradientSize=1.0,wipeStyle=1,motion=forward,enabled=0,Duration=0.60)",5,"progid:DXImageTransform.Microsoft.Wipe(GradientSize=1.0,wipeStyle=1,motion=reverse,enabled=0,Duration=0.60)",4]);
stm_aix("p5i0","p1i0",[1,"<TABLE width=191 border=0 cellpadding=0 cellspacing=0>\r\n  <TR>\r\n    <TD valign=top bgcolor=#003366><TABLE width=191 border=0 cellpadding=0 cellspacing=0>\r\n      <TR>\r\n        <TD width=11 height=6 valign=top><IMG src=img/_dot.gif width=9 height=6></TD>\r\n        <TD width=226 height=6></TD>\r\n        <TD width=10 height=6></TD>\r\n      </TR>\r\n      <TR>\r\n        <TD width=11>&nbsp;</TD>\r\n        <TD width=226><A href=service_enligne.php class=blanc10lien></A><SPAN class=blanc10lien><A href=service_enligne.php class=blanc10lien>&gt;Services en ligne</A></SPAN></TD>\r\n        <TD width=10>&nbsp;</TD>\r\n      </TR>\r\n      <TR>\r\n        <TD width=11 height=1 valign=top><IMG src=img/_dot.gif width=9 height=1></TD>\r\n        <TD height=1 valign=top background=img/menu/pointille_menu.gif class=marineclair10></TD>\r\n        <TD width=10 height=1></TD>\r\n      </TR>\r\n      <TR>\r\n        <TD>&nbsp;</TD>\r\n        <TD class=blanc10><A href=service_international.php class=blanc10lien>&gt;International</A></TD>\r\n        <TD>&nbsp;</TD>\r\n      </TR>\r\n      <TR>\r\n        <TD height=1 valign=top><IMG src=img/_dot.gif width=9 height=1></TD>\r\n        <TD height=1 valign=top background=img/menu/pointille_menu.gif class=marineclair10></TD>\r\n        <TD height=1></TD>\r\n      </TR>\r\n      <TR>\r\n        <TD>&nbsp;</TD>\r\n        <TD class=blanc10><A href=fichier_economique.php class=blanc10lien>&gt;Fichier &eacute;conomique</A></TD>\r\n        <TD>&nbsp;</TD>\r\n      </TR>\r\n      <TR>\r\n        <TD height=1 valign=top><IMG src=img/_dot.gif width=9 height=1></TD>\r\n        <TD height=1 valign=top background=img/menu/pointille_menu.gif class=marineclair10></TD>\r\n        <TD height=1></TD>\r\n      </TR>\r\n      <TR>\r\n        <TD>&nbsp;</TD>\r\n        <TD class=blanc10><A href=service_informatique360.php class=blanc10lien>&gt;Espace informatique </A></TD>  \r\n        <TD>&nbsp;</TD>\r\n      </TR>\r\n      <TR>\r\n        <TD height=1 valign=top><IMG src=img/_dot.gif width=9 height=1></TD>\r\n        <TD height=1 valign=top background=img/menu/pointille_menu.gif class=marineclair10></TD>\r\n        <TD height=1></TD>\r\n      </TR>\r\n    <TR>\r\n        <TD>&nbsp;</TD>\r\n        <TD class=blanc10><A href=\"service_cfe.php\" class=\"blanc10lien\">&gt;CFE</A></TD>\r\n        <TD>&nbsp;</TD>\r\n      </TR>\r\n      <TR>\r\n        <TD height=1 valign=top><IMG src=img/_dot.gif width=9 height=1></TD>\r\n        <TD height=1 valign=top background=img/menu/pointille_menu.gif class=marineclair10></TD>\r\n        <TD height=1></TD>\r\n      </TR>\r\n      <TR>\r\n        <TD>&nbsp;</TD>\r\n        <TD class=blanc10><A href=service_1pourcent.php class=blanc10lien>&gt;1% logement </A></TD>\r\n        <TD>&nbsp;</TD>\r\n      </TR>\r\n      <TR>\r\n        <TD height=1 valign=top><IMG src=img/_dot.gif width=9 height=1></TD>\r\n        <TD height=1 valign=top background=img/menu/pointille_menu.gif class=marineclair10></TD>\r\n        <TD height=1></TD>\r\n      </TR>\r\n      <TR>\r\n        <TD>&nbsp;</TD>\r\n        <TD class=blanc10><A href=service_social_emploi.php class=blanc10lien>&gt;Social &amp; Emploi </A></TD>\r\n        <TD>&nbsp;</TD>\r\n      </TR>\r\n      <TR>\r\n        <TD height=1 valign=top><IMG src=img/_dot.gif width=9 height=1></TD>\r\n        <TD height=1 valign=top background=img/menu/pointille_menu.gif class=marineclair10></TD>\r\n        <TD height=1></TD>\r\n      </TR>\r\n      <TR>\r\n        <TD>&nbsp;</TD>\r\n        <TD class=blanc10><A href=\"titre_restaurant.php\" class=\"blanc10lien\">&gt;Collecte des titres restaurant </A></TD>\r\n        <TD>&nbsp;</TD>\r\n      </TR>\r\n      <TR>\r\n        <TD height=1 valign=top><IMG src=img/_dot.gif width=9 height=1></TD>\r\n        <TD height=1 valign=top background=img/menu/pointille_menu.gif class=marineclair10></TD>\r\n        <TD height=1></TD>\r\n      </TR>\r\n      <TR>\r\n        <TD>&nbsp;</TD>\r\n        <TD class=blanc10><A href=\"quifaitquoi.php\" class=\"blanc10lien\">&gt;Qui fait quoi &agrave; la CCI </A></TD>\r\n        <TD>&nbsp;</TD>\r\n      </TR>\r\n      <TR>\r\n        <TD width=11 height=6><IMG src=img/_dot.gif width=9 height=6></TD>\r\n        <TD width=226 height=6></TD>\r\n        <TD width=10 height=6></TD>\r\n      </TR>\r\n    </TABLE></TD>\r\n  </TR>\r\n</TABLE>"]);
stm_ep();
stm_aix("p0i7","p0i0",[2,"","img/menu/item10.gif","img/menu/item10_on.gif",188,30,0,"port_treport.php","_self","port_treport.php"]);
stm_aix("p0i7","p0i0",[2,"","img/menu/item8.gif","img/menu/item8_on.gif",188,30,0,"actualites.php","_self","actualites.php"]);
stm_aix("p0i8","p0i0",[2,"","img/menu/item9.gif","img/menu/item9_on.gif",188,30,0,"supports_communications.php","_self","supports_communications.php"]);
stm_ep();
stm_em();
//-->
</SCRIPT></TD>
  </TR>
</TABLE> 
<a href="http://www.investir-en-picardie-maritime.fr" target="_blank"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/logo_investir_picardie2.jpg" width="188" height="125" border="0" /></a><br />
<br />
<A href="http://www.drakkaronline.com" target="_blank"><IMG src="http://littoral-normand-picard.cci.fr/Scripts/img/logo_DrakkarOnline2.gif" width="188" height="90" border="0"></A><br />
<table width="188" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left">&nbsp;</td>
    <td colspan="2" align="center"><a href="http://www.e-picardie.net/" class="marineclair10B">www.e-picardie.net</a></td>
  </tr>
  <tr>
    <td width="10" align="left"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="10" height="16" /></td>
    <td width="26" align="left">&nbsp;</td>
    <td width="152" align="left">&nbsp;</td>
  </tr>
  <tr>
    <td align="left"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="10" height="16" /></td>
    <td align="left"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/drapo_en.gif" width="21" height="16" /></td>
    <td align="left"><a href="http://littoral-normand-picard.cci.fr/Scripts/version_anglaise.php" class="marine10">English version</a></td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/logo_adresse.gif" width="43" height="50" /></td>
  </tr>
  <tr>
    <td colspan="3" align="center" class="marine10"><strong>Adresse postale</strong><br />
      20, rue du Chevalier de la Barre<br />
      80142 Abbeville C&eacute;dex<br />
      <strong>Si&egrave;ge social</strong><br />
      Saint Quentin Lamotte - Somme<br />
      T&eacute;l. : 0820 80 76 00 <br />
    Fax : 0820 80 76 01</td>
  </tr>
  <tr>
    <td colspan="3" align="center"><span class="marine10"><a href="http://littoral-normand-picard.cci.fr/Scripts/ecrivez_nous.php" class="marine10">Contact</a></span></td>
  </tr>
  <tr>
    <td colspan="3" align="center" valign="top"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="16" height="15" /></td>
  </tr>
  <tr>
    <td colspan="3" align="center" valign="top"><a href="http://www.entreprise.cci.fr/HomePage_CCIFR" target="_blank"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/logo_email.gif" width="36" height="25" border="0" /></a></td>
  </tr>
  <tr>
    <td colspan="3"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="16" height="43" /></td>
  </tr>
</table>
</td>

        <td width="570" rowspan="2" valign="top" style="background:url(http://littoral-normand-picard.cci.fr/Scripts/img/fond_ciel_haut.jpg) no-repeat;"><table width="570" border="0" cellspacing="0" cellpadding="0">

          <tr>

            <td><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="22" height="17"></td>

            <td><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="385" height="16"></td>

            <td><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="12" height="16"></td>

            <td><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="151" height="16"></td>

            </tr>

          <tr>

            <td>&nbsp;</td>

            <td valign="top" class="marine10">
			<!--<table width="380" height="22" border="0" cellpadding="0" cellspacing="0" >
			<tr>
                  <td width="20"><img src="img/_dot.gif" width="20" height="16"></td>
                  <td width="365" class="marine10"><br><br>
				  <img src="/img/la_lettre_de_lenjeu.jpg" style="float:left;margin-right:10px"><strong>POUR RECEVOIR GRATUITEMENT LES INFOS DE LA CCI JE COMMUNIQUE MON ADRESSE MEL.</strong><br><br>
<a class="bleu10" href="javascript:popupSansScroll('popup_inscription_newsletter.php',550,450)">Enregistrez-vous d�s maintenant en remplissant le formulaire d'inscription</a>
				</td>
                </tr>
				</table> -->
			<table width="380" height="22" border="0" cellpadding="0" cellspacing="0" background="http://littoral-normand-picard.cci.fr/Scripts/img/equerre.gif">

              <tr>

                <td class="marine10"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/home_une.gif" width="74" height="15" hspace="8"></td>

                <td align="right" class="marine10">Lundi 22 mars 2010</td>

              </tr>

            </table>

              

              
              <table width="385" border="0" cellspacing="0" cellpadding="0">

                <tr>

                  <td class="marine18"><span class="bleu18B">Ev�nement :</span> <b>ENERGIES RENOUVELABLES Une source naturelle d'�conomie</b></td>

                  </tr>

                <tr>

                  <td class="marine10">

                  	

			<img src="http://littoral-normand-picard.cci.fr/Scripts/fichiers/articles/pictos/une/206.jpg?51" hspace="6" align="right" class="bordureBleue">			

                  

                  <font class='orange10'><b><H5>Colloque jeudi 1er avril 2010 !</H5></b></font><br><b>En pr�ambule au Colloque qui se tiendra en salle de s�minaire de la P�pini�re EnR � Oust-Marest, la CCI Littoral Normand-Picard vous propose la visite du Centre de Recherche EnR de la Soci�t� AUER � Feuqui�res-en-Vimeu.</b>
                  <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=206" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                   <br></td>

                  </tr>

              </table>

              <br>

              <table width="380" height="22" border="0" cellpadding="0" cellspacing="0" background="http://littoral-normand-picard.cci.fr/Scripts/img/equerre.gif">

                <tr>

                  <td class="marine10"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/home_bref.gif" width="67" height="16" hspace="8"></td>

                </tr>

              </table>

                            <table width="385" border="0" cellspacing="0" cellpadding="0">


                

                <!-- actu -->

                <tr>

                  <td width="20"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="20" height="16"></td>

                  <td width="365" class="marine10"><span class="bleu10"><strong>Communication :</strong></span> <strong>RAPPORT ACTIVITES 2009 CCI LITTORAL NORMAND PICARD EST PARU</strong><br>

	

			<img src="http://littoral-normand-picard.cci.fr/Scripts/fichiers/articles/pictos/bref/215.jpg?51" vspace="3" align="left" class="bordureBleue">			


<font class='orange10'><b>Vous souhaitez conna�tre nos actions conduites en 2009 ?<br>Consultez le document de nos r�alisations les plus repr�sentatives en faveur du d�veloppement des entreprises de Picardie Maritime et de la Vall�e de la Bresle.</b></font> <br><br>

                      <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=215" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                      <br><br></td>

                </tr>

                <!-- /actu -->

                


                

                <!-- actu -->

                <tr>

                  <td width="20"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="20" height="16"></td>

                  <td width="365" class="marine10"><span class="bleu10"><strong>Tourisme :</strong></span> <strong>LA CLEF VERTE HEBERGEMENTS TOURISTIQUES</strong><br>

	

			<img src="http://littoral-normand-picard.cci.fr/Scripts/fichiers/articles/pictos/bref/214.jpg?51" vspace="3" align="left" class="bordureBleue">			


<font class='orange10'><b>Pas encore labellis� ?<br><br>Contactez notre Conseill�re qui vous guidera dans la d�marche et dans votre dossier de candidature � d�poser avant le 25 avril 2010.</b></font><br><br>

                      <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=214" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                      <br><br></td>

                </tr>

                <!-- /actu -->

                


                

                <!-- actu -->

                <tr>

                  <td width="20"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="20" height="16"></td>

                  <td width="365" class="marine10"><span class="bleu10"><strong>Ev�nement :</strong></span> <strong><font class='orange10'><b>FORUM REGIONAL DE LA TRANSMISSION REPRISE D ENTREPRISES</b></font></strong><br>

	

			<img src="http://littoral-normand-picard.cci.fr/Scripts/fichiers/articles/pictos/bref/213.jpg?51" vspace="3" align="left" class="bordureBleue">			


<b>Mardi 1er juin 2010 � partir de 13 h 00<br>la 3�me �dition r�gionale se tiendra au Centre culturel de Chauny, place Yves Brinon.<br><br>D�couvrez les 5 p�les d'expertises pour un conseil personnalis� et inscrivez-vous !</b><br>

                      <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=213" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                      <br><br></td>

                </tr>

                <!-- /actu -->

                


                

                <!-- actu -->

                <tr>

                  <td width="20"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="20" height="16"></td>

                  <td width="365" class="marine10"><span class="bleu10"><strong>Ev�nement :</strong></span> <strong>Lundi 29 mars de 17 h � 19 h � 5 � 7 Industrie � � Blangy-sur-Bresle</strong><br>

	

			<img src="http://littoral-normand-picard.cci.fr/Scripts/fichiers/articles/pictos/bref/212.jpg?51" vspace="3" align="left" class="bordureBleue">			


" La finalit� et les enjeux de la qualit� d�accueil des stagiaires et nouveaux salari�s au sein de l�entreprise ". <br>

                      <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=212" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                      <br><br></td>

                </tr>

                <!-- /actu -->

                


                

                <!-- actu -->

                <tr>

                  <td width="20"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="20" height="16"></td>

                  <td width="365" class="marine10"><span class="bleu10"><strong>D�veloppement durable :</strong></span> <strong>Eco entreprise en Picardie</strong><br>

	

			<img src="http://littoral-normand-picard.cci.fr/Scripts/fichiers/articles/pictos/bref/210.jpg?51" vspace="3" align="left" class="bordureBleue">			


Vous �tes une �co-entreprise implant�e en Picardie ?<br>Faites-vous conna�tre gratuitement !<br>

                      <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=210" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                      <br><br></td>

                </tr>

                <!-- /actu -->

                


                

                <!-- actu -->

                <tr>

                  <td width="20"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="20" height="16"></td>

                  <td width="365" class="marine10"><span class="bleu10"><strong>Entreprise :</strong></span> <strong>R�forme de la taxe professionnelle : <b>un nouveau simulateur � destination des entreprises,</b></strong><br>

	

			<img src="http://littoral-normand-picard.cci.fr/Scripts/fichiers/articles/pictos/bref/209.jpg?51" vspace="3" align="left" class="bordureBleue">			


con�u pour permettre d��valuer les effets de la r�forme � partir de donn�es simples et de l�avis de taxe professionnelle 2009, <a href="http://www3.finances.gouv.fr/formulaires/dgi/2010/CET/" target="_blank"><b>simulateur, cliquer ici.</b></a><br>

                      <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=209" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                      <br><br></td>

                </tr>

                <!-- /actu -->

                


                

                <!-- actu -->

                <tr>

                  <td width="20"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="20" height="16"></td>

                  <td width="365" class="marine10"><span class="bleu10"><strong>Industrie :</strong></span> <strong>REACH : le catalogue des bonnes pratiques gratuit sur demande</strong><br>

	

			<img src="http://littoral-normand-picard.cci.fr/Scripts/fichiers/articles/pictos/bref/207.jpg?51" vspace="3" align="left" class="bordureBleue">			


D�entreprises � entreprises : des r�flexions et des propositions concr�tes de dirigeants de PME. <br>Comment s�organiser pour respecter REACH mais aussi en tirer un avantage commercial ?<br>

                      <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=207" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                      <br><br></td>

                </tr>

                <!-- /actu -->

                


                

                <!-- actu -->

                <tr>

                  <td width="20"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="20" height="16"></td>

                  <td width="365" class="marine10"><span class="bleu10"><strong>Entreprise :</strong></span> <strong>Transmission d'entreprises : nous sommes l� pour vous accompagner</strong><br>

	

			<img src="http://littoral-normand-picard.cci.fr/Scripts/fichiers/articles/pictos/bref/202.jpg?51" vspace="3" align="left" class="bordureBleue">			


<font class='orange10'>Votre CCI lance une enqu�te Transmission pour faciliter la cession des entreprises, p�renniser le tissu �conomique et social du territoire.</font><br>

                      <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=202" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                      <br><br></td>

                </tr>

                <!-- /actu -->

                


                

                <!-- actu -->

                <tr>

                  <td width="20"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="20" height="16"></td>

                  <td width="365" class="marine10"><span class="bleu10"><strong>Industrie :</strong></span> <strong><font class='orange10'><b>ACTION COLLECTIVE PME TIC</b></font></strong><br>

	

			<img src="http://littoral-normand-picard.cci.fr/Scripts/fichiers/articles/pictos/bref/203.jpg?51" vspace="3" align="left" class="bordureBleue">			


<b>Pour mettre en place au sein de votre entreprise :<br>- Un site Internet, intranet, extranet... <br>- Une d�mat�rialisation et des t�l�proc�dures <br>- Un syst�me EDI/WebEDI <br>- Un progiciel de gestion int�gr� (PGI/ERP) <br>- Un travail collaboratif...</b> <br>

                      <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=203" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                      <br><br></td>

                </tr>

                <!-- /actu -->

                


                

                <!-- actu -->

                <tr>

                  <td width="20"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="20" height="16"></td>

                  <td width="365" class="marine10"><span class="bleu10"><strong>Ev�nement :</strong></span> <strong><b>LA PEPINIERE EnR INAUGUREE</b></strong><br>

	

			<img src="http://littoral-normand-picard.cci.fr/Scripts/fichiers/articles/pictos/bref/198.jpg?51" vspace="3" align="left" class="bordureBleue">			


<font class='orange10'><b>Lundi 11 janvier 2010, �lus et acteurs �conomiques inauguraient la P�pini�re d�entreprises Energies Renouvelables � Oust-Marest implant�e au c�ur du Parc environnemental d�activit�s de Gros Jacques.</b></font><br>

                      <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=198" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                      <br><br></td>

                </tr>

                <!-- /actu -->

                


                

                <!-- actu -->

                <tr>

                  <td width="20"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="20" height="16"></td>

                  <td width="365" class="marine10"><span class="bleu10"><strong>Tourisme :</strong></span> <strong>Restauration & H�tellerie : les Pr�ts Participatifs OSEO</strong><br>

	

			<img src="http://littoral-normand-picard.cci.fr/Scripts/fichiers/articles/pictos/bref/196.jpg?51" vspace="3" align="left" class="bordureBleue">			


Pr�t Participatif pour la Modernisation et la Transmission de la Restauration. <br>Pr�t Participatif pour la R�novation H�teli�re. <br>

                      <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=196" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                      <br><br></td>

                </tr>

                <!-- /actu -->

                


                

                <!-- actu -->

                <tr>

                  <td width="20"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="20" height="16"></td>

                  <td width="365" class="marine10"><span class="bleu10"><strong>Pratique :</strong></span> <strong>CREATEURS et REPRENEURS D ENTREPRISE les dates 2010 des sessions d'information</strong><br>

	

			<img src="http://littoral-normand-picard.cci.fr/Scripts/fichiers/articles/pictos/bref/195.jpg?51" vspace="3" align="left" class="bordureBleue">			


<font class='orange10'><b><br><a href="http://www.littoral-normand-picard.cci.fr/fichiers/pdf/217_calendrier_2010_des_demi_journees_information_createurs.pdf" target="_blank">Consultez le calendrier 2010</a><br></b></font> des demi-journ�es d'information destin�es aux cr�ateurs et repreneurs d'entreprise.<br>Les sessions ont lieu � l'antenne d'Abbeville ou � l'antenne du Tr�port.<br>

                      <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=195" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                      <br><br></td>

                </tr>

                <!-- /actu -->

                


                

                <!-- actu -->

                <tr>

                  <td width="20"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="20" height="16"></td>

                  <td width="365" class="marine10"><span class="bleu10"><strong>Ev�nement :</strong></span> <strong>Pour la 1�re fois en France une p�pini�re d entreprises d�di�e aux Energies Renouvelables</strong><br>

	

			<img src="http://littoral-normand-picard.cci.fr/Scripts/fichiers/articles/pictos/bref/194.jpg?51" vspace="3" align="left" class="bordureBleue">			


La conf�rence tenue le 4 d�cembre dernier dans le cadre du programme officiel de Pollutec 2009 a remport� un vif succ�s aupr�s des visiteurs.<br>

                      <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=194" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                      <br><br></td>

                </tr>

                <!-- /actu -->

                


                

                <!-- actu -->

                <tr>

                  <td width="20"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="20" height="16"></td>

                  <td width="365" class="marine10"><span class="bleu10"><strong>Pratique :</strong></span> <strong>Les aides du Conseil G�n�ral de la Somme</strong><br>

	

			<img src="http://littoral-normand-picard.cci.fr/Scripts/fichiers/articles/pictos/bref/185.jpg?51" vspace="3" align="left" class="bordureBleue">			


<font class='orange10'><b>Les "nouveaux" dispositifs d'aide du d�partement de la Somme disponibles sur fiches descriptives.</b></font><br>

                      <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=185" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                      <br><br></td>

                </tr>

                <!-- /actu -->

                


                

                <!-- actu -->

                <tr>

                  <td width="20"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="20" height="16"></td>

                  <td width="365" class="marine10"><span class="bleu10"><strong>Entreprise :</strong></span> <strong>Cr�er son entreprise dans les Energies Renouvelables</strong><br>

	

			<img src="http://littoral-normand-picard.cci.fr/Scripts/fichiers/articles/pictos/bref/184.jpg?51" vspace="3" align="left" class="bordureBleue">			


La P�pini�re d'entreprises Energies Renouvelables accueillait le 20 octobre dernier sa premi�re Conf�rence th�matique dans le cadre du Mois de la Cr�ation.<br>

                      <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=184" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                      <br><br></td>

                </tr>

                <!-- /actu -->

                


                

                <!-- actu -->

                <tr>

                  <td width="20"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="20" height="16"></td>

                  <td width="365" class="marine10"><span class="bleu10"><strong>Entreprise :</strong></span> <strong>AchatVille avec votre CCI LNP : Commerces & Services de proximit� sur Internet</strong><br>

	

			<img src="http://littoral-normand-picard.cci.fr/Scripts/fichiers/articles/pictos/bref/181.jpg?51" vspace="3" align="left" class="bordureBleue">			


Vos commer�ants sont aussi sur Internet<br><a href="http://www.achat-littoral-normand-picard.com" target="_blank"><b><font class='orange10'>achat-littoral-normand-picard.com </font></b></a><br>

                      <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=181" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                      <br><br></td>

                </tr>

                <!-- /actu -->

                


                

                <!-- actu -->

                <tr>

                  <td width="20"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="20" height="16"></td>

                  <td width="365" class="marine10"><span class="bleu10"><strong>Industrie :</strong></span> <strong>VEILLE DES MARCHES PUBLICS ET PRIVES </strong><br>

	

			<img src="http://littoral-normand-picard.cci.fr/Scripts/fichiers/articles/pictos/bref/168.jpg?51" vspace="3" align="left" class="bordureBleue">			


<b>Une action collective r�gionale mise en place par la DREAL de Haute-Normandie.</b><br>

                      <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=168" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                      <br><br></td>

                </tr>

                <!-- /actu -->

                


                

                <!-- actu -->

                <tr>

                  <td width="20"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="20" height="16"></td>

                  <td width="365" class="marine10"><span class="bleu10"><strong>Entreprise :</strong></span> <strong>Plan de Continuit� d'Activit� en cas de pand�mie de grippe A : 5 r�unions d'info en septembre</strong><br>

	

			<img src="http://littoral-normand-picard.cci.fr/Scripts/fichiers/articles/pictos/bref/165.jpg?51" vspace="3" align="left" class="bordureBleue">			


Face au risque de pand�mie, les entreprises doivent pr�voir un fonctionnement de leur activit� en mode d�grad� int�grant un absent�isme pouvant atteindre 40 % de leur effectif. L�ensemble de ces mesures doit �tre pr�sent� dans un Plan de Continuit� d�Activit�, PCA.<br><font class='orange10'><b>Cliquer "en savoir + " pour t�l�charger les documents pratiques mis � votre disposition.</b></font><br>

                      <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=165" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                      <br><br></td>

                </tr>

                <!-- /actu -->

                


                

                <!-- actu -->

                <tr>

                  <td width="20"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="20" height="16"></td>

                  <td width="365" class="marine10"><span class="bleu10"><strong>Entreprise :</strong></span> <strong><font class='orange10'>NACRE - Nouvel accompagnement pour la cr�ation et la reprise d'entreprise</font></strong><br>

	

			<img src="http://littoral-normand-picard.cci.fr/Scripts/fichiers/articles/pictos/bref/162.jpg?51" vspace="3" align="left" class="bordureBleue">			


Ce nouveau dispositif est destin� � accompagner les porteurs de projet, les demandeurs d'emploi ou b�n�ficiaires de minima sociaux.<br>

                      <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=162" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                      <br><br></td>

                </tr>

                <!-- /actu -->

                


                

                <!-- actu -->

                <tr>

                  <td width="20"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="20" height="16"></td>

                  <td width="365" class="marine10"><span class="bleu10"><strong>D�veloppement durable :</strong></span> <strong>LES EnR, c'est en Picardie Maritime !</strong><br>

	

			<img src="http://littoral-normand-picard.cci.fr/Scripts/fichiers/articles/pictos/bref/161.jpg?51" vspace="3" align="left" class="bordureBleue">			


<font class='orange10'><b>La P�pini�re EnR re�oit les premiers dossiers d�entreprises qui s�installeront d�s cet automne 2009.<br>Sp�cialis�e dans le secteur des Energies Renouvelables, la toute nouvelle P�pini�re est dans ce domaine unique sur le territoire fran�ais.<b></font><br><br>

                      <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=161" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                      <br><br></td>

                </tr>

                <!-- /actu -->

                


                

                <!-- actu -->

                <tr>

                  <td width="20"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="20" height="16"></td>

                  <td width="365" class="marine10"><span class="bleu10"><strong>Entreprise :</strong></span> <strong><b>LE TITRE EMPLOI SERVICE - TESE</b></strong><br>

	

			<img src="http://littoral-normand-picard.cci.fr/Scripts/fichiers/articles/pictos/bref/158.jpg?51" vspace="3" align="left" class="bordureBleue">			


Depuis le 19 mai 2009, le TESE se substitue au ch�que emploi tr�s petites entreprises et au titre emploi-entreprise.<br>Vous �tes d�j� employeur ou vous souhaitez embaucher un salari� prochainement, <br>Vous reculez car vous pensez avoir trop de formalit�s � accomplir,<br>n�h�sitez plus, optez pour le TESE !<br><b>Contact CCI : a.medkour@littoral-normand-picard.cci.fr</b><br>

                      <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=158" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                      <br><br></td>

                </tr>

                <!-- /actu -->

                


                

                <!-- actu -->

                <tr>

                  <td width="20"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="20" height="16"></td>

                  <td width="365" class="marine10"><span class="bleu10"><strong>Communication :</strong></span> <strong>ALERTE !</strong><br>


<b>Vous �tes destinataires de factures en provenance d�organismes portant la d�nomination � CCI � ?</b><br><font class='orange10'><b>Attention ! V�rifiez que ces factures proviennent bien de votre chambre de commerce afin d��viter d��ventuelles escroqueries.</b></font><br><br><b>Un doute ? N�effectuez pas de versement avant d�avoir v�rifi� l�origine de ces factures, demandez conseil � votre CCI<br>au 0820 80 76 00 ou au 03 22 25 47 84</b><br>Contact : JP Le Hir<br>

                      <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=137" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                      <br><br></td>

                </tr>

                <!-- /actu -->

                


                

                <!-- actu -->

                <tr>

                  <td width="20"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="20" height="16"></td>

                  <td width="365" class="marine10"><span class="bleu10"><strong>Entreprise :</strong></span> <strong>VOTRE CCI VOUS INFORME SUR LES DISPOSITIFS DE SOUTIEN AUX ENTREPRISES</strong><br>

	

			<img src="http://littoral-normand-picard.cci.fr/Scripts/fichiers/articles/pictos/bref/129.jpg?51" vspace="3" align="left" class="bordureBleue">			


<b>Composez le 0820 80 76 00<br>Consultez la <a href="http://littoral-normand-picard.cci.fr/fichiers/pdf/158_dispositif_de_soutien_aux_pme.pdf" target="_blank">fiche pratique</a> des dispositifs locaux et de vos interlocuteurs.<br>Nouveaut� Assurance-cr�dit : <a href="http://premier-ministre.gouv.fr/chantiers/plan_relance_economie_1393/assurance_credit_creation_dispositif_63353.html" target="_blank">cr�ation du dispositif CAP+</a><br>La CCI LNP vous soutient en mettant en place une �quipe pour vous renseigner sur les dispositifs du plan de soutien au financement des entreprises mis en place par l�Etat</b><br>

                      <div align="right"><a href="http://littoral-normand-picard.cci.fr/Scripts/actualites_article.php?id=129" class="bleu10">&gt; En savoir +</a><img src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="30" height="14" align="absmiddle"></div>

                      <br><br></td>

                </tr>

                <!-- /actu -->

                

                                

              </table></td>

            <td><br>

                <br>

                <br>

                <br>

                <br>

                <br></td>

            <td valign="top">
			<table border="0" cellspacing="0" cellpadding="0">
<td><a href="http://littoral-normand-picard.cci.fr/Scripts/erapport/index.html"><img src="http://littoral-normand-picard.cci.fr/Scripts/img/tit_eRapport.png" width="133" height="36" border="0"></a></td>

                </tr>

                

                <tr>

                  <td class="marine10">
                   <object type="application/x-shockwave-flash" data="P3DPlayerLite.swf" width="133" height="86">
		<param name="movie" value="/erapport/P3DPlayerLite.swf">
		<param name="id" value="PBI">
		<param name="quality" value="high">
		<param name="wmode" value="transparent">
		<param name="flashvars" value="source=/erapport/RAPPORT-2009-OK.txt&target=erapport/index.html">
		<param name="allowScriptAccess" value="sameDomain">
		</object>
		

			
<!--
                  <br>

<strong>VOTRE CCI VOUS INFORME SUR LES DISPOSITIFS DE SOUTIEN AUX ENTREPRISES</strong><br>

                  <b>Composez le 0820 80 76 00<br>Consultez la <a href="http://littoral-normand-picard.cci.fr/fichiers/pdf/158_dispositif_de_soutien_aux_pme.pdf" target="_blank">fiche pratique</a> des dispositifs locaux et de vos interlocuteurs.<br>Nouveaut� Assurance-cr�dit : <a href="http://premier-ministre.gouv.fr/chantiers/plan_relance_economie_1393/assurance_credit_creation_dispositif_63353.html" target="_blank">cr�ation du dispositif CAP+</a><br>La CCI LNP vous soutient en mettant en place une �quipe pour vous renseigner sur les dispositifs du plan de soutien au financement des entreprises mis en place par l�Etat</b></a>
				  -->
</td>
            </table>
			<br>
			<table border="0" cellspacing="0" cellpadding="0">

                                <TR>
                <TD colspan="2"><IMG src="http://littoral-normand-picard.cci.fr/Scripts/img/cci_pratique.gif" width="133" height="34"></TD>
              </TR>
              <TR>
                <TD colspan="2" bgcolor="#FFFFFF"><IMG src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="16" height="1"></TD>
              </TR>
                  <LINK href="http://littoral-normand-picard.cci.fr/Scripts/css/style.css" rel="stylesheet" type="text/css">

              <TR>

                <TD width="30" align="right" valign="top" bgcolor="#0071BC"><IMG src="http://littoral-normand-picard.cci.fr/Scripts/img/icone_fichier.gif" width="23" height="26" hspace="2" vspace="4"></TD>

  <TD width="103" id="pad6v" bgcolor="#0071BC" class="blanc10v"><A href="http://littoral-normand-picard.cci.fr/Scripts/service_enligne.php" class="blanc10">March�s publics</A>

              </TR>
                  <LINK href="http://littoral-normand-picard.cci.fr/Scripts/css/style.css" rel="stylesheet" type="text/css">

              <TR>

                <TD colspan="2" bgcolor="#FFFFFF"><IMG src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="16" height="1"></TD>

              </TR>

              <TR>

                <TD width="30" align="right" valign="top" bgcolor="#0071BC"><IMG src="http://littoral-normand-picard.cci.fr/Scripts/img/icone_fichier.gif" width="23" height="26" hspace="2" vspace="4"></TD>

                <TD width="103" valign="middle" bgcolor="#0071BC" class="blanc10v" id="pad6v"><A href="http://www.chambersign.fr" target="_blank" class="blanc10">Chambersign</A></TD>

              </TR>
                  <LINK href="http://littoral-normand-picard.cci.fr/Scripts/css/style.css" rel="stylesheet" type="text/css">

              <TR>

                <TD colspan="2" bgcolor="#FFFFFF"><IMG src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="16" height="1"></TD>

              </TR>

              <TR>

                <TD width="30" align="right" valign="top" bgcolor="#0071BC"><IMG src="http://littoral-normand-picard.cci.fr/Scripts/img/icone_buste.gif" width="23" height="26" hspace="2" vspace="4"></TD>

                <TD width="103" bgcolor="#0071BC" class="blanc10v" id="pad6v"><A href="http://littoral-normand-picard.cci.fr/Scripts/quifaitquoi.php" class="blanc10">Qui fait quoi ?</A></TD>

              </TR>
            </table>
		
			<table width="133" border="0" cellspacing="0" cellpadding="0">
 				<tr>

                  <td></td>

                </tr>                <TR>
                <TD colspan="2"><IMG src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="16" height="20"></TD>
              </TR>
			   <TR>

                <TD colspan="2"><IMG src="http://littoral-normand-picard.cci.fr/Scripts/img/newsletter.gif" width="133" height="34"></TD>

              </TR>
 				<TR>

                <TD colspan="2" bgcolor="#FFFFFF"><IMG src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="16" height="1"></TD>

              </TR>
             				</table>
              <table width="133" border="0" cellspacing="0" cellpadding="10">
 				
              
			 
              <TR>
                <TD colspan="2" bgcolor="#65b822" class="blanc10v" ><a class="blanc10" href="javascript:popupSansScroll('popup_inscription_newsletter.php',550,450)">Inscrivez-vous d&egrave;s maintenant</a> pour recevoir les infos de votre CCI</TD>
              </TR>				</table>

		
				<table width="133" border="0" cellspacing="0" cellpadding="0">
                <tr>

                  <td></td>

                </tr>              <TR>
                <TD colspan="2"><IMG src="http://littoral-normand-picard.cci.fr/Scripts/img/_dot.gif" width="16" height="20"></TD>
              </TR>
              <TR>
                <TD colspan="2" bgcolor="#FF5400"><A href="javascript:popupSansScroll('maison_picarde.html',745,500)"><IMG src="http://littoral-normand-picard.cci.fr/Scripts/img/maison_picarde_tit.gif" width="133" height="43" border="0"></A></TD>
              </TR>
              <TR>
                <TD colspan="2" align="center" bgcolor="#FF5400"><A href="javascript:popupSansScroll('maison_picarde.php',745,500)"><IMG src="http://littoral-normand-picard.cci.fr/Scripts/img/maison_picarde_pic.gif" width="133" height="89" border="0"></A></TD>
              </TR>
              <TR>
                <TD colspan="2" bgcolor="#FF5400"><A href="javascript:popupSansScroll('maison_picarde.php',745,500)"><IMG src="http://littoral-normand-picard.cci.fr/Scripts/img/maison_picarde_bt.gif" width="133" height="36" border="0"></A></TD>
              </TR>
                <tr>

                </tr>

              </table>

              <br><a href="http://www.pme.gouv.fr/commerce/index.html" target="_blank"></a></td>

            </tr>

        </table>

          <br>

<!-- /contenu --></td>

      </tr>

      <tr>

        <td></td>

        </tr>

    </table></td>

  </tr>

<LINK href="http://littoral-normand-picard.cci.fr/Scripts/css/style.css" rel="stylesheet" type="text/css">
  <TR>
    <TD height="18" align="center" bgcolor="#003366" class="blanc10v"><A href="http://littoral-normand-picard.cci.fr/Scripts/plan_site.php" class="blanc10lien">Plan du site</A> | <A href="http://littoral-normand-picard.cci.fr/Scripts/credits.php" class="blanc10lien">Cr&eacute;dits</A> | <A href="http://littoral-normand-picard.cci.fr/Scripts/mentions.php" class="blanc10lien">Mentions l&eacute;gales</A></TD>
  </TR>
</table>

<br>

</body>

</html>