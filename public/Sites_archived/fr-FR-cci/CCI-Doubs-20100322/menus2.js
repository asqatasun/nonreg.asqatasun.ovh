
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator)
  	{
	  if ((appName=="Netscape")&&(parseInt(appVersion)==4))
	  	{
	    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; 
		}
	}
  else if(innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH)
  	{
	 location.reload();
	}
}
MM_reloadPage(true);

function BW_centerLayers() { //v4.1.1
	if (document.layers || document.all || document.getElementById)
		{
		onresize = BW_reload;

		winWidth = (document.all)?document.body.clientWidth:window.innerWidth;
		winHeight = (document.all)?document.body.clientHeight:window.innerHeight;
		}
}

function BW_reload()	{
	location.reload();
}