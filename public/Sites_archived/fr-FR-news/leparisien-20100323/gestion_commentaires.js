// tableau contenant le nombre de commentaires pour un id_sans_version : c[id_sans_version]=nombre;
nb_com=new Array();
com_actif=true;

// appel du fichier js en évitant le cache
function load_com_js(j, param){
	document.write('<script type="text/javascript" src="'+j+"?r=" + Math.random()+"&"+param+'"><\/script>');
}

function valid_email(champs_mail)
{
	var email =    /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
	if(!$j("#"+champs_mail).val().match(email))
	{
		return "";	
	}
	else
	{
		return $j("#"+champs_mail).val();
	}
}

function valid_ajout_com_nolog()
{
	if ($j("#reac-nolog-charte:checked").val() != "true") {
		alert_box('<p><strong class="coul-rouge">Charte de modération</strong></p><p>Pour poster une réaction, vous devez, au préalable, accepter la <a href="/reactions/charte.php" target="_blank">charte de modération</a></p>');
		return false;
	}
	else if ($j("#reac-nolog-reac").val() == "" || $j("#reac-nolog-pseudo").val() == "" || valid_email('reac-nolog-email') == "")
	{
		alert_box('<p><strong class="coul-rouge">Veuillez comptéter le(s) champ(s) suivant(s) :</strong></p><p>Pseudo<br/>Email<br/>Votre réaction</p>');
		return false;
	}
	else
	{
		send_com('form-reac-nolog');
		return false;
	}
}

function valid_ajout_com_log()
{
	if ($j("#reac-log-charte:checked").val() != "true") {
		alert_box('<p><strong class="coul-rouge">Charte de modération</strong></p><p>Pour poster une réaction, vous devez, au préalable, accepter la <a href="/reactions/charte.php" target="_blank">charte de modération</a></p>');
		return false;
	}
	else if ($j("#reac-log-reac").val() == "")
	{
		alert_box('<p><strong class="coul-rouge">Veuillez comptéter le(s) champ(s) suivant(s) :</strong></p><p>Votre réaction</p>');
		return false;
	}
	else
	{
		send_com('form-reac-log');
		return false;
	}
}

function send_com(valid_formulaire){
	
	if ((getCookie('token'))!=null){
		if ($j(".logged").length) {	
			$j.ajax({
			type: "POST",
			url: "/commentaires/get_datas.php",
			async: false,
			success: function(msg){
				if(msg == "1") {
					tokenmail=getCookie('tokenmail').split('|');
					$j("#reac-log-pseudo").val(tokenmail[1]);
					$j("#reac-log-email").val(tokenmail[0]);
					
				}
			}
			});
		}
	}
	
	$j.ajax({
		type: "POST",
		url: "/commentaires/ajout_com.php",
		data: $j("#"+valid_formulaire).serialize(),
		success: function(msg){
			if(msg == "") {
				alert_box('<p><strong class="coul-rouge">Votre réaction a bien été envoyée</strong></p><p>Elle sera modérée et publiée dans les 15 prochaines minutes, conformément à la <a href="/reactions/charte.php">charte de modération</a>.</p><p style="text-align: right;"><a href="#" onclick="javascript:$j(\'#alert_box\').dialog(\'close\');">Revenir à l\'article</a> <img src="/icons/f_tous.png" class="f-tous"/></p>');
				$j("#"+valid_formulaire).each(function() {
					this.reset();
				});
			}
			else
				alert_box(msg);
		}
	});
}

function valid_connexion(){
	if ($j("#reac-connecte-id").val() == "" || $j("#reac-connecte-pass").val() == "")
	{
		alert_box('<p><strong class="coul-rouge">Veuillez comptéter le(s) champ(s) suivant(s) :</strong></p><p>Identifiant</p><p>Mot de passe</p>');
		return false;
	}
}

function valid_retrieve(){
	if ($j("#retrieve-password-email").val() == "")
	{
		$j("#emptyconfirm").show();
		return false;
	}
	else
	{
		$j(".popupAuthError").hide();
		$j(".popupAuthMail").hide();
	}
}

//Alert box / dialog jquery
function alert_box(m)
{
	$j('#alert_box').html(m);
	$j('#alert_box').dialog('open');
}

// renvoie le nombre de commentaires
function get_nb_com(id)
{
	if(typeof(nb_com[id])=='number')
	{
		if(nb_com[id]==0)
			return '';
		else if(nb_com[id]==1)
			return '1 réaction';
		else
			return nb_com[id]+' réactions';
	}
	else
		return '';
}

// affichage du nombre de commentaires
function w(id){
	document.write(get_nb_com(id));
}

// affichage du nombre de commentaires pour les articles
function wa(id, etat){
	if(get_nb_com(id)!="")
		document.write('<a href="#reactions-article" class="nb-reac-rouge">'+get_nb_com(id)+'</a>');
	else if( etat=='1' )
	{
		$j('.nb-reac-end').hide();
		$j('.nb-reac').hide();
	}
		
}

// affichage du nombre de commentaires pour les rubriques
function wr(id, href){
  if(get_nb_com(id)!="")
		document.write('<a class="nb-reac" href="'+href+'">'+get_nb_com(id)+'</a>');
}

function wrclass(id, href,classe){
  if(get_nb_com(id)!="")
                document.write('<a class="'+classe+'" href="'+href+'">'+get_nb_com(id)+'</a>');
}

// affichage du nombre de commentaires / QDJ pour les rubriques
function wrqdj(id, href){
  if(get_nb_com(id)!="")
		document.write('<a class="nb-reac-kes" href="'+href+'">'+get_nb_com(id)+'</a> |&nbsp;');
}
