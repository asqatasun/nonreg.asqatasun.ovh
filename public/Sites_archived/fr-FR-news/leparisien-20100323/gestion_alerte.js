//Alert box / dialog jquery
function alert_box(m)
{
	 $j('#alert_box').html(m);
	 $j('#alert_box').dialog('open');
} 

function valid_alerte()
{
	var email = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
	if ((($j("#email").val() == "ADRESSE EMAIL")||($j("#email").val() == ""))&&(($j("#recherche").val() == "VILLE ou CODE POSTAL")||($j("#recherche").val() == ""))) {
		alert_box('<p><strong class="coul-rouge">Alerte E-mail</strong></p><br/><p>Veuillez renseigner votre email.<br/>Veuillez renseigner votre ville ou code postal.</p>');
		return false;	
	}
	else if (($j("#email").val() == "ADRESSE EMAIL")||($j("#email").val() == "")) {
		alert_box('<p><strong class="coul-rouge">Alerte E-mail</strong></p><br/><p>Veuillez renseigner votre email.</p>');
		return false;
	}
	else if(!($j("#email").val().match(email)))
	{
		alert_box('<p><strong class="coul-rouge">Alerte E-mail</strong></p><br/><p>Votre email n\'est pas valide.</p>');
		return false;
	}
	else if (($j("#recherche").val() == "VILLE ou CODE POSTAL")||($j("#recherche").val() == "")) {
		alert_box('<p><strong class="coul-rouge">Alerte E-mail</strong></p><br/><p>Veuillez renseigner votre ville ou code postal.</p>');
		return false;
	}
}

function valid_alerte_310()
{
	var email = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
	if ((($j("#email_310").val() == "ADRESSE EMAIL")||($j("#email_310").val() == ""))&&(($j("#recherche_310").val() == "VILLE ou CODE POSTAL")||($j("#recherche_310").val() == ""))) {
		alert_box('<p><strong class="coul-rouge">Alerte E-mail</strong></p><br/><p>Veuillez renseigner votre email.<br/>Veuillez renseigner votre ville ou code postal.</p>');
		return false;	
	}
	else if (($j("#email_310").val() == "ADRESSE EMAIL")||($j("#email_310").val() == "")) {
		alert_box('<p><strong class="coul-rouge">Alerte E-mail</strong></p><br/><p>Veuillez renseigner votre email.</p>');
		return false;
	}
	else if(!($j("#email_310").val().match(email)))
	{
		alert_box('<p><strong class="coul-rouge">Alerte E-mail</strong></p><br/><p>Votre email n\'est pas valide.</p>');
		return false;
	}
	else if (($j("#recherche_310").val() == "VILLE ou CODE POSTAL")||($j("#recherche_310").val() == "")) {
		alert_box('<p><strong class="coul-rouge">Alerte E-mail</strong></p><br/><p>Veuillez renseigner votre ville ou code postal.</p>');
		return false;
	}
}

function envoi_alerte(message)
{
	alert_box('<p><strong class="coul-rouge">Alerte E-mail</strong></p><br/><p>'+message+'</p>');
}
function urlize(valeure) {
	keyword = valeure.toLowerCase();
	keyword = keyword.replace(/[èéêë]/g, "e");
	keyword = keyword.replace(/[áâàäã]/g, "a");
	keyword = keyword.replace(/[ùúûü]/g, "u");
	keyword = keyword.replace(/[ôõóò]/g, "o");
	keyword = keyword.replace(/[ç]/g, "c");
	var exp_reg1 = /, /g; keyword = keyword.replace(exp_reg1, "-");
	var exp_reg2 = / /g; keyword = keyword.replace(exp_reg2, "-");
	var exp_reg3 = /,/g; keyword = keyword.replace(exp_reg3, "-");
	return keyword;
};
function recherche_villes()
{
	if (($j("#recherche_ville").val() == "VILLE ou CODE POSTAL")||($j("#recherche_ville").val() == "")) {
		alert_box('<p><strong class="coul-rouge">Régionales 2010 : Recherche Ville</strong></p><br/><p>Veuillez renseigner votre ville ou code postal.</p>');
		return false;
	}
	else{
		$j("#recherche_ville").val(urlize($j("#recherche_ville").val()));
	}
	
}
function recherche_villes_470()
{
	if (($j("#recherche_ville_470").val() == "VILLE ou CODE POSTAL")||($j("#recherche_ville_470").val() == "")) {
		alert_box('<p><strong class="coul-rouge">Régionales 2010 : Recherche Ville</strong></p><br/><p>Veuillez renseigner votre ville ou code postal.</p>');
		return false;
	}
	else{
		$j("#recherche_ville_470").val(urlize($j("#recherche_ville_470").val()));
	}
}
