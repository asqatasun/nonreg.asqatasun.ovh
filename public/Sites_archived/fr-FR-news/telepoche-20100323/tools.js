$(document).ready(
	function()
	{
		$('#email').focus(
			function ()
			{
				if ($('#email').attr("value") == "Saisissez votre email")
				{
					$('#email').attr("value", "");
				}
			}
		);
		$('#email').blur(
			function ()
			{
				if ($('#email').attr("value") == "")
				{
					$('#email').attr("value", "Saisissez votre email");
				}
			}
		);
		
		$('#selection tr').hover(
			function ()
			{
				$(this).css("background-color", "#FFBA18");
			},
			function ()
			{
				$(this).css("background-color", "");
			}
		);
		
		$('#radio_1').click(
			function ()
			{
				$("#user_newsletter_reason_label").css("display", "none");
				$("#user_newsletter_reason_select").css("display", "none");
			}
		);
		$('#radio_2').click(
			function ()
			{
				$("#user_newsletter_reason_label").css("display", "block");
				$("#user_newsletter_reason_select").css("display", "block");
			}
		);
		
		$('#radio_3').click(
			function ()
			{
				$("#user_partner_email_reason_label").css("display", "none");
				$("#user_partner_email_reason_select").css("display", "none");
			}
		);
		$('#radio_4').click(
			function ()
			{
				$("#user_partner_email_reason_label").css("display", "block");
				$("#user_partner_email_reason_select").css("display", "block");
			}
		);
	}
);