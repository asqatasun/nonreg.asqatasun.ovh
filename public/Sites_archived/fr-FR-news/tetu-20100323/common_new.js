/*********************************************************
 * common_new.js - v1.0
 ********************************************************/
$(document).ready(function(){
	fixPNG();
	initNavigation();
	fixCSS();
	initClearInput();
	initLogPanel();
	// initTetuX();
	initPopupRadio();
	initSearchField();
	initSwitchGenre();	// Header
	initSwitchTetue();	// Switcher
	initSearchButtons();
	
	// Initialisation du sondage
	if (document.getElementById('sondage')!=null) {
		var so = new SWFObject(base_url+"/theme/front/swf/sondage.swf", "sondage", "316", "458", "8", "#000");
		so.addParam("allowScriptAccess", "always");
		so.addParam("quality", "high");
	    so.addParam("scale", "noscale");
	    so.useExpressInstall(base_url+'/files/swf/expressinstall.swf');
		so.write("sondage");
	}
	
	if ($('.partenariat').html()==null || $('.partenariat').html().indexOf('empty.gif')!=-1) { $('.partenariat').attr('style', 'display:none;'); }
});

/*********************************************************
 * fixPNG
 * Corrige la transparence des PNG sous IE6 et inferieur
 ********************************************************/
function fixPNG(){
	jQuery.ifixpng(base_url+"/theme/front/img/interface/pixel.gif");
	jQuery("#populaire-inner .populaire-inner-title img").ifixpng();
	jQuery(".clubbing h3 img").ifixpng();
	jQuery(".sidebar_left_2cols .avis").ifixpng();
	jQuery("#navigation .sous_categories li.last a").ifixpng();
	jQuery("#header .switcher img").ifixpng();
}

/*********************************************************
 * initNavigation
 * Gestion du menu de navigation principale en haut de page
 ********************************************************/
function initNavigation(){
	// corrections de styles
	if($("#navigation .sous_categories").hasClass("tetue")){
		$("#navigation .sous_categories ul:gt(0) li:first-child").css({"background":"none", "padding-left":0});
		$("#navigation .categories li:gt(0) a").corner("3px");
		// Ajout pour nouveau menu (@see blogs)
		$("#navigation").corner("8px");
		$("#navigation .sous_categories").corner("3px");
	}
	menuTimer = null;
	// affichage des sous categories au rollover
	$("#navigation > .categories > li").hover(function() { //mouse in
		selectedCat = $(this);
		menuTimer = setInterval(function(){
			$("#navigation > .categories > li").removeClass("hover");
			$(selectedCat).addClass("hover");
			$("#navigation > .sous_categories ul").hide();
			var catToShow = $(selectedCat).attr("id");
			$("."+catToShow).show();
			if($("#navigation .sous_categories").hasClass("tetue")){
				$("#navigation").removeClass().addClass("color_"+catToShow);
			}
			clearInterval(menuTimer);
		}, 50);
	}, function(){ /*mouse out */
		clearInterval(menuTimer);
	});
}

/*********************************************************
 * fixCSS
 * Ajuste les styles du bloc populaire 24h
 ********************************************************/
function fixCSS(){
	/* Popolaire 24h */
	$("#populaire-inner.left .content_left .block:last").css({"border-bottom-width":0});
	$("#populaire-inner.left .content_center .block:last").css({"border-bottom-width":0});
	$("#populaire-inner.left .content_right .block:last").css({"border-bottom-width":0});
	$("#populaire .right .content_center .block:last").css({"border-bottom-width":0});
	/* Clubbing */
	$(".sidebar_right_2cols .clubbing .content ul.event:last").css({"border-bottom-width":0});
	$(".sidebar_right_2cols  .content div li:last").css({"background":"none"});
	/* Localisation */
	gmapTimer = setInterval(function(){
		$("#gmap_container > div:eq(2)").css({"text-align":"center", "background":"#fff"});
		$("#gmap_container > div:eq(2) span").css({"display":"block"});
		clearInterval(gmapTimer);
	}, 3000);
}

/*********************************************************
 * clearInput
 * efface le contenu d'un champ au clic
 ********************************************************/
function initClearInput(){
	var $inputNewsletter = $(".sidebar_right_2cols .newsletter input[type=text]");
	var $inputSearch = $(".sidebar_right_2cols .search textarea");
	clearInput($inputNewsletter, $inputNewsletter.val());
	clearInput($inputSearch, $inputSearch.val());
}

/*********************************************************
 * clearInput
 * efface le contenu d'un champ au clic
 ********************************************************/
function clearInput(element, textToClear){
	element.click(function(){
		if($(this).val() == textToClear){
			$(this).val("");
		}
	});
}

/*********************************************************
 * initLogPanel
 * gestion de l'apparition des champs de login
 ********************************************************/
function initLogPanel(){
	$("#bloc_login .connect").click(function(){
		$("#bloc_login").hide();
		$("#bloc_login_form").fadeIn();
		return false;
	});
	$("#bloc_login_form .oublier").click(function(){
		$("#bloc_login_form").hide();
		$("#bloc_motdepasse_form").fadeIn();
		return false;
	});
}
 
/*********************************************************
 * initSwitchTetue
 * passage entre la versin fille et garcon
 ********************************************************/
function initSwitchTetue(){
	// fermeture switcher
	$("#header .switcher .close").click(function(){$(this).parents(".switcher").slideUp(200);});
	// Click sur le choix garcons/filles
	$('.switcher .garcons a').click(initSwitchGarcons);
	$('.switcher .filles a').click(initSwitchFilles);
	$('.switcher .img_garcons a').click(initSwitchGarcons);
	$('.switcher .img_filles a').click(initSwitchFilles);
}

function initSwitchGarcons() {
	// Set genre 'G'
	$.ajax({
		url: base_url + '/filtre_genre/fixe_genre',
		type: "POST",
		data: '&garcons=on&redirect=false',
		error: function(){},
		beforeSend: function(){},
		success: function(){
			document.location.reload();
		}
	});
	return false;
}

function initSwitchFilles() {
	// Set genre 'F'
	$.ajax({
		url: base_url + '/filtre_genre/fixe_genre',
		type: "POST",
		data: '&filles=on&redirect=false',
		error: function(){},
		beforeSend: function(){},
		success: function(){
			document.location.reload();
		}
	});
	return false;
}

/**
 * Affiche la bannière Têtu X dans la colonne de droite
 * - affichage si public est M ou G
 */
/*
function initTetuX() {
	if (document.getElementById('tetux')!=null) {
		var so = new SWFObject(base_url+"/theme/front/swf/tetux.swf", "tetux", "316", "370", "8", "#000");
		so.addParam("allowScriptAccess", "always");
		so.addParam("quality", "high");
	    so.addParam("scale", "noscale");
	    // so.addVariable("clicktag", "actualites/culture/ttu-vous-invite-a-lavant-premiere-d-harvey-milk-14074");
	    so.useExpressInstall(base_url+'/files/swf/expressinstall.swf');
		so.write("tetux");
	}
}
*/

/*********************************************************
 * initPopupRadio
 * ouvre une popup avec tetu radio
 ********************************************************/
function initPopupRadio(){
	// Si on est en campagne d'affichage, le lien sur la bannière "top" n'ouvre PAS la Radio Têtu
	var habillage = (document.getElementById('gouttiere_g')!=null && document.getElementById('gouttiere_d')!=null);
	if (!habillage) {
		/*
		$("#ads_top a:last").click(function(){
			window.open (base_url+"/radio/", "goom_radio", config="height=320, width=320");
			return false;
		});
		*/
		
		$('.tetu_radio a').click(function() {
			window.open (base_url+"/radio/", "goom_radio", config="height=320, width=320");
			return false;
		});
	}
}

/**
 * Capture l'événement "touche entrée" pour valider le formulaire
 * 
 * @name	initSearchField
 * @author	Rémy Vuong <remy@upian.com>
 * @version	1.0
 * @date	2009-02-27
 * @return	bool false
 */
function initSearchField() {
	$('.search form textarea').keypress(function(e){
		if (e.which==13) {
			$('.search form').submit();
			return false;
		}
	});
	return false;
}

/**
 * Permute entre les sections filles/garçons
 * 
 * @name	initSwitchGenre
 * @author	Rémy Vuong <remy@upian.com>
 * @date	2009-04-23
 * @version	1.0
 * @return	bool false
 */
function initSwitchGenre() {
	$('.switch').click(function(){
		var target = $(this).html();
		if (target.indexOf('filles') != -1) {
			$.ajax({
				url: base_url + '/filtre_genre/fixe_genre',
				type: "POST",
				data: '&filles=on',
				error: function(){
					document.location = base_url + '/accueil/index_fille/';
					return false;
				},
				beforeSend: function(){},
				success: function(){
					document.location = base_url;
				}
			});
		}
		else {
			$.ajax({
				url: base_url + '/filtre_genre/fixe_genre',
				type: "POST",
				data: '&garcons=on',
				error: function(){
					document.location = base_url + '/accueil/index_garcon/';
					return false;
				},
				beforeSend: function(){},
				success: function(){
					document.location = base_url;
				}
			});
		}
		return false;
	});
}

/**
 * Initialise le comportement hover du bouton du formulaire de recherche
 * 
 * @name	initSearchButtons
 * @author	Rémy Vuong <remy@upian.com>
 * @date	2009-05-11
 * @version	1.0
 * @return	bool false
 */
function initSearchButtons() {
	$('.search form input').hover(
		function(){
			var filles = ($(this).attr('src').indexOf('tetue')!=-1);
			if (filles) { $(this).attr('src', base_url+'/theme/front/img/interface/bt_rechercher_tetue_roll.png'); }
			else { $(this).attr('src', base_url+'/theme/front/img/interface/bt_rechercher_roll.png'); }
		},
		function () {
			var filles = ($(this).attr('src').indexOf('tetue')!=-1);
			if (filles) { $(this).attr('src', base_url+'/theme/front/img/interface/bt_rechercher_tetue.png'); }
			else { $(this).attr('src', base_url+'/theme/front/img/interface/bt_rechercher.png'); }
		}
	);
	
	return false;
}

/*********************************************************
 * CSS Browser Selector v0.2.9
 * Rafael Lima (http://rafael.adm.br)
 * http://rafael.adm.br/css_browser_selector
 * License: http://creativecommons.org/licenses/by/2.5/
 * Contributors: http://rafael.adm.br/css_browser_selector#contributors
 ********************************************************/
var css_browser_selector = function() {var ua=navigator.userAgent.toLowerCase(),is=function(t){return ua.indexOf(t) != -1;},h=document.getElementsByTagName('html')[0],b=(!(/opera|webtv/i.test(ua))&&/msie\s(\d)/.test(ua))?('ie ie'+RegExp.$1):is('firefox/2')?'gecko ff2':is('firefox/3')?'gecko ff3':is('gecko/')?'gecko':is('opera/9')?'opera opera9':/opera\s(\d)/.test(ua)?'opera opera'+RegExp.$1:is('konqueror')?'konqueror':is('chrome')?'chrome webkit safari':is('applewebkit/')?'webkit safari':is('mozilla/')?'gecko':'',os=(is('x11')||is('linux'))?' linux':is('mac')?' mac':is('win')?' win':'';var c=b+os+' js'; h.className += h.className?' '+c:c;}();
