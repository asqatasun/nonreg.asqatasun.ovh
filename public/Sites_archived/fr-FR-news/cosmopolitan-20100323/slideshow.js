/**************************************************************

 Script : Slide Show base
 Version : 1.0
 Authors : Raphael Chemin

**************************************************************/

var slideShowBase = new Class({

	 initialize: function(options) {
	 
	 	var conteneur = new Element('div');
		conteneur.injectInside($('slideshow'));
		
		this.linksConteneur = new Element('a');
		this.linksConteneur.injectInside(conteneur);
		this.linksConteneur.setAttribute('href', $('thumbnail-1').getParent().href);
		
		this.imagesHolder = new Element('img');
		this.imagesHolder.setStyles({position:'absolute'});
	 	this.imagesHolder.injectInside(this.linksConteneur);
	 	this.imagesHolder.setAttribute('src', imagesHD[0]);
		this.textHolder = new Element('p').addClass('titreSlide');
		this.textHolder.setStyles({
					'background-color':'white',
					display: 'block',
					position:'absolute',
					visibility: 'visible',
					'z-index':'1000'
				});
		this.textHolder.setOpacity(0.8);		
		this.textHolder.injectInside(this.linksConteneur);
	 	this.textHolder.innerHTML = $('thumbnail-1').getParent().innerHTML;
	 	
		$$('#thumbnail .slideshowThumbnail img').each(function(el, i){
			el.addEvent('mouseenter',function(){
				this.linksConteneur.setAttribute('href', el.getParent().getParent().href);
				this.imagesHolder.setAttribute('src', imagesHD[i]);
				this.textHolder.innerHTML = $('thumbnail-'+(i+1)).getParent().innerHTML;
					
			}.bind(this,el));
		}.bind(this));
	}
});