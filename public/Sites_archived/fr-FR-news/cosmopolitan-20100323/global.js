/*  ---------------------------------------------------------------------------------- */
/* ---------------------------- SET HABILLAGE CLICKABLE ------------------------------ */
	function setHabillage(l, r)
	{
		$(document.body).setStyle('cursor','pointer');
		var liste = $$('#global-header-content, #page-content, #global-footer-content');
		liste.each(function(emplacement){
			emplacement.setStyle('cursor','auto');
			
		});
		var coordinates = $('page-content').getPosition();
		var sizes = $('page-content').getSize();
		
		$(document.body).addEvent('click', function(event){
			if( event.client.x < coordinates.x && l!='') window.open(l,'pop_under','toolbar=1,location=1,directories=1,status=1,menubar=1,scrollbars=yes,resizable=1,width=1024,height=1024');
			if( event.client.x > coordinates.x+sizes.x && r!='') window.open(r,'pop_under','toolbar=1,location=1,directories=1,status=1,menubar=1,scrollbars=yes,resizable=1,width=1024,height=1024');
		});
	}


function addFavorites(title) {
	if (window.sidebar) { // Mozilla Firefox Bookmark
		window.sidebar.addPanel(title, location.href,"");
	} else if( window.external ) { // IE Favorite
		window.external.AddFavorite( location.href, title); }
	else if(window.opera && window.print) { // Opera Hotlist
		return true; 
	}
 }
 
function verticalAlign(wrapper,content)
{
	if($(wrapper) && $(content)) {
		var calculMargin = ($(wrapper).offsetHeight/2) - ($(content).offsetHeight/2);
		if(calculMargin>0)$(content).setStyle('marginTop', calculMargin); 
	}
}

function checkInputText(champ,libelleErreur)
{
	var message = "";
	var res = IsEmpty(champ);
	if(res)
		message = libelleErreur+"<br />";
	return message;
}

function checkEmail(champ,libelleErreur)
{	
	var message = "";	
	var res = IsMail(champ);
	if(!res)
		message = libelleErreur+"<br />";
	return message;
}

function IsEmpty(champ){
    return (champ.trim()=='');
}

function IsMail(champ){
	var filter = /^[a-z.A-Z0-9_-]+@[a-z.A-Z0-9_-]{2,}[.][a-zA-Z]{2,3}$/
	return(filter.exec(champ));
}
function scrollAuto(div)
 {
 var zone = 0+document.body.scrollTop+$(div).offsetTop;
 window.scroll(0,zone);
 }
 
function enregistre_clic(campagne,emplacement)
{
	clic_img = new Image();
	clic_img.src = "/clic.asp?emplacement=" + escape(emplacement) + "&campagne=" + escape(campagne);
	return;
}


function checkField(field,force)
{
	if(force || $(field).value)$(field).getParent().className='input';
}


/* -- COOKIE -- */
function getCookieVal (offset) {
      var endstr = document.cookie.indexOf (";", offset);
      if (endstr == -1)
        endstr = document.cookie.length;
      return unescape(document.cookie.substring(offset, endstr));
    }

function GetCookie (name) {
      var arg = name + "=";
      var alen = arg.length;
      var clen = document.cookie.length;
      var i = 0;
      while (i < clen) {
        var j = i + alen;
        if (document.cookie.substring(i, j) == arg)
          return getCookieVal (j);
    	i = document.cookie.indexOf(" ", i) + 1;
        if (i == 0) break; 
      }
      return null;
    }
function SetCookie (name,value,expires,path,domain,secure) {
      document.cookie = name + "=" + escape (value) +
        ((expires) ? "; expires=" + expires.toGMTString() : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
    }
 function DeleteCookie (name,path,domain) {
      if (GetCookie(name)) {
        document.cookie = name + "=" +
          ((path) ? "; path=" + path : "") +
          ((domain) ? "; domain=" + domain : "") +
          "; expires=Thu, 01-Jan-70 00:00:01 GMT";
      }
    }

    
function writeCookie(nomCookie,dureeCookie) {
	date_expire = new Date();
	date_expire.setTime(date_expire.getTime() + (dureeCookie*24*60*60*1000));
	SetCookie(nomCookie,"1",date_expire);
}
		
function showPano(timer, cookieOn, dureeCookie)
{
	var nomCookie = "cosmo_pubPano";
	var pubPano = new Fx.Slide('flashBannerTop',{duration: 1500,mode: 'vertical'});
	var OS   = BrowserDetect.OS;	//detection de l'os
	var brow = BrowserDetect.browser;//detection du navigateur
	
	if(brow == "Safari")
	{
		$('toggle').style.visibility='hidden';
	}
	var type;
	switch (OS)
	{
		case 'Windows':
			type = '1';
		break;
		
		default:
			type = '0';
		break;
	}

	togglePub(type,pubPano);
	
	if(cookieOn == 1) {	//cookie
		var hasCookie = GetCookie(nomCookie);
		if(hasCookie == null){ //il y a un cookie
			writeCookie(nomCookie,dureeCookie);
			$('flashBannerTop').style.height="360px";
			showPub(type,timer,pubPano);
		} 
		else {
			pubPano.hide();	
			$('flashBannerTop').style.margin = "-351px 0px 0px 0px";
			$('flashBannerTop').style.height="360px";
		}
	}
	else {
		$('flashBannerTop').style.height="360px";
		showPub(type,timer,pubPano);
	}
	
}
	
	
/* ----------------------------- COPYRIGHT ------------------------------ */
	window.addEvent('domready', function(){
		if($('creditphoto-txt'))
		{
			var myVerticalSlide = new Fx.Slide('creditphoto-txt', {mode: 'horizontal', duration:100});
		
			var size = $('img-credit').offsetHeight;
			
			$('creditphoto-lien').getParent().setStyle("margin-top", size-20);
			$('creditphoto-lien').addEvent('domready', function(e){
				myVerticalSlide.hide();
			});
			
			$('creditphoto-lien').addEvent('mouseover', function(e){
				e.stop();
				myVerticalSlide.slideIn();
				myVerticalSlide.slideOut.delay(1000, myVerticalSlide);
			});
			
			$('creditphoto-lien').addEvent('mouseout', function(e){
				e.stop();
				myVerticalSlide.slideOut();
			});
		}
	});
	
	
/* ---------------------------------------------------------------------- */


		
function togglePub(type,pubPano)
{
	switch (type)
	{
		case '1':
			$('toggle').addEvent('click', function(e){
			$('flashBannerTop').style.height="360px";
			e = new Event(e);
			pubPano.toggle();
			e.stop();});
		break;
		default:
			$('toggle').addEvent('click', function(e){
			e = new Event(e);
			if($('flashBannerTop').style.margin == "0px 0px 0px 0px"){
				pubPano.hide();
			}
			else{
				$('flashBannerTop').style.height="360px";
				$('flashBannerTop').style.margin = "-351px 0px 0px 0px";
				pubPano.show();
			}
			e.stop(); });						
		break;
	}
}

function showPub(type,timer,pubPano)
{
	switch (type)
	{
		case '1':
			pubPano.toggle.delay(timer, pubPano);
			setTimeout(function() {
		  	}, (timer+1500));
		break;
		
		default:
			pubPano.hide.delay(timer, pubPano);
			setTimeout(function() {
			}, (timer));
		break;
	}
}

/* ---------------------------- COLLECTION VOTE ------------------------------ */

function vote(quest, rep, valeur,nbMax)
{
	var champ = $('action-vote').getElement('input[id=vote-'+rep+']');
	if(!champ)
	{
		var champ_vide = $('action-vote').getElement('input[value=0]');
		if(champ_vide)
		{
			champ_vide.set('id', 'vote-'+rep);
			champ_vide.set('name', 'vote['+quest+']['+rep+']');
			champ_vide.set('value', showNbVote(nbMax)-1);
			return 1;
		}
	}
	return 0;
}

function supprimerVote(id)
{
	var champ = $('action-vote').getElement('input[id=vote-'+id+']');
	if(champ)
	{
		champ.set('id','');
		champ.set('name','');
		champ.set('value',0);
	}
}

function showNbVote(nbMax)
{
	var champs = $('action-vote').getElements('input[value!=0]');
	return nbMax - (champs.length - 1);
}

function refreshFormVote(id_collection)
{
	Cookie.dispose('voteDatas_'+id_collection);
	location.reload(true);
}

function refreshFormVote2(id_collection)
{
	Cookie.dispose('voteDatas_'+id_collection);
}

function loadVote(id_collection, nbMax)
{
	if(myCookie = Cookie.read('voteDatas_'+id_collection))
	{
		lc_votes = JSON.decode(myCookie);
		/*console.log(lc_votes);*/
		for (i=0 ; i < lc_votes.length ; i++)
		{
			var champ = $('action-vote').getElement('input[value=0]');
			if(champ)
			{
				champ.set('id', lc_votes[i].id);
				champ.set('name', lc_votes[i].name);
				champ.set('value', lc_votes[i].value);
				
				var obj = $('element-'+lc_votes[i].id);
				if($chk($(obj)))
				{
					var j = i + 1;
					var nb = nbMax - (j - 1);
					if(nb < 10)obj.getElement(".nb").setStyle('margin-left','18px');
			 		obj.getElement(".nb").set('text', nb);
			 		obj.set('class','vote-over');
				}
			}
		}
	}
}

function sauveVote(id_collection)
{
	var lc_votes = new Object();
	var enfants = new Array;
	$('action-vote').getElements('input[value!=0]').each(function (champ) {
		var lc_vote = new Object();
	 	lc_vote.id = champ.get('id');
	 	lc_vote.name = champ.get('name');
	 	lc_vote.value = champ.get('value');
		enfants.push(lc_vote);
	});
	/*console.log(JSON.encode(enfants));*/
	var myCookie  = Cookie.write('voteDatas_'+id_collection, JSON.encode(enfants), {duration: 0.05});
}

// Cache les liens
function noLink(myLink, libelle, myClass)
{
	document.write('<a href="'+myLink+'" title="'+libelle+'" class="'+myClass+'">'+libelle+'</a>');
}

/* fonction nolink sans le 'title' pour pouvoir lui passer une image en libelle*/
function noLink_img(myLink, libelle, myClass, title)
{
	document.write('<a href="'+myLink+'" title="'+title+'" class="'+myClass+'">'+libelle+'</a>');
}

/**
* Fonction de masquage universelle.
* @var libelle : string
* @var params : hash( {'key1':"value1",'key2':"value2" [, ...]} )
* @author jbschrempp
*/
function noLink_global(libelle, params) {
	 var strAttributs = '';
	for (var key in params) {
		strAttributs += key + " = \"" + params[key] + "\" ";
	}
	document.write('<a '+ strAttributs + '>'+libelle+'</a>');
}



