
function trim(s)
{
 return (""+s=="") ? "" : s.replace(/^\s+/, "").replace(/\s+$/, "");
}

function email_valid(s)
{
	var er = /^[a-z0-9._\-]+[@][a-z0-9._\-]+[.][a-z0-9._\-]+$/i
	return (""+s=="") ? false : er.test(s);
}

function word_valid(s)
{
	var er = /^([zrtpqsdfghjklmwxcvbn�0-9 ]+)|(([aeiouy����0-9 ]+))$/i
	return (""+s=="") ? true : er.test(s);
}

function set_class(obj,cls)
{
	if(obj!=undefined && ""+cls!="") obj.className = cls;
}

function zelflink(uld, cib)
{
	var l = "";
	var rg = new RegExp("\\|","g");
	var rgd = new RegExp("@","g");
	l = uld.replace(rg,"/").replace(rgd,".");
	if (l.charAt(0)=="#")
	{
		l = l.substring(1,l.length);
	}
	if (cib == "target=_blank" || cib == "_blank" )
	{
		window.open(l);
	}
	else
	{
		window.location.href=l;
	}
}

function popup_perdu()
{
	var whlost = window.open ('', "popuplost", "width=525,height=180,resizable=0,scrollbars=0,top=100,left=100");
	whlost.focus();
}

function popup (url, nom, width, height)
{
	var popup = window.open (url, nom, 'width='+width+',height='+height+',resizable=1,scrollbars=yes,top=100,left=100');
}
function ouvrir_popup_centree()
	{
		var nom = arguments[0];
		var larg = arguments[1];
		var haut = arguments[2];
		var url = "";
		if(arguments.length==4) url = arguments[3];

		var wh = window.open(url,nom,'width='+ larg +',height='+ haut +',location=0,resize=1,scrollbars=auto');
		var x = screen.width;
		var y = screen.height;
		wh.moveTo( (x-larg)/2,(y-haut)/2 );
		wh.focus();
		return(wh);
	}
