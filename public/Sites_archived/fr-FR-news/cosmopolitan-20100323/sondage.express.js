
var path_script_bdd = "/sondages/sondage_express_bdd.php";
var path_xml_script = "/sondages/sondage_express_xml.php";
var method_script_bdd = "post";
var url_bouton_voter = "/sondages/images/votez_on.gif"







//var nom_formulaire = "frmSondage";

var adresse_serveur = "";
//var adresse_serveur = '127.0.0.1:84';

// exemple d'utilisation du fichier
// init des variables initiales
//		<script type="text/javascript">
// 		var nb_max_reponse=8;
// 		var num_sondage= 102 ;
// 		var num_question= 523 ;
// 		var id_div= "sondage102";
//		var nom_formulaire= "formulaire102";
//		</script>
// ajout d'un div pour y afficher le formulaire
//		<div id='sondage102'></div>
// ajout de la feuille de style par d�faut
//		<link rel="stylesheet" href="/sondages/sondages_express.css" type="text/css" />
//  appel du fichier si present
//	    <script type="text/javascript" src="/sondages/sondage.express.js"></script>


/*--- afin d'eviter le pb des nom fluctuants je gele a un seul sondage par page --*/
nom_formulaire = 'sondageexpress';


/*****************************
 *                           *
 * LIBRAIRIE XMLHTTP JS      *
 *                           *
 * version 1.0 - 07/2006     *
 *****************************/

// on cr�� et instancie l'objet httpRequest en global
var XHTTP_object = null;
var XHTTP_debug = true;

// la fonction permet divers possibilit�e appel en get en post. considere post si rien indiqu�. presence de fct dynamique 
// pour le cas �a marche , pour le cas echec de connexion, fonction de check.

function XHTTP_creation(url_script_distant,donnees_encodees,methode,ptr_fonction_ok,ptr_fonction_ko,ptr_fonction_check) 
{
	if(XHTTP_debug) msg_debug_init();
	//
	// r�cup�ration des param�tres de la fonction
	//
	var nbre_parametres = arguments.length;
	var url     = url_script_distant;
	var varpost = donnees_encodees;
	var mthd    = methode;
	var fcntn1  = ptr_fonction_ok;
	var fcntn2  = ptr_fonction_ko;
	var fcntn3  = ptr_fonction_check;
	switch(nbre_parametres)
	{
	case 0: 
	case 1:
	case 2:
		alert("il manque des param�tres dans l'appel de la fonction");
		nbre_parametres = 0;
		XHTTP_object = null;
		return(false);
		break;

	case 3:
		// �a doit �tre URL, DONNEES et FONCTION_OK
		url = url_script_distant;
		varpost = donnees_encodees;
		fcntn1 = methode;
		fcntn2 = null;
		fcntn3 = null;
		mthd   = "POST";
		if(XHTTP_debug) msg_debug("3 param�tres");
		break;

	case 4:
		// �a peut �tre :
		// 1) URL, DONNEES, METHODE, FONCTION_OK
		// 2) URL, DONNEES, FONCTION_OK, FONCTION_KO
		url    = url_script_distant;
		varpost = donnees_encodees;
		if( typeof(methode)=='string' ) // la variable est initialis�e
		{
			mthd   = methode;
			fcntn1 = ptr_fonction_ok;
			fcntn2 = null;
			fcntn3 = null;
		}
		else
		{
			mthd   = "POST";
			//fcntn1 = methode;
			//fcntn2 = ptr_fonction_ok;
			//fcntn3 = null;
			fcntn1 = ptr_fonction_ok;
			fcntn2 = ptr_fonction_ko;
			fcntn3 = null;
		}
		if(XHTTP_debug) msg_debug("4 param�tres");
		break;

	case 5:
		// �a peut �tre :
		// 1) URL, DONNEES, METHODE, FONCTION_OK et FONCTION_KO
		// 2) URL, DONNEES, FONCTION_OK, FONCTION_KO et FONCTION_CHECK
		url    = url_script_distant;
		varpost = donnees_encodees;
		if( typeof(methode)=='string' )
		{
			mthd   = methode;
			fcntn1 = ptr_fonction_ok;
			fcntn2 = ptr_fonction_ko;
			fcntn3 = null;
		}
		else
		{
			mthd   = "POST";
			//fcntn1 = methode;
			//fcntn2 = ptr_fonction_ok;
			//fcntn3 = ptr_fonction_ko;
			fcntn1 = ptr_fonction_ok;
			fcntn2 = ptr_fonction_ko;
			fcntn3 = ptr_fonction_check;
		}
		if(XHTTP_debug) msg_debug("5 param�tres");
		break;

	case 6:
		// �a doit �tre URL, DONNEES, METHODE, FONCTION_OK, FONCTION_KO et FONCTION_CHECK
		url    = url_script_distant;
		varpost = donnees_encodees;
		mthd   = methode;
		fcntn1 = ptr_fonction_ok;
		fcntn2 = ptr_fonction_ko;
		fcntn3 = ptr_fonction_check;
		if(XHTTP_debug) msg_debug("6 param�tres");
		break;

	default:
		alert("nombre de param�tres incoh�rent dans l'appel de la fonction"); 
		nbre_parametres = 0;
		XHTTP_object = null;
		return(false);
		break;
	}
	
	//
	// init de l'objet hpptRequest
	//
	
	// on test le navigateur pour connaitre la version de XMLHTTP support�e
	if(window.XMLHttpRequest) // Firefox   
		XHTTP_object = new XMLHttpRequest();   
	else if(window.ActiveXObject) // Internet Explorer   
		XHTTP_object = new ActiveXObject("Microsoft.XMLHTTP");   
	else { // XMLHttpRequest non support� par le navigateur   
		alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
		XHTTP_object = null;
		return(false);   
	}
	
	

	XHTTP_object.onreadystatechange = function()
	{
		if(XHTTP_debug) msg_debug("changement d'�tat ("+ XHTTP_object.readyState +")");
		if(XHTTP_object.readyState == 4) 
		{
		    // tout va bien, la r�ponse a �t� re�ue
		    if(fcntn1!=null && fcntn1!=undefined && XHTTP_object.status==200) 
		    {
		      
			    if(XHTTP_debug) msg_debug("appel fonction ok ");
			    
			    if(XHTTP_object.responseXML.xml=="" && fcntn2!=null && fcntn2!=undefined) fcntn2.call();
			    else fcntn1.call();
		    }

		    // la r�ponse a �t� re�ue mais ya une erreur
		    if(fcntn2!=null && fcntn2!=undefined && XHTTP_object.status!=200) 
		    {
		    	 
			    if(XHTTP_debug) msg_debug("appel fonction ko");
			    fcntn2.call();
		    }

		} 
		else if(XHTTP_object.readyState == 1 || XHTTP_object.readyState == 2 || XHTTP_object.readyState == 3)
		{
		    // en cours de chargement OU charg�e OU en cours d'interaction
		    if(fcntn3!=null || fcntn3!=undefined) 
		    {
			if(XHTTP_debug) msg_debug("appel fonction check");
		    	fcntn3.call();
		    }
		}
	};
	
	if(mthd.toUpperCase()=="POST")
	{
		
		if(XHTTP_debug) msg_debug("m�thode POST");
		XHTTP_object.open("POST", url, true);
		
		XHTTP_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	}
	else
		XHTTP_object.open("GET", url, true);

	if(XHTTP_debug) msg_debug("envoi de la requ�te ("+ methode +") \n"+  urlencodage(varpost) +"\nvers\n"+ url);
	XHTTP_object.send( varpost );
	return(true);
}


function urlencodage(chaine)
{
	var final = ""+ chaine;
	if(chaine!="" && chaine.indexOf("%")<=0)
	{
		if(chaine.indexOf("&")>0)
		{
			var tableau = chaine.split("&");
			for(var i=0;i<tableau.length;i++)
			{
				var tmp = tableau[i].split("=");
				final += "&"+ tmp[0] +"="+ escape(tmp[1]);
			}
			if( final!="" ) final = final.substr(2,final.length);
		}
		else if(chaine.indexOf("=")>0)
		{
			var tmp = chaine.split("=");
			final = tmp[0] +"="+ escape(tmp[1]);
		}
	}
	return(final);
}

var private_msg_debug_txt = "";

function msg_debug(msg)
{
private_msg_debug_txt += ""+ msg +"\n";
}

function msg_debug_affiche()
{
	alert("debug:\n"+ private_msg_debug_txt);
	msg_debug_init();
}

function msg_debug_init()
{
private_msg_debug_txt = "";
}



function exemple_traitement_ok()
{
  
	var resultat = "";
	
	if(XHTTP_debug) msg_debug( "xml="+ XHTTP_object.responseXML.xml );
	
	// on cr�e le noeud contenant le s�lecteur
	var root_node = XHTTP_object.responseXML.getElementsByTagName('dns');
	
	// on traite la valeur des noeuds selon le r�sultat d�sir�
	for (var i=0;i<root_node.length;i++)
	{
		var valeur = root_node.item(i).firstChild.nodeValue;
		resultat += "["+ i +"]="+ valeur +"\n";
	}
	if(resultat=="") resultat = "longueur="+root_node.length;
	
	if(XHTTP_debug) msg_debug_affiche();
	alert("resultats:\n"+ resultat);	
		
	XHTTP_object = null;
}

function exemple_utilisation()
{
	//XHTTP_debug = true;
	if( !XHTTP_creation("http://img.magicmaman.com/exemple.php","ip=193.252.197.111",exemple_traitement_ok)  ) alert("�chec du xhttp");
}





/////////////////////////////////////////////////////////////////////////
// fonctions permettant la creation 
//
//
//
//
///////////////////////////////////////////////////////////////////////

// donne un tableau de parametre/valeur pass� en url.
function PrmUrl()
{
 var prm = new Array();
 var tmp = unescape(window.location.search).substr(1).split("&");
 var inter;
 for ( i=0; i<tmp.length; i++)
 {
  inter=tmp[i].indexOf("=");
  if ( inter>=0 )
    {
     prm[tmp[i].substr(0,inter)]=tmp[i].substr(inter+1);
    }
  else {
     prm[tmp[i]]="";
    }
 }
 return prm;
}

//////////////////////// fonctions qui a partir du xml produit par le serveur ajoute des affichages dans le sondage (titre,question,reponse,etc..)/////////////
function CreateReponseValidation(tagName,attrName)
{
	var root_node = XHTTP_object.responseXML.getElementsByTagName(tagName.toLowerCase());
	if (root_node.item(0)) document.getElementById('label_rep'+nom_formulaire).firstChild.nodeValue = root_node.item(0).firstChild.nodeValue;
}

function recordCountViaXml(tagName,attrName)
{
	var root_node = XHTTP_object.responseXML.getElementsByTagName(tagName.toLowerCase());
	if (root_node.item(0)) nb_reponse_possible = root_node.item(0).firstChild.nodeValue;
}
function createTitreViaXml(tagName,attrName)
{
	var root_node = XHTTP_object.responseXML.getElementsByTagName(tagName.toLowerCase());
	if (root_node.item(0)) document.getElementById('label_titre'+nom_formulaire).innerHTML = root_node.item(0).firstChild.nodeValue;
}
function createQuestionViaXml(tagName,attrName)
{
	var root_node = XHTTP_object.responseXML.getElementsByTagName(tagName.toLowerCase());

	if (root_node.item(0)) document.getElementById('label_question'+nom_formulaire).innerHTML = root_node.item(0).firstChild.nodeValue;
}

function createMessageErreurViaXml(tagName) 
{
	var root_node = XHTTP_object.responseXML.getElementsByTagName(tagName.toLowerCase());
	if (root_node.item(0)) document.getElementById('label_erreur'+nom_formulaire).innerHTML = root_node.item(0).firstChild.nodeValue;
}

function createReponseViaXml(tagName,attrName)
{
	var root_node = XHTTP_object.responseXML.getElementsByTagName(tagName.toLowerCase());
	
	for (i=1;i<=nb_reponse_possible;i++)
	{
				
			document.getElementById('label_rep'+nom_formulaire + i).innerHTML = root_node.item(i-1).firstChild.nodeValue;
			document.getElementById('input_rep'+nom_formulaire + i).value = root_node.item(i-1).getAttribute(attrName.toLowerCase());
	}
}

//////////////////////////////////////////// fonction qui est utiliser par defaut pour le onreadystatechange de l'objet xHTTPrequest. 
function construit_sondage()
{
	createMessageErreurViaXml("messageerreur");	
	recordCountViaXml("item4","nombre");	
//	createTitreViaXml("item1","titre");
	
	
	createQuestionViaXml("item2","question");
	createReponseViaXml("item3","reponse");
	XHTTP_object = null;

	
	
	for (i=1;i<=nb_max_reponse;i++)
	{
		document.getElementById('rep'+ nom_formulaire + i).style.display='none';
	}

	for (i=1;i<=nb_reponse_possible;i++)
	{
		document.getElementById('rep'+ nom_formulaire + i).style.display='block';
	}
	
	document.getElementById('sondage_complet'+nom_formulaire).style.display='block';
	document.getElementById('sondage_vote'+nom_formulaire).disabled=true;
	document.forms[nom_formulaire]["num_sondage"].value=num_sondage;
	document.forms[nom_formulaire]["num_question"].value=num_question;

}
/////////////////////////////////////// fonction qui pourrait servir dans le cas confirmation///////////////
function construit_confirmation()
{
  createTitreViaXml("item1","titre");
  createQuestionViaXml("item2","question");
  CreateReponseValidation("item5");
}

////////////////////////
function clic_sondage()
{
	document.getElementById('sondage_vote'+nom_formulaire).disabled=false;
}

 function init_sondage()
 {
  if (num_sondage!=undefined && num_question!=undefined && num_sondage!="" && num_question!="")
  {
   document.getElementById('sondage_complet'+nom_formulaire).style.display='none';
   var XHTTPurl =  adresse_serveur+ path_xml_script;
  // XHTTPurl = '/sondages/sondage_express_xml.php'; 
   var r2 = XHTTP_creation(XHTTPurl,"num_sondage=" + num_sondage + "&num_question=" + num_question, construit_sondage);
  	
  }
 }

//////////////////////////////////////////////////////////////////////////////////////
 // mise en place du code html du sondage dans une variable texte_sondage_express
 //////////////////////////////////////////////////////////////////////////////////////
var texte_sondage_express =
'<div class="module module-question">'
+'	<div class="module-content" id="sondage_complet'+nom_formulaire+'">'
+'	<div class="zone zone-top">'
+'		<h2>SONDAGE</h2>'
+'	</div>'
+'	<div class="zone zone-middle">'
+'		<p id="label_titre'+nom_formulaire+'"></p>'
+'		<p id="label_question'+nom_formulaire+'"></p>'
+'	</div>'
+'	<div class="zone zone-bottom" id="sondage_content'+nom_formulaire+'">'
+'		<form name="'+nom_formulaire+'" action="'+adresse_serveur+ path_script_bdd +'" method="'+method_script_bdd+'">'
+'	 		<input type="hidden" name="num_sondage" value="" />'
+'     	 	<input type="hidden" name="num_question" value="" />'
+' 			<div id="label_erreur'+nom_formulaire+'"></div>'
+'			<fieldset class="fieldset-radio">'
+'				<fieldset class="fieldset-content">'
+'					<div class="fields">';

if(nb_max_reponse!=undefined && nb_max_reponse!="")
for(i=1;i<=nb_max_reponse;i++ ) 
{

texte_sondage_express +='	<div id="rep'+nom_formulaire+i+'"><input class="field field-radio" type="radio" name="num_reponse" onclick="javascript:clic_sondage();" id="input_rep'+ nom_formulaire+ i+'" /> <label for="input_rep'+ nom_formulaire +i+'"><span id="label_rep'+ nom_formulaire + i+'"> </span></label><span class="clear"></span></div>';	
}

	
texte_sondage_express +=
'		</div>'
+'	</fieldset>'
+'</fieldset>'
+'<fieldset id="sondage_vote'+nom_formulaire+'" class="fieldset-action">'
+'	<fieldset class="fieldset-content">'
+'		<div class="fields">'
+'			<input type="submit" class="field submit-field" id="submit-sondage-field" name="submit-sondage-field" value=" " />'
+'		</div>'
+'  </fieldset>'
+'</fieldset>'
+'</form>'
+'</div>'
+'<span class="clear"/>'
+'</div>'
+'</div>';
//texte_sondage_express += texte_sondage_express2	
		
// mise en place de la page de confirmation
// a faire si on veut integrer la reponse dans l'emplacement pub
texte_page_confirmation ='';

if(id_div!=undefined && id_div!="" && document.getElementById(id_div))
{
	document.getElementById(id_div).innerHTML = texte_sondage_express; 
	init_sondage();
}

