



/******************************************************************************/
/*  Finestra CONDIVIDI per articoli                                          */
    var x_offset_tooltip = 0;
    var y_offset_tooltip = 0;
    var ajax_tooltipObj = false;
    var contentDivOmbra = false;
    var ajax_tooltipObj_iframe = false;
    var ajax_tooltip_MSIE = false;
    if(navigator.userAgent.indexOf('MSIE')>=0)ajax_tooltip_MSIE=true;
    
    function ajax_showTooltip(text,inputObj,classDiv)
    {
    	if(!ajax_tooltipObj)	/* Tooltip div not created yet ? */
    	{
    		ajax_tooltipObj = document.createElement('DIV');
    		ajax_tooltipObj.style.position = 'absolute';
    		ajax_tooltipObj.id = 'ajax_tooltipObj';		
    		ajax_tooltipObj.className = classDiv;
            document.body.appendChild(ajax_tooltipObj);
            
            contentDivOmbra = document.createElement('DIV'); /* Create tooltip content div */
    		contentDivOmbra.className = 'ajax_tooltip_content_ombra';
            ajax_tooltipObj.appendChild(contentDivOmbra);
            
            var contentDiv1 = document.createElement('DIV'); /* Create tooltip content div */
    		contentDiv1.className = 'ajax_tooltip_content1';
            ajax_tooltipObj.appendChild(contentDiv1);
    		contentDiv1.id = 'ajax_tooltip_content1';
            var contentDiv = document.createElement('DIV'); /* Create tooltip content div */
    		contentDiv.className = 'ajax_tooltip_content';
    		contentDiv1.appendChild(contentDiv);
    		contentDiv.id = 'ajax_tooltip_content';
            
            if(ajax_tooltip_MSIE){	/* Create iframe object for MSIE in order to make the tooltip cover select boxes */
    			ajax_tooltipObj_iframe = document.createElement('<IFRAME frameborder="0">');
    			ajax_tooltipObj_iframe.style.position = 'absolute';
    			ajax_tooltipObj_iframe.border='0';
    			ajax_tooltipObj_iframe.frameborder=0;
    			ajax_tooltipObj_iframe.style.backgroundColor='#FFF';
    			ajax_tooltipObj_iframe.src = 'about:blank';
    			contentDiv.appendChild(ajax_tooltipObj_iframe);
    			ajax_tooltipObj_iframe.style.left = '0px';
    			ajax_tooltipObj_iframe.style.top = '0px';
    		}
            
    	}
        ajax_tooltipObj.childNodes[1].firstChild.innerHTML = text;
    	// Find position of tooltip
    	ajax_tooltipObj.style.display='block';
    	if(ajax_tooltip_MSIE){
    		ajax_tooltipObj_iframe.style.width = ajax_tooltipObj.clientWidth + 'px';
    		ajax_tooltipObj_iframe.style.height = ajax_tooltipObj.clientHeight + 'px';
    	}
    
    	ajax_positionTooltip(inputObj);
    }
    
    function ajax_positionTooltip(inputObj)
    {
        var myWidth = 0, myHeight = 0;
        if( typeof( window.innerWidth ) == 'number' )
        {
            //Non-IE
            myWidth = window.innerWidth;
            myHeight = window.innerHeight;
            if(navigator.userAgent.indexOf('safari')>=0)
                contentDivOmbra.style.width = (document.documentElement.scrollWidth) + 'px';
            else
                contentDivOmbra.style.width = (document.documentElement.scrollWidth) + 'px';
            contentDivOmbra.style.height = document.documentElement.scrollHeight + 'px';
        }
        else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) )
        {
            //IE 6+ in 'standards compliant mode'
            myWidth = document.documentElement.clientWidth;
            myHeight = document.documentElement.clientHeight;
            contentDivOmbra.style.width = (document.documentElement.scrollWidth) + 'px';
            contentDivOmbra.style.height = (document.documentElement.scrollHeight) + 'px';
        }
        else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) )
        {
            //IE 4 compatible
            myWidth = document.body.clientWidth;
            myHeight = document.body.clientHeight;
            contentDivOmbra.style.width = (document.documentElement.offsetWidth) + 'px';
            contentDivOmbra.style.height = document.documentElement.offsetHeight + 'px';
        }
    	ajax_tooltipObj.style.left = Math.round((myWidth-300)/2) + 'px';
        ajax_tooltipObj.style.top = Math.round((myHeight-200)/2) + 'px';
    
        contentDivOmbra.style.left = (1-Math.round((myWidth-300)/2)-1) + 'px';
        contentDivOmbra.style.top = (1-Math.round((myHeight-200)/2)-1) + 'px';
        
        document.documentElement.scrollTop = 0;
        document.documentElement.scrollLeft = 0;
    }
    function ajax_hideTooltip()
    {
        ajax_tooltipObj.style.display='none';
    }
    function ajaxTooltip_getTopPos(inputObj)
    {		
        var returnValue = inputObj.offsetTop - 30;
        while((inputObj = inputObj.offsetParent) != null)
            if(inputObj.tagName!='HTML')returnValue += inputObj.offsetTop;
        return returnValue;
    }
    
    function ajaxTooltip_getLeftPos(inputObj)
    {
        var returnValue = inputObj.offsetLeft;
        while((inputObj = inputObj.offsetParent) != null)
        	if(inputObj.tagName!='HTML')returnValue += inputObj.offsetLeft;
        return returnValue;
    }
    
    function createRequestObject()
    {
    	var request = false;
        try
        {
            // Mozilla/Safari
            if (window.XMLHttpRequest)
            {
                request = new XMLHttpRequest();
            }
            // IE
            else if (window.ActiveXObject)
            {
                request = new ActiveXObject("Microsoft.XMLHTTP");
            }
        }
        catch (e)
        {
            alert(e);
        }
        if(!request )
        {
            alert("Impossibile creare l'oggetto XHR.");
        }
        return request;
    }
    var http = false;
    function share(what, url, t_link, t_nome, t_mail, t_destmail, t_messaggio)
    {
        if (http==false) {http = createRequestObject();} 
        http.abort()
        http.open('GET', '/share/share.asp?action='+what+'&url='+url+'&t_link='+t_link+'&t_nome='+t_nome+'&t_mail='+t_mail+'&t_destmail='+t_destmail+'&t_messaggio='+t_messaggio, true);
        http.onreadystatechange = shareEnd;
        http.send(null);
    }
    function share_newsletter(what, url, title, t_nome, t_mail, t_destmail, t_messaggio)
    {
        if (http==false) {http = createRequestObject();} 
        http.abort()
        http.open('GET', '/_form/popupNewsletterFrame.aspx?email='+what, true);
        http.onreadystatechange = shareEnd;
        http.send(null);
    }
    function shareEnd()
    {
        if(http.readyState == 4)
        {
            var response = http.responseText;
            ajax_showTooltip(response,null,"ajax_userDiv");
        }
    }
    function shareNewsletterEnd()
    {
        
            var response = http.responseText;
            ajax_showTooltip(response,null,"ajax_userDiv");
        
    }
    /* Modified to support Opera */
    function addFavorites(url, title)
    {
        if (window.sidebar)
            // firefox
	        window.sidebar.addPanel(title, url, "");
        else if(window.opera && window.print)
        {
            // opera
        	var elem = document.createElement('a');
        	elem.setAttribute('href',url);
        	elem.setAttribute('title',title);
        	elem.setAttribute('rel','sidebar');
        	elem.click();
        } 
        else if(document.all)
            // ie
	        window.external.AddFavorite(url, title);
}
/******************************************************************************/





function share(what, url, title, t_nome, t_mail, t_destmail, t_messaggio)
    {
        if (http==false) {http = createRequestObject();} 
        http.abort()
        http.open('GET', '/share/share.asp?action='+what+'&url='+url+'&titolo='+title+'&t_nome='+t_nome+'&t_mail='+t_mail+'&t_destmail='+t_destmail+'&t_messaggio='+t_messaggio, true);
        http.onreadystatechange = shareEnd;
        http.send(null);
    }
    function shareEnd()
    {
        if(http.readyState == 4)
        {
            var response = http.responseText;
            ajax_showTooltip(response,null,"ajax_userDiv");
        }
    }
    /* Modified to support Opera */
    function addFavorites(url, title)
    {
        if (window.sidebar)
            // firefox
	        window.sidebar.addPanel(title, url, "");
        else if(window.opera && window.print)
        {
            // opera
        	var elem = document.createElement('a');
        	elem.setAttribute('href',url);
        	elem.setAttribute('title',title);
        	elem.setAttribute('rel','sidebar');
        	elem.click();
        } 
        else if(document.all)
            // ie
	        window.external.AddFavorite(url, title);
}
