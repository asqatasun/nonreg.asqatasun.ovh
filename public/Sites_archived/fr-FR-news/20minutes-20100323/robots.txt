User-agent: *
Disallow: /include
Disallow: /includes
Disallow: /delcookies.php
Disallow: /404.php
Disallow: /cache
Disallow: /img
Disallow: /js
Disallow: /css
Disallow: /diaporama/384
Disallow: /diaporama/78

User-agent: Mediapartners-Google
Allow: /diaporama/384
Allow: /diaporama/78

Sitemap: http://www.20minutes.fr/sitemap.xml

