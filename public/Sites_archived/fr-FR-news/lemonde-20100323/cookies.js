var cookie_perso_web_ok = 0;

// Initialisation d'un flag pour savoir si on se trouve sur "La Une"
var La_Une;
La_Une = 0;        
function GetCookieVal (offset) 
{
   var endstr = document.cookie.indexOf (";", offset);
   if (endstr == -1)
      endstr = document.cookie.length;
   return unescape(document.cookie.substring(offset, endstr));
}

function GetCookie (name) 
{
   var arg = name + "=";
   var alen = arg.length;
   var clen = document.cookie.length;
   var i = 0;
   while (i < clen) 
   {
      var j = i + alen;
      if (document.cookie.substring(i, j) == arg)
      {
         return GetCookieVal(j);
      }
      i = document.cookie.indexOf(" ", i) + 1;
      if (i == 0) 
      {
         break;
      }
   }
   return null;
}

function SetCookieTdb (name,value,expires,path,domain,secure) 
{
   document.cookie = name + "=" + escape (value) +
   ((expires) ? "; expires=" + expires.toGMTString() : "") +
   ((path) ? "; path=" + path : "; path=") +
   ((domain) ? "; domain=" + domain : "") +
   ((secure) ? "; secure" : "");
}
