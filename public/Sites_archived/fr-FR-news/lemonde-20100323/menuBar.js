var gl_menuBar_f = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
var gl_menuBar_loaded = new Array();
var visible_niveau3 = 0;
var numBar;
var url_defaut = null;

function menuBar_on(num)
{  
   if (gl_menuBar_f[num] != 1)
   {
      // On clear le timeout
      if (timer_reveal)
      {
         clearTimeout(timer_reveal);
         timer_reveal = null
      }
      if (timer)
      {
         clearTimeout(timer);
         timer = null;
      }
      
      numBar = num;
      gl_menuBar_f[num] = 2; // en attente.
      
      /*if ( document.getElementById("fleche") )
      {
         document.getElementById("fleche").style.backgroundImage = 'url("/medias/www/img/icn/flechebas_filet.gif")';
      }*/
         
      
      var elts_du_dom = document.getElementById('menuBar').getElementsByTagName('div');
      for (var i = 0; i < elts_du_dom.length ; i++)
      {
         if ((elts_du_dom[i].id).substr(0, 7) == 'level3_')
         {
            $('#'+elts_du_dom[i].id).slideUp("normal", menuBarTimeOut);
            visible_niveau3 = 0;
         }
      }
   }
}

function menuBar_reset_flags()
{
   for (cpt = 0; cpt < 9; ++cpt)
      if (gl_menuBar_f[cpt] != 1)
         gl_menuBar_f[cpt] = 0;
}

function menuBar_out(num)
{
   menuBar_reset_flags();
   visible_niveau3 = 0;
}

function menuBar(num)
{ 
   if (gl_menuBar_f[num] == 2)
   {
      gl_menuBar_f = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
      gl_menuBar_f[num] = 1;
      var cpt;
      for (cpt = 0; cpt < 10; ++cpt)
      {
         if (cpt != num && document.getElementById("menuBarTab_" + cpt) != null)
         {
            document.getElementById("menuTable_" + cpt).style.display = "none";
            
            menuEnCours = null;
            var classNow = document.getElementById("menuBarTab_" + cpt).className;
            var classNew = classNow.replace('tabOn', 'tabOff');
            
            document.getElementById("menuBarTab_" + cpt).setAttribute("class", classNew);
            document.getElementById("menuBarTab_" + cpt).setAttribute("className", classNew);
         }
      }  
   
      if (document.getElementById("menuBarTab_" + num) != null)
      {
         var classNow = document.getElementById("menuBarTab_" + num).className;
         var classNew = classNow.replace('tabOff', 'tabOn');

         document.getElementById("menuBarTab_" + num).setAttribute("class", classNew);
         document.getElementById("menuBarTab_" + num).setAttribute("className", classNew);
         
         document.getElementById("menuTable_" + num).style.display = "block";
         document.getElementById("menuBarContent_" + num).style.display = "block";
      }
   }
}


/*******************************************
 **                                       **
 ** Fonction pour la gestion du Sous-Menu **
 **             Merci à CBS               **
 **                                       **
 *******************************************/
var timer = null;
var timer_reveal = null;
var slideEnCours = 0;
var menuEnCours = null;
var nb_elt_actualite = 0;

function activerMenu(nb_element)
{
   // visible_niveau3 == -1 (survole sur la flèche pour ouvrir le menu (niveau 3) des actualités)
   if ( !visible_niveau3 || visible_niveau3 == 0 )
      visible_niveau3 = -1;
   
   nb_elt_actualite = nb_element;
   
   afficherMenu('level3_0','');
}

function afficherMenu(obj, url_esi)
{
   if ( !visible_niveau3 || visible_niveau3 == 0 )
      return;
      
   if (timer_reveal)
   {
      clearTimeout(timer_reveal);
   }
   if (timer)
   {
      clearTimeout(timer);
      timer = null;
   }
   
   if ( url_esi == '' ) {
      url_esi = url_defaut;
   }
   
   if (obj.substr(0, 7) == 'level3_')
      cpt_level3 = obj.substr(7);
         
   if ( !gl_menuBar_loaded[cpt_level3] || gl_menuBar_loaded[cpt_level3] == 0 ) {
            
      //document.getElementById('level3_vide').style.display = 'block';
      document.getElementById('level3_tempo').style.display = 'block';
      
      gl_menuBar_loaded[cpt_level3] = 1;

      if ( url_esi != undefined ) {
            // On fait appel au sous-menu en ajax
            $.ajax({
              url: url_esi,
              cache: false,
              success: function(html){
                $("#level3_vide").append(html);
                timer_reveal = setTimeout('afficherMenuDo("' + obj +'")', 0);
                document.getElementById('level3_tempo').style.display = 'none';
              }
            });
            
      } else {
         timer_reveal = setTimeout('afficherMenuDo("' + obj +'")', 0);
      }
   } else {
      timer_reveal = setTimeout('afficherMenuDo("' + obj +'")', 0);
   }
   
}

/**
 ** Affiche le sous Menu
 ** @string obj   l'identifiant de la div du menu
 **/
function afficherMenuDo(obj)
{
   // On masque tous les sous-menus
   var elts_du_dom = document.getElementById('menuBar').getElementsByTagName('div');
   for (var i = 0; i < elts_du_dom.length ; i++)
   {
      if ((elts_du_dom[i].id).substr(0, 7) == 'level3_' && (elts_du_dom[i].id).substr(7) != 'vide' )
      {
         document.getElementById(elts_du_dom[i].id).style.display = 'none';
      }
   }

   // On clear le timeout
   if (timer_reveal)
   {
      clearTimeout(timer_reveal);
   }
   if (timer)
   {
      clearTimeout(timer);
      timer = null;
   }

   // affichage du sous menu sélectionné
   var menu_layer = document.getElementById(obj);
   var menu_item = obj;

   menuEnCours = obj;
   
   // visible_niveau3 == -1 (survole sur la flèche pour ouvrir le menu (niveau 3) des actualités)
   if (!visible_niveau3 || visible_niveau3 == -1)
   {
		if (menu_layer) {
			menu_layer.style.display = 'block';
		}
			
      annoncerFinSlide();
          
      //document.getElementById("fleche").style.backgroundImage = 'url("/medias/www/img/icn/flechehaut_filet.gif")';
   }
   else
   {
      if (!slideEnCours)
      {
         if (menu_layer) {
            menu_layer.style.display = 'block';
            //document.getElementById("fleche").style.backgroundImage = 'url("/medias/www/img/icn/flechehaut_filet.gif")';
         }
      }
   }
   
   visible_niveau3 = 1;
   document.getElementById('level3_tempo').style.display = 'none';
}

var xt_med_open=false;
function ouvrirMenu(nb_element, url_esi)
{
   if ( url_defaut == null ) 
   {
      url_defaut = url_esi;
   }
    
   if ( menuEnCours == null )
   {
      // visible_niveau3 == -1 (survole sur la flèche pour ouvrir le menu (niveau 3) des actualités)
      if ( !visible_niveau3 || visible_niveau3 == 0 )
         visible_niveau3 = -1;
      
      nb_elt_actualite = nb_element;
              
      if ( !xt_med_open && 'function' == typeof xt_med )
      {
         xt_med('C','1','Home_menu_Rubriques_Open', 'N');
         xt_med_open=true;
      }
      afficherMenu('level3_0','');
      $('#level3_vide').slideDown("normal",annoncerFinSlide);
      //document.getElementById("fleche").style.backgroundImage = 'url("/medias/www/img/icn/flechehaut_filet.gif")';
   } 
   else 
   {
      //gerer fermeture du 'ce que j ai manque'
      gererFermetureCeQueJaiLoupe();
   }
}

function annoncerFinSlide()
{
   slideEnCours = 0;
   {
   if (menuEnCours)
      afficherMenu(menuEnCours,'');
   afficheDateCqjm();
   }
}

function menuBarTimeOut()
{
   menuBar(numBar);
}

/**
 ** Masque le sous Menu
 ** @string obj   l'identifiant de la div du menu
 **/
function masquerMenu(obj)
{
   var menu_layer = document.getElementById(obj);
   if (menu_layer)
   {
      $('#'+obj).slideUp();
      visible_niveau3 = 0;
      menuEnCours = null;
      
      $('#level3_vide').slideUp();
      //document.getElementById("fleche").style.backgroundImage = 'url("/medias/www/img/icn/flechebas_filet.gif")';
   }

   for (var i = 0; i < nb_elt_actualite ; i++)
   {
      if ( document.getElementById('level2_'+i) ) {
         $("#level2_"+i).mouseover(function()
         {
            $(this).removeClass("rollover");
         });
      }
   }
}

/**
 ** affecte le timeout de masquage du menu
 ** @string obj   l'identifiant de la div du menu
 **/
function timerMenu(obj)
{
   if (timer)
   {
      clearTimeout(timer);
      timer = null;
   }
   timer = setTimeout('masquerMenu("' + obj +'")', 250);
}

function gererAffichageCeQueJaiLoupe()
{
   if (  (typeof MIA.CookieSemiPermanent != 'undefined') &&
         (
            (! _testerPageAbo()) || 
            (! MIA.CookieSemiPermanent.testerExistenceInfo('cqjl')) ||
            (typeof MIA.CookieSemiPermanent.getInfoInCookie()['cqjl']['ferm'] == 'undefined') ||
            (! _testerDsPeriodeFermetureCQJL(MIA.CookieSemiPermanent.getInfoInCookie()['cqjl']['ferm']))) )
   {
      //ouvrirMenu(0, url_cequejailoupe);
   }
}
function _testerPageAbo()
{
   var val_cook_temp = GetCookie('tdb_user_abo');

   return ((val_cook_temp != null) && (val_cook_temp != ""))  ? true : false;
}

function _testerDsPeriodeFermetureCQJL(tmstp)
{
   var date_auj = new Date();
   var tmstp_auj = date_auj.getTime();
   var nb_jour = 3;
   var jour =  date_auj.getDay();
   
   if (jour < 3) //si nous sommes lun, mar ou merc on autorise une journee en +
   {
      nb_jour = 4;
   }
   
   if ((1 * tmstp)  + (24 * 3600000 * nb_jour) > tmstp_auj)
   {
      return true;
   }   
   return false;
}

function _declarerFermeture()
{
   if (  _testerPageAbo() &&  
         (
            ! MIA.CookieSemiPermanent.testerExistenceInfo('cqjl') ||
            (typeof MIA.CookieSemiPermanent.getInfoInCookie()['cqjl']['ferm'] == 'undefined') ||
            (! _testerDsPeriodeFermetureCQJL(1 * MIA.CookieSemiPermanent.getInfoInCookie()['cqjl']['ferm'])) ) )
   {
      var date_auj = new Date();
      var tmstp = date_auj.getTime();
      MIA.CookieSemiPermanent.addInfoInCookie('cqjl', 'ferm', tmstp);
   }
}

function gererFermetureCeQueJaiLoupe()
{
   //depot du cookie de fermeture
   _declarerFermeture();
      
   //fermeture effective
   $('#level3_vide').slideUp();
   menuEnCours = null;
}


function afficheDateCqjm()
{
   var date = new Date();
   date.setHours(0, 0);
   var timestamp_auj = Math.floor(date.getTime() / 1000);
   var timestamp_hier = Math.floor(date.getTime() / 1000) - (24 * 3600);
   
   $("span[type='cqjm_date']").each(
      function (i) 
      { 
         if (this.getAttribute('tmstp') >= timestamp_auj) 
         { 
            this.innerHTML = 'Aujourd\'hui &agrave; ' + this.getAttribute('heure'); 
         }
         else if (this.getAttribute('tmstp') > timestamp_hier)
         {
            this.innerHTML = 'Hier &agrave; ' + this.getAttribute('heure'); 
         }
      }
   );
}


