function writeAlttabs(by, id, range)
{
	var ti=TITRE;
	if(!document.getElementById(id))return;
	var xhr_object;
	   if(window.XMLHttpRequest) // Firefox 
	      xhr_object = new XMLHttpRequest(); 
	   else if(window.ActiveXObject) // Internet Explorer 
	      xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
	   else { // XMLHttpRequest non support� par le navigateur 
	      alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
	      return false; 
	   } 

	   xhr_object.open("GET", xmlBaseUrl+'?w=alttabs&by='+by+'&ti='+ti+'&rg='+range+'&ch='+CHAINE, true); 
		
   		xhr_object.onreadystatechange = function() {
   		  if(xhr_object.aborted) return;
	      if(xhr_object.readyState == 4)
	      {
	      	if(xhr_object.status  == 200)
	      	{
	        
	        //f.style.backgroundColor='transparent';
	        //alert(xhr_object.responseText);
	        	var xml = xhr_object.responseXML;                                             //  Assigner le fichier XML � une variable
				//alert(xhr_logos_object.responseText);
//					listProgramme = doc.getElementsByTagName("programme");

					var photook=false;
					var buffer='';
					var j=0;
					for(var i=0;i<xml.getElementsByTagName("article").length;i++)
					{
						var node_art=xml.getElementsByTagName("article")[i];
						if(node_art&&node_art.getElementsByTagName("titre").item(0).firstChild&&node_art.getElementsByTagName("accroche").item(0).firstChild)
						{
							j++;
							//alert(listVilles[i].firstChild.nodeValue);
							//var video='';
							buffer+='<div class="a pos'+(j%2)+'">';
							buffer+='<a href="'+$P.htmlentities(edito_url('ra/' + node_art.getElementsByTagName('idrub').item(0).firstChild.nodeValue + '/' + node_art.getElementsByTagName('idart').item(0).firstChild.nodeValue))+'" >';
							if(node_art.getElementsByTagName("photo").item(0)&&(!photook))
							{
								buffer+='<img class="ph" alt="photo" src="'+$P.htmlentities(node_art.getElementsByTagName("photo").item(0).getAttribute('src'))+'"/>';
								photook=true;
							}
   							buffer+='<div>';
   							buffer+='<span class="pop">'+$P.htmlentities(node_art.getElementsByTagName("popularite").item(0).firstChild.nodeValue)+'</span> ';
   							if(node_art.getElementsByTagName("commune").item(0))
   							{
   								buffer+='<span class="co">'+$P.htmlentities(node_art.getElementsByTagName("commune").item(0).firstChild.nodeValue)+'</span> ';
   							}
    						buffer+='<span class="ti">'+$P.htmlentities(node_art.getElementsByTagName("titre").item(0).firstChild.nodeValue)+'</span> ';
							buffer+='<span class="par">'+$P.htmlentities(node_art.getElementsByTagName("parution").item(0).firstChild.nodeValue)+'</span> ';
							buffer+='</div>';
							//buffer+='<span class="acr">'+$P.htmlentities(node_art.getElementsByTagName("accroche").item(0).firstChild.nodeValue)+'</span> ';
    						buffer+='</a>';
    						buffer+='</div>';
							//title=title[0].firstChild.nodeValue;
							//var tag=node_tag.firstChild.nodeValue;
							//var color=node_tag.getAttribute('c');
							//var size=node_tag.getAttribute('s');
						}
					}
					
					var bloc=document.getElementById(id);
					bloc.innerHTML=buffer;
					//bloc.style.display='block';
					
					/*displayLogos(id,logos);*/
					/*
					var villes = new Array();
					Ville = document.getElementsByTagName("Ville");
					// find the one with our special text
					for(i = 0; i < Ville.length; i++)
					{
						villes[i]=new Object();
						villes[i].nom=Ville[i].firstChild.nodeValue;
						alert(Ville[i].firstChild.nodeValue);
					}
					
					* */
					
				}
	        //	alert('Votre message a bien �t� envoy�');
	        // else
	      } 
	   } 
	 
	   //xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); 
	   xhr_object.send(null); 


	return false;	
}

function alttab_refresh()
{
	var rg=mmc.getField('alttabrg', 30);
	alttab_setrg(rg);
	writeAlttabs('vu', 'ctnAltTabs-vu', rg);
	writeAlttabs('vote', 'ctnAltTabs-vt', rg);
	writeAlttabs('cmt', 'ctnAltTabs-cmt', rg);
}

function alttab_sl2d(i)
{
	return Math.round(Math.exp(i/10));
}

function alttab_d2sl(r)
{
	return Math.round(Math.log(r)*10);	
}

function alttab_setrg(rg)
{
	var str;
	if(rg==1) str='aujourd\'hui';
	else if(rg<30) str='depuis '+rg+' jours';
	else if(rg<365) str='depuis '+Math.round(rg/30)+' mois';
	else str='depuis 1 an';
	document.getElementById('altTabsRgVal').innerHTML=str;
}

mca.push(function(){
	if(document.getElementById('altTabsRgVal'))
	{
		alttab_refresh();
		//$('#altTabsRg').slider( { min: 1, max: 365 } );
		$('#ctnAltTabs .ui-tabs-nav').bind('tabsselect',
				function(event, ui) {
					var zmurl = ui.tab;
					if(zmurl)
					{
						var znurl = zmurl.toString().split("#"); 
						mmc.setField('alttabid', znurl[1]);
						mmc.write();
					}
				}
			);
	}
});
