ScrollDMANews = function(dma, nature, heure, commune, titre, importance, url)
{
	this._dma=dma;
	this._nature=nature;
	this._heure=heure;
	this._commune=commune;
	this._titre=titre;
	this._importance=importance;
	this._url=url;
};

ScrollDMANews.prototype.write = function(div)
{
	var	a = document.createElement('a');
	div.appendChild(a);
	a.setAttribute('href', this._url);
	a.setAttribute('class', 'importance'+this._importance);
	
	var tmp;
	
	if(this._nature)
	{
		tmp = document.createElement('span');
		a.appendChild(tmp);
		tmp.setAttribute('class', this._nature);
		tmp.innerHTML=this._nature=='afp'?'AFP ':'';
	}

	if(this._heure!='00:00')
	{
		tmp = document.createElement('span');
		a.appendChild(tmp);
		tmp.setAttribute('class', 'heure');
		tmp.innerHTML=this._heure;
		a.innerHTML+=' ';
	}
	
	if(this._commune)
	{
		tmp = document.createElement('span');
		a.appendChild(tmp);
		tmp.setAttribute('class', 'commune');
		tmp.innerHTML=this._commune;
		a.innerHTML+=' ';
	}
	
	tmp = document.createElement('span');
	a.appendChild(tmp);
	tmp.setAttribute('class', 'titre');
	tmp.innerHTML=this._titre;

	
};

ScrollDMA = function(edito, idCtn, idNews)
{
	this._edito=edito;
	this._idCtn=idCtn;
	this._idNews=idNews;
	this._news=new Array();
};

ScrollDMA.prototype.init = function()
{
	var self=this;
	this.query(function(){ self.dispNews(0); });
};

ScrollDMA.prototype.addNews = function (news)
{
	this._news.push(news);
};

ScrollDMA.prototype.dispNews = function (i)
{
	/*
	$('#scrollDMANews').cycle({ 
		fx:    'scrollRight', 
		delay: -1000 
	});
	*/
	
	if(i>=this._news.length) i=0;
	if(!this._news[i])
	{
		$('#'+this._idCtn).hide();
		return;
	}
	var self=this;
	for(var u=0; u<this._news.length; u++)
	{
		self.writeNews(u);
	}
	/*
	$('#'+this._idNews).fadeOut('fast', function(){
		self.writeNews(i);
		$('#'+self._idNews).fadeIn('fast');
		window.setTimeout(function(){ self.dispNews(i+1); }, 5000);
	});
	*/
	$('#scrollDMANews').cycle({ 
		fx:    'scrollLeft', 
		delay: -1000,
		timeout: 5000,
		pause:         1
	});
	
	
};

ScrollDMA.prototype.writeNews = function (i)
{
	var div=document.getElementById(this._idNews);
	myDiv = document.createElement('div');
	div.appendChild(myDiv);
					

	this._news[i].write(myDiv);
		
	if($.browser.msie)
	{
		//weirdo bug : ie does not refresh the innerHTML with appenChild without this :
		div.innerHTML+='';
	}	
}

ScrollDMA.prototype.query = function (callback)
{
	var xhr_object;
   if(window.XMLHttpRequest) // Firefox 
      xhr_object = new XMLHttpRequest(); 
   else if(window.ActiveXObject) // Internet Explorer 
      xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
   else { // XMLHttpRequest non support� par le navigateur 
      alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
      return false; 
   } 
	 
   xhr_object.open("GET", this._edito._xmlUrl+'?w=scrolfil&t='+this._edito._TITRE, true); 
	
   var self=this;
   xhr_object.onreadystatechange = function() {
	  if(xhr_object.aborted) return;
      if(xhr_object.readyState == 4)
      {
      	if(xhr_object.status  == 200)
      	{
			self.processQueryResults(xhr_object.responseXML);
			callback();
        }
      }
	};
	xhr_object.send(null); 
};

ScrollDMA.prototype.getNodeValue = function(node)
{
	if(!node) return null;
	if(!node.item(0)) return null;
	if(!node.item(0).firstChild) return null;
	return node.item(0).firstChild.nodeValue;
};

ScrollDMA.prototype.processQueryResults = function (docXML)
{
	for(var i=0;i<docXML.getElementsByTagName("breve").length;i++)
	{
		var breve = docXML.getElementsByTagName("breve")[i];
		this.addNews(new ScrollDMANews(
				this, 
				this.getNodeValue(breve.getElementsByTagName("nature")), 
				this.getNodeValue(breve.getElementsByTagName("heure")),
				this.getNodeValue(breve.getElementsByTagName("commune")), 
				this.getNodeValue(breve.getElementsByTagName("titre")), 
				this.getNodeValue(breve.getElementsByTagName("importance")),
				this._edito.getUrl('ra/' + this.getNodeValue(breve.getElementsByTagName("idrub")) + '/' + this.getNodeValue(breve.getElementsByTagName("idbreve")))));
	}

};