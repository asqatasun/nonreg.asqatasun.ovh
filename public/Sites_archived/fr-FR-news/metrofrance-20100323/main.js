
var msg_featuredcontent_previous        = 'Précedent';
var msg_featuredcontent_next            = 'Suivant';
var msg_featuredcontent_readfullstory   = 'Lire la suite';
var msg_featuredcontent_of              = 'de';
  

    

    
var prm_gallery2_main_url = "http://gallery.prd.metro.g30.se/main.php";

  
/* Omniture Sitecatalyst settings. Used in omniture_s_code.js */
var msg_omniture_account = 'metrointfrancewib';
var msg_omniture_linkinternalfilters = 'javascript:,http://fr-front2.prd.metro.g30.se/';

/* SiteCatalyst code version: H.12.
Copyright 1997-2007 Omniture, Inc. More info available at
http://www.omniture.com */
/************************ ADDITIONAL FEATURES ************************
     Plugins
*/
/* Specify the Report Suite ID(s) to track here */
var s_account=msg_omniture_account // ** BE SURE TO CHANGE THE FIRST VALUE IN THIS VARIABLE TO A PRODUCTION SUITE BEFORE GOING LIVE!
var s=s_gi(s_account)
/************************** CONFIG SECTION **************************/
/* You may add or alter any code config here. */
s.charSet="ISO-8859-1"
/* E-commerce Config */
s.currencyCode="USD"
/* Link Tracking Config */
s.trackDownloadLinks=true
s.trackExternalLinks=true
s.trackInlineStats=true
s.linkDownloadFileTypes="exe,zip,wav,mp3,mov,mpg,avi,wmv,doc,pdf,xls"
s.linkInternalFilters=msg_omniture_linkinternalfilters
s.linkLeaveQueryString=false
s.linkTrackVars="None"
s.linkTrackEvents="event2"
/* Plugin Config */
s.usePlugins=true
function s_doPlugins(s) {
	/* Get campaign tracking code */
	if(!s.campaign) {
		s.campaign=s.getQueryParam('cid');
		s.campaign=s.getValOnce(s.campaign,'s_campaign',0);
	}
		
	/* Lowercase variables */
	if(s.prop1)
		s.prop1=s.prop1.toLowerCase()
	
	/* Copy search term to eVar */
	if(s.prop1){
		s.eVar1=s.prop1
	
		/* Set de-duped onsite search event */
		var t_search=s.getValOnce(s.eVar1,'ev1',0)
		if(t_search)
			s.events=s.apl(s.events,'event1',',',2)
	}
	
	/* Set Page View Event */
	s.events=s.apl(s.events,'event2',',',2)
	
	/* Set Time Parting Variables */
	s.prop11=s.getTimeParting('h','1','2010'); // Set hour 
	s.prop12=s.getTimeParting('d','1','2010'); // Set day
	s.prop13=s.getTimeParting('w','1','2010'); // Set Weekend / Weekday
	
	/* Copy props to eVars */
	if(s.pageName&&!s.eVar2) s.eVar2=s.pageName;
	if(s.prop3&&!s.eVar3) s.eVar3=s.prop3;
	if(s.prop4&&!s.eVar4) s.eVar4=s.prop4;
	if(s.prop5&&!s.eVar5) s.eVar5=s.prop5;
	if(s.prop6&&!s.eVar6) s.eVar6=s.prop6;
	if(s.prop7&&!s.eVar7) s.eVar7=s.prop7;
	if(s.prop8&&!s.eVar8) s.eVar8=s.prop8;
	if(s.prop9&&!s.eVar9) s.eVar9=s.prop9;
	if(s.prop10&&!s.eVar10) s.eVar10=s.prop10;
	if(s.prop11&&!s.eVar11) s.eVar11=s.prop11;
	if(s.prop12&&!s.eVar12) s.eVar12=s.prop12;
	if(s.prop13&&!s.eVar13) s.eVar13=s.prop13;
	if(s.prop14&&!s.eVar16) s.eVar16=s.prop14;
	if(s.prop15&&!s.eVar17) s.eVar17=s.prop15;
	if(s.prop16&&!s.eVar18) s.eVar18=s.prop16;
	if(s.prop17&&!s.eVar19) s.eVar19=s.prop17;
	if(s.prop18&&!s.eVar20) s.eVar20=s.prop18;
}
s.doPlugins=s_doPlugins
/************************** PLUGINS SECTION *************************/
/* You may insert any plugins you wish to use here.                 */

/*
 * Plugin: Days since last Visit 1.1.H - capture time from last visit
 */
s.getDaysSinceLastVisit=new Function("c",""
+"var s=this,e=new Date(),es=new Date(),cval,cval_s,cval_ss,ct=e.getT"
+"ime(),day=24*60*60*1000,f1,f2,f3,f4,f5;e.setTime(ct+3*365*day);es.s"
+"etTime(ct+30*60*1000);f0='Cookies Not Supported';f1='First Visit';f"
+"2='More than 30 days';f3='More than 7 days';f4='Less than 7 days';f"
+"5='Less than 1 day';cval=s.c_r(c);if(cval.length==0){s.c_w(c,ct,e);"
+"s.c_w(c+'_s',f1,es);}else{var d=ct-cval;if(d>30*60*1000){if(d>30*da"
+"y){s.c_w(c,ct,e);s.c_w(c+'_s',f2,es);}else if(d<30*day+1 && d>7*day"
+"){s.c_w(c,ct,e);s.c_w(c+'_s',f3,es);}else if(d<7*day+1 && d>day){s."
+"c_w(c,ct,e);s.c_w(c+'_s',f4,es);}else if(d<day+1){s.c_w(c,ct,e);s.c"
+"_w(c+'_s',f5,es);}}else{s.c_w(c,ct,e);cval_ss=s.c_r(c+'_s');s.c_w(c"
+"+'_s',cval_ss,es);}}cval_s=s.c_r(c+'_s');if(cval_s.length==0) retur"
+"n f0;else if(cval_s!=f1&&cval_s!=f2&&cval_s!=f3&&cval_s!=f4&&cval_s"
+"!=f5) return '';else return cval_s;");

/*
 * Plugin: getQueryParam 2.1 - return query string parameter(s)
 */
s.getQueryParam=new Function("p","d","u",""
+"var s=this,v='',i,t;d=d?d:'';u=u?u:(s.pageURL?s.pageURL:s.wd.locati"
+"on);if(u=='f')u=s.gtfs().location;while(p){i=p.indexOf(',');i=i<0?p"
+".length:i;t=s.p_gpv(p.substring(0,i),u+'');if(t)v+=v?d+t:t;p=p.subs"
+"tring(i==p.length?i:i+1)}return v");
s.p_gpv=new Function("k","u",""
+"var s=this,v='',i=u.indexOf('?'),q;if(k&&i>-1){q=u.substring(i+1);v"
+"=s.pt(q,'&','p_gvf',k)}return v");
s.p_gvf=new Function("t","k",""
+"if(t){var s=this,i=t.indexOf('='),p=i<0?t:t.substring(0,i),v=i<0?'T"
+"rue':t.substring(i+1);if(p.toLowerCase()==k.toLowerCase())return s."
+"epa(v)}return ''");

/*********************************************************************
* Function getValOnce(v,c,e): return v if that value is not found in
*                  the cookie 'c'. If v has a value, write the cookie
*                  'c' which expires at 'e' days (0 for session).
*     v = Value to write in cookie or return
*     c = Cookie Name - something like 's_campaign'
*     e = Number of days to expiration - 0 for session
* Returns:
*     v or ''
*
* TEST CASES:
* 1. Page A: s.campaign="123"
* 2. Page A: s.campaign=s.getValOnce(s.campaign,"cname",0)
* 3. Page B: s.campaign="" (cookie value is not overwritten)
* 4. Page A: (user clicks "back") s.campaign=""
* This will de-inflate click-throughs due to back button
*********************************************************************/

/*
 * Plugin: getValOnce 0.2 - get a value once per session or number of days
 */
s.getValOnce=new Function("v","c","e",""
+"var s=this,k=s.c_r(c),a=new Date;e=e?e:0;if(v){a.setTime(a.getTime("
+")+e*86400000);s.c_w(c,v,e?a:0);}return v==k?'':v");

/*
 * Plugin: getTimeParting 1.4 - Set timeparting values based on time zone (15 min)
 */

s.getTimeParting=new Function("t","z","y",""
+"dc=new Date('1/1/2000');var f=15;var ne=8;if(dc.getDay()!=6||"
+"dc.getMonth()!=0){return'Data Not Available'}else{;z=parseInt(z);"
+"if(y=='2009'){f=8;ne=1};gmar=new Date('3/1/'+y);dsts=f-gmar.getDay("
+");gnov=new Date('11/1/'+y);dste=ne-gnov.getDay();spr=new Date('3/'"
+"+dsts+'/'+y);fl=new Date('11/'+dste+'/'+y);cd=new Date();"
+"if(cd>spr&&cd<fl){z=z+1}else{z=z};utc=cd.getTime()+(cd.getTimezoneO"
+"ffset()*60000);tz=new Date(utc + (3600000*z));thisy=tz.getFullYear("
+");var days=['Sunday','Monday','Tuesday','Wednesday','Thursday','Fr"
+"iday','Saturday'];if(thisy!=y){return'Data Not Available'}else{;thi"
+"sh=tz.getHours();thismin=tz.getMinutes();thisd=tz.getDay();var dow="
+"days[thisd];var ap='AM';var dt='Weekday';var mint='00';if(thismin>1"
+"5&&thismin<30){mint='15'}if(thismin>30&&thismin<45){mint='30'}if(th"
+"ismin>45&&thismin<60){mint='45'}"
+"if(thish>=12){ap='PM';thish=thish-12};if (thish==0){th"
+"ish=12};if(thisd==6||thisd==0){dt='Weekend'};var timestring=thish+'"
+":'+mint+ap;var daystring=dow;var endstring=dt;if(t=='h'){return tim"
+"estring}if(t=='d'){return daystring};if(t=='w'){return en"
+"dstring}}};"
);

/*
 * Plugin Utility: apl v1.1
 */
s.apl=new Function("L","v","d","u",""
+"var s=this,m=0;if(!L)L='';if(u){var i,n,a=s.split(L,d);for(i=0;i<a."
+"length;i++){n=a[i];m=m||(u==1?(n==v):(n.toLowerCase()==v.toLowerCas"
+"e()));}}if(!m)L=L?L+d+v:v;return L");

/*
 * Utility Function: split v1.5 - split a string (JS 1.0 compatible)
 */
s.split=new Function("l","d",""
+"var i,x=0,a=new Array;while(l){i=l.indexOf(d);i=i>-1?i:l.length;a[x"
+"++]=l.substring(0,i);l=l.substring(i+d.length);}return a");


/* WARNING: Changing any of the below variables will cause drastic
changes to how your visitor data is collected.  Changes should only be
made when instructed to do so by your account manager.*/
s.visitorNamespace="metrointernational"
s.trackingServer="metrics.metrofrance.com"
s.trackingServerSecure="smetrics.metrofrance.com"
s.dc=112

/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
var s_code='',s_objectID;function s_gi(un,pg,ss){var d="function s_dr"
+"(x,o,n){var i=x.indexOf(o);if(i>=0&&x.split)x=(x.split(o)).join(n);"
+"else while(i>=0){x=x.substring(0,i)+n+x.substring(i+o.length);i=x.i"
+"ndexOf(o)}return x}function s_d(x) {var t='`^@$#',l='0123456789ABCD"
+"EFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz',d,n=0,b,k,w,i=x.l"
+"astIndexOf('~~');if(i>0){d=x.substring(0,i);x=x.substring(i+2);whil"
+"e(d){w=d;i=d.indexOf('~');if(i>0){w=d.substring(0,i);d=d.substring("
+"i+1)}else d='';b=parseInt(n/62);k=n-b*62;k=t.substring(b,b+1)+l.sub"
+"string(k,k+1);x=s_dr(x,k,w);n++}for(i=0;i<5;i++){w=t.substring(i,i+"
+"1);x=s_dr(x,w+' ',w)}}return x}",c=".substring(~.indexOf(~return ~="
+"f`K(~){`Ls=^u~';`At`g~;$1~.toLowerCase()~`YF`K('e`z`Ls=s_c_il['+@f+"
+"']~};s.~`r $1~.length~`YObject~.toUpperCase~s.wd~.location~')q='~s."
+"apv~s.`v~$d$T~unction~var ~s.pt(~ookieDomainPeriods~,`z,'~while(~.p"
+"rotocol~){$1~);s.~:'')~;@E^Us[k],255)}~=''~javaEnabled~connection^E"
+"~=new ~.lastIndexOf('~tm.get~@5\"$Ns.b.addBehavior('# default# ~onc"
+"lick~ternalFilters~entElement~Name~=='~javascriptVersion~=parseFloa"
+"t(~=s.dynamicAccount~s_c_il['+@f+'].mrq(\"'+un+'\")'~visitor~cookie"
+"~parseInt(~s.^I~o@6oid~browser~else~referrer~colorDepth~String~link"
+"~.host~s.rep(~}catch(e){~','~r=s.m(f)?s[f](~}$1~s.un~s.eo~s.sq~t=s."
+"ot(o)~track~j='1.~)?'Y':'N'~$XURL~@6c_i~s.ismac~lugins~;for(~Type~s"
+".rc[un]~s.b.addEventListener~Download~tfs~resolution~.get@H()~s.eh~"
+"s.isie~s.vl_l~s.vl_t~Height~t,h){t=t?t~isopera~escape(~screen.~s.fl"
+"(~harCode~&&(~variableProvider~s.gg('objectID')~&&s.~:'';h=h?h~e&&l"
+"$cSESSION'~');~f',~Date~name~home$X~s_c2~s.c_r(~s.rl[u~o.href~Lifet"
+"ime~Width~sEnabled~'){q='~transactionID~b.attachEvent~&&l$cNONE'){~"
+"ExternalLinks~_'+~this~charSet~onerror~currencyCode~s=s_gi(~e$PElem"
+"ent~;s.gl(s.vl_g~.parent~Array~lnk~Opera~eval(~.s_~Math.~s.fsg~s.ns"
+"6~docum~s.oun~InlineStats~'0123456789~s[k]=~window~onload~Time~s.ep"
+"a(~s.c_w(~(s.ssl~n=s.oid(o)~LeaveQuery~')>=~&&t~'=')~){n=~+1))~' '+"
+"~s.t()}~\",''),~=s.oh(o);~+(y<1900?~ingServer~true~sess~campaign~li"
+"f~s_gs(~,100)~s.co(~s._in~x in ~='s_~ffset~s.c_d~'&pe~s.gv(~s.qav~s"
+".pl~=(apn~Sampling~sqs',q);~Year(~=s.n.app~(''+~)+'/~',s~'||t~s()+'"
+":'+~a):f(~){v=s.n.~channel~if(~un)~.target~o.value~\".tl(\")~etscap"
+"e~(ns?ns:~s_')t=t~omePage~++}~&&!~')<~){x~1);~e))~'+n~height~events"
+"~trk~random~code~un,~try{~'MSIE ~.src~floor(~s.pg~s.num(~s.ape(~s.c"
+"_gd~s.dc~.inner~page~.set~.fromC~++){~?'':~!='~='+~?'&~+';~(f){~){p"
+"=~>=5)~&&i>~[b](~=l[n];~~f`K ^hfe$g`Lx`V,s=0,e,a,b,c;`P1){e=f`1'\"@"
+"v);b=f`1'\\\\',s);c=f`1\"\\n\",s)`6e<0||(b>=0&&b<$Fe=b`6e<0||(c>=0&"
+"&c<$Fe=c`6e>=0$D+=(e>s?f`0s,e)`T+(e==c?'\\\\n':'\\\\'+f`0e,e@R;s=e+"
+"1}`r `2x+f`0s)}`2f}f`K ^hfa$g`Ls=f`1'(')+1,e=f`1')'),a`V,c;`Ps>=0&&"
+"s<e){c=f`0s,s+1)`6c`g,')a+='\",\"';`A(\"\\n\\r\\t \")`1c)<0)a+=c;s$"
+"A`2a?'\"'+a+'\"':a}f`K ^hf(cc){cc`V+cc;`Lfc='`Lf`YF`K(@v=cc`1';',cc"
+"`1'{')),e=cc`Z}'),o,a,d,q,c,f,h,x;fc+=^hfa(cc)+',\"`Ls`C;';c=cc`0s+"
+"1,e);s=c`1'f`K^c`Ps>=0){d=1;q`V;x=0;f=c`0s);a=^hfa(f);e=o=c`1'{@v);"
+"e++;`Pd>0){h=c`0e,e+1)`6q`Rh==q$Bx)q`V`6h`g\\\\')x=x?0:1;`r x=0}`r{"
+"$1h`g\"'||h==\"'\")q=h`6h`g{')d++`6h`g}')d--^1d>0)e$Ac=c`00,s)+'new"
+" F`K('+(a?a+`z`T+'\"'+^hfe(c`0o+1,$F+'\")'+c`0e+$Es=c`1'f`K')}fc+=^"
+"hfe(c)$f`2s\");';@5fc);`2f}f`K s_co(o){`L^y\"^ \",1,$E`2@eo)}f`K @c"
+"$2{`L^y$M1,$E`2@Tf`K s_dc($2{`L^y$M$E`2@Tf`K s_c($Mpg,ss`4;s._c@hc'"
+";`E=@F`6!`E^An){`E^Al`Y@2;`E^An=0;}s._il=`E^Al;@f=`E^An;s._il[@f]=s"
+";`E^An++;s.m`3m){`2@tm)`1'{$C0`9fl`3x,l){`2x?@tx)`00,l):x`9co`3o`R!"
+"o)`2o;`Ln`C,x^D@go)$1x`1'select$C0&&x`1'filter$C0)n[x]=o[x];`2n`9nu"
+"m`3x$D`V+x^D`Lp=0;p<x`B;p++)$1(@D')`1x`0p,p@R<0)`20;`21`9rep`3x,o,n"
+"){`Li=x`1o);`Px$j=0$D=x`00,i)+n+x`0i+o`B);i=x`1o,i+n`B)}`2x`9ape`3x"
+"`4,h=@DABCDEF',i,c=s.^v,n,l,e,y`V;c=c?c`D():''`6x$D`V+x`6c`gAUTO'^W"
+"'').c^VAt){for(i=0;i<x`B;i$ac=x`0i,i+$En=x.c^VAt(i)`6n>127){l=0;e`V"
+";`Pn||l<4){e=h`0n%16,n%16+1)+e;n=`nn/16);l$Ay+='%u'+e}`Ac`g+')y+='%"
+"2B';`r y+=^Sc)}x=y}`r{x=x?`x^S''+x),'+`z%2B'):x`6x&&c^Zem==1&&x`1'%"
+"u$C0&&x`1'%U$C0){i=x`1'%^c`Pi>=0){i++`6h`08)`1x`0i,i+1)`D())>=0)`2x"
+"`00,i)+'u00'+x`0i);i=x`1'%',i)}}}}`2x`9epa`3x`4;`2x?un^S`x''+x,'+`z"
+" ')):x`9pt`3x,d,f,a`4,t=x,z=0,y,r;`Pt){y=t`1d);y=y<0?t`B:y;t=t`00,y"
+");^0t,@yt,a)`6r)`2r;z+=y+d`B;t=x`0z,x`B);t=z<x`B?t:''}`2''`9isf`3t,"
+"a){`Lc=a`1':')`6c>=0)a=a`00,c)`6t`00,2)`g$8`02);`2(t!`V@O==a)`9fsf`"
+"3t,a`4`6`Ma`Ois^dt))@8+=(@8!`V?`z`T+t;`20`9fs`3x,f`4;@8`V;`Mx`Ofs^d"
+"f);`2@8`9c_d`V;$Uf`3t,a`4`6!$St))`21;`20`9c_gd`3`4,d=`E`F`w^f,n=s.f"
+"pC`N,p`6!n)n=s.c`N`6d$B@j@Qn?`nn):2;n=n>2?n:2;p=d`Z.')`6p>=0){`Pp>="
+"0&&n>1$hd`Z.',p-$En--}@j=p>0&&`Md,'.`zc_gd^d0)?d`0p):d}}`2@j`9c_r`3"
+"k`4;k=$Tk);`Lc=@Ss.d.`m,i=c`1@Sk+@P,e=i<0?i:c`1';',i),v=i<0$b@Ic`0i"
+"+2+k`B,e<0?c`B:$F;`2v$c[[B]]'?v:''`9c_w`3k,v,e`4,d=$U(),l=s.`m^l,t;"
+"v`V+v;l=l?@tl)`D():''`6^b^rt=(v!`V?`nl?l:0):-60)`6t){e`Y^e;e$Y@H(e^"
+"K+(t*1000))}^1k^rs.d.`m=k+'`Jv!`V?v:'[[B]]')$f path=/;'+(^b?' expir"
+"es$de.toGMT`u()$f'`T+(d?' domain$dd$f'`T;`2^ik)==v}`20`9eh`3o,e,r,f"
+"`4,b='s^te+'^t@f,n=-1,l,i,x`6!^Ll)^Ll`Y@2;l=^Ll^Di=0;i<l`B&&n<0;i++"
+"`Rl[i].o==o&&l[i].e==e)n=i^1n<0@Qi;l[n]`C}x$lx.o=o;x.e=e;f=r?x.b:f`"
+"6r||f$D.b=r?0:o[e];x.o[e]=f^1x.b$D.o[b]=x.b;`2b}`20`9cet`3f,a,t,o,b"
+"`4,r`6`H>=5^W!s.^R||`H>=7))@5'$N^0@ya)`yr=s.m(t)?s[t](e):t(e)}^c`r{"
+"$1^B^Zu`1$O4@N0)r=s.m(b)?s$ka):b(a);`r{^L(`E,'^w',0,o);^0@ya`Seh(`E"
+",'^w',1)}}`2r`9g^Iet`3e`4;`2`o`9g^Ioe`8;^L(@F,\"^w\",1`Se^I=1;`Lc=s"
+".t()`6c)s.d.write(c`Se^I=0;`2@Y'`Sg^Ifb`3a){`2@F`9g^If`3w`4,p=w@1,l"
+"=w`F;`o=w`6p&&p`F!=l&&p`F`w==l`w){`o=p;`2s.g^If(`o)}`2`o`9g^I`3`4`6"
+"!`o){`o=`E`6!s.e^I)`o=s.cet('g^I^d`o,'g^Iet@v.g^Ioe,'g^Ifb')}`2`o`9"
+"mrq`3u`4,l=^j],n,r;^j]=0`6l)for(n=0;n<l`B;n$ar$ls.mr(0,0,r.t,r.u,r."
+"r)}`9mr`3@Z,q,ta,u,rs`4,dc=$V,t1=s.^6@X,t2=s.^6@XSecure,ns=s.`l`fsp"
+"ace,un=u?u:$7s.f$2,unc=`x$M'_`z-'),r`C,l,imn@hi^t($2,im,b,e`6!rs){r"
+"s='http'+@K?'s'`T+'://'+(t1?@K@O2?t2:t1):($7@K?'102':unc))+'.'+($V?"
+"$V:112)+'.2o7.net')@ub/ss/'+^2+'/1/H.12-Pdvu-2/'+@Z+'?[AQB]&ndh=1'+"
+"(q?q`T+'&[AQE]'`6^M$B^B`R`H>5.5)rs=^Urs,4095);`r rs=^Urs,2047)}^1s."
+"d.images&&`H>=3^W!s.^R||`H>=7)^W@9<0||`H>=6.1)`R!s.rc)s.rc`C`6!^F){"
+"^F=1`6!s.rl)s.rl`C;^jn]`Y@2;set@Hout('`k,750)}`r{l=^jn]`6l){r.t=ta;"
+"r.u=un;r.r=rs;l[l`B]=r;`2''}imn+='^t^F;^F$Aim=`E[imn]`6!im)im=`E[im"
+"n]`YImage;im@6l=0;im.@G`YF`K('e`z^u@6l=1;`k);im$P=rs`6rs`1@k=@N0^W!"
+"ta||ta`g_self@wa`g_top'||(`E.^f@Oa==`E.^f))){b=e`Y^e;`P!im@6l&&e^K-"
+"b^K<500)e`Y^e}`2''}`2'<im'+'g sr'+'c=\"'+rs+'\" width=1 $H=1 border"
+"=0 alt=\"\">'`9gg`3v`4;`2`E['s^tv]`9glf`3t,a`Rt`00,2)`g$8`02);`Ls=^"
+"u,v=s.gg(t)`6v)s[t]=v`9gl`3v`4`6$R)`Mv`Ogl^d0)`9gv`3v`4;`2s['vpm^tv"
+"]?s['vpv^tv]:(s[v]?s[v]`T`9havf`3t,a`4,b=t`00,4),x=t`04),n=`nx),k='"
+"g^tt,m='vpm^tt,q=t,v=`ITrackVars,e=`ITrackEvents;@E@lt)`6s.@3||^3){"
+"v=v?v+`z+^N+`z+^N2:''`6v$B`Mv`Ois^dt))s[k]`V`6t`g$I'&&e)@Es.fs(s[k]"
+",e)}s[m]=0`6t`g`lID`Gvid`5^9^og'`U`At`g`s^or'`U`At`gvmk`Gvmt`5^v^oc"
+"e'`6s[k]&&s[k]`D()`gAUTO')@E'ISO8859-1';`As[k]^Zem==2)@E'UTF-8'}`At"
+"`g`l`fspace`Gns`5c`N`Gcdp`5`m^l`Gcl`5^X`Gvvp`5^x`Gcc`5$0`Gch`5^p`Gx"
+"act`5@a`Gv0`5^J`Gs`5`t`Gc`5`h`Gj`5`W`Gv`5`m^n`Gk`5`q^m`Gbw`5`q^P`Gb"
+"h`5`X`Gct`5^g`Ghp`5p^C`Gp';`A$Sx)`Rb`gprop`Gc$G;`Ab`geVar`Gv$G;`Ab`"
+"ghier^oh$G`U^1s[k]@O$c`v`f'@O$c`v^E')@m+='&'+q+'`Js[k]);`2''`9hav`3"
+"`4;@m`V;`M^O`Ohav^d0);`2@m`9lnf`3^Q`7^a`7:'';`Lte=t`1@P`6t@Oe>0&&h`"
+"1t`0te@R>=0)`2t`00,te);`2''`9ln`3h`4,n=`I`fs`6n)`2`Mn`Oln^dh);`2''`"
+"9ltdf`3^Q`7^a`7:'';`Lqi=h`1'?^ch=qi>=0?h`00,qi):h`6t&&h`0h`B-(t`B@R"
+"`g.'+t)`21;`20`9ltef`3^Q`7^a`7:''`6t&&h`1t)>=0)`21;`20`9lt`3h`4,lft"
+"=`I^HFile^Es,lef=`IEx`d,@b=`IIn`d;@b=@b?@b:`E`F`w^f;h=h`7`6s.^6^HLi"
+"nks&&lft&&`Mlft`Oltd^dh))`2'd'`6s.^6^s^Wlef||@b)^W!lef||`Mlef`Olte^"
+"dh))^W!@b||!`M@b`Olte^dh)))`2'e';`2''`9lc`8,b=^L(^u,\"`c\"`S@3=@e^u"
+"`St(`S@3=0`6b)`2^u$ke);`2@Y'`Sbc`8,f`6s.d^Zd.all^Zd.all.cppXYctnr)r"
+"eturn;^3=^z?^z:e$3;@5\"$N$1^3^W^3.tag`f||^3.par`e||^3@1Nod$F@Tcatch"
+"$g}\"`Seo=0'`Soh`3o`4,l=`E`F,h=^k?^k:'',i,j,k,p;i=h`1':^cj=h`1'?^ck"
+"=h`1'/')`6h^Wi<0||(j>=0$jj)||(k>=0$jk))$ho`Q&&o`Q`B>1?o`Q:(l`Q?l`Q`"
+"T;i=l.path^f`Z/^ch=(p?p+'//'`T+(o`w?o`w:(l`w?l`w`T)+(h`00,1)$c/'?l."
+"path^f`00,i<0?0:i@u'`T+h}`2h`9ot`3o){`La=o.type,b=o.tag`f;`2(a&&a`D"
+"?a:b&&b`D?b:^k?'A'`T`D()`9oid`3o`4,^5,p=o`Q,c=o.`c,n`V,x=0`6!`p`R^k"
+"^Wt`gA@w`gAREA')^W!c||!p||p`7`1'javascript$C0))n@V`Ac@Q`xs.rep(`xs."
+"rep@tc,\"\\r@U\"\\n@U\"\\t@U' `z^cx=2}`A$4^Wt`gINPUT@w`gSUBMIT')@Q$"
+"4;x=3}`Ao$P@O`gIMAGE')n=o$P`6n){`p=^Un@d;`pt=x}}`2`p`9rqf`3t,un`4,e"
+"=t`1@P,u=e>=0?`z+t`00,e)+`z:'';`2u&&u`1`z+un+`z)>=0?@It`0e@R:''`9rq"
+"`3un`4,c=un`1`z),v=^i's_sq'),q`V`6c<0)`2`Mv,'&`zrq^d$2;`2`Mun`Orq',"
+"0)`9sqp`3t,a`4,e=t`1@P,q=e<0$b@It`0e+1)`Ssqq[q]`V`6e>=0)`Mt`00,e)`O"
+"@q`20`9sqs`3$Mq`4;^4u[un]=q;`20`9sq`3q`4,k@hsq',v=^ik),x,c=0;^4q`C;"
+"^4u`C;^4q[q]`V;`Mv,'&`zsqp',0);`M^2`O@qv`V^D@g^4u)^4q[^4u[x]]+=(^4q"
+"[^4u[x]]?`z`T+x^D@g^4q)$1x&&^4q[x]^Wx==q||c<2)){v+=(v$e'`T+^4q[x]+'"
+"`Jx);c$A`2@Jk,v,0)`9wdl`8,r=@Y,b=^L(`E,\"@G\"),i,o,oc`6b)r=^u$ke)^D"
+"i=0;i<s.d.`vs`B;i$ao=s.d.`vs[i];oc=o.`c?\"\"+o.`c:\"\"`6(oc`1\"@c\""
+")<0||oc`1\"@6oc(\")>=0)&&oc`1$5<0)^L(o,\"`c\",0,s.lc);}`2r^c`Es`3`4"
+"`6`H>3^W!^M||!^B||`H$i`Rs.b^Z^q)s.^q('`c@v.bc);`As.b&&^G)^G('click@"
+"v.bc,false);`r ^L(`E,'@G',0,`El)}`9vs`3x`4,v=s.`l@p,g=s.`l@pGroup,k"
+"@hvsn^t^2+(g?'^tg`T,n=^ik),e`Y^e,y=e.get@r);e$Y@ry+10@W1900:0))`6v)"
+"{v*=100`6!n`R!@Jk,x,$F`20;n=x^1n%10000>v)`20}`21`9dyasmf`3t,m`Rt&&m"
+"&&m`1t)>=0)`21;`20`9dyasf`3t,m`4,i=t?t`1@P:-1,n,x`6i>=0&&m){`Ln=t`0"
+"0,i),x=t`0i+1)`6`Mx`Odyasm^dm))`2n}`20`9uns`3`4,x`jSelection,l`jLis"
+"t,m`jMatch,n,i;^2=^2`7`6x&&l`R!m)m=`E`F`w`6!m.toLowerCase)m`V+m;l=l"
+"`7;m=m`7;n=`Ml,';`zdyas^dm)`6n)^2=n}i=^2`1`z`Sfun=i<0?^2:^2`00,i)`9"
+"sa`3un`4;^2=un`6!@B)@B=un;`A(`z+@B+`z)`1$2<0)@B+=`z+un;^2s()`9t`3`4"
+",$J=1,tm`Y^e,sed=Math&&@7$K?@7$Q@7$K()*10000000000000):`a@H(),@Z='s"
+"'+@7$Q`a@H()/10800000)%10+sed,y=`a@r),vt=`a^e(@u'+`aMonth(@u'@Wy+19"
+"00:y)+@S`aHour@x`aMinute@x`aSeconds()+@S`aDay()+@S`a@HzoneO@i(),^I="
+"s.g^I(),ta`V,q`V,qs`V@0`Suns()`6!s.td){`Ltl=^I`F,a,o,i,x`V,c`V,v`V,"
+"p`V,bw`V,bh`V,^70',k=@J's_cc`z@Y',0^8,hp`V,ct`V,pn=0,ps`6`u&&`u.pro"
+"totype){^71'`6j.match){^72'`6tm$YUTC^e){^73'`6^M&&^B&&`H$i^74'`6pn."
+"toPrecision){^75';a`Y@2`6a.forEach){^76';i=0;o`C;@5'$Ni`YIterator(o"
+")`y}')`6i&&i.next)^77'}}}}^1`H>=4)x=^Twidth+'x'+^T$H`6s.isns||s.^R`"
+"R`H>=3@z`W(^8`6`H>=4){c=^TpixelDepth;bw=`E$W^m;bh=`E$W^P}}@n=s.n.p^"
+"C}`A^M`R`H>=4@z`W(^8;c=^T`t`6`H$i{bw=s.d.@A`e.o@i^m;bh=s.d.@A`e.o@i"
+"^P`6!^B^Zb){`bh$9^chp=s.b.isH$9(tl^8`y}\");`bclientCaps^cct=s.b.`X`"
+"y}\")}}}`r r`V^1@n)`Ppn<@n`B&&pn<30){ps=^U@n[pn].^f@d$f'`6p`1ps)<0)"
+"p+=ps;pn$As.^J=x;s.`t=c;s.`h=j;s.`W=v;s.`m^n=k;s.`q^m=bw;s.`q^P=bh;"
+"s.`X=ct;s.^g=hp;s.p^C=p;s.td=1^1s.useP^C)s.doP^C(s);`Ll=`E`F,r=^I.@"
+"Aent.`s`6!s.^9)s.^9=l`6!s.`s)s.`s=r`6s.@3||^3){`Lo=^3?^3:s.@3`6!o)`"
+"2'';`Lp=@l'$X`f'),w=1,^5,@L,x=`pt,h,l,i,oc`6^3&&o==^3){`Po$Bn@O$cBO"
+"DY'){o=o.par`e?o.par`e:o@1Node`6!o)`2'';^5;@L;x=`pt}oc=o.`c?''+o.`c"
+":''`6(oc`1\"@c\")>=0&&oc`1\"@6oc(\")<0)||oc`1$5>=0)`2''}ta=n?o$3:1;"
+"h@Vi=h`1'?^ch=`I@M`u||i<0?h:h`00,i);l=`I`f?`I`f:s.ln(h);t=`I^E?`I^E"
+"`7:s.lt(h)`6t^Wh||l))q+=@k=@3^t(t`gd@w`ge'?$Tt):'o')+(h?@kv1`Jh)`T+"
+"(l?@kv2`Jl)`T;`r $J=0`6s.^6@C`R!p$h@l'^9^cw=0}^5;i=o.sourceIndex`6^"
+"Y@Q^Y;x=1;i=1^1p&&n@O)qs='&pid`J^Up,255))+(w$epidt$dw`T+'&oid`J^Un@"
+"d)+(x$eoidt$dx`T+'&ot`Jt)+(i$eoi$di`T}^1!$J$Bqs)`2''`6s.p_r)s.p_r()"
+";`L$L`V`6$J^Zvs(sed))$L=s.mr(@Z,(vt$et`Jvt)`T+s.hav()+q+(qs?qs:s.rq"
+"(^2)),ta`Ssq($J$bqs`S@3=^3=`I`f=`I^E=`E@6objectID=s.ppu`V`6$R)`E@6@"
+"3=`E@6eo=`E@6`v`f=`E@6`v^E`V;`2$L`9tl`3o,t,n`4;s.@3=@eo);`I^E=t;`I`"
+"f=n;s.t()`9ssl=(`E`F`Q`7`1'https@N0`Sd=@Aent;s.b=s.d.body;s.n=navig"
+"ator;s.u=s.n.userAgent;@9=s.u`1'N$66/^c`Lapn@s`f,v@sVersion,ie=v`1$"
+"O'),o=s.u`1'@4 '),i`6v`1'@4@N0||o>0)apn='@4';^M@o`gMicrosoft Intern"
+"et Explorer'`Sisns@o`gN$6'`S^R@o`g@4'`Sismac=(s.u`1'Mac@N0)`6o>0)`H"
+"`is.u`0o+6));`Aie>0){`H=`ni=v`0ie+5))`6`H>3)`H`ii)}`A@9>0)`H`is.u`0"
+"@9+10));`r `H`iv`Sem=0`6`u$Z^V){i=^S`u$Z^V(256))`D(`Sem=(i`g%C4%80'"
+"?2:(i`g%U0100'?1:0))}s.sa(un`Svl_l='`lID,vmk,ppu,^v,`l`fspace,c`N,`"
+"m^l,$X`f,^9,`s,^x,purchaseID';^O=^N+',^X,$0,server,$X^E,^p,@a,state"
+",zip,$I,products,`v`f,`v^E'^D`Ln=1;n<51;n++)^O+=',prop$G+',eVar$G+'"
+",hier$G;^N2='^J,`t,`h,`W,`m^n,`q^m,`q^P,`X,^g,p^C';^O+=`z+^N2;s.vl_"
+"g=^O+',^6^HLinks,^6^s,^6@C,`v@M`u,`v^HFile^Es,`vEx`d,`vIn`d,`v`fs,@"
+"3';$R=pg@0)`6!ss)`Es()}",
w=window,l=w.s_c_il,n=navigator,u=n.userAgent,v=n.appVersion,e=
v.indexOf('MSIE '),m=u.indexOf('Netscape6/'),a,i,s;if(un){un=
un.toLowerCase();if(l)for(i=0;i<l.length;i++){s=l[i];if(s._c=='s_c'){
if(s.oun==un)return s;else if(s.fs(s.oun,un)){s.sa(un);return s}}}}
w.eval(d);c=s_d(c);i=c.indexOf("function s_c(");w.eval(c.substring(0,i
));if(!un)return 0;c=c.substring(i);if(e>0){a=parseInt(i=v.substring(e
+5));if(a>3)a=parseFloat(i)}else if(m>0)a=parseFloat(u.substring(m+10)
);else a=parseFloat(v);if(a>=5&&v.indexOf('Opera')<0&&u.indexOf(
'Opera')<0){eval(c);return new s_c(un,pg,ss)}else s=s_c2f(c);return s(
un,pg,ss)}s_gi()

<!--cache control: force proto cache-->
// If this file is included, omniture is enabled
var omniture_enabled = true;

//Define strBoxName for later use
var strBoxName;

// -----------------------
// FUNCTION: fTrackBoxPosition
// DESCRIPTION: Tracks what { section | margin } box the push was in on click.
// ARGUMENTS: sReportSuiteID (Report Suite Id), sBoxName (box name), sLinkName
// RETURN: -
// -----------------------
function fTrackBoxPosition(sReportSuiteID, sBoxName, sLinkName) {
	//alert("sReportSuiteID: " + sReportSuiteID + ", sBoxName: " + sBoxName + ", sLinkName: " + sLinkName);
	var s=s_gi(sReportSuiteID);
	s.eVar21=sBoxName;	
	s.linkTrackVars='eVar21';
	s.tl(this, 'o', sLinkName);
}

// -----------------------
// FUNCTION: fTrackFeaturedContentClicks
// DESCRIPTION: Tracks tabs and thumbnails in 
//              featured content component (carousel).
// ARGUMENTS: sReportSuiteID (Report Suite Id), sFeaturedContentId, sLinkName
// RETURN: -
// -----------------------
function fTrackFeaturedContentClicks(sReportSuiteID, sFeaturedContentId, sLinkName) {
        //alert("sReportSuiteID: "+sReportSuiteID+", sFeaturedContentId: "+sFeaturedContentId+", sLinkName: "+sLinkName);
	var s=s_gi(sReportSuiteID);
	s.events='event6';
	s.eVar14= sFeaturedContentId;

	// These are also set in the omniture_s_code.js file,
	// But for some reason it didn't work as for all the 
	// other variables, so I'm setting them here again:
	if(s.prop17&&!s.eVar19) s.eVar19=s.prop17;
	if(s.prop18&&!s.eVar20) s.eVar20=s.prop18;
	s.linkTrackEvents='event6';
	s.linkTrackVars='events,eVar2,eVar3,eVar4,eVar5,eVar6,eVar7,eVar8,eVar9,eVar10,eVar11,eVar12,eVar13,eVar10,eVar14,eVar16,eVar17,eVar18,eVar19,eVar20,eVar21';
	s.tl(this, 'o', sLinkName);
}


// -----------------------
// FUNCTION: fSetOnclickAttribute
// DESCRIPTION: Sets an onclick event on links sending necessary data to track box name.
// ARGUMENTS: sDiv
// RETURNS: 
// -----------------------
function fSetOnclickAttribute(sDiv, strPagePath) {
  //alert(sDiv + ', ' + strPagePath);
  if(document.getElementById(sDiv)){
    var a_tags = document.getElementById(sDiv).getElementsByTagName("a");

    for (var i = 0; i < a_tags.length; i++) {
      var a_tag = a_tags[i];
      if (a_tag.href) {
        if (a_tag.href.indexOf("mailto:") == -1){
	  // Don't override existing onclick attributes
	    if(!a_tag.getAttribute("onclick")){
	      a_tag.onclick = new Function ("fTrackBoxPosition(s_account, '" + sDiv + "', '" + strPagePath + " - " + fEscapeSpecialChars(a_tag.innerHTML) + "'); return checkCityCookie('"+ a_tag.getAttribute("href", 2) +"');");

	  }
        }
      }
    }
  }
}

//----------------------------
//GLOBAL VARIABLE
var nTotalCharacters = 5000;
var nCurrentPage=1;
var nTotalCommentsDisplayed=10;
//----------------------------

// -----------------------
// FUNCTION: fShowHideElement
// DESCRIPTION: A function shows or hides an element.
// ARGUMENTS: sElemendId
// RETURN: None
// -----------------------
function fShowHideElement(sElementId) {
	var eElement = document.getElementById(sElementId);
	var sClassName = eElement.className;
	if (sClassName.match(' access-text') || sClassName.match('access-text')) {
		// Shows element
		eElement.className = eElement.className.replace(/access-text/, '');
	} else {
		// Hides element
		eElement.className = sClassName + ' access-text';
	}
}
// -----------------------
// FUNCTION: fShowHideSwapper
// DESCRIPTION: A function that hides one element and shows another - uses fShowHideElement
// ARGUMENTS: sElementId sVariableName
// RETURN: None but updates the global variable for the module that called it
// -----------------------
function fShowHideSwapper(sElementId, sVariableName) {

	var sHideElement = eval(sVariableName);
	if (sHideElement == sElementId) {
		fShowHideElement(sElementId);
		sVariableName += ' = \'none\';';
		eval(sVariableName);
	}else {
		if (sHideElement != 'none') {
			fShowHideElement(sHideElement);
		}
		fShowHideElement(sElementId);
		sVariableName += ' = \'' + sElementId + '\';';
		eval(sVariableName);
	}
}
// -----------------------
// FUNCTION: fStyleRegionalDropDown
// DESCRIPTION: A function that writes out the regional navigation CSS drop down
// ARGUMENTS: sElementId, sFormId , SDropdownWidth - can be either 70, 160, 190, 195, 235 - NB to use different widths new versions of the CSS class drop-down-bg-xxx will need to be created
// RETURN: None
// -----------------------
function fStyleRegionalDropDown(sElementId, sFormId, sDropdownWidth) {
	var sDropDownAreaName = sElementId + '-area';
	var sFirstValue = '';
	var sFirstOptionId = sElementId + '-id';
	var sMasterOptionLinkColor = '';
	var sFormAction = '/city/';
	var sDropDownName = '';
	var sRedir = '';

	if(document.getElementById('redir')) {
	  sRedir = document.getElementById('redir').value;
	  // Handle cases where we default the redir to emtpy,
	  // indicating we should use location.href instead.
	  if (sRedir == '') {
	    var loc = document.location.href;
	    var last_slash = loc.lastIndexOf('/');
	    if (last_slash != -1)
	      sRedir = loc.substr(0, last_slash + 1);
	  }
	}

	if(document.getElementById('regional-dropdown-select')) {
	  sDropDownName = document.getElementById('regional-dropdown-select').name;
	}

	var selIndex = document.getElementById(sFormId).selectedIndex;
	if (selIndex < 0)
	  selIndex = 0;
	if(selIndex) {
	  sFirstValue = document.getElementById(sFormId).options[selIndex].text;
	  if(document.getElementById(sFormId).options[selIndex].value == 'index'){
	    var sMasterOptionLinkColor = 'color:red;';
	  }
	} else {
	  sFirstValue = document.getElementById(sFormId).options[0].text;
	}
	var sFormLength = document.getElementById(sFormId).options.length;

	var sHTML = '';

	sHTML += '<div class="css-drop-down drop-down-bg-' + sDropdownWidth + ' width-' + sDropdownWidth + '" style="position:absolute;">';
	sHTML += '<ul class="v-small">';
	sHTML += '<li><a id="' + sFirstOptionId + '" class="first" style="' + sMasterOptionLinkColor + 'width:' + (sDropdownWidth -3) + 
	         'px;" href="javascript:fShowHideElement(\'' + sDropDownAreaName + '\');">' + sFirstValue + '</a>';
	if (sFormLength > 10) {
	  sHTML += '<ul id="' + sDropDownAreaName + '" class="access-text long-drop-down" style="width:' + (sDropdownWidth - 7) + 'px; ">';
	} else {
	  sHTML += '<ul id="' + sDropDownAreaName + '" class="access-text" style="width:' + (sDropdownWidth -25) + 'px;">';
	}

	nInputCount = document.getElementById(sFormId).length;
	
	for (nCount = 0; nCount < nInputCount; nCount++ )
	{
	  sMasterOptionLinkColor = '';
	  if(document.getElementById(sFormId).options[nCount].value == 'index'){
	    sMasterOptionLinkColor = 'color:red;';
	  }
	  sWriteValue = document.getElementById(sFormId).options[nCount].text;
	  sNewCityCookie = document.getElementById(sFormId).options[nCount].value;
	  sURL = sFormAction+'?' + sDropDownName + '=' + document.getElementById(sFormId).options[nCount].value + '&redir=' + sRedir;
	  sOptionLinkColor = document.getElementById(sFormId).options[nCount].style.color;
	  sHTML += '<li><a onclick="stl(this, \'o\', \'CityDropDown:' + c_city + ':' + sNewCityCookie + ':' + fEscapeSpecialChars(window.location.href) + '\')" style="' + sMasterOptionLinkColor + 'width:' + (sDropdownWidth -25) + 'px;" href="' + sURL + '" target="_top">' + sWriteValue + '</a>';
	}
	sHTML += '</ul></li>';
	sHTML += '</ul>'
	sHTML += '</div>';

	document.getElementById(sElementId).innerHTML = sHTML;
	fShowHideElement(sElementId);
}
// -----------------------
// FUNCTION: fCreateCarouselNextImage
// DESCRIPTION: A function to create the image pagination for the article once user clicks on next button.
// ARGUMENTS: none
// RETURN: none
function fCreateCarouselNextImage() {
	if(nSelectedCarouselImage<=aCarouselImages.length-1) {
			nSelectedCarouselImage++;
		}
		if(nSelectedCarouselImage==aCarouselImages.length) {
			nSelectedCarouselImage=0;
		}
	if(document.getElementById('carousel-image')) {
		document.getElementById('carousel-image').getElementsByTagName('img')[0].src = sCarouselImagePath + aCarouselImages[nSelectedCarouselImage][0];
		document.getElementById('carousel-image').getElementsByTagName('img')[0].alt = aCarouselImages[nSelectedCarouselImage][1];
		
		var sHTML = '';

		sHTML += aCarouselImages[nSelectedCarouselImage][2];

		if (!sHTML.length) {
		  $('photographer-byline').hide();
		  document.getElementById('photographer-credit').innerHTML = sHTML;
		}
		else {
		  document.getElementById('photographer-credit').innerHTML = sHTML;
		  $('photographer-byline').show();
		}		
	}
}
//---------------------------
// FUNCTION: fCreateCarouselPreviousImage
// DESCRIPTION: A function to create the image pagination for the article once user clicks on previous button.
// ARGUMENTS: none
// RETURN: none
//---------------------------
function fCreateCarouselPreviousImage() {
	if(nSelectedCarouselImage >= 0) {
			nSelectedCarouselImage--;
		}
	if (nSelectedCarouselImage < 0) {
			nSelectedCarouselImage = aCarouselImages.length-1;
		}
	if(document.getElementById('carousel-image') || nSelectedCarouselImage==aCarouselImages.length ) {
		document.getElementById('carousel-image').getElementsByTagName('img')[0].src = sCarouselImagePath + aCarouselImages[nSelectedCarouselImage][0];
		document.getElementById('carousel-image').getElementsByTagName('img')[0].alt = aCarouselImages[nSelectedCarouselImage][1];
		
		var sHTML = '';

		sHTML += aCarouselImages[nSelectedCarouselImage][2];

		if (!sHTML.length) {
		  $('photographer-byline').hide();
		  document.getElementById('photographer-credit').innerHTML = sHTML;
		}
		else {
		  document.getElementById('photographer-credit').innerHTML = sHTML;
		  $('photographer-byline').show();
		}		
	}
}
//---------------------------
// FUNCTION: fRandomiseBackgrounds
// DESCRIPTION: A function place randomly selected background images on a page
// ARGUMENTS: none
// RETURN: none
//---------------------------
function fRandomiseBackgrounds() {
	nFirstBackground = fRandomNumber(8);
	nSecondBackground = fRandomNumber(8);
	
	document.getElementById('content').className = 'top-wavey-line-' + nFirstBackground;
	document.getElementById('background-2').className = 'float-left width-960 bottom-wavey-line-' + nSecondBackground;
}
function fRandomNumber(nHowMany) {
	var nRandomNumber = Math.floor(Math.random()*nHowMany)+1;
	return nRandomNumber;
}
//---------------------------
// FUNCTION: fSetTimeout
// DESCRIPTION: A function that calls another function after a set time
// ARGUMENTS: aFunctionName nTimeToWait
// RETURN: aFunctionName
//---------------------------
function fSetTimeout(aFunctionName, nTimeToWait) {
	nTimeOut = setTimeout(aFunctionName, nTimeToWait);
}
//---------------------------
// FUNCTION: fIsScrolling
// DESCRIPTION: A function that checks the height of form elements and changes the class at a given height
// ARGUMENTS: sElementId nMaxHeight sOtherClasses
// RETURN: none
//---------------------------
function fIsScrolling(sElementId, nMaxHeight, sOtherClasses) {
	 var sElement = document.getElementById(sElementId);
	 var nHeight = document.getElementById(sElementId).scrollHeight;
	 if (nHeight > nMaxHeight) {
		document.getElementById(sElementId).className = sOtherClasses + ' scrolling';
	 }
	 else {
		document.getElementById(sElementId).className = sOtherClasses;
	 }
}
//---------------------------
// FUNCTION: fSwapImage
// DESCRIPTION: A function that swaps an image on mouseover.
// ARGUMENTS: sElementId sOriginalImage sRollOverImage
// RETURN: none
//---------------------------
function fSwapImage(sElementId, sImage) {
	document.getElementById(sElementId).src = sImage;
}
//---------------------------
// FUNCTION: fPreLoad
// DESCRIPTION: A function that preloads an array of images.
// ARGUMENTS: aImageArray
// RETURN: none
//---------------------------
function fPreLoad(aImageArray) {
	var nArrayLength = aImageArray.length;
	preLoadImages = new Array();

	for (nCount = 0; nCount < nArrayLength ; nCount++ ) {
		preLoadImages[nCount] = new Image();
		preLoadImages[nCount].src = aImageArray[nCount];
	}
}


function onClickTeaser(id, show_tab, hide_tab) {
  if (!id || !show_tab || !hide_tab) {
    return;
  }

  var active_position;
  if (show_tab == "most-read") {
    active_position = "left";
  }
  else if (show_tab == "most-recent") {
    active_position = "right"
  }
  
  $(show_tab + "-" + id).className = "active-" + active_position;
  $(hide_tab + "-" + id).className = "passive";
  $(hide_tab + "-list-" + id).hide();
  $(show_tab + "-list-" + id).show();
}

// -*- java -*-

function getXMLHttpRequest()
{
  var objXmlHttp = null;
  if( window.XMLHttpRequest )
    objXmlHttp = new XMLHttpRequest();
  else if( window.ActiveXObject ) {
    try {
      objXmlHttp = new ActiveXObject( "Msxml2.XMLHTTP" );
    } catch (e) {
      objXmlHttp = new ActiveXObject( "Microsoft.XMLHTTP" );
    }
  }
  return objXmlHttp;
}

function ajaxLoad (id, url, vars)
{
  var req = getXMLHttpRequest();
  if (req != null) {
    var elem = document.getElementById (id);
    elem.innerHTML = "<i>Loading...</i>";
    req.open ("GET", url, true);
    req.onreadystatechange = function() {
      if ((req.readyState == 4 || req.readyState == "Complete") &&
	  req.status == 200 && req.responseText)
	elem.innerHTML = req.responseText;
    };
    req.send (vars);
  }
}
// Support function for submitting extra search boxes. E.g. populates
// the q variable from query.
function extraSearchSubmit(query, q, form) {
  $(q).value = $F(query);
  $(form).submit();
}

// Manual Link Tracking Using Custom Link Code
//
// This small wrapper around the Omniture s.tl()-function makes sure
// we dont track clicks when we are in edit more. E.g. we have a
// SiteBuilder cookie set.
//
// obj : object|true
//
//   this - Use a 500 ms delay to insure data is collected before
//   leaving the page
//
//   true - Disable the 500 ms delay when the user is not going to
//   leave the page
//
// link_type : string
//
//  'e' - For an exit link.
//
//  'd' - For a file download.
//
//  'o' - For a generic custom link.
//
// link_name : string
//
//  If the link name parameter is not defined, the URL of the link
// (determined from the "this" object) will be used as the link name.
function stl (obj, link_type, link_name) {
  if (!edit_mode())
    return s.tl(obj, link_type , link_name);
  else 
    return true;  
}


// Test if we are in edit mode or not. This is based on if we have a
// "SiteBuilder" cookie or not.
function edit_mode() {
  return !!get_cookie ("SiteBuilder");
}


// Returns the value of the cookie or true if it exists but have no
// value. Returns false if no such cookie exists.
function get_cookie (name) {
  var cookies = document.cookie.split( ';' );

  for (var  i = 0; i < cookies.length; i++ ) {
    temp_cookie = cookies[i].split( '=' );

    if (temp_cookie[0].replace(/^\s+|\s+$/g, '') == name) {
      if (temp_cookie.length > 1) {
	return unescape(temp_cookie[1].replace(/^\s+|\s+$/g, ''));
      }
      return true;
    }
  }
  return false;
}


// Changes the body div class to 'class' and calls print()
function print_page() {
  var body = document.getElementsByTagName ("body")[0];
  Element.extend (body);
  body.addClassName ('print');
  print();
}


// -----------------------
// FUNCTION: fEscapeSpecialChars
// DESCRIPTION: Replaces certain characters (double and single quotes) with html entity representation.
//              (Note: Used to send variables values to Omniture SiteCatalyst)
// ARGUMENTS: sIn
// RETURNS: sOut
// -----------------------
function fEscapeSpecialChars(sIn) {
  var sOut = String(sIn).replace(/\"/g,"&#92;&#34;");
  sOut = sOut.replace(/\'/g,"&#92;&#39;");
  sOut = sOut.replace(/\n/g,"");

  // Not sure if the following two are necessary?
  sOut = sOut.replace(/</g,"&#60;");	
  sOut = sOut.replace(/>/g,"&#62;");
  return sOut;
}


// Set the elements value
function setValue(element, value) {
  if (!element)
    return;
  element.value = value;
}


// Clears the value if its current value is equal to value
function clearValue(element, value) {
  if (!element)
    return;
  if ($F(element) == value)
    element.value = "";
}

<!--
// -----------------------
// FUNCTION: fClassifiedsModulePopUp
// DESCRIPTION: A function that shows or hides the top stories content.
// ARGUMENTS: sElementId
// RETURN: None
// -----------------------

function fClassifiedsModulePopUp(nClassifiedsLinks) {
	var sHTML ='';
	sHTML +='<div><h3><a href=\"'+sClassifiedsTeaser[nClassifiedsLinks][10]+'\" class="color-fff">'+sClassifiedsTeaser[nClassifiedsLinks][0]+'</a></h3></div>';
	sHTML +='<div class="float-right margin-top-neg-20"><a href=\"'+sClassifiedsTeaser[nClassifiedsLinks][10]+'\">'+'<img src=\"'+sClassifiedsImagePath+''+sClassifiedsTeaser[nClassifiedsLinks][2]+'\"/>'+'</a></div>';
	sHTML +='<p class="small">'+sClassifiedsTeaser[nClassifiedsLinks][1]+'</p>';
	sHTML +='<ul class="link-list bg-d1f400">';
	sHTML +='<li><a href=\"'+sClassifiedsTeaser[nClassifiedsLinks][4]+'\" class="color-fff">'+ sClassifiedsTeaser[nClassifiedsLinks][3] +'</a></li>';
	sHTML +='<li><a href=\"'+sClassifiedsTeaser[nClassifiedsLinks][6]+'\" class="color-fff">'+ sClassifiedsTeaser[nClassifiedsLinks][5] +'</a></li>';
	sHTML +='<li><a href=\"'+sClassifiedsTeaser[nClassifiedsLinks][8]+'\" class="color-fff">'+ sClassifiedsTeaser[nClassifiedsLinks][7] +'</a></li>';
	sHTML +='<li><a href=\"'+sClassifiedsTeaser[nClassifiedsLinks][10]+'\" class="color-fff">'+ sClassifiedsTeaser[nClassifiedsLinks][9] +'</a></li>' ;
	sHTML += '</ul>';
	sHTML +='<div class="classifieds-down-arrow"></div>';
	document.getElementById('classifieds-pop-up-module').innerHTML = sHTML;
	fShowHideElement('classifieds-pop-up-module');
	
	switch(nClassifiedsLinks) {
		case "0":
			document.getElementById('classifieds-pop-up-module').style.left= 270 + 'px';
			break;
		case "1":
			document.getElementById('classifieds-pop-up-module').style.left= 495 + 'px';
			break;
		case "2":
			document.getElementById('classifieds-pop-up-module').style.left= 605 + 'px';
			break;
		case "3":
			document.getElementById('classifieds-pop-up-module').style.left= 750 + 'px';
			break;
		case "4":
			document.getElementById('classifieds-pop-up-module').style.left= 860 + 'px';
			break;
	}
}


<!--
// -----------------------
// FUNCTION: fRenderAggregatedContentPagination
// DESCRIPTION: Shows and hides the relevent pages. 
// ARGUMENTS: target RSS group div, desired page (optional)
// RETURN: n/a
// -----------------------
function fRenderAggregatedContentPagination(targetRssGroup,intCurrentPage) {
    
	if (intCurrentPage == null)
	{
		intCurrentPage = 0;
	}
	
	// -----------------------
	// FUNCTION: drawPagnation
	// DESCRIPTION: Generate Pagnation html string
        // 2008-04-02: NOTE: Made some structural code changes to the pagination 
        // of the rss-list-component, so be careful when syncing or doing any other 
        // change to this function
	// ARGUMENTS: total items, target page, target RSS group div
	// RETURN: n/a
	// -----------------------
	function drawPagnation(intRSSCount,intPagePosition,strTargetGroup)
	{
		var nAggregatedCount = Math.ceil(intRSSCount / 4);
		var sPagination = '';
		
		sPagination += '<div class="clear width-383 border-dotted-top-62991F padding-top-10">'; // margin-left-15


                sPagination += '<ul class="pagination-list">';
                sPagination += '<li style="line-height:1.1em;">';
		if (intPagePosition > 0)
		{                        
			sPagination += '<a class="previous-button small bold" href="#" onclick="fRenderAggregatedContentPagination(\'' + strTargetGroup + '\',' + (intPagePosition - 1) + '); return false;">' + msg_featuredcontent_previous + ' </a>';
		}
		else {
			sPagination += '<span class="previous-grey-button">' + msg_featuredcontent_previous + ' </span>';
		}
                sPagination += '</li>';

		for (nCount=0; nCount < nAggregatedCount ; nCount++ ) {
			

			if (nCount == intPagePosition)
			{
				sPagination += '<li style="line-height:1.1em;"><a class="pagination small bold" href="#" onclick="return false;" style="color:black;">' + (nCount + 1) + '</a></li>';
			}
			else
			{
				sPagination += '<li style="line-height:1.1em;"><a class="pagination small bold" href="#" onclick="fRenderAggregatedContentPagination(\'' + strTargetGroup + '\',' + (nCount) + ');return false;">' + (nCount + 1) + '</a></li>';
			}
		}

		sPagination += '<li style="line-height:1.1em;">';
		if (intPagePosition == (nAggregatedCount - 1))
		{
			sPagination += '<span class="next-grey-button">' + msg_featuredcontent_next + '</span>';
		}
		else
		{
			sPagination += '<a class="next-button small bold" href=#" onclick="fRenderAggregatedContentPagination(\'' + strTargetGroup + '\',' + (intPagePosition + 1) + ');"> ' + msg_featuredcontent_next + '</a>';
		}
		sPagination += '</li>';

		sPagination += '</ul>';
		
	
		sPagination += '</div>';
		return sPagination;
	}
	
		
	objListLayers = document.getElementById('aggregated-content-blocks-wrap').getElementsByTagName('DIV');


	for (keyLayer in objListLayers)
	{		
			
		if (objListLayers[keyLayer] && objListLayers[keyLayer].id != null)
		{
			
			if (objListLayers[keyLayer].id.indexOf('-level-rss-feeds') > -1)
			{
				if (objListLayers[keyLayer].id == targetRssGroup)
				{
					objListLayers[keyLayer].className = objListLayers[keyLayer].className = 'float-left width-373';
					
				} else
				{
					objListLayers[keyLayer].className = objListLayers[keyLayer].className = 'float-left width-373 access-text';
					
				}
			}
		}
	}
	
	objListSubLayers = document.getElementById(targetRssGroup).getElementsByTagName('DIV');
	var intRSSCount = 0;
	
	for (keySubLayer in objListSubLayers)
	{
		var strClassName = objListSubLayers[keySubLayer].className;
		
		if (strClassName == 'aggregated-rss-block' || strClassName == 'aggregated-rss-first-block')
		{	
			intRSSCount++;
			
					
			if (intRSSCount <= (4 * (intCurrentPage + 1)) && intRSSCount >= (4 * (intCurrentPage)) + 1)
			{
				objListSubLayers[keySubLayer].style.display = 'block';
			}
			else
			{
				objListSubLayers[keySubLayer].style.display = 'none';
	
			}
		}
	}
	
	
	if (intRSSCount > 4)
	{
		document.getElementById('aggregated-content-pagination').innerHTML = drawPagnation(intRSSCount,intCurrentPage,targetRssGroup);
	}
	else
	{
		document.getElementById('aggregated-content-pagination').innerHTML = '';

	}
}
// -----------------------
// FUNCTION: fToggleAggregated
// DESCRIPTION: Toggles the aggregated content navigation.
// ARGUMENTS: sElementId, sGlobalVar, sRSSFeedsId
// RETURN: n/a
// -----------------------
function fToggleAggregated(sElementId, sGlobalVar, sRSSFeedsId) {
		// Check no extra buttons left open and if so close them
		if (sActiveButton != 'none') {
			if (sActiveButton != sElementId) {
				fToggleListStyle(sActiveButton);
			}
		}
		// Toggle the button for Nav that was actually clicked
		fToggleListStyle(sElementId);
		switchRssListIcon(sElementId);
		
		// Show Hide the associated RSS list
		if(sElementId != 'aggregated-rss-home'){
			sElementId += '-sub';
		}
		if(document.getElementById(sElementId)){
			fShowHideSwapper(sElementId, sGlobalVar);
			if (sRSSFeedsId == sActiveRSSListing)
			{
				fShowHideSwapper('first-level-rss-feeds', 'sActiveRSSListing');
			}
		}

	// FIXME
	fMakeActive(false);
}

//------------------------
//FUNCTION: switchRssListIcon
//DESCRIPTION: Switch +/-
// ARGUMENTS: sToggleElement
//------------------------
function switchRssListIcon(sToggleElement){		
		var classNameTemp = 'aggregated-menu-rss-icon-aggregated-menu-';
		var spanId = 'aggregated-menu-rss-icon-' +sToggleElement;
		var toggleItem = '';
		
		if(spanId.match('first-level-rss')){		  
		
		  for (var i = 1; i<=homelevelitemsrss; i++)
		  {
		    toggleItem = classNameTemp+i;
		    document.getElementById(toggleItem).innerHTML = '<img src="/templates/img/lists/plus-14x13.gif" width="14" height="13" alt=""/>';
		    document.getElementById(toggleItem).className = ('closed');
		  }
		
		 return;
		}
		
		sClassName = document.getElementById(spanId).className;
		
		for (var i = 1; i<=homelevelitemsrss; i++)
		{
		   toggleItem = classNameTemp+i;
		   if(toggleItem == spanId){
			if (sClassName.match('closed'))
			{
				document.getElementById(spanId).innerHTML = '<img src="/templates/img/lists/minus-14x13.gif" width="14" height="13" alt=""/>';
				document.getElementById(spanId).className = ('open');
			}
			else{
				document.getElementById(spanId).innerHTML = '<img src="/templates/img/lists/plus-14x13.gif" width="14" height="13" alt=""/>';
				document.getElementById(spanId).className = ('closed');
			}
		   }
		   else{
		    document.getElementById(toggleItem).innerHTML = '<img src="/templates/img/lists/plus-14x13.gif" width="14" height="13" alt=""/>';
		    document.getElementById(toggleItem).className = ('closed');
		   }
		}
	}
	
// -----------------------
// FUNCTION: fToggleListStyle
// DESCRIPTION: Toggles the list style.
// ARGUMENTS: sToggleElement
// RETURN: n/a
// -----------------------
function fToggleListStyle(sToggleElement) {
		sClassName = document.getElementById(sToggleElement).className;
		if (sClassName.match('closed'))
		{
			document.getElementById(sToggleElement).className = ('aggregated-level-1 open');
			sActiveButton = sToggleElement;
		}else {
			document.getElementById(sToggleElement).className = ('aggregated-level-1 closed');
			sActiveButton = 'none';
		}
	}
// -----------------------
// FUNCTION: fMakeActive
// DESCRIPTION: Toggles active links. The link you have clicked on (sLinkId) 
//              becomes active while the link that was active before (sActiveLink) 
//		becomes inactive.
// ARGUMENTS: sLinkId (the link you have clicked on) 
// RETURN: n/a
// -----------------------
function fMakeActive(sLinkId) {
    var sClickedLinkIconId, 
	sActiveLinkIconId, 
	sClickedLinkIconClassName, 
	sActiveLinkIconClassName;
    
    if (sActiveLink) {
	document.getElementById(sActiveLink).className = 'border-dotted-top';
	sActiveLinkIconId = 'aggregated-menu-rss-icon-' + sActiveLink;
	sActiveLinkIconClassName = document.getElementById(sActiveLinkIconId).className;
	
	if (sActiveLinkIconClassName.match('open')){
	    document.getElementById(sActiveLinkIconId).innerHTML = '<img src="/templates/img/lists/green-chevron-10x10.gif" width="10" height="10"/>';
	    document.getElementById(sActiveLinkIconId).className = ('closed');
	    
	}
    }
    
    if (sLinkId) {
	document.getElementById(sLinkId).className = 'border-dotted-top active';
	sClickedLinkIconId = 'aggregated-menu-rss-icon-' + sLinkId;
	sClickedLinkIconClassName = document.getElementById(sClickedLinkIconId).className;
	
	if (sClickedLinkIconClassName.match('closed')) {
	    document.getElementById(sClickedLinkIconId).innerHTML = '<img src="/templates/img/backgrounds/arrow-rss.gif"/>';
	    document.getElementById(sClickedLinkIconId).className = ('open');
	}
    } 
    
    if (sActiveLink && sLinkId) {
	sActiveLink = sLinkId;
    }
}


function frenderMenuStartUp(){

		var menuItem = '';
		for(var i = 1; i<=homelevelitemsrss; i++)
		{
			menuItem = 'aggregated-menu-'+i+'-sub';
			fShowHideElement(menuItem);
		}

}

//-->
// -----------------------
// FUNCTION: fRenderFeaturedContent
// DESCRIPTION: 
// ARGUMENTS: nActiveTab, nActiveFeature, nHighlightedThumb, nBoxName, nPagePath
// RETURN: 
// -----------------------
function fRenderFeaturedContent(nActiveTab, nActiveFeature, nHighlightedThumb, nBoxName, nPagePath) {
	if(window.aTheContent[0][0]){
	// Render Tabs & set selected image to 0.
	var sTabHTML = '';
	for (nTabCount=0; nTabCount < aTheContent.length; nTabCount++ )
	{
		if (nTabCount == nActiveTab) {
			sTabHTML += '<li>';
			sTabHTML += '<a class="active" '
			         + 'href="javascript:nSelectedTab = ' + nTabCount + '; nSelectedThumb = 0; fRenderFeaturedContent(' + nTabCount + ', \'0\', \'0\', \'' + nBoxName + '\', \'' + nPagePath + '\');" ';
			if(omniture_enabled) {
			  sTabHTML += 'onClick="fTrackFeaturedContentClicks(s_account, \'' + fEscapeSpecialChars(aTheContent[nTabCount] [0] [0]) 
			            + ' (tab ' + (nTabCount+1) + ')\', \'' + nPagePath + ' - Carousel-Tab' + (nTabCount+1) + '\');"';
			}
			sTabHTML += '>' + aTheContent[nTabCount] [0] [0] + '</a> ';
			sTabHTML += '</li>';
		}
		else {
			sTabHTML += '<li>';
			sTabHTML += '<a class="" '
			         + 'href="javascript:nSelectedTab = ' + nTabCount + '; nSelectedThumb = 0; fRenderFeaturedContent(' + nTabCount + ', \'0\', \'0\', \'' + nBoxName + '\', \'' + nPagePath + '\');" ';
			if(omniture_enabled) {
			  sTabHTML += 'onClick="fTrackFeaturedContentClicks(s_account, \'' + fEscapeSpecialChars(aTheContent[nTabCount] [0] [0]) 
			           + ' (tab ' + (nTabCount+1) + ')\', \'' + nPagePath + ' - Carousel-Tab' + (nTabCount+1) + '\');"';
			}
			sTabHTML += '>' + aTheContent[nTabCount] [0] [0] + '</a> ';
			sTabHTML += '</li>';
		}
	}
	document.getElementById('fc-tabs').innerHTML = sTabHTML;
	// Render Thumbnails
	nThumbArrayLength = aTheContent[nActiveTab].length-1;
	var nStartThumb = nActiveFeature - nHighlightedThumb;
	if (nStartThumb < 0)
	{
		nStartThumb = (nThumbArrayLength + nStartThumb) +1;
	}
	var nIterations = -1;
	var nLastThumb = nStartThumb + 5;
	var sThumbsHTML = '';
	
	for (nThumbCount = nStartThumb; nIterations < 4; nThumbCount++ )
	{
		nIterations++;
		if (nThumbCount > nThumbArrayLength)
		{
			nThumbCount = 0;
		}
		if (nIterations == nHighlightedThumb) {
			sThumbsHTML += '<li><img class="active" src="' + aTheContent[nActiveTab] [nThumbCount] [2] + '" width="72" height="54"	 /><span class="fc-indicator">&nbsp;</span></li>';
			
			var sContentHTML = ''; /* Only used when main content area is text */
			var sTeaserHTML = '';

			// Set to User or Metro Generated
			switch (aTheContent[nActiveTab] [nThumbCount] [8])
			{
			case 'metro' :
				switch (aTheContent[nActiveTab] [nThumbCount] [7])
				{
				case 'text':
				sContentHTML += '<div class="width-330">';
				sContentHTML += '<div class="padding-tl-15">';
				sContentHTML += '<h1 class="fc-metro">' + aTheContent[nActiveTab] [nThumbCount] [1] + '</h1>';
				sContentHTML += '<p class="v-small padding-left-3 padding-top-4">' + aTheContent[nActiveTab] [nThumbCount] [9] + '</p>';
				sContentHTML += '</div>';
				sContentHTML += '</div>';
				
				// Write Out Metro generated Headline
				document.getElementById('fc-main-image').innerHTML = sContentHTML;
				break;

				case 'image':
				// Write Out Main Image
				document.getElementById('fc-main-image').innerHTML = '<a href="' + aTheContent[nActiveTab] [nThumbCount] [4] + ' " target="_top" onclick="return checkCityCookie(\'' + aTheContent[nActiveTab] [nThumbCount] [4] + '\');"><img src="' + aTheContent[nActiveTab] [nThumbCount] [1] + '" width="' + aTheContent[nActiveTab] [nThumbCount] [10] + '" height="200" /></a>';
				break;
				}
				document.getElementById('fc-teaser').className = 'fc-metro-quote';
				sTeaserHTML += '<div class="fc-metro-container">';
				sTeaserHTML += '<p class="color-fff bold padding-left-15"><a href="' + aTheContent[nActiveTab] [nThumbCount] [4] + ' " target="_top" onclick="return checkCityCookie(\'' + aTheContent[nActiveTab] [nThumbCount] [4] + '\');">' + aTheContent[nActiveTab] [nThumbCount] [5] + '</a></p>';
				sTeaserHTML += '<p class="color-fff bold small padding-top-3 padding-left-15">' + aTheContent[nActiveTab] [nThumbCount] [6] + '</p>';
				break;
			case 'reader' :
				switch (aTheContent[nActiveTab] [nThumbCount] [7])
				{
				case 'text':
				sContentHTML += '<div class="width-330">';
				sContentHTML += '<div class="padding-tl-15">';
				sContentHTML += '<h1 class="fc-reader">' + aTheContent[nActiveTab] [nThumbCount] [1] + '</h1>';
				sContentHTML += '<p class="v-small padding-left-3 padding-top-4">' + aTheContent[nActiveTab] [nThumbCount] [9] + '</p>';
				sContentHTML += '</div>';
				sContentHTML += '</div>';

				// Writ Out User Generated Headline
				document.getElementById('fc-main-image').innerHTML = sContentHTML;
				break;

				case 'image':
				// Write Out Main Image
				document.getElementById('fc-main-image').innerHTML = '<a href="' + aTheContent[nActiveTab] [nThumbCount] [4] + ' " target="_top" onclick="return checkCityCookie(\'' + aTheContent[nActiveTab] [nThumbCount] [4] + '\');"><img src="' + aTheContent[nActiveTab] [nThumbCount] [1] + '" width="563" height="200" /></a>';
				break;
				}
				document.getElementById('fc-teaser').className = 'fc-user-quote';
				// Write Out Accompanying Text
				sTeaserHTML += '<div class="fc-quote-container">';
				sTeaserHTML += '<blockquote class="user-white"><div><a href="' + aTheContent[nActiveTab] [nThumbCount] [4] + ' " target="_top" onclick="return checkCityCookie(\'' + aTheContent[nActiveTab] [nThumbCount] [4] + '\');">' + aTheContent[nActiveTab] [nThumbCount] [5] + '</a></div></blockquote>';
				break;
			}
		
			// Write out link and image map

			// link text
			var read_more_link_text = msg_featuredcontent_readfullstory;
			if (aTheContent[nActiveTab] [nThumbCount] [11] && aTheContent[nActiveTab] [nThumbCount] [11].length) {
			  read_more_link_text = aTheContent[nActiveTab] [nThumbCount] [11];
			}
			
			sTeaserHTML += '<ul class="link-list bg-ffb000 padding-left-15 fc-reader-more-link">';
			sTeaserHTML += '<li><a href="' + aTheContent[nActiveTab] [nThumbCount] [4] + '" target="_top" '
				     + 'onClick="fTrackBoxPosition(s_account, '
				     + '\'' + nBoxName + '\', '
				     + '\'' + read_more_link_text + '\'); '
				     + 'return checkCityCookie(\'' + aTheContent[nActiveTab] [nThumbCount] [4] + '\');">' 
				     + read_more_link_text + '</a></li>';
			sTeaserHTML += '</ul>';			

			document.getElementById('fc-teaser').innerHTML = sTeaserHTML; 

			// Write Out Next & Previous
			var sNextPreviousHTML = '';

			// Omniture Related Variables
			var iLPS = nSelectedFeature-1<=-1?aTheContent[nSelectedTab].length-1:nSelectedFeature-1;
			var sLPT = aTheContent[nSelectedTab] [iLPS] [5];
			var iLNS = nSelectedFeature+1 == aTheContent[nSelectedTab].length?0:nSelectedFeature+1;
			var sLNT = aTheContent[nSelectedTab] [iLNS] [5];
			// End Omniture Related Variables

			sNextPreviousHTML += '<div class="padding-top-5"></div>';
			sNextPreviousHTML += '<a class="previous small" href="javascript:fNextPreviousFeaturedContent(nSelectedFeature -1, \'previous\', \'' + nBoxName + '\', \'' + nPagePath + '\')" ';
			if(omniture_enabled) {
			  sNextPreviousHTML += 'onClick="fTrackFeaturedContentClicks(s_account, \'' + fEscapeSpecialChars(sLPT) 
			                    + ' (previous link)\', \'' + nPagePath + ' - Carousel-Previous\');"';
			}
			sNextPreviousHTML += '>' + msg_featuredcontent_previous + '</a>';
			sNextPreviousHTML += '<span class="v-small fc-count">' + (nThumbCount+1) + ' ' + msg_featuredcontent_of + ' ' + (nThumbArrayLength + 1) + '</span>';
			sNextPreviousHTML += '<a class="next small" href="javascript:fNextPreviousFeaturedContent(nSelectedFeature +1, \'next\', \'' + nBoxName + '\', \'' + nPagePath + '\');" ';
			if (omniture_enabled) {
			  sNextPreviousHTML += 'onClick="fTrackFeaturedContentClicks(s_account, \'' + fEscapeSpecialChars(sLNT) 
			                    +  ' (next link)\', \''+ nPagePath + ' - Carousel-Next\');"';
			}
			sNextPreviousHTML += '>' + msg_featuredcontent_next + '</a>';

			document.getElementById('fc-next-previous').innerHTML = sNextPreviousHTML;
		}
		else {
			sThumbsHTML += '<li><a href="javascript:nSelectedThumb = ' + nIterations + ';nSelectedFeature = ' + nThumbCount 
			            + ';fRenderFeaturedContent(nSelectedTab, nSelectedFeature, nSelectedThumb, \'' + nBoxName + '\', \'' + nPagePath + '\');" ';
		  if (omniture_enabled) {
		    sThumbsHTML += 'onClick="fTrackFeaturedContentClicks(s_account, \'' + fEscapeSpecialChars(aTheContent[nActiveTab] [nThumbCount] [5]) 
		                 + ' (thumbnail ' + (nThumbCount+1) + ')\', \'' + nPagePath + ' - Carousel-Thumb' + (nThumbCount+1) + '\');"';
		  }
		  sThumbsHTML += '><img src="' + aTheContent[nActiveTab] [nThumbCount] [2] + '" width="72" height="54" /></a></li>';
		}
	}
	document.getElementById('fc-thumbnails').innerHTML = sThumbsHTML;
  }
}

// -----------------------
// FUNCTION: fNextPreviousFeaturedContent
// DESCRIPTION: Next & Previous functionality for Featured Content Navigation
// ARGUMENTS: nNewSelectedFeature, sDirection, nBoxName, nPagePath
// RETURN: nSelectedTab, nSelectedFeature, nSelectedThumb
// -----------------------
function fNextPreviousFeaturedContent(nNewSelectedFeature, sDirection, nBoxName, nPagePath) {

	if(window.aTheContent[0][0]){
	
	switch (sDirection)
	{
	case 'next':
		if (nNewSelectedFeature == aTheContent[nSelectedTab].length)
		{
			nNewSelectedFeature = 0;
		}
	break;
	case 'previous':
		if (nNewSelectedFeature <= -1)
		{
			nNewSelectedFeature = aTheContent[nSelectedTab].length-1;
		}
	break;
	}
	nSelectedFeature = nNewSelectedFeature;
	fRenderFeaturedContent(nSelectedTab, nSelectedFeature, nSelectedThumb, nBoxName, nPagePath);
      }
}

// -----------------------
// FUNCTION: fPreloadFeaturedContent
// DESCRIPTION: Selects image order for preloading
// ARGUMENTS: 
// RETURN: 
// -----------------------
function fPreloadFeaturedContent() {
	if(aTheContent.length){
	
	
	var aPreLoad1 = new Array();
	var nArrayCount = 0;

	// Preload the main images for visible tabs.
	for (nCount = nSelectedFeature+1; nCount < nSelectedFeature+5 ; nCount++) {
		if (aTheContent[nSelectedTab][nCount] &&
		    aTheContent[nSelectedTab][nCount][7] == 'image') {
			aPreLoad1[nArrayCount] = aTheContent[nSelectedTab][nCount][1];
			nArrayCount++;
		}
	}
	// Pass array to preloder.
	if (nArrayCount > 0) {
		fPreLoad(aPreLoad1);
	}

	var aPreload2 = new Array();
	var aPreload3 = new Array();
	var nThumbCount = aTheContent[nSelectedTab].length;
	nArrayCount = 0;

	// Preload remaining thumbnails and associated images for tab.
	for (nCount = nSelectedFeature+4; nCount < nThumbCount ; nCount++) {
		// Add thumbnail to array
			aPreload2[nArrayCount] = aTheContent[nSelectedTab][nCount][2];
		if (aTheContent[nSelectedTab][nCount][7] == 'image') {
			// Add main image to array
			aPreload3[nArrayCount] = aTheContent[nSelectedTab][nCount][1];
			nArrayCount++;
		}
	}
	// Pass Arrays to preloader
	if (nArrayCount > 0) {
		fPreLoad(aPreload2);
		fPreLoad(aPreload3);
	}

	// Preload initial state for 2nd & 3rd Tabs
	var aPreload4 = new Array();
	nArrayCount = 0;

	// Determine number of tabs
	var nTabCount = aTheContent.length;
	
	// Iterate 2nd & third tabs
	for (nCount=1; nCount < nTabCount; nCount++) {
		if (nCount != nSelectedTab) {
			// Preload first main image
			if (aTheContent[nCount][0][7] == 'image') {
				aPreload4[nArrayCount] = aTheContent[nCount][0][1];
				nArrayCount++;
			}
			// Preload the visible thumbnails
			for (nThumbCount = 0; nThumbCount < 5; nThumbCount++) {
			    if (aTheContent[nCount][nThumbCount]) {
				aPreload4[nArrayCount] = aTheContent[nCount][nThumbCount][2];
				nArrayCount++;
			    }
			}
		}
	}
	// Pass Array to preloader
	if (nArrayCount > 0) {
		fPreLoad(aPreload4);
	}
	// Preload large images images for 2nd & 3rd tabs associatd with visible thumbs
	var aPreload5 = new Array();
	nArrayCount = 0;

	// Iterate 2nd & third tabs
	for (nCount=1; nCount < nTabCount; nCount++) {
		if (nCount != nSelectedTab) {
			// Preload both sets of large images for visible thumbnails
			for (nThumbCount = 0; nThumbCount < 5; nThumbCount++) {
				if (aTheContent[nCount][nThumbCount] &&
				    aTheContent[nCount][nThumbCount][7] == 'image')	{
					aPreload5[nArrayCount] = aTheContent[nCount][nThumbCount][1];
					nArrayCount++;
				}
			}
		}
	}
	// Pass Array to preloader
	if (nArrayCount > 0) {
		fPreLoad(aPreload5);
	}

	// Preload last remaining thumbs and large images
	var aPreload6 = new Array();
	nArrayCount = 0;
	
	// Iterate 2nd & third tabs
	for (nCount=1; nCount < nTabCount; nCount++) {
		if (nCount != nSelectedTab) {
			nThumbCount = aTheContent[nCount].length;
			for (nSubCount = 4; nSubCount < nThumbCount ; nSubCount++ ) {
				if (aTheContent[nCount][nSubCount][7] == 'image')
				{
					aPreload6[nArrayCount] = aTheContent[nCount][nSubCount][2];
					nArrayCount++;
					aPreload6[nArrayCount] = aTheContent[nCount][nSubCount][1];
					nArrayCount++;
				}
			}
		}
	}
	// Pass Array to preloader
	if (nArrayCount > 0) {
		fPreLoad(aPreload6);
	}
   }
}

// -----------------------
// FUNCTION: fShowHidePollsElement
// DESCRIPTION: A function that shows or hides the top stories content.
// ARGUMENTS: sElementId
// RETURN: None
// -----------------------
function fShowHidePollsElement(sHideElementId, sShowElementId, sInactiveTab, sActiveTab) {
	// Show & hide the elements
	fShowHideElement(sHideElementId);
	fShowHideElement(sShowElementId);
	// Update the Tabs
	document.getElementById(sInactiveTab).className = '';
	document.getElementById(sActiveTab).className = 'active';
}

var cookieDomain = cookieDomain;
var lastClickedUrl;

// -----------------------
// FUNCTION: fStyleRegionalDropDownPop
// DESCRIPTION: A function that writes out the regional navigation CSS drop down
// ARGUMENTS: sElemendId, sFormId , SDropdownWidth - can be either 70, 160, 190, 195, 235 - NB to use different widths new versions of the CSS class drop-down-bg-xxx will need to be created
// RETURN: None
// -----------------------
function fStyleRegionalDropDownPop(sElementId, sFormId, sDropdownWidth) {

    var sDropDownAreaName = sElementId + '-area';
    var sFirstOptionId = sElementId + '-id';
    var sFirstValue = document.getElementById(sFormId).options[0].text;
    var sFormLength = document.getElementById(sFormId).options.length;

    // Determine the scroll height of the screen.
    var nScrollHeight = 0;
   
    if ( typeof(window.pageYOffset) == 'number' ) {

	// Compliant browsers
	nScrollHeight = window.pageYOffset;

    } else if ( document.body && document.body.scrollTop ) {

	// DOM compliant browsers
	nScrollHeight = document.body.scrollTop;

    } else if ( document.documentElement && document.documentElement.scrollTop ) {

	// Microsoft browsers
	nScrollHeight = document.documentElement.scrollTop;

    }
   
    // Set top margin to be the 116 pixels from the top of the viewable area
    var nTopMargin = (nScrollHeight + 116);

    // Set the bottom margin to be 300px more than the top margin
    var nBottomMargin = (nTopMargin + 330);
   	
    // Apply the margins to city-filter div
    document.getElementById('city-filter').style.marginTop = nTopMargin + 'px';
    document.getElementById('city-filter').style.marginBottom = '-' + nBottomMargin + 'px';
   
    var sHTML = '';

    sHTML += '<div class="css-drop-down drop-down-bg-' + sDropdownWidth + ' width-' + sDropdownWidth + '" style="position:absolute;">';
    sHTML += '<ul class="v-small">';
    sHTML += '<li><a id="' + sFirstOptionId + '" class="first" style="width:' + (sDropdownWidth -3) + 'px;" href="javascript:fShowHideElement(\'' + sDropDownAreaName + '\');">' + sFirstValue + '</a>';
    if (sFormLength > 10)
	{
	    sHTML += '<ul id="' + sDropDownAreaName + '" class="access-text long-drop-down" style="width:' + (sDropdownWidth - 7) + 'px; ">';
	} else {
	    sHTML += '<ul id="' + sDropDownAreaName + '" class="access-text" style="width:' + (sDropdownWidth -25) + 'px;">';
	}
    nInputCount = document.getElementById(sFormId).length;
	
    for (nCount = 1; nCount < nInputCount; nCount++ )
	{
	    sWriteText = document.getElementById(sFormId).options[nCount].text;
	    sWriteValue = document.getElementById(sFormId).options[nCount].value;
	    sNewCityCookie = sWriteValue;
	    sURL = lastClickedUrl;
	    var last_slash = lastClickedUrl.lastIndexOf('/');
	    if (last_slash != -1)
		sURL = lastClickedUrl.substr(0, last_slash + 1);
	    sHTML += '<li><a onclick="stl(this, \'o\', \'CityPopUp:' + fEscapeSpecialChars(window.location.href) + ':' + sNewCityCookie + '\')" style="width:' + (sDropdownWidth -25) + 'px;" href="/city/?go_city=' + sWriteValue + '&redir=' + sURL + '">' + sWriteText + '</a>';
	}
    sHTML += '</ul></li>';
    sHTML += '</ul>'
	sHTML += '</div>';

    document.getElementById(sElementId).innerHTML = sHTML;
    fShowHideElement(sElementId);
}


//Function thats sets city cookie
function setCityCookie(value) {
  var name = "city";
  var domain = cookieDomain;
  var path = "/"
  var expires = new Date(new Date().getTime() + (30*24*60*50000))

   document.cookie= name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires.toGMTString() : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "");

   document.getElementById('city-filter').style.display = 'none';
   
   window.location = lastClickedUrl;
   

}


// Function that checks if either city or SiteBuilder cookies are set
// and if not, it fires the city selector (div with id = city-filter)
function checkCityCookie(linkUrl) {

    // On e.g. export pages ignore the city cookie and go to the target page...
    if(ignore_city_cookie ||
       linkUrl.match('://')) {

	// window.top.location = linkUrl;
	return true;

    } else {

	var enabledcookies = fTestCookies();
        lastClickedUrl = linkUrl;

	
	if(enabledcookies == 1) {

	    var check_name = "city";
	    var a_all_cookies = document.cookie.split( ';' );
	    var a_temp_cookie = '';
	    var cookie_name = '';
	    var cookie_value = '';
	    var b_cookie_found = false; 
	
	    // Go look for that city cookie
	    for (var  i = 0; i < a_all_cookies.length; i++ ) {
		a_temp_cookie = a_all_cookies[i].split( '=' );
		
		cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');

		if ( cookie_name == check_name ) {
		    b_cookie_found = true;

		    if ( a_temp_cookie.length > 1 )	{
			cookie_value = unescape( a_temp_cookie[1].replace(/^\s+|\s+$/g, '') );
		    }

		    // window.top.location = lastClickedUrl;
		    return true;
		}
		a_temp_cookie = null;
		cookie_name = '';
	    }
	 
	    // If there's no city cookie, check if we are in edit mode (SiteBuilder)
	    if ( !b_cookie_found ) {

		check_name = "SiteBuilder";
	
		for (var  i = 0; i < a_all_cookies.length; i++ ) {
		    a_temp_cookie = a_all_cookies[i].split( '=' );
			
		    cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');

		    if ( cookie_name == check_name ) {
			b_cookie_found = true;
			if ( a_temp_cookie.length > 1 ) {
			    cookie_value = unescape( a_temp_cookie[1].replace(/^\s+|\s+$/g, '') );
			}
			// window.top.location = lastClickedUrl;
			return true;
		    }
		    a_temp_cookie = null;
		    cookie_name = '';
		}

		// If the city cookie has not been found or 
		// we're not in edit mode (SiteBuilder):
		// Fire the city selector!
		if ( !b_cookie_found ) {
		    fFireRegionalSelector();
		    window.focus();
		    return false;
		}
		
	    }
	    
	} else {
	    // Cookies are disabled, go to target page
	    // window.top.location = lastClickedUrl;
	    return true;
	}
    }
}

//Function tests if cookies is enabled in browser

function fTestCookies() {
	// Create a expiry date object
	var oExpDate = new Date();
	// Set the expiry time to be 1 minute in the future
	oExpDate.setTime(oExpDate.getTime() + (60 * 1000));
	// Create the cookie
	document.cookie = 'TestCookie=enabled; expires=' + oExpDate + '; path=/';
	// Get & split the cookie
	var sTestCookies = document.cookie;
	var enabled = sTestCookies.indexOf('TestCookie');
	// Check the value of the cookie
	if (enabled > 0) {
		// If a match then cookies are enabled
		return 1;
	}	else {
		// otherwise they must be disabled
		return 0;
	}
  }
 
// FireRegionalSelector: 
// Hides the traditional drop down, styles it and reveals it.
function fFireRegionalSelector() {
    fShowHideElement('regional-dropdown-2');
    fStyleRegionalDropDownPop('css_regional_dropdown_2', 'regional_dropdown_select_2', '190');
    fShowHideElement('city-filter');
}
// Global Timouts
var nTime;
var nFade;

// -----------------------
// FUNCTION: fUpdateGallery
// DESCRIPTION: Renders image gallery
// ARGUMENTS: nImageId
// RETURN: none
// -----------------------
function fUpdateGallery(nImageId) {
	nSelectedImage = nImageId;
	document.getElementById('pg-main-image').src = aPGContent[nImageId] [0];

	if (aPGContent[nImageId][2] && aPGContent[nImageId][2].length > 0)
	  document.getElementById('pg-description').innerHTML = aPGContent[nImageId] [2];
	else
	  document.getElementById('pg-description').innerHTML = "<span/>";

	if (aPGContent[nImageId][3] && aPGContent[nImageId][3].length > 0)
	  document.getElementById('pg-photographer').innerHTML = aPGContent[nImageId] [3];
	else
  	  document.getElementById('pg-photographer').innerHTML = "<span/>";

	if (aPGContent[nImageId][6] && aPGContent[nImageId][6].length > 0)
	  document.getElementById('pg-caption').innerHTML = aPGContent[nImageId] [6];
	else
  	  document.getElementById('pg-caption').innerHTML = "<span/>";
	
	var sActiveLink = 'ig-link-' + nSelectedImage;
	var sLink = '';
	var nShowLength = aPGContent.length;

	
	for (nCount = 0; nCount < nShowLength; nCount++)
	{
		sLink = 'ig-link-' + nCount;
		
		if (sActiveLink == sLink)
		{
			document.getElementById(sActiveLink).className = 'selected';
		} else {
			var temp = document.getElementById(sLink);
			if(temp){
			document.getElementById(sLink).className = '';
			}
		}
	}
}

// -----------------------
// FUNCTION: fNextPreviousImageGallery
// DESCRIPTION: Next & Previous functionality for Image Gallery Navigation
// ARGUMENTS: nNewSelectedFeature, sDirection
// RETURN: nSelectedTab, nSelectedFeature, nSelectedThumb
// -----------------------
function fNextPreviousImageGallery(nNewSelectedImage, sDirection) {
	
	switch (sDirection)
	{
	case 'next':	  
	  // alert (nNewSelectedImage + ":" + nPGImageCount);
		if (nNewSelectedImage == aPGContent.length)
		{
			nNewSelectedImage = 0;
			var end = nPGImageCount - 1;
			
			if((aPGContent.length-1) < nPGImageCount){
			  end = aPGContent.length-1;
			}
			
			
			switchPictures(0, end);
		}
		else if (!(nNewSelectedImage % nPGImageCount)) {
		  var end;
		  if ((nNewSelectedImage + (nPGImageCount - 1)) < aPGContent.length)
		    end = nNewSelectedImage + (nPGImageCount - 1);
		  else
		    end = aPGContent.length - 1;
		  
		  switchPictures(nNewSelectedImage, end);
		}
	break;
	case 'previous':
	  // alert (nNewSelectedImage + ":" + nPGImageCount);
		if (nNewSelectedImage <= -1)
		{
		    nNewSelectedImage = aPGContent.length-1;
		    var startpic = nNewSelectedImage  - (nNewSelectedImage % nPGImageCount);

		    if (startpic == nNewSelectedImage)
		      startpic =  nNewSelectedImage - (nPGImageCount - 1);

		    if (startpic < 0) startpic = 0;		      
		
		    switchPictures(startpic, nNewSelectedImage);
		}
		else if ((nNewSelectedImage % nPGImageCount) == (nPGImageCount - 1)) {
		  var startpic;
		  if ((nNewSelectedImage - (nPGImageCount - 1)) > 0)
		    startpic = nNewSelectedImage - (nPGImageCount - 1);
		  else
		    startpic = 0
		  
		  switchPictures(startpic, nNewSelectedImage);
		}
	break;
	}
	nSelectedImage = nNewSelectedImage;
	fUpdateGallery(nNewSelectedImage);
}

//---------------------------
// FUNCTION: fImageGalleryPreload
// DESCRIPTION: A function that preloadds images for the Gallery
// ARGUMENTS: 
// RETURN:
//---------------------------
function fImageGalleryPreload() {
	// Determine Gallery Size
	nImages = aPGContent.length;
	// Pre Load the thumbnail images.
	var aThumbs = new Array(); 

	for (nCount = 0; nCount < nImages ; nCount++ ) {
		aThumbs[nCount] = aPGContent[nCount][1];
	}
	// Pass array to preloader
	fPreLoad(aThumbs);

	// Pre Load the main images
	var aImages = new Array();

	for (nCount = 1; nCount < nImages ; nCount++ ) {
		aImages[nCount] = aPGContent[nCount][0];
	}

	// Pass array to preloader
	fPreLoad(aImages);
}
//---------------------------
// FUNCTION: fGalleryControls
// DESCRIPTION: A function that renders the gallery controls
// ARGUMENTS: 
// RETURN:
//---------------------------
function fGalleryControls() {
	var sHTML = '';

	// Write out play button.
	sHTML += '<a href="javascript:void(0)" onclick="fPlay(); return false;"><img src="/templates/img/buttons/play-btn.gif" width="37" height="36" alt="play" title"play" /></a>';
	document.getElementById('slideshow-controls').innerHTML = sHTML;
}
//--------------------------
// FUNCTION: fPlay
// DESCRIPTION: A function that begins the slide show
// ARGUMENTS: 
// RETURN:
//---------------------------
function fPlay() {
	var sHTML = '';

	var nCurrentImage = nSelectedImage;
	var nShowLength = aPGContent.length;

	// Write out pause button
	sHTML += '<a href="javascript:void(0)" onclick="fPause(); return false;"><img src="/templates/img/buttons/pause-btn.gif" width="37" height="36" alt="pause" title="pause" /></a>';
	document.getElementById('slideshow-controls').innerHTML = sHTML;
	
	fFadeIn();
	
}
//--------------------------
// FUNCTION: fPlay
// DESCRIPTION: A function that pauses the slide show
// ARGUMENTS: 
// RETURN:
//---------------------------
function fPause() {
	clearTimeout(nTime);
	fGalleryControls();
}

//DESCRIPTION: A function for pagination of pcitures
//ARGUMENTS: Start and end point of visble "picture scroll".

function switchPictures(start, end){
 
 var updatedlist = "";
 var i;
 var link;

  updatedlist += '<td class="first-child" align=\"right\"><a href="javascript:void(0)" onclick="fNextPreviousImageGallery(nSelectedImage-1, \'previous\'); return false;" onkeydown="fNextPreviousImageGallery(nSelectedImage-1, \'previous\'); return false;"><img src="/templates/img/icons/carousel-previous-20x19.gif" width="20" height="19" alt="previous" /></a></td>\n';  
		
  for(i = start; i <= end; i++){
	
	if(i == end){
	updatedlist += '<td class="relative" align="center" width="25">\n';
	}
	else{
	updatedlist += '<td align="center" width="25">';
	}
	updatedlist += '<a href="javascript:void(0)" id="ig-link-'+ i;
	updatedlist += '" class=" " onmouseover="fShowHideElement(\'ig-thumb-' + i;
	updatedlist += '\');" onmouseout="fShowHideElement(\'ig-thumb-' + i +'\');"; onclick="fUpdateGallery('+ i;
	updatedlist += '); return false;" onkeydown="fUpdateGallery(' + i;
	updatedlist += '); return false;">' + (i+1);
        updatedlist += '<span id="ig-thumb-' + i;
	updatedlist += '" class="photo-gallery-thumb access-text"><img src="'+ aPGContent[i][1];
	updatedlist += '" width="72" height="54" alt="yyy" /></span></a></td>\n';
  }
		
   updatedlist += '<td class="last-child"><a href="javascript:void(0)" onclick="fNextPreviousImageGallery(nSelectedImage+1, \'next\'); return false;" onkeydown="fNextPreviousImageGallery(nSelectedImage+1, \'next\'); return false;"><img src="/templates/img/icons/carousel-next-20x19.gif" width="20" height="19" alt="previous" /></a></td>';
   document.getElementById('pag').innerHTML = updatedlist;

}



//--------------------------
// FUNCTION: fFadeIn
// DESCRIPTION: A function that controls the fade in of images in slide show
// ARGUMENTS: 
// RETURN:
//---------------------------
function fFadeIn() {

	var nDelay = nSlideSpeed * 1000;
	
	var nNewImage = nSelectedImage+1;
	var nShowLength = aPGContent.length-1;

	// Reset slideshow if at last image

	if (nSelectedImage == nShowLength) {
		nNewImage = 0;
		var end =  nPGImageCount - 1;

		if((aPGContent.length-1) < nPGImageCount){
		  end = aPGContent.length-1;
		}
		
	        switchPictures(0, end);
	}
	
	// Change visble "picture scroll" at picture nPGImageCount
	else if (!(nNewImage % nPGImageCount)) {
	  var end;
	  if ((nNewImage + (nPGImageCount - 1)) < aPGContent.length)
	    end = nNewImage + (nPGImageCount - 1);
	  else
	    end = aPGContent.length - 1;
	  
	  switchPictures(nNewImage, end);
	}
	
	nSelectedImage = nNewImage;
	fUpdateGallery(nNewImage);
	
	fChangeOpacity(nFadePercentage);
	nTime =  setTimeout("fFadeIn();",nDelay);
	



}
//--------------------------
// FUNCTION: ffChangeOpacity
// DESCRIPTION: A function that controls opacity in various browsers
// ARGUMENTS: 
// RETURN:
//---------------------------
function fChangeOpacity(opacity) {	
	var object = document.getElementById('pg-main-image').style;
	object.opacity = (opacity / 100);
	object.MozOpacity = (opacity / 100);
	object.KhtmlOpacity = (opacity / 100);
	object.filter = "alpha(opacity=" + opacity + ")";
	opacity+=nFadePercentage;
	
	if (opacity <= 100)
	{
		 nFade = setTimeout("fChangeOpacity(" +opacity+ ");", nFadeSpeed); 
	}
}
// -----------------------
// FUNCTION: fShowHideHallOfFameShameContent
// DESCRIPTION: A function that shows or hides the top stories content.
// ARGUMENTS: sElementId
// RETURN: None
// -----------------------
function fShowHideHallOfFameShameContent(sElementId, uni_id) {
  sSelectedTab = self["sSelectedHallOfFameShameTab_" + uni_id];
  // Update the content and save which is selected
  fShowHideElement(sSelectedTab);
  fShowHideElement(sElementId + "-" + uni_id);
  self["sSelectedHallOfFameShameTab_" + uni_id] = sElementId + "-" + uni_id;
  // Update the tabs
  sSelectedTab += '-tab';
  document.getElementById(sSelectedTab).className = '';
  sSelectedTab = self["sSelectedHallOfFameShameTab_" + uni_id] + '-tab';
  document.getElementById(sSelectedTab).className = 'active';
}

//-----------------------
// FUNCTION: fShowHideHallOfFameShameContentNoNumber
// DESCRIPTION: A function that shows or hides the top stories content.
// ARGUMENTS: sElementId
// RETURN: None
// -----------------------
function fShowHideHallOfFameShameContentNoNumber(sElementId, uni_id) {
  sSelectedTab = self["sSelectedHallOfFameShameNoNumberTab_" + uni_id];
  // Update the content and save which is selected
  fShowHideElement(sSelectedTab);
  fShowHideElement(sElementId + "-" + uni_id);
  self["sSelectedHallOfFameShameNoNumberTab_" + uni_id] = sElementId + "-" + uni_id;
  // Update the tabs
  sSelectedTab += '-tab';
  document.getElementById(sSelectedTab).className = '';
  sSelectedTab = self["sSelectedHallOfFameShameNoNumberTab_" + uni_id] + '-tab';
  document.getElementById(sSelectedTab).className = 'active';
}
function pollVote(poll_component_id, page_path, form_id, target_div, question_div, variant, tab, unique_ident) {
  var form = $(form_id);
  var radio_inputs = form.getInputs('radio', 'poll_radio');
  var selected_input = radio_inputs.find(function(radio) { return radio.checked; });
  var choice;
  if (selected_input)
    choice = selected_input.value;
  
  if(choice) {
    new Ajax.Updater(target_div, page_path, {
      method: 'get',
      parameters: {
        __xsl: '/poll-actions.xsl',
        __toolbar: 0,
        action: 'vote',
        choice: choice,
        poll: poll_component_id,
        variant: variant,
	tab: tab,
        'unique-ident': unique_ident
        }
      });
  }
  else
    return false;
  
  fShowHideElement(question_div);
  fShowHideElement(target_div);
}


function pollLoad(cms_path, target_div, variant, tab, unique_ident, render_image, render_links) {

  var placeholder_div_name = target_div + '-placeholder';
  var placeholder_div = $(placeholder_div_name);
  
  if(placeholder_div) {      
    new Ajax.Updater(target_div, cms_path, {
      method: 'get',
      parameters: {
        __xsl: '/poll-actions.xsl',
        __toolbar: 0,
        variant: variant,
	tab: tab,
	'unique-ident': unique_ident,
        'render-image': render_image,
        'render-links': render_links    
        }
      });
  }
}
function loadMap (mapContainerDiv, saveDataVar) {
  // Initialize the map      
  var gMap = new google.maps.Map2(document.getElementById(mapContainerDiv));
  gMap.enableScrollWheelZoom();
  gMap.disableDoubleClickZoom();
  gMap.addControl(new google.maps.SmallMapControl());
  gMap.addControl(new google.maps.MapTypeControl());

  var centerLat = 0;
  var centerLng = 0;
  var zoom = 0;
  var markers = [];

  if (document.getElementById(saveDataVar).value) {
    var saveDataXml = google.maps.Xml.parse(document.getElementById(saveDataVar).value);
    var mapStateNode = saveDataXml.getElementsByTagName("Mapstate")[0];
    var placeMarkNodes = saveDataXml.getElementsByTagName("Placemark");

    if (mapStateNode) {
      var centerLatLng = mapStateNode.getElementsByTagName("center")[0].firstChild.nodeValue.split(",");
      centerLat = parseFloat(centerLatLng[0]);
      centerLng = parseFloat(centerLatLng[1]);
      zoom = parseInt(mapStateNode.getElementsByTagName("zoom")[0].firstChild.nodeValue);
    }

    if (placeMarkNodes) {
      markers = placeMarkNodes;
    }    
  }

  // Now that we now the map state, set it.
  gMap.setCenter(new google.maps.LatLng(centerLat, centerLng), zoom);
  
  for (var i = 0; i < markers.length; i++) {
    var titleNode  = markers[i].getElementsByTagName("name")[0];
    var title = (titleNode.firstChild ? titleNode.firstChild.nodeValue : "");      
    var descNode = markers[i].getElementsByTagName("description")[0];
    var desc = (descNode.firstChild ? descNode.firstChild.nodeValue : "");
    var coords = markers[i].getElementsByTagName("coordinates")[0].firstChild.nodeValue.split(",");
    var lat = parseFloat(coords[0]);
    var lng = parseFloat(coords[1]);

    var marker = new MapMarker(lat, lng, title, desc);
    gMap.addOverlay (marker);
  }
}

function MapMarker (lat, lng, title, desc) {
  var markerOptions = { title: title };
  var marker  = new google.maps.Marker (new google.maps.LatLng (lat, lng), markerOptions);
	
  var containerDiv = document.createElement ("div");
  containerDiv.className = "marker";
  $(containerDiv).setStyle({width: '217px'});

  var titleDiv = document.createElement ("div");
  titleDiv.className = "title";

  titleDiv.innerHTML = unquoteHTML (title);

  containerDiv.appendChild (titleDiv);

  var descDiv = document.createElement ("div");
  descDiv.className = "description";

  descDiv.innerHTML = unquoteHTML (desc).replace (/\n/gi, "<br/>");

  containerDiv.appendChild (descDiv);

  marker.bindInfoWindow (containerDiv);
  return marker;
}

function quoteHTML (unquoted) {
  var quoted = unquoted;
  quoted = quoted.replace (/&/gi, "&amp;");
  quoted = quoted.replace (/</gi, "&lt;");
  quoted = quoted.replace (/>/gi, "&gt;");
  return quoted;  
}

function unquoteHTML (unquoted) {
  var quoted = unquoted;
  quoted = quoted.replace (/&amp;/gi, "&");
  quoted = quoted.replace (/&lt;/gi, "<");
  quoted = quoted.replace (/&gt;/gi, ">");
  return quoted;  
}
// Sort results based on relevace, this is the default so we do that
// by setting the form variable sort to the default value 'date:D:L:d1'.
function searchSortRelevance() {
  $('search-sort-1').value = 'date:D:L:d1';
  searchFormSubmit(1);
}

// Sort results based on date (and secondly relevance) this is done
// by setting the the form variable sort to 'date:D:S:d1'.
function searchSortDate() {
  $('search-sort-1').value = 'date:D:S:d1';
  searchFormSubmit(1);
}

// Submit the form. No posts without query.
function searchFormSubmit(form_id, filtered) {
  if ($F('search-query-' + form_id) == '') 
    return false;

  // Open popup with google search
  if ($F('google-search-radio-'+ form_id) == 'google') {
    window.open('http://www.google.com/search?q=' + $F('search-query-' + form_id));
    return false;
  }
    

  if (filtered) {
    searchFormSubmitFiltered(form_id);
  }
  else {
    // perform unfiltered query
    $('search-q-' + form_id).value = $F('search-query-' + form_id);
    
    // reset filters
    $('search-requiredfields-' + form_id).value = '';
    $('search-meta-' + form_id).value = '';
    $('search-date-' + form_id).value = '';
    
    $('search-form-' + form_id).submit();
  }
}

// Submit the form but add required filters
function searchFormSubmitFiltered(form_id) {
  $('search-requiredfields-' + form_id).value = '';
  
  if ($F('search-meta-' + form_id) != '') {
    $('search-requiredfields-' + form_id).value = $F('search-meta-' + form_id);
    $('search-q-' + form_id).value = $F('search-query-' + form_id);
  }
  
  if ($F('search-date-' + form_id) != '')    
    $('search-q-' + form_id).value = $F('search-query-' + form_id) + ' ' + $F('search-date-' + form_id);
  else
    $('search-q-' + form_id).value = $F('search-query-' + form_id);

  $('search-form-' + form_id).submit();
}

// Re-submit the form with the suggested query string
function searchSpellingFormSubmit(suggested_query) {
  $('search-query-1').value = suggested_query;
  searchFormSubmit(1);
}

// Set the mediatype filter
function searchSetMediatypeFilter(filter, a_href) {
  if($F('search-meta-1') == filter) {
    // Unset filter
    active_filter = '';
    a_href.removeClassName('bold'); 
    a_href.removeClassName('color-666');
    $('search-meta-1').value = '';
    $('search-meta-2').value = '';
    return;
  }

  if (active_filter != '') {
    // Deactive current filter
    var active_filter_a_href = $(active_filter);
    active_filter_a_href.removeClassName('bold'); 
    active_filter_a_href.removeClassName('color-666');
  }

  // Activate new filter
  a_href.addClassName('bold');
  a_href.addClassName('color-666');
  active_filter = a_href.id;

  // Set form
  $('search-meta-1').value = filter;
  $('search-meta-2').value = filter;
}

// Set the date filter 
function searchSetDateFilter(filter) {
  if($F('search-date-1') == filter)
    return;
    
  $('search-date-1').value = filter;
  $('search-date-2').value = filter;
}

// Change class for the selected By type filter based on what is selected
function searchHighlightFilter() {
  var requiredfields = $F('search-meta-1');

  if (requiredfields.match('mediatype:Article\\|mediatype:Feed\\|mediatype:Blog')) {
    $('search-filter-articles').addClassName('bold color-666');
    active_filter = $('search-filter-articles').id;
  }
  
  else if (requiredfields.match('mediatype:Article')) {
    $('search-filter-article').addClassName('bold color-666');
    active_filter = $('search-filter-article').id;
  }

  else if (requiredfields.match('mediatype:Feed')) {
    $('search-filter-feed').addClassName('bold color-666');
    active_filter = $('search-filter-feed').id;
  }

  else if (requiredfields.match('mediatype:Blog')) {
    $('search-filter-blog').addClassName('bold color-666');
    active_filter = $('search-filter-blog').id;
  }
  
  else if (requiredfields.match('mediatype:Photo')) {
    $('search-filter-photo').addClassName('bold color-666');
    active_filter = $('search-filter-photo').id;
  }

  else if (requiredfields.match('mediatype:Video')) {
    $('search-filter-video').addClassName('bold color-666');
    active_filter = $('search-filter-video').id;
  }
  
  else if (requiredfields.match('mediatype:Audio')) {
    $('search-filter-audio').addClassName('bold color-666');
    active_filter = $('search-filter-audio').id;
  }

  else if (requiredfields.match('mediatype:Podcast')) {
    $('search-filter-podcast').addClassName('bold color-666');
    active_filter = $('search-filter-podcast').id;
  }

  else if (requiredfields.match('mediatype:RSS')) {
    $('search-filter-rss').addClassName('bold color-666');
    active_filter = $('search-filter-rss').id;
  }

  else if (requiredfields.match('mediatype:Poll')) {
    $('search-filter-poll').addClassName('bold color-666');
    active_filter = $('search-filter-poll').id;
  }
}

// Do the same search again, but start displaying results
// from 'start'.
function searchPagedStart (start) {
  if (!start) {
    return;
  }

  $('search-start-1').value = start;
    searchFormSubmitFiltered(1)
}


// -----------------------
// FUNCTION: fStyleDropDowns
// DESCRIPTION: A function that writes out a CSS drop down
// ARGUMENTS: sElemendId, sFormId , SDropdownWidth - can be either 70, 160, 195, 235 - NB to use different widths new versions of the CSS class drop-down-bg-xxx will need to be created
// RETURN: None
// -----------------------
function fStyleDropDowns(sElementId, sFormId, sDropdownWidth) {
	var sDropDownAreaName = sElementId + '-area';
	var sFirstOptionId = sElementId + '-id';
	var sFirstValue = document.getElementById(sFormId).options[0].text;
	var sFormLength = document.getElementById(sFormId).options.length;

	var sHTML = '';

	sHTML += '<div class="css-drop-down drop-down-bg-' + sDropdownWidth + ' width-' + sDropdownWidth + '">';
	sHTML += '<ul class="v-small">';
	sHTML += '<li><a id="' + sFirstOptionId + '" class="first" style="width:' + (sDropdownWidth -3) + 'px;" href="javascript:fShowHideElement(\'' + sDropDownAreaName + '\');">' + sFirstValue + '</a>';
	if (sFormLength > 10)
	{
		sHTML += '<ul id="' + sDropDownAreaName + '" class="access-text long-drop-down" style="width:' + (sDropdownWidth - 7) + 'px; ">';
	} else {
	sHTML += '<ul id="' + sDropDownAreaName + '" class="access-text" style="width:' + (sDropdownWidth -25) + 'px;">';
	}
	nInputCount = document.getElementById(sFormId).length;
	
	for (nCount = 1; nCount < nInputCount; nCount++ )
	{
		sWriteValue = document.getElementById(sFormId).options[nCount].text;
		sHTML += '<li><a style="width:' + (sDropdownWidth -25) + 'px;" href="javascript:fUpdateHiddenForm(\'' + sFormId + '\', \'' + nCount + '\', \'' + sDropDownAreaName + '\', \'' + sFirstOptionId + '\');">' + sWriteValue + '</a>';
	}
	sHTML += '</ul></li>';
	sHTML += '</ul>'
	sHTML += '</div>';

	document.getElementById(sElementId).innerHTML = sHTML;
	fShowHideElement(sElementId);
}
// -----------------------
// FUNCTION: fUpdateHiddenForm
// DESCRIPTION: A function that updates a hidden dropdown for CSS psuedo drop downs
// ARGUMENTS: sOption
// RETURN: None
// -----------------------
function fUpdateHiddenForm(sFormId, nOption, sDropDownAreaName, sFirstOptionId) {
	var sSelectText = document.getElementById(sFormId).options[nOption].text;
	// Update the real form
	document.getElementById(sFormId).options[nOption].selected = true;
	// Update the psuedo form
	document.getElementById(sFirstOptionId).innerHTML = sSelectText;
	fShowHideElement(sDropDownAreaName);
}
// -----------------------
// FUNCTION: fStyleDropDown
// DESCRIPTION: A function that writes out a CSS drop down
// ARGUMENTS: sElemendId
// RETURN: None
// -----------------------
function fStyleDropDown(sElementId) {

	sHTML = '';
	sHTML += '<div class="css-drop-down z-index-9000">';
	sHTML += '<ul>';
	sHTML += '<li><a class="first" href="javascript:fShowHideElement(\'regions\');">Please select a city</a>';
	sHTML += '<ul id="regions" class="access-text">';
	sHTML += '<li><span class="padding-left-10">UNITED STATES</span>';
	sHTML += '<ul>';
	sHTML += '<li><a href="xxx">New York</a></li>';
	sHTML += '<li class="last"><a href="xxx">Boston</a></li>';
	sHTML += '</ul>';
	sHTML += '</li>';
	sHTML += '<li><a href="xxx">CANADA</a></li>';
	sHTML += '<li><a href="xxx">SPAIN</a></li>';
	sHTML += '<li><a href="xxx">FRANCE</a></li>';
	sHTML += '</ul>';
	sHTML += '</li>';
	sHTML += '</ul>';
	sHTML += '</div>';
	document.getElementById(sElementId).innerHTML = sHTML;
}
// -----------------------
// FUNCTION: fStyleDropDownRegional
// DESCRIPTION: A function that writes out a CSS drop down
// ARGUMENTS: sElemendId
// RETURN: None
// -----------------------
function fStyleDropDownRegional(sElementId) {
	sHTML = '';
	sHTML += '<div class="css-drop-down">';
	sHTML += '<ul>';
	sHTML += '<li><a class="first" href="javascript:fShowHideElement(\'regions\');">Please select a location</a>';
	sHTML += '<ul id="regions" class="access-text">';
	sHTML += '<li><span class="padding-left-10">UNITED STATES</span>';
	sHTML += '<ul>';
	sHTML += '<li><a href="xxx">New York</a></li>';
	sHTML += '<li class="last"><a href="xxx">Boston</a></li>';
	sHTML += '</ul>';
	sHTML += '</li>';
	sHTML += '<li><a href="xxx">CANADA</a></li>';
	sHTML += '<li><a href="xxx">SPAIN</a></li>';
	sHTML += '<li><a href="xxx">FRANCE</a></li>';
	sHTML += '</ul>';
	sHTML += '</li>';
	sHTML += '</ul>';
	sHTML += '</div>';
	document.getElementById(sElementId).innerHTML = sHTML;
}function fHighlightStars(nStarCount) {
	clearTimeout(nTimeOut);
	for (nCount = 1; nCount <= 5 ; nCount++ )
	{
		if (nCount <= nStarCount)
		{
			document.getElementById('star-' + nCount).src = "/templates/img/icons/orange-star-18x12.gif";
		} else {
			document.getElementById('star-' + nCount).src = "/templates/img/icons/grey-star-18x12.gif";
		}
	}
}
function fShowCurrentRating() {
	for (nCount = 1; nCount <= 5 ; nCount++ )
	{
		if (nCount <= nRating)
		{
			document.getElementById('star-' + nCount).src = "/templates/img/icons/orange-star-18x12.gif";
		} else {
			document.getElementById('star-' + nCount).src = "/templates/img/icons/grey-star-18x12.gif";
		}
	}
}
// Note: It's important that the id attrib of the object tag and the 
// name of the embed tag are different.
function fRenderFlashObject(sId, sFilePath, iWidth, iHeight) {

 var c = document.getElementById(sId);

 c.innerHTML = 
  '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" ' + 
	  'wmode="opaque" ' +
          'codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" ' + 
          'width="' + iWidth + '" ' + 
          'height="' + iHeight + '" ' + 
          'id="abc_' + sId + '" ' + 
          'align="top">' +
    '<param name="allowScriptAccess" value="sameDomain" />' +
    '<param name="movie" value="' + sFilePath + '" />' +
    '<param name="SRC" value="' + sFilePath + '" />' +
    '<param name="quality" value="high" />' + 
    '<param name="bgcolor" value="#ffffff" />' + 
    '<param name="wmode" value="opaque">' +
    '<embed src="' + sFilePath + '" ' + 
           'quality="high" ' + 
           'bgcolor="#ffffff" ' + 
           'width="' + iWidth + '" ' + 
           'height="' + iHeight + '" ' + 
           'name="def_' + sId + '" ' + 
           'align="top" ' + 
           'allowScriptAccess="sameDomain" ' + 
           'type="application/x-shockwave-flash" ' + 
	   'wmode="opaque" ' +
           'pluginspage="http://www.macromedia.com/go/getflashplayer" />' +
  '</object>';     
}/*  Prototype JavaScript framework, version 1.6.0.2
 *  (c) 2005-2008 Sam Stephenson
 *
 *  Prototype is freely distributable under the terms of an MIT-style license.
 *  For details, see the Prototype web site: http://www.prototypejs.org/
 *
 *--------------------------------------------------------------------------*/

var Prototype = {
  Version: '1.6.0.2',

  Browser: {
    IE:     !!(window.attachEvent && !window.opera),
    Opera:  !!window.opera,
    WebKit: navigator.userAgent.indexOf('AppleWebKit/') > -1,
    Gecko:  navigator.userAgent.indexOf('Gecko') > -1 && navigator.userAgent.indexOf('KHTML') == -1,
    MobileSafari: !!navigator.userAgent.match(/Apple.*Mobile.*Safari/)
  },

  BrowserFeatures: {
    XPath: !!document.evaluate,
    ElementExtensions: !!window.HTMLElement,
    SpecificElementExtensions:
      document.createElement('div').__proto__ &&
      document.createElement('div').__proto__ !==
        document.createElement('form').__proto__
  },

  ScriptFragment: '<script[^>]*>([\\S\\s]*?)<\/script>',
  JSONFilter: /^\/\*-secure-([\s\S]*)\*\/\s*$/,

  emptyFunction: function() { },
  K: function(x) { return x }
};

if (Prototype.Browser.MobileSafari)
  Prototype.BrowserFeatures.SpecificElementExtensions = false;


/* Based on Alex Arnell's inheritance implementation. */
var Class = {
  create: function() {
    var parent = null, properties = $A(arguments);
    if (Object.isFunction(properties[0]))
      parent = properties.shift();

    function klass() {
      this.initialize.apply(this, arguments);
    }

    Object.extend(klass, Class.Methods);
    klass.superclass = parent;
    klass.subclasses = [];

    if (parent) {
      var subclass = function() { };
      subclass.prototype = parent.prototype;
      klass.prototype = new subclass;
      parent.subclasses.push(klass);
    }

    for (var i = 0; i < properties.length; i++)
      klass.addMethods(properties[i]);

    if (!klass.prototype.initialize)
      klass.prototype.initialize = Prototype.emptyFunction;

    klass.prototype.constructor = klass;

    return klass;
  }
};

Class.Methods = {
  addMethods: function(source) {
    var ancestor   = this.superclass && this.superclass.prototype;
    var properties = Object.keys(source);

    if (!Object.keys({ toString: true }).length)
      properties.push("toString", "valueOf");

    for (var i = 0, length = properties.length; i < length; i++) {
      var property = properties[i], value = source[property];
      if (ancestor && Object.isFunction(value) &&
          value.argumentNames().first() == "$super") {
        var method = value, value = Object.extend((function(m) {
          return function() { return ancestor[m].apply(this, arguments) };
        })(property).wrap(method), {
          valueOf:  function() { return method },
          toString: function() { return method.toString() }
        });
      }
      this.prototype[property] = value;
    }

    return this;
  }
};

var Abstract = { };

Object.extend = function(destination, source) {
  for (var property in source)
    destination[property] = source[property];
  return destination;
};

Object.extend(Object, {
  inspect: function(object) {
    try {
      if (Object.isUndefined(object)) return 'undefined';
      if (object === null) return 'null';
      return object.inspect ? object.inspect() : String(object);
    } catch (e) {
      if (e instanceof RangeError) return '...';
      throw e;
    }
  },

  toJSON: function(object) {
    var type = typeof object;
    switch (type) {
      case 'undefined':
      case 'function':
      case 'unknown': return;
      case 'boolean': return object.toString();
    }

    if (object === null) return 'null';
    if (object.toJSON) return object.toJSON();
    if (Object.isElement(object)) return;

    var results = [];
    for (var property in object) {
      var value = Object.toJSON(object[property]);
      if (!Object.isUndefined(value))
        results.push(property.toJSON() + ': ' + value);
    }

    return '{' + results.join(', ') + '}';
  },

  toQueryString: function(object) {
    return $H(object).toQueryString();
  },

  toHTML: function(object) {
    return object && object.toHTML ? object.toHTML() : String.interpret(object);
  },

  keys: function(object) {
    var keys = [];
    for (var property in object)
      keys.push(property);
    return keys;
  },

  values: function(object) {
    var values = [];
    for (var property in object)
      values.push(object[property]);
    return values;
  },

  clone: function(object) {
    return Object.extend({ }, object);
  },

  isElement: function(object) {
    return object && object.nodeType == 1;
  },

  isArray: function(object) {
    return object != null && typeof object == "object" &&
      'splice' in object && 'join' in object;
  },

  isHash: function(object) {
    return object instanceof Hash;
  },

  isFunction: function(object) {
    return typeof object == "function";
  },

  isString: function(object) {
    return typeof object == "string";
  },

  isNumber: function(object) {
    return typeof object == "number";
  },

  isUndefined: function(object) {
    return typeof object == "undefined";
  }
});

Object.extend(Function.prototype, {
  argumentNames: function() {
    var names = this.toString().match(/^[\s\(]*function[^(]*\((.*?)\)/)[1].split(",").invoke("strip");
    return names.length == 1 && !names[0] ? [] : names;
  },

  bind: function() {
    if (arguments.length < 2 && Object.isUndefined(arguments[0])) return this;
    var __method = this, args = $A(arguments), object = args.shift();
    return function() {
      return __method.apply(object, args.concat($A(arguments)));
    }
  },

  bindAsEventListener: function() {
    var __method = this, args = $A(arguments), object = args.shift();
    return function(event) {
      return __method.apply(object, [event || window.event].concat(args));
    }
  },

  curry: function() {
    if (!arguments.length) return this;
    var __method = this, args = $A(arguments);
    return function() {
      return __method.apply(this, args.concat($A(arguments)));
    }
  },

  delay: function() {
    var __method = this, args = $A(arguments), timeout = args.shift() * 1000;
    return window.setTimeout(function() {
      return __method.apply(__method, args);
    }, timeout);
  },

  wrap: function(wrapper) {
    var __method = this;
    return function() {
      return wrapper.apply(this, [__method.bind(this)].concat($A(arguments)));
    }
  },

  methodize: function() {
    if (this._methodized) return this._methodized;
    var __method = this;
    return this._methodized = function() {
      return __method.apply(null, [this].concat($A(arguments)));
    };
  }
});

Function.prototype.defer = Function.prototype.delay.curry(0.01);

Date.prototype.toJSON = function() {
  return '"' + this.getUTCFullYear() + '-' +
    (this.getUTCMonth() + 1).toPaddedString(2) + '-' +
    this.getUTCDate().toPaddedString(2) + 'T' +
    this.getUTCHours().toPaddedString(2) + ':' +
    this.getUTCMinutes().toPaddedString(2) + ':' +
    this.getUTCSeconds().toPaddedString(2) + 'Z"';
};

var Try = {
  these: function() {
    var returnValue;

    for (var i = 0, length = arguments.length; i < length; i++) {
      var lambda = arguments[i];
      try {
        returnValue = lambda();
        break;
      } catch (e) { }
    }

    return returnValue;
  }
};

RegExp.prototype.match = RegExp.prototype.test;

RegExp.escape = function(str) {
  return String(str).replace(/([.*+?^=!:${}()|[\]\/\\])/g, '\\$1');
};

/*--------------------------------------------------------------------------*/

var PeriodicalExecuter = Class.create({
  initialize: function(callback, frequency) {
    this.callback = callback;
    this.frequency = frequency;
    this.currentlyExecuting = false;

    this.registerCallback();
  },

  registerCallback: function() {
    this.timer = setInterval(this.onTimerEvent.bind(this), this.frequency * 1000);
  },

  execute: function() {
    this.callback(this);
  },

  stop: function() {
    if (!this.timer) return;
    clearInterval(this.timer);
    this.timer = null;
  },

  onTimerEvent: function() {
    if (!this.currentlyExecuting) {
      try {
        this.currentlyExecuting = true;
        this.execute();
      } finally {
        this.currentlyExecuting = false;
      }
    }
  }
});
Object.extend(String, {
  interpret: function(value) {
    return value == null ? '' : String(value);
  },
  specialChar: {
    '\b': '\\b',
    '\t': '\\t',
    '\n': '\\n',
    '\f': '\\f',
    '\r': '\\r',
    '\\': '\\\\'
  }
});

Object.extend(String.prototype, {
  gsub: function(pattern, replacement) {
    var result = '', source = this, match;
    replacement = arguments.callee.prepareReplacement(replacement);

    while (source.length > 0) {
      if (match = source.match(pattern)) {
        result += source.slice(0, match.index);
        result += String.interpret(replacement(match));
        source  = source.slice(match.index + match[0].length);
      } else {
        result += source, source = '';
      }
    }
    return result;
  },

  sub: function(pattern, replacement, count) {
    replacement = this.gsub.prepareReplacement(replacement);
    count = Object.isUndefined(count) ? 1 : count;

    return this.gsub(pattern, function(match) {
      if (--count < 0) return match[0];
      return replacement(match);
    });
  },

  scan: function(pattern, iterator) {
    this.gsub(pattern, iterator);
    return String(this);
  },

  truncate: function(length, truncation) {
    length = length || 30;
    truncation = Object.isUndefined(truncation) ? '...' : truncation;
    return this.length > length ?
      this.slice(0, length - truncation.length) + truncation : String(this);
  },

  strip: function() {
    return this.replace(/^\s+/, '').replace(/\s+$/, '');
  },

  stripTags: function() {
    return this.replace(/<\/?[^>]+>/gi, '');
  },

  stripScripts: function() {
    return this.replace(new RegExp(Prototype.ScriptFragment, 'img'), '');
  },

  extractScripts: function() {
    var matchAll = new RegExp(Prototype.ScriptFragment, 'img');
    var matchOne = new RegExp(Prototype.ScriptFragment, 'im');
    return (this.match(matchAll) || []).map(function(scriptTag) {
      return (scriptTag.match(matchOne) || ['', ''])[1];
    });
  },

  evalScripts: function() {
    return this.extractScripts().map(function(script) { return eval(script) });
  },

  escapeHTML: function() {
    var self = arguments.callee;
    self.text.data = this;
    return self.div.innerHTML;
  },

  unescapeHTML: function() {
    var div = new Element('div');
    div.innerHTML = this.stripTags();
    return div.childNodes[0] ? (div.childNodes.length > 1 ?
      $A(div.childNodes).inject('', function(memo, node) { return memo+node.nodeValue }) :
      div.childNodes[0].nodeValue) : '';
  },

  toQueryParams: function(separator) {
    var match = this.strip().match(/([^?#]*)(#.*)?$/);
    if (!match) return { };

    return match[1].split(separator || '&').inject({ }, function(hash, pair) {
      if ((pair = pair.split('='))[0]) {
        var key = decodeURIComponent(pair.shift());
        var value = pair.length > 1 ? pair.join('=') : pair[0];
        if (value != undefined) value = decodeURIComponent(value);

        if (key in hash) {
          if (!Object.isArray(hash[key])) hash[key] = [hash[key]];
          hash[key].push(value);
        }
        else hash[key] = value;
      }
      return hash;
    });
  },

  toArray: function() {
    return this.split('');
  },

  succ: function() {
    return this.slice(0, this.length - 1) +
      String.fromCharCode(this.charCodeAt(this.length - 1) + 1);
  },

  times: function(count) {
    return count < 1 ? '' : new Array(count + 1).join(this);
  },

  camelize: function() {
    var parts = this.split('-'), len = parts.length;
    if (len == 1) return parts[0];

    var camelized = this.charAt(0) == '-'
      ? parts[0].charAt(0).toUpperCase() + parts[0].substring(1)
      : parts[0];

    for (var i = 1; i < len; i++)
      camelized += parts[i].charAt(0).toUpperCase() + parts[i].substring(1);

    return camelized;
  },

  capitalize: function() {
    return this.charAt(0).toUpperCase() + this.substring(1).toLowerCase();
  },

  underscore: function() {
    return this.gsub(/::/, '/').gsub(/([A-Z]+)([A-Z][a-z])/,'#{1}_#{2}').gsub(/([a-z\d])([A-Z])/,'#{1}_#{2}').gsub(/-/,'_').toLowerCase();
  },

  dasherize: function() {
    return this.gsub(/_/,'-');
  },

  inspect: function(useDoubleQuotes) {
    var escapedString = this.gsub(/[\x00-\x1f\\]/, function(match) {
      var character = String.specialChar[match[0]];
      return character ? character : '\\u00' + match[0].charCodeAt().toPaddedString(2, 16);
    });
    if (useDoubleQuotes) return '"' + escapedString.replace(/"/g, '\\"') + '"';
    return "'" + escapedString.replace(/'/g, '\\\'') + "'";
  },

  toJSON: function() {
    return this.inspect(true);
  },

  unfilterJSON: function(filter) {
    return this.sub(filter || Prototype.JSONFilter, '#{1}');
  },

  isJSON: function() {
    var str = this;
    if (str.blank()) return false;
    str = this.replace(/\\./g, '@').replace(/"[^"\\\n\r]*"/g, '');
    return (/^[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]*$/).test(str);
  },

  evalJSON: function(sanitize) {
    var json = this.unfilterJSON();
    try {
      if (!sanitize || json.isJSON()) return eval('(' + json + ')');
    } catch (e) { }
    throw new SyntaxError('Badly formed JSON string: ' + this.inspect());
  },

  include: function(pattern) {
    return this.indexOf(pattern) > -1;
  },

  startsWith: function(pattern) {
    return this.indexOf(pattern) === 0;
  },

  endsWith: function(pattern) {
    var d = this.length - pattern.length;
    return d >= 0 && this.lastIndexOf(pattern) === d;
  },

  empty: function() {
    return this == '';
  },

  blank: function() {
    return /^\s*$/.test(this);
  },

  interpolate: function(object, pattern) {
    return new Template(this, pattern).evaluate(object);
  }
});

if (Prototype.Browser.WebKit || Prototype.Browser.IE) Object.extend(String.prototype, {
  escapeHTML: function() {
    return this.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
  },
  unescapeHTML: function() {
    return this.replace(/&amp;/g,'&').replace(/&lt;/g,'<').replace(/&gt;/g,'>');
  }
});

String.prototype.gsub.prepareReplacement = function(replacement) {
  if (Object.isFunction(replacement)) return replacement;
  var template = new Template(replacement);
  return function(match) { return template.evaluate(match) };
};

String.prototype.parseQuery = String.prototype.toQueryParams;

Object.extend(String.prototype.escapeHTML, {
  div:  document.createElement('div'),
  text: document.createTextNode('')
});

with (String.prototype.escapeHTML) div.appendChild(text);

var Template = Class.create({
  initialize: function(template, pattern) {
    this.template = template.toString();
    this.pattern = pattern || Template.Pattern;
  },

  evaluate: function(object) {
    if (Object.isFunction(object.toTemplateReplacements))
      object = object.toTemplateReplacements();

    return this.template.gsub(this.pattern, function(match) {
      if (object == null) return '';

      var before = match[1] || '';
      if (before == '\\') return match[2];

      var ctx = object, expr = match[3];
      var pattern = /^([^.[]+|\[((?:.*?[^\\])?)\])(\.|\[|$)/;
      match = pattern.exec(expr);
      if (match == null) return before;

      while (match != null) {
        var comp = match[1].startsWith('[') ? match[2].gsub('\\\\]', ']') : match[1];
        ctx = ctx[comp];
        if (null == ctx || '' == match[3]) break;
        expr = expr.substring('[' == match[3] ? match[1].length : match[0].length);
        match = pattern.exec(expr);
      }

      return before + String.interpret(ctx);
    });
  }
});
Template.Pattern = /(^|.|\r|\n)(#\{(.*?)\})/;

var $break = { };

var Enumerable = {
  each: function(iterator, context) {
    var index = 0;
    iterator = iterator.bind(context);
    try {
      this._each(function(value) {
        iterator(value, index++);
      });
    } catch (e) {
      if (e != $break) throw e;
    }
    return this;
  },

  eachSlice: function(number, iterator, context) {
    iterator = iterator ? iterator.bind(context) : Prototype.K;
    var index = -number, slices = [], array = this.toArray();
    while ((index += number) < array.length)
      slices.push(array.slice(index, index+number));
    return slices.collect(iterator, context);
  },

  all: function(iterator, context) {
    iterator = iterator ? iterator.bind(context) : Prototype.K;
    var result = true;
    this.each(function(value, index) {
      result = result && !!iterator(value, index);
      if (!result) throw $break;
    });
    return result;
  },

  any: function(iterator, context) {
    iterator = iterator ? iterator.bind(context) : Prototype.K;
    var result = false;
    this.each(function(value, index) {
      if (result = !!iterator(value, index))
        throw $break;
    });
    return result;
  },

  collect: function(iterator, context) {
    iterator = iterator ? iterator.bind(context) : Prototype.K;
    var results = [];
    this.each(function(value, index) {
      results.push(iterator(value, index));
    });
    return results;
  },

  detect: function(iterator, context) {
    iterator = iterator.bind(context);
    var result;
    this.each(function(value, index) {
      if (iterator(value, index)) {
        result = value;
        throw $break;
      }
    });
    return result;
  },

  findAll: function(iterator, context) {
    iterator = iterator.bind(context);
    var results = [];
    this.each(function(value, index) {
      if (iterator(value, index))
        results.push(value);
    });
    return results;
  },

  grep: function(filter, iterator, context) {
    iterator = iterator ? iterator.bind(context) : Prototype.K;
    var results = [];

    if (Object.isString(filter))
      filter = new RegExp(filter);

    this.each(function(value, index) {
      if (filter.match(value))
        results.push(iterator(value, index));
    });
    return results;
  },

  include: function(object) {
    if (Object.isFunction(this.indexOf))
      if (this.indexOf(object) != -1) return true;

    var found = false;
    this.each(function(value) {
      if (value == object) {
        found = true;
        throw $break;
      }
    });
    return found;
  },

  inGroupsOf: function(number, fillWith) {
    fillWith = Object.isUndefined(fillWith) ? null : fillWith;
    return this.eachSlice(number, function(slice) {
      while(slice.length < number) slice.push(fillWith);
      return slice;
    });
  },

  inject: function(memo, iterator, context) {
    iterator = iterator.bind(context);
    this.each(function(value, index) {
      memo = iterator(memo, value, index);
    });
    return memo;
  },

  invoke: function(method) {
    var args = $A(arguments).slice(1);
    return this.map(function(value) {
      return value[method].apply(value, args);
    });
  },

  max: function(iterator, context) {
    iterator = iterator ? iterator.bind(context) : Prototype.K;
    var result;
    this.each(function(value, index) {
      value = iterator(value, index);
      if (result == null || value >= result)
        result = value;
    });
    return result;
  },

  min: function(iterator, context) {
    iterator = iterator ? iterator.bind(context) : Prototype.K;
    var result;
    this.each(function(value, index) {
      value = iterator(value, index);
      if (result == null || value < result)
        result = value;
    });
    return result;
  },

  partition: function(iterator, context) {
    iterator = iterator ? iterator.bind(context) : Prototype.K;
    var trues = [], falses = [];
    this.each(function(value, index) {
      (iterator(value, index) ?
        trues : falses).push(value);
    });
    return [trues, falses];
  },

  pluck: function(property) {
    var results = [];
    this.each(function(value) {
      results.push(value[property]);
    });
    return results;
  },

  reject: function(iterator, context) {
    iterator = iterator.bind(context);
    var results = [];
    this.each(function(value, index) {
      if (!iterator(value, index))
        results.push(value);
    });
    return results;
  },

  sortBy: function(iterator, context) {
    iterator = iterator.bind(context);
    return this.map(function(value, index) {
      return {value: value, criteria: iterator(value, index)};
    }).sort(function(left, right) {
      var a = left.criteria, b = right.criteria;
      return a < b ? -1 : a > b ? 1 : 0;
    }).pluck('value');
  },

  toArray: function() {
    return this.map();
  },

  zip: function() {
    var iterator = Prototype.K, args = $A(arguments);
    if (Object.isFunction(args.last()))
      iterator = args.pop();

    var collections = [this].concat(args).map($A);
    return this.map(function(value, index) {
      return iterator(collections.pluck(index));
    });
  },

  size: function() {
    return this.toArray().length;
  },

  inspect: function() {
    return '#<Enumerable:' + this.toArray().inspect() + '>';
  }
};

Object.extend(Enumerable, {
  map:     Enumerable.collect,
  find:    Enumerable.detect,
  select:  Enumerable.findAll,
  filter:  Enumerable.findAll,
  member:  Enumerable.include,
  entries: Enumerable.toArray,
  every:   Enumerable.all,
  some:    Enumerable.any
});
function $A(iterable) {
  if (!iterable) return [];
  if (iterable.toArray) return iterable.toArray();
  var length = iterable.length || 0, results = new Array(length);
  while (length--) results[length] = iterable[length];
  return results;
}

if (Prototype.Browser.WebKit) {
  $A = function(iterable) {
    if (!iterable) return [];
    if (!(Object.isFunction(iterable) && iterable == '[object NodeList]') &&
        iterable.toArray) return iterable.toArray();
    var length = iterable.length || 0, results = new Array(length);
    while (length--) results[length] = iterable[length];
    return results;
  };
}

Array.from = $A;

Object.extend(Array.prototype, Enumerable);

if (!Array.prototype._reverse) Array.prototype._reverse = Array.prototype.reverse;

Object.extend(Array.prototype, {
  _each: function(iterator) {
    for (var i = 0, length = this.length; i < length; i++)
      iterator(this[i]);
  },

  clear: function() {
    this.length = 0;
    return this;
  },

  first: function() {
    return this[0];
  },

  last: function() {
    return this[this.length - 1];
  },

  compact: function() {
    return this.select(function(value) {
      return value != null;
    });
  },

  flatten: function() {
    return this.inject([], function(array, value) {
      return array.concat(Object.isArray(value) ?
        value.flatten() : [value]);
    });
  },

  without: function() {
    var values = $A(arguments);
    return this.select(function(value) {
      return !values.include(value);
    });
  },

  reverse: function(inline) {
    return (inline !== false ? this : this.toArray())._reverse();
  },

  reduce: function() {
    return this.length > 1 ? this : this[0];
  },

  uniq: function(sorted) {
    return this.inject([], function(array, value, index) {
      if (0 == index || (sorted ? array.last() != value : !array.include(value)))
        array.push(value);
      return array;
    });
  },

  intersect: function(array) {
    return this.uniq().findAll(function(item) {
      return array.detect(function(value) { return item === value });
    });
  },

  clone: function() {
    return [].concat(this);
  },

  size: function() {
    return this.length;
  },

  inspect: function() {
    return '[' + this.map(Object.inspect).join(', ') + ']';
  },

  toJSON: function() {
    var results = [];
    this.each(function(object) {
      var value = Object.toJSON(object);
      if (!Object.isUndefined(value)) results.push(value);
    });
    return '[' + results.join(', ') + ']';
  }
});

// use native browser JS 1.6 implementation if available
if (Object.isFunction(Array.prototype.forEach))
  Array.prototype._each = Array.prototype.forEach;

if (!Array.prototype.indexOf) Array.prototype.indexOf = function(item, i) {
  i || (i = 0);
  var length = this.length;
  if (i < 0) i = length + i;
  for (; i < length; i++)
    if (this[i] === item) return i;
  return -1;
};

if (!Array.prototype.lastIndexOf) Array.prototype.lastIndexOf = function(item, i) {
  i = isNaN(i) ? this.length : (i < 0 ? this.length + i : i) + 1;
  var n = this.slice(0, i).reverse().indexOf(item);
  return (n < 0) ? n : i - n - 1;
};

Array.prototype.toArray = Array.prototype.clone;

function $w(string) {
  if (!Object.isString(string)) return [];
  string = string.strip();
  return string ? string.split(/\s+/) : [];
}

if (Prototype.Browser.Opera){
  Array.prototype.concat = function() {
    var array = [];
    for (var i = 0, length = this.length; i < length; i++) array.push(this[i]);
    for (var i = 0, length = arguments.length; i < length; i++) {
      if (Object.isArray(arguments[i])) {
        for (var j = 0, arrayLength = arguments[i].length; j < arrayLength; j++)
          array.push(arguments[i][j]);
      } else {
        array.push(arguments[i]);
      }
    }
    return array;
  };
}
Object.extend(Number.prototype, {
  toColorPart: function() {
    return this.toPaddedString(2, 16);
  },

  succ: function() {
    return this + 1;
  },

  times: function(iterator) {
    $R(0, this, true).each(iterator);
    return this;
  },

  toPaddedString: function(length, radix) {
    var string = this.toString(radix || 10);
    return '0'.times(length - string.length) + string;
  },

  toJSON: function() {
    return isFinite(this) ? this.toString() : 'null';
  }
});

$w('abs round ceil floor').each(function(method){
  Number.prototype[method] = Math[method].methodize();
});
function $H(object) {
  return new Hash(object);
};

var Hash = Class.create(Enumerable, (function() {

  function toQueryPair(key, value) {
    if (Object.isUndefined(value)) return key;
    return key + '=' + encodeURIComponent(String.interpret(value));
  }

  return {
    initialize: function(object) {
      this._object = Object.isHash(object) ? object.toObject() : Object.clone(object);
    },

    _each: function(iterator) {
      for (var key in this._object) {
        var value = this._object[key], pair = [key, value];
        pair.key = key;
        pair.value = value;
        iterator(pair);
      }
    },

    set: function(key, value) {
      return this._object[key] = value;
    },

    get: function(key) {
      return this._object[key];
    },

    unset: function(key) {
      var value = this._object[key];
      delete this._object[key];
      return value;
    },

    toObject: function() {
      return Object.clone(this._object);
    },

    keys: function() {
      return this.pluck('key');
    },

    values: function() {
      return this.pluck('value');
    },

    index: function(value) {
      var match = this.detect(function(pair) {
        return pair.value === value;
      });
      return match && match.key;
    },

    merge: function(object) {
      return this.clone().update(object);
    },

    update: function(object) {
      return new Hash(object).inject(this, function(result, pair) {
        result.set(pair.key, pair.value);
        return result;
      });
    },

    toQueryString: function() {
      return this.map(function(pair) {
        var key = encodeURIComponent(pair.key), values = pair.value;

        if (values && typeof values == 'object') {
          if (Object.isArray(values))
            return values.map(toQueryPair.curry(key)).join('&');
        }
        return toQueryPair(key, values);
      }).join('&');
    },

    inspect: function() {
      return '#<Hash:{' + this.map(function(pair) {
        return pair.map(Object.inspect).join(': ');
      }).join(', ') + '}>';
    },

    toJSON: function() {
      return Object.toJSON(this.toObject());
    },

    clone: function() {
      return new Hash(this);
    }
  }
})());

Hash.prototype.toTemplateReplacements = Hash.prototype.toObject;
Hash.from = $H;
var ObjectRange = Class.create(Enumerable, {
  initialize: function(start, end, exclusive) {
    this.start = start;
    this.end = end;
    this.exclusive = exclusive;
  },

  _each: function(iterator) {
    var value = this.start;
    while (this.include(value)) {
      iterator(value);
      value = value.succ();
    }
  },

  include: function(value) {
    if (value < this.start)
      return false;
    if (this.exclusive)
      return value < this.end;
    return value <= this.end;
  }
});

var $R = function(start, end, exclusive) {
  return new ObjectRange(start, end, exclusive);
};

var Ajax = {
  getTransport: function() {
    return Try.these(
      function() {return new XMLHttpRequest()},
      function() {return new ActiveXObject('Msxml2.XMLHTTP')},
      function() {return new ActiveXObject('Microsoft.XMLHTTP')}
    ) || false;
  },

  activeRequestCount: 0
};

Ajax.Responders = {
  responders: [],

  _each: function(iterator) {
    this.responders._each(iterator);
  },

  register: function(responder) {
    if (!this.include(responder))
      this.responders.push(responder);
  },

  unregister: function(responder) {
    this.responders = this.responders.without(responder);
  },

  dispatch: function(callback, request, transport, json) {
    this.each(function(responder) {
      if (Object.isFunction(responder[callback])) {
        try {
          responder[callback].apply(responder, [request, transport, json]);
        } catch (e) { }
      }
    });
  }
};

Object.extend(Ajax.Responders, Enumerable);

Ajax.Responders.register({
  onCreate:   function() { Ajax.activeRequestCount++ },
  onComplete: function() { Ajax.activeRequestCount-- }
});

Ajax.Base = Class.create({
  initialize: function(options) {
    this.options = {
      method:       'post',
      asynchronous: true,
      contentType:  'application/x-www-form-urlencoded',
      encoding:     'UTF-8',
      parameters:   '',
      evalJSON:     true,
      evalJS:       true
    };
    Object.extend(this.options, options || { });

    this.options.method = this.options.method.toLowerCase();

    if (Object.isString(this.options.parameters))
      this.options.parameters = this.options.parameters.toQueryParams();
    else if (Object.isHash(this.options.parameters))
      this.options.parameters = this.options.parameters.toObject();
  }
});

Ajax.Request = Class.create(Ajax.Base, {
  _complete: false,

  initialize: function($super, url, options) {
    $super(options);
    this.transport = Ajax.getTransport();
    this.request(url);
  },

  request: function(url) {
    this.url = url;
    this.method = this.options.method;
    var params = Object.clone(this.options.parameters);

    if (!['get', 'post'].include(this.method)) {
      // simulate other verbs over post
      params['_method'] = this.method;
      this.method = 'post';
    }

    this.parameters = params;

    if (params = Object.toQueryString(params)) {
      // when GET, append parameters to URL
      if (this.method == 'get')
        this.url += (this.url.include('?') ? '&' : '?') + params;
      else if (/Konqueror|Safari|KHTML/.test(navigator.userAgent))
        params += '&_=';
    }

    try {
      var response = new Ajax.Response(this);
      if (this.options.onCreate) this.options.onCreate(response);
      Ajax.Responders.dispatch('onCreate', this, response);

      this.transport.open(this.method.toUpperCase(), this.url,
        this.options.asynchronous);

      if (this.options.asynchronous) this.respondToReadyState.bind(this).defer(1);

      this.transport.onreadystatechange = this.onStateChange.bind(this);
      this.setRequestHeaders();

      this.body = this.method == 'post' ? (this.options.postBody || params) : null;
      this.transport.send(this.body);

      /* Force Firefox to handle ready state 4 for synchronous requests */
      if (!this.options.asynchronous && this.transport.overrideMimeType)
        this.onStateChange();

    }
    catch (e) {
      this.dispatchException(e);
    }
  },

  onStateChange: function() {
    var readyState = this.transport.readyState;
    if (readyState > 1 && !((readyState == 4) && this._complete))
      this.respondToReadyState(this.transport.readyState);
  },

  setRequestHeaders: function() {
    var headers = {
      'X-Requested-With': 'XMLHttpRequest',
      'X-Prototype-Version': Prototype.Version,
      'Accept': 'text/javascript, text/html, application/xml, text/xml, */*'
    };

    if (this.method == 'post') {
      headers['Content-type'] = this.options.contentType +
        (this.options.encoding ? '; charset=' + this.options.encoding : '');

      /* Force "Connection: close" for older Mozilla browsers to work
       * around a bug where XMLHttpRequest sends an incorrect
       * Content-length header. See Mozilla Bugzilla #246651.
       */
      if (this.transport.overrideMimeType &&
          (navigator.userAgent.match(/Gecko\/(\d{4})/) || [0,2005])[1] < 2005)
            headers['Connection'] = 'close';
    }

    // user-defined headers
    if (typeof this.options.requestHeaders == 'object') {
      var extras = this.options.requestHeaders;

      if (Object.isFunction(extras.push))
        for (var i = 0, length = extras.length; i < length; i += 2)
          headers[extras[i]] = extras[i+1];
      else
        $H(extras).each(function(pair) { headers[pair.key] = pair.value });
    }

    for (var name in headers)
      this.transport.setRequestHeader(name, headers[name]);
  },

  success: function() {
    var status = this.getStatus();
    return !status || (status >= 200 && status < 300);
  },

  getStatus: function() {
    try {
      return this.transport.status || 0;
    } catch (e) { return 0 }
  },

  respondToReadyState: function(readyState) {
    var state = Ajax.Request.Events[readyState], response = new Ajax.Response(this);

    if (state == 'Complete') {
      try {
        this._complete = true;
        (this.options['on' + response.status]
         || this.options['on' + (this.success() ? 'Success' : 'Failure')]
         || Prototype.emptyFunction)(response, response.headerJSON);
      } catch (e) {
        this.dispatchException(e);
      }

      var contentType = response.getHeader('Content-type');
      if (this.options.evalJS == 'force'
          || (this.options.evalJS && this.isSameOrigin() && contentType
          && contentType.match(/^\s*(text|application)\/(x-)?(java|ecma)script(;.*)?\s*$/i)))
        this.evalResponse();
    }

    try {
      (this.options['on' + state] || Prototype.emptyFunction)(response, response.headerJSON);
      Ajax.Responders.dispatch('on' + state, this, response, response.headerJSON);
    } catch (e) {
      this.dispatchException(e);
    }

    if (state == 'Complete') {
      // avoid memory leak in MSIE: clean up
      this.transport.onreadystatechange = Prototype.emptyFunction;
    }
  },

  isSameOrigin: function() {
    var m = this.url.match(/^\s*https?:\/\/[^\/]*/);
    return !m || (m[0] == '#{protocol}//#{domain}#{port}'.interpolate({
      protocol: location.protocol,
      domain: document.domain,
      port: location.port ? ':' + location.port : ''
    }));
  },

  getHeader: function(name) {
    try {
      return this.transport.getResponseHeader(name) || null;
    } catch (e) { return null }
  },

  evalResponse: function() {
    try {
      return eval((this.transport.responseText || '').unfilterJSON());
    } catch (e) {
      this.dispatchException(e);
    }
  },

  dispatchException: function(exception) {
    (this.options.onException || Prototype.emptyFunction)(this, exception);
    Ajax.Responders.dispatch('onException', this, exception);
  }
});

Ajax.Request.Events =
  ['Uninitialized', 'Loading', 'Loaded', 'Interactive', 'Complete'];

Ajax.Response = Class.create({
  initialize: function(request){
    this.request = request;
    var transport  = this.transport  = request.transport,
        readyState = this.readyState = transport.readyState;

    if((readyState > 2 && !Prototype.Browser.IE) || readyState == 4) {
      this.status       = this.getStatus();
      this.statusText   = this.getStatusText();
      this.responseText = String.interpret(transport.responseText);
      this.headerJSON   = this._getHeaderJSON();
    }

    if(readyState == 4) {
      var xml = transport.responseXML;
      this.responseXML  = Object.isUndefined(xml) ? null : xml;
      this.responseJSON = this._getResponseJSON();
    }
  },

  status:      0,
  statusText: '',

  getStatus: Ajax.Request.prototype.getStatus,

  getStatusText: function() {
    try {
      return this.transport.statusText || '';
    } catch (e) { return '' }
  },

  getHeader: Ajax.Request.prototype.getHeader,

  getAllHeaders: function() {
    try {
      return this.getAllResponseHeaders();
    } catch (e) { return null }
  },

  getResponseHeader: function(name) {
    return this.transport.getResponseHeader(name);
  },

  getAllResponseHeaders: function() {
    return this.transport.getAllResponseHeaders();
  },

  _getHeaderJSON: function() {
    var json = this.getHeader('X-JSON');
    if (!json) return null;
    json = decodeURIComponent(escape(json));
    try {
      return json.evalJSON(this.request.options.sanitizeJSON ||
        !this.request.isSameOrigin());
    } catch (e) {
      this.request.dispatchException(e);
    }
  },

  _getResponseJSON: function() {
    var options = this.request.options;
    if (!options.evalJSON || (options.evalJSON != 'force' &&
      !(this.getHeader('Content-type') || '').include('application/json')) ||
        this.responseText.blank())
          return null;
    try {
      return this.responseText.evalJSON(options.sanitizeJSON ||
        !this.request.isSameOrigin());
    } catch (e) {
      this.request.dispatchException(e);
    }
  }
});

Ajax.Updater = Class.create(Ajax.Request, {
  initialize: function($super, container, url, options) {
    this.container = {
      success: (container.success || container),
      failure: (container.failure || (container.success ? null : container))
    };

    options = Object.clone(options);
    var onComplete = options.onComplete;
    options.onComplete = (function(response, json) {
      this.updateContent(response.responseText);
      if (Object.isFunction(onComplete)) onComplete(response, json);
    }).bind(this);

    $super(url, options);
  },

  updateContent: function(responseText) {
    var receiver = this.container[this.success() ? 'success' : 'failure'],
        options = this.options;

    if (!options.evalScripts) responseText = responseText.stripScripts();

    if (receiver = $(receiver)) {
      if (options.insertion) {
        if (Object.isString(options.insertion)) {
          var insertion = { }; insertion[options.insertion] = responseText;
          receiver.insert(insertion);
        }
        else options.insertion(receiver, responseText);
      }
      else receiver.update(responseText);
    }
  }
});

Ajax.PeriodicalUpdater = Class.create(Ajax.Base, {
  initialize: function($super, container, url, options) {
    $super(options);
    this.onComplete = this.options.onComplete;

    this.frequency = (this.options.frequency || 2);
    this.decay = (this.options.decay || 1);

    this.updater = { };
    this.container = container;
    this.url = url;

    this.start();
  },

  start: function() {
    this.options.onComplete = this.updateComplete.bind(this);
    this.onTimerEvent();
  },

  stop: function() {
    this.updater.options.onComplete = undefined;
    clearTimeout(this.timer);
    (this.onComplete || Prototype.emptyFunction).apply(this, arguments);
  },

  updateComplete: function(response) {
    if (this.options.decay) {
      this.decay = (response.responseText == this.lastText ?
        this.decay * this.options.decay : 1);

      this.lastText = response.responseText;
    }
    this.timer = this.onTimerEvent.bind(this).delay(this.decay * this.frequency);
  },

  onTimerEvent: function() {
    this.updater = new Ajax.Updater(this.container, this.url, this.options);
  }
});
function $(element) {
  if (arguments.length > 1) {
    for (var i = 0, elements = [], length = arguments.length; i < length; i++)
      elements.push($(arguments[i]));
    return elements;
  }
  if (Object.isString(element))
    element = document.getElementById(element);
  return Element.extend(element);
}

if (Prototype.BrowserFeatures.XPath) {
  document._getElementsByXPath = function(expression, parentElement) {
    var results = [];
    var query = document.evaluate(expression, $(parentElement) || document,
      null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
    for (var i = 0, length = query.snapshotLength; i < length; i++)
      results.push(Element.extend(query.snapshotItem(i)));
    return results;
  };
}

/*--------------------------------------------------------------------------*/

if (!window.Node) var Node = { };

if (!Node.ELEMENT_NODE) {
  // DOM level 2 ECMAScript Language Binding
  Object.extend(Node, {
    ELEMENT_NODE: 1,
    ATTRIBUTE_NODE: 2,
    TEXT_NODE: 3,
    CDATA_SECTION_NODE: 4,
    ENTITY_REFERENCE_NODE: 5,
    ENTITY_NODE: 6,
    PROCESSING_INSTRUCTION_NODE: 7,
    COMMENT_NODE: 8,
    DOCUMENT_NODE: 9,
    DOCUMENT_TYPE_NODE: 10,
    DOCUMENT_FRAGMENT_NODE: 11,
    NOTATION_NODE: 12
  });
}

(function() {
  var element = this.Element;
  this.Element = function(tagName, attributes) {
    attributes = attributes || { };
    tagName = tagName.toLowerCase();
    var cache = Element.cache;
    if (Prototype.Browser.IE && attributes.name) {
      tagName = '<' + tagName + ' name="' + attributes.name + '">';
      delete attributes.name;
      return Element.writeAttribute(document.createElement(tagName), attributes);
    }
    if (!cache[tagName]) cache[tagName] = Element.extend(document.createElement(tagName));
    return Element.writeAttribute(cache[tagName].cloneNode(false), attributes);
  };
  Object.extend(this.Element, element || { });
}).call(window);

Element.cache = { };

Element.Methods = {
  visible: function(element) {
    return $(element).style.display != 'none';
  },

  toggle: function(element) {
    element = $(element);
    Element[Element.visible(element) ? 'hide' : 'show'](element);
    return element;
  },

  hide: function(element) {
    $(element).style.display = 'none';
    return element;
  },

  show: function(element) {
    $(element).style.display = '';
    return element;
  },

  remove: function(element) {
    element = $(element);
    element.parentNode.removeChild(element);
    return element;
  },

  update: function(element, content) {
    element = $(element);
    if (content && content.toElement) content = content.toElement();
    if (Object.isElement(content)) return element.update().insert(content);
    content = Object.toHTML(content);
    element.innerHTML = content.stripScripts();
    content.evalScripts.bind(content).defer();
    return element;
  },

  replace: function(element, content) {
    element = $(element);
    if (content && content.toElement) content = content.toElement();
    else if (!Object.isElement(content)) {
      content = Object.toHTML(content);
      var range = element.ownerDocument.createRange();
      range.selectNode(element);
      content.evalScripts.bind(content).defer();
      content = range.createContextualFragment(content.stripScripts());
    }
    element.parentNode.replaceChild(content, element);
    return element;
  },

  insert: function(element, insertions) {
    element = $(element);

    if (Object.isString(insertions) || Object.isNumber(insertions) ||
        Object.isElement(insertions) || (insertions && (insertions.toElement || insertions.toHTML)))
          insertions = {bottom:insertions};

    var content, insert, tagName, childNodes;

    for (var position in insertions) {
      content  = insertions[position];
      position = position.toLowerCase();
      insert = Element._insertionTranslations[position];

      if (content && content.toElement) content = content.toElement();
      if (Object.isElement(content)) {
        insert(element, content);
        continue;
      }

      content = Object.toHTML(content);

      tagName = ((position == 'before' || position == 'after')
        ? element.parentNode : element).tagName.toUpperCase();

      childNodes = Element._getContentFromAnonymousElement(tagName, content.stripScripts());

      if (position == 'top' || position == 'after') childNodes.reverse();
      childNodes.each(insert.curry(element));

      content.evalScripts.bind(content).defer();
    }

    return element;
  },

  wrap: function(element, wrapper, attributes) {
    element = $(element);
    if (Object.isElement(wrapper))
      $(wrapper).writeAttribute(attributes || { });
    else if (Object.isString(wrapper)) wrapper = new Element(wrapper, attributes);
    else wrapper = new Element('div', wrapper);
    if (element.parentNode)
      element.parentNode.replaceChild(wrapper, element);
    wrapper.appendChild(element);
    return wrapper;
  },

  inspect: function(element) {
    element = $(element);
    var result = '<' + element.tagName.toLowerCase();
    $H({'id': 'id', 'className': 'class'}).each(function(pair) {
      var property = pair.first(), attribute = pair.last();
      var value = (element[property] || '').toString();
      if (value) result += ' ' + attribute + '=' + value.inspect(true);
    });
    return result + '>';
  },

  recursivelyCollect: function(element, property) {
    element = $(element);
    var elements = [];
    while (element = element[property])
      if (element.nodeType == 1)
        elements.push(Element.extend(element));
    return elements;
  },

  ancestors: function(element) {
    return $(element).recursivelyCollect('parentNode');
  },

  descendants: function(element) {
    return $(element).select("*");
  },

  firstDescendant: function(element) {
    element = $(element).firstChild;
    while (element && element.nodeType != 1) element = element.nextSibling;
    return $(element);
  },

  immediateDescendants: function(element) {
    if (!(element = $(element).firstChild)) return [];
    while (element && element.nodeType != 1) element = element.nextSibling;
    if (element) return [element].concat($(element).nextSiblings());
    return [];
  },

  previousSiblings: function(element) {
    return $(element).recursivelyCollect('previousSibling');
  },

  nextSiblings: function(element) {
    return $(element).recursivelyCollect('nextSibling');
  },

  siblings: function(element) {
    element = $(element);
    return element.previousSiblings().reverse().concat(element.nextSiblings());
  },

  match: function(element, selector) {
    if (Object.isString(selector))
      selector = new Selector(selector);
    return selector.match($(element));
  },

  up: function(element, expression, index) {
    element = $(element);
    if (arguments.length == 1) return $(element.parentNode);
    var ancestors = element.ancestors();
    return Object.isNumber(expression) ? ancestors[expression] :
      Selector.findElement(ancestors, expression, index);
  },

  down: function(element, expression, index) {
    element = $(element);
    if (arguments.length == 1) return element.firstDescendant();
    return Object.isNumber(expression) ? element.descendants()[expression] :
      element.select(expression)[index || 0];
  },

  previous: function(element, expression, index) {
    element = $(element);
    if (arguments.length == 1) return $(Selector.handlers.previousElementSibling(element));
    var previousSiblings = element.previousSiblings();
    return Object.isNumber(expression) ? previousSiblings[expression] :
      Selector.findElement(previousSiblings, expression, index);
  },

  next: function(element, expression, index) {
    element = $(element);
    if (arguments.length == 1) return $(Selector.handlers.nextElementSibling(element));
    var nextSiblings = element.nextSiblings();
    return Object.isNumber(expression) ? nextSiblings[expression] :
      Selector.findElement(nextSiblings, expression, index);
  },

  select: function() {
    var args = $A(arguments), element = $(args.shift());
    return Selector.findChildElements(element, args);
  },

  adjacent: function() {
    var args = $A(arguments), element = $(args.shift());
    return Selector.findChildElements(element.parentNode, args).without(element);
  },

  identify: function(element) {
    element = $(element);
    var id = element.readAttribute('id'), self = arguments.callee;
    if (id) return id;
    do { id = 'anonymous_element_' + self.counter++ } while ($(id));
    element.writeAttribute('id', id);
    return id;
  },

  readAttribute: function(element, name) {
    element = $(element);
    if (Prototype.Browser.IE) {
      var t = Element._attributeTranslations.read;
      if (t.values[name]) return t.values[name](element, name);
      if (t.names[name]) name = t.names[name];
      if (name.include(':')) {
        return (!element.attributes || !element.attributes[name]) ? null :
         element.attributes[name].value;
      }
    }
    return element.getAttribute(name);
  },

  writeAttribute: function(element, name, value) {
    element = $(element);
    var attributes = { }, t = Element._attributeTranslations.write;

    if (typeof name == 'object') attributes = name;
    else attributes[name] = Object.isUndefined(value) ? true : value;

    for (var attr in attributes) {
      name = t.names[attr] || attr;
      value = attributes[attr];
      if (t.values[attr]) name = t.values[attr](element, value);
      if (value === false || value === null)
        element.removeAttribute(name);
      else if (value === true)
        element.setAttribute(name, name);
      else element.setAttribute(name, value);
    }
    return element;
  },

  getHeight: function(element) {
    return $(element).getDimensions().height;
  },

  getWidth: function(element) {
    return $(element).getDimensions().width;
  },

  classNames: function(element) {
    return new Element.ClassNames(element);
  },

  hasClassName: function(element, className) {
    if (!(element = $(element))) return;
    var elementClassName = element.className;
    return (elementClassName.length > 0 && (elementClassName == className ||
      new RegExp("(^|\\s)" + className + "(\\s|$)").test(elementClassName)));
  },

  addClassName: function(element, className) {
    if (!(element = $(element))) return;
    if (!element.hasClassName(className))
      element.className += (element.className ? ' ' : '') + className;
    return element;
  },

  removeClassName: function(element, className) {
    if (!(element = $(element))) return;
    element.className = element.className.replace(
      new RegExp("(^|\\s+)" + className + "(\\s+|$)"), ' ').strip();
    return element;
  },

  toggleClassName: function(element, className) {
    if (!(element = $(element))) return;
    return element[element.hasClassName(className) ?
      'removeClassName' : 'addClassName'](className);
  },

  // removes whitespace-only text node children
  cleanWhitespace: function(element) {
    element = $(element);
    var node = element.firstChild;
    while (node) {
      var nextNode = node.nextSibling;
      if (node.nodeType == 3 && !/\S/.test(node.nodeValue))
        element.removeChild(node);
      node = nextNode;
    }
    return element;
  },

  empty: function(element) {
    return $(element).innerHTML.blank();
  },

  descendantOf: function(element, ancestor) {
    element = $(element), ancestor = $(ancestor);
    var originalAncestor = ancestor;

    if (element.compareDocumentPosition)
      return (element.compareDocumentPosition(ancestor) & 8) === 8;

    if (element.sourceIndex && !Prototype.Browser.Opera) {
      var e = element.sourceIndex, a = ancestor.sourceIndex,
       nextAncestor = ancestor.nextSibling;
      if (!nextAncestor) {
        do { ancestor = ancestor.parentNode; }
        while (!(nextAncestor = ancestor.nextSibling) && ancestor.parentNode);
      }
      if (nextAncestor && nextAncestor.sourceIndex)
       return (e > a && e < nextAncestor.sourceIndex);
    }

    while (element = element.parentNode)
      if (element == originalAncestor) return true;
    return false;
  },

  scrollTo: function(element) {
    element = $(element);
    var pos = element.cumulativeOffset();
    window.scrollTo(pos[0], pos[1]);
    return element;
  },

  getStyle: function(element, style) {
    element = $(element);
    style = style == 'float' ? 'cssFloat' : style.camelize();
    var value = element.style[style];
    if (!value) {
      var css = document.defaultView.getComputedStyle(element, null);
      value = css ? css[style] : null;
    }
    if (style == 'opacity') return value ? parseFloat(value) : 1.0;
    return value == 'auto' ? null : value;
  },

  getOpacity: function(element) {
    return $(element).getStyle('opacity');
  },

  setStyle: function(element, styles) {
    element = $(element);
    var elementStyle = element.style, match;
    if (Object.isString(styles)) {
      element.style.cssText += ';' + styles;
      return styles.include('opacity') ?
        element.setOpacity(styles.match(/opacity:\s*(\d?\.?\d*)/)[1]) : element;
    }
    for (var property in styles)
      if (property == 'opacity') element.setOpacity(styles[property]);
      else
        elementStyle[(property == 'float' || property == 'cssFloat') ?
          (Object.isUndefined(elementStyle.styleFloat) ? 'cssFloat' : 'styleFloat') :
            property] = styles[property];

    return element;
  },

  setOpacity: function(element, value) {
    element = $(element);
    element.style.opacity = (value == 1 || value === '') ? '' :
      (value < 0.00001) ? 0 : value;
    return element;
  },

  getDimensions: function(element) {
    element = $(element);
    var display = $(element).getStyle('display');
    if (display != 'none' && display != null) // Safari bug
      return {width: element.offsetWidth, height: element.offsetHeight};

    // All *Width and *Height properties give 0 on elements with display none,
    // so enable the element temporarily
    var els = element.style;
    var originalVisibility = els.visibility;
    var originalPosition = els.position;
    var originalDisplay = els.display;
    els.visibility = 'hidden';
    els.position = 'absolute';
    els.display = 'block';
    var originalWidth = element.clientWidth;
    var originalHeight = element.clientHeight;
    els.display = originalDisplay;
    els.position = originalPosition;
    els.visibility = originalVisibility;
    return {width: originalWidth, height: originalHeight};
  },

  makePositioned: function(element) {
    element = $(element);
    var pos = Element.getStyle(element, 'position');
    if (pos == 'static' || !pos) {
      element._madePositioned = true;
      element.style.position = 'relative';
      // Opera returns the offset relative to the positioning context, when an
      // element is position relative but top and left have not been defined
      if (window.opera) {
        element.style.top = 0;
        element.style.left = 0;
      }
    }
    return element;
  },

  undoPositioned: function(element) {
    element = $(element);
    if (element._madePositioned) {
      element._madePositioned = undefined;
      element.style.position =
        element.style.top =
        element.style.left =
        element.style.bottom =
        element.style.right = '';
    }
    return element;
  },

  makeClipping: function(element) {
    element = $(element);
    if (element._overflow) return element;
    element._overflow = Element.getStyle(element, 'overflow') || 'auto';
    if (element._overflow !== 'hidden')
      element.style.overflow = 'hidden';
    return element;
  },

  undoClipping: function(element) {
    element = $(element);
    if (!element._overflow) return element;
    element.style.overflow = element._overflow == 'auto' ? '' : element._overflow;
    element._overflow = null;
    return element;
  },

  cumulativeOffset: function(element) {
    var valueT = 0, valueL = 0;
    do {
      valueT += element.offsetTop  || 0;
      valueL += element.offsetLeft || 0;
      element = element.offsetParent;
    } while (element);
    return Element._returnOffset(valueL, valueT);
  },

  positionedOffset: function(element) {
    var valueT = 0, valueL = 0;
    do {
      valueT += element.offsetTop  || 0;
      valueL += element.offsetLeft || 0;
      element = element.offsetParent;
      if (element) {
        if (element.tagName == 'BODY') break;
        var p = Element.getStyle(element, 'position');
        if (p !== 'static') break;
      }
    } while (element);
    return Element._returnOffset(valueL, valueT);
  },

  absolutize: function(element) {
    element = $(element);
    if (element.getStyle('position') == 'absolute') return;
    // Position.prepare(); // To be done manually by Scripty when it needs it.

    var offsets = element.positionedOffset();
    var top     = offsets[1];
    var left    = offsets[0];
    var width   = element.clientWidth;
    var height  = element.clientHeight;

    element._originalLeft   = left - parseFloat(element.style.left  || 0);
    element._originalTop    = top  - parseFloat(element.style.top || 0);
    element._originalWidth  = element.style.width;
    element._originalHeight = element.style.height;

    element.style.position = 'absolute';
    element.style.top    = top + 'px';
    element.style.left   = left + 'px';
    element.style.width  = width + 'px';
    element.style.height = height + 'px';
    return element;
  },

  relativize: function(element) {
    element = $(element);
    if (element.getStyle('position') == 'relative') return;
    // Position.prepare(); // To be done manually by Scripty when it needs it.

    element.style.position = 'relative';
    var top  = parseFloat(element.style.top  || 0) - (element._originalTop || 0);
    var left = parseFloat(element.style.left || 0) - (element._originalLeft || 0);

    element.style.top    = top + 'px';
    element.style.left   = left + 'px';
    element.style.height = element._originalHeight;
    element.style.width  = element._originalWidth;
    return element;
  },

  cumulativeScrollOffset: function(element) {
    var valueT = 0, valueL = 0;
    do {
      valueT += element.scrollTop  || 0;
      valueL += element.scrollLeft || 0;
      element = element.parentNode;
    } while (element);
    return Element._returnOffset(valueL, valueT);
  },

  getOffsetParent: function(element) {
    if (element.offsetParent) return $(element.offsetParent);
    if (element == document.body) return $(element);

    while ((element = element.parentNode) && element != document.body)
      if (Element.getStyle(element, 'position') != 'static')
        return $(element);

    return $(document.body);
  },

  viewportOffset: function(forElement) {
    var valueT = 0, valueL = 0;

    var element = forElement;
    do {
      valueT += element.offsetTop  || 0;
      valueL += element.offsetLeft || 0;

      // Safari fix
      if (element.offsetParent == document.body &&
        Element.getStyle(element, 'position') == 'absolute') break;

    } while (element = element.offsetParent);

    element = forElement;
    do {
      if (!Prototype.Browser.Opera || element.tagName == 'BODY') {
        valueT -= element.scrollTop  || 0;
        valueL -= element.scrollLeft || 0;
      }
    } while (element = element.parentNode);

    return Element._returnOffset(valueL, valueT);
  },

  clonePosition: function(element, source) {
    var options = Object.extend({
      setLeft:    true,
      setTop:     true,
      setWidth:   true,
      setHeight:  true,
      offsetTop:  0,
      offsetLeft: 0
    }, arguments[2] || { });

    // find page position of source
    source = $(source);
    var p = source.viewportOffset();

    // find coordinate system to use
    element = $(element);
    var delta = [0, 0];
    var parent = null;
    // delta [0,0] will do fine with position: fixed elements,
    // position:absolute needs offsetParent deltas
    if (Element.getStyle(element, 'position') == 'absolute') {
      parent = element.getOffsetParent();
      delta = parent.viewportOffset();
    }

    // correct by body offsets (fixes Safari)
    if (parent == document.body) {
      delta[0] -= document.body.offsetLeft;
      delta[1] -= document.body.offsetTop;
    }

    // set position
    if (options.setLeft)   element.style.left  = (p[0] - delta[0] + options.offsetLeft) + 'px';
    if (options.setTop)    element.style.top   = (p[1] - delta[1] + options.offsetTop) + 'px';
    if (options.setWidth)  element.style.width = source.offsetWidth + 'px';
    if (options.setHeight) element.style.height = source.offsetHeight + 'px';
    return element;
  }
};

Element.Methods.identify.counter = 1;

Object.extend(Element.Methods, {
  getElementsBySelector: Element.Methods.select,
  childElements: Element.Methods.immediateDescendants
});

Element._attributeTranslations = {
  write: {
    names: {
      className: 'class',
      htmlFor:   'for'
    },
    values: { }
  }
};

if (Prototype.Browser.Opera) {
  Element.Methods.getStyle = Element.Methods.getStyle.wrap(
    function(proceed, element, style) {
      switch (style) {
        case 'left': case 'top': case 'right': case 'bottom':
          if (proceed(element, 'position') === 'static') return null;
        case 'height': case 'width':
          // returns '0px' for hidden elements; we want it to return null
          if (!Element.visible(element)) return null;

          // returns the border-box dimensions rather than the content-box
          // dimensions, so we subtract padding and borders from the value
          var dim = parseInt(proceed(element, style), 10);

          if (dim !== element['offset' + style.capitalize()])
            return dim + 'px';

          var properties;
          if (style === 'height') {
            properties = ['border-top-width', 'padding-top',
             'padding-bottom', 'border-bottom-width'];
          }
          else {
            properties = ['border-left-width', 'padding-left',
             'padding-right', 'border-right-width'];
          }
          return properties.inject(dim, function(memo, property) {
            var val = proceed(element, property);
            return val === null ? memo : memo - parseInt(val, 10);
          }) + 'px';
        default: return proceed(element, style);
      }
    }
  );

  Element.Methods.readAttribute = Element.Methods.readAttribute.wrap(
    function(proceed, element, attribute) {
      if (attribute === 'title') return element.title;
      return proceed(element, attribute);
    }
  );
}

else if (Prototype.Browser.IE) {
  // IE doesn't report offsets correctly for static elements, so we change them
  // to "relative" to get the values, then change them back.
  Element.Methods.getOffsetParent = Element.Methods.getOffsetParent.wrap(
    function(proceed, element) {
      element = $(element);
      var position = element.getStyle('position');
      if (position !== 'static') return proceed(element);
      element.setStyle({ position: 'relative' });
      var value = proceed(element);
      element.setStyle({ position: position });
      return value;
    }
  );

  $w('positionedOffset viewportOffset').each(function(method) {
    Element.Methods[method] = Element.Methods[method].wrap(
      function(proceed, element) {
        element = $(element);
        var position = element.getStyle('position');
        if (position !== 'static') return proceed(element);
        // Trigger hasLayout on the offset parent so that IE6 reports
        // accurate offsetTop and offsetLeft values for position: fixed.
        var offsetParent = element.getOffsetParent();
        if (offsetParent && offsetParent.getStyle('position') === 'fixed')
          offsetParent.setStyle({ zoom: 1 });
        element.setStyle({ position: 'relative' });
        var value = proceed(element);
        element.setStyle({ position: position });
        return value;
      }
    );
  });

  Element.Methods.getStyle = function(element, style) {
    element = $(element);
    style = (style == 'float' || style == 'cssFloat') ? 'styleFloat' : style.camelize();
    var value = element.style[style];
    if (!value && element.currentStyle) value = element.currentStyle[style];

    if (style == 'opacity') {
      if (value = (element.getStyle('filter') || '').match(/alpha\(opacity=(.*)\)/))
        if (value[1]) return parseFloat(value[1]) / 100;
      return 1.0;
    }

    if (value == 'auto') {
      if ((style == 'width' || style == 'height') && (element.getStyle('display') != 'none'))
        return element['offset' + style.capitalize()] + 'px';
      return null;
    }
    return value;
  };

  Element.Methods.setOpacity = function(element, value) {
    function stripAlpha(filter){
      return filter.replace(/alpha\([^\)]*\)/gi,'');
    }
    element = $(element);
    var currentStyle = element.currentStyle;
    if ((currentStyle && !currentStyle.hasLayout) ||
      (!currentStyle && element.style.zoom == 'normal'))
        element.style.zoom = 1;

    var filter = element.getStyle('filter'), style = element.style;
    if (value == 1 || value === '') {
      (filter = stripAlpha(filter)) ?
        style.filter = filter : style.removeAttribute('filter');
      return element;
    } else if (value < 0.00001) value = 0;
    style.filter = stripAlpha(filter) +
      'alpha(opacity=' + (value * 100) + ')';
    return element;
  };

  Element._attributeTranslations = {
    read: {
      names: {
        'class': 'className',
        'for':   'htmlFor'
      },
      values: {
        _getAttr: function(element, attribute) {
          return element.getAttribute(attribute, 2);
        },
        _getAttrNode: function(element, attribute) {
          var node = element.getAttributeNode(attribute);
          return node ? node.value : "";
        },
        _getEv: function(element, attribute) {
          attribute = element.getAttribute(attribute);
          return attribute ? attribute.toString().slice(23, -2) : null;
        },
        _flag: function(element, attribute) {
          return $(element).hasAttribute(attribute) ? attribute : null;
        },
        style: function(element) {
          return element.style.cssText.toLowerCase();
        },
        title: function(element) {
          return element.title;
        }
      }
    }
  };

  Element._attributeTranslations.write = {
    names: Object.extend({
      cellpadding: 'cellPadding',
      cellspacing: 'cellSpacing'
    }, Element._attributeTranslations.read.names),
    values: {
      checked: function(element, value) {
        element.checked = !!value;
      },

      style: function(element, value) {
        element.style.cssText = value ? value : '';
      }
    }
  };

  Element._attributeTranslations.has = {};

  $w('colSpan rowSpan vAlign dateTime accessKey tabIndex ' +
      'encType maxLength readOnly longDesc').each(function(attr) {
    Element._attributeTranslations.write.names[attr.toLowerCase()] = attr;
    Element._attributeTranslations.has[attr.toLowerCase()] = attr;
  });

  (function(v) {
    Object.extend(v, {
      href:        v._getAttr,
      src:         v._getAttr,
      type:        v._getAttr,
      action:      v._getAttrNode,
      disabled:    v._flag,
      checked:     v._flag,
      readonly:    v._flag,
      multiple:    v._flag,
      onload:      v._getEv,
      onunload:    v._getEv,
      onclick:     v._getEv,
      ondblclick:  v._getEv,
      onmousedown: v._getEv,
      onmouseup:   v._getEv,
      onmouseover: v._getEv,
      onmousemove: v._getEv,
      onmouseout:  v._getEv,
      onfocus:     v._getEv,
      onblur:      v._getEv,
      onkeypress:  v._getEv,
      onkeydown:   v._getEv,
      onkeyup:     v._getEv,
      onsubmit:    v._getEv,
      onreset:     v._getEv,
      onselect:    v._getEv,
      onchange:    v._getEv
    });
  })(Element._attributeTranslations.read.values);
}

else if (Prototype.Browser.Gecko && /rv:1\.8\.0/.test(navigator.userAgent)) {
  Element.Methods.setOpacity = function(element, value) {
    element = $(element);
    element.style.opacity = (value == 1) ? 0.999999 :
      (value === '') ? '' : (value < 0.00001) ? 0 : value;
    return element;
  };
}

else if (Prototype.Browser.WebKit) {
  Element.Methods.setOpacity = function(element, value) {
    element = $(element);
    element.style.opacity = (value == 1 || value === '') ? '' :
      (value < 0.00001) ? 0 : value;

    if (value == 1)
      if(element.tagName == 'IMG' && element.width) {
        element.width++; element.width--;
      } else try {
        var n = document.createTextNode(' ');
        element.appendChild(n);
        element.removeChild(n);
      } catch (e) { }

    return element;
  };

  // Safari returns margins on body which is incorrect if the child is absolutely
  // positioned.  For performance reasons, redefine Element#cumulativeOffset for
  // KHTML/WebKit only.
  Element.Methods.cumulativeOffset = function(element) {
    var valueT = 0, valueL = 0;
    do {
      valueT += element.offsetTop  || 0;
      valueL += element.offsetLeft || 0;
      if (element.offsetParent == document.body)
        if (Element.getStyle(element, 'position') == 'absolute') break;

      element = element.offsetParent;
    } while (element);

    return Element._returnOffset(valueL, valueT);
  };
}

if (Prototype.Browser.IE || Prototype.Browser.Opera) {
  // IE and Opera are missing .innerHTML support for TABLE-related and SELECT elements
  Element.Methods.update = function(element, content) {
    element = $(element);

    if (content && content.toElement) content = content.toElement();
    if (Object.isElement(content)) return element.update().insert(content);

    content = Object.toHTML(content);
    var tagName = element.tagName.toUpperCase();

    if (tagName in Element._insertionTranslations.tags) {
      $A(element.childNodes).each(function(node) { element.removeChild(node) });
      Element._getContentFromAnonymousElement(tagName, content.stripScripts())
        .each(function(node) { element.appendChild(node) });
    }
    else element.innerHTML = content.stripScripts();

    content.evalScripts.bind(content).defer();
    return element;
  };
}

if ('outerHTML' in document.createElement('div')) {
  Element.Methods.replace = function(element, content) {
    element = $(element);

    if (content && content.toElement) content = content.toElement();
    if (Object.isElement(content)) {
      element.parentNode.replaceChild(content, element);
      return element;
    }

    content = Object.toHTML(content);
    var parent = element.parentNode, tagName = parent.tagName.toUpperCase();

    if (Element._insertionTranslations.tags[tagName]) {
      var nextSibling = element.next();
      var fragments = Element._getContentFromAnonymousElement(tagName, content.stripScripts());
      parent.removeChild(element);
      if (nextSibling)
        fragments.each(function(node) { parent.insertBefore(node, nextSibling) });
      else
        fragments.each(function(node) { parent.appendChild(node) });
    }
    else element.outerHTML = content.stripScripts();

    content.evalScripts.bind(content).defer();
    return element;
  };
}

Element._returnOffset = function(l, t) {
  var result = [l, t];
  result.left = l;
  result.top = t;
  return result;
};

Element._getContentFromAnonymousElement = function(tagName, html) {
  var div = new Element('div'), t = Element._insertionTranslations.tags[tagName];
  if (t) {
    div.innerHTML = t[0] + html + t[1];
    t[2].times(function() { div = div.firstChild });
  } else div.innerHTML = html;
  return $A(div.childNodes);
};

Element._insertionTranslations = {
  before: function(element, node) {
    element.parentNode.insertBefore(node, element);
  },
  top: function(element, node) {
    element.insertBefore(node, element.firstChild);
  },
  bottom: function(element, node) {
    element.appendChild(node);
  },
  after: function(element, node) {
    element.parentNode.insertBefore(node, element.nextSibling);
  },
  tags: {
    TABLE:  ['<table>',                '</table>',                   1],
    TBODY:  ['<table><tbody>',         '</tbody></table>',           2],
    TR:     ['<table><tbody><tr>',     '</tr></tbody></table>',      3],
    TD:     ['<table><tbody><tr><td>', '</td></tr></tbody></table>', 4],
    SELECT: ['<select>',               '</select>',                  1]
  }
};

(function() {
  Object.extend(this.tags, {
    THEAD: this.tags.TBODY,
    TFOOT: this.tags.TBODY,
    TH:    this.tags.TD
  });
}).call(Element._insertionTranslations);

Element.Methods.Simulated = {
  hasAttribute: function(element, attribute) {
    attribute = Element._attributeTranslations.has[attribute] || attribute;
    var node = $(element).getAttributeNode(attribute);
    return node && node.specified;
  }
};

Element.Methods.ByTag = { };

Object.extend(Element, Element.Methods);

if (!Prototype.BrowserFeatures.ElementExtensions &&
    document.createElement('div').__proto__) {
  window.HTMLElement = { };
  window.HTMLElement.prototype = document.createElement('div').__proto__;
  Prototype.BrowserFeatures.ElementExtensions = true;
}

Element.extend = (function() {
  if (Prototype.BrowserFeatures.SpecificElementExtensions)
    return Prototype.K;

  var Methods = { }, ByTag = Element.Methods.ByTag;

  var extend = Object.extend(function(element) {
    if (!element || element._extendedByPrototype ||
        element.nodeType != 1 || element == window) return element;

    var methods = Object.clone(Methods),
      tagName = element.tagName, property, value;

    // extend methods for specific tags
    if (ByTag[tagName]) Object.extend(methods, ByTag[tagName]);

    for (property in methods) {
      value = methods[property];
      if (Object.isFunction(value) && !(property in element))
        element[property] = value.methodize();
    }

    element._extendedByPrototype = Prototype.emptyFunction;
    return element;

  }, {
    refresh: function() {
      // extend methods for all tags (Safari doesn't need this)
      if (!Prototype.BrowserFeatures.ElementExtensions) {
        Object.extend(Methods, Element.Methods);
        Object.extend(Methods, Element.Methods.Simulated);
      }
    }
  });

  extend.refresh();
  return extend;
})();

Element.hasAttribute = function(element, attribute) {
  if (element.hasAttribute) return element.hasAttribute(attribute);
  return Element.Methods.Simulated.hasAttribute(element, attribute);
};

Element.addMethods = function(methods) {
  var F = Prototype.BrowserFeatures, T = Element.Methods.ByTag;

  if (!methods) {
    Object.extend(Form, Form.Methods);
    Object.extend(Form.Element, Form.Element.Methods);
    Object.extend(Element.Methods.ByTag, {
      "FORM":     Object.clone(Form.Methods),
      "INPUT":    Object.clone(Form.Element.Methods),
      "SELECT":   Object.clone(Form.Element.Methods),
      "TEXTAREA": Object.clone(Form.Element.Methods)
    });
  }

  if (arguments.length == 2) {
    var tagName = methods;
    methods = arguments[1];
  }

  if (!tagName) Object.extend(Element.Methods, methods || { });
  else {
    if (Object.isArray(tagName)) tagName.each(extend);
    else extend(tagName);
  }

  function extend(tagName) {
    tagName = tagName.toUpperCase();
    if (!Element.Methods.ByTag[tagName])
      Element.Methods.ByTag[tagName] = { };
    Object.extend(Element.Methods.ByTag[tagName], methods);
  }

  function copy(methods, destination, onlyIfAbsent) {
    onlyIfAbsent = onlyIfAbsent || false;
    for (var property in methods) {
      var value = methods[property];
      if (!Object.isFunction(value)) continue;
      if (!onlyIfAbsent || !(property in destination))
        destination[property] = value.methodize();
    }
  }

  function findDOMClass(tagName) {
    var klass;
    var trans = {
      "OPTGROUP": "OptGroup", "TEXTAREA": "TextArea", "P": "Paragraph",
      "FIELDSET": "FieldSet", "UL": "UList", "OL": "OList", "DL": "DList",
      "DIR": "Directory", "H1": "Heading", "H2": "Heading", "H3": "Heading",
      "H4": "Heading", "H5": "Heading", "H6": "Heading", "Q": "Quote",
      "INS": "Mod", "DEL": "Mod", "A": "Anchor", "IMG": "Image", "CAPTION":
      "TableCaption", "COL": "TableCol", "COLGROUP": "TableCol", "THEAD":
      "TableSection", "TFOOT": "TableSection", "TBODY": "TableSection", "TR":
      "TableRow", "TH": "TableCell", "TD": "TableCell", "FRAMESET":
      "FrameSet", "IFRAME": "IFrame"
    };
    if (trans[tagName]) klass = 'HTML' + trans[tagName] + 'Element';
    if (window[klass]) return window[klass];
    klass = 'HTML' + tagName + 'Element';
    if (window[klass]) return window[klass];
    klass = 'HTML' + tagName.capitalize() + 'Element';
    if (window[klass]) return window[klass];

    window[klass] = { };
    window[klass].prototype = document.createElement(tagName).__proto__;
    return window[klass];
  }

  if (F.ElementExtensions) {
    copy(Element.Methods, HTMLElement.prototype);
    copy(Element.Methods.Simulated, HTMLElement.prototype, true);
  }

  if (F.SpecificElementExtensions) {
    for (var tag in Element.Methods.ByTag) {
      var klass = findDOMClass(tag);
      if (Object.isUndefined(klass)) continue;
      copy(T[tag], klass.prototype);
    }
  }

  Object.extend(Element, Element.Methods);
  delete Element.ByTag;

  if (Element.extend.refresh) Element.extend.refresh();
  Element.cache = { };
};

document.viewport = {
  getDimensions: function() {
    var dimensions = { };
    var B = Prototype.Browser;
    $w('width height').each(function(d) {
      var D = d.capitalize();
      dimensions[d] = (B.WebKit && !document.evaluate) ? self['inner' + D] :
        (B.Opera) ? document.body['client' + D] : document.documentElement['client' + D];
    });
    return dimensions;
  },

  getWidth: function() {
    return this.getDimensions().width;
  },

  getHeight: function() {
    return this.getDimensions().height;
  },

  getScrollOffsets: function() {
    return Element._returnOffset(
      window.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
      window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop);
  }
};
/* Portions of the Selector class are derived from Jack Slocumâs DomQuery,
 * part of YUI-Ext version 0.40, distributed under the terms of an MIT-style
 * license.  Please see http://www.yui-ext.com/ for more information. */

var Selector = Class.create({
  initialize: function(expression) {
    this.expression = expression.strip();
    this.compileMatcher();
  },

  shouldUseXPath: function() {
    if (!Prototype.BrowserFeatures.XPath) return false;

    var e = this.expression;

    // Safari 3 chokes on :*-of-type and :empty
    if (Prototype.Browser.WebKit &&
     (e.include("-of-type") || e.include(":empty")))
      return false;

    // XPath can't do namespaced attributes, nor can it read
    // the "checked" property from DOM nodes
    if ((/(\[[\w-]*?:|:checked)/).test(this.expression))
      return false;

    return true;
  },

  compileMatcher: function() {
    if (this.shouldUseXPath())
      return this.compileXPathMatcher();

    var e = this.expression, ps = Selector.patterns, h = Selector.handlers,
        c = Selector.criteria, le, p, m;

    if (Selector._cache[e]) {
      this.matcher = Selector._cache[e];
      return;
    }

    this.matcher = ["this.matcher = function(root) {",
                    "var r = root, h = Selector.handlers, c = false, n;"];

    while (e && le != e && (/\S/).test(e)) {
      le = e;
      for (var i in ps) {
        p = ps[i];
        if (m = e.match(p)) {
          this.matcher.push(Object.isFunction(c[i]) ? c[i](m) :
    	      new Template(c[i]).evaluate(m));
          e = e.replace(m[0], '');
          break;
        }
      }
    }

    this.matcher.push("return h.unique(n);\n}");
    eval(this.matcher.join('\n'));
    Selector._cache[this.expression] = this.matcher;
  },

  compileXPathMatcher: function() {
    var e = this.expression, ps = Selector.patterns,
        x = Selector.xpath, le, m;

    if (Selector._cache[e]) {
      this.xpath = Selector._cache[e]; return;
    }

    this.matcher = ['.//*'];
    while (e && le != e && (/\S/).test(e)) {
      le = e;
      for (var i in ps) {
        if (m = e.match(ps[i])) {
          this.matcher.push(Object.isFunction(x[i]) ? x[i](m) :
            new Template(x[i]).evaluate(m));
          e = e.replace(m[0], '');
          break;
        }
      }
    }

    this.xpath = this.matcher.join('');
    Selector._cache[this.expression] = this.xpath;
  },

  findElements: function(root) {
    root = root || document;
    if (this.xpath) return document._getElementsByXPath(this.xpath, root);
    return this.matcher(root);
  },

  match: function(element) {
    this.tokens = [];

    var e = this.expression, ps = Selector.patterns, as = Selector.assertions;
    var le, p, m;

    while (e && le !== e && (/\S/).test(e)) {
      le = e;
      for (var i in ps) {
        p = ps[i];
        if (m = e.match(p)) {
          // use the Selector.assertions methods unless the selector
          // is too complex.
          if (as[i]) {
            this.tokens.push([i, Object.clone(m)]);
            e = e.replace(m[0], '');
          } else {
            // reluctantly do a document-wide search
            // and look for a match in the array
            return this.findElements(document).include(element);
          }
        }
      }
    }

    var match = true, name, matches;
    for (var i = 0, token; token = this.tokens[i]; i++) {
      name = token[0], matches = token[1];
      if (!Selector.assertions[name](element, matches)) {
        match = false; break;
      }
    }

    return match;
  },

  toString: function() {
    return this.expression;
  },

  inspect: function() {
    return "#<Selector:" + this.expression.inspect() + ">";
  }
});

Object.extend(Selector, {
  _cache: { },

  xpath: {
    descendant:   "//*",
    child:        "/*",
    adjacent:     "/following-sibling::*[1]",
    laterSibling: '/following-sibling::*',
    tagName:      function(m) {
      if (m[1] == '*') return '';
      return "[local-name()='" + m[1].toLowerCase() +
             "' or local-name()='" + m[1].toUpperCase() + "']";
    },
    className:    "[contains(concat(' ', @class, ' '), ' #{1} ')]",
    id:           "[@id='#{1}']",
    attrPresence: function(m) {
      m[1] = m[1].toLowerCase();
      return new Template("[@#{1}]").evaluate(m);
    },
    attr: function(m) {
      m[1] = m[1].toLowerCase();
      m[3] = m[5] || m[6];
      return new Template(Selector.xpath.operators[m[2]]).evaluate(m);
    },
    pseudo: function(m) {
      var h = Selector.xpath.pseudos[m[1]];
      if (!h) return '';
      if (Object.isFunction(h)) return h(m);
      return new Template(Selector.xpath.pseudos[m[1]]).evaluate(m);
    },
    operators: {
      '=':  "[@#{1}='#{3}']",
      '!=': "[@#{1}!='#{3}']",
      '^=': "[starts-with(@#{1}, '#{3}')]",
      '$=': "[substring(@#{1}, (string-length(@#{1}) - string-length('#{3}') + 1))='#{3}']",
      '*=': "[contains(@#{1}, '#{3}')]",
      '~=': "[contains(concat(' ', @#{1}, ' '), ' #{3} ')]",
      '|=': "[contains(concat('-', @#{1}, '-'), '-#{3}-')]"
    },
    pseudos: {
      'first-child': '[not(preceding-sibling::*)]',
      'last-child':  '[not(following-sibling::*)]',
      'only-child':  '[not(preceding-sibling::* or following-sibling::*)]',
      'empty':       "[count(*) = 0 and (count(text()) = 0 or translate(text(), ' \t\r\n', '') = '')]",
      'checked':     "[@checked]",
      'disabled':    "[@disabled]",
      'enabled':     "[not(@disabled)]",
      'not': function(m) {
        var e = m[6], p = Selector.patterns,
            x = Selector.xpath, le, v;

        var exclusion = [];
        while (e && le != e && (/\S/).test(e)) {
          le = e;
          for (var i in p) {
            if (m = e.match(p[i])) {
              v = Object.isFunction(x[i]) ? x[i](m) : new Template(x[i]).evaluate(m);
              exclusion.push("(" + v.substring(1, v.length - 1) + ")");
              e = e.replace(m[0], '');
              break;
            }
          }
        }
        return "[not(" + exclusion.join(" and ") + ")]";
      },
      'nth-child':      function(m) {
        return Selector.xpath.pseudos.nth("(count(./preceding-sibling::*) + 1) ", m);
      },
      'nth-last-child': function(m) {
        return Selector.xpath.pseudos.nth("(count(./following-sibling::*) + 1) ", m);
      },
      'nth-of-type':    function(m) {
        return Selector.xpath.pseudos.nth("position() ", m);
      },
      'nth-last-of-type': function(m) {
        return Selector.xpath.pseudos.nth("(last() + 1 - position()) ", m);
      },
      'first-of-type':  function(m) {
        m[6] = "1"; return Selector.xpath.pseudos['nth-of-type'](m);
      },
      'last-of-type':   function(m) {
        m[6] = "1"; return Selector.xpath.pseudos['nth-last-of-type'](m);
      },
      'only-of-type':   function(m) {
        var p = Selector.xpath.pseudos; return p['first-of-type'](m) + p['last-of-type'](m);
      },
      nth: function(fragment, m) {
        var mm, formula = m[6], predicate;
        if (formula == 'even') formula = '2n+0';
        if (formula == 'odd')  formula = '2n+1';
        if (mm = formula.match(/^(\d+)$/)) // digit only
          return '[' + fragment + "= " + mm[1] + ']';
        if (mm = formula.match(/^(-?\d*)?n(([+-])(\d+))?/)) { // an+b
          if (mm[1] == "-") mm[1] = -1;
          var a = mm[1] ? Number(mm[1]) : 1;
          var b = mm[2] ? Number(mm[2]) : 0;
          predicate = "[((#{fragment} - #{b}) mod #{a} = 0) and " +
          "((#{fragment} - #{b}) div #{a} >= 0)]";
          return new Template(predicate).evaluate({
            fragment: fragment, a: a, b: b });
        }
      }
    }
  },

  criteria: {
    tagName:      'n = h.tagName(n, r, "#{1}", c);      c = false;',
    className:    'n = h.className(n, r, "#{1}", c);    c = false;',
    id:           'n = h.id(n, r, "#{1}", c);           c = false;',
    attrPresence: 'n = h.attrPresence(n, r, "#{1}", c); c = false;',
    attr: function(m) {
      m[3] = (m[5] || m[6]);
      return new Template('n = h.attr(n, r, "#{1}", "#{3}", "#{2}", c); c = false;').evaluate(m);
    },
    pseudo: function(m) {
      if (m[6]) m[6] = m[6].replace(/"/g, '\\"');
      return new Template('n = h.pseudo(n, "#{1}", "#{6}", r, c); c = false;').evaluate(m);
    },
    descendant:   'c = "descendant";',
    child:        'c = "child";',
    adjacent:     'c = "adjacent";',
    laterSibling: 'c = "laterSibling";'
  },

  patterns: {
    // combinators must be listed first
    // (and descendant needs to be last combinator)
    laterSibling: /^\s*~\s*/,
    child:        /^\s*>\s*/,
    adjacent:     /^\s*\+\s*/,
    descendant:   /^\s/,

    // selectors follow
    tagName:      /^\s*(\*|[\w\-]+)(\b|$)?/,
    id:           /^#([\w\-\*]+)(\b|$)/,
    className:    /^\.([\w\-\*]+)(\b|$)/,
    pseudo:
/^:((first|last|nth|nth-last|only)(-child|-of-type)|empty|checked|(en|dis)abled|not)(\((.*?)\))?(\b|$|(?=\s|[:+~>]))/,
    attrPresence: /^\[([\w]+)\]/,
    attr:         /\[((?:[\w-]*:)?[\w-]+)\s*(?:([!^$*~|]?=)\s*((['"])([^\4]*?)\4|([^'"][^\]]*?)))?\]/
  },

  // for Selector.match and Element#match
  assertions: {
    tagName: function(element, matches) {
      return matches[1].toUpperCase() == element.tagName.toUpperCase();
    },

    className: function(element, matches) {
      return Element.hasClassName(element, matches[1]);
    },

    id: function(element, matches) {
      return element.id === matches[1];
    },

    attrPresence: function(element, matches) {
      return Element.hasAttribute(element, matches[1]);
    },

    attr: function(element, matches) {
      var nodeValue = Element.readAttribute(element, matches[1]);
      return nodeValue && Selector.operators[matches[2]](nodeValue, matches[5] || matches[6]);
    }
  },

  handlers: {
    // UTILITY FUNCTIONS
    // joins two collections
    concat: function(a, b) {
      for (var i = 0, node; node = b[i]; i++)
        a.push(node);
      return a;
    },

    // marks an array of nodes for counting
    mark: function(nodes) {
      var _true = Prototype.emptyFunction;
      for (var i = 0, node; node = nodes[i]; i++)
        node._countedByPrototype = _true;
      return nodes;
    },

    unmark: function(nodes) {
      for (var i = 0, node; node = nodes[i]; i++)
        node._countedByPrototype = undefined;
      return nodes;
    },

    // mark each child node with its position (for nth calls)
    // "ofType" flag indicates whether we're indexing for nth-of-type
    // rather than nth-child
    index: function(parentNode, reverse, ofType) {
      parentNode._countedByPrototype = Prototype.emptyFunction;
      if (reverse) {
        for (var nodes = parentNode.childNodes, i = nodes.length - 1, j = 1; i >= 0; i--) {
          var node = nodes[i];
          if (node.nodeType == 1 && (!ofType || node._countedByPrototype)) node.nodeIndex = j++;
        }
      } else {
        for (var i = 0, j = 1, nodes = parentNode.childNodes; node = nodes[i]; i++)
          if (node.nodeType == 1 && (!ofType || node._countedByPrototype)) node.nodeIndex = j++;
      }
    },

    // filters out duplicates and extends all nodes
    unique: function(nodes) {
      if (nodes.length == 0) return nodes;
      var results = [], n;
      for (var i = 0, l = nodes.length; i < l; i++)
        if (!(n = nodes[i])._countedByPrototype) {
          n._countedByPrototype = Prototype.emptyFunction;
          results.push(Element.extend(n));
        }
      return Selector.handlers.unmark(results);
    },

    // COMBINATOR FUNCTIONS
    descendant: function(nodes) {
      var h = Selector.handlers;
      for (var i = 0, results = [], node; node = nodes[i]; i++)
        h.concat(results, node.getElementsByTagName('*'));
      return results;
    },

    child: function(nodes) {
      var h = Selector.handlers;
      for (var i = 0, results = [], node; node = nodes[i]; i++) {
        for (var j = 0, child; child = node.childNodes[j]; j++)
          if (child.nodeType == 1 && child.tagName != '!') results.push(child);
      }
      return results;
    },

    adjacent: function(nodes) {
      for (var i = 0, results = [], node; node = nodes[i]; i++) {
        var next = this.nextElementSibling(node);
        if (next) results.push(next);
      }
      return results;
    },

    laterSibling: function(nodes) {
      var h = Selector.handlers;
      for (var i = 0, results = [], node; node = nodes[i]; i++)
        h.concat(results, Element.nextSiblings(node));
      return results;
    },

    nextElementSibling: function(node) {
      while (node = node.nextSibling)
	      if (node.nodeType == 1) return node;
      return null;
    },

    previousElementSibling: function(node) {
      while (node = node.previousSibling)
        if (node.nodeType == 1) return node;
      return null;
    },

    // TOKEN FUNCTIONS
    tagName: function(nodes, root, tagName, combinator) {
      var uTagName = tagName.toUpperCase();
      var results = [], h = Selector.handlers;
      if (nodes) {
        if (combinator) {
          // fastlane for ordinary descendant combinators
          if (combinator == "descendant") {
            for (var i = 0, node; node = nodes[i]; i++)
              h.concat(results, node.getElementsByTagName(tagName));
            return results;
          } else nodes = this[combinator](nodes);
          if (tagName == "*") return nodes;
        }
        for (var i = 0, node; node = nodes[i]; i++)
          if (node.tagName.toUpperCase() === uTagName) results.push(node);
        return results;
      } else return root.getElementsByTagName(tagName);
    },

    id: function(nodes, root, id, combinator) {
      var targetNode = $(id), h = Selector.handlers;
      if (!targetNode) return [];
      if (!nodes && root == document) return [targetNode];
      if (nodes) {
        if (combinator) {
          if (combinator == 'child') {
            for (var i = 0, node; node = nodes[i]; i++)
              if (targetNode.parentNode == node) return [targetNode];
          } else if (combinator == 'descendant') {
            for (var i = 0, node; node = nodes[i]; i++)
              if (Element.descendantOf(targetNode, node)) return [targetNode];
          } else if (combinator == 'adjacent') {
            for (var i = 0, node; node = nodes[i]; i++)
              if (Selector.handlers.previousElementSibling(targetNode) == node)
                return [targetNode];
          } else nodes = h[combinator](nodes);
        }
        for (var i = 0, node; node = nodes[i]; i++)
          if (node == targetNode) return [targetNode];
        return [];
      }
      return (targetNode && Element.descendantOf(targetNode, root)) ? [targetNode] : [];
    },

    className: function(nodes, root, className, combinator) {
      if (nodes && combinator) nodes = this[combinator](nodes);
      return Selector.handlers.byClassName(nodes, root, className);
    },

    byClassName: function(nodes, root, className) {
      if (!nodes) nodes = Selector.handlers.descendant([root]);
      var needle = ' ' + className + ' ';
      for (var i = 0, results = [], node, nodeClassName; node = nodes[i]; i++) {
        nodeClassName = node.className;
        if (nodeClassName.length == 0) continue;
        if (nodeClassName == className || (' ' + nodeClassName + ' ').include(needle))
          results.push(node);
      }
      return results;
    },

    attrPresence: function(nodes, root, attr, combinator) {
      if (!nodes) nodes = root.getElementsByTagName("*");
      if (nodes && combinator) nodes = this[combinator](nodes);
      var results = [];
      for (var i = 0, node; node = nodes[i]; i++)
        if (Element.hasAttribute(node, attr)) results.push(node);
      return results;
    },

    attr: function(nodes, root, attr, value, operator, combinator) {
      if (!nodes) nodes = root.getElementsByTagName("*");
      if (nodes && combinator) nodes = this[combinator](nodes);
      var handler = Selector.operators[operator], results = [];
      for (var i = 0, node; node = nodes[i]; i++) {
        var nodeValue = Element.readAttribute(node, attr);
        if (nodeValue === null) continue;
        if (handler(nodeValue, value)) results.push(node);
      }
      return results;
    },

    pseudo: function(nodes, name, value, root, combinator) {
      if (nodes && combinator) nodes = this[combinator](nodes);
      if (!nodes) nodes = root.getElementsByTagName("*");
      return Selector.pseudos[name](nodes, value, root);
    }
  },

  pseudos: {
    'first-child': function(nodes, value, root) {
      for (var i = 0, results = [], node; node = nodes[i]; i++) {
        if (Selector.handlers.previousElementSibling(node)) continue;
          results.push(node);
      }
      return results;
    },
    'last-child': function(nodes, value, root) {
      for (var i = 0, results = [], node; node = nodes[i]; i++) {
        if (Selector.handlers.nextElementSibling(node)) continue;
          results.push(node);
      }
      return results;
    },
    'only-child': function(nodes, value, root) {
      var h = Selector.handlers;
      for (var i = 0, results = [], node; node = nodes[i]; i++)
        if (!h.previousElementSibling(node) && !h.nextElementSibling(node))
          results.push(node);
      return results;
    },
    'nth-child':        function(nodes, formula, root) {
      return Selector.pseudos.nth(nodes, formula, root);
    },
    'nth-last-child':   function(nodes, formula, root) {
      return Selector.pseudos.nth(nodes, formula, root, true);
    },
    'nth-of-type':      function(nodes, formula, root) {
      return Selector.pseudos.nth(nodes, formula, root, false, true);
    },
    'nth-last-of-type': function(nodes, formula, root) {
      return Selector.pseudos.nth(nodes, formula, root, true, true);
    },
    'first-of-type':    function(nodes, formula, root) {
      return Selector.pseudos.nth(nodes, "1", root, false, true);
    },
    'last-of-type':     function(nodes, formula, root) {
      return Selector.pseudos.nth(nodes, "1", root, true, true);
    },
    'only-of-type':     function(nodes, formula, root) {
      var p = Selector.pseudos;
      return p['last-of-type'](p['first-of-type'](nodes, formula, root), formula, root);
    },

    // handles the an+b logic
    getIndices: function(a, b, total) {
      if (a == 0) return b > 0 ? [b] : [];
      return $R(1, total).inject([], function(memo, i) {
        if (0 == (i - b) % a && (i - b) / a >= 0) memo.push(i);
        return memo;
      });
    },

    // handles nth(-last)-child, nth(-last)-of-type, and (first|last)-of-type
    nth: function(nodes, formula, root, reverse, ofType) {
      if (nodes.length == 0) return [];
      if (formula == 'even') formula = '2n+0';
      if (formula == 'odd')  formula = '2n+1';
      var h = Selector.handlers, results = [], indexed = [], m;
      h.mark(nodes);
      for (var i = 0, node; node = nodes[i]; i++) {
        if (!node.parentNode._countedByPrototype) {
          h.index(node.parentNode, reverse, ofType);
          indexed.push(node.parentNode);
        }
      }
      if (formula.match(/^\d+$/)) { // just a number
        formula = Number(formula);
        for (var i = 0, node; node = nodes[i]; i++)
          if (node.nodeIndex == formula) results.push(node);
      } else if (m = formula.match(/^(-?\d*)?n(([+-])(\d+))?/)) { // an+b
        if (m[1] == "-") m[1] = -1;
        var a = m[1] ? Number(m[1]) : 1;
        var b = m[2] ? Number(m[2]) : 0;
        var indices = Selector.pseudos.getIndices(a, b, nodes.length);
        for (var i = 0, node, l = indices.length; node = nodes[i]; i++) {
          for (var j = 0; j < l; j++)
            if (node.nodeIndex == indices[j]) results.push(node);
        }
      }
      h.unmark(nodes);
      h.unmark(indexed);
      return results;
    },

    'empty': function(nodes, value, root) {
      for (var i = 0, results = [], node; node = nodes[i]; i++) {
        // IE treats comments as element nodes
        if (node.tagName == '!' || (node.firstChild && !node.innerHTML.match(/^\s*$/))) continue;
        results.push(node);
      }
      return results;
    },

    'not': function(nodes, selector, root) {
      var h = Selector.handlers, selectorType, m;
      var exclusions = new Selector(selector).findElements(root);
      h.mark(exclusions);
      for (var i = 0, results = [], node; node = nodes[i]; i++)
        if (!node._countedByPrototype) results.push(node);
      h.unmark(exclusions);
      return results;
    },

    'enabled': function(nodes, value, root) {
      for (var i = 0, results = [], node; node = nodes[i]; i++)
        if (!node.disabled) results.push(node);
      return results;
    },

    'disabled': function(nodes, value, root) {
      for (var i = 0, results = [], node; node = nodes[i]; i++)
        if (node.disabled) results.push(node);
      return results;
    },

    'checked': function(nodes, value, root) {
      for (var i = 0, results = [], node; node = nodes[i]; i++)
        if (node.checked) results.push(node);
      return results;
    }
  },

  operators: {
    '=':  function(nv, v) { return nv == v; },
    '!=': function(nv, v) { return nv != v; },
    '^=': function(nv, v) { return nv.startsWith(v); },
    '$=': function(nv, v) { return nv.endsWith(v); },
    '*=': function(nv, v) { return nv.include(v); },
    '~=': function(nv, v) { return (' ' + nv + ' ').include(' ' + v + ' '); },
    '|=': function(nv, v) { return ('-' + nv.toUpperCase() + '-').include('-' + v.toUpperCase() + '-'); }
  },

  split: function(expression) {
    var expressions = [];
    expression.scan(/(([\w#:.~>+()\s-]+|\*|\[.*?\])+)\s*(,|$)/, function(m) {
      expressions.push(m[1].strip());
    });
    return expressions;
  },

  matchElements: function(elements, expression) {
    var matches = $$(expression), h = Selector.handlers;
    h.mark(matches);
    for (var i = 0, results = [], element; element = elements[i]; i++)
      if (element._countedByPrototype) results.push(element);
    h.unmark(matches);
    return results;
  },

  findElement: function(elements, expression, index) {
    if (Object.isNumber(expression)) {
      index = expression; expression = false;
    }
    return Selector.matchElements(elements, expression || '*')[index || 0];
  },

  findChildElements: function(element, expressions) {
    expressions = Selector.split(expressions.join(','));
    var results = [], h = Selector.handlers;
    for (var i = 0, l = expressions.length, selector; i < l; i++) {
      selector = new Selector(expressions[i].strip());
      h.concat(results, selector.findElements(element));
    }
    return (l > 1) ? h.unique(results) : results;
  }
});

if (Prototype.Browser.IE) {
  Object.extend(Selector.handlers, {
    // IE returns comment nodes on getElementsByTagName("*").
    // Filter them out.
    concat: function(a, b) {
      for (var i = 0, node; node = b[i]; i++)
        if (node.tagName !== "!") a.push(node);
      return a;
    },

    // IE improperly serializes _countedByPrototype in (inner|outer)HTML.
    unmark: function(nodes) {
      for (var i = 0, node; node = nodes[i]; i++)
        node.removeAttribute('_countedByPrototype');
      return nodes;
    }
  });
}

function $$() {
  return Selector.findChildElements(document, $A(arguments));
}
var Form = {
  reset: function(form) {
    $(form).reset();
    return form;
  },

  serializeElements: function(elements, options) {
    if (typeof options != 'object') options = { hash: !!options };
    else if (Object.isUndefined(options.hash)) options.hash = true;
    var key, value, submitted = false, submit = options.submit;

    var data = elements.inject({ }, function(result, element) {
      if (!element.disabled && element.name) {
        key = element.name; value = $(element).getValue();
        if (value != null && (element.type != 'submit' || (!submitted &&
            submit !== false && (!submit || key == submit) && (submitted = true)))) {
          if (key in result) {
            // a key is already present; construct an array of values
            if (!Object.isArray(result[key])) result[key] = [result[key]];
            result[key].push(value);
          }
          else result[key] = value;
        }
      }
      return result;
    });

    return options.hash ? data : Object.toQueryString(data);
  }
};

Form.Methods = {
  serialize: function(form, options) {
    return Form.serializeElements(Form.getElements(form), options);
  },

  getElements: function(form) {
    return $A($(form).getElementsByTagName('*')).inject([],
      function(elements, child) {
        if (Form.Element.Serializers[child.tagName.toLowerCase()])
          elements.push(Element.extend(child));
        return elements;
      }
    );
  },

  getInputs: function(form, typeName, name) {
    form = $(form);
    var inputs = form.getElementsByTagName('input');

    if (!typeName && !name) return $A(inputs).map(Element.extend);

    for (var i = 0, matchingInputs = [], length = inputs.length; i < length; i++) {
      var input = inputs[i];
      if ((typeName && input.type != typeName) || (name && input.name != name))
        continue;
      matchingInputs.push(Element.extend(input));
    }

    return matchingInputs;
  },

  disable: function(form) {
    form = $(form);
    Form.getElements(form).invoke('disable');
    return form;
  },

  enable: function(form) {
    form = $(form);
    Form.getElements(form).invoke('enable');
    return form;
  },

  findFirstElement: function(form) {
    var elements = $(form).getElements().findAll(function(element) {
      return 'hidden' != element.type && !element.disabled;
    });
    var firstByIndex = elements.findAll(function(element) {
      return element.hasAttribute('tabIndex') && element.tabIndex >= 0;
    }).sortBy(function(element) { return element.tabIndex }).first();

    return firstByIndex ? firstByIndex : elements.find(function(element) {
      return ['input', 'select', 'textarea'].include(element.tagName.toLowerCase());
    });
  },

  focusFirstElement: function(form) {
    form = $(form);
    form.findFirstElement().activate();
    return form;
  },

  request: function(form, options) {
    form = $(form), options = Object.clone(options || { });

    var params = options.parameters, action = form.readAttribute('action') || '';
    if (action.blank()) action = window.location.href;
    options.parameters = form.serialize(true);

    if (params) {
      if (Object.isString(params)) params = params.toQueryParams();
      Object.extend(options.parameters, params);
    }

    if (form.hasAttribute('method') && !options.method)
      options.method = form.method;

    return new Ajax.Request(action, options);
  }
};

/*--------------------------------------------------------------------------*/

Form.Element = {
  focus: function(element) {
    $(element).focus();
    return element;
  },

  select: function(element) {
    $(element).select();
    return element;
  }
};

Form.Element.Methods = {
  serialize: function(element) {
    element = $(element);
    if (!element.disabled && element.name) {
      var value = element.getValue();
      if (value != undefined) {
        var pair = { };
        pair[element.name] = value;
        return Object.toQueryString(pair);
      }
    }
    return '';
  },

  getValue: function(element) {
    element = $(element);
    var method = element.tagName.toLowerCase();
    return Form.Element.Serializers[method](element);
  },

  setValue: function(element, value) {
    element = $(element);
    var method = element.tagName.toLowerCase();
    Form.Element.Serializers[method](element, value);
    return element;
  },

  clear: function(element) {
    $(element).value = '';
    return element;
  },

  present: function(element) {
    return $(element).value != '';
  },

  activate: function(element) {
    element = $(element);
    try {
      element.focus();
      if (element.select && (element.tagName.toLowerCase() != 'input' ||
          !['button', 'reset', 'submit'].include(element.type)))
        element.select();
    } catch (e) { }
    return element;
  },

  disable: function(element) {
    element = $(element);
    element.blur();
    element.disabled = true;
    return element;
  },

  enable: function(element) {
    element = $(element);
    element.disabled = false;
    return element;
  }
};

/*--------------------------------------------------------------------------*/

var Field = Form.Element;
var $F = Form.Element.Methods.getValue;

/*--------------------------------------------------------------------------*/

Form.Element.Serializers = {
  input: function(element, value) {
    switch (element.type.toLowerCase()) {
      case 'checkbox':
      case 'radio':
        return Form.Element.Serializers.inputSelector(element, value);
      default:
        return Form.Element.Serializers.textarea(element, value);
    }
  },

  inputSelector: function(element, value) {
    if (Object.isUndefined(value)) return element.checked ? element.value : null;
    else element.checked = !!value;
  },

  textarea: function(element, value) {
    if (Object.isUndefined(value)) return element.value;
    else element.value = value;
  },

  select: function(element, index) {
    if (Object.isUndefined(index))
      return this[element.type == 'select-one' ?
        'selectOne' : 'selectMany'](element);
    else {
      var opt, value, single = !Object.isArray(index);
      for (var i = 0, length = element.length; i < length; i++) {
        opt = element.options[i];
        value = this.optionValue(opt);
        if (single) {
          if (value == index) {
            opt.selected = true;
            return;
          }
        }
        else opt.selected = index.include(value);
      }
    }
  },

  selectOne: function(element) {
    var index = element.selectedIndex;
    return index >= 0 ? this.optionValue(element.options[index]) : null;
  },

  selectMany: function(element) {
    var values, length = element.length;
    if (!length) return null;

    for (var i = 0, values = []; i < length; i++) {
      var opt = element.options[i];
      if (opt.selected) values.push(this.optionValue(opt));
    }
    return values;
  },

  optionValue: function(opt) {
    // extend element because hasAttribute may not be native
    return Element.extend(opt).hasAttribute('value') ? opt.value : opt.text;
  }
};

/*--------------------------------------------------------------------------*/

Abstract.TimedObserver = Class.create(PeriodicalExecuter, {
  initialize: function($super, element, frequency, callback) {
    $super(callback, frequency);
    this.element   = $(element);
    this.lastValue = this.getValue();
  },

  execute: function() {
    var value = this.getValue();
    if (Object.isString(this.lastValue) && Object.isString(value) ?
        this.lastValue != value : String(this.lastValue) != String(value)) {
      this.callback(this.element, value);
      this.lastValue = value;
    }
  }
});

Form.Element.Observer = Class.create(Abstract.TimedObserver, {
  getValue: function() {
    return Form.Element.getValue(this.element);
  }
});

Form.Observer = Class.create(Abstract.TimedObserver, {
  getValue: function() {
    return Form.serialize(this.element);
  }
});

/*--------------------------------------------------------------------------*/

Abstract.EventObserver = Class.create({
  initialize: function(element, callback) {
    this.element  = $(element);
    this.callback = callback;

    this.lastValue = this.getValue();
    if (this.element.tagName.toLowerCase() == 'form')
      this.registerFormCallbacks();
    else
      this.registerCallback(this.element);
  },

  onElementEvent: function() {
    var value = this.getValue();
    if (this.lastValue != value) {
      this.callback(this.element, value);
      this.lastValue = value;
    }
  },

  registerFormCallbacks: function() {
    Form.getElements(this.element).each(this.registerCallback, this);
  },

  registerCallback: function(element) {
    if (element.type) {
      switch (element.type.toLowerCase()) {
        case 'checkbox':
        case 'radio':
          Event.observe(element, 'click', this.onElementEvent.bind(this));
          break;
        default:
          Event.observe(element, 'change', this.onElementEvent.bind(this));
          break;
      }
    }
  }
});

Form.Element.EventObserver = Class.create(Abstract.EventObserver, {
  getValue: function() {
    return Form.Element.getValue(this.element);
  }
});

Form.EventObserver = Class.create(Abstract.EventObserver, {
  getValue: function() {
    return Form.serialize(this.element);
  }
});
if (!window.Event) var Event = { };

Object.extend(Event, {
  KEY_BACKSPACE: 8,
  KEY_TAB:       9,
  KEY_RETURN:   13,
  KEY_ESC:      27,
  KEY_LEFT:     37,
  KEY_UP:       38,
  KEY_RIGHT:    39,
  KEY_DOWN:     40,
  KEY_DELETE:   46,
  KEY_HOME:     36,
  KEY_END:      35,
  KEY_PAGEUP:   33,
  KEY_PAGEDOWN: 34,
  KEY_INSERT:   45,

  cache: { },

  relatedTarget: function(event) {
    var element;
    switch(event.type) {
      case 'mouseover': element = event.fromElement; break;
      case 'mouseout':  element = event.toElement;   break;
      default: return null;
    }
    return Element.extend(element);
  }
});

Event.Methods = (function() {
  var isButton;

  if (Prototype.Browser.IE) {
    var buttonMap = { 0: 1, 1: 4, 2: 2 };
    isButton = function(event, code) {
      return event.button == buttonMap[code];
    };

  } else if (Prototype.Browser.WebKit) {
    isButton = function(event, code) {
      switch (code) {
        case 0: return event.which == 1 && !event.metaKey;
        case 1: return event.which == 1 && event.metaKey;
        default: return false;
      }
    };

  } else {
    isButton = function(event, code) {
      return event.which ? (event.which === code + 1) : (event.button === code);
    };
  }

  return {
    isLeftClick:   function(event) { return isButton(event, 0) },
    isMiddleClick: function(event) { return isButton(event, 1) },
    isRightClick:  function(event) { return isButton(event, 2) },

    element: function(event) {
      var node = Event.extend(event).target;
      return Element.extend(node.nodeType == Node.TEXT_NODE ? node.parentNode : node);
    },

    findElement: function(event, expression) {
      var element = Event.element(event);
      if (!expression) return element;
      var elements = [element].concat(element.ancestors());
      return Selector.findElement(elements, expression, 0);
    },

    pointer: function(event) {
      return {
        x: event.pageX || (event.clientX +
          (document.documentElement.scrollLeft || document.body.scrollLeft)),
        y: event.pageY || (event.clientY +
          (document.documentElement.scrollTop || document.body.scrollTop))
      };
    },

    pointerX: function(event) { return Event.pointer(event).x },
    pointerY: function(event) { return Event.pointer(event).y },

    stop: function(event) {
      Event.extend(event);
      event.preventDefault();
      event.stopPropagation();
      event.stopped = true;
    }
  };
})();

Event.extend = (function() {
  var methods = Object.keys(Event.Methods).inject({ }, function(m, name) {
    m[name] = Event.Methods[name].methodize();
    return m;
  });

  if (Prototype.Browser.IE) {
    Object.extend(methods, {
      stopPropagation: function() { this.cancelBubble = true },
      preventDefault:  function() { this.returnValue = false },
      inspect: function() { return "[object Event]" }
    });

    return function(event) {
      if (!event) return false;
      if (event._extendedByPrototype) return event;

      event._extendedByPrototype = Prototype.emptyFunction;
      var pointer = Event.pointer(event);
      Object.extend(event, {
        target: event.srcElement,
        relatedTarget: Event.relatedTarget(event),
        pageX:  pointer.x,
        pageY:  pointer.y
      });
      return Object.extend(event, methods);
    };

  } else {
    Event.prototype = Event.prototype || document.createEvent("HTMLEvents").__proto__;
    Object.extend(Event.prototype, methods);
    return Prototype.K;
  }
})();

Object.extend(Event, (function() {
  var cache = Event.cache;

  function getEventID(element) {
    if (element._prototypeEventID) return element._prototypeEventID[0];
    arguments.callee.id = arguments.callee.id || 1;
    return element._prototypeEventID = [++arguments.callee.id];
  }

  function getDOMEventName(eventName) {
    if (eventName && eventName.include(':')) return "dataavailable";
    return eventName;
  }

  function getCacheForID(id) {
    return cache[id] = cache[id] || { };
  }

  function getWrappersForEventName(id, eventName) {
    var c = getCacheForID(id);
    return c[eventName] = c[eventName] || [];
  }

  function createWrapper(element, eventName, handler) {
    var id = getEventID(element);
    var c = getWrappersForEventName(id, eventName);
    if (c.pluck("handler").include(handler)) return false;

    var wrapper = function(event) {
      if (!Event || !Event.extend ||
        (event.eventName && event.eventName != eventName))
          return false;

      Event.extend(event);
      handler.call(element, event);
    };

    wrapper.handler = handler;
    c.push(wrapper);
    return wrapper;
  }

  function findWrapper(id, eventName, handler) {
    var c = getWrappersForEventName(id, eventName);
    return c.find(function(wrapper) { return wrapper.handler == handler });
  }

  function destroyWrapper(id, eventName, handler) {
    var c = getCacheForID(id);
    if (!c[eventName]) return false;
    c[eventName] = c[eventName].without(findWrapper(id, eventName, handler));
  }

  function destroyCache() {
    for (var id in cache)
      for (var eventName in cache[id])
        cache[id][eventName] = null;
  }

  if (window.attachEvent) {
    window.attachEvent("onunload", destroyCache);
  }

  return {
    observe: function(element, eventName, handler) {
      element = $(element);
      var name = getDOMEventName(eventName);

      var wrapper = createWrapper(element, eventName, handler);
      if (!wrapper) return element;

      if (element.addEventListener) {
        element.addEventListener(name, wrapper, false);
      } else {
        element.attachEvent("on" + name, wrapper);
      }

      return element;
    },

    stopObserving: function(element, eventName, handler) {
      element = $(element);
      var id = getEventID(element), name = getDOMEventName(eventName);

      if (!handler && eventName) {
        getWrappersForEventName(id, eventName).each(function(wrapper) {
          element.stopObserving(eventName, wrapper.handler);
        });
        return element;

      } else if (!eventName) {
        Object.keys(getCacheForID(id)).each(function(eventName) {
          element.stopObserving(eventName);
        });
        return element;
      }

      var wrapper = findWrapper(id, eventName, handler);
      if (!wrapper) return element;

      if (element.removeEventListener) {
        element.removeEventListener(name, wrapper, false);
      } else {
        element.detachEvent("on" + name, wrapper);
      }

      destroyWrapper(id, eventName, handler);

      return element;
    },

    fire: function(element, eventName, memo) {
      element = $(element);
      if (element == document && document.createEvent && !element.dispatchEvent)
        element = document.documentElement;

      var event;
      if (document.createEvent) {
        event = document.createEvent("HTMLEvents");
        event.initEvent("dataavailable", true, true);
      } else {
        event = document.createEventObject();
        event.eventType = "ondataavailable";
      }

      event.eventName = eventName;
      event.memo = memo || { };

      if (document.createEvent) {
        element.dispatchEvent(event);
      } else {
        element.fireEvent(event.eventType, event);
      }

      return Event.extend(event);
    }
  };
})());

Object.extend(Event, Event.Methods);

Element.addMethods({
  fire:          Event.fire,
  observe:       Event.observe,
  stopObserving: Event.stopObserving
});

Object.extend(document, {
  fire:          Element.Methods.fire.methodize(),
  observe:       Element.Methods.observe.methodize(),
  stopObserving: Element.Methods.stopObserving.methodize(),
  loaded:        false
});

(function() {
  /* Support for the DOMContentLoaded event is based on work by Dan Webb,
     Matthias Miller, Dean Edwards and John Resig. */

  var timer;

  function fireContentLoadedEvent() {
    if (document.loaded) return;
    if (timer) window.clearInterval(timer);
    document.fire("dom:loaded");
    document.loaded = true;
  }

  if (document.addEventListener) {
    if (Prototype.Browser.WebKit) {
      timer = window.setInterval(function() {
        if (/loaded|complete/.test(document.readyState))
          fireContentLoadedEvent();
      }, 0);

      Event.observe(window, "load", fireContentLoadedEvent);

    } else {
      document.addEventListener("DOMContentLoaded",
        fireContentLoadedEvent, false);
    }

  } else {
    document.write("<script id=__onDOMContentLoaded defer src=//:><\/script>");
    $("__onDOMContentLoaded").onreadystatechange = function() {
      if (this.readyState == "complete") {
        this.onreadystatechange = null;
        fireContentLoadedEvent();
      }
    };
  }
})();
/*------------------------------- DEPRECATED -------------------------------*/

Hash.toQueryString = Object.toQueryString;

var Toggle = { display: Element.toggle };

Element.Methods.childOf = Element.Methods.descendantOf;

var Insertion = {
  Before: function(element, content) {
    return Element.insert(element, {before:content});
  },

  Top: function(element, content) {
    return Element.insert(element, {top:content});
  },

  Bottom: function(element, content) {
    return Element.insert(element, {bottom:content});
  },

  After: function(element, content) {
    return Element.insert(element, {after:content});
  }
};

var $continue = new Error('"throw $continue" is deprecated, use "return" instead');

// This should be moved to script.aculo.us; notice the deprecated methods
// further below, that map to the newer Element methods.
var Position = {
  // set to true if needed, warning: firefox performance problems
  // NOT neeeded for page scrolling, only if draggable contained in
  // scrollable elements
  includeScrollOffsets: false,

  // must be called before calling withinIncludingScrolloffset, every time the
  // page is scrolled
  prepare: function() {
    this.deltaX =  window.pageXOffset
                || document.documentElement.scrollLeft
                || document.body.scrollLeft
                || 0;
    this.deltaY =  window.pageYOffset
                || document.documentElement.scrollTop
                || document.body.scrollTop
                || 0;
  },

  // caches x/y coordinate pair to use with overlap
  within: function(element, x, y) {
    if (this.includeScrollOffsets)
      return this.withinIncludingScrolloffsets(element, x, y);
    this.xcomp = x;
    this.ycomp = y;
    this.offset = Element.cumulativeOffset(element);

    return (y >= this.offset[1] &&
            y <  this.offset[1] + element.offsetHeight &&
            x >= this.offset[0] &&
            x <  this.offset[0] + element.offsetWidth);
  },

  withinIncludingScrolloffsets: function(element, x, y) {
    var offsetcache = Element.cumulativeScrollOffset(element);

    this.xcomp = x + offsetcache[0] - this.deltaX;
    this.ycomp = y + offsetcache[1] - this.deltaY;
    this.offset = Element.cumulativeOffset(element);

    return (this.ycomp >= this.offset[1] &&
            this.ycomp <  this.offset[1] + element.offsetHeight &&
            this.xcomp >= this.offset[0] &&
            this.xcomp <  this.offset[0] + element.offsetWidth);
  },

  // within must be called directly before
  overlap: function(mode, element) {
    if (!mode) return 0;
    if (mode == 'vertical')
      return ((this.offset[1] + element.offsetHeight) - this.ycomp) /
        element.offsetHeight;
    if (mode == 'horizontal')
      return ((this.offset[0] + element.offsetWidth) - this.xcomp) /
        element.offsetWidth;
  },

  // Deprecation layer -- use newer Element methods now (1.5.2).

  cumulativeOffset: Element.Methods.cumulativeOffset,

  positionedOffset: Element.Methods.positionedOffset,

  absolutize: function(element) {
    Position.prepare();
    return Element.absolutize(element);
  },

  relativize: function(element) {
    Position.prepare();
    return Element.relativize(element);
  },

  realOffset: Element.Methods.cumulativeScrollOffset,

  offsetParent: Element.Methods.getOffsetParent,

  page: Element.Methods.viewportOffset,

  clone: function(source, target, options) {
    options = options || { };
    return Element.clonePosition(target, source, options);
  }
};

/*--------------------------------------------------------------------------*/

if (!document.getElementsByClassName) document.getElementsByClassName = function(instanceMethods){
  function iter(name) {
    return name.blank() ? null : "[contains(concat(' ', @class, ' '), ' " + name + " ')]";
  }

  instanceMethods.getElementsByClassName = Prototype.BrowserFeatures.XPath ?
  function(element, className) {
    className = className.toString().strip();
    var cond = /\s/.test(className) ? $w(className).map(iter).join('') : iter(className);
    return cond ? document._getElementsByXPath('.//*' + cond, element) : [];
  } : function(element, className) {
    className = className.toString().strip();
    var elements = [], classNames = (/\s/.test(className) ? $w(className) : null);
    if (!classNames && !className) return elements;

    var nodes = $(element).getElementsByTagName('*');
    className = ' ' + className + ' ';

    for (var i = 0, child, cn; child = nodes[i]; i++) {
      if (child.className && (cn = ' ' + child.className + ' ') && (cn.include(className) ||
          (classNames && classNames.all(function(name) {
            return !name.toString().blank() && cn.include(' ' + name + ' ');
          }))))
        elements.push(Element.extend(child));
    }
    return elements;
  };

  return function(className, parentElement) {
    return $(parentElement || document.body).getElementsByClassName(className);
  };
}(Element.Methods);

/*--------------------------------------------------------------------------*/

Element.ClassNames = Class.create();
Element.ClassNames.prototype = {
  initialize: function(element) {
    this.element = $(element);
  },

  _each: function(iterator) {
    this.element.className.split(/\s+/).select(function(name) {
      return name.length > 0;
    })._each(iterator);
  },

  set: function(className) {
    this.element.className = className;
  },

  add: function(classNameToAdd) {
    if (this.include(classNameToAdd)) return;
    this.set($A(this).concat(classNameToAdd).join(' '));
  },

  remove: function(classNameToRemove) {
    if (!this.include(classNameToRemove)) return;
    this.set($A(this).without(classNameToRemove).join(' '));
  },

  toString: function() {
    return $A(this).join(' ');
  }
};

Object.extend(Element.ClassNames.prototype, Enumerable);

/*--------------------------------------------------------------------------*/

Element.addMethods();
/*
 * Date Format 1.2.3
 * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
 * MIT license
 *
 * Includes enhancements by Scott Trenda <scott.trenda.net>
 * and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default. 
 */

// Found on: http://blog.stevenlevithan.com/archives/date-time-format
// Downloaded from: http://stevenlevithan.com/assets/misc/date.format.js
   
   
//    Mask  	Description
//    d 	Day of the month as digits; no leading zero for single-digit days.
//    dd 	Day of the month as digits; leading zero for single-digit days.
//    ddd 	Day of the week as a three-letter abbreviation.
//    dddd 	Day of the week as its full name.
//    m 	Month as digits; no leading zero for single-digit months.
//    mm 	Month as digits; leading zero for single-digit months.
//    mmm 	Month as a three-letter abbreviation.
//    mmmm 	Month as its full name.
//    yy 	Year as last two digits; leading zero for years less than 10.
//    yyyy 	Year represented by four digits.
//    h 	Hours; no leading zero for single-digit hours (12-hour clock).
//    hh 	Hours; leading zero for single-digit hours (12-hour clock).
//    H 	Hours; no leading zero for single-digit hours (24-hour clock).
//    HH 	Hours; leading zero for single-digit hours (24-hour clock).
//    M 	Minutes; no leading zero for single-digit minutes.
//    		Uppercase M unlike CF timeFormat's m to avoid conflict with months.
//    MM 	Minutes; leading zero for single-digit minutes.
//    		Uppercase MM unlike CF timeFormat's mm to avoid conflict with months.
//    s 	Seconds; no leading zero for single-digit seconds.
//    ss 	Seconds; leading zero for single-digit seconds.
//    l or L 	Milliseconds. l gives 3 digits. L gives 2 digits.
//    t 	Lowercase, single-character time marker string: a or p.
//    		No equivalent in CF.
//    tt 	Lowercase, two-character time marker string: am or pm.
//    		No equivalent in CF.
//    T 	Uppercase, single-character time marker string: A or P.
//    		Uppercase T unlike CF's t to allow for user-specified casing.
//    TT 	Uppercase, two-character time marker string: AM or PM.
//    		Uppercase TT unlike CF's tt to allow for user-specified casing.
//    Z 	US timezone abbreviation, e.g. EST or MDT. With non-US timezones or in the Opera browser, the GMT/UTC offset is returned, e.g. GMT-0500
//    		No equivalent in CF.
//    o 	GMT/UTC timezone offset, e.g. -0500 or +0230.
//    		No equivalent in CF.
//    S 	The date's ordinal suffix (st, nd, rd, or th). Works well with d.
//    		No equivalent in CF.
//    UTC: 	Must be the first four characters of the mask. Converts the date from local time to UTC/GMT/Zulu time before applying the mask. The "UTC:" prefix is removed.
//    		No equivalent in CF.
//    'â¦' or "â¦" 	Literal character sequence. Surrounding quotes are removed.
//         		No equivalent in CF.
//   	
//   
//    And here are the named masks provided by default (you can easily change these or add your own):
//    Name 	     	Mask 	     	      	 	 Example
//    default 		ddd mmm dd yyyy HH:MM:ss 	 Sat Jun 09 2007 17:46:21
//    shortDate 	m/d/yy 	   			 6/9/07
//    mediumDate 	mmm d, yyyy			 Jun 9, 2007
//    longDate 		mmmm d, yyyy 			 June 9, 2007
//    fullDate 		dddd, mmmm d, yyyy 		 Saturday, June 9, 2007
//    shortTime 	h:MM TT 			 5:46 PM
//    mediumTime 	h:MM:ss TT 			 5:46:21 PM
//    longTime 		h:MM:ss TT Z 			 5:46:21 PM EST
//    isoDate 		yyyy-mm-dd 			 2007-06-09
//    isoTime 		HH:MM:ss 			 17:46:21
//    isoDateTime 	yyyy-mm-dd'T'HH:MM:ss 		 2007-06-09T17:46:21
//    isoUtcDateTime 	UTC:yyyy-mm-dd'T'HH:MM:ss'Z' 	 2007-06-09T22:46:21Z
//   
//    A couple issues:
//   
//        * In the unlikely event that there is ambiguity in the meaning of your mask (e.g., m followed by mm, with no separating characters), put a pair of empty quotes between your metasequences. The quotes will be removed automatically.
//        * If you need to include literal quotes in your mask, the following rules apply:
//              o Unpaired quotes do not need special handling.
//              o To include literal quotes inside masks which contain any other quote marks of the same type, you need to enclose them with the alternative quote type (i.e., double quotes for single quotes, and vice versa).
//                E.g., date.format('h "o\'clock, y\'all!"') returns "6 o'clock, y'all". This can get a little hairy, perhaps, but I doubt people will really run into it that often.
//                The previous example can also be written as date.format("h") + "o'clock, y'all!".
   

var dateFormat = function () {
	var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
			val = String(val);
			len = len || 2;
			while (val.length < len) val = "0" + val;
			return val;
		};

	// Regexes and supporting functions are cached through closure
	return function (date, mask, utc) {
		var dF = dateFormat;

		// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
		if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}

		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date;
		if (isNaN(date)) throw SyntaxError("invalid date");

		mask = String(dF.masks[mask] || mask || dF.masks["default"]);

		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == "UTC:") {
			mask = mask.slice(4);
			utc = true;
		}

		var	_ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
				d:    d,
				dd:   pad(d),
				ddd:  dF.i18n.dayNames[D],
				dddd: dF.i18n.dayNames[D + 7],
				m:    m + 1,
				mm:   pad(m + 1),
				mmm:  dF.i18n.monthNames[m],
				mmmm: dF.i18n.monthNames[m + 12],
				yy:   String(y).slice(2),
				yyyy: y,
				h:    H % 12 || 12,
				hh:   pad(H % 12 || 12),
				H:    H,
				HH:   pad(H),
				M:    M,
				MM:   pad(M),
				s:    s,
				ss:   pad(s),
				l:    pad(L, 3),
				L:    pad(L > 99 ? Math.round(L / 10) : L),
				t:    H < 12 ? "a"  : "p",
				tt:   H < 12 ? "am" : "pm",
				T:    H < 12 ? "A"  : "P",
				TT:   H < 12 ? "AM" : "PM",
				Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
				o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
				S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

		return mask.replace(token, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
}();

// Some common format strings
dateFormat.masks = {
	"default":      "ddd mmm dd yyyy HH:MM:ss",
	shortDate:      "m/d/yy",
	mediumDate:     "mmm d, yyyy",
	longDate:       "mmmm d, yyyy",
	fullDate:       "dddd, mmmm d, yyyy",
	shortTime:      "h:MM TT",
	mediumTime:     "h:MM:ss TT",
	longTime:       "h:MM:ss TT Z",
	isoDate:        "yyyy-mm-dd",
	isoTime:        "HH:MM:ss",
	isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
	isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
	dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
	monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
	return dateFormat(this, mask, utc);
};

function onclickTwitterWithTitle(article_title, window_title, domain, url) {
  // Needs to be opened in the onclick event, if opened in the callback
  // pop-up blockers may very well block the window
  var new_window = window.open("");
  new_window.document.title = window_title + "...";

  function callback(shortUrl) {
    var twitter_string = article_title + " " + shortUrl + " (via @" + domain + ")";
    new_window.location = "http://twitter.com/home?status=" + encodeURIComponent(twitter_string);
  }

  twitterShortenUrl(url, callback);
}

function twitterShortenUrl(url, callback) {
  BitlyCB.twitterShortenUrlResponse = function(response) {
    var result;
    if (response.statusCode != "OK") {
      callback(url);
      return;
    }
    
    for (var idx in response.results) {
      result = response.results[idx];
      if (result.shortUrl) {
        callback(result.shortUrl);
        return;
      }
    }

    callback(url);
    return;
  }

  BitlyClient.shorten(url, 'BitlyCB.twitterShortenUrlResponse');
}

/* Example response:

BitlyCB.alertResponse({
    "errorCode": 0, 
    "errorMessage": "", 
    "results": {
        "http://f s sd    www.dn.se": {
            "hash": "65CfCP", 
            "shortKeywordUrl": "", 
            "shortUrl": "http://bit.ly/4SIhfi", 
            "userHash": "4SIhfi"
        }
    }, 
    "statusCode": "OK"
})
*/
<!--cache control: force proto cache-->