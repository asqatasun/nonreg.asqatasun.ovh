pic0= new Image(92,15); 
pic0.src="/MTE_CP/mt-static//plugins/Recommend/star_0.gif"; 
pic1= new Image(92,15); 
pic1.src="/MTE_CP/mt-static//plugins/Recommend/star_1.gif"; 
pic2= new Image(92,15); 
pic2.src="/MTE_CP/mt-static//plugins/Recommend/star_2.gif"; 
pic3= new Image(92,15); 
pic3.src="/MTE_CP/mt-static//plugins/Recommend/star_3.gif"; 
pic4= new Image(92,15); 
pic4.src="/MTE_CP/mt-static//plugins/Recommend/star_4.gif"; 

pic5= new Image(92,15); 
pic5.src="/MTE_CP/mt-static//plugins/Recommend/star_5.gif"; 

var Favorites = {
	 addEvent: function(elm, evType, fn, useCapture) {
		 if (elm.addEventListener) {
			 elm.addEventListener(evType, fn, useCapture);
			 return true;
		 } else if (elm.attachEvent) {
			 var r = elm.attachEvent('on' + evType, fn);
			 return r;
		 } else {
			 elm['on' + evType] = fn;
		 }
	 },

	setRecommendation: function(entry, value) {
		var url = ratingURL + '?entry_id=' + entry + ';value=' + value;
		var myAjax = new Ajax.Updater( 'recommended' , url, {
			onLoading: function(request) { Favorites.recommendation_loading(entry) },
			onComplete: function(request) { Favorites.recommendation_added(entry) },
			asynchronous: true,
			method: 'get'} );
	},
	
	recommendation_loading: function(entry) {
		var prog = 'progress_recommend_' + entry;
		try {
			new Effect.Appear(prog);
		} catch (e) {}
	},
	
	recommendation_added: function(entry) {
		var prog = 'progress_recommend_' + entry;
		try {
			new Effect.Fade(prog);
		} catch (e) {}
		
		txt = recommendedText;
		var div_rectext = 'ratetext_' + entry;
		var div_recvotes = 'ratevotes_' + entry;

		if (!document.getElementById(div_recvotes)) {
			txt += ' (1)';
		} else {
			var votes = document.getElementById(div_recvotes).innerHTML;
			votes = parseInt(votes) + 1;
			document.getElementById(div_recvotes).innerHTML = votes;		
			new Effect.Highlight(div_recvotes);
		}
		
		document.getElementById(div_rectext).innerHTML = txt;
		new Effect.Highlight(div_rectext);
	
	},
	
	createCookie: function(name,value,days) {
        if (days)
        {
                var date = new Date();
                date.setTime(date.getTime()+(days*24*60*60*1000));
                var expires = "; expires="+date.toGMTString();
        }
        else var expires = "";
        document.cookie = name+"="+value+expires+"; path=/";
	},

	readCookie: function(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++)
        {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
	},

	eraseCookie: function(name) {
        Favorites.createCookie(name,"",-1);
	},

	init: function() {
		var img = document.getElementsByTagName("img");
		for (x=0;x<img.length;x++) {
			if (img[x].className && img[x].className == 'rating') {
				Event.observe(img[x], 'mousemove', Favorites.starRating, true);
				Event.observe(img[x], 'mouseout', Favorites.clearRating, false);
				Event.observe(img[x], 'click', Favorites.sendRating, false);
			}
		}
	},
	
	starRating: function(e) {
		var elem = Event.element(e);
		var positions = Position.cumulativeOffset(elem);
		var offsetX = Event.pointerX(e) - positions[0];
		var elemWidth = Element.getWidth(elem);
		if (!elem.getAttribute('oldsrc')) elem.setAttribute('oldsrc', elem.getAttribute('src'));
		src = elem.src.substring(0, elem.src.lastIndexOf('/'));
		elem.setAttribute('src', src + '/star_' + Math.ceil((offsetX/elemWidth)*5) + '.gif');
	},
	
	clearRating: function(e) {
		var elem = Event.element(e);
		if (elem.getAttribute('oldsrc')) {
			elem.setAttribute('src', elem.getAttribute('oldsrc'));
		}
	},
	
	sendRating: function(e) {
		var elem = Event.element(e);
		var positions = Position.cumulativeOffset(elem);
		var offsetX = Event.pointerX(e) - positions[0];
		var elemWidth = Element.getWidth(elem);
		var rating = Math.ceil((offsetX/elemWidth)*5);
		entry = elem.id.replace(/ratevotes_/, '');
		var url = ratingURL + '?entry_id=' + entry + ';value=' + rating;
		var myAjax = new Ajax.Updater( 'recommended' , url, {
			asynchronous: true,
			method: 'get'} );
			
		src = elem.src.substring(0, elem.src.lastIndexOf('/'));
		elem.setAttribute('src', src + '/star_' + Math.ceil((offsetX/elemWidth)*5) + '_red.gif');

		Event.stopObserving(elem, 'mousemove', Favorites.starRating, true);
		Event.stopObserving(elem, 'mouseout', Favorites.clearRating, false);
		Event.stopObserving(elem, 'click', Favorites.sendRating, false);
	}
	
};
