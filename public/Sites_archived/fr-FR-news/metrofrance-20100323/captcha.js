var captchaLoaded = false;
if (window.mtBlogID) {
document.observe('dom:loaded', function() {
  if ($$('#comment-text').length > 0) {
  Event.observe('comment-text', 'focus', function(e) {
     if (! captchaLoaded) {
       new Ajax.Updater('captcha-image', "/MTE_CP/plugins/MetroCaptcha/captcha.fcgi?blog_id=" + mtBlogID + "&mode=ajax&rand=" + Math.ceil(Math.random() * 1000001), { evalScripts: true });
       captchaLoaded = true;
       Element.show('captcha-fields');
    }
  });
  }
});

} else {

Ajax.Responders.register({
   onComplete: function() { 
    Event.observe('comment-text', 'focus', function(e) {
     if (! captchaLoaded) {
       new Ajax.Updater('captcha-image', "/MTE_CP/plugins/MetroCaptcha/captcha.fcgi?blog_id=7&mode=ajax&rand=" + Math.ceil(Math.random() * 1000001), { evalScripts: true });
       captchaLoaded = true;
       Element.show('captcha-fields');
     }
    });
   }
});
}
