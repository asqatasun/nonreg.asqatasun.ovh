// borrowed this from lightbox js
function getPageSize(){
	
	var xScroll, yScroll;
	
	if (window.innerHeight && window.scrollMaxY) {	
		xScroll = document.body.scrollWidth;
		yScroll = window.innerHeight + window.scrollMaxY;
	} else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
		xScroll = document.body.scrollWidth;
		yScroll = document.body.scrollHeight;
	} else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
		xScroll = document.body.offsetWidth;
		yScroll = document.body.offsetHeight;
	}
	
	var windowWidth, windowHeight;
	if (self.innerHeight) {	// all except Explorer
		windowWidth = self.innerWidth;
		windowHeight = self.innerHeight;
	} else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
		windowWidth = document.documentElement.clientWidth;
		windowHeight = document.documentElement.clientHeight;
	} else if (document.body) { // other Explorers
		windowWidth = document.body.clientWidth;
		windowHeight = document.body.clientHeight;
	}	
	
	// for small pages with total height less then height of the viewport
	if(yScroll < windowHeight){
		pageHeight = windowHeight;
	} else { 
		pageHeight = yScroll;
	}

	// for small pages with total width less then width of the viewport
	if(xScroll < windowWidth){	
		pageWidth = windowWidth;
	} else {
		pageWidth = xScroll;
	}

	// from quirksmode.org
	var x,y;
	if (self.pageYOffset) // all except Explorer
	{
		x = self.pageXOffset;
		y = self.pageYOffset;
	}
	else if (document.documentElement && document.documentElement.scrollTop)
		// Explorer 6 Strict
	{
		x = document.documentElement.scrollLeft;
		y = document.documentElement.scrollTop;
	}
	else if (document.body) // all other Explorers
	{
		x = document.body.scrollLeft;
		y = document.body.scrollTop;
	}


	arrayPageSize = new Array(pageWidth,pageHeight,x,y) 
	return arrayPageSize;
}

clickedElem = null;
function moderate(elem, href) {
	pgSize = getPageSize();
	var iframe = document.createElement("iframe");
	iframe.setAttribute('width', 500);
	iframe.setAttribute('height', 500);
	iframe.setAttribute('frameborder', 0);
	iframe.setAttribute("src", href);
	iframe.style.borderTop = "1px solid black";
	iframe.style.marginTop = "4px";
	
	var obj = document.createElement("div");
	obj.setAttribute("id", "moderate_frame");
	obj.style.border = "1px solid black";
	obj.style.position = "absolute";
	obj.style.textAlign = "right";
	obj.style.backgroundColor = "#eee";
	obj.style.left = (parseInt(pgSize[0])/2 - 225) + "px";
	obj.style.top = (parseInt(pgSize[3]) + 50) + "px"
	obj.style.zIndex = 20;
	
	closeHref = document.createElement("a");
	closeHref.style.fontSize = "12px";
	closeHref.style.padding = "5px";
	closeHref.setAttribute("href", "javascript:void(0)");
	if (window.addEventListener) {
		closeHref.addEventListener("click", closeOverlay, false);
	} else {
		closeHref.attachEvent("onclick", closeOverlay);
	}
	
	closeHref.appendChild(document.createTextNode("Cliquez pour fermer"));
	obj.appendChild(closeHref);
	obj.appendChild(document.createElement("br"));
	obj.appendChild(iframe);

	document.body.appendChild(obj);
	
	var overlay = document.createElement("div");
	overlay.setAttribute("id", "moderate_overlay");
	overlay.style.position = "absolute";
	overlay.style.top = "0px";
	overlay.style.left = "0px";
	overlay.style.zIndex = 10;
	overlay.style.filter = "alpha(opacity=60)";
	overlay.style.backgroundColor = "#000";
	overlay.style.opacity = 0.6;
	overlay.style.width = pgSize[0] + "px";
	overlay.style.height = pgSize[1] + "px";
	
	document.body.appendChild(overlay);
	clickedElem = elem;
	return false;
}

function closeOverlay() {
	if (document.getElementById("moderate_frame") && document.getElementById("moderate_overlay")) {
		document.body.removeChild(document.getElementById("moderate_frame"));
		document.body.removeChild(document.getElementById("moderate_overlay"));
	}
	return false;
}
