
////////////////////////////////////////////////////////////////////////////////////////////////////
/// INITIALISATION /////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
var posiPointer = 1;
var maxItem = 6;

/*
function init()
{
	document.btleftscroll.src="images/trombi/bt_left_off.gif";
	if (nbTrombiResult <= maxItem) { document.btrightscroll.src="images/trombi/bt_right_off.gif"; }
	if (nbTrombiResult < maxItem)
	{
		//document.btrightscroll.src="images/trombi/bt_right_off.gif";
		for (i=1;i<=nbTrombiResult;i++)
		{
			document.getElementById('trombi'+i).style.display = "block";
		}
	}
	else
	{
		for (i=1;i<=maxItem;i++)
		{
			document.getElementById('trombi'+i).style.display = "block";
		}
	}	
}


////////////////////////////////////////////////////////////////////////////////////////////////////
/// GESTION DE LA BARRE DE DEFILEMENT DU TROMBI ////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

function trombiScroll(sens)
{
	if (nbTrombiResult > maxItem)
	{
		if (sens == "right")
		{
			if ((posiPointer + maxItem) <= nbTrombiResult)
			{
				for (i=1;i<=nbTrombiResult;i++)
				{
					document.getElementById('trombi'+i).style.display = "none";
				}
				posiPointer++;
				document.btleftscroll.src="images/trombi/bt_left.gif";
				for (i=posiPointer;i<(posiPointer+maxItem);i++)
				{
					document.getElementById('trombi'+i).style.display = "block";
				}
			}
			if ((posiPointer + maxItem) > nbTrombiResult)
			{
				document.btrightscroll.src="images/trombi/bt_right_off.gif";
			}
		}
		if (sens == "left")
		{
			if (posiPointer > 1)
			{
				for (i=1;i<=nbTrombiResult;i++)
				{
					document.getElementById('trombi'+i).style.display = "none";
				}
				posiPointer--;
				document.btrightscroll.src="images/trombi/bt_right.gif";
				for (i=posiPointer;i<=(posiPointer+maxItem);i++)
				{
					document.getElementById('trombi'+i).style.display = "block";
				}
			}
			if (posiPointer <= 1) { document.btleftscroll.src="images/trombi/bt_left_off.gif"; }
		}
	}
}
*/

////////////////////////////////////////////////////////////////////////////////////////////////////
/// GESTION DE LA DESCRIPTION DU TROMBI EN SURVOL //////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

function trombiWhoIs(descript)
{
	if (descript == 0)
	{
		for (i=1;i<=nbTrombiResult;i++)
		{
			document.getElementById('textBox'+i).style.background = "#FFFFFF";
			document.getElementById('textBox'+i).style.color = "#000000";
		}
	}
	else
	{
		document.getElementById('textBox'+descript).style.background = "#7C3A5D";
		document.getElementById('textBox'+descript).style.color = "#FFFFFF";
	}
}


////////////////////////////////////////////////////////////////////////////////////////////////////
/// GESTION DE L'AFFICHAGE DES LAYERS DE DETAILS ///////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

function gestFiche(numFiche)
{
	for (i=1;i<=nbTrombiResult;i++)
	{
		document.getElementById('fiche'+i).style.display = "none";
	}
	document.getElementById('fiche'+numFiche).style.display = "block";
}



/*  Modification JEB */

function trimAll(sString) 
	{
		while (sString.substring(0,1) == ' ')
		{
			sString = sString.substring(1, sString.length);
		}
		while (sString.substring(sString.length-1, sString.length) == ' ')
		{
			sString = sString.substring(0,sString.length-1);
		}
		return sString;
	}