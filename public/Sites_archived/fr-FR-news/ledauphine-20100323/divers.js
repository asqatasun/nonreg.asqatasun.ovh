//Divers

var dateExp="Sat, 01-Jan-3010 08:00:00 GMT"

function newCookie(name,value)
{
	document.cookie=name+"="+value+";expires="+dateExp+";path=/";
}

function getCookie(nameCookie)
{
	var cookieTrouve=false;
	var debut=0;
	var fin=0;
	var ch=document.cookie;
	var i=0;
	
	while (i<=ch.length)
	{
		debut=i;
		
		fin=debut+nameCookie.length;
		
		if (ch.substring(debut,fin)==nameCookie)
		{
			cookieTrouve=true;
			
			break;
		}
		
		i++;
		
	}
	
	if (cookieTrouve)
	{
		debut=fin+1;
		
		fin=document.cookie.indexOf(";",debut);
		
		if(fin<debut)
		fin=document.cookie.length;
		
		return document.cookie.substring(debut,fin);
	}
	
	return "";
	
}

function getCookieDL(nameCookie)
{
	var cookieTrouve=false;
	var debut=0;
	var fin=0;
	var ch=document.cookie;
	var i=0;
	
	while (i<=ch.length)
	{
		debut=i;
		
		fin=debut+nameCookie.length;
		
		if (ch.substring(debut,fin)==nameCookie)
		{
			cookieTrouve=true;
			
			break;
		}
		
		i++;
		
	}
	
	if (cookieTrouve)
	{
		debut=fin+1;
		
		fin=document.cookie.indexOf(";",debut);
		
		if(fin<debut)
		fin=document.cookie.length;
		
		return document.cookie.substring(debut,fin);
	}
	
	return "";
	
}


function getRadioValue(radioName) {

	var radio = document.getElementsByName(radioName);
	
	var res = "0";
	
	for(var i=0;i<radio.length;i++) {
		
		if(radio[i].checked) {
		
			res = radio[i].value;
			
			break;
		}
	}
	
	return res;
}