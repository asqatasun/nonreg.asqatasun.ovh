function voteCommentaire(vote, idCommentaire, objetCommentaire){
	if(AccepteCookies()){
		if(LireCookie("a_vote_"+objetCommentaire+"_" + idCommentaire)!="ok"){
			var url = contextPath+"/include/vote_commentaire.jspz";
			var pars = "";
			pars += "&id="+idCommentaire;
			pars += "&vote="+vote;
			pars += "&objet="+objetCommentaire;
			var myAjax = new Ajax.Request(url, {method: "post", parameters: pars, onComplete: traiteReponseVoteCommentaire, onFailure: afficheErreurVoteCommentaire, asynchronous:true});
		}
		else{
			alert("Vous avez d�j� vot�.");
			return false;
		}
	}
	else{
		alert("Pour pouvoir voter, votre navigateur doit accepter les cookies.");
	}
}

function traiteReponseVoteCommentaire(res){
	var str = res.responseText;
	if(str.lastIndexOf("|") != -1){
		var tab = new Array();
		tab = str.split("|");
		var idCommentaire = parseInt(tab[0]);
		var note = parseFloat(tab[1]);
		var nbvotes = parseInt(tab[2]);
		var objetCommentaire = tab[3];
		afficheNouvelleNoteCommentaire(idCommentaire, note, nbvotes);
		var date=new Date();
		date.setFullYear(date.getFullYear()+1);
		EcrireCookie("a_vote_"+objetCommentaire+"_" + idCommentaire, "ok", date);
	}
	else{
		afficheErreurVoteCommentaire(str);
	}
}

function afficheNouvelleNoteCommentaire(idCommentaire, note, nbvotes){
	$('noteCommentaire_' + idCommentaire).innerHTML = note;
	var str_nbvotes = "";
	if(nbvotes==0) str_nbvotes = '(0 vote)';
	else if(nbvotes==1) str_nbvotes = '(1 vote)';
	else str_nbvotes = '(' + nbvotes + ' votes)';
	$('nbVotesCommentaire_' + idCommentaire).innerHTML = str_nbvotes;
}

function afficheErreurVoteCommentaire(chaine){
	alert("Une erreur est survenue : " + chaine);
	return false;
}