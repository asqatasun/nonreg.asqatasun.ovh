/*Menu de navigation*/

function onClickMenuLoisirs(){
	deselectionneTousOnglets();
	Element.show("sousmenuloisirs");
	Element.hide("sousmenudepartement");
	Element.hide("sousmenusport");
	$("menuloisirs").className = "selected";
	$("menucuisine").className = "noBorder";
	$("titremenuloisirs").className = "strong";
	
	return false;
}

function onClickMenuDepartement(){
	deselectionneTousOnglets();
	Element.hide("sousmenuloisirs");
	Element.show("sousmenudepartement");
	Element.hide("sousmenusport");
	$("menudepartement").className = "selected";
	$("menuinfoslocales").className = "noBorder";
	$("titremenudepartement").className = "strong";
	
	return false;
}

function onClickMenuSport(){
	deselectionneTousOnglets();
	Element.hide("sousmenuloisirs");
	Element.hide("sousmenudepartement");
	Element.show("sousmenusport");
	$("menusport").className = "selected";
	$("menudepartement").className = "noBorder";
	$("titremenusport").className = "strong";
	
	return false;
}

function deselectionneTousOnglets(){
	$("menualaune").className = "noBorder";
	$("menufrancemonde").className = "";
	$("menueconomie").className = "";
	$("menupolitique").className = "";
	$("menusport").className = "";
	$("menudepartement").className = "";
	$("menuinfoslocales").className = "";
	$("menuloisirs").className = "";
	$("menucuisine").className = "";
	
	deselectionneTitres();
	
}

function deselectionneTitres(){
	$("titremenualaune").className = "";
	$("titremenufrancemonde").className = "";
	$("titremenueconomie").className = "";
	$("titremenupolitique").className = "";
	$("titremenusport").className = "";
	$("titremenudepartement").className = "";
	$("titremenuinfoslocales").className = "";
	$("titremenuloisirs").className = "";
	$("titremenucuisine").className = "";
}

/*Menu des départements pour les communes*/
function cacheTousDepartements(){
	departements = Array("01","04","05","07","26","30","38","73","74","84","13");
	for(var i=0;i<11;i++){
		Element.hide("listRubLocale_" + departements[i]);
		$("ongletDPT" + departements[i]).className = "";
	}
}

function afficheDepartement(num){
	cacheTousDepartements();
	num = "" + num;
	if(num.length == 1){
		num = "0" + num;
	}
	Element.show("listRubLocale_" + num);
	$("ongletDPT" + num).className = "selected";
}

/*Menu pour les loisirs*/
function cacheTousLoisirs(){
	departements = Array("Loisir","Sport","Culture","Foire","Brocante","Soiree");
	for(var i=0;i<6;i++){
		if($("rssLoisir_" + departements[i])!=null){
			Element.hide("rssLoisir_" + departements[i]);
		}
		$("ongletLoisir" + departements[i]).className = "";
	}
}

function afficheOngletLoisir(libelle){
	cacheTousLoisirs();
	if($("rssLoisir_" + libelle)!=null){
		Element.show("rssLoisir_" + libelle);
	}
	$("ongletLoisir" + libelle).className = "selected";
}

/*Menu pour les agendas*/
function afficheAgendaLoisir(){
	Element.hide('rss_agenda_pratique');
	$("onglet_agenda_pratique").className = "";
	Element.show('rss_agenda_loisir');
	$("onglet_agenda_loisir").className = "selected";
}

function afficheAgendaPratique(){
	Element.hide('rss_agenda_loisir');
	$("onglet_agenda_loisir").className = "";
	Element.show('rss_agenda_pratique');
	$("onglet_agenda_pratique").className = "selected";
}

/*Menu pour les catalogues*/
function cacheTousCatalogues(){
	catalogues = Array("1","2","3");
	for(var i=0;i<3;i++){
		Element.hide("listCatalogue" + catalogues[i]);
		$("ongletCatalogue" + catalogues[i]).className = "";
	}
}
function afficheCatalogue(num){
	cacheTousCatalogues();
	Element.show("listCatalogue" + num);
	$("ongletCatalogue" + num).className = "selected";
}