var captchaValide = 0;

function controleCaptcha(){
	var url = contextPath+"/include/captcha_controle.jspz";
	var pars = "";
	pars += "&captcha="+$("captcha").value;
	var myAjax = new Ajax.Request(url, {method: "post", parameters: pars, onComplete: traiteReponseControleCaptcha, onFailure: afficheErreurControleCaptcha, asynchronous:false});
}

function traiteReponseControleCaptcha(res){
	var str = res.responseText;
	//alert(str);
	// 1 : la captcha est valide
	// 0 : la captcha est invalide
	if(str == 0) {
		captchaValide = 0;
		$("captcha").style.backgroundColor='#FF7F50';
		alert("Le code antispam que vous avez saisi est invalide.");
		$("captcha").focus();
		return false;
	}
	else if(str == 1) {
		$("captcha").style.backgroundColor='#FFFFFF';
		captchaValide = 1;
	}
	else{		
	 	afficheErreurControleCaptcha(str);
	}
}

function afficheErreurControleCaptcha(chaine){
	alert("Une erreur est survenue au contr�le du captcha : " + chaine);
	return false;
}
