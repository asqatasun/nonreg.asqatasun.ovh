var nbrComment = 10;

function writeComment(id, idTitre)
{
	if(!document.getElementById(id))
	{
  		return false;
  	}

	var xhr_object;
	   if(window.XMLHttpRequest) // Firefox 
	      xhr_object = new XMLHttpRequest(); 
	   else if(window.ActiveXObject) // Internet Explorer 
	      xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
	   else { // XMLHttpRequest non support� par le navigateur 
	      alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
	      return false; 
	   } 
	 
	   xhr_object.open("GET", xmlBaseUrl+'?w=comment&n='+nbrComment+'&titre='+idTitre+'&ch='+CHAINE, true); 
		
   		xhr_object.onreadystatechange = function() {
   		  if(xhr_object.aborted) return;
	      if(xhr_object.readyState == 4)
	      {
	      	if(xhr_object.status  == 200)
	      	{

	      		var bloc = document.getElementById(id);
	      	
	        	var xml = xhr_object.responseXML;                   //  Assigner le fichier XML � une variable
				var buffer='';
				
				if(xml.getElementsByTagName("comment").length < 1)
				{
					$("#ctnAllComment").slideUp("slow");
					return false;
				}
				for(var i=0;i<Math.min(nbrComment, xml.getElementsByTagName("comment").length);i++)
				{
					comment = xml.getElementsByTagName("comment")[i];
					article = xml.getElementsByTagName("article")[i];
				
					buffer+= '<div class="artComment' + (i%2) + '">';					
					buffer+='<a href="' + edito_url('ra/' + article.getElementsByTagName("idrub").item(0).firstChild.nodeValue + '/' + article.getElementsByTagName("idart").item(0).firstChild.nodeValue) + '">';
					buffer+='<div class="titreArt">';
					buffer+=article.getElementsByTagName("titre").item(0).firstChild.nodeValue;
					buffer+='</div>';
					buffer+='<div class="comment">';
					buffer+='<div class="texte">';
					buffer+='<img class="bulle" src="'+shareBaseUrl+'img/commun/logo/nbcmt.png" />';
					if(comment.getElementsByTagName("heure").item(0).firstChild.nodeValue!='00:00')
					{
						buffer+='<span class="heure">'+comment.getElementsByTagName("heure").item(0).firstChild.nodeValue+'</span> ';
					}
					buffer+='<span class="pseudo">'+comment.getElementsByTagName("pseudo").item(0).firstChild.nodeValue+'</span> ';
					buffer+=comment.getElementsByTagName("texte").item(0).firstChild.nodeValue;
					buffer+='</div>';
					buffer+='</div>';
					buffer+='</a>';
					buffer+= '</div>';
				}
				
				bloc.innerHTML=buffer;

				bloc.style.display='block';
			}
	      } 
	   } 

	   xhr_object.send(null); 

	return false;	
}

edito.addInitCallback(function(){writeComment('ctnComment', TITRE);});