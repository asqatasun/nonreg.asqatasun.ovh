function registerNewsletter(idti, email)
{
	var xhr = null;
 	var ok=true;
 	if(!newsletter_validMail(email)) ok=false;

	if(!ok)
	{
		return false;
	}
	    
    if (window.XMLHttpRequest) 
    { 
        xhr = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) 
    {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
    else
    {
    	return false;
    }
    xhr.onreadystatechange = function() {
 		if (xhr.readyState == 4) { 
			var docXML = xhr.responseXML;
			if(docXML.getElementsByTagName("code").item(0).firstChild.data=='ok')
			{
				alert('Votre demande a �t� prise en compte. Vous allez recevoir un mail dans quelques minutes vous indiquant comment valider votre inscription.');
			}
 		}
    };
	
    xhr.open("GET", xmlBaseUrl + "?w=subnews"+"&em="+escape(email)+"&ti="+idti, true);
    xhr.send(null);
}

function newsletter_validMail(email)
{
	var re_mail = new RegExp('^[^@]+[@][^@]+$');
	if(!re_mail.test(email))
	{
		alert('Veuillez saisir votre email.');
		return false;
	}
	return true;
}