function setPiclens(url)
{
	mmc.setField('piclens', true);
	mmc.write();
	
	document.location = url;
}

function goPiclens()
{
/*
	var aUrl = document.location.search.replace(/\?/, '').split('&');
	var aParam = [];

	for(var nI=0; nI < aUrl.length; nI++)
	{
		var aC = aUrl[nI].split('=');
		if(aC[0] != 'undefined' && aC[1] != 'undefined')
		{
			aParam[aC[0]] = aC[1];
		}
	}
	
	if(aParam['piclens'] != 'undefined' && aParam['piclens'] == '1')
	{
		PicLensLite.start();
	}
*/

	if(mmc.getField('piclens', false))
		PicLensLite.start();

	mmc.setField('piclens', false);
	mmc.write();
}

function edito_url(route, params, base)
{
	if(base===undefined||base===null) base=baseUrl;
	if(params===undefined||params===null) params=new Array();
	url=base+route;
	for(param in erpp)
	{
		if(params[param]===undefined||params[param]===null) params[param]=erpp[param];
	}
	for(param in params)
	{
		url+='/'+param+'/'+escape(params[param]);
	}
	return url;
}

function editoTitre(id, base)
{
	this.id=id;
	this.base=base;
}

editoUser = function(edito)
{
	this._edito=edito;
	this._name=null;
	this._pseudo=null;
	this._email=null;
	this._credits=null;
	this._connected=false;

	this._testConnectCallback=new Array();

	this.init();
	this.readCookie();
}

editoUser.prototype.readCookie = function()
{
	var c=mmc;
	
	this._name=c.getField('uName');
	this._pseudo=c.getField('uPseudo');
	this._email=c.getField('uEmail');
	this._credits=c.getField('uCredits');
	this._connected=c.getField('uConnected');
}

editoUser.prototype.writeCookie = function()
{
	var c=mmc;
	
	c.setField('uName',this._name);
	c.setField('uPseudo',this._pseudo);
	c.setField('uEmail',this._email);
	c.setField('uCredits',this._credits);
	c.setField('uConnected', this._connected);
	c.write();
}

editoUser.prototype.isConnected = function()
{
	return this._connected;
}

editoUser.prototype.init = function()
{
	this._name=null;
	this._pseudo=null;
	this._email=null;
	this._credits=null;
	this._connected=false;
}

editoUser.prototype.connectKo = function(tried, callback)
{
	this.init();
	this.writeCookie();
	this.manage(tried);
	if(callback) callback();
	for(i in this._testConnectCallback)
	{	
		this._testConnectCallback[i](false);
	}	
}

editoUser.prototype.connectOk = function(tried, callback)
{
	this._connected=true;
	this.writeCookie();
	this.manage(tried);
	if(callback) callback();
	for(i in this._testConnectCallback)
	{
		this._testConnectCallback[i](true);
	}
}

editoUser.prototype.manage = function(tried)
{
	this.manageJel(tried);
}

editoUser.prototype.manageJel = function(tried)
{
	//FIXME a virer asap
	if(document.getElementById('ctnJelv2'))
	{
		if(this.isConnected())
		{
			$("#ctnJelv2Con").slideDown("fast");
			$("#ctnJelv2NoCon").slideUp("fast");
			$("#ctnJelv2Error").slideUp("fast");
			if(jelv2)
			{
				jelv2.init();
			}
		}
		else
		{
			$("#ctnJelv2Con").slideUp("fast");
			$("#ctnJelv2NoCon").slideDown("fast");
			if(tried===true)
			{
				document.getElementById('ctnJelv2Error').innerHTML='Identifiant ou mot de passe incorrect';
				$("#ctnJelv2Error").slideDown("fast");
			}
			else
			{
				$("#ctnJelv2Error").slideUp("fast");
			}
		}
	}
}

editoUser.prototype.connect = function(login, password, callbackok, callbackko)
{
	var xhr_object;
   if(window.XMLHttpRequest) // Firefox 
      xhr_object = new XMLHttpRequest(); 
   else if(window.ActiveXObject) // Internet Explorer 
      xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
   else { // XMLHttpRequest non support� par le navigateur 
      alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
      return false; 
   } 
   xhr_object.open("GET", xmlBaseUrl+'?w=connect&l='+escape(login)+'&p='+escape($P.md5(password)), true); 
   var self=this;
	xhr_object.onreadystatechange = function() {
	  if(xhr_object.aborted) return;
      if(xhr_object.readyState == 4)
      {
      	if(xhr_object.status == 200)
      	{
      		var docXML = xhr_object.responseXML;
      		if(docXML.getElementsByTagName("code").item(0).firstChild.data=='ok')
      		{
      			self._name=docXML.getElementsByTagName("name").item(0).firstChild.data;
      			self._pseudo=docXML.getElementsByTagName("pseudo").item(0).firstChild.data;
      			self._email=docXML.getElementsByTagName("email").item(0).firstChild.data;
      			self._credits=docXML.getElementsByTagName("credits").item(0).firstChild.data;
      			self.connectOk(true, callbackok);
      		}
      		else
      		{
      			self.connectKo(true, callbackko);
      		}
      	}
     } 
   } 
 
   //xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); 
   xhr_object.send(null); 

}

editoUser.prototype.testConnect = function(callbackok, callbackko)
{

	var xhr_object;
   if(window.XMLHttpRequest) // Firefox 
      xhr_object = new XMLHttpRequest(); 
   else if(window.ActiveXObject) // Internet Explorer 
      xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
   else { // XMLHttpRequest non support� par le navigateur 
      alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
      return false; 
   } 
   xhr_object.open("GET", xmlBaseUrl+'?w=testconnect&'+Math.random(), true); 
   var self=this;
	xhr_object.onreadystatechange = function() {
	  if(xhr_object.aborted) return;
      if(xhr_object.readyState == 4)
      {
      	if(xhr_object.status  == 200)
      	{
      		var docXML = xhr_object.responseXML;
      		if(docXML.getElementsByTagName("code").item(0).firstChild.data=='ok')
      		{
      			self._name=docXML.getElementsByTagName("name").item(0).firstChild.data;
      			self._pseudo=docXML.getElementsByTagName("pseudo").item(0).firstChild.data;
      			self._email=docXML.getElementsByTagName("email").item(0).firstChild.data;
      			self._credits=docXML.getElementsByTagName("credits").item(0).firstChild.data;
      			self.connectOk(false, callbackok);
      		}
      		else
      		{
      			self.connectKo(false, callbackko);
      		}
      	}
     } 
   } 
 
   //xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); 
   xhr_object.send(null); 

}

editoUser.prototype.addConnectCallback = function(callback)
{
	this._testConnectCallback.push(callback);
}

editoUser.prototype.disconnect = function(callback)
{
	var xhr_object;
   if(window.XMLHttpRequest) // Firefox 
      xhr_object = new XMLHttpRequest(); 
   else if(window.ActiveXObject) // Internet Explorer 
      xhr_object = new ActiveXObject("Microsoft.XMLHTTP"); 
   else { // XMLHttpRequest non support� par le navigateur 
      alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest..."); 
      return false; 
   } 
   xhr_object.open("GET", xmlBaseUrl+'?w=disconnect', true); 
   var self=this;
	xhr_object.onreadystatechange = function() {
	  if(xhr_object.aborted) return;
      if(xhr_object.readyState == 4)
      {
      	if(xhr_object.status  == 200)
      	{
       		self.connectKo(false, callback);
      	}
     } 
   } 
 
   //xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); 
   xhr_object.send(null); 

}

Edito = function(params)
{
	this._userUrl=null;
	this._hotUrl=null;
	this._shareUrl=null;
	this._photoUrl=null;
	this._toolboxUrl=null;
	this._xmlUrl=null;
	this._user=null;
	this._today=null;
	this._mca=null;
	this._mmc=null;
	this._mpc=null;
	this._erpp=null;
	this._SID=null;
	this._TITRE=null;
	this._titres=null;
	
	if(params)
	{
		this.setParams(params);
	}
}

Edito.prototype.addConnectCallback = function(callback)
{
	this._user.addConnectCallback(callback);
}

Edito.prototype.setParams = function(params)
{
	if(params.userUrl) this._userUrl=params.userUrl;
	if(params.hotUrl) this._hotUrl=params.hotUrl;
	if(params.shareUrl) this._shareUrl=params.shareUrl;
	if(params.photoUrl) this._photoUrl=params.photoUrl;
	if(params.toolboxUrl) this._toolboxUrl=params.toolboxUrl;
	if(params.xmlUrl) this._xmlUrl=params.xmlUrl;
	if(params.user) this._user=params.user;
	if(params.today) this._today=params.today;
	if(params.mca) this._mca=params.mca;
	if(params.mmc) this._mmc=params.mmc;
	if(params.mpc) this._mmc=params.mpc;
	if(params.erpp) this._erpp=params.erpp;
	if(params.SID) this._SID=params.SID;
	if(params.TITRE) this._TITRE=params.TITRE;
}

Edito.prototype.userUrl = function(what)
{
	var session='';
	if(this._SID)
	{
		var c=new extCookie(this._SID);
		session=c.getValue();
		if(session)session='?sso1='+session;
		else session='';
	}
	return this._userUrl+what+session;
}

Edito.prototype.getUrl = function(route, params, base)
{
	if(base===undefined||base===null) base=baseUrl;
	if(params===undefined||params===null) params=new Array();
	url=base+route;
	for(param in erpp)
	{
		if(params[param]===undefined||params[param]===null) params[param]=erpp[param];
	}
	for(param in params)
	{
		url+='/'+param+'/'+escape(params[param]);
	}
	return url;
}

Edito.prototype.getTitre = function(id)
{
	if(id===undefined||id===null) id=this._TITRE;
	return this._titres[id];
};

Edito.prototype.addInitCallback = function(callback)
{
	edito._mca.push(callback);
}

Edito.prototype.init = function()
{
	var forceTest=false;
	var url=location.href;
	
	var reg = new RegExp('\#(.*)$');
	var match=reg.exec(url);
	
	if((match) && (match[1]=='sso1'))forceTest=true;

	if(this._user.isConnected()||forceTest) this._user.testConnect();
	else this._user.connectKo();
	goPiclens();
	for(i in this._mca){ this._mca[i](); }
}

var edito=new Edito();

var mca=new Array();
edito._mca=mca;
var mmc=new objCookie('mmc', null, '/');
edito._mmc=mmc;
var mpc=new objCookie('mpc', new Date(2099, 12, 31), '/');
edito._mpc=mpc;
var edusr = new editoUser(edito);
edito._user=edusr;
var erpp=new Array();
edito._erpp=erpp;

$(document).ready(function(){edito.init();});

/*
function print_r(obj) {
	  win_print_r = window.open('about:blank', 'win_print_r');
	  win_print_r.document.write('<html><body>');
	  r_print_r(obj, win_print_r);
	  win_print_r.document.write('</body></html>');
	 }

	 function r_print_r(theObj, win_print_r) {
	  if(theObj.constructor == Array ||
	   theObj.constructor == Object){
	   if (win_print_r == null)
	    win_print_r = window.open('about:blank', 'win_print_r');
	   }
	   for(var p in theObj){
	    if(theObj[p].constructor == Array||
	     theObj[p].constructor == Object){
	     win_print_r.document.write("<li>["+p+"] =>"+typeof(theObj)+"</li>");
	     win_print_r.document.write("<ul>")
	     r_print_r(theObj[p], win_print_r);
	     win_print_r.document.write("</ul>")
	    } else {
	     win_print_r.document.write("<li>["+p+"] =>"+theObj[p]+"</li>");
	    }
	   }
	  win_print_r.document.write("</ul>")
	 }
	 */
