var counter_vus=new idsCookie('art_vus', new Date(2099, 12, 31), '/');
var counter_votes=new idsCookie('art_votes', new Date(2099, 12, 31), '/');

function cntUp(idobj, doincvote, doincvu)
{
	var obj='article';

	var isdet=false;

    if(doincvu)
    {
    	if(counter_vus.isId(idobj)) doincvu=false;
    	else
    	{
    		counter_vus.addId(idobj);
    		counter_vus.write();
    	}
    	isdet=true;
    }

    if(doincvote)
    {
    	if(counter_votes.isId(idobj)) doincvote=false;
    	else
    	{
    		counter_votes.addId(idobj);
    		counter_votes.write();
    	}
    }
   
    if(!(doincvote||doincvu||isdet))
    {
    	// affichage seulement pas de requete (optim)
		counter_affVuArt(idobj, null);
		counter_affVoteArt(idobj, null);
		return;
    }

	var xhr = null;
    
    if (window.XMLHttpRequest) 
    { 
        xhr = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) 
    {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
    else
    {
    	return false;
    }
    xhr.onreadystatechange = function() {
 		if (xhr.readyState == 4) { 
			var docXML = xhr.responseXML;
			counter_affVuArt(idobj, docXML.getElementsByTagName("vu").item(0).firstChild.data);
			counter_affVoteArt(idobj, docXML.getElementsByTagName("vote").item(0).firstChild.data);
 		}
    };
    var f='compteurget';
    if(doincvote||doincvu)
    {
    	f='compteur';
    }
    xhr.open("GET", xmlBaseUrl + "?w="+f+"&obj="+obj+"&id="+idobj+"&vote="+doincvote+"&vu="+doincvu+"&det="+isdet+'&'+Math.random(), true);
    xhr.send(null);
}

function counter_affVoteArt(idobj, vote)
{
	//var obj='article';
	var idvote='cntavo'+idobj;
	if(vote!==null) window.document.getElementById(idvote+'_nb').innerHTML = vote;
	if(counter_votes.isId(idobj))
	{
		window.document.getElementById(idvote+'_l').innerHTML='Vot�';
		window.document.getElementById(idvote+'_b').setAttribute("title", "Vous avez d�j� vot� pour cet article");
		window.document.getElementById(idvote+'_b').setAttribute((document.all ? "className" : "class"), "wo");
	}
}

function counter_affVuArt(idobj, vu)
{
	//var obj='article';
    var idvu='cntavu'+idobj;
	
	if(vu!==null) window.document.getElementById(idvu+'_nb').innerHTML=vu;
	if(counter_vus.isId(idobj))
	{
		window.document.getElementById(idvu+'_l').innerHTML='Vu';
		window.document.getElementById(idvu+'_b').setAttribute("title", "Vous avez d�j� lu cet article");
		window.document.getElementById(idvu+'_b').setAttribute((document.all ? "className" : "class"), "wu");
	}
}