/**
 * @author sangele
 */

var Onglet = Class.create();
Onglet.prototype = {
	listebtn:null,
	listeonglet:null,
	class_onglet:'x_actif',
	initialize: function(tabBtn,tabOnglet) {	
	this.listebtn = tabBtn;
	this.listeonglet =tabOnglet;

	var DelayedEventOnglet={
			handler:null,
			click :function(event,onglet,btn,ong){				
				onglet.affiche(btn,ong);
				Event.stop(event);
			}
		}	

	for(indexbtn=0;indexbtn<tabBtn.length;indexbtn++)
	{
		DelayedEventOnglet.handler = DelayedEventOnglet.click.bindAsEventListener(DelayedEventOnglet,this,tabBtn[indexbtn],tabOnglet[indexbtn])				
		Event.observe(tabBtn[indexbtn],"click",DelayedEventOnglet.handler);			
	}
	},
	affiche: function(btn,ong) {
		for(indexbtn=0;indexbtn<this.listebtn.length;indexbtn++)
		{
			this.listebtn[indexbtn].up().removeClassName(this.class_onglet)
		}
		for(indexbtn=0;indexbtn<this.listeonglet.length;indexbtn++)
		{
			this.listeonglet[indexbtn].hide()
		}	
		btn.up().addClassName(this.class_onglet);
		ong.show();		
	}

};

var OngletStyle = Class.create();
OngletStyle.prototype = {
	listebtn:null,
	listeonglet:null,
	initialize: function(tabBtn,tabOnglet) {	
		this.listebtn = tabBtn;
		this.listeonglet =tabOnglet;
		var DelayedEventOnglet={
			handler:null,
			click :function(event,onglet,btn,ong){				
				onglet.affiche(btn,ong);
			}
		}	

		for(indexbtn=0;indexbtn<tabBtn.length;indexbtn++)
		{
			DelayedEventOnglet.handler = DelayedEventOnglet.click.bindAsEventListener(DelayedEventOnglet,this,tabBtn[indexbtn],tabOnglet[indexbtn])				
			Event.observe(tabBtn[indexbtn],"click",DelayedEventOnglet.handler);			
		}
	},
	affiche: function(btn,ong) {
		for(indexbtn=0;indexbtn<this.listebtn.length;indexbtn++)
		{
			this.listebtn[indexbtn].up().removeClassName('x_cafe')
		}
		for(indexbtn=0;indexbtn<this.listeonglet.length;indexbtn++)
		{
			this.listeonglet[indexbtn].hide()
		}	
		btn.up().addClassName('x_cafe');
		ong.show();		
	}

};

