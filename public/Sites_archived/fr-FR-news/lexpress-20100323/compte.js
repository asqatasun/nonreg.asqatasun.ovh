/***********************
 * Cr�� le 31/07/2008 par S�bastien Ang�le
 * Modification :
 *
 */

var Compte = Class.create();

Compte.prototype = {
	//Formulaire � surveiller
	container_erreur:'compte_erreur',
	ancre_erreur:'form_inscription',
	btn_identification : 'btn_identification',	
	container_identification:'onglet-identifie',	
	btn_deconnexion : 'btn_deconnexion',
	btn_deconnexion2 : 'btn_deconnexion2',
	btn_deconnexionarticle : 'btn_deconnexionarticle',
	btn_preinscription : 'btn_preinscription',	
	container_inscription :'onglet-inscription',
	btn_devenir_membre : "btn_devenir_membre",
	btn_deja_inscrit : 'btn_dejainscrit',
	container_motdepasse: "container_motdepasse",
	btn_motdepasse : "btn_motdepasse",
	container_motdepasse_ok : "motdepasse_ok",
	container_motdepasse_erreur : "motdepasse_erreur",
	cookiename :'compte@newexpress',	
	url_inscription :'/membre/validation.asp',
	url_espaceperso :'/membre/accueil.asp',
	form_motdepasse : 'form_motdepasse',
	form_inscription :null,
	form_modification : null,	
	idcompte : -2,
	
	//Initialisation des �v�nements sur les formulaires et les boutons
	initialize: function()
	{				
	},
	//Permet d'ouvrir le calque de login en cliquant sur l'�l�ment d'ID btn_identification
	initBtnIdentification: function()
	{
		var btn = $(this.btn_identification);
		
		DelayedEvent={
			handler:null,
			click:function(event,compte,container){
				new Ajax.Request('/includes/compte/compte_identification.html',{
						method: 'get',
						postBody:'',
						requestHeaders:['Content-Type','application/x-www-form-urlencoded;charset=iso-8859-1'],
						encoding:'iso-8859-1',
						onSuccess: function(req){
							if(null!=$("inscription"))
							{								
								$("inscription").remove();
								compte.initBtnPreinscription();	
							}
							container.update(container.innerHTML+req.responseText);
							compte.initFormIdentification();
						},onFaillure: function(req){
							alert('erreur');
						}
					});//Ajax.Request								
			}
		}
		DelayedEvent.handler = DelayedEvent.click.bindAsEventListener(DelayedEvent,this,$(this.container_identification))
		Event.observe(btn,'click',DelayedEvent.handler,false);
				
	},
	initFormIdentificationPleinePage: function(form)
	{		
		if(null!=form)
		{			
			//Gestion du formulaire de login
			DelayedEvent={
				handler:null,
				submit:function(event,compte){
					Event.stop(event);
					var elem_error=$(Event.element(event)).getElementsByClassName('erreur')[0];
					var form=Event.element(event);
					var inputs_error;
					var json;
					inputs_error=checkForm(form);							
					if(inputs_error)
					{
						var theErrors = '<ul class="erreurcompte">'
						for(i=0;i<inputs_error.length;i++)
						{					
							json=check[inputs_error[i].name];
							theErrors = theErrors + "<li>" + json.info + '</li>';	
						}
						theErrors = theErrors + "</ul>"
						$(compte.container_erreur).update(theErrors);
					}
					else
					{
						if($(form['redirect']).getValue().length>0){
							compte.sendFormIdentification(form,compte,$(form['redirect']).getValue());					
						}else{
							compte.sendFormIdentification(form,compte,compte.url_espaceperso);					
						}
					}
				}
			}
			DelayedEvent.handler = DelayedEvent.submit.bindAsEventListener(DelayedEvent,this)
			Event.observe(form,'submit',DelayedEvent.handler,false);
			
		/*var btn = $(this.btn_devenir_membre+ '2');
		DelayedEvent={
			handler:null,
			click:function(event,compte,container){								
				compte.initBtnIdentification();	
				new Ajax.Request('/includes/compte/compte_preinscription.html',{
						method: 'get',
						postBody:'',
						requestHeaders:['Content-Type','application/x-www-form-urlencoded;charset=iso-8859-1'],
						encoding:'iso-8859-1',
						onSuccess: function(req){							
							container.update(container.innerHTML+req.responseText)													
							compte.initFormPreinscription()							
						},onFaillure: function(req){
							alert('erreur');
						}
					});//Ajax.Request					
			}
		}
		DelayedEvent.handler = DelayedEvent.click.bindAsEventListener(DelayedEvent,this,$(this.container_inscription))
		Event.observe(btn,'click',DelayedEvent.handler,false);*/	
		
		var btn = $(this.btn_motdepasse + '2');
		DelayedEvent={
			handler:null,
			click:function(event,compte){
				
				$(compte.container_motdepasse + '2').toggle();
				$(compte.container_motdepasse_ok + '2').hide()
				$(compte.container_motdepasse_erreur + '2').hide()									
			}
		}
		DelayedEvent.handler = DelayedEvent.click.bindAsEventListener(DelayedEvent,this)
		Event.observe(btn,'click',DelayedEvent.handler,false);	
		
		var formPassword = $(this.form_motdepasse+'2');
		DelayedEvent={
			handler:null,
			submit:function(event,compte){
				Event.stop(event);
				$(compte.container_motdepasse_erreur +'2').hide();
				$(compte.container_motdepasse_ok + '2').hide();
				new Ajax.Request('/includes/ajax/compte.asp',
				{
					method:'post',
					postBody:'action=6&email='+formPassword['email'].value,
					encoding:form.readAttribute('accept-charset'),
					onSuccess:function(req)
					{												
						var json=req.responseText.evalJSON();														
						if(json.etat==1)
						{										
							$(compte.container_motdepasse_ok +'2').show()																													
						}
						else
						{
							$(compte.container_motdepasse_erreur + '2').show()
						}										
					},
					onFaillure:function(req)
					{
										
					}
				});
			}
		}
		DelayedEvent.handler = DelayedEvent.submit.bindAsEventListener(DelayedEvent,this)
		if(null!=formPassword)
		{
			Event.observe(formPassword,'submit',DelayedEvent.handler,false);
		}			
		}		
	},	
	initFormIdentification: function()
	{
		var DelayedEvent;
		var DelayedEventFermeture;
				
		var form = $('form_identification');		
						
		//Fermeture du calque de login
		DelayedEventFermeture={
			handler:null,
			click:function(event,compte){						
				$("connexion").remove();				
				compte.initBtnIdentification();						
			}
		}
		DelayedEventFermeture.handler = DelayedEventFermeture.click.bindAsEventListener(DelayedEventFermeture,this);
		Event.observe($("fermer_connexion"),'click',DelayedEventFermeture.handler,false);		
		
		//Gestion du formulaire de login
		DelayedEvent={
			handler:null,
			submit:function(event,compte){
				Event.stop(event);
				var elem_error=$(Event.element(event)).select('.erreur')[0];
				var form=Event.element(event);
				var inputs_error;
				var json;
				inputs_error=checkForm(form);							
				if(inputs_error)
				{
					var theErrors = '<ul class="erreurcompte">'
					for(i=0;i<inputs_error.length;i++)
					{					
						json=check[inputs_error[i].name];
						theErrors = theErrors + "<li>" + json.info + '</li>';	
					}
					theErrors = theErrors + "</ul>"
					$(compte.container_erreur).update(theErrors);					
				}
				else
				{														
					compte.sendFormIdentification(form,compte,'');					
				}
			}
		}
		DelayedEvent.handler = DelayedEvent.submit.bindAsEventListener(DelayedEvent,this)
		if(null!=form)
		{
			Event.observe(form,'submit',DelayedEvent.handler,false);
		}

		DelayedEvent={
			handler:null,
			keydown:function(event){
				if(event.keyCode==13)
				{
					Event.stop(event);
				}
			}
		}
		DelayedEvent.handler = DelayedEvent.keydown.bindAsEventListener(DelayedEvent)
		if(null!=form)
		{
			Event.observe(form,'keydown',DelayedEvent.handler,false);
		}		
		
		var btn = $(this.btn_devenir_membre);
		DelayedEvent={
			handler:null,
			click:function(event,compte,container){
				$("connexion").remove();				
				compte.initBtnIdentification();	
				new Ajax.Request('/includes/compte/compte_preinscription.html',{
						method: 'get',
						postBody:'',
						requestHeaders:['Content-Type','application/x-www-form-urlencoded;charset=iso-8859-1'],
						encoding:'iso-8859-1',
						onSuccess: function(req){							
							container.update(container.innerHTML+req.responseText)													
							compte.initFormPreinscription()							
						},onFaillure: function(req){
							alert('erreur');
						}
					});//Ajax.Request					
			}
		}
		DelayedEvent.handler = DelayedEvent.click.bindAsEventListener(DelayedEvent,this,$(this.container_inscription))
		Event.observe(btn,'click',DelayedEvent.handler,false);	
		
		var btn = $(this.btn_motdepasse);
		DelayedEvent={
			handler:null,
			click:function(event,compte){
				
				$(compte.container_motdepasse).toggle();
				$(compte.container_motdepasse_ok).hide()
				$(compte.container_motdepasse_erreur).hide()									
			}
		}
		DelayedEvent.handler = DelayedEvent.click.bindAsEventListener(DelayedEvent,this)
		Event.observe(btn,'click',DelayedEvent.handler,false);	
		
		var formPassword = $(this.form_motdepasse);
		DelayedEvent={
			handler:null,
			submit:function(event,compte){
				Event.stop(event);
				$(compte.container_motdepasse_erreur).hide();
				$(compte.container_motdepasse_ok).hide();
				new Ajax.Request('/includes/ajax/compte.asp',
				{
					method:'post',
					postBody:'action=6&email='+formPassword['email'].value,
					encoding:form.readAttribute('accept-charset'),
					onSuccess:function(req)
					{												
						var json=req.responseText.evalJSON();														
						if(json.etat==1)
						{										
							$(compte.container_motdepasse_ok).show()																													
						}
						else
						{
							$(compte.container_motdepasse_erreur).show()
						}										
					},
					onFaillure:function(req)
					{
										
					}
				});
			}
		}
		DelayedEvent.handler = DelayedEvent.submit.bindAsEventListener(DelayedEvent,this)
		if(null!=formPassword)
		{
			Event.observe(formPassword,'submit',DelayedEvent.handler,false);
		}								
	},
	sendFormIdentification: function(form,compte,redirect)
	{				
			
		//On met un sablier au curseur le temps de la connexion
		var theBody = $$('body')[0]		
		theBody.toggleClassName('load')		
		new Ajax.Request('/includes/ajax/compte.asp',
		{
			method:'post',
			postBody:serializeForm(form),
			encoding:form.readAttribute('accept-charset'),
			onSuccess:function(req)
			{												
				var json=req.responseText.evalJSON();																		
				if(json.etat==1)
				{										
					if(json.cookie!='')
					{
						//On pose le cookie et on rafraichit la page							
						compte.writeCookie(compte.cookiename,json.cookie,compte,1)
					}										
					compte.idcompte = json.idcompte;																	
					compte.identificationSite(compte,form,redirect);																								
				}
				else
				{
					theBody.toggleClassName('load');
					$(compte.container_erreur).update(json.message);
				}										
			},
			onFaillure:function(req)
			{
				theBody.toggleClassName('load');	
				$('erreur_identification').update('Serveur indisponible, veuillez nous excuser de la g�ne occassionn�e.');				
			}
		});
		
	},
	identificationSite: function(compte,form,redirect)
	{		
		if(redirect!='')
		{
			window.location = redirect;
		}
		else
		{				
			theLocation = window.location.href			
			theIndexDiese = theLocation.lastIndexOf('#');
			if(theIndexDiese > -1)
			{
				theLocation = 	theLocation.substr(0,theIndexDiese-1);	
			}  					
			//window.location.href = theLocation;
			window.location.reload();
		}			
	},
	//Permet d'ouvrir le calque de login en cliquant sur l'�l�ment d'ID btn_identification
	initBtnDeconnexion: function()
	{
		var btn = $(this.btn_deconnexion);	
		var btn2 = $(this.btn_deconnexion2);		
		var btn3 = $(this.btn_deconnexionarticle);
		
		DelayedEvent={
			handler:null,
			click:function(event,compte){
				new Ajax.Request('/includes/ajax/compte.asp',{
						method: 'post',
						postBody:'action=4',
						requestHeaders:['Content-Type','application/x-www-form-urlencoded;charset=iso-8859-1'],
						encoding:'iso-8859-1',
						onSuccess: function(req){														
							compte.deleteCookie(compte.cookiename,compte);							
							compte.deconnexionSite(compte);	
						},onFaillure: function(req){
							alert('erreur');
						}
					});//Ajax.Request								
			}
		}
		if(null!=btn)
		{
			DelayedEvent.handler = DelayedEvent.click.bindAsEventListener(DelayedEvent,this)
			Event.observe(btn,'click',DelayedEvent.handler,false);		
		}
		
		if(null!=btn2)
		{
			DelayedEvent.handler = DelayedEvent.click.bindAsEventListener(DelayedEvent,this)
			Event.observe(btn2,'click',DelayedEvent.handler,false);				
		}
		
		if(null!=btn3)
		{
			DelayedEvent.handler = DelayedEvent.click.bindAsEventListener(DelayedEvent,this)
			Event.observe(btn3,'click',DelayedEvent.handler,false);				
		}			
	},	
	deconnexionSite: function(compte)
	{	
		theLocation = window.location.href;
		theIndexDiese = theLocation.lastIndexOf('#');
		if(theIndexDiese > -1)
		{
			theLocation = 	theLocation.substr(0,theIndexDiese-1);	
		} 
//Probleme de rechargement sur certaines pages avec certains navigateurs	
//		theLocation=theLocation+'?action=deconnexion';
//		alert(theLocation);
//		window.location.href = theLocation;	
		
		window.location.reload();
	},
	//Permet d'ouvrir le calque d'inscription en cliquant sur l'�l�ment d'ID btn_inscription
	initBtnPreinscription: function()
	{
		var btn = $(this.btn_preinscription);
		
		DelayedEvent={
			handler:null,
			click:function(event,compte,container){
				new Ajax.Request('/includes/compte/compte_preinscription.html',{
						method: 'get',
						postBody:'',
						requestHeaders:['Content-Type','application/x-www-form-urlencoded;charset=iso-8859-1'],
						encoding:'iso-8859-1',
						onSuccess: function(req){							
							if(null!=$("connexion"))
							{								
								$("connexion").remove();
								compte.initBtnIdentification();
							}							
							container.update(container.innerHTML+req.responseText)													
							compte.initFormPreinscription()							
						},onFaillure: function(req){
							alert('erreur');
						}
					});//Ajax.Request								
			}
		}
		DelayedEvent.handler = DelayedEvent.click.bindAsEventListener(DelayedEvent,this,$(this.container_inscription))
		Event.observe(btn,'click',DelayedEvent.handler,false);				
	},
	initFormPreinscription: function()
	{
		var DelayedEvent;

		var DelayedEventFermeture;
		
		var form = $("form_preinscription");
				
		//Fermeture du calque de login
		DelayedEventFermeture={
			handler:null,
			click:function(event,compte){				
				$("inscription").remove();				
				compte.initBtnPreinscription();						
			}
		}
		DelayedEventFermeture.handler = DelayedEventFermeture.click.bindAsEventListener(DelayedEventFermeture,this);
		Event.observe($("fermer_preinscription"),'click',DelayedEventFermeture.handler,false);	
				
		
		DelayedEvent={
			handler:null,
			submit:function(event,compte){														
				var form=Event.element(event);
				var inputs_error;
				var json;
				var elem;
				var elements;
				var i;
				
				elements=form.getElements();
				Event.stop(event);
				theErrors = "";
				$(compte.container_erreur).update(theErrors);
				inputs_error=checkForm(form);
				if(inputs_error)
				{										
					var theErrors = '<ul class="erreurcompte">'
					for(i=0;i<inputs_error.length;i++)
					{					
						json=check[inputs_error[i].name];
						theErrors = theErrors + "<li>" + json.info + '</li>';	
					}
					theErrors = theErrors + "</ul>"
					$(compte.container_erreur).update(theErrors);					
					
				}
				else
				{
					var booCheck=true;					
					if(form['password'].value!=form['confirmpassword'].value)
					{
						theErrors = "Votre mot de passe de confirmation diff�re de votre mot de passe.\nVeuillez ressaisir votre mot de passe de confirmation." + "<br/>";
						$(compte.container_erreur).update(theErrors);
						booCheck=false;
					}
					if(booCheck==true)
					{
						//V�rifier l'email d'inscription												
						new Ajax.Request('/includes/ajax/compte.asp',{
						method: 'post',
						postBody:'action=5&pseudo='+form['pseudo'].value + '&email='+form['email'].value,
						requestHeaders:['Content-Type','application/x-www-form-urlencoded;charset=iso-8859-1'],
						encoding:'iso-8859-1',
						onSuccess: function(req){																	
							var json=req.responseText.evalJSON();													
							if(json.etat==1)
							{					
								compte.preinscriptionSite(form,compte,event);
							}
							else
							{
								theErrors = json.message + "<br/>";
								$(compte.container_erreur).update(theErrors);
							}								 
						},onFaillure: function(req){
							alert('erreur');							
						}
						});//Ajax.Request														
					}
				}
			}
		}
		DelayedEvent.handler = DelayedEvent.submit.bindAsEventListener(DelayedEvent,this)
		Event.observe(form,'submit',DelayedEvent.handler,false);
	
		DelayedEvent={
			handler:null,
			keydown:function(event){
				if(event.keyCode==13)
				{
					Event.stop(event);
				}
			}
		}
		DelayedEvent.handler = DelayedEvent.keydown.bindAsEventListener(DelayedEvent)
		Event.observe(form,'keydown',DelayedEvent.handler,false);
					
		var btndejaninscrit = $(this.btn_deja_inscrit)	
		DelayedEvent={
			handler:null,
			click:function(event,compte,container){
				$("inscription").remove();				
				compte.initBtnPreinscription();							
				new Ajax.Request('/includes/compte/compte_identification.html',{
						method: 'get',
						postBody:'',
						requestHeaders:['Content-Type','application/x-www-form-urlencoded;charset=iso-8859-1'],
						encoding:'iso-8859-1',
						onSuccess: function(req){							
							container.update(container.innerHTML+req.responseText)													
							compte.initFormIdentification()							
						},onFaillure: function(req){
							alert('erreur');
						}
					});//Ajax.Request								
			}
		}
		DelayedEvent.handler = DelayedEvent.click.bindAsEventListener(DelayedEvent,this,$(this.container_identification))
		Event.observe(btndejaninscrit,'click',DelayedEvent.handler,false);		
	},
	preinscriptionSite: function(myform,compte,event)
	{		
		Event.stopObserving(myform,'submit',event.handler);		
		myform.submit();		
	},
	initFormInscription: function()
	{
		var DelayedEvent;
		
		var form = $('form_inscription');
		DelayedEvent={
			handler:null,
			submit:function(event,compte){
				Event.stop(event);
				var form=Event.element(event);
				var inputs_error;
				var json;
				var elem;
				var elements;
				var i;
				elements=form.getElements();
				
				//ecrire dans le champs cache date
				form['datenaissance'].value=form['jour'].getValue() + '/' + form['mois'].getValue() + '/' + form['annee'].getValue();
				inputs_error=checkForm(form);
				if(inputs_error)
				{
					var theErrors = '<ul class="erreurcompte">'
					for(i=0;i<inputs_error.length;i++)
					{					
						json=check[inputs_error[i].name];
						theErrors = theErrors + "<li>" + json.info + '</li>';	
					}
					theErrors = theErrors + "</ul>"
					$(compte.container_erreur).update(theErrors);					
					$(compte.ancre_erreur).scrollTo();
				}
				else
				{								
					if(form['password'].value!=form['confirmpassword'].value)
					{
						theErrors = '<ul class="erreurcompte"><li>Votre mot de passe de confirmation diff�re de votre mot de passe.\nVeuillez ressaisir votre mot de passe de confirmation.</li></ul>';											
						$(compte.container_erreur).update(theErrors);
						$(compte.ancre_erreur).scrollTo();																							
					}
					else
					{						
						if(form['cp'].getValue().length != 5 && form['pays'].getValue() == 72)
						{
							theErrors = '<ul class="erreurcompte"><li>Votre code postal n\'est pas valide pour la France.</li></ul>';											
							$(compte.container_erreur).update(theErrors);
							$(compte.ancre_erreur).scrollTo();										
						} 
						else
						{
						compte.sendFormInscription(form);
						}
					}
						
				}
			}
		}
		DelayedEvent.handler = DelayedEvent.submit.bindAsEventListener(DelayedEvent,this)
		Event.observe(form,'submit',DelayedEvent.handler,false);
	
		DelayedEvent={
			handler:null,
			keydown:function(event){
				if(event.keyCode==13)
				{
					Event.stop(event);
				}
			}
		}
		DelayedEvent.handler = DelayedEvent.keydown.bindAsEventListener(DelayedEvent)
		Event.observe(form,'keydown',DelayedEvent.handler,false);
	
	
		if($('optin-non') && $('optin-oui'))
		{
			DelayedEvent={
				handler:null,
				click:function(event,form){
					form['optin'].value=Event.element(event).value;
				}
			}
			DelayedEvent.handler = DelayedEvent.click.bindAsEventListener(DelayedEvent,form)
			Event.observe($('optin-non'),'click',DelayedEvent.handler,false);
			Event.observe($('optin-oui'),'click',DelayedEvent.handler,false);	
		}
		
		if($('civilite-m') && $('civilite-mme') && $('civilite-mlle'))
		{
			DelayedEvent={
				handler:null,
				click:function(event,form){
					form['civilite'].value=Event.element(event).value;
				}
			}
			DelayedEvent.handler = DelayedEvent.click.bindAsEventListener(DelayedEvent,form)
			Event.observe($('civilite-m'),'click',DelayedEvent.handler,false);
			Event.observe($('civilite-mme'),'click',DelayedEvent.handler,false);	
			Event.observe($('civilite-mlle'),'click',DelayedEvent.handler,false);			
		}		
	},
	sendFormInscription : function(form)
	{
		var compte = this;				
		new Ajax.Request("/includes/ajax/compte.asp",
		{
			method:'post',
			postBody:serializeForm(form),
			encoding:form.readAttribute('accept-charset'),
			onSuccess:function(req)
			{																
				var json=req.responseText.evalJSON();			
				if(json.etat==1 || json.etat==2)
				{
					compte.idcompte = json.idcompte;								
					compte.inscriptionSite(compte,form,json.etat);
				}
				else
				{
					theErrors = json.message											
					$(compte.container_erreur).update(theErrors);
					$(compte.ancre_erreur).scrollTo();					
				}
			},
			onFaillure:function (req)
			{				
				alert('Erreur serveur.!!!');
			}
		});
	},
	inscriptionSite: function(compte,form,etat)
	{		
		window.location = compte.url_inscription;
	},
	initFormModification: function(form,booPassword)
	{
		var DelayedEvent;
		
		DelayedEvent={
			handler:null,
			submit:function(event,compte){
				Event.stop(event);
				var form=Event.element(event);
				var inputs_error;
				var json;
				var elem;
				var elements;
				var i;

				inputs_error=checkForm(form);
				if(inputs_error)
				{
					var theErrors = '<ul class="erreurcompte">'
					for(i=0;i<inputs_error.length;i++)
					{					
						json=check[inputs_error[i].name];
						theErrors = theErrors + "<li>" + json.info + '</li>';	
					}
					theErrors = theErrors + "</ul>"
					$(compte.container_erreur).update(theErrors);					
					$(compte.ancre_erreur).scrollTo();
				}
				else
				{
					if(booPassword)
					{
						if(form['password'].value!=form['confirmpassword'].value)
						{
							theErrors = "Votre mot de passe de confirmation diff�re de votre mot de passe.\nVeuillez ressaisir votre mot de passe de confirmation." + "<br/>";											
							$(compte.container_erreur).update(theErrors);
							$(compte.ancre_erreur).scrollTo();																							
						}
						else
						{											
							compte.sendFormModification(form);
						}
					}
					else
					{
						compte.sendFormModification(form);
					}					
				}				
			}
		}
		DelayedEvent.handler = DelayedEvent.submit.bindAsEventListener(DelayedEvent,this)
		Event.observe(form,'submit',DelayedEvent.handler,false);
	
		DelayedEvent={
			handler:null,
			keydown:function(event){
				if(event.keyCode==13)
				{
					Event.stop(event);
				}
			}
		}
		
		DelayedEvent.handler = DelayedEvent.keydown.bindAsEventListener(DelayedEvent)
		Event.observe(form,'keydown',DelayedEvent.handler,false);
				
	},
	sendFormModification : function(form)
	{
		var compte = this;
		$("compte_modification_email_ok").hide();	
		new Ajax.Request("/includes/ajax/compte.asp",
		{
			method:'post',
			postBody:serializeForm(form),
			encoding:form.readAttribute('accept-charset'),
			onSuccess:function(req)
			{												
				var json=req.responseText.evalJSON();	
				if(json.etat==1)
				{
					compte.idcompte = json.idcompte;					
					compte.modificationSite(compte,form,json.etat);
				}
				else
				{
					theErrors = json.message;											
					$(compte.container_erreur).update(theErrors);
					$(compte.ancre_erreur).scrollTo();						
				}
			},
			onFaillure:function (req)
			{				
				theErrors = "Erreur inconnue, veuillez contacter le webmaster ou essayer de nouveau ult�rieurement.";											
				$(compte.container_erreur).update(theErrors);
				$(compte.ancre_erreur).scrollTo();						
			}
		});
	},
	modificationSite: function(compte,myform,etat)
	{
		
	},
	initFormAmis: function (form)
	{		
		DelayedEvent={
			handler:null,
			click:function(event,elem){
				elem.value=""
			}
		}

		var tabChamp = $$('.amis_prenom input');							
		for(i=0;i<tabChamp.length;i++)
		{	
			DelayedEvent.handler = DelayedEvent.click.bindAsEventListener(DelayedEvent,tabChamp[i]);
			Event.observe(tabChamp[i],'click',DelayedEvent.handler,false);
		}
		
		var tabChamp = $$('.amis_email input');							
		for(i=0;i<tabChamp.length;i++)
		{	
			DelayedEvent.handler = DelayedEvent.click.bindAsEventListener(DelayedEvent,tabChamp[i]);
			Event.observe(tabChamp[i],'click',DelayedEvent.handler,false);
		}
		
	},
	writeCookie: function (nom, valeur,compte,duree)
	{		
//		alert("ok1");
		if(duree == 1)
		{			
			var date = new Date();
	    	date.setTime(date.getTime() + (365*24*60*60*1000) );
			var expires = "expires="+date.toGMTString()+";"
		}
		else
		{
			var expires = ""
		}
		var thecookie = nom + "=" + escape(valeur)+ ";" + expires + "path=/;domain="+ compte.cookiedomaine;
//		alert("thecookie:"+ thecookie);
		document.cookie = thecookie ;
//		alert('apres cookie'); 
	},
	getCookieVal: function (offset)
	{
		var endstr=document.cookie.indexOf (";", offset);
		if (endstr==-1) endstr=document.cookie.length;
		return unescape(document.cookie.substring(offset, endstr)); 
	},
	readCookie: function(nom) 
	{
		var arg=nom+"=";
		var alen=arg.length;
		var clen=document.cookie.length;
		var i=0;
		while (i<clen)
		{
			var j=i+alen;
			if (document.cookie.substring(i, j)==arg) return getCookieVal(j);
			i=document.cookie.indexOf(" ",i)+1;
			if (i==0) break;
		}
		return null; 
	},
	deleteCookie: function (nom,compte)
	{
		var date=new Date;
		date.setFullYear(date.getFullYear()-1);
		expires = date.toGMTString();
		var thecookie = nom + "=;expires=" + expires + ";domain=" + compte.cookiedomaine;
		document.cookie = thecookie ; 
	}
}

var DelayedEventCompte;
var insCompte;

DelayedEventCompte={
	handler:null,
	load:function(event){
		Event.stop(event);
		insCompte = new Compte();		
	}
}
DelayedEventCompte.handler = DelayedEventCompte.load.bindAsEventListener(DelayedEventCompte)
Event.observe(window,'load',DelayedEventCompte.handler,false);