function load_bloc_depeche(){
	var DelayedEventDepechesPrecedentes={
		handler:null,
		click :function(event,btnNavigation){
		depeches_precedentes(btnNavigation);
		}
	}
	if($('depeche_page_precedent') )
	{
		DelayedEventDepechesPrecedentes.handler = DelayedEventDepechesPrecedentes.click.bindAsEventListener(DelayedEventDepechesPrecedentes,$('depeche_page_precedent'));
		Event.observe($('depeche_page_precedent'),'click',DelayedEventDepechesPrecedentes.handler,false);
			}
	var DelayedEventDepechesSuivantes={
		handler:null,
		click :function(event,btnNavigation){
		depeches_suivantes(btnNavigation);
		}
	}
	if( $('depeche_page_suivant'))
	{
		DelayedEventDepechesSuivantes.handler = DelayedEventDepechesSuivantes.click.bindAsEventListener(DelayedEventDepechesSuivantes,$('depeche_page_suivant'));
		Event.observe($('depeche_page_suivant'),'click',DelayedEventDepechesSuivantes.handler,false);
	}
	
	//$('depeche_page_txt').innerHTML = '1/' + nbMaxPage ;
	$('depeche_page_precedent').hide();

			
}
function depeches_precedentes()
{
	navigation_depeche(-1);
}
function depeches_suivantes()
{
	navigation_depeche(1);
}
function navigation_depeche(index)
{
	var i;
	var deb;
	var fin;
	var page;
	nbDepecheParPage = parseInt(nbDepecheParPage);
	nbMaxPage = parseInt(nbMaxPage);
	
	for (i=1; i <= nbDepecheParPage*nbMaxPage; i++){
		$('depeche_' + i).hide();
	}
	
	page = parseInt($('depeche_page').value) + index;

	deb = (page-1)*nbDepecheParPage   +1;
	fin = (page)*nbDepecheParPage  ;
	
	for (i=deb; i<=fin ; i++) {
		$('depeche_' +  i).show();
	}
	$('depeche_page').value = page;	
	$('depeche_page_suivant').show();
	$('depeche_page_precedent').show();
	if(parseInt($('depeche_page').value) == nbMaxPage)
		$('depeche_page_suivant').hide();
	if(parseInt($('depeche_page').value) == 1)
		$('depeche_page_precedent').hide();
	
	$('depeche_page_txt').innerHTML = page + '\/' + nbMaxPage ;
	
}