// Objet pour representer l onglet defilant avec ses parametres
function diaposOnglet(sIdBlocLien, sTag, sLibelleIdOnglet, sRotationActive)
{
  this.idBlocLien = sIdBlocLien;
  this.tag = sTag;
  this.libelleIdOnglet = sLibelleIdOnglet;
  this.dessus = false;
  this.actuelle = 0;
  this.precedente = 0;
  this.nbOnglets = 0;
  this.temps = 3000;
  this.rotationActive = sRotationActive;
  
  this.styleOngletSelected = "block";
  this.styleOngletNotSelected = "none";
  this.nomClasseOngletHover = "unOngletHover";
  this.nomClasseOngletNormal = "unOnglet";
  
  compteBlocOnglet(this);
}

function compteBlocOnglet(diaposOnglet) {
  diaposCourante = diaposOnglet;
	var navroot = document.getElementById(diaposOnglet.idBlocLien);
  if ( navroot ) {
	  var lis = navroot.getElementsByTagName(diaposOnglet.tag);
	  diaposOnglet.nbOnglets = lis.length;

  	for ( i = 0; i < lis.length; i++ ) {
  		  j= i+1;
        var hrefbloc = lis[i].href;
        chaineID = diaposOnglet.libelleIdOnglet + "Bloc"+ j +" = '"+lis[i].id+"';"
        eval(chaineID);
        // on affecte les evenement de la souris aux onglets
        var elt = document.getElementById(diaposOnglet.libelleIdOnglet + j);

        /** DOMMAGE cela marche sur FireFox mais pas sur IE
         * du coup on est oblige de le parametrer dans les blocs...         
        // On affecte les evenement de la souris aux images
        lis[i].setAttribute("onmouseout", "javascript:sortieImage(diaposCourante);");
        lis[i].setAttribute("onmouseover", "javascript:surImage(diaposCourante);");
                
        document.getElementById(diaposOnglet.libelleIdOnglet + j).setAttribute("onMouseout", "javascript:sortieOnglet(diaposCourante," + j +");");
        document.getElementById(diaposOnglet.libelleIdOnglet + j).setAttribute("onMouseOver", "javascript:surOnglet(diaposCourante,"+ j + ");");
        */
        document.getElementById(diaposOnglet.libelleIdOnglet + j).href=hrefbloc;
        lis[i].style.display=diaposOnglet.styleOngletNotSelected;        
    }
    afficherOnglet(diaposOnglet);
	}
}

function afficherOnglet(diaposOnglet)
{
   if (! diaposOnglet.dessus)
   {
     if (diaposOnglet.actuelle == 0) {
      diaposOnglet.precedente = diaposOnglet.nbOnglets;
     }
     else {
      diaposOnglet.precedente = diaposOnglet.actuelle;
     }
    diaposOnglet.actuelle = diaposOnglet.precedente + 1;
    
    if (diaposOnglet.actuelle > diaposOnglet.nbOnglets) diaposOnglet.actuelle = 1;
    diaporamaOnglet(diaposOnglet);
   }
   ongletCourant = diaposOnglet;
   if (diaposOnglet.rotationActive)
   {
     setTimeout("afficherOnglet(ongletCourant);",diaposOnglet.temps);
   }
}

function diaporamaOnglet(diaposOnglet)
{   
  diapoPrecedente = document.getElementById(eval(diaposOnglet.libelleIdOnglet + 'Bloc'+diaposOnglet.precedente)).style;
  diapoActuelle = document.getElementById(eval(diaposOnglet.libelleIdOnglet + 'Bloc'+diaposOnglet.actuelle)).style;

  diaposcourante = diaposOnglet;
  diapoActuelle.display=diaposcourante.styleOngletSelected;
  diapoPrecedente.display=diaposOnglet.styleOngletNotSelected;
  diapoPrecedente.zIndex='0';
  diapoActuelle.zIndex='99';
 
  document.getElementById(diaposOnglet.libelleIdOnglet+diaposOnglet.precedente).className = diaposOnglet.nomClasseOngletNormal;
  document.getElementById(diaposOnglet.libelleIdOnglet+diaposOnglet.actuelle).className = diaposOnglet.nomClasseOngletHover;

}

function montreDiapoOnglet(diaposOnglet, indexAAfficher)
{
  for ( iOnglet = 0; iOnglet < diaposOnglet.nbOnglets; iOnglet++ ) {
    jOnglet = iOnglet + 1;
    styleDiaposCourante = document.getElementById(eval(diaposOnglet.libelleIdOnglet + 'Bloc'+jOnglet)).style;
    if (indexAAfficher == jOnglet)
    {
      document.getElementById(diaposOnglet.libelleIdOnglet+jOnglet).className = diaposOnglet.nomClasseOngletHover;
      styleDiaposCourante.display=diaposOnglet.styleOngletSelected;
      styleDiaposCourante.zIndex='99';
    }
    else
    {
      document.getElementById(diaposOnglet.libelleIdOnglet+jOnglet).className = diaposOnglet.nomClasseOngletNormal;
      styleDiaposCourante.display= diaposOnglet.styleOngletNotSelected;
      styleDiaposCourante.zIndex='0';
    }
  }
}

function surOnglet(diaposOnglet, index)
{
  indexCourant = index;
  diaposOnglet.dessus=true;
  montreDiapoOnglet(diaposOnglet,indexCourant);
}

function sortieOnglet(diaposOnglet, index)
{
  indexCourant = index;
  diaposOnglet.actuelle = indexCourant;
  diaposOnglet.dessus=false;
}

function surImage(diaposOnglet)
{
  diaposOnglet.dessus=true;
}

function sortieImage(diaposOnglet)
{
  diaposOnglet.dessus=false;
}

function changeOpacOnglet(opacity, object)
{ 
// var object = document.getElementById(id).style; 
 object.opacity = (opacity / 100); 
 object.MozOpacity = (opacity / 100); 
 object.KhtmlOpacity = (opacity / 100); 
 object.filter = "alpha(opacity=" + opacity + ")"; 
}
