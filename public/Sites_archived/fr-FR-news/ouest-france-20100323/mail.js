/**
 * novembre 2008
 * t.chanoski
 * 
 * cette fondtion ne permet qu'un envoi ajax par page (prend le premier element
 * nomExp, emailExp..)
 * 
 * le formulaire peut etre affich� via une requete ajax, ce qui fait que plusieurs articles/etablissment/annonces... 
 * peuvent utiliser sur une meme page cette fonction.
 * 
 * la version avec le formulaire cach� dans la page continue de fonctionner!
 * 
 * requis : JQuery.js 
 * 
 * 
 */

function ajaxAfficheFormulaireAmi(vTypeObjet, vIdObjet, width, height){
	
	jQuery.post( "/ofm_lib/ajaxMail.php", { etape: 'formulaireAmi', type : vTypeObjet, id : vIdObjet}, 
	function(data){
		affichModal(data, width, height);
	});	
}

function ajaxAfficheFormulaireContacter(vTypeObjet, vIdObjet, width, height, vNomDest, vEmailDest){
    if (vEmailDest == ''){
    	alert("le responsable de cet \351tablissement n'a pas pr\351cis\351 d'adresse mail,\n nous ne pouvons pas vous mettre en relation.");
    	return false;
    } else {
	    jQuery.post( "/ofm_lib/ajaxMail.php", { etape: 'formulaireContacter', type : vTypeObjet, id : vIdObjet, nomDest:vNomDest, emailDest:vEmailDest }, 
	    function(data){
	        affichModal(data, width, height);
	    }); 
    }
}

function ajaxAfficheFormulairelinkBait(vTypeObjet, vIdObjet, width, height){
    	var vtitreEcard =document.getElementById('linkBait_Titre').value;
        jQuery.post( "/ofm_lib/ajaxMail.php", { etape: 'formulaireLinkBait', type : vTypeObjet, id : vIdObjet, titreEcard:vtitreEcard}, 
        function(data){
            affichModal(data, width, height);
        }); 
}
function ajaxEnvoiMail(typeObjet, idObjet){

    if (!nomExp) var nomExp =document.getElementById(typeObjet+'_nomExp').value;
    if (!emailExp) var emailExp = document.getElementById(typeObjet+'_emailExp').value;
    if (!nomDest) var nomDest = document.getElementById(typeObjet+'_nomDest').value;
    if (!emailDest) var emailDest = document.getElementById(typeObjet+'_emailDest').value;
    if (!titreEcard) var titreEcard = document.getElementById(typeObjet+'_titreEcard').value;
    if (!messageEcard) var messageEcard = document.getElementById(typeObjet+'_messageEcard').value;
    
    var urlCourante = document.location;

// verification des infos :
    if ((!nomExp) || (!emailExp) || (!nomDest) || (!emailDest) || (!titreEcard) || (!messageEcard)) {
        alert('Veuillez v\351rifiez les donn\351es saisies, elles semblent erron\351es ou vides');
        return false;
    }
    if ( ! testEMail(emailExp) ||  ! testEMail(emailDest)){
        alert('Veuillez v\351rifiez les adresses mails saisies, elles semblent erron\351es');
        return false;
    }

    jQuery.post( "/ofm_lib/ajaxMail.php", {type : typeObjet, id:idObjet,nomExp:nomExp, emailExp:emailExp,
		nomDest:nomDest,emailDest:emailDest,titreEcard:titreEcard,messageEcard:messageEcard,urlCourante:urlCourante}, 
	    function(data){
	        if (data != ""){
	        	alert('Votre information a bien \351t\351 envoy\351e');
	            tb_remove();
	            if (typeof(redirection) != "undefined" && redirection!=""){
	                window.location.replace(redirection);
	            }            
	        } else {
	            alert("Un probleme est apparu lors de l'envoi : \n" + "veuillez recommencer l'operation");
	            MailOK(false);
	        }
	    }); 
}

  function MailOK(val){
    alert (val);
    return val;
} 

function alertValidMail(value)
{
	if(testEMail(value))
	{
		return true;
	}
	else
	{
		alert("L'email saisi est incorrect.");
	}
}

/**
 * renvoie true si le mail est bon
 */
function testEMail(adresse){
  var maReg = new RegExp("^[a-zA-Z0-9][a-zA-Z0-9_\\.-]*@(?:[a-zA-Z0-9][a-zA-Z0-9-]*\\.)*[a-zA-Z0-9][a-zA-Z0-9-]*\\.[a-zA-Z]{2,5}$");
  if (adresse.search(maReg) == -1 ){
    return (false);
  } else {
    return (true);
  }
}