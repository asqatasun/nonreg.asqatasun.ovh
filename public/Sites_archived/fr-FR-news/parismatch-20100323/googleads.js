google_ad_client = 'pub-8819602504296048';
google_ad_output = 'js';
google_item_width = '';//Math.floor(100/google_max_num_ads) - 3;
google_ad_type = 'text';
google_language = 'fr';
google_encoding = 'utf8';
google_safe = 'high';

function google_ad_request_done(google_ads) {
	var s = '', i;

	if (google_ads.length == 0) return;
	
	if (google_ads[0].type == "text") {
		
		if(google_ad_direction == 'scanad'){
			s += '<div class="headline googlads scanad">' + google_link + '<ul>';
		}
		else if(google_ad_direction == 'vert'){
			s += google_link + '<div class="googlads ' + google_ad_direction + '"><ul>';	
		}
		else{
			s += google_link + '<div class="headlines googlads ' + google_ad_direction + '"><ul>';	
		}
		
		for(i=0; i < google_ads.length; ++i) {
			s += '<li';
			if (i == 0){	s += ' class="prime"';	}

			s += ' style="width:' + google_item_width + '%">'
				+ '<strong><a href="' + google_ads[i].url + '" target="_blank" ' 
				+ 'onmouseout="window.status=\'\'" ' 
				+ 'onmouseover="window.status=\'go to ' 
				+ google_ads[i].visible_url
				+ '\';return true;" ' 
				+ 'style="text-decoration:none">' 
				+ google_ads[i].line1
				+ '</a></strong><p>'
				+ google_ads[i].line2
				+ '<br />'
				+ google_ads[i].line3
				+ '</p>';

			s += '<a class="adlink" href="' + google_ads[i].url + '" ' +
				'onmouseout="window.status=\'\'" ' +
				'onmouseover="window.status=\'go to ' +
				google_ads[i].visible_url + '\';return true;">' +
				google_ads[i].visible_url + '</a></li>';
        }
        
        s += '<li class="clad"></li></ul></div>';

    }
	document.write(s);
	
	return;
};	