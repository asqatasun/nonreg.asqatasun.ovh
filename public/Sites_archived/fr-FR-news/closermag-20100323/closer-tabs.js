$(document).ready(function() {

   // Home page - Block Photos de stars
   // Gestion des onglets
   $('#starpicts_tab_picts').click(function(){
      $(this).addClass('selected');
      $('#starpicts_tab_duel').removeClass('selected');
      $('#starpicts_duel').css({display:"none"});
      $('#starpicts_pictures').css({display:"block"});
   });
   $('#starpicts_tab_duel').click(function(){
      $('#starpicts_tab_picts').removeClass('selected');
      $('#starpicts_tab_duel').addClass('selected');
      $('#starpicts_pictures').css({display:"none"});
      $('#starpicts_duel').css({display:"block"});
   });
   
   // Home page - Block Video et son
   // Gestion des onglets
   $('#soundvideos_tab_tv').click(function(){
      $(this).addClass('selected');
      $('#soundvideos_tab_radio').removeClass('selected');
      $('#soundvideos_tv').css({display:"block"});
      $('#soundvideos_radio').css({display:"none"});
   });
   $('#soundvideos_tab_radio').click(function(){
      $(this).addClass('selected');
      $('#soundvideos_tab_tv').removeClass('selected');
      $('#soundvideos_radio').css({display:"block"});
      $('#soundvideos_tv').css({display:"none"});
   });
   
   // Home page - Block Closer Style
   // Gestion des onglets
   $('.block_style .blackbutton a').click(function(){
      $('.block_style .blackbutton').removeClass('selected');
      $(this).parent().addClass('selected');
      var lookclass = extractClass($(this).parent(), 'look', null);
      $('.block_style .portrait').removeClass('selected');
      $('.block_style .portrait.' + lookclass).addClass('selected');
   });
   
   // Pages Vecus - Block forum histoires vecues
   // Gestion des onglets
   $('.block_forumvecu .header .tab').click(function(){
      var children = $(this).parent().children();
      children.removeClass('tabsel');
      var tabs = $(this).parent().parent().find('.topics .tab');
      tabs.removeClass('tabsel');
      $(this).addClass('tabsel');
      for (i = 0; i < children.length; ++i) {
         if (children[i] == this) {
            $(tabs[i]).addClass('tabsel');
         }
      }
   });

});
