//////////////////////
// PRELOADIMAGES //
/////////////////////
function preloadImages() {
	var d=document; 
	if(d.images){ 
		if(!d.PrLD) 
			d.PrLD=new Array();
		var i, 
		j=d.PrLD.length,
		Imgs=preloadImages.arguments; 
		for(i=0; i<Imgs.length; i++)
			if (Imgs[i].indexOf("#")!=0){ 
				d.PrLD[j]=new Image; 
				d.PrLD[j++].src=Imgs[i];
			}
	}
}
//////////////////////////
// FIN PRELOADIMAGES //
/////////////////////////
