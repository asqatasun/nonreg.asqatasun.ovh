User-Agent: *
Allow: /

Disallow: *.ico$
Disallow: *.exe$
Disallow: *.js$
Disallow: *.css$
Disallow: *.zip$
Disallow: *.xls$
Disallow: /user/classeur/
Disallow: /Special_Nautisme/
Disallow: /contact/
Disallow: /data/
Disallow: /edito/
Disallow: /impression/
Disallow: /html/conditions/infos_abos_boutique.jsp
Disallow: /store/
Disallow: /html/abonnements/
Disallow: /search/recherche/
Disallow: /nominations/
Disallow: /nominations/2038/sommaire/
Disallow: /content/societe/list_commentary?idDoc=355397&url=%2Fcontent%2Factualites-societe%2Finfo-lepoint-fr-la-grande-loge-nationale-francaise-fait-son%2F920%2Farticle.html%3Fid%3D355397&submitForm=0
Disallow: /actualites-societe/2008-10-13/moeurs-ces-pretres-qui-disent-oui/920/0/281176
Disallow: /actualites-politique/2009-10-15/l-elysee-paie-43-500-euros-mois-au-specialiste-des-sondages/917/0/386162
Disallow: /actualites-politique/2009-10-15/l-elysee-paie-43-500-euros-mois-le-specialiste-des-sondages/917/0/386193
Disallow: /actualites-economie/2009-10-09/feu-vert-federal-des-etats-unis-a-l-accord-edf-constellation/916/0/384547
Disallow: /culture/2009-11-03/document-regardez-quand-levi-strauss-se-confiait-face-a-la-camera/249/0/391456
Disallow: /actualites-medias/2009-10-29/morandini-condamne/1253/0/390176
Disallow: /actualites-medias/2009-10-23/guerre-des-audiences-le-site-de-morandini-condamne-pour-concurrence-deloyale-au/1253/0/387137
Disallow: /actualites-medias/2010-01-04/tf1-martin-bouygues-se-separe-completement-de-patrick-le-lay/1253/0/410243
Disallow: /actualites-politique/1996-07-27/ajaccio-l-heure-de-l-addition/917/0/105382
Disallow: /actualites-societe/2007-01-19/banlieue-l-equipee-meurtriere-de-vitry-sur-seine/920/0/45818
Disallow: /actualites-monde/2010-03-22/services-secrets-deballages-entre-amis-a-la-dgse/1648/0/436314

Sitemap: http://www.lepoint.fr/sitemap.xml