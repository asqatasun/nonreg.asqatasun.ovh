<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/global_shop.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<title>Le Patriote Beaujolais : Journal d'informations locales r�gion beaujolaise.</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="description" content="Bienvenue sur notre site." />
<meta name="keywords" content="" />
<link href="http://wwwdata.appli-box.com/styles/shop/message_1-16-2.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="http://wwwdata.appli-box.com/styles/shop/jquery.lightbox-0.5.css" type="text/css" media="screen" />
<link href="global_100201.css" rel="stylesheet" type="text/css" />
<!-- InstanceBeginEditable name="head" -->
<link href="accueil.css" rel="stylesheet" type="text/css" />
<!-- InstanceEndEditable -->
<!--[if IE ]>
<link href="/css/globalIE.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!--[if lt IE 7]>
<link href="/css/globalIE6_090512.css" rel="stylesheet" type="text/css" />
<![endif]-->
<link href="userEdit.css" rel="stylesheet" type="text/css" />
<link href="print.css" rel="stylesheet" type="text/css" media="print" />

</head>
<body>
<div id="fond">
  <div id="bloc">
    <div id="blocLeft">
      <div id="selectionUnes">
        <ul class="monthList">
          <li class="month" id="janvier">1</li>
          <li class="month" id="fevrier">2</li>
          <li class="month" id="mars">3</li>
          <li class="month" id="avril">4</li>
          <li class="month" id="mai">5</li>
          <li class="month" id="juin">6</li>
          <li class="month" id="juillet">7</li>
          <li class="month" id="aout">8</li>
          <li class="month" id="septembre">9</li>
          <li class="month" id="octobre">10</li>
          <li class="month" id="novembre">11</li>
          <li class="month" id="decembre">12</li>
        </ul>
        <ul id="years">
          <li class="title"></li>
          <li id="nextYearButton"><img src="calendrier_flecheD.gif" alt="Suivant" /></li>
          <li id="previousYearButton"><img src="calendrier_flecheG.gif" alt="Pr�c�dent" /></li>
          <li id="previousYear"></li>
          <li id="year"></li>
          <li id="nextYear"></li>
        </ul>
        <div id="backnumber">Recherche
          <form action="index.php" method="get" name="searchForm" id="searchForm" class="searchForm">
            <input type="hidden" name="object[]" value="category" />
            <input type="hidden" name="object[]" value="article" />
            <input type="hidden" name="page" value="shop/search" />
            <input name="keyword" type="text" id="keyword" value="Chercher" size="12" class="formText" onfocus="if(this.value=='Chercher')this.value='';" />
            <input type="submit" value="OK" class="formBouton" onmouseover="this.className='formBoutonOver'" onmouseout="this.className='formBouton'" />
          </form>
        </div>
        <div id="imagesContains"></div>
      </div>
    </div>
    <div id="blocRight">
      <div id="header">
        <div id="logo"><a href="index.php"><img src="logo.gif" alt="ACCUEIL" /></a></div>
        <ul id="menuTop">
          	<li class="menuLevel0 menuNumInLevel1 menuLevelNumInLevel01"><a href="http://www.lepatriote.fr/accueil/le-journal">Le journal</a>	</li>
	<li class="menuLevel0 menuNumInLevel2 menuLevelNumInLevel02"><a href="http://www.lepatriote.fr/accueil/lequipe">L��quipe</a>	</li>
	<li class="menuLevel0 menuNumInLevel3 menuLevelNumInLevel03"><a href="http://www.lepatriote.fr/accueil/les-correspondants">Les correspondants</a>	</li>
	<li class="menuLevel0 menuNumInLevel4 menuLevelNumInLevel04"><a href="http://www.lepatriote.fr/accueil/nos-partenaires">Nos partenaires</a>	</li>
        </ul>
        <div class="annoncesLegales"><img src="annoncesLegales.gif" alt="Annonces Legales" /></div>
      </div>
      <div id="columnRight">
        <h2 class="columnRightTitle">et aussi</h2>
        <ul id="articleList">
          <li title="Le projet du multiplexe cin�matographique est pass� dans beaucoup de mains de promoteurs qui n'ont pas pu mener le projet. L'ADECSE,
Association de diffusion et d'exploitation cin�matographique du Sud-Est ne baisse pas les bras et veut g�rer maintenant seule le projet. Questions � Rodolphe Donati, directeur des cin�mas Rex et 400 Coups, partie prenante dans ce projet.  (photo : Franck Chapolard)" class="article_1">
            <h2><a href="http://www.lepatriote.fr/accueil/cinema-multiplexe-il-faudra-attendre">Cin�ma multiplexe : il faudra attendre</a></h2>
            Le projet du multiplexe cin�matographique est pass� dans beaucoup de mains de promoteurs qui n'ont pas pu mener le projet. L'ADECSE,
Association de diffusion et d'exploitation cin�matographique du Sud-Est ne baisse pas les bras et veut g�rer maintenant seule le projet. Questions � Rodolphe Donati, directeur des cin�mas Rex et 400 Coups, partie prenante dans ce projet.  (photo : Franck Chapolard)</li><li title="La ville pourrait accueillir en septembre 2010 une antenne du CFA du b�timent et travaux publics de Dardilly, compl�tement satur�. A condition que les financeurs du projet, la R�gion Rh�ne-Alpes en t�te, acceptent enfin de mettre la main au portefeuille." class="article_2">
            <h2><a href="http://www.lepatriote.fr/accueil/saint-georges-de-reneins-ca-bloque-toujours-pour-le-centre-de-formation-des-apprentis">Saint-Georges-de-Reneins�: �a bloque toujours pour le centre de formation des apprentis</a></h2>
            La ville pourrait accueillir en septembre 2010 une antenne du CFA du b�timent et travaux publics de Dardilly, compl�tement satur�. A condition que les financeurs du projet, la R�gion Rh�ne-Alpes en t�te, acceptent enfin de mettre la main au portefeuille.</li><li title="Paul Bacot est professeur de science politique � l'IEP (Institut d'�tudes politiques) de Lyon. Il est l'auteur aussi d'un hors-s�rie du quotidien Le Monde intitul� "Connaissez-vous la politique�?". Il analyse les r�sultats du premier tour et donne son sentiment en pr�lude
au second tour dimanche." class="article_3">
            <h2><a href="http://www.lepatriote.fr/accueil/la-crise-viticole-y-est-pour-quelque-chose">"La crise viticole y est pour quelque chose"</a></h2>
            Paul Bacot est professeur de science politique � l'IEP (Institut d'�tudes politiques) de Lyon. Il est l'auteur aussi d'un hors-s�rie du quotidien Le Monde intitul� "Connaissez-vous la politique�?". Il analyse les r�sultats du premier tour et donne son sentiment en pr�lude
au second tour dimanche.</li><li title="" class="article_4">
            <h2><a href="http://www.lepatriote.fr/accueil/la-mediatheque-baisse-ses-tarifs-et-amplifie-ses-activites">La m�diath�que baisse ses tarifs et amplifie ses activit�s</a></h2>
            </li><li title="" class="article_5">
            <h2><a href="http://www.lepatriote.fr/accueil/rallye-des-vignes-le-plein-pour-un-cru-prometteur">Rallye des vignes :  le plein pour un cru prometteur</a></h2>
            </li><li title="" class="article_6">
            <h2><a href="http://www.lepatriote.fr/accueil/grosse-affluence-a-la-presentation-du-velo-club-caladois">Grosse affluence � la pr�sentation  du V�lo Club Caladois</a></h2>
            </li>        </ul>      </div>
      <div id="mainContainer">
        <div id="encartAbonnement"><a href="http://www.lepatriote.fr/plus/abonnement"><img src="abonnement.gif" alt="ABONNEZ-VOUS" /></a></div>
        <p class="kiosque">En kiosque aujourd�hui</p>
        <div id="kiosqueImage"><img src="headerImage.jpg" alt="Le Patriote" /></div>
        <div id="mainContent">
          
          <!-- InstanceBeginEditable name="mainContent" -->
          <div>
            <div class="une_1">
              <h1><a href="http://www.lepatriote.fr/accueil/cinema-multiplexe-il-faudra-attendre">Cin�ma multiplexe : il faudra attendre</a></h1>
              <div id="articleFullImage"><img src="1268901649DSC_0019.jpg" width="585" alt="Cin�ma multiplexe : il faudra attendre" /></div>              Le projet du multiplexe cin�matographique est pass� dans beaucoup de mains de promoteurs qui n'ont pas pu mener le projet. L'ADECSE,
Association de diffusion et d'exploitation cin�matographique du Sud-Est ne baisse pas les bras et veut g�rer maintenant seule le projet. Questions � Rodolphe Donati, directeur des cin�mas Rex et 400 Coups, partie prenante dans ce projet.  (photo : Franck Chapolard)</div><div class="une_2">
              <h1><a href="http://www.lepatriote.fr/accueil/saint-georges-de-reneins-ca-bloque-toujours-pour-le-centre-de-formation-des-apprentis">Saint-Georges-de-Reneins�: �a bloque toujours pour le centre de formation des apprentis</a></h1>
                            La ville pourrait accueillir en septembre 2010 une antenne du CFA du b�timent et travaux publics de Dardilly, compl�tement satur�. A condition que les financeurs du projet, la R�gion Rh�ne-Alpes en t�te, acceptent enfin de mettre la main au portefeuille.</div><div class="une_3">
              <h1><a href="http://www.lepatriote.fr/accueil/la-crise-viticole-y-est-pour-quelque-chose">"La crise viticole y est pour quelque chose"</a></h1>
                            Paul Bacot est professeur de science politique � l'IEP (Institut d'�tudes politiques) de Lyon. Il est l'auteur aussi d'un hors-s�rie du quotidien Le Monde intitul� "Connaissez-vous la politique�?". Il analyse les r�sultats du premier tour et donne son sentiment en pr�lude
au second tour dimanche.</div><div class="une_4">
              <h1><a href="http://www.lepatriote.fr/accueil/la-mediatheque-baisse-ses-tarifs-et-amplifie-ses-activites">La m�diath�que baisse ses tarifs et amplifie ses activit�s</a></h1>
                            </div><div class="une_5">
              <h1><a href="http://www.lepatriote.fr/accueil/rallye-des-vignes-le-plein-pour-un-cru-prometteur">Rallye des vignes :  le plein pour un cru prometteur</a></h1>
                            </div><div class="une_6">
              <h1><a href="http://www.lepatriote.fr/accueil/grosse-affluence-a-la-presentation-du-velo-club-caladois">Grosse affluence � la pr�sentation  du V�lo Club Caladois</a></h1>
                            </div>          </div>          <!-- InstanceEndEditable -->
          <p class="copyright">Toute reproduction, modification, publication, totale ou partielle du site ou de son contenu est interdite.</p>
        </div>
      </div>
      <div id="pubBottom"><img src="pub.gif" alt="PUB" /></div>
      <div class="bar"></div>
      <div id="footerLinks"> <a href="http://rhone-alpes-auvergne.france3.fr/dossiers/30017398-fr.php" class="popup">Le Patriote sur France 3</a> <a href="http://www.lepatriote.fr/plus/nous-contacter">Contact</a><a href="http://www.lepatriote.fr/plus/archives">Archives</a><a href="http://www.lepatriote.fr/plus/mentions-legales">Mentions l�gales</a></div>
    </div>
    <ul class="annoncesLegalesTexteContainer">
      <li id="annoncesLegalesTexteTitre">Contact</li>
      <li id="annoncesLegalesTexte">Jo�lle LAVAIVRE<br />
        <a href="mailto:legales@lepatriote.com">legales@lepatriote.com</a><br />
        <br />
        Le Patriote est habilit� pour publier les annonces judiciaires et l�gales pour l�ensemble du d�partement du Rh�ne. Elles sont re�ues � nos bureaux jusqu�au mercredi 10 h de chaque semaine pour parution le jeudi. </li>
    </ul>
  </div>
  <div id="credit"> <a href="http://www.lepatriote.fr/index.php?login=1&amp;">| Connexion |</a>&nbsp;&nbsp;&nbsp;<a href="http://www.appli-box.com" title="Cr�ation de sites internet Lyon Villefranche" class="popup">&copy; Cr�ation du site Internet et r�f�rencement : Appli-Box</a></div>
</div>
<!--[if lt IE 7]>
<script language="JavaScript" src="http://wwwdata.appli-box.com/scripts/minmax.js" type="text/JavaScript"></script>
<script language="JavaScript" src="http://wwwdata.appli-box.com/scripts/unitpngfix.js" type="text/JavaScript"></script>
<![endif]-->
<!-- � utiliser jquery version 1.2.6 � la place de la derni�re version(1.3.1) pour jquery.searchField.js (formulaire de contact, texte d'exemple)-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.1/jquery.min.js" type="text/javascript"></script>
<script src="jquery.lightbox-0.5.js" type="text/javascript"></script>
<script language="JavaScript" src="selectionDesUnes.js" type="text/JavaScript"></script>
<!-- InstanceBeginEditable name="footer_script" -->
<script type="text/javascript" src="shopCategory_090512.js"></script>
<!-- InstanceEndEditable --> 
<!-- 
shop/index
63341ed379b5f873f90200bea17fa9a4
ebox_patriote
ebox2-1.16.8
shop/category/Accueil.htm - 
-->
</body>
<!-- InstanceEnd --></html>
<!-- 
shop/index
ebox_patriote
ebox2-1.16.8 -->