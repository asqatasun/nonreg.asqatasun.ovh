// This function opens a centered pop-up window
	function openMe(url,wname,swidth,sheight,scroll){
		topPos = (screen.height - sheight) / 2;
		topPos = parseInt(topPos);
		leftPos = (screen.width - swidth) / 2;
		leftPos = parseInt(leftPos);
		features = 'top=' + topPos + ', left=' + leftPos + ', height=' + sheight + ', width=' + swidth + ', resizable=0, scrollbars=' + scroll + ', statusbar=0';
		v = window.open(url, wname, features);
		v.focus();
	}

//Form Validator functions
// Checks if the variable is empty
function empty(variable) {
	if (variable == '') {return true;}
	return false;
}

// Checks if the variable contains spaces
function spaces(variable) {
	var espacio = " \n\t\r";
	var i;
	for (i = 0; i < variable.length; i++) {
		var car = variable.substring (i, i+1);
		if (espacio.indexOf(car) != -1) return true;
	}
	return false;
}

function emailOK(dir) {
	//checks if email is empty or has spaces
	if(empty(dir)) return false;
	if(spaces(dir)) return false;

	var i = 1;
	var dirLength = dir.length;
	var fuera = false;
	var car = "";

	// looking for @
	while (!fuera) {
		if(i < dirLength) {
			car =  dir.substring (i, i+1);
			if(car == "@") fuera=true;
		} else { fuera = true }
		i++;
	}

	if (i >= dirLength) { return false; }
	if (car != "@") { return false; }
	i++;

    // looking for .
	fuera = false;
	car = "";
	while (!fuera) {
		if(i < dirLength) {
			car =  dir.substring (i, i+1);
			if(car == ".") fuera=true;
		} else { fuera = true }
		i++;
	}
	if (i >= dirLength) { return false; }
	if (car != ".") { return false; }
	return true;
}

function checkIt(){
	// Ajustamos a verdaderas las variables de los campos a comprobar, cualquier fallo en la comprobaci�n las vuelve falsas y ya el formulario no ser� correcto
	OKform=true;
	OKemail=true;
	OKpass=true;
	OKpersonaldata=true;

	//bucle de comprobacion de datos personales (s�lo comprueba que no est� vacio)
	personalDataId= new Array('title','day','month','year','gender','fname','lname','address1','address2','town','state','postalcode','country');
	for (i = 0; i < personalDataId.length; i++) {
	//comprueba si est� vacio
		if (empty(document.getElementById(personalDataId[i]).value)){
		//si est� vacio resalta el campos y pone la variable a false
			document.getElementById(personalDataId[i]).style.background='#FBDE4A';
			OKpersonaldata=false;
		} else {
		//si no est� vacio corrige el resaltado (si lo hubiera)
			document.getElementById(personalDataId[i]).style.background='#ffffff';
		}
	}

	// Comprueba que el email introducido es v�lido
	if (!emailOK(document.getElementById("email").value)){
		OKemail=false;
	}

	// Comprueba que los campos de clave cumplan los requisitos m�nimos
	// No puede estar vacio
	if ((empty(document.getElementById("pass").value)) || (empty(document.getElementById("passConf").value))){
		//alert('Empty');
		OKpass=false;
	}

	// No puede tener espacios
	if ((spaces(document.getElementById("pass").value)) || (spaces(document.getElementById("passConf").value))){
		//alert('Has spaces');
		OKpass=false;
	}

	// La clave tiene que tener m�s de 6 caracteres
	if ((document.getElementById("pass").value.length<6) || (document.getElementById("passConf").value.length<6)){
		//alert('Length < 6');
		OKpass=false;
	}

	// La clave y la confirmaci�n tienen que ser iguales
	if (!(document.getElementById("pass").value==document.getElementById("passConf").value)){
		//alert('Pass do not match');
		OKpass=false;
	}

	//Comprueba si todos los campos est�n correctos
	if (!OKpass || !OKemail || !OKpersonaldata){
		OKform=false;
	}


	if(!OKform){//Lanza un alert si el formulario no est� correctamente rellenado
		alert('Please check the selected fields');

		//Resalta (y borra) los campos de clave si esta no es correcta, adem�s muestra u oculta una notificaci�n en pantalla
		if (!OKpass){
			document.getElementById("pass").value='';
			document.getElementById("pass").className='passWrong';
			document.getElementById("passConf").value='';
			document.getElementById("passConf").className='passWrong';
			document.getElementById("passAlert").className='alertOn';
		} else {
   			document.getElementById("pass").className='pass';
			document.getElementById("passConf").className='pass';
			document.getElementById("passAlert").className='alertOff';
		}

		//Resalta o no el campo email si este no es v�lido
		if (!OKemail){
			document.getElementById("email").value='Introduce un email valido';
			document.getElementById("email").className='emailWrong';
		} else {
			document.getElementById("email").className='email';
		}

		return false;
	} else { //Aplica los estilos correctos y env�a el formulario
        document.getElementById("pass").className='pass';
		document.getElementById("passConf").className='pass';
		document.getElementById("passAlert").className='alertOff';
		document.getElementById("email").className='email';
		//alert('Correcto');
		document.forms.joinUs.action='about-you.html';
		return true;
	}
}