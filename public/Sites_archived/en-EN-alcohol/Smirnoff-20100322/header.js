(function($) { // start anonymous function so we can use jQuery as $
	REGISTRATION_URL = '/dialogs/dialog_register.aspx?lang=' + languageId;
	REGISTRATION_PAGEONE_URL = '/dialogs/dialog_register_display.aspx?lang=' + languageId;
	REGISTRATION_PAGETWO_URL = '/dialogs/dialog_register_display_step2.aspx?lang=' + languageId;
	REGISTRATION_PAGETHREE_URL = '/dialogs/dialog_register_display_step3.aspx?lang=' + languageId;
	LOGIN_URL = '/dialogs/login.aspx?lang=' + languageId;
	LOGIN_PAGE_URL = '/dialogs/dialog_login_display.aspx?lang=' + languageId;
	LOGIN_PAGETWO_URL = '/dialogs/dialog_login_display_step2.aspx?lang=' + languageId;
	LOGOUT_URL = '/templates/logout.aspx?lang=' + languageId;
	

	var D = Smirnoff.UI.Dialogs,
		RD = Smirnoff.UI.RegistrationDialog;
	

	$(document).ready(function() {
		Smirnoff.DOM.addBodyClass();
		
		// Add Pardon My French tag when user clicks on the Facebook link
		if ($(document.body).hasClass('en-US')) {
		    $('div.header div.right-links li.fb a').click(function() {
		        var href = $(this).attr('href');
		        $(document.body).append('<img height="1" width="1" src="http://view.atdmt.com/action/erismf_SmirnoffFacebookLink_10"/>');
		        setTimeout(function() {
		            document.location = href;
		        },150);
		        return false;
		    });
		}
		
		$('#register').add(".reg").bind("click", function(e) {
		    e.preventDefault();
			RD.pageOne.show(false);
		});
		$('.login').bind('click', function(e) {
		    e.preventDefault();
			Smirnoff.UI.LoginDialog.show(false);
		});
		
		$(window).load(function () {
			var height = $(".wrap").height();
			
			if ( $(document).height() > height ){
				height = $(document).height();
			}
			
			$("#outer-wrap-bg").css("height",height);
			$("#pattern").css("height",height);
			
			if (Smirnoff.util.isIE6) {
//                $(".png").pngFix({
//                    blankgif: "/images/blank.gif"
//                });
//                $("#terms").pngFix({
//                    blankgif: "/images/blank.gif"
//                });
            }
			
			
			// If we're coming from smirnoff.com and there's a 'noagegate' param, 
			//  user has already passed the agegate. Set the agegate cookie so we don't show it.
			if(gup("noagegate") == "1"){
			  if(location.href.indexOf('smirnoff.com')>=0){
					$.cookie("gw_chkRemember",true,{path:'/',domain:'.smirnoff.com',duration:'1'});
				}else{
					$.cookie("gw_chkRemember",true,{path:'/',duration:'1'});
				}
			}
			
			if (Smirnoff.Session.needsAgeGate()) {
				Smirnoff.UI.showAgeGate();
			}
		});
		// Enable Coremetrics for download links
		$(".download-link").each(function(){
			//cmCreateConversionEventTag(cm_pageid+" DOWNLOAD: "+this.href, 2);
			$(this).click(function(){cmCreateConversionEventTag(cm_pageid + ' DOWNLOAD: ' + this.href,2)});
		});
	});

	

	
	
	

	
	/**
	 * Load and show the age gate
	 */
	function agegate() {
        Smirnoff.UI.showAgeGate();
	}
	
	
	/**
	 * Wrapper for backward compatibility. 
	 * 
	 * TODO: replace all check_login calls with Smirnoff.Session.loggedInToECM.
	 */
	check_login = function(){
        return Smirnoff.Session.loggedInToECM();
	}

	// renders errors in forms to DOM
	display_form_errors = function(data){
		if(data.success == true || data.success == "true"){return true;}
		$(".error-field").removeClass("error-field");
		for( error in data.errors ){
			//console.log(form.find("#"+error));
			if(error.indexOf("dob_") == 0){
				form.find("#dob_error").addClass("error-field");
			} else {
				form.find("#"+error+',.'+error).addClass("error-field");
			}
		}
		return false;
	}
   // Add a stylesheet to the document and return a reference to it
   addStyle = function( css ) {
      var style = document.createElement( 'style' );
      style.type = 'text/css';
      var head = document.getElementsByTagName('head')[0];
      head.appendChild( style );
      if( style.styleSheet )  // IE
         style.styleSheet.cssText = css;
      else  // other browsers
         style.appendChild( document.createTextNode(css) );
      return style;
   }
	// Replace the entire contents of a stylesheet
	changeStyle = function( style, css ) {
		if( style.styleSheet )  // IE
			style.styleSheet.cssText = css;
		else  // other browsers
			style.replaceChild( document.createTextNode(css), style.firstChild );
		return style;
	}
	
	/**
	 * Wrapper for Smirnoff.util.getUrlParam().
	 * 
	 * TODO: replace all calls to this function with the direct call to Smirnoff.util.getUrlParam.
	 * 
	 * @param {Object} name
	 */
	gup = function( name ) {
        return Smirnoff.util.getUrlParam(name);
	};
	
	$.extend({
		// The opposiite of jQuery's native $.param() method. Deserialises a param string to an object:
		// Note: TODO: Allow for multiple params with same name (to become anarray attribute).
		unparam : function(params){
			var objResult = {};
			$.each(params.split("&"), function(){ var prm = this.split("="); objResult[prm[0]] = prm[1]; });
			return objResult;
		}
	});
	
	if ($.akqa == undefined){ $.extend({ akqa: {} }); }
	
	$.extend($.akqa, {
		tracking: {
			//coremetrics
			//		cmCreatePageviewTag(cm_pageid+": Registration Form Completed",cm_categoryid,cm_search);
			//		cmCreateConversionEventTag(cm_pageid+": Registration Form", 2);
			videoStart: function(id){
				if (typeof(id[0]) == "string") {
					id=id[0];
				} else {
					id = flashTrackingId;
				}
				Smirnoff.util.log("videoStart called with " + id);
				cmCreatePageElementTag("play", cm_pageid+" Player: "+id);
				cmCreateConversionEventTag(cm_category_prefix+" Player", 1);
			},
			videoStop: function(id,pct) {
				if (typeof(id[0]) == "string") {
					id=id[0];
				} else {
					id = flashTrackingId;
				}
				Smirnoff.util.log("videoStop called with " + id);
				cmCreatePageElementTag("stop", cm_category_prefix+" Player: "+id);
			},
			videoComplete: function(id) {
				if (typeof(id[0]) == "string") {
					id=id[0];
				} else {
					id = flashTrackingId;
				}
				Smirnoff.util.log("videoComplete called with " + id);
				cmCreatePageElementTag("complete", cm_pageid+" Player: "+id);
				cmCreateConversionEventTag(cm_category_prefix+" Player", 2);
			},
			videoFullScreen: function(id) {
				Smirnoff.util.log("videoFullScreen called with " + id);
				cmCreatePageElementTag("fullscreen", cm_category_prefix+" Player");
			}
		},
		share: function(a,b) { }
	});
	
	/**
	 * Looks for a default value associated with the element,
	 * either attached by $(el).data() or the value="..." attribute
	 * 
	 * In the latter case, it will set the default using $(el).data().
	 * 
	 * Also resets some custom events.
	 */
	$.fn.defaultAsLabel = function() {
		return this.filter('input[type=text]').each(function() {
			var el = $(this);
			var defaultValue = el.data('defaultValue');
			if (!defaultValue) {
				defaultValue = el.attr('value');
				el.data('defaultValue',defaultValue);
			}
			if (!defaultValue) { return; }
			
			el.unbind('focus.defaultAsLabel').unbind('blur.defaultAsLabel')
				.bind('focus.defaultAsLabel',function() { if (el.attr('value') === defaultValue) { el.attr('value',''); } })
				.bind('blur.defaultAsLabel',function() { if (el.attr('value') === '') { el.attr('value',defaultValue); } });
		}).end();
	};
})(jQuery);


