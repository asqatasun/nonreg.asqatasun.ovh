(function($) { // start anonymous function so we can use jQuery as $

	function rfc3339ToDate(val) {
		var pattern = /^(\d{4})(?:-(\d{2}))?(?:-(\d{2}))?(?:[Tt](\d{2}):(\d{2}):(\d{2})(?:\.(\d*))?)?([Zz])?(?:([+-])(\d{2}):(\d{2}))?$/;
	 
		var m = pattern.exec(val);
		var year = new Number(m[1] ? m[1] : 0);
		var month = new Number(m[2] ? m[2] : 0);
		var day = new Number(m[3] ? m[3] : 0);
		var hour = new Number(m[4] ? m[4] : 0);
		var minute = new Number(m[5] ? m[5] : 0);
		var second = new Number(m[6] ? m[6] : 0);
		var millis = new Number(m[7] ? m[7] : 0);
		var gmt = m[8];
		var dir = m[9];
		var offhour = new Number(m[10] ? m[10] : 0);
		var offmin = new Number(m[11] ? m[11] : 0);
	 
		if (dir && offhour && offmin) {
			var offset = ((offhour * 60) + offmin);
			if (dir == "+") {
				minute -= offset;
			} else if (dir == "-") {
				minute += offset;
			}
		}

		return new Date(Date.UTC(year, month - 1, day, hour, minute, second, millis));
	}
	 
	// zeropad a number to two digits
	function pad(v) {
		if (v < 10) {
			v = "0" + v;
		}
		return v;
	}
	 
	// format a date returned from friendfeed api for display
	function formatFriendFeedDate(ffdate) {
		var m = new Array('jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec');
		var d = rfc3339ToDate(ffdate);
		return d.getDate() + ' ' + m[d.getMonth()] + ', ' + pad(d.getHours()) + ':' + pad(d.getMinutes());
	}
		
	
	$(document).ready(function() {
		// friendfeed: name of user who's feed we want to retrieve
		//var usr = 'smirnoffthere';
		var nam = 'Smirnoff Experience';
		// friendfeed: number of entries to retrieve
		var container = $("#home-feed");
		var data=FRIEND_FEED_DATA;
		var entryUrl='';
		// loop for each friendfeed entry retrieved
		urlPattern = /((ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?)/gi;
		if(data.entries){
			$.each(data.entries, function(i, entry) {
				// ignore entry if it is marked as 'hidden'
				if (entry.hidden != true) {
					// reference the service details
					var svc = entry.service;
					// build markup for entry - adapt as you require
					var t = '';
					t += '<p id="feed-text">';
					entryUrl=entry.user.profileUrl;
					t += entry.title.replace(urlPattern,'<a href="$1">$1</a>');
					t += '</p>';
					t += '<p id="signature">';
					t += formatFriendFeedDate(entry.published);
					t += ' via '
					t += '<a target="_blank" href="'+svc.profileUrl+'" title="View '+nam+'\'s profile on '+svc.name+'">';
					t += svc.name;
					t += '</a>';
					t += '</p>';
					// append content to container
					container.append(t);
				}
			});
			$("#feed-follow-us a").attr('href',entryUrl);
		}else{
			$("#home-feed-wrapper").css("visibility","hidden");
		}
	});
})(jQuery);		
