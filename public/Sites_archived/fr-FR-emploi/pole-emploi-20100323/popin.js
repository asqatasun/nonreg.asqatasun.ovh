$(document).ready(function () {
   initPopin();
	initShowPopinDirect();
});

var ancienTop = 0;
var initAnchorLink = function(){
	if($("#lexique-scroll").is("div")){
		$(".lexique-nav a").click(function(){
			var ancre = $(this).attr("href");
			var ancreTop = $(ancre).offset().top-220+ancienTop;
			ancienTop = ancreTop;
			$('#lexique-scroll').animate({ scrollTop: ancreTop});
			return false;
		});
	}
}

var initPopin = (function () {		
	//Creer les divs 
	var divs = '<div id="mask"></div><div id="popin"></div>';
	
	var oPage = $(".page");
	if (!oPage) return;
	
	$(".page").after(divs);
		
	// cacher les div popin	
	$("#popin").hide();  
	$("#mask").hide();
   
	initShowPopin();	
});

var initShowPopin = (function () {
 $("a.loadPopin").bind("click", function(){
	 showPopin();
	 //afficher le popin
    $("#popin").html('<img src="file/sitemodel/interpe/images/commun/loading.gif" alt="Loading..." />');
    $("#popin").load($(this).attr("href"), "", function(){initClosePopin(); initAnchorLink(); initSubmitFormInPopin();});
	 $("#popin").show();
	 $("#popin").css("z-index","10000000");
    $("#popin").css("opacity", "0.1");
    $("#popin").animate({
     opacity: 1
    }, 500 );
	 window.location = "#"; 
	 return false;
 });
});

var showPopinConfirmation = (function (form) {
	 showPopin();
	 //afficher le popin
    $("#popin").html('<img src="file/sitemodel/interpe/images/commun/loading.gif" alt="Loading..." />');
    $("#popin").load($("#"+form).attr("action"), "", function(){initClosePopin();});
	 $("#popin").show();
	 $("#popin").css("z-index","10000000");
    $("#popin").css("opacity", "0.1");
    $("#popin").animate({
     opacity: 1
    }, 500 );
	 window.location = "#"; 
});

var showPopin = (function () {
	 if($.browser.msie){
		// virer les selects pour IE
		$("select").css("visibility","hidden");
		$("div#popin select").css("visibility","visible");
	 }
	 if((navigator.userAgent.toLowerCase().indexOf("mac")!=-1)?true:false){
		// on masque les Flashs sur Mac
		$("embed").css("visibility","hidden");
	 }
    //masquer les div contenant du flash
    if($(".embed").is("div")){$(".embed").css("visibility","hidden");}
	 //hauteur du masque
	 if (typeof(window.innerHeight)=="number") {
  		wHeight=window.innerHeight;
  	}
  	else {
  		if (document.documentElement && document.documentElement.clientHeight && parseInt(document.documentElement.clientHeight) !=0 ) {
  			wHeight = document.documentElement.clientHeight;
  		}
  		else {
  			if (document.body&&document.body.clientHeight){
  				wHeight=document.body.clientHeight;
  			}
  		}
  	}
	 var pageH = $(".page").height();
	 var maskHeight;
	 if(wHeight>pageH){
	 	maskHeight = wHeight;
	 }
	 else{
	 	maskHeight = pageH;
	 }
	 $("#mask").height(maskHeight+"px");
	 $("#mask").show();
    $("#mask").css("opacity", "0.1");
    $("#mask").animate({
     opacity: 0.7
    }, 500 );
});

var hidePopin = (function () {	
	$("#popin").empty();
	$("#popin").hide();
	$("#mask").hide();
	
	if($.browser.msie){
		$("select").css("visibility","visible");
	}
	if((navigator.userAgent.toLowerCase().indexOf("mac")!=-1)?true:false){
		$("embed").css("visibility","visible");		
	}
   //afficher les div contenant du flash
   if($(".embed").is("div")){$(".embed").css("visibility","visible");}
	return false;
});

var initClosePopin = (function () {
	$(".closePopin").bind("click", function(){
		hidePopin();
		return false;
	});
   $("#mask").bind("click", function(){
		hidePopin();
		return false;
	});
	return false;
});
 
 
//Initialisation du click sur le bouton valider du formulaire
var initSubmitFormInPopin = (function () {
	if($(".formInPopin").is("form")) {
		$(".formInPopin .btSubmitFormInPopin").bind("click", function(){
			submitFormInPopin();
			return false;
		});
	}
});

//Submission du formulaire
var submitFormInPopin = (function () {
	var myForm = $(".formInPopin");   
   var result = "";
   $(".formInPopin input:text").each(function() {
      result += $(this).attr("name")+"="+$(this).val()+"&";
   })
	$(".formInPopin input:radio").each(function() {
      result += $(this).attr("name")+"="+$(this).val()+"&";
   })
	$(".formInPopin input:checkbox").each(function() {
      result += $(this).attr("name")+"="+$(this).val()+"&";
   })
   $(".formInPopin select").each(function() {
      result += $(this).attr("name")+"="+$(this).val()+"&";
   })
   $(".formInPopin textarea").each(function() {
      result += $(this).attr("name")+"="+$(this).val()+"&";
   })
	//Envoie les donnees saisies au serveur
   $.ajax({
      type:"GET",
      url: myForm.attr("action"),
      data: result,
      success:function(msg) {
			//Traitement cote serveur, si tout va bien on cache le formulaire et on affiche la confirmation
			$(".formInPopin").css("display", "none");
			$("#cofirmZone").css("display", "block");
      	initClosePopin();
      }
   });
});

var getParam = function(name){
  var start=location.search.indexOf("?"+name+"=");
  if (start<0) start=location.search.indexOf("&"+name+"=");
  if (start<0) return '';
  start += name.length+2;
  var end=location.search.indexOf("&",start)-1;
  if (end<0) end=location.search.length;
  var result='';
  for(var i=start;i<=end;i++) {
    var c=location.search.charAt(i);
    result=result+(c=='+'?' ':c);
  }
  return unescape(result);
}

try{
	mapUrl['trois'] = "SCU_CSL_CONFIRMATION_FORM.html";
} catch (err){
	mapUrl = new Array(2);
	mapUrl['un'] = "SCU_CSL_CONFIRMATION_FORM.html";
	mapUrl['deux'] = "SCU_SAI_FORM_ABONNEMENT_NWSLTTR.html";
}

var initShowPopinDirect = (function () {
	var popinURL = getParam("popinURL");
	if(popinURL!=""){
		url = mapUrl[popinURL];
		showPopin();
		//afficher le popin
	    $("#popin").html('<img src="file/sitemodel/interpe/images/commun/loading.gif" alt="Loading..." />');
	    $("#popin").load(url, "", function(){initClosePopin(); initAnchorLink(); initSubmitFormInPopin();});
		 $("#popin").show();
		 $("#popin").css("z-index","10000000");
	    $("#popin").css("opacity", "0.1");
	    $("#popin").animate({
	     opacity: 1
	    }, 500 );
 	}
});