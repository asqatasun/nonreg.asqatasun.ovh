function FlashMessageStatus(flashMessage,flashSpeed) {
  if (typeof window.onload != "function") {
    window.onload = function() {
      FlashMessageStatusOnLoad(flashMessage);
    };
  } else {
    var oldonload = window.onload;
    window.onload = function() {
      oldonload();
      FlashMessageStatusOnLoad(flashMessage);
    };
  }
  
}

// On fait un trim sur le message � tester car quand il n'y a pas de message
// on re�oit ' ' et non ''
function FlashMessageStatusOnLoad(flashMessage) {
  if (flashMessage.replace(/^\s+|\s+$/g, "") != "") {
    document.getElementById("messageInformatif").innerHTML = flashMessage;
    document.getElementById("boxMessageInformatif").style.display = "block";
  }
}
