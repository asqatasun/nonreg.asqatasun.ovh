function openWindow(theURL,winName,features) { //v2.0
  newWindow = window.open(theURL,winName,features);
  newWindow.focus();
}

function openWindowEx(theURL,winName,scrollbars,resizable,width,height) { //v2.0
  features = "scrollbars=" + scrollbars + ",resizable=" + resizable + ",width=" + width + ",height=" + height;
  newWindow = window.open(theURL,winName,features);
  newWindow.focus();
}