$(document).ready(function() {

	// liens d'evitement
	$("div.skip_link").addClass('accessibilite_off');
	$("div.skip_link a").focus(function(){
		$(this).parents("div.skip_link").removeClass('accessibilite_off');
	});
	$("div.skip_link a").blur(function(){
		$(this).parents("div.skip_link").addClass('accessibilite_off');
	});

	var CURRENT_ANIMATION = 0;
	// Preload de l'image suivante
	$('#animation li.next').mouseover(function() {
		if (CURRENT_ANIMATION > 0)
			return;

		if ($('#animation div.animation_int img').size() == 3)
			return;

		$('#animation div.animation_int li>a').eq(1).html('<img src="' + tab_image_default[1] + '" alt="' + tab_legend_default[1] + '" />');
		$('#animation div.animation_int li>a').eq(2).html('<img src="' + tab_image_default[2] + '" alt="' + tab_legend_default[2] + '" />');
	});
	$('#animation li.next').click(function() {
		if (CURRENT_ANIMATION == 2)
			return false;

		$('#animation li.prev').css('display', 'inline');
		$('#animation div.animation_int ul').animate({left: '-=939px'}, 'slow');
		CURRENT_ANIMATION++;

		if (CURRENT_ANIMATION == 2)
			$(this).css('display', 'none');

		return false;
	});
	$('#animation li.prev').click(function() {
		if (CURRENT_ANIMATION == 0)
			return false;

		$('#animation li.next').css('display', 'inline');
		$('#animation div.animation_int ul').animate({left: '+=939px'}, 'slow');
		CURRENT_ANIMATION--;

		if (CURRENT_ANIMATION == 0)
			$(this).css('display', 'none');

		return false;
	});

	var headline_count;
	var headline_interval;
	var old_headline = 0;
	var current_headline = 0;

	headline_count = $("div.news li").size();
	$("div.news li:eq("+current_headline+")").css('top','0px');
	if (headline_count > 1)
		headline_interval = setInterval(headline_rotate, 5000);

	function headline_rotate()
	{
		current_headline = (old_headline + 1) % headline_count;
		$("div.news li:eq(" + old_headline + ")").animate({top: -20},"slow", function() {
			$(this).css('top','20px');
		});
		$("div.news li:eq(" + current_headline + ")").show().animate({top: 0},"slow");
		old_headline = current_headline;
	}
	
	$('div.alert_niveau_2 p.close').click(function() {
		$(this).parent().css('display', 'none');
		return false;
	});
});