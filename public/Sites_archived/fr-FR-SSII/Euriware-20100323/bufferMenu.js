/*****************************************************/
/*			Menu haut (Level 0 et 1)			*/
/*****************************************************/

var TabLevel0Index = new Array();
var TabLevel0Values = new Array();
var CELL_1_1 = [ 'Qui sommes-nous ?', '', '', '/scripts/home/publigen/content/templates/show.asp?P=84&L=FR', '', '', '', '', '', [] ];
var CELL_1_2 = [ 'Chiffres clés', '', '', '/scripts/home/publigen/content/templates/show.asp?P=530&L=FR', '', '', '', '', '', [] ];
var CELL_1_3 = [ 'Partenaires', '', '', '/scripts/home/publigen/content/templates/show.asp?P=85&L=FR', '', '', '', '', '', [] ];
var CELL_1_4 = [ 'Implantations', '', '', '/scripts/home/publigen/content/templates/show.asp?P=479&L=FR', '', '', '', '', '', [] ];
var CELL_1_5 = [ 'Contacts', '', '', '/scripts/home/publigen/content/templates/show.asp?P=240&L=FR', '', '', '', '', '', [] ];
var SSMENU_1 = [  CELL_1_1, CELL_1_2, CELL_1_3, CELL_1_4, CELL_1_5 ];
var CELL_1 = [ 'Euriware', '', '', '/scripts/home/publigen/content/templates/show.asp?P=84&L=FR', '', 'Menu1On', 'Menu1Off', 'SubMenu1On', 'SubMenu1Off', SSMENU_1 ];
TabLevel0Index.push("home/Summary_FR@23499");
TabLevel0Values.push(new Array('0','/lib/images/INTERAREVABUCSI/menubar/menu1_FR_on.gif'));
var CELL_2_1 = [ 'Conseil', '', '', '/scripts/home/publigen/content/templates/show.asp?P=78&L=FR', '', '', '', '', '', [] ];
var CELL_2_2 = [ 'Intégration de systèmes', '', '', '/scripts/home/publigen/content/templates/show.asp?P=80&L=FR', '', '', '', '', '', [] ];
var CELL_2_3 = [ 'Infogérance évolutive', '', '', '/scripts/home/publigen/content/templates/show.asp?P=79&L=FR', '', '', '', '', '', [] ];
var SSMENU_2 = [  CELL_2_1, CELL_2_2, CELL_2_3 ];
var CELL_2 = [ 'Métiers', '', '', '/scripts/home/publigen/content/templates/show.asp?P=78&L=FR', '', 'Menu2On', 'Menu2Off', 'SubMenu2On', 'SubMenu2Off', SSMENU_2 ];
TabLevel0Index.push("home/Summary_FR@22768");
TabLevel0Values.push(new Array('1','/lib/images/INTERAREVABUCSI/menubar/menu2_FR_on.gif'));
var CELL_3_1 = [ 'Performance industrielle', '', '', '/scripts/home/publigen/content/templates/show.asp?P=258&L=FR', '', '', '', '', '', [] ];
var CELL_3_2 = [ 'Gestion de contenu', '', '', '/scripts/home/publigen/content/templates/show.asp?P=98&L=FR', '', '', '', '', '', [] ];
var CELL_3_3 = [ 'Gestion du cycle de vie du produit', '', '', '/scripts/home/publigen/content/templates/show.asp?P=100&L=FR', '', '', '', '', '', [] ];
var CELL_3_4 = [ 'Sécurité des Systèmes d\'Information', '', '', '/scripts/home/publigen/content/templates/show.asp?P=104&L=FR', '', '', '', '', '', [] ];
var CELL_3_5 = [ 'Solutions d\'entreprise ERP', '', '', '/scripts/home/publigen/content/templates/show.asp?P=103&L=FR', '', '', '', '', '', [] ];
var CELL_3_6 = [ 'TMA - Tierce Maintenance Applicative', '', '', '/scripts/home/publigen/content/templates/show.asp?P=102&L=FR', '', '', '', '', '', [] ];
var CELL_3_7 = [ 'AMOA - Assistance à Maîtrise d\'Ouvrage', '', '', '/scripts/home/publigen/content/templates/show.asp?P=101&L=FR', '', '', '', '', '', [] ];
var CELL_3_8 = [ 'BI - Business Intelligence', '', '', '/scripts/home/publigen/content/templates/show.asp?P=129&L=FR', '', '', '', '', '', [] ];
var SSMENU_3 = [  CELL_3_1, CELL_3_2, CELL_3_3, CELL_3_4, CELL_3_5, CELL_3_6, CELL_3_7, CELL_3_8 ];
var CELL_3 = [ 'Solutions', '', '', '', '', 'Menu3On', 'Menu3Off', 'SubMenu3On', 'SubMenu3Off', SSMENU_3 ];
TabLevel0Index.push("home/Summary_FR@95047");
TabLevel0Values.push(new Array('2','/lib/images/INTERAREVABUCSI/menubar/menu3_FR_on.gif'));
var CELL_4_1 = [ 'Energie', '', '', '/scripts/home/publigen/content/templates/show.asp?P=312&L=FR', '', '', '', '', '', [] ];
var CELL_4_2 = [ 'Industrie', '', '', '/scripts/home/publigen/content/templates/show.asp?P=281&L=FR', '', '', '', '', '', [] ];
var CELL_4_3 = [ 'Défense', '', '', '/scripts/home/publigen/content/templates/show.asp?P=282&L=FR', '', '', '', '', '', [] ];
var CELL_4_4 = [ 'Services', '', '', '/scripts/home/publigen/content/templates/show.asp?P=283&L=FR', '', '', '', '', '', [] ];
var SSMENU_4 = [  CELL_4_1, CELL_4_2, CELL_4_3, CELL_4_4 ];
var CELL_4 = [ 'Secteurs', '', '', '', '', 'Menu4On', 'Menu4Off', 'SubMenu4On', 'SubMenu4Off', SSMENU_4 ];
TabLevel0Index.push("home/Summary_FR@40213");
TabLevel0Values.push(new Array('3','/lib/images/INTERAREVABUCSI/menubar/menu4_FR_on.gif'));
var CELL_5_1 = [ 'Communiqués de presse', '', '', '/scripts/home/publigen/content/templates/show.asp?P=108&L=FR', '', '', '', '', '', [] ];
var CELL_5_2 = [ 'Archives', '', '', '/scripts/home/publigen/content/templates/show.asp?P=109&L=FR', '', '', '', '', '', [] ];
var SSMENU_5 = [  CELL_5_1, CELL_5_2 ];
var CELL_5 = [ 'Presse', '', '', '/scripts/home/publigen/content/templates/show.asp?P=108&L=FR', '', 'Menu5On', 'Menu5Off', 'SubMenu5On', 'SubMenu5Off', SSMENU_5 ];
TabLevel0Index.push("home/Summary_FR@20511");
TabLevel0Values.push(new Array('4','/lib/images/INTERAREVABUCSI/menubar/menu5_FR_on.gif'));
var CELL_6_1 = [ 'Votre carrière', '', '', '/scripts/home/publigen/content/templates/show.asp?P=105&L=FR', '', '', '', '', '', [] ];
var CELL_6_2 = [ 'Nos postes', '', '', '/scripts/home/publigen/content/templates/show.asp?P=204&L=FR', '', '', '', '', '', [] ];
var CELL_6_3 = [ 'Etudiants / Jeunes diplômés', '', '', '/scripts/home/publigen/content/templates/show.asp?P=334&L=FR', '', '', '', '', '', [] ];
var SSMENU_6 = [  CELL_6_1, CELL_6_2, CELL_6_3 ];
var CELL_6 = [ 'Carrières', '', '', '/scripts/home/publigen/content/templates/show.asp?P=105&L=FR', '', 'Menu6On', 'Menu6Off', 'SubMenu6On', 'SubMenu6Off', SSMENU_6 ];
TabLevel0Index.push("home/Summary_FR@32257");
TabLevel0Values.push(new Array('5','/lib/images/INTERAREVABUCSI/menubar/menu6_FR_on.gif'));
var CELL_7_1 = [ '2010', '', '', '/scripts/home/publigen/content/templates/show.asp?P=510&L=FR', '', '', '', '', '', [] ];
var CELL_7_2 = [ '2009', '', '', '/scripts/home/publigen/content/templates/show.asp?P=411&L=FR', '', '', '', '', '', [] ];
var CELL_7_3 = [ 'Archives', '', '', '/scripts/home/publigen/content/templates/show.asp?P=301&L=FR', '', '', '', '', '', [] ];
var SSMENU_7 = [  CELL_7_1, CELL_7_2, CELL_7_3 ];
var CELL_7 = [ 'Evénements', '', '', '/scripts/home/publigen/content/templates/show.asp?P=411&L=FR', '', 'Menu7On', 'Menu7Off', 'SubMenu7On', 'SubMenu7Off', SSMENU_7 ];
TabLevel0Index.push("home/Summary_FR@33360");
TabLevel0Values.push(new Array('6','/lib/images/INTERAREVABUCSI/menubar/menu7_FR_on.gif'));
var SSMENU = [  CELL_1, CELL_2, CELL_3, CELL_4, CELL_5, CELL_6, CELL_7 ];

/*****************************************************/
/*			Affichage du menu haut				*/
/*****************************************************/

intNombreCellulesParColonne = 50;
ShowMainMenu(SSMENU, intNombreCellulesParColonne, 'AREVA_TOP_MENU');

/*******************************************************/


/*****************************************************/
function showLevel1Active(param, action){
return ("<TABLE BORDER='0' CELLSPACING='0' CELLPADDING='0'><TR>  <TD colspan=2 height=2><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' width=1 height=2></TD></TR><TR>  <TD WIDTH=15 valign='top'><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' border='0' width=1 height=2><A HREF='JavaScript:menu_Click(\"" + param[0] + "\");' class='" + action + "'>&nbsp;&nbsp;&nbsp;</A></nobr></TD>  <TD><A class='lbl_active1' HREF='" + param[1] + "' TARGET='" + param[2] + "'>" + param[3] + "</A></TD></TR></TABLE>");
}

/*****************************************************/
function showLevel1Inactive(param, action){
return ("<TABLE BORDER='0' CELLSPACING='0' CELLPADDING='0'><TR>  <TD colspan=2 height=2><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' width=1 height=2></TD></TR><TR>  <TD WIDTH=15 valign='top'><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' border='0' width=1 height=2><A HREF='JavaScript:menu_Click(\"" + param[0] + "\");' class='" + action + "'>&nbsp;&nbsp;&nbsp;</A></nobr></TD>  <TD><A class='lbl_inactive1' HREF='" + param[1] + "' TARGET='" + param[2] + "'>" + param[3] + "</A></TD></TR></TABLE>");
}

/*****************************************************/
function showLevel2Active(param, action){
return ("<TABLE BORDER='0' CELLSPACING='0' CELLPADDING='0'><TR>  <TD colspan=2 height=2><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' width=1 height=2></TD></TR><TR>  <TD WIDTH=30 valign='top'><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' border='0' width=16 height=2><A HREF='JavaScript:menu_Click(\"" + param[0] + "\");' class='" + action + "'>&nbsp;&nbsp;&nbsp;</A></nobr></TD>  <TD><A class='lbl_active2' HREF='" + param[1] + "' TARGET='" + param[2] + "'>" + param[3] + "</A></TD></TR></TABLE>");
}

/*****************************************************/
function showLevel2Inactive(param, action){
return ("<TABLE BORDER='0' CELLSPACING='0' CELLPADDING='0'><TR>  <TD colspan=2 height=2><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' width=1 height=2></TD></TR><TR>  <TD WIDTH=30 valign='top'><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' border='0' width=16 height=2><A HREF='JavaScript:menu_Click(\"" + param[0] + "\");' class='" + action + "'>&nbsp;&nbsp;&nbsp;</A></nobr></TD>  <TD><A class='lbl_inactive2' HREF='" + param[1] + "' TARGET='" + param[2] + "'>" + param[3] + "</A></TD></TR></TABLE>");
}

/*****************************************************/
function showLevel3Active(param, action){
return ("<TABLE BORDER='0' CELLSPACING='0' CELLPADDING='0'><TR>  <TD colspan=2 height=2><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' width=1 height=2></TD></TR><TR>  <TD WIDTH=45 valign='top'><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' border='0' width=31 height=2><span class='" + action + "'>&nbsp;&nbsp;&nbsp;</span></nobr></TD>  <TD><A class='lbl_active2' HREF='" + param[1] + "' TARGET='" + param[2] + "'>" + param[3] + "</A></TD></TR></TABLE>");
}

/*****************************************************/
function showLevel3Inactive(param, action){
return ("<TABLE BORDER='0' CELLSPACING='0' CELLPADDING='0'><TR>  <TD colspan=2 height=2><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' width=1 height=2></TD></TR><TR>  <TD WIDTH=45 valign='top'><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' border='0' width=31 height=2><span class='" + action + "'>&nbsp;&nbsp;&nbsp;</span></nobr></TD>  <TD><A class='lbl_inactive2' HREF='" + param[1] + "' TARGET='" + param[2] + "'>" + param[3] + "</A></TD></TR></TABLE>");
}

/*****************************************************/
function showLevel4Active(param, action){
return ("<TABLE BORDER='0' CELLSPACING='0' CELLPADDING='0' width='159'><TR>  <TD colspan=2 height=2><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' width=1 height=2></TD></TR><TR>  <TD WIDTH=15 valign='top'><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' width=5 height=2><A HREF='JavaScript:menu_Click(\"" + param[0] + "\");'><IMG ID='ImgSubMenu4_"+ param[0] +"' SRC='/lib/images/INTERAREVABUCSI/menu/MenuLevel1Red" + action + ".gif' BORDER='0'></A></nobr></TD>  <TD><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' width=5 height=2><A style='color:#CC0033; font-weight:normal; text-decoration:none;' HREF='" + param[1] + "' TARGET='" + param[2] + "'>" + param[3] + "</A><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' width=5 height=2></TD></TR></TABLE>");
}

/*****************************************************/
function showLevel4Inactive(param, action){
return ("<TABLE BORDER='0' CELLSPACING='0' CELLPADDING='0' width='159'><TR>  <TD colspan=2 height=2><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' width=1 height=2></TD></TR><TR>  <TD WIDTH=15 valign='top'><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' width=5 height=2><A HREF='JavaScript:menu_Click(\"" + param[0] + "\");'><IMG ID='ImgSubMenu4_"+ param[0] +"' SRC='/lib/images/INTERAREVABUCSI/menu/MenuLevel1Black" + action + ".gif' BORDER='0'></A></nobr></TD>  <TD><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' width=5 height=2><A style='color:#000000; font-weight:normal; text-decoration:none;' HREF='" + param[1] + "' TARGET='" + param[2] + "'>" + param[3] + "</A><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' width=5 height=2></TD></TR></TABLE>");
}

/*****************************************************/
function showLevel5Active(param, action){
return ("<TABLE BORDER='0' CELLSPACING='0' CELLPADDING='0'><TR>  <TD colspan=2 height=2><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' width=1 height=2></TD></TR><TR>  <TD WIDTH=30 valign='top'><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' border='0' width=16 height=2><A HREF='JavaScript:menu_Click(\"" + param[0] + "\");'><IMG ID='ImgSubMenu5_"+ param[0] +"' SRC='/lib/images/INTERAREVABUCSI/menu/MenuLevel2Red" + action + ".gif' BORDER='0'></A></nobr></TD>  <TD><A class='lbl_active' HREF='" + param[1] + "' TARGET='" + param[2] + "'>" + param[3] + "</A></TD></TR></TABLE>");
}

/*****************************************************/
function showLevel5Inactive(param, action){
return ("<TABLE BORDER='0' CELLSPACING='0' CELLPADDING='0'><TR>  <TD colspan=2 height=2><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' width=1 height=2></TD></TR><TR>  <TD WIDTH=30 valign='top'><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' border='0' width=16 height=2><A HREF='JavaScript:menu_Click(\"" + param[0] + "\");'><IMG ID='ImgSubMenu5_"+ param[0] +"' SRC='/lib/images/INTERAREVABUCSI/menu/MenuLevel2Black" + action + ".gif' BORDER='0'></A></nobr></TD>  <TD><A class='lbl_inactive' HREF='" + param[1] + "' TARGET='" + param[2] + "'>" + param[3] + "</A></TD></TR></TABLE>");
}

/*****************************************************/
function showLevel6Active(param, action){
return ("<TABLE BORDER='0' CELLSPACING='0' CELLPADDING='0'><TR>  <TD colspan=2 height=2><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' width=1 height=2></TD></TR><TR>  <TD WIDTH=45 valign='top'><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' border='0' width=31 height=2><IMG ID='ImgSubMenu6_"+ param[0] +"' SRC='/lib/images/INTERAREVABUCSI/menu/MenuLevel3Red" + action + ".gif' BORDER='0'></nobr></TD>  <TD><A class='lbl_active' HREF='" + param[1] + "' TARGET='" + param[2] + "'>" + param[3] + "</A></TD></TR></TABLE>");
}

/*****************************************************/
function showLevel6Inactive(param, action){
return ("<TABLE BORDER='0' CELLSPACING='0' CELLPADDING='0'><TR>  <TD colspan=2 height=2><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' width=1 height=2></TD></TR><TR>  <TD WIDTH=45 valign='top'><nobr><IMG SRC='/lib/images/INTERAREVABUCSI/vide.gif' border='0' width=31 height=2><IMG ID='ImgSubMenu6_"+ param[0] +"' SRC='/lib/images/INTERAREVABUCSI/menu/MenuLevel3Black" + action + ".gif' BORDER='0'></nobr></TD>  <TD><A class='lbl_inactive' HREF='" + param[1] + "' TARGET='" + param[2] + "'>" + param[3] + "</A></TD></TR></TABLE>");
}

/*****************************************************/
/*		Menu (Niveau 1, 2, 3, 4, 5 et 6)			*/
/*****************************************************/

var lstIdNodesIndex = new Array();
var lstIdNodesValues = new Array();
var lstSelectedIdNodesIndex = new Array();
var lstSelectedIdNodesValues = new Array();
/***********************************************************/
/*		Variable contenant les données du Menu gauche		*/
/***********************************************************/

var menuLevel1Index = new Array();
var menuLevel1Values = new Array();
var menuLevel2Index = new Array();
var menuLevel2Values = new Array();
var menuLevel3Index = new Array();
var menuLevel3Values = new Array();
/***********************************************************/
/*		Variable contenant les données du Menu droit		*/
/***********************************************************/

var menuLevel4Index = new Array();
var menuLevel4Values = new Array();
var menuLevel5Index = new Array();
var menuLevel5Values = new Array();
var menuLevel6Index = new Array();
var menuLevel6Values = new Array();


var id0Nodes0Index = new Array();
var id0Nodes0Values = new Array();
lstSelectedIdNodesIndex.push("home/Summary_FR@23499");
lstSelectedIdNodesValues.push("home/Summary_FR@23499");
lstSelectedIdNodesIndex.push("home/Summary_FR@86254");
lstSelectedIdNodesValues.push("home/Summary_FR@23499,home/Summary_FR@86254");
var id1Nodes0Index = new Array();
var id1Nodes0Values = new Array();
menuLevel1Index.push("home/Summary_FR@86254");
menuLevel1Values.push(new Array("home/Summary_FR@86254","/scripts/home/publigen/content/templates/show.asp?P=84&L=FR","","Qui sommes-nous ?"));


id0Nodes0Index.push('home/Summary_FR@86254');
id0Nodes0Values.push(new Array(id1Nodes0Index, id1Nodes0Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@5194");
lstSelectedIdNodesValues.push("home/Summary_FR@23499,home/Summary_FR@5194");
var id1Nodes1Index = new Array();
var id1Nodes1Values = new Array();
menuLevel1Index.push("home/Summary_FR@5194");
menuLevel1Values.push(new Array("home/Summary_FR@5194","/scripts/home/publigen/content/templates/show.asp?P=530&L=FR","_blank","Chiffres clés"));


id0Nodes0Index.push('home/Summary_FR@5194');
id0Nodes0Values.push(new Array(id1Nodes1Index, id1Nodes1Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@94540");
lstSelectedIdNodesValues.push("home/Summary_FR@23499,home/Summary_FR@94540");
var id1Nodes2Index = new Array();
var id1Nodes2Values = new Array();
menuLevel1Index.push("home/Summary_FR@94540");
menuLevel1Values.push(new Array("home/Summary_FR@94540","/scripts/home/publigen/content/templates/show.asp?P=85&L=FR","","Partenaires"));


id0Nodes0Index.push('home/Summary_FR@94540');
id0Nodes0Values.push(new Array(id1Nodes2Index, id1Nodes2Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@69421");
lstSelectedIdNodesValues.push("home/Summary_FR@23499,home/Summary_FR@69421");
var id1Nodes3Index = new Array();
var id1Nodes3Values = new Array();
menuLevel1Index.push("home/Summary_FR@69421");
menuLevel1Values.push(new Array("home/Summary_FR@69421","/scripts/home/publigen/content/templates/show.asp?P=479&L=FR","","Implantations"));


id0Nodes0Index.push('home/Summary_FR@69421');
id0Nodes0Values.push(new Array(id1Nodes3Index, id1Nodes3Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@80235");
lstSelectedIdNodesValues.push("home/Summary_FR@23499,home/Summary_FR@80235");
var id1Nodes4Index = new Array();
var id1Nodes4Values = new Array();
menuLevel1Index.push("home/Summary_FR@80235");
menuLevel1Values.push(new Array("home/Summary_FR@80235","/scripts/home/publigen/content/templates/show.asp?P=240&L=FR","","Contacts"));


id0Nodes0Index.push('home/Summary_FR@80235');
id0Nodes0Values.push(new Array(id1Nodes4Index, id1Nodes4Values));
lstIdNodesIndex.push('home/Summary_FR@23499');
lstIdNodesValues.push(new Array(id0Nodes0Index, id0Nodes0Values));
var id0Nodes1Index = new Array();
var id0Nodes1Values = new Array();
lstSelectedIdNodesIndex.push("home/Summary_FR@22768");
lstSelectedIdNodesValues.push("home/Summary_FR@22768");
lstSelectedIdNodesIndex.push("home/Summary_FR@95906");
lstSelectedIdNodesValues.push("home/Summary_FR@22768,home/Summary_FR@95906");
var id1Nodes0Index = new Array();
var id1Nodes0Values = new Array();
menuLevel1Index.push("home/Summary_FR@95906");
menuLevel1Values.push(new Array("home/Summary_FR@95906","/scripts/home/publigen/content/templates/show.asp?P=78&L=FR","","Conseil"));


id0Nodes1Index.push('home/Summary_FR@95906');
id0Nodes1Values.push(new Array(id1Nodes0Index, id1Nodes0Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@87620");
lstSelectedIdNodesValues.push("home/Summary_FR@22768,home/Summary_FR@87620");
var id1Nodes1Index = new Array();
var id1Nodes1Values = new Array();
menuLevel1Index.push("home/Summary_FR@87620");
menuLevel1Values.push(new Array("home/Summary_FR@87620","/scripts/home/publigen/content/templates/show.asp?P=80&L=FR","","Intégration de systèmes"));


id0Nodes1Index.push('home/Summary_FR@87620');
id0Nodes1Values.push(new Array(id1Nodes1Index, id1Nodes1Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@65594");
lstSelectedIdNodesValues.push("home/Summary_FR@22768,home/Summary_FR@65594");
var id1Nodes2Index = new Array();
var id1Nodes2Values = new Array();
menuLevel1Index.push("home/Summary_FR@65594");
menuLevel1Values.push(new Array("home/Summary_FR@65594","/scripts/home/publigen/content/templates/show.asp?P=79&L=FR","","Infogérance évolutive"));


id0Nodes1Index.push('home/Summary_FR@65594');
id0Nodes1Values.push(new Array(id1Nodes2Index, id1Nodes2Values));
lstIdNodesIndex.push('home/Summary_FR@22768');
lstIdNodesValues.push(new Array(id0Nodes1Index, id0Nodes1Values));
var id0Nodes2Index = new Array();
var id0Nodes2Values = new Array();
lstSelectedIdNodesIndex.push("home/Summary_FR@95047");
lstSelectedIdNodesValues.push("home/Summary_FR@95047");
lstSelectedIdNodesIndex.push("home/Summary_FR@51414");
lstSelectedIdNodesValues.push("home/Summary_FR@95047,home/Summary_FR@51414");
var id1Nodes0Index = new Array();
var id1Nodes0Values = new Array();
menuLevel1Index.push("home/Summary_FR@51414");
menuLevel1Values.push(new Array("home/Summary_FR@51414","/scripts/home/publigen/content/templates/show.asp?P=258&L=FR","","Performance industrielle"));


id0Nodes2Index.push('home/Summary_FR@51414');
id0Nodes2Values.push(new Array(id1Nodes0Index, id1Nodes0Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@64726");
lstSelectedIdNodesValues.push("home/Summary_FR@95047,home/Summary_FR@64726");
var id1Nodes1Index = new Array();
var id1Nodes1Values = new Array();
menuLevel1Index.push("home/Summary_FR@64726");
menuLevel1Values.push(new Array("home/Summary_FR@64726","/scripts/home/publigen/content/templates/show.asp?P=98&L=FR","","Gestion de contenu"));


id0Nodes2Index.push('home/Summary_FR@64726');
id0Nodes2Values.push(new Array(id1Nodes1Index, id1Nodes1Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@64963");
lstSelectedIdNodesValues.push("home/Summary_FR@95047,home/Summary_FR@64963");
var id1Nodes2Index = new Array();
var id1Nodes2Values = new Array();
menuLevel1Index.push("home/Summary_FR@64963");
menuLevel1Values.push(new Array("home/Summary_FR@64963","/scripts/home/publigen/content/templates/show.asp?P=100&L=FR","","Gestion du cycle de vie du produit"));


id0Nodes2Index.push('home/Summary_FR@64963');
id0Nodes2Values.push(new Array(id1Nodes2Index, id1Nodes2Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@77522");
lstSelectedIdNodesValues.push("home/Summary_FR@95047,home/Summary_FR@77522");
var id1Nodes3Index = new Array();
var id1Nodes3Values = new Array();
menuLevel1Index.push("home/Summary_FR@77522");
menuLevel1Values.push(new Array("home/Summary_FR@77522","/scripts/home/publigen/content/templates/show.asp?P=104&L=FR","","Sécurité des Systèmes d'Information"));


id0Nodes2Index.push('home/Summary_FR@77522');
id0Nodes2Values.push(new Array(id1Nodes3Index, id1Nodes3Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@11883");
lstSelectedIdNodesValues.push("home/Summary_FR@95047,home/Summary_FR@11883");
var id1Nodes4Index = new Array();
var id1Nodes4Values = new Array();
menuLevel1Index.push("home/Summary_FR@11883");
menuLevel1Values.push(new Array("home/Summary_FR@11883","/scripts/home/publigen/content/templates/show.asp?P=103&L=FR","","Solutions d'entreprise ERP"));


id0Nodes2Index.push('home/Summary_FR@11883');
id0Nodes2Values.push(new Array(id1Nodes4Index, id1Nodes4Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@58565");
lstSelectedIdNodesValues.push("home/Summary_FR@95047,home/Summary_FR@58565");
var id1Nodes5Index = new Array();
var id1Nodes5Values = new Array();
menuLevel1Index.push("home/Summary_FR@58565");
menuLevel1Values.push(new Array("home/Summary_FR@58565","/scripts/home/publigen/content/templates/show.asp?P=102&L=FR","","TMA - Tierce Maintenance Applicative"));


id0Nodes2Index.push('home/Summary_FR@58565');
id0Nodes2Values.push(new Array(id1Nodes5Index, id1Nodes5Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@27050");
lstSelectedIdNodesValues.push("home/Summary_FR@95047,home/Summary_FR@27050");
var id1Nodes6Index = new Array();
var id1Nodes6Values = new Array();
menuLevel1Index.push("home/Summary_FR@27050");
menuLevel1Values.push(new Array("home/Summary_FR@27050","/scripts/home/publigen/content/templates/show.asp?P=101&L=FR","","AMOA - Assistance à Maîtrise d'Ouvrage"));


id0Nodes2Index.push('home/Summary_FR@27050');
id0Nodes2Values.push(new Array(id1Nodes6Index, id1Nodes6Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@93829");
lstSelectedIdNodesValues.push("home/Summary_FR@95047,home/Summary_FR@93829");
var id1Nodes7Index = new Array();
var id1Nodes7Values = new Array();
menuLevel1Index.push("home/Summary_FR@93829");
menuLevel1Values.push(new Array("home/Summary_FR@93829","/scripts/home/publigen/content/templates/show.asp?P=129&L=FR","","BI - Business Intelligence"));


lstSelectedIdNodesIndex.push("home/Summary_FR@76413");
lstSelectedIdNodesValues.push("home/Summary_FR@95047,home/Summary_FR@93829,home/Summary_FR@76413");
var id2Nodes0Index = new Array();
var id2Nodes0Values = new Array();
menuLevel2Index.push("home/Summary_FR@76413");
menuLevel2Values.push(new Array("home/Summary_FR@76413","/scripts/home/publigen/content/templates/show.asp?P=208&L=FR","","Nos consultants"));


id1Nodes7Index.push('home/Summary_FR@76413');
id1Nodes7Values.push(new Array(id2Nodes0Index, id2Nodes0Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@85543");
lstSelectedIdNodesValues.push("home/Summary_FR@95047,home/Summary_FR@93829,home/Summary_FR@85543");
var id2Nodes1Index = new Array();
var id2Nodes1Values = new Array();
menuLevel2Index.push("home/Summary_FR@85543");
menuLevel2Values.push(new Array("home/Summary_FR@85543","/scripts/home/publigen/content/templates/show.asp?P=130&L=FR","","Métier"));


id1Nodes7Index.push('home/Summary_FR@85543');
id1Nodes7Values.push(new Array(id2Nodes1Index, id2Nodes1Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@63517");
lstSelectedIdNodesValues.push("home/Summary_FR@95047,home/Summary_FR@93829,home/Summary_FR@63517");
var id2Nodes2Index = new Array();
var id2Nodes2Values = new Array();
menuLevel2Index.push("home/Summary_FR@63517");
menuLevel2Values.push(new Array("home/Summary_FR@63517","/scripts/home/publigen/content/templates/show.asp?P=131&L=FR","","Services"));


id1Nodes7Index.push('home/Summary_FR@63517');
id1Nodes7Values.push(new Array(id2Nodes2Index, id2Nodes2Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@3039");
lstSelectedIdNodesValues.push("home/Summary_FR@95047,home/Summary_FR@93829,home/Summary_FR@3039");
var id2Nodes3Index = new Array();
var id2Nodes3Values = new Array();
menuLevel2Index.push("home/Summary_FR@3039");
menuLevel2Values.push(new Array("home/Summary_FR@3039","/scripts/home/publigen/content/templates/show.asp?P=132&L=FR","","Technologies"));


id1Nodes7Index.push('home/Summary_FR@3039');
id1Nodes7Values.push(new Array(id2Nodes3Index, id2Nodes3Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@84475");
lstSelectedIdNodesValues.push("home/Summary_FR@95047,home/Summary_FR@93829,home/Summary_FR@84475");
var id2Nodes4Index = new Array();
var id2Nodes4Values = new Array();
menuLevel2Index.push("home/Summary_FR@84475");
menuLevel2Values.push(new Array("home/Summary_FR@84475","/scripts/home/publigen/content/templates/show.asp?P=134&L=FR","","Références"));


id1Nodes7Index.push('home/Summary_FR@84475');
id1Nodes7Values.push(new Array(id2Nodes4Index, id2Nodes4Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@66721");
lstSelectedIdNodesValues.push("home/Summary_FR@95047,home/Summary_FR@93829,home/Summary_FR@66721");
var id2Nodes5Index = new Array();
var id2Nodes5Values = new Array();
menuLevel2Index.push("home/Summary_FR@66721");
menuLevel2Values.push(new Array("home/Summary_FR@66721","/scripts/home/publigen/content/templates/show.asp?P=486&L=FR","","Formation"));


id1Nodes7Index.push('home/Summary_FR@66721');
id1Nodes7Values.push(new Array(id2Nodes5Index, id2Nodes5Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@93425");
lstSelectedIdNodesValues.push("home/Summary_FR@95047,home/Summary_FR@93829,home/Summary_FR@93425");
var id2Nodes6Index = new Array();
var id2Nodes6Values = new Array();
menuLevel2Index.push("home/Summary_FR@93425");
menuLevel2Values.push(new Array("home/Summary_FR@93425","mailto:laurent.pasquette@euriware.fr","_blank","Contact"));


id1Nodes7Index.push('home/Summary_FR@93425');
id1Nodes7Values.push(new Array(id2Nodes6Index, id2Nodes6Values));
id0Nodes2Index.push('home/Summary_FR@93829');
id0Nodes2Values.push(new Array(id1Nodes7Index, id1Nodes7Values));
lstIdNodesIndex.push('home/Summary_FR@95047');
lstIdNodesValues.push(new Array(id0Nodes2Index, id0Nodes2Values));
var id0Nodes3Index = new Array();
var id0Nodes3Values = new Array();
lstSelectedIdNodesIndex.push("home/Summary_FR@40213");
lstSelectedIdNodesValues.push("home/Summary_FR@40213");
lstSelectedIdNodesIndex.push("home/Summary_FR@67239");
lstSelectedIdNodesValues.push("home/Summary_FR@40213,home/Summary_FR@67239");
var id1Nodes0Index = new Array();
var id1Nodes0Values = new Array();
menuLevel1Index.push("home/Summary_FR@67239");
menuLevel1Values.push(new Array("home/Summary_FR@67239","/scripts/home/publigen/content/templates/show.asp?P=312&L=FR","","Energie"));


lstSelectedIdNodesIndex.push("home/Summary_FR@98893");
lstSelectedIdNodesValues.push("home/Summary_FR@40213,home/Summary_FR@67239,home/Summary_FR@98893");
var id2Nodes0Index = new Array();
var id2Nodes0Values = new Array();
menuLevel2Index.push("home/Summary_FR@98893");
menuLevel2Values.push(new Array("home/Summary_FR@98893","/scripts/home/publigen/content/templates/show.asp?P=314&L=FR","","Nucléaire"));


lstSelectedIdNodesIndex.push("home/Summary_FR@70299");
lstSelectedIdNodesValues.push("home/Summary_FR@40213,home/Summary_FR@67239,home/Summary_FR@98893,home/Summary_FR@70299");
var id3Nodes0Index = new Array();
var id3Nodes0Values = new Array();
menuLevel3Index.push("home/Summary_FR@70299");
menuLevel3Values.push(new Array("home/Summary_FR@70299","/scripts/home/publigen/content/templates/show.asp?P=279&L=FR","","Centrales de production"));


id2Nodes0Index.push('home/Summary_FR@70299');
id2Nodes0Values.push(new Array(id3Nodes0Index, id3Nodes0Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@74291");
lstSelectedIdNodesValues.push("home/Summary_FR@40213,home/Summary_FR@67239,home/Summary_FR@98893,home/Summary_FR@74291");
var id3Nodes1Index = new Array();
var id3Nodes1Values = new Array();
menuLevel3Index.push("home/Summary_FR@74291");
menuLevel3Values.push(new Array("home/Summary_FR@74291","/scripts/home/publigen/content/templates/show.asp?P=280&L=FR","","Cycle du combustible"));


id2Nodes0Index.push('home/Summary_FR@74291');
id2Nodes0Values.push(new Array(id3Nodes1Index, id3Nodes1Values));
id1Nodes0Index.push('home/Summary_FR@98893');
id1Nodes0Values.push(new Array(id2Nodes0Index, id2Nodes0Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@90608");
lstSelectedIdNodesValues.push("home/Summary_FR@40213,home/Summary_FR@67239,home/Summary_FR@90608");
var id2Nodes1Index = new Array();
var id2Nodes1Values = new Array();
menuLevel2Index.push("home/Summary_FR@90608");
menuLevel2Values.push(new Array("home/Summary_FR@90608","/scripts/home/publigen/content/templates/show.asp?P=315&L=FR","","Transport et distribution"));


id1Nodes0Index.push('home/Summary_FR@90608');
id1Nodes0Values.push(new Array(id2Nodes1Index, id2Nodes1Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@39588");
lstSelectedIdNodesValues.push("home/Summary_FR@40213,home/Summary_FR@67239,home/Summary_FR@39588");
var id2Nodes2Index = new Array();
var id2Nodes2Values = new Array();
menuLevel2Index.push("home/Summary_FR@39588");
menuLevel2Values.push(new Array("home/Summary_FR@39588","/scripts/home/publigen/content/templates/show.asp?P=316&L=FR","","Pétrole et gaz"));


id1Nodes0Index.push('home/Summary_FR@39588');
id1Nodes0Values.push(new Array(id2Nodes2Index, id2Nodes2Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@31302");
lstSelectedIdNodesValues.push("home/Summary_FR@40213,home/Summary_FR@67239,home/Summary_FR@31302");
var id2Nodes3Index = new Array();
var id2Nodes3Values = new Array();
menuLevel2Index.push("home/Summary_FR@31302");
menuLevel2Values.push(new Array("home/Summary_FR@31302","/scripts/home/publigen/content/templates/show.asp?P=317&L=FR","","Autres énergies"));


id1Nodes0Index.push('home/Summary_FR@31302');
id1Nodes0Values.push(new Array(id2Nodes3Index, id2Nodes3Values));
id0Nodes3Index.push('home/Summary_FR@67239');
id0Nodes3Values.push(new Array(id1Nodes0Index, id1Nodes0Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@28618");
lstSelectedIdNodesValues.push("home/Summary_FR@40213,home/Summary_FR@28618");
var id1Nodes1Index = new Array();
var id1Nodes1Values = new Array();
menuLevel1Index.push("home/Summary_FR@28618");
menuLevel1Values.push(new Array("home/Summary_FR@28618","/scripts/home/publigen/content/templates/show.asp?P=281&L=FR","","Industrie"));


id0Nodes3Index.push('home/Summary_FR@28618');
id0Nodes3Values.push(new Array(id1Nodes1Index, id1Nodes1Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@20332");
lstSelectedIdNodesValues.push("home/Summary_FR@40213,home/Summary_FR@20332");
var id1Nodes2Index = new Array();
var id1Nodes2Values = new Array();
menuLevel1Index.push("home/Summary_FR@20332");
menuLevel1Values.push(new Array("home/Summary_FR@20332","/scripts/home/publigen/content/templates/show.asp?P=282&L=FR","","Défense"));


id0Nodes3Index.push('home/Summary_FR@20332');
id0Nodes3Values.push(new Array(id1Nodes2Index, id1Nodes2Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@98305");
lstSelectedIdNodesValues.push("home/Summary_FR@40213,home/Summary_FR@98305");
var id1Nodes3Index = new Array();
var id1Nodes3Values = new Array();
menuLevel1Index.push("home/Summary_FR@98305");
menuLevel1Values.push(new Array("home/Summary_FR@98305","/scripts/home/publigen/content/templates/show.asp?P=283&L=FR","","Services"));


id0Nodes3Index.push('home/Summary_FR@98305');
id0Nodes3Values.push(new Array(id1Nodes3Index, id1Nodes3Values));
lstIdNodesIndex.push('home/Summary_FR@40213');
lstIdNodesValues.push(new Array(id0Nodes3Index, id0Nodes3Values));
var id0Nodes4Index = new Array();
var id0Nodes4Values = new Array();
lstSelectedIdNodesIndex.push("home/Summary_FR@20511");
lstSelectedIdNodesValues.push("home/Summary_FR@20511");
lstSelectedIdNodesIndex.push("home/Summary_FR@98484");
lstSelectedIdNodesValues.push("home/Summary_FR@20511,home/Summary_FR@98484");
var id1Nodes0Index = new Array();
var id1Nodes0Values = new Array();
menuLevel1Index.push("home/Summary_FR@98484");
menuLevel1Values.push(new Array("home/Summary_FR@98484","/scripts/home/publigen/content/templates/show.asp?P=108&L=FR","","Communiqués de presse"));


id0Nodes4Index.push('home/Summary_FR@98484');
id0Nodes4Values.push(new Array(id1Nodes0Index, id1Nodes0Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@38005");
lstSelectedIdNodesValues.push("home/Summary_FR@20511,home/Summary_FR@38005");
var id1Nodes1Index = new Array();
var id1Nodes1Values = new Array();
menuLevel1Index.push("home/Summary_FR@38005");
menuLevel1Values.push(new Array("home/Summary_FR@38005","/scripts/home/publigen/content/templates/show.asp?P=109&L=FR","","Archives"));


id0Nodes4Index.push('home/Summary_FR@38005');
id0Nodes4Values.push(new Array(id1Nodes1Index, id1Nodes1Values));
lstIdNodesIndex.push('home/Summary_FR@20511');
lstIdNodesValues.push(new Array(id0Nodes4Index, id0Nodes4Values));
var id0Nodes5Index = new Array();
var id0Nodes5Values = new Array();
lstSelectedIdNodesIndex.push("home/Summary_FR@32257");
lstSelectedIdNodesValues.push("home/Summary_FR@32257");
lstSelectedIdNodesIndex.push("home/Summary_FR@99668");
lstSelectedIdNodesValues.push("home/Summary_FR@32257,home/Summary_FR@99668");
var id1Nodes0Index = new Array();
var id1Nodes0Values = new Array();
menuLevel1Index.push("home/Summary_FR@99668");
menuLevel1Values.push(new Array("home/Summary_FR@99668","/scripts/home/publigen/content/templates/show.asp?P=105&L=FR","","Votre carrière"));


id0Nodes5Index.push('home/Summary_FR@99668');
id0Nodes5Values.push(new Array(id1Nodes0Index, id1Nodes0Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@42654");
lstSelectedIdNodesValues.push("home/Summary_FR@32257,home/Summary_FR@42654");
var id1Nodes1Index = new Array();
var id1Nodes1Values = new Array();
menuLevel1Index.push("home/Summary_FR@42654");
menuLevel1Values.push(new Array("home/Summary_FR@42654","/scripts/home/publigen/content/templates/show.asp?P=204&L=FR","","Nos postes"));


id0Nodes5Index.push('home/Summary_FR@42654');
id0Nodes5Values.push(new Array(id1Nodes1Index, id1Nodes1Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@63706");
lstSelectedIdNodesValues.push("home/Summary_FR@32257,home/Summary_FR@63706");
var id1Nodes2Index = new Array();
var id1Nodes2Values = new Array();
menuLevel1Index.push("home/Summary_FR@63706");
menuLevel1Values.push(new Array("home/Summary_FR@63706","/scripts/home/publigen/content/templates/show.asp?P=334&L=FR","","Etudiants / Jeunes diplômés"));


lstSelectedIdNodesIndex.push("home/Summary_FR@49327");
lstSelectedIdNodesValues.push("home/Summary_FR@32257,home/Summary_FR@63706,home/Summary_FR@49327");
var id2Nodes0Index = new Array();
var id2Nodes0Values = new Array();
menuLevel2Index.push("home/Summary_FR@49327");
menuLevel2Values.push(new Array("home/Summary_FR@49327","/scripts/home/publigen/content/templates/show.asp?P=335&L=FR","","Stage"));


id1Nodes2Index.push('home/Summary_FR@49327');
id1Nodes2Values.push(new Array(id2Nodes0Index, id2Nodes0Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@16739");
lstSelectedIdNodesValues.push("home/Summary_FR@32257,home/Summary_FR@63706,home/Summary_FR@16739");
var id2Nodes1Index = new Array();
var id2Nodes1Values = new Array();
menuLevel2Index.push("home/Summary_FR@16739");
menuLevel2Values.push(new Array("home/Summary_FR@16739","/scripts/home/publigen/content/templates/show.asp?P=336&L=FR","","Apprentissage"));


id1Nodes2Index.push('home/Summary_FR@16739');
id1Nodes2Values.push(new Array(id2Nodes1Index, id2Nodes1Values));
id0Nodes5Index.push('home/Summary_FR@63706');
id0Nodes5Values.push(new Array(id1Nodes2Index, id1Nodes2Values));
lstIdNodesIndex.push('home/Summary_FR@32257');
lstIdNodesValues.push(new Array(id0Nodes5Index, id0Nodes5Values));
var id0Nodes6Index = new Array();
var id0Nodes6Values = new Array();
lstSelectedIdNodesIndex.push("home/Summary_FR@33360");
lstSelectedIdNodesValues.push("home/Summary_FR@33360");
lstSelectedIdNodesIndex.push("home/Summary_FR@92950");
lstSelectedIdNodesValues.push("home/Summary_FR@33360,home/Summary_FR@92950");
var id1Nodes0Index = new Array();
var id1Nodes0Values = new Array();
menuLevel1Index.push("home/Summary_FR@92950");
menuLevel1Values.push(new Array("home/Summary_FR@92950","/scripts/home/publigen/content/templates/show.asp?P=510&L=FR","","2010"));


id0Nodes6Index.push('home/Summary_FR@92950');
id0Nodes6Values.push(new Array(id1Nodes0Index, id1Nodes0Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@68127");
lstSelectedIdNodesValues.push("home/Summary_FR@33360,home/Summary_FR@68127");
var id1Nodes1Index = new Array();
var id1Nodes1Values = new Array();
menuLevel1Index.push("home/Summary_FR@68127");
menuLevel1Values.push(new Array("home/Summary_FR@68127","/scripts/home/publigen/content/templates/show.asp?P=411&L=FR","","2009"));


id0Nodes6Index.push('home/Summary_FR@68127');
id0Nodes6Values.push(new Array(id1Nodes1Index, id1Nodes1Values));
lstSelectedIdNodesIndex.push("home/Summary_FR@24840");
lstSelectedIdNodesValues.push("home/Summary_FR@33360,home/Summary_FR@24840");
var id1Nodes2Index = new Array();
var id1Nodes2Values = new Array();
menuLevel1Index.push("home/Summary_FR@24840");
menuLevel1Values.push(new Array("home/Summary_FR@24840","/scripts/home/publigen/content/templates/show.asp?P=301&L=FR","","Archives"));


id0Nodes6Index.push('home/Summary_FR@24840');
id0Nodes6Values.push(new Array(id1Nodes2Index, id1Nodes2Values));
lstIdNodesIndex.push('home/Summary_FR@33360');
lstIdNodesValues.push(new Array(id0Nodes6Index, id0Nodes6Values));


/*****************************************************/
/*		Fonction d'affichage du Menu gauche		*/
/*****************************************************/


function menu_draw(){
	var currentActiveIndex = null;
	var currentIndex = null;
	var cpt = 0;
	var selectionTrouve = false;

	if(currentActiveId){
		for(i=0; (i<lstSelectedIdNodesIndex.length)&&(!selectionTrouve); i++){
			if(lstSelectedIdNodesIndex[i] == currentActiveId){
				selectionTrouve = true;
				cpt = i;
			}
		}
		if(selectionTrouve){
			selectionTrouve = false;
			currentActiveIndex = (lstSelectedIdNodesValues[cpt]).split(",");
		}
	}

	if(currentId){
		for(i=0; (i<lstSelectedIdNodesIndex.length)&&(!selectionTrouve); i++){
			if(lstSelectedIdNodesIndex[i] == currentId){
				selectionTrouve = true;
				cpt = i;
			}
		}
		if(selectionTrouve){
			selectionTrouve = false;
			currentIndex = (lstSelectedIdNodesValues[cpt]).split(",");
		}
	}

	m2_aff = "";
	m2_aff += "<table width=100% cellspacing=2><tr><td></td></tr></table>";
	m2_aff += "<table width=100% border='0' cellspacing='0' cellppushing='0'><tr><td height='4'></td></tr></table>";
	m2_aff += "<TABLE width='100%' border='0' cellspacing='0' cellppushing='0'>";
	m2_aff += "<TR><TD height='30'>&nbsp;";
	m2_aff += "</TD></TR>";
	m2_aff += "</table>";
	m2_aff += "<TABLE width='100%' border='0' cellspacing='0' cellppushing='0'>";
	m2_aff += "  <TR>";
	m2_aff += "    <TD class='menu_right'>";
	m2_aff += "<TABLE BORDER=0 CELLPpushING=0 CELLSPACING=0><TR><TD valign='top'>";

	m_aff = "";
	m_aff += "<TABLE BORDER=0 CELLPpushING=2 CELLSPACING=0><TR><TD valign='top'><B>";

	if(currentIndex){
	if(currentIndex.length > 0){
	if(lstIdNodesIndex.length > 0){
		bAfficheMenuDroit = false;
		var tabKeys = lstIdNodesIndex;
		var tabItems = lstIdNodesValues;
		SelectionLevel0();
		for(o=0;o<lstIdNodesIndex.length;o++){
//debug("Niv0:" + tabKeys[o]);
			if(tabKeys[o] == currentIndex[0]){
				if((tabItems[o][0]).length > 0){
					var tabKeys1 = tabItems[o][0];
					var tabItems1 = tabItems[o][1];
					for(a=0;a<(tabItems[o][0]).length;a++){
//debug("&nbsp;&nbsp;Niv1:" + tabKeys1[a]);
						for(i=0; (i<menuLevel1Index.length)&&(!selectionTrouve); i++){
							if(menuLevel1Index[i] == tabKeys1[a]){
								selectionTrouve = true;
								cpt = i;
							}
						}
						if(selectionTrouve){
							selectionTrouve = false;
						if(tabKeys1[a] == currentIndex[1]){
							if(tabKeys1[a] == currentActiveIndex[1]){
								m_aff += showLevel1Active(menuLevel1Values[cpt], "img_level1_active_open");
							}
							else{
								m_aff += showLevel1Inactive(menuLevel1Values[cpt], "img_level1_inactive_open");
							}
						if((tabItems1[a][0]).length > 0){
								var tabKeys2 = tabItems1[a][0];
								var tabItems2 = tabItems1[a][1];
								for(b=0;b<(tabItems1[a][0]).length;b++){
//debug("&nbsp;&nbsp;&nbsp;&nbsp;Niv2:" + tabKeys2[b]);
									for(i=0; (i<menuLevel2Index.length)&&(!selectionTrouve); i++){
										if(menuLevel2Index[i] == tabKeys2[b]){
											selectionTrouve = true;
											cpt = i;
										}
									}
									if(selectionTrouve){
										selectionTrouve = false;
									if(tabKeys2[b] == currentIndex[2]){
										if(tabKeys2[b] == currentActiveIndex[2]){
											m_aff += showLevel2Active(menuLevel2Values[cpt], "img_level2_active_open");
										}
										else{
											m_aff += showLevel2Inactive(menuLevel2Values[cpt], "img_level2_inactive_open");
										}
										if((tabItems2[b][0]).length > 0){
											var tabKeys3 = tabItems2[b][0];
											var tabItems3 = tabItems2[b][1];
											for(c=0;c<(tabItems2[b][0]).length;c++){
//debug("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Niv3:" + tabKeys3[c]);
												for(i=0; (i<menuLevel3Index.length)&&(!selectionTrouve); i++){
													if(menuLevel3Index[i] == tabKeys3[c]){
														selectionTrouve = true;
														cpt = i;
													}
												}
												if(selectionTrouve){
													selectionTrouve = false;
												if(tabKeys3[c] == currentIndex[3]){
													m_aff += showLevel3Active(menuLevel3Values[cpt], "img_level3_active_open");
													if((tabItems3[c][0]).length > 0){
														bAfficheMenuDroit = true;
														var tabKeys4 = tabItems3[c][0];
														var tabItems4 = tabItems3[c][1];
														for(d=0;d<(tabItems3[c][0]).length;d++){
//debug("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Niv4:" + tabKeys4[d]);
															for(i=0; (i<menuLevel4Index.length)&&(!selectionTrouve); i++){
																if(menuLevel4Index[i] == tabKeys4[d]){
																	selectionTrouve = true;
																	cpt = i;
																}
															}
															if(selectionTrouve){
																selectionTrouve = false;
															if(tabKeys4[d] == currentIndex[4]){
																if(tabKeys4[d] == currentActiveIndex[4]){
																	m2_aff += showLevel4Active(menuLevel4Values[cpt], "Open");
																}
																else{
																	m2_aff += showLevel4Inactive(menuLevel4Values[cpt], "Open");
																}
																if((tabItems4[d][0]).length > 0){
																	var tabKeys5 = tabItems4[d][0];
																	var tabItems5 = tabItems4[d][1];
																	for(e=0;e<(tabItems4[d][0]).length;e++){
//debug("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Niv5:" + tabKeys5[e]);
																		for(i=0; (i<menuLevel5Index.length)&&(!selectionTrouve); i++){
																			if(menuLevel5Index[i] == tabKeys5[e]){
																				selectionTrouve = true;
																				cpt = i;
																			}
																		}
																		if(selectionTrouve){
																			selectionTrouve = false;
																		if(tabKeys5[e] == currentIndex[5]){
																			if(tabKeys5[e] == currentActiveIndex[5]){
																				m2_aff += showLevel5Active(menuLevel5Values[cpt], "Open");
																			}
																			else{
																				m2_aff += showLevel5Inactive(menuLevel5Values[cpt], "Open");
																			}
																			if(tabItems5[e].length > 0){
																				for(f=0;f<(tabItems5[e]).length;f++){
//debug("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;;&nbsp;&nbsp;Niv6:" + tabItems5[e][f]);
																					for(i=0; (i<menuLevel6Index.length)&&(!selectionTrouve); i++){
																						if(menuLevel6Index[i] == tabItems5[e][f]){
																							selectionTrouve = true;
																							cpt = i;
																						}
																					}
																					if(selectionTrouve){
																						selectionTrouve = false;
																					if(tabItems5[e][f] == currentIndex[6]){
																						m2_aff += showLevel6Active(menuLevel6Values[cpt], "Open");
																					}
																					else{
																						m2_aff += showLevel6Inactive(menuLevel6Values[cpt], "Closed");
																					}
																					}
																				}
																			}
																		}
																		else{
																			if(tabKeys5[e] == currentActiveIndex[5]){
																				m2_aff += showLevel5Active(menuLevel5Values[cpt], "Closed");
																			}
																			else{
																				m2_aff += showLevel5Inactive(menuLevel5Values[cpt], "Closed");
																			}
																		}
																		}
																	}
																}
															}
															else{
																if(tabKeys4[d] == currentActiveIndex[4]){
																	m2_aff += showLevel4Active(menuLevel4Values[cpt], "Closed");
																}
																else{
																	m2_aff += showLevel4Inactive(menuLevel4Values[cpt], "Closed");
																}
															}
															}
														}
													}
												}
												else{
													m_aff += showLevel3Inactive(menuLevel3Values[cpt], "img_level3_inactive_closed");
												}
											}
											}
										}
									}
									else{
										if(tabKeys2[b] == currentActiveIndex[2]){
											m_aff += showLevel2Active(menuLevel2Values[cpt], "img_level2_active_closed");
										}
										else{
											m_aff += showLevel2Inactive(menuLevel2Values[cpt], "img_level2_inactive_closed");
										}
									}
									}
								}
							}
						}
						else{
							if(tabKeys1[a] == currentActiveIndex[1]){
								m_aff += showLevel1Active(menuLevel1Values[cpt], "img_level1_active_closed");
							}
							else{
								m_aff += showLevel1Inactive(menuLevel1Values[cpt], "img_level1_inactive_closed");
							}
						}
						}
					}
				}
			}
		}
	}
	}// else debug("=> currentIndex est vide <BR>");
	}// else debug("=> currentIndex est null <BR>");

	m_aff += "</B></TD></TR></TABLE>";
	if(document.getElementById("AREVA_LEFT_MENU"))
		document.getElementById("AREVA_LEFT_MENU").innerHTML = m_aff;

	m2_aff += "</TD></TR></TABLE>";
	m2_aff += "    </TD>";
	m2_aff += "  </TR>";
	m2_aff += "</TABLE>";
	m2_aff += "<TABLE width='100%' border='0' cellspacing='0' cellppushing='0'>";
	m2_aff += "<TR><TD height='10px'>&nbsp;";
	m2_aff += "</TD></TR>";
	m2_aff += "</table>";

	if(document.getElementById("HIDDEN_RIGHT_MENU"))
		document.getElementById("HIDDEN_RIGHT_MENU").innerHTML = m2_aff;

	if(bAfficheMenuDroit){
		if(document.getElementById("AREVA_RIGHT_MENU")) afficheMenuDroit();
	}
}


/*******************************************************/

function SelectionLevel0(){
	var currentActiveIndex = null;
	var nodeSelected = "";
	var idImg = "";
	var urlImg = "";
	var cpt = 0;
	var selectionTrouve = false;

	if(currentActiveId){
		for(i=0; (i<lstSelectedIdNodesIndex.length)&&(!selectionTrouve); i++){
			if(lstSelectedIdNodesIndex[i] == currentActiveId){
				selectionTrouve = true;
				cpt = i;
			}
		}
		if(selectionTrouve){
			selectionTrouve = false;
			currentActiveIndex = (lstSelectedIdNodesValues[cpt]).split(",");
			nodeSelected = currentActiveIndex[0];

			for(i=0; (i<TabLevel0Index.length)&&(!selectionTrouve); i++){
				if(TabLevel0Index[i] == nodeSelected){
					selectionTrouve = true;
					cpt = i;
				}
			}

			if(selectionTrouve){
				idImg = TabLevel0Values[cpt][0];
				urlImg = TabLevel0Values[cpt][1];

				if(document.getElementById('V_1_MENUIMG_' + idImg)){
					document.getElementById('V_1_MENUIMG_' + idImg).src = urlImg;
					SSMENU[idImg][1]= urlImg;
				}
				else{
					SSMENU[idImg][6]= SSMENU[idImg][5];
					ShowMainMenu(SSMENU, intNombreCellulesParColonne, 'AREVA_TOP_MENU');
				}
			}
		}
	}
}
/******************************************************/

function menu_Click(idNode){
	currentId = idNode;
	menu_draw();
}
/*******************************************************/

function openNavMenu(idNode){
	currentActiveId = idNode;
	currentId = idNode;
	menu_draw();
}
/******************************************************/

function afficheMenuDroit(){
	if((document.getElementById('HIDDEN_RIGHT_MENU')) && (bAfficheMenuDroit)){
		document.getElementById('trait').style.visibility='visible';
		bForceEnroule=true;
		enroule();
		document.getElementById('AREVA_RIGHT_MENU').innerHTML = document.getElementById('HIDDEN_RIGHT_MENU').innerHTML;
		document.getElementById('AREVA_RIGHT_MENU').style.display ='block';
	}
	else{
		if(document.getElementById('trait'))
			document.getElementById('trait').style.visibility='hidden';
		bForceEnroule=false;
		deroule();
		document.getElementById('AREVA_RIGHT_MENU').innerHTML = "<table width=100% cellspacing=2><tr><td></td></tr></table>";
		document.getElementById('AREVA_RIGHT_MENU').style.display ='none';
	}
}
/******************************************************/

function debug(txt){
	if(top.DEBUG){
		top.DEBUG.document.write('<A HREF="/home/vide.htm">R.A.Z</A>&nbsp;&nbsp;');
		top.DEBUG.document.write(txt+'<BR>\n');
		window.defaultStatus='Mode Debug is On';
	}
}
/******************************************************/
