jQuery.fn.accessNews = function(settings) {
    settings = jQuery.extend({
        newsHeadline: "Top Stories",
        newsSpeed: "normal",
		slideBy:1
    }, settings);
    return this.each(function(i) {
        aNewsSlider.itemWidth = parseInt(jQuery(".item:eq(" + i + ")",".news_slider").css("width")) + parseInt(jQuery(".item:eq(" + i + ")",".news_slider").css("margin-right"));
        aNewsSlider.init(settings,this);
        /*// Lien pour afficher/masquer toutes les news
		jQuery(".view_all > a", this).click(function() {
            aNewsSlider.vAll(settings,this);
            return false;
        });*/
    });
};
var aNewsSlider = {
    itemWidth: 0,
    init: function(s,p) {
        /* 	// Message d'info sur la desactivation css/javascript
		jQuery(".messaging",p).css("display","none");
		*/
		
        itemLength = jQuery(".item",p).length;
        /* // Lien pour afficher/masquer toutes les news
			if (jQuery(".view_all",p).width() == null) {
            jQuery(".news_items",p).prepend("<p class='view_all'>" + s.newsHeadline + " [ " + itemLength + " total ] &nbsp;-&nbsp; <a href='#'>View All</a></p>");
        }*/
        newsContainerWidth = itemLength * aNewsSlider.itemWidth;
        jQuery(".item_container",p).css("width",newsContainerWidth + "px");
        jQuery(".btn-play",p).css("display","block");
        animating = false;
        
		var myFunction = function() {        	
            if (animating == false) {
                animating = true;
                if (parseInt(jQuery(".item_container",p).css("left")) + parseInt(jQuery(".item_container",p).css("width")) <= aNewsSlider.itemWidth * s.slideBy) {
                	animateLeft = 0;
                } else {                
                	animateLeft = parseInt(jQuery(".item_container",p).css("left")) - (aNewsSlider.itemWidth * s.slideBy);
                }                
                animateLeft = parseInt(jQuery(".item_container",p).css("left")) - (aNewsSlider.itemWidth * s.slideBy);
                if (animateLeft + parseInt(jQuery(".item_container",p).css("width")) > 0) {
                    jQuery(".prev",p).css("display","block");
                    jQuery(".item_container",p).animate({left: animateLeft}, s.newsSpeed, function() {
                        jQuery(this).css("left",animateLeft);
                        // Sur le dernier message, on retourne au d�but
                        if (parseInt(jQuery(".item_container",p).css("left")) + parseInt(jQuery(".item_container",p).css("width")) <= aNewsSlider.itemWidth * s.slideBy) {
                            // on attend 5s pour retourner au 1er message
                        	$(".item_container").oneTime(5000, 'controlled', function() {
                        		jQuery(this).css("left","0");
                        	});
                        }
                        animating = false;
                    });
                } else {
                    animating = false;
                }
            }
		};
        // D�roulement auto des messages toutes les 5s
        $(".item_container").everyTime(5000, 'controlled', myFunction);
		
	    //Gestion du PLAY/STOP
        var active = true;
        jQuery(".btn-play",p).click(function() {
        	active = !active;
        	if (!active) {
				$(".item_container").stopTime('controlled');
				$(".btn-play").removeClass("pause");
				$(".btn-play").addClass("play");
			} else {
				myFunction();
				$(".item_container").everyTime(5000, 'controlled', myFunction);
				$(".btn-play").removeClass("play");
				$(".btn-play").addClass("pause");
			}
        });  
	        
			
	        
        /*
        jQuery(".prev",p).click(function() {
            if (animating == false) {
                animating = true;
                animateLeft = parseInt(jQuery(".item_container",p).css("left")) + (aNewsSlider.itemWidth * s.slideBy);
                if ((animateLeft + parseInt(jQuery(".item_container",p).css("width"))) <= parseInt(jQuery(".item_container",p).css("width"))) {
                    jQuery(".next",p).css("display","block");
                    jQuery(".item_container",p).animate({left: animateLeft}, s.newsSpeed, function() {
                        jQuery(this).css("left",animateLeft);
                        if (parseInt(jQuery(".item_container",p).css("left")) == 0) {
                            jQuery(".prev",p).css("display","none");
                        }
                        animating = false;
                    });
                } else {
                    animating = false;
                }
            }
            return false;
        });
        */
    }
	/*// Lien pour afficher/masquer toutes les news
	,vAll: function(s,p) {
        var o = p;
        while (p) {
            p = p.parentNode;
            if (jQuery(p).attr("class") != undefined && jQuery(p).attr("class").indexOf("news_slider") != -1) {
                break;
            }
        }
        if (jQuery(o).text().indexOf("View All") != -1) {
            jQuery(".next",p).css("display","none");
            jQuery(".prev",p).css("display","none");
            jQuery(o).text("View Less");
            jQuery(".item_container",p).css("left","0px").css("width",aNewsSlider.itemWidth * s.slideBy + "px");
        } else {
            jQuery(o).text("View All");
            aNewsSlider.init(s,p);
        }
    }*/
};
