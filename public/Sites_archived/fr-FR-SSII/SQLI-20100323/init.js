/* 
------------------------------------------------------------------------------*/
var BrowserDetect = {
	init: function () {
			this.browser = this.searchString(this.dataBrowser) 
				|| "An unknown browser";
			this.version = this.searchVersion(navigator.userAgent)
				|| this.searchVersion(navigator.appVersion)
				|| "an unknown version";
			this.OS = this.searchString(this.dataOS)
				|| "an unknown OS";
		},
		searchString: function (data) {
			for (var i=0;i<data.length;i++) {
				var dataString = data[i].string;
				var dataProp = data[i].prop;
				this.versionSearchString = data[i].versionSearch || data[i].identity;
				if (dataString) {
					if (dataString.indexOf(data[i].subString) != -1)
						return data[i].identity;
					}
					else if (dataProp)
						return data[i].identity;
			}
		},
		searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari"
		},
		{
			prop: window.opera,
			identity: "Opera"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]
};
BrowserDetect.init();
/* 
------------------------------------------------------------------------------*/
var browser = "js";
var addclass = "class";
/* OSDetect
------------------------------------------------------------------------------*/
if(BrowserDetect.OS == "Mac")				browser += " mac";
if(BrowserDetect.OS == "Linux")			browser += " linux";
if(BrowserDetect.OS == "Windows")		browser += " win";
/* BrowserDetect
------------------------------------------------------------------------------*/
if(BrowserDetect.browser == "Firefox")	browser += " firefox";
if(BrowserDetect.browser == "Opera" )	browser += " opera";
if(BrowserDetect.browser == "Safari")	browser += " safari";
if(BrowserDetect.browser == "Explorer")addclass = "className";
/* 
------------------------------------------------------------------------------*/
document.getElementsByTagName("HTML")[0].setAttribute(addclass, browser);
/* EOF
------------------------------------------------------------------------------*/