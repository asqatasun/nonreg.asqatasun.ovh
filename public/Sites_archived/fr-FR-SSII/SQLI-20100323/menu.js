// JavaScript Document

<!--
$(function () {
	// On cache les sous-menus
	// sauf celui qui porte la classe "open_at_load" :
	$("ul.subMenu:not('.open_at_load')").hide();
	// On selectionne tous les items de liste portant la classe "toggleSubMenu"

	// et on remplace l'element span qu'ils contiennent par un lien :
	$("li.toggleSubMenu .open").each( function () {
		// On stocke le contenu du span :
		var TexteSpan = $(this).text();
		$(this).replaceWith('<a href="" class="open" title="Masque la liste de ' + TexteSpan + '"><span>' + TexteSpan + '</span></a>') ;
	} ) ;

	// et on remplace l'element span qu'ils contiennent par un lien :
	$("li.toggleSubMenu span").each( function () {
		// On stocke le contenu du span :
		var TexteSpan = $(this).text();
		$(this).replaceWith('<a href="" title="Affiche / Masque la liste ' + TexteSpan + '"><span>' + TexteSpan + '</span></a>') ;
	} ) ;
	

	// On modifie l'evenement "click" sur les liens dans les items de liste
	// qui portent la classe "toggleSubMenu" :
	$("li.toggleSubMenu > a").click( function () {
	$("li.toggleSubMenu .open").each( function () {
			$(this).removeClass("open")				
		} ) ;
		// Si le sous-menu etait deja ouvert, on le referme :
		if ($(this).next("ul.subMenu:visible").length != 0) {
			$(this).next("ul.subMenu").slideUp("normal", function () { $(this).parent().removeClass("open") } );
		}
		// Si le sous-menu est cache, on ferme les autres et on l'affiche :
		else {
			//..........
			//$("ul.subMenu").slideUp("normal", function () { $(this).parent().removeClass("open") } );
			
			//..........
			$(this).next("ul.subMenu").slideDown("normal", function () { $(this).parent("li").addClass("open") } );
		}
		// On emp�che le navigateur de suivre le lien :
		return false;
	});
	
	//-----------------------------------------------//
	
	$("ul.agences:not('.open_at_load')").hide();
	
	$("li.toggleAgences > a").click( function () {
	$("li.toggleAgences a.open").each( function () {
				 
			$(this).removeClass("open")
		} ) ;
		// Si le sous-menu etait deja ouvert, on le referme :
		if ($(this).next("ul.agences:visible").length != 0) {
			$(this).next("ul.agences").slideUp("normal", function () { $(this).parent().removeClass("open") } );
		}
		// Si le sous-menu est cache, on ferme les autres et on l'affiche :
		else {
			//..........
			$("ul.agences").slideUp("normal", function () { $(this).parent().removeClass("open") } );
			
			//..........
			$(this).next("ul.agences").slideDown("normal", function () { $(this).parent("li").children("a").addClass("open") } );
		}
		// On emp�che le navigateur de suivre le lien :
		return true;
	});
	
	//-----------------------------------------------//
	
	$("ul.finances:not('.open_at_load')").hide();
	
	$("li.toggleFinances > a").click( function () {
	$("li.toggleFinances a.open").each( function () {
				 
			$(this).removeClass("open")
		} ) ;
		// Si le sous-menu etait deja ouvert, on le referme :
		if ($(this).next("ul.finances:visible").length != 0) {
			$(this).next("ul.finances").slideUp("normal", function () { $(this).parent().removeClass("open") } );
		}
		// Si le sous-menu est cache, on ferme les autres et on l'affiche :
		else {
			//..........
			$("ul.finances").slideUp("normal", function () { $(this).parent().removeClass("open") } );
			
			//..........
			$(this).next("ul.finances").slideDown("normal", function () { $(this).parent("li").children("a").addClass("open") } );
		}
		// On emp�che le navigateur de suivre le lien :
		return false;
	});
	
	//-----------------------------------------------//
	
	$("ul.sousfinances:not('.open_at_load')").hide();
	
	$("li.toggleSousFinances > a").click( function () {
	$("li.toggleSousFinances a.open").each( function () {
				 
			$(this).removeClass("open")
		} ) ;
		// Si le sous-menu etait deja ouvert, on le referme :
		if ($(this).next("ul.sousfinances:visible").length != 0) {
			$(this).next("ul.sousfinances").slideUp("normal", function () { $(this).parent().removeClass("open") } );
		}
		// Si le sous-menu est cache, on ferme les autres et on l'affiche :
		else {
			//..........
			$("ul.sousfinances").slideUp("normal", function () { $(this).parent().removeClass("open") } );
			
			//..........
			$(this).next("ul.sousfinances").slideDown("normal", function () { $(this).parent("li").children("a").addClass("open") } );
		}
		// On emp�che le navigateur de suivre le lien :
		return false;
	});

} ) ;
// -->