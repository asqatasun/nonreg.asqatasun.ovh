// flash sample code
// 04.11.06 - MH, commented alert pop-up

//Model : general flash object
function flashObj(u, w, h, t, v) {
	this.u = u;
	this.h = (!h) ? 200 : h;
	this.w = (!w) ? 200 : w;
	this.t = (!t) ? window.alert('div target not specified') : t;
	//set to flash version 8 if not specified
	this.v = (!v) ? 6 : v;
}

// View: Display style 1 
flashObj.prototype.basic = function (paramsobj) {
					var targetdiv = document.getElementById(this.t);
					var hasparams = (!paramsobj) ? false : true;
					if(!targetdiv) {window.alert('bad target div');}
					var prms = '' ;
					var embds = '' ;
					if(hasparams) { for (k in paramsobj) { prms+= '<param name="'+k+'" value="'+paramsobj[k]+'">' ; } }
				        var ft = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" ';
					ft += ' codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version='+this.v+',0,0,0"  ';
					ft +=' width="'+this.w+'" height="'+this.h+'"> ';
					ft +=' <param name="movie" value="'+this.u+'"> ';
					ft +=' <param name="quality" value="high"> ';
					ft +=' <param name="menu" value="false"> ';
					if(prms!='') ft+= prms;
					ft +=' <embed src="'+this.u+'" ';
					ft +=' menu="false" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer"  ';
					ft +=' type="application/x-shockwave-flash" width="'+this.w+'" height="'+this.h+'" ' ;
					if(hasparams) { for (k in paramsobj) { embds +=' '+k+'="'+paramsobj[k]+'" '; } }
					if(embds!='') ft+=embds;
					ft+='></embed>';
					ft+=' </object> ';
					// alert(ft);
					targetdiv.innerHTML = ft;
					}	
					
//Controller: not needed at this point
function flashCaller() {}