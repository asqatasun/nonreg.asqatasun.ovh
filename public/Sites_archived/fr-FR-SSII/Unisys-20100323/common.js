//________________
name = 'unisys';



//________________
var navTitle;
var backText;
var backPath;



//____________________start POPUP contact us WINDOW
var newWindow

function newContactUnisysWindow()
{
	newWindow = window.open("","Contact","toolbar=false,scrollbars=true,location=false,resizable=false,status=true, screenX=10,screenY=10,width=500,height=500") 
	newWindow.focus()
}

//____________________start POPUP case study WINDOW
var newWindow

function newCaseStudyWindow() 
{
  newWindow = window.open("","CaseStudy","scrollbars=yes,resizable=no,width=517,height=517") 
  newWindow.focus()
}

// ___________________start MOUSE OVER AND OUT FUCTION for rollovers

function msover_Image(imgName) 
{
  if (true)
  {
document.images[imgName].src = "/corporate/images/admin/misc/"+imgName + "_on.gif";
  }
}

function msout_Image(imgName) 
{
  if (true)
  {
    document.images[imgName].src = "/corporate/images/admin/misc/"+imgName + "_out.gif";
  }
}

//_________________SOL finder Window
		function sol_popupWindow(sol_url)
		{
		window.name = 'mainWindow';
		var popup=window.open(sol_url,'popup','toolbar=no,resizable=yes,scrollbars=no,width=300,height=420');
		}


//___POP-UP SCRIPT FOR ALL SIDEBARS (features)

 
function popwin(theURL,winName,features) 
{
      newWindow = window.open(theURL,winName,features);
      newWindow.focus();
} 

<!-- POP-UP WINDOW FOR E-MAIL NEWS-->
function makeEmailWindow(path)
{
  makeNewEmailWindow = 
window.open(path,'emailArticle','scrollbars=no,resizable=no,left=180,top=150,screenX=180,screenY=150,alwaysRaised=yes,width=330,height=360')
  makeNewEmailWindow.focus()
}

//______END POP_UP FOR FEATURES

var loadImgCount = 0;

function loadImgCache(outImgPath)
{
	var outImg = outImgPath.substr(outImgPath.lastIndexOf("/")+1)
	var onImg = outImg.replace(".gif","_on.gif");
	var onImgPath = outImgPath.substring(0,outImgPath.lastIndexOf("/"));
	onImgPath = (onImgPath.substring(0,onImgPath.lastIndexOf("/")+1) + "admin_only/" + onImg);

	//this function substitutes for Unisys Cache on load. Its called from the Links Object  JWL 2.27.02
	//will only cache if there is a mouse over
	//need to make array for more than one
	var feature1_out = new Array()
	feature1_out[loadImgCount] = new Image();
	feature1_out[loadImgCount].src = outImgPath;

	var feature1_on = new Array()
	feature1_on[loadImgCount] = new Image();
	feature1_on[loadImgCount].src = onImgPath;

	loadImgCount++

}

function msoverFeature(outImgPath) 
{
	//was in Unisys supplied feature.js moved to a block on common.js
	//had to manipulate the Unisys code to accept a dynamic path in the function var --JWL 2.27.02

	var outImg = outImgPath.substr(outImgPath.lastIndexOf("/")+1)
	var onImg = outImg.replace(".gif","_on.gif");
	var onImgPath = outImgPath.substring(0,outImgPath.lastIndexOf("/"));
	onImgPath = (onImgPath.substring(0,onImgPath.lastIndexOf("/")+1) + "admin_only/" + onImg);

	feature_on = new Image();
	feature_on.src = onImgPath;


	//checks for mouse over img
	//fileSize works for IE, height for Netscape	
	if (feature_on.fileSize)
	{
		if (feature_on.fileSize > 0) 
		{
			document.images[outImgPath].src = onImgPath;
		}
	}
	else
	{
		if (feature_on.height > 0)	
		{
			document.images[outImgPath].src = onImgPath;
		}
	}	
}


function msoutFeature(outImgPath) 
{
	//was in Unisys supplied feature.js moved to a block on common.js
	//had to manipulate the Unisys code to accept a dynamic path in the function var --JWL 2.27.02
	
	//checks for mouse over img
	feature_out = new Image();
	feature_out.src = outImgPath;

	//checks for mouse over img
	//fileSize works for IE, height for Netscape	
	if (feature_out.fileSize)
	{
		if (feature_out.fileSize > 0) 
		{
			document.images[outImgPath].src = outImgPath;
		}
	}
	else
	{
		if (feature_out.height > 0)	
		{
			document.images[outImgPath].src = outImgPath;
		}
	}	

}
 function makeCaseHistoryEmailWindow(path) 
 {
	makeNewCaseHistoryEmailWindow =  
	window.open(path,'emailArticle','scrollbars=no,resizable=no,left=180,top=150,screenX=180,screenY=150,alwaysRaised=yes,width=330,height=360') 
	makeNewCaseHistoryEmailWindow.focus() 
 } 
