         
         /*Function for changing image on mouseover and mouseout event.*/
         function changeimage(object,mouseover)
         {
			/*Check if the calling event came from a mouseover event*/
            if (mouseover)
            {
			   /* mouseover argument equal to true */
               /* Find the divider image before the navimage then change the image source to  divider image on */
               document.getElementById("TopNav" + (parseInt(object.id.substr(6,object.id.length)) - 1)).src=nav_divider_on;  
               
               /* Change the image source of the calling Image to nav image on */
               object.src=nav_image_on[parseInt(object.id.substr(6,object.id.length))];
               
               /*Find the divider image after the navimage then change the image source to divider image on */
               document.getElementById("TopNav" + (parseInt(object.id.substr(6,object.id.length)) + 1)).src=nav_divider_on;
            }
            else
            {
			   /* mouseover argument equal to false */	
               /* Find the divider image before the navimage then change the image source to  divider image off */
               document.getElementById("TopNav" + (parseInt(object.id.substr(6,object.id.length)) - 1)).src=nav_divider_off;  
               
               /* Change the image source of the calling Image to nav image off */
               object.src=nav_image_off[parseInt(object.id.substr(6,object.id.length))];
               
               /*Find the divider image after the navimage then change the image source to divider image off */
               document.getElementById("TopNav" + (parseInt(object.id.substr(6,object.id.length)) + 1)).src=nav_divider_off;
            }
            try
            {
				setActiveImage(ActiveImageID) 
			}
			catch(err)
			{
			}
         }

		function setActiveImage(objectid) 
		{
			   try
			   {
			   var object = document.getElementById(objectid)
			   
			   if (object==null) return;
			   
			   /* mouseover argument equal to true */
               /* Find the divider image before the navimage then change the image source to  divider image on */
               document.getElementById("TopNav" + (parseInt(objectid.substr(6,objectid.length)) - 1)).src=nav_divider_on;  
               
               /* Change the image source of the calling Image to nav image on */
               object.src=nav_image_on[parseInt(objectid.substr(6,objectid.length))];
               
               /*Find the divider image after the navimage then change the image source to divider image on */
               document.getElementById("TopNav" + (parseInt(objectid.substr(6,objectid.length)) + 1)).src=nav_divider_on;
               }
               catch(err)
               {
               }
		}