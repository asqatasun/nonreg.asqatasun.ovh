//*****************************************************************************************
//
// REVISION HISTORY
//
// Date Created: 08/01/2006
// Author:       Carlos Angelo A. Mendoza
// Description:	 Javascript source code for nth Visitor 
//
// Revision Date     Author     Description 
// -------------     ------     -----------         
// 09.05.2006		DAV		Update for RMT 1230.
// 07.09.2008		MBS		RMT 3560: Survey will appear for every user.
// 07.16.2008		MXT		RMT 3574: Updated Cookie expiration date in setCookie() function.
// 09.02.2008       WCE     RMT 3566: Revert back the changes made in RMT 3560.
// 08.27.2009       EFJ     Remove Nth Visitor Survey Popup 
//*****************************************************************************************


function CheckCookie()
{

}

function generate(x, y) 
{
	var range = y-x+1;
	var l = ("" + range).length;
	var num = (Math.floor(Math.random() * Math.pow(10,l)) % range) + parseInt(x);
	return num;
} 

function GetCookie(name) 
{
	var arg = name + "=";
	var alen = arg.length;
	var clen = document.cookie.length;
	var i = 0;
	while (i < clen) 
		{
			var j = i + alen;
			if (document.cookie.substring(i, j) == arg)
			return getCookieVal(j);
			i = document.cookie.indexOf(" ", i) + 1;
			if (i == 0) break; 
		}
return null;
}

function getCookieVal(offset) 
{
	var endstr = document.cookie.indexOf (";", offset);
	if (endstr == -1)
	endstr = document.cookie.length;
	return unescape(document.cookie.substring(offset, endstr));
}

function setCookie() 
{    
	var currDate = new Date();
	currDate.setMonth(11,31);
	currDate.setHours(23,59,59);	
	document.cookie = "SurveyAccenture=Surveyshown; expires=" + currDate.toGMTString() + "; path=/;";	
} 