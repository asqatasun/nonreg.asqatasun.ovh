//*****************************************************************************************
//
// REVISION HISTORY
//
// Date Created: 04/15/2009
// Author:       EHA
//
// Revision Date     Author     Description 
// -------------     ------     -----------         
// 04.08.09			  EHA		RMT 4020 - Added functions that load and display XML 
// 04.14.09           APD       RMT 4020 - Added functions rendering that search 
//										   suggestions text box (front end)
//*****************************************************************************************

var isClose = false; 
var origVal = ""; 
var rowHighlighted; 
var pos = 0; 
var searchBox;
var searchSuggestion = false;
var divID = "";
var chosenRow = ""; 
var timeOut;


// retains input on search text box 
function SearchBoxOnFocus(searchTextBox) 
{
	if (searchSuggestion == false)		
		searchTextBox.value = "";
}

// detects keyboard events
function Detect(searchTextBox,e)
{	
	var KeyID = e.keyCode;
	
	var backspaceKey = 8;
	var enterKey = 13;	
	var escapeKey = 27;	
	var arrowUpKey = 38;
	var rightKey = 39;	
	var arrowDownKey = 40;
	var printScreenKey = 44;
	var deleteKey = 46;
	
	searchBox = searchTextBox;
	
	if (GetParentDiv(searchBox) != "SiteSearchControlBottomBasic_pnlBasic")
	{
		switch (KeyID)
		{
			case printScreenKey:
				break;
			
			case enterKey:
				Collapse(searchTextBox);
				break;
				
			case backspaceKey:  
			{			
				origVal = searchTextBox.value;
				if (isClose == false)
					{
						clearTimeout(timeOut);
						timeOut=setTimeout("Expand()",200)
					}
				if (Ltrim(searchTextBox.value).length == 0)
					isClose = false;
				pos = 0;
			}
				break;
				
			case deleteKey:
			{			
				origVal = searchTextBox.value;
				if (isClose == false)
					{
						clearTimeout(timeOut);
						timeOut=setTimeout("Expand()",200)
					}
				if (Ltrim(searchTextBox.value).length == 0)
					isClose = false;
				pos = 0;
			}
				break;
				
			case escapeKey:
				{ 
					if(document.getElementById("row1") != null) 
						Collapse(searchTextBox);
					searchTextBox.value = origVal;
				}
				break;
				
			case rightKey:
				OnRightKey(rowHighlighted);
				break;
				
			case arrowDownKey:
			{
				OnDownKey(searchTextBox);				
			}
				break;

			case arrowUpKey:
			{
				OnUpKey(searchTextBox);
			}	
				break;
					
			default:
			{
				origVal = searchTextBox.value;
				if (isClose==false) 
				{
					clearTimeout(timeOut);
					timeOut=setTimeout("Expand()",250)
				}
			}
		}
	}	
}

// displays search suggestion text box
function Expand ()
{    
	if (isClose==false) 
	{
		divID = GetSearchSuggestDiv(searchBox);
		var state = document.getElementById(divID);
		var width = SetSearchSuggestWidth(searchBox);
		
		if ((Ltrim(searchBox.value).length) > 0)
		{
			origVal = Ltrim(searchBox.value);
			
			if (window.ActiveXObject)  
				state.innerHTML = DisplayResult(origVal, width);
			else if (document.implementation && document.implementation.createDocument)  
				state.appendChild(DisplayResult(origVal, width));
			
			if(document.getElementById("row1") != null) 
				state.className = "expand";		
			else 
				Collapse(searchBox);
		}	
		else 
		{
			Collapse(searchBox);
			isClose = false;
		}	
	}
}

// hides the search suggestion text box 
function Collapse (searchTextBox)
{
	divID = GetSearchSuggestDiv(searchTextBox);
	var state = document.getElementById(divID);
	state.className = "collapse";
	state.innerHTML = "";
	searchSuggestion = true;
	searchTextBox.focus();	
}

//Displays resulting search suggestions
function DisplayResult(input, width)
{
var str = input;
var firstChar=String(str).substring(0,1);
var xmlPath = "/Accenture/XML/SearchSuggestion_"

input = input.replace(",","&#44");
input = input.replace(";","&#59");

var param = "width," + width + ";rawinput," + input;

if (IsNumeric(firstChar))
	{var xmlPath = xmlPath + "Numeric.xml";}
else
	{var xmlPath = xmlPath + firstChar + ".xml"; }

  var resultSuggestions = xsltTransform(xmlPath,"/Accenture/XSLT/SearchSuggestion.xslt",param);
  Collapse(searchBox);
  return(resultSuggestions);
}

// shows table when right arrow key is pressed
function OnRightKey (row)
{
	Expand(searchBox);
	Dehighlight(row);
	pos = 0;
}

// handles up Arrowkey event
function OnUpKey (searchTextBox)
{
	if(document.getElementById("row1") != null)
	{
		var row = document.getElementById('row'+pos);
		
		if (pos==0)
		{
			//saves original value of search text field
			origVal = searchTextBox.value;
			pos = document.getElementById('searchSuggest').rows.length - 1;
		}
		
		else 
		{
			Dehighlight(row);
			pos--
		}
		
		if (pos==0)
		{
			searchTextBox.value = origVal; //displays original value
			chosenRow = "";
		}
		else 
		{
			var data = document.getElementById('data'+pos);
			row = document.getElementById('row'+pos)
			Highlight(row);
			
			if (window.ActiveXObject)  	
				searchTextBox.value = data.innerText;  // IE
			else if (document.implementation && document.implementation.createDocument)  
				searchTextBox.value = data.textContent; //firefox	
			chosenRow = searchTextBox.value;						
		}	
	}
}

// handles down Arrowkey event
function OnDownKey(searchTextBox)
{
	if(document.getElementById("row1") != null)
	{	
		var row = document.getElementById('row'+pos);
						
		if ((pos==0) && (origVal.lenght!=0))					
				origVal = searchTextBox.value;					
		else 
			Dehighlight(row);				
		
		if (pos!=(document.getElementById('searchSuggest').rows.length-1))
			pos++;
		else if (pos>=(document.getElementById('searchSuggest').rows.length-1))
			pos = 0;
		
		var data = document.getElementById('data'+pos);
		
		if (pos==0)
		{
			searchTextBox.value = origVal;	
			chosenRow = "";
		}					
		else 
		{
			row = document.getElementById('row'+pos)
			Highlight(row);
			
			if (window.ActiveXObject)  
				searchTextBox.value = data.innerText;  // IE
			else if (document.implementation && document.implementation.createDocument)  
				searchTextBox.value = data.textContent; //firefox		
			chosenRow = searchTextBox.value;			
		}
	}
}

// closes search suggest textbox when any part of the page is clicked 
function OnBlurSearchTextBox (searchTextBox)
{
	var innerTextVar;
	
	if (window.ActiveXObject)  
		innerTextVar = divID.InnerText ;  
	else if (document.implementation && document.implementation.createDocument)  
		innerTextVar = divID.textContent ; 
	
	if (innerTextVar == "")
		Collapse(searchTextBox);
		
	divID = GetSearchSuggestDiv(searchTextBox);
	var state = document.getElementById(divID);
	
	if (state.innerHTML != "")
	{	
		if (chosenRow == "")
			searchTextBox.value = origVal;
		else if (chosenRow == "CloseLink")
			{
				searchTextBox.focus();
				searchTextBox.value = origVal;
				isClose = true;		
			}
		else
			{
				searchTextBox.focus();
				searchTextBox.value = chosenRow;	
			}
	}
	
	state.className = "collapse";
	state.innerHTML = "";
	searchSuggestion = true;
	chosenRow = "";		
}

// handles mouse out event 
function SuggestMouseOut()
{	
	chosenRow = "";	
}

// highlights row when mouse overed
function SuggestMouseOver (row)
{	
	rowHighlighted = document.getElementById("row" + pos);
	if (rowHighlighted != undefined)
		Dehighlight(rowHighlighted);	
	Highlight(row);
	pos = row.id.replace("row","");
	
	if (window.ActiveXObject)  
		chosenRow = document.getElementById("data" + pos).innerText;   
	else if (document.implementation && document.implementation.createDocument)  
		chosenRow = document.getElementById("data" + pos).textContent; 
}

// highlights row on search suggestion table
function Highlight (row)
{
	row.className = "highlight";
	if (row.id == "row1")
		document.getElementById("suggestions").className = "suggestHighlight";
	rowHighlighted = row;
}

// dehighlights previous row on search suggestion table
function Dehighlight (row)
{
	if (document.getElementById("suggestions")!= null)
	{
		document.getElementById("suggestions").className = "suggestionsText"
		row.className = "dehighlight";
		rowHighlighted = row; 
	}
}

// hides search suggestion table when "close" link is pressed
function SuggestClosed (row)
{
	Collapse(searchBox);
	isClose = true;
}

// gets parent DIV
function GetParentDiv (node)
{
	
	if ((node.tagName == "DIV") || ((node.tagName == "TABLE") && (node.id != "")))
		return(node.id);	
	else 
		return(GetParentDiv(node.parentNode));	
	
}

// gets name of search text box 
function GetSearchSuggestDiv (searchTextBox)
{ 	
  	if (Ltrim(searchTextBox.id) == "SiteSearchControlStandard_txtBasicSearch")
		 return("searchSuggestPage");
	else 
		return("searchSuggestBanner");
}

// sets width of search suggest textbox
function SetSearchSuggestWidth(searchTextBox)
{
	if (Ltrim(searchTextBox.id) == "SiteSearchControlStandard_txtBasicSearch")
			return("240px");
	else 
		return("180px");
}

function IsNumeric(numstr)
{
    var mystring = numstr;
    var IsNumber=false;

	if ((mystring.match(/^\d+$/)) || (mystring.match(/^\W+$/)))
        IsNumber=true; 

	return IsNumber;

}

function Ltrim(str) 
{
	var chars = chars || "\\s";
	return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}
 