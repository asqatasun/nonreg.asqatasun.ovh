/* Affiche / cache un sous menu */
function subMenu(num_id, hide) {	
	var obj=document.getElementById('sub-menu-' + num_id);
	
	if(obj) {
		switch( hide ) {
			case false	:
				obj.style.display='block';
				break;
			default		:
				obj.style.display='none';
		}
	}
}


/* Fonction de callback */
function ajaxfilemanager(field_name, url, type, win) {
	var ajaxfilemanagerurl = "/js/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
	switch (type) {
		case "image":
			break;
		case "media":
			break;
		case "flash": 
			break;
		case "file":
			break;
		default:
			return false;
	}
	var fileBrowserWindow = new Array();
	fileBrowserWindow["file"] = ajaxfilemanagerurl;
	fileBrowserWindow["title"] = "Ajax File Manager";
	fileBrowserWindow["width"] = "782";
	fileBrowserWindow["height"] = "440";
	fileBrowserWindow["close_previous"] = "no";
	tinyMCE.openWindow(fileBrowserWindow, {
	  window : win,
	  input : field_name,
	  resizable : "yes",
	  inline : "yes",
	  editor_id : tinyMCE.getWindowArg("editor_id")
	});
	
	return false;
}