var isLoadingHtml = false;
var isIE = false;
var request;
var globalDivId;
var globalAction;

var globaltb1;
var globaltb2;

var lastpostalcode = '';
var lasthousenumber = '';

var callbackfunc;

function loadAjaxDoc(url, CallBackFunction) {
    callbackfunc = CallBackFunction;
    if (window.XMLHttpRequest) 	{ 	// no IE
        request = new XMLHttpRequest();
        request.onreadystatechange = checkStatusAjaxDoc;
        request.open("GET", url, true);
        request.send(null);
    } 
	else if (window.ActiveXObject) { // IE
        isIE = true;
        request = new ActiveXObject("Microsoft.XMLHTTP");
        if (request) {
            request.onreadystatechange = checkStatusAjaxDoc;
            request.open("GET", url, true);
            request.send();
        }
    }
}

function loadHtmlSnippetGet(url, divID, action) {
	globalDivId = divID;
	globalAction = action;
	
	if (isLoadingHtml && request != null)
	{
  		request.abort();
  	}
  		
	isLoadingHtml = true;
	
    if (window.XMLHttpRequest) 	{ 	// no IE
        request = new XMLHttpRequest();
        //alert(request + " / NO IE /  " + url);
        request.onreadystatechange = checkStatus;
        request.open("GET", url, true);
        request.send(null);
    } 
	else if (window.ActiveXObject) { // IE
        isIE = true;
        request = new ActiveXObject("Microsoft.XMLHTTP"); 
        //alert(request + " / WITH IE /  " + url);
        if (request) {
            request.onreadystatechange = checkStatus;
            request.open("GET", url, true);
            request.send();
        }
    }
}

function loadHtmlSnippet(url, divID, action) {
	globalDivId = divID;
	globalAction = action;
	
	if (isLoadingHtml && request != null)
	{
  		request.abort();
  	}
  		
	isLoadingHtml = true;
	
    if (window.XMLHttpRequest) 	{ 	// no IE
        request = new XMLHttpRequest();
        //alert(request + " / NO IE /  " + url);
        request.onreadystatechange = checkStatus;
        request.open("POST", url, true);
        request.send(null);
    } 
	else if (window.ActiveXObject) { // IE
        isIE = true;
        request = new ActiveXObject("Microsoft.XMLHTTP"); 
        //alert(request + " / WITH IE /  " + url);
        if (request) {
            request.onreadystatechange = checkStatus;
            request.open("POST", url, true);
            request.send();
        }
    }
}

function AjaxServerValidate(url, arguments) {
	if (isLoadingHtml && request != null) 
  		request.abort();

	isLoadingHtml = true;
	
	request = getXMLHttpRequester();
    if (request != null) 	{ 	
        request.open("GET", url, false);
        request.send(null);
    }
    else
    {
        arguments.IsValid = false;
        alert('Your browser does not support AJAX');
        return;
    }

    arguments.IsValid = (request.responseText == 'true')
	isLoadingHtml = false;
}

function checkStatus() {
    //alert(request.readyState);

    if (request.readyState == 4) {
		if (request.status == 0)  
		{
			placeSnippet(); 
		}
		else if (request.status == 200)
		{
    		placeSnippet();
    	}
        else {
			isLoadingHtml = false;
            //alert("Fout bij ophalen XML data:\n(" + request.status + ") " + request.statusText);
        }
    }
}
function checkStatusAjaxDoc() {
    if (request.readyState == 4) {
		if (request.status == 0)  
		{
			callbackfunc(request); reso
		}
		else if (request.status == 200)
		{
    		callbackfunc(request);
    	}
        else {
            //alert("Fout bij ophalen checkStatusAjaxDoc data:\n(" + request.status + ") " + request.statusText);
        }
    }
}

function placeSnippet() {
	if (!isLoadingHtml)	return; 
	var responsehtml = request.responseText;
	if(globalAction == 'getModelChapter')
	{
	    //alert('getModelChapter: ' + responsehtml + "ajax.js : 126");
	    var el = document.getElementById(globalDivId);
	    if (el != null) 
	    {
	        //alert(responsehtml);
            el.innerHTML = responsehtml;
        }
        
	}
	
    if (globalAction == 'delete')
    {
        var el = document.getElementById(globalDivId);
        if (el != null)
            el.style.display = 'none';
    }

    if (globalAction == 'getaddressdata')
    {
        if (responsehtml.length > 2) 
        {
            var ar = responsehtml.split(";");
            if (globaltb1 != null)
            {
                globaltb1.value = ar[0];
            }
            if (globaltb2 != null)
            {
                globaltb2.value = ar[1];
            }
        }
    }    
	isLoadingHtml = false;
}

function getXMLHttpRequester() {
    var request;

    if(window.XMLHttpRequest)
    { 
        request = new XMLHttpRequest(); 
    } 
    else if(window.ActiveXObject) 
    { 
        var ie_versions = ["MSXML2.XMLHttp.5.0", "MSXML2.XMLHttp.4.0", 
        "MSXML2.XMLHttp.3.0", "MSXML2.XMLHttp",
        "Microsoft.XMLHttp"];

        for(var i=0; i <ie_versions.length; i++) 
        {
            try 
            {
                request = new ActiveXObject(ie_versions[i]);
                break;
            }
            catch (error) {}
        }
    }

    return request;
}