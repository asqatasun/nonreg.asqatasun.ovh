function switchCountries(locale) {
	_mlc = 'Selector/' + locale;
	
	/**** set domestic locales for blue martini 
	var domestic = false;
	var curLocale = '<c:out value="${current_locale}"/>';
	if (curLocale == 'en_US') {
	domestic = true;
	} else if (curLocale == 'en_CA') {
	domestic = true;
	} else if (curLocale == 'fr_CA') {
	domestic = true;
	} else {
	domestic = false;
	}	
	if (domestic == true) {	*****/
	
	
	switch(locale) {
	 case 'china':
		_hbPageView('country_select', _mlc);
		window.location.href = 'http://www.harley-davidson.cn/';
		break;
	 case 'japan':
		_hbPageView('country_select', _mlc);
		window.location.href = 'http://www.harley-davidson.co.jp/';
		break;
	 case 'india':
		_hbPageView('country_select', _mlc);
		window.location.href = 'http://www.harley-davidson.in/';
		break;

	 case 'es_MX':
	 case 'da_DK':
	 case 'el_GR':
	 case 'no_NO':
	 case 'pl_PL':
	 case 'pt_BR':
	 case 'en_AA':
	 case 'en_ZZ':
	 case 'en_XX':
		_hbPageView('country_select', _mlc);
		window.location.href = '/wcm/selectLocale.h-d?locale=' + locale;
		break;

	 default:
		_hbPageView('country_select', _mlc);
		window.location.href = '/' + locale + '/Content/Pages/home.html';
		break;

	}
}
