/**
 * @author Stan Carrico, WHITTMANHART Interactive
 * Dec, 2009, INT version
 *
 */


//GLOBAL VARS
var linkArray = new Array();
var domLinks = new Array();
var docBody;


// YUI shorthand
var YUC = YAHOO.util.Connect;
var YUD = YAHOO.util.Dom; 
var YUE = YAHOO.util.Event;
var YUS = YAHOO.util.StyleSheet;


YUS('homeTemp').set("#" + 'home_source', {
    display : "none"
});

if (typeof home == "undefined" || !home) {
	var home = {};
}

YAHOO.util.Event.onDOMReady(function(){	
	
	home.initPage();

});




home = {
	
	initPage : function (){	
		
		domLinks = YUD.getElementsBy(function(el){return true;},'a','home_source');
		
		for (var i=0; i<domLinks.length; i++) {
			linkArray[i] = {
				name: domLinks[i].name,
				href: domLinks[i].href,
				onclick: domLinks[i].onclick,
				target: domLinks[i].target
			};
			domLinks[i].href = "event:" + i;
			domLinks[i].onclick = "";
			
		}		
		
		home.initFlash();
		
	},
	
	initFlash : function (){
           
            flashvars = {
					locale: "fr_FR"
				};
            params = {
                  wmode: "opaque",
                  bgcolor: '#000000'
            };
            attributes = {
                  id: "home_flash_app",
                  name: "home_flash_app"
            };
            /* removed, not in use, required for liquid layout
            function flashSize () {
                        this.origWidth = 1038;
                        this.origHeight = 643;
                        this.flashProp = .619; //this.origHeight / this.origWidth;
                        this.viewport = YUD.getClientRegion();
                        this.initWidth = this.viewport.right - this.viewport.left;
                        if(this.initWidth < this.origWidth){
                              this.initWidth = this.origWidth;
                        }
                        //console.log('computed width: ',this.initWidth);
                        this.initHeight = Math.round(this.initWidth * this.flashProp);
                        //console.log('computed height: ',this.initHeight);
                        if(this.initHeight < this.origHeight){
                              this.initHeight = this.origHeight;
                        }
                        return true;
            };
 
            var handleResize = function(){
                  
                  var computedSize = new flashSize();
                  
                  //console.log('handleResize', 'initWidth: ', computedSize.initWidth, 'initHeight: ', computedSize.initHeight);
                  
                  swfobject.getObjectById("home_flash_app").width = computedSize.initWidth;
                  swfobject.getObjectById("home_flash_app").height = computedSize.initHeight;
            };
            
            YUE.addListener(window, 'resize', handleResize); 
            
            var initSize = new flashSize();
            */
			
            //swfobject.embedSWF(swfUrl, id, width, height, version, expressInstallSwfurl, flashvars, params, attributes, callbackFn)
            
			swfobject.embedSWF('/en_US/Media/flash/Apps/INT_Homepage/HD_Homepage.swf', 'home_flash', '980', '630', '9.0.115.0', '',flashvars,params,attributes,home.embedCallback);   
            
			//REMOVE, enables liquid layout --> swfobject.embedSWF('HD_Homepage.swf', 'home_flash', initSize.initWidth, initSize.initHeight,'9.0.115.0', '',flashvars,params,attributes,home.embedCallback);   
      },
	embedCallback : function(response){
		
		if(response.success !== true){
			YUS('homeTemp').disable();
			for (var i=0; i<domLinks.length; i++) {
				domLinks[i].href = linkArray[i].href;
				domLinks[i].onclick = linkArray[i].onclick;
			}
		}
		
	}
};
