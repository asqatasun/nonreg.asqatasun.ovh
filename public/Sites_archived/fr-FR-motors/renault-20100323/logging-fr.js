/* Touch Clarity logging request. http://www.touchclarity.com
 * Site identifier code for Renault UK
 * Copyright (c) Touch Clarity Ltd 2001-2004. All rights reserved. Patent Pending.
 * Change the value of tc_logging_active to switch off logging on the site. */

if (typeof tc_logging_active == 'undefined') tc_logging_active = true;

tc_site_id = 377;
tc_server_url = "renault.touchclarity.com";
tc_log_path = "/js";
document.write("<scr"+"ipt language='JavaScript' type='text/javascript' src='"+tc_log_path+"/logging-code.js'></scr"+"ipt>");
