function cImageRandomizer() 
{
	/* ********************************************************************************************************************************* */
	/*						constants 																*/
	/* ********************************************************************************************************************************* */
	// each time specification is done in milliseconds
	
	var TIME_TO_NEXT_IMAGE = 3000;		// duration of an image of the slideshow is displayed. (including time to fade in and fade out)
	var LOGO_TIME_NEXT_IMAGE = 3000;	// duration the logo is displayed (including time to fade in and fade out)
	
	var TIME_TO_FADE_IN = 700;			// duration for fade-in effect
	var FADE_IN_STEPS = 10;				// number of step the fade-in effect will be devided in
	
	var TIME_TO_FADE_OUT = 700;			// duration for fade-out effect
	var FADE_OUT_STEPS = 10;			// number of step the fade-out effect will be devided in
	var FADE_OUT_TOLERANCE = 200;		// because processing take some time, calculate some extra time to be able to finish fade-out effect in time	
	
	var SHOW_LOGO_EACH_IMAGE = 5;		// determine how often the logo will be shown. Currently every fifth image....
	
	var IMAGE_HOME = './images/';		// relative path to the image base directory
	var IMAGE_SLIDESHOW_HOME = IMAGE_HOME + 'slideshow/';	// relative path to the folder which contains all images for the slideshow
	var IMAGE_ANIMATION_HOME = IMAGE_HOME + 'animation/'; 	// relative path to the folder which contains the images for the animation to simulate fade-in and fade-out effect.
	
	var SLIDESHOW_CONFIG_FILE_NAME = 'slideshow.xml';		// file name of the slideshow configuration file
	
	var FADE_IN = 1;
	var FADE_OUT = 0;
	
	/* ********************************************************************************************************************************* */
	/*						variables 																*/
	/* ********************************************************************************************************************************* */
	// two-dimensional array. Index of first dimension determine the category. Index of second dimension contains the image path
	var maMap = new Array();
	
	// path to Suzuki logo
	var mtSuzukiLogo = "";		
		
	// some browser doesn't supported fade-in/-out effects via javascript. Use images instead
	var mbUseImageAnimation = false;
	
	// some browser doesnt' support fade-in/-out effects via Javascript and doesn't support transparent image. Turn all effects of that stuff...
	var mbNoEffects = false;
	
	// signalizes that the current image is the first image. This means that the user just have entered the page
	var mbIsFirstImageOnPageLoad = true;
	
	// determine the sequence of the categories which are displayed
	// 0 = auto, 1 = moto, 2 = marine, 3=atv
	var maCategoryCycle = new Array(0,1,3,2);
	
	// internal counter to determine which category have to be displayed and when the logo have to be shown
	var miImageCount = 5;
	var miCategoryCount = 0; 
	
	// determine which image size have to be used
	var mbUseSmallImages = false;
	
	// lock to allow either fade-in or fade-out
	var miMode = "";
	
	// number of images which should be preloaded
	var miImagesToPreload = 0;
	
	
	/* ********************************************************************************************************************************* */
	/*						methods 																*/
	/* ********************************************************************************************************************************* */			
	/**
	* 
	*/
	this.start = function() {
		// No-JavaScript Bild ausblenden
		document.getElementById('contentNoJavaScript').style.display = 'none';
		
		// Bildschirmaufl�schung und Browserabh�ngigkeiten auswerten
		determineSystem();	
		setUpContentArea();
		
		//  Inhaltsbereich zun�chst ausblenden							
		document.getElementById('contentImgCover').style.display = 'none';
		document.getElementById('contentImageAnimation').style.display = 'none';
		document.getElementById('contentImg').style.display = 'none';
		document.getElementById('content').style.display = 'block';			
		document.getElementById('content').style.backgroundColor = '#000000';		
		document.getElementById('loadingImg').style.display = 'block';
		
		// Bilder laden
		loadImages();		
	}
	
	
	/**
	* 
	*/
	this.startSlideshow = function() {	
		//  Inhaltsbereich wieder ausblenden: je nach Modus entsprechende Bereiche ein- bzw. ausblenden
		document.getElementById('loadingImg').style.display = 'none';
		
		document.getElementById('content').style.display = 'block';
		document.getElementById('content').style.backgroundColor = '';
		
		document.getElementById('logoTopRight').style.display = 'block';	
		document.getElementById('contentImg').style.display = 'block';
		
		// Animations-Bereich resetten
		if( mbNoEffects == true ) {
			document.getElementById('contentImgCover').style.display = 'none';
			document.getElementById('contentImageAnimation').style.display = 'none';
		} else {
			if( mbUseImageAnimation == true ) {	
				document.getElementById('contentImgCover').style.display = 'none';
				document.getElementById('contentImageAnimation').style.display = 'block';
			} else {
				document.getElementById('contentImgCover').style.display = 'block';
				document.getElementById('contentImageAnimation').style.display = 'none';
			}
		}
				
		// NoScript-Bereich ausblenden
		if( document.getElementById('contentNoJavaScript') ) {			
			document.getElementById('contentNoJavaScript').style.display = 'none';
		}
			
		// starten ....
		ImageRandomizer.setRandomImage();
	}
	
	
	/**
	* Bilder anzeigen
	*/
	this.setRandomImage = function() {
		miImageCount += 1;
		
		// neues Bild speichern. jedes 4. Bild Suzuki Logo anzeigen
		if( miImageCount >= SHOW_LOGO_EACH_IMAGE ) {
			miImageCount = 0;
			
			// kleines Logo rechts-oben ausblenden
			document.getElementById('logoTopRight').style.display = 'none';
			
			// Logo anzeigen
			document.getElementById('contentImg').src = mtSuzukiLogo;
						
			// festlegen, wann das n�chste Bild angezeigt werden soll
			setTimeout('ImageRandomizer.setRandomImage()', LOGO_TIME_NEXT_IMAGE );
			
			// festlegen, wann der fade-out Effekt gestartet werden soll
			setTimeout('ImageRandomizer.fadeOut()', LOGO_TIME_NEXT_IMAGE - TIME_TO_FADE_OUT - FADE_OUT_TOLERANCE);
						
			// fade-in direkt starten
			this.fadeIn();
			
		} else {				
			// neues Bild zuf�llig aus dem Pool der aktuellen Kategorie w�hlen						
			var laImageList = maMap[maCategoryCycle[miCategoryCount]];							
			var liRandomNumber = Math.floor( laImageList.length * (Math.random ()) );
			document.getElementById('contentImg').src = laImageList[liRandomNumber].src;
					
			// festlegen, wann das n�chste Bild angezeigt werden soll
			setTimeout('ImageRandomizer.setRandomImage()', TIME_TO_NEXT_IMAGE);		
			
			// festlegen, wann der fade-out Effekt gestartet werden soll
			setTimeout('ImageRandomizer.fadeOut()', TIME_TO_NEXT_IMAGE - TIME_TO_FADE_OUT - FADE_OUT_TOLERANCE);		
			
			// interne Datenstruktur aktualisieren
			if( miCategoryCount < maCategoryCycle.length - 1) {
				miCategoryCount += 1;
			} else {
				miCategoryCount = 0;
			}
			
			// f�r das erste Bild nach dem gro�em Logo wird das kleine Logo mit in den fade-in Effekt einbezogen
			if( mbIsFirstImageOnPageLoad == true ) {
				document.getElementById('logoTopRight').style.zIndex = '20';
				mbIsFirstImageOnPageLoad = false;
			} else {
				if( miImageCount == 1 ) {
					document.getElementById('logoTopRight').style.zIndex = '6';
					//document.getElementById('content').style.zIndex = 'none';
				}
			}
			
			// fade-in direkt starten
			this.fadeIn();
			
			// kleines Logo rechts-oben anzeigen
			document.getElementById('logoTopRight').style.display = 'block';		
		}			
	}
	
	
	/**
	* Bild ausblenden 
	*/
	this.fadeOut = function() {
		miMode = FADE_OUT;
		
		if( mbNoEffects == true ) {
			// kein Ausblendeffekt
			return;
		}
			
		if( miImageCount == 1 ) {
			document.getElementById('logoTopRight').style.zIndex = '20';
			//document.getElementById('content').style.zIndex = '5';
		}
			
		if( mbUseImageAnimation ) {
			// Bild-Animation verwenden, um den Ausblendeffekt zu simulieren
			this.fadeOutImageAnimation(0);
		} else {
			// CSS & Javascript verwenden, um den Ausblendeffekt zu simulieren
			var lobjNode = document.getElementById('contentImgCover');
		
			lobjNode.style.opacity = 0.0;
			lobjNode.style.MozOpacity = 0.0;
			lobjNode.style.filter = "alpha(opacity=0)";
			
			this.increaseOpacity();
		}
	}
	
	
	/**
	* 
	*/
	this.increaseOpacity= function() {
		if( miMode != FADE_OUT ) {
			return;
		}
	
		var lobjContentImgCoverNode = document.getElementById('contentImgCover');
		var liNewOpacity = lobjContentImgCoverNode.style.opacity / 1 + (1.00/FADE_OUT_STEPS);
		
		lobjContentImgCoverNode.style.opacity =  liNewOpacity;
		lobjContentImgCoverNode.style.MozOpacity = liNewOpacity;	
		lobjContentImgCoverNode.style.filter = "Alpha(opacity="+(liNewOpacity*100)+")";
		
		if( lobjContentImgCoverNode.style.opacity < 1.0 ) {
			setTimeout('ImageRandomizer.increaseOpacity()', TIME_TO_FADE_OUT/FADE_OUT_STEPS);
		} 
	}
	
	
	/**
	* 
	*/
	this.fadeOutImageAnimation = function(piImage) {
		if( miMode != FADE_OUT ) {
			return;
		}
		
		document.getElementById('contentImageAnimation').src = IMAGE_ANIMATION_HOME + piImage+'.png';
		
		if( piImage < 100 ) {
			setTimeout('ImageRandomizer.fadeOutImageAnimation('+(piImage+10)+')',TIME_TO_FADE_OUT/11);
		}
	}
	
	
	/**
	* Bild einblenden
	*/
	this.fadeIn = function() {
		miMode = FADE_IN;
	
		if( mbNoEffects == true ) {
			// kein Einblendeffekt
			return;
		}
		
		if( mbUseImageAnimation ) {			
			// Bild-Animation verwenden, um den Einblendeffekt zu simulieren
			this.fadeInImageAnimation(100);			
		} else {
			// CSS & Javascript verwenden, um den Einblendeffekt zu simulieren
			var lobjNode = document.getElementById('contentImgCover');
			
			lobjNode.style.opacity = 1.00;
			lobjNode.style.MozOpacity = 1.00;
			lobjNode.style.filter = "alpha(opacity=100)";
						
			this.decreaseOpacity();
		}
	}
	
	
	/**
	*
	*/
	this.decreaseOpacity = function() {
		if( miMode != FADE_IN ) {
			return;
		}
	
		var lobjContentImgCoverNode = document.getElementById('contentImgCover');
		var liNewOpacity = lobjContentImgCoverNode.style.opacity / 1 - (1.00/FADE_IN_STEPS);
		
		lobjContentImgCoverNode.style.opacity =  liNewOpacity;
		lobjContentImgCoverNode.style.MozOpacity = liNewOpacity;		
		lobjContentImgCoverNode.style.filter = "Alpha(opacity="+(liNewOpacity*100)+")";
		
		if( lobjContentImgCoverNode.style.opacity > 0 ) {
			setTimeout('ImageRandomizer.decreaseOpacity()', TIME_TO_FADE_IN/FADE_IN_STEPS);
		}
	}
	
	
	/**
	*
	*/
	this.fadeInImageAnimation = function(piImage) {
		if( miMode != FADE_IN ) {
			return;
		}
		
		document.getElementById('contentImageAnimation').src = IMAGE_ANIMATION_HOME + piImage+'.png';
		
		if( piImage > 0 ) {
			setTimeout('ImageRandomizer.fadeInImageAnimation('+(piImage-10)+')', TIME_TO_FADE_IN/11);
		}
	}
	
	
	/**
	*
	*/
	function loadImages() {
		// interne Datenstruktor initialisieren
		maMap[0] = new Array();		// Index 0	=> 	auto
		maMap[1] = new Array();		// Index 1	=>	motorbike
		maMap[2] = new Array();		// Index 2 	=>	marine
		maMap[3] = new Array();		// Index 3 	=> 	atv 
	
		// AJAX Request stellen
		var lobjHTTPRequest = null;
			
		try {
			lobjHTTPRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch(e1) {
			try {
				lobjHTTPRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch(e2) {
				try {
					lobjHTTPRequest = new XMLHttpRequest();			
				} catch(e3) {
					alert("instantiate HTTP Request failed.")
					return;
				}				
			}
		}
					
		//  Bilder auslesen (synchroner Aufruf)
		lobjHTTPRequest.open("GET", IMAGE_SLIDESHOW_HOME + SLIDESHOW_CONFIG_FILE_NAME, false);					
		lobjHTTPRequest.send(null);
		
		// aus der HTTP-Response automatisch generierter DOM
		var lobjHTTPResponse = lobjHTTPRequest.responseXML; 

		if( !lobjHTTPResponse || !lobjHTTPResponse.documentElement ) { 
			// sofern der Standardprozess nicht funktioniert hat, versuchen Browser-eigene Methoden zu verwenden
			if( window.ActiveXObject ) { 
				// Msxml COM Object verwenden (IE)
				lobjHTTPResponse = new ActiveXObject("Msxml2.DOMDocument");
				lobjHTTPResponse.loadXML( lobjHTTPRequest.responseText );
			} else if ( DOMParser ) { 
				// Gecko DOM-Parser verwenden
				lobjHTTPResponse = new DomParser().parseFromString(lobjHTTPRequest.responseText, "text/xml" );
			}
		}
		
		// endg�ltig aufgeben: ....... :-(
		if( !lobjHTTPResponse || !lobjHTTPResponse.documentElement ) { 
			mbNoEffects = true;
			return;
		}
		
		// Array aller Bilder, die ins Preloading mit einbezogen werden sollen
		var loPreloadImageList = new Array();
		
		// Logo-Bild separat speichern und sofort anzeigen
		mtSuzukiLogo = IMAGE_HOME + lobjHTTPResponse.getElementsByTagName('logo')[0].firstChild.nodeValue;	
		loPreloadImageList.push(new Array(mtSuzukiLogo));
		
				
		// Bilder der jeweiligen Category einlesen
		if( lobjHTTPResponse.getElementsByTagName('auto').length == 1 ) {
			loadImagesXML_LoadCategory(lobjHTTPResponse.getElementsByTagName('auto')[0], 0, loPreloadImageList)
		}
		if( lobjHTTPResponse.getElementsByTagName('moto').length == 1 ) {
			loadImagesXML_LoadCategory(lobjHTTPResponse.getElementsByTagName('moto')[0], 1, loPreloadImageList)
		}
		if( lobjHTTPResponse.getElementsByTagName('marine').length == 1 ) {
			loadImagesXML_LoadCategory(lobjHTTPResponse.getElementsByTagName('marine')[0], 2, loPreloadImageList)
		}
		if( lobjHTTPResponse.getElementsByTagName('atv').length == 1 ) {
			loadImagesXML_LoadCategory(lobjHTTPResponse.getElementsByTagName('atv')[0], 3, loPreloadImageList)
		}
		
		// Bilder u.a. f�r die Slideshow bereits jetzt vollst�ndig laden
		miImagesToPreload = loPreloadImageList.length;
		
		for(liPreloadImageCount=0; liPreloadImageCount<loPreloadImageList.length; liPreloadImageCount++) {
			var laCurrentImageConfig = loPreloadImageList[liPreloadImageCount];
					
			var loCurrentImage = new Image();
			//loCurrentImage.onLoad = onPreloadComplete();
			loCurrentImage.src = laCurrentImageConfig[0];	
			
			if( laCurrentImageConfig.length == 3 ) {
				// Image Objekt f�r die Slideshow merken
				maMap[laCurrentImageConfig[1]][laCurrentImageConfig[2]] = loCurrentImage;
			} else {
				// Logo
				document.getElementById('contentImg').src = mtSuzukiLogo
			}
		}
		
		setTimeout('ImageRandomizer.checkCompleteState()', 50);
	}
	
	
	/**
	*
	*/
	function loadImagesXML_LoadCategory(pobjCategoryNode, piCategoryIndex, poPreloadImageList) {
		var ltCategorySubfolder = pobjCategoryNode.getAttribute('subfolder')		
		var laImageNodeList = pobjCategoryNode.getElementsByTagName('image');			
		
		for(var liImagePos=0; liImagePos<laImageNodeList.length; liImagePos++) {						
			poPreloadImageList.push( new Array(IMAGE_SLIDESHOW_HOME+ltCategorySubfolder+"/"+laImageNodeList[liImagePos].firstChild.nodeValue, piCategoryIndex, liImagePos) );			
		}
	}

	
	/**
	* �berpr�ft, ob alle Bilder geladen wurden
	*/
	this.checkCompleteState = function() {
		var liNoLoadedCount = 0;
		
		for(var liCatCount=0; liCatCount<maMap.length; liCatCount++) {
			for(var liImgCount=0; liImgCount<maMap[liCatCount].length; liImgCount++) {			
				if( maMap[liCatCount][liImgCount].complete == false ) {
					liNoLoadedCount++;
				} 
			}
		}
		
		if( liNoLoadedCount > 0 ) {
			setTimeout('ImageRandomizer.checkCompleteState()', 200);
		} else {
			setTimeout('ImageRandomizer.startSlideshow()', 100);
		}
	}
	
	
	/**
	*
	*/
	function determineSystem() {
		// Bildschirmgr��e
		if( screen.width == 800 ) {
			if( screen.height == 600 ) {
				mbUseSmallImages = true;
			}
		} 
	
		// Browser-weiche
		var mtUserAgent = navigator.userAgent;
	
		if (mtUserAgent.indexOf("Opera") != -1)
		{
			if( mtUserAgent.indexOf("8.5") != -1 ) 	{
				FADE_OUT_TOLERANCE = FADE_OUT_TOLERANCE + 50
				TIME_TO_FADE_OUT = TIME_TO_FADE_OUT + 50;
				mbUseImageAnimation = true; 
			}
			
			if( mtUserAgent.indexOf("8.0") != -1 ) {
				TIME_TO_FADE_OUT = TIME_TO_FADE_OUT + 50;
				TIME_TO_FADE_IN = TIME_TO_FADE_IN - 50;
				FADE_OUT_TOLERANCE = FADE_OUT_TOLERANCE + 50
				mbUseImageAnimation = true; 
			}
		}
		else if(mtUserAgent.indexOf("Netscape") != -1)
		{
			mbUseImageAnimation = true;
			FADE_OUT_TOLERANCE = FADE_OUT_TOLERANCE + 100;
		}
		else if (mtUserAgent.indexOf("MSIE") != -1)
		{
		    if( mtUserAgent.indexOf("5.5") != -1 ) 	{
				mbNoEffects = true;
			}
			if( mtUserAgent.indexOf("5.0") != -1 ) 	{
				mbNoEffects = true;
			}
			
			if( mtUserAgent.toLowerCase().indexOf("mac") != -1 ) 	{
				mbNoEffects = true;
			}
		}
		else if (mtUserAgent.indexOf("Firefox") != -1)
		{
			if( mtUserAgent.indexOf("1.0") != -1 ) 	{
				FADE_OUT_TOLERANCE = FADE_OUT_TOLERANCE + 75
				mbUseImageAnimation = true;
			}
		}
		
		
		// je nach Modus entsprechende Bereiche ein- bzw. ausblenden
		if( mbNoEffects == true ) {
			document.getElementById('contentImgCover').style.display = 'none';
			document.getElementById('contentImageAnimation').style.display = 'none';
		} else {
			if( mbUseImageAnimation == true ) {	
				document.getElementById('contentImgCover').style.display = 'none';
				document.getElementById('contentImageAnimation').style.display = 'block';
			} else {
				document.getElementById('contentImgCover').style.display = 'block';
				document.getElementById('contentImageAnimation').style.display = 'none';
			}
		}
	}
	
	
	/**
	*
	*/	
	function setUpContentArea() {
		if( mbUseSmallImages == true ) {
			// Pfad zur Slideshow Konfigurationsdatei setzen
			IMAGE_SLIDESHOW_HOME += 'small/';
		} else {
			// Pfad zur Slideshow Konfigurationsdatei setzen
			IMAGE_SLIDESHOW_HOME += 'large/';
			return;
		}
		
		//document.getElementById('content').style.width = '710px';
		//document.getElementById('content').style.height = '450px';
		document.getElementById('content').style.width = '474px';
		document.getElementById('content').style.height = '300px';
		
		// Haupt-Element ausrichten
		//document.getElementById('main').style.marginLeft = '-355px';
		document.getElementById('main').style.marginLeft = '-230px';
		
		// CSS animierte Effekte
		//document.getElementById('contentImgCover').style.width = '710px';
		//document.getElementById('contentImgCover').style.height = '450px';
		document.getElementById('contentImgCover').style.width = '474px';
		document.getElementById('contentImgCover').style.height = '300px';
		
		// PNG animierte Effekte
		//document.getElementById('contentImageAnimation').style.width = '710px';
		//document.getElementById('contentImageAnimation').style.height = '450px';
		document.getElementById('contentImageAnimation').style.width = '474px';
		document.getElementById('contentImageAnimation').style.height = '300px';
		
		// Bilder
		//document.getElementById('contentImg').style.width = '710px';
		//document.getElementById('contentImg').style.height = '450px';
		document.getElementById('contentImg').style.width = '474px';
		document.getElementById('contentImg').style.height = '300px';
		
		// kleines Logo verschieben
		/*document.getElementById('logoTopRight').src = 'images/logoclaim.jpg';
		document.getElementById('logoTopRight').style.left = '630px';
		document.getElementById('logoTopRight').style.width = '80px';
		document.getElementById('logoTopRight').style.height = '160px';*/		
		document.getElementById('logoTopRight').src = 'images/logoclaim_small.jpg';
		document.getElementById('logoTopRight').style.left = ' 409px';
		document.getElementById('logoTopRight').style.width = '65px';
		document.getElementById('logoTopRight').style.height = '130px';
		
		// Navigation nach unten verschieben
		//document.getElementById('navigation').style.top = '463px';
		//document.getElementById('navigation').style.width = '710px';
		document.getElementById('navigation').style.top = '310px';
		document.getElementById('navigation').style.width = '474px';
		
		// Impressum nach untern verschieben
		/*document.getElementById('imprint').style.top = '545px';
		document.getElementById('imprint').style.width = '710px';
		document.getElementById('imprint').getElementsByTagName('a')[0].style.marginLeft = '560px';*/
		document.getElementById('imprint').style.top = '385px';
		document.getElementById('imprint').style.width = '474px';
		document.getElementById('imprint').getElementsByTagName('a')[0].style.marginLeft = '324px';
		
		// loading-Grafik ausrichten
		document.getElementById('loadingImg').style.top = '147px';
		document.getElementById('loadingImg').style.left = '229px';
	}
}
