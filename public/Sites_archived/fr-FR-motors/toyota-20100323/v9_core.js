/* ty_v9_javascript_core */

var G={gecko:navigator.product=='Gecko',flashVer:'10.0.0',hasFla:false,autoFla:false,ie6:(!!document.all && !(typeof window.XMLHttpRequest=='object')),
init:function(){
var o,i
G.detectFlash()
try{document.execCommand('BackgroundImageCache', false, true)}catch(err){}
for(var n in G)if(G[n].init)G[n].init()
o=window.fnstack;if(o)for(i=0;i<o.length;i++)o[i]()
},
event:function() {
	var o=this,v
    o.name=arguments[0]
    o.subscribe=function(fn){if(!v)v=[];v.push(fn)}
    o.fire=function(sender,args){if(v)for(var i=0;i<l.length;i++)v[i](sender,args)}
},
set:function(a,b,c){for(var o in b)a[o]=b[o];if(c){for(var o in c)a.style[o]=c[o]};return a},
create:function(a,b,c,d){var o=(d||document).createElement(a);G.set(o,b);G.set(o.style,c);return o},
append:function(a,b,c,d){var o=b.tagName?b:G.create(b,c,d,a.ownerDocument);a.appendChild(o);return o},
time:function(){return new Date().getTime()},
fn:function(a){return(0.5-Math.cos(a*Math.PI)/2)},
middle:function(o,a,b){o=o.split(a);return(o.length<2)?'':o[1].split(b)[0]},
pos:function(a){var x=0,y=0;if(a.y)return{x:a.x,y:a.y};while(a!=null){x+=a.offsetLeft;y+=a.offsetTop;a=a.offsetParent};return{x:x,y:y}},
stop:function(a){if(a){clearTimeout(a);a=null}},
sub:function(a,b,c){var o=a.split(b);return(o.length>1)?o[1].split(c)[0]:''},
remove:function(a){var o=a.tagName?a:Tme.get(a);o.parentNode.removeChild(o)},
absTop:function(a){var y=0;if(a.y)return a.y;while(a!=null){y+=a.offsetTop;a=a.offsetParent};return y},
absLeft:function(a){var x=0;if(a.x)return a.x;while(a!=null){x+=a.offsetLeft;a=a.offsetParent};return x},
detectFlash:function(){if(G.middle(document.cookie,'acc_type=',';')=='html'){G.noFlash=true;return};var o=G.flash._o;G.hasFla=o.has(G.flashVer);G.autoFla=o.has('6.0.65')&&(o.ua.win||o.ua.mac)}
}
function $(a){return document.getElementById(a)}
function $$(a,b){return (b||document).getElementsByTagName(a)}
window.onload=G.init

G.viamichelin={
createNamedElement:function(a,b,c,d,n){var o=null;try{o=(d||document).createElement('<'+a+' name="'+n+'">');}catch(err){}if (!o || o.nodeName != a.toUpperCase()){o=(d||document).createElement(a);o.name= n;}G.set(o,b);G.set(o.style,c);return o}
}

G.sifr={movie:'/images/toyota-display.swf',
bc:'999999',
cGroup:{corp:'e50000','urban-iq':'92539a','urban-aygo':'c90123','urban-cruiser':'c90123',life:'ae9f6d',life2:'ae9f6d',recreation:'7e6f5d',energy:'5cbac8',together:'c47e3d',work:'808b83','drive-2010':'c90123'},on:0,
init:function(){
	var m=this,cn=$$('body')[0].className,o=$('img-focus'),i
	m.set($('hdrt'),{css:'.sIFR-root{color:#'+m.bc+';font-weight:bold;text-align:right}strong{color:#'+m.cGroup[cn||'corp']+'}',size:16,offsettop:0,thickness:20})
	if(o){
		m.items=$$('li',o)
		o=$$('h2',o);for(i=0;i<o.length;i++)m.setTitle(o[i])
		m.prev=G.set($('sm-prev'),{onclick:m.doPrev})
		m.next=G.set($('sm-next'),{onclick:m.doNext})
		m.go(0)
	}
	o=$('one-focus');if(o){m.setTitle($$('h1',o)[0])}
	o=$('gr-title');if(o)m.set(o,{css:'.sIFR-root{color:#'+m.bc+';font-weight:bold}strong{color:#'+m.cGroup[cn||'corp']+'}',size:16,offsettop:0,thickness:20})
	o=$$('h2')
	for(i=0;i<o.length;i++){
		if(o[i].className=="h1")m.set(o[i],{css:'.sIFR-root{color:#'+m.bc+';font-weight:bold}strong{color:#'+m.cGroup[cn||'corp']+'}',size:16,offsettop:0,thickness:20})
	}
},
escape:function(a){return a.replace('\n','').replace(/\%/g, '%2525').replace(/&nbsp;/g,' ').replace(/\+/g,'%2B')},
set:function(a,b){
	var m=this,o,v=[],p,s;if(!a)return
	a.style.visibility='visible'
	if(G.noFlash)return
	o={movie:m.movie,height:a.offsetHeight,width:a.offsetWidth}
	for(p in b)v.push(p+'='+b[p])
	s=m.escape(a.innerHTML)
	v.push('content='+s)	
	v.push('version=beta2')
	v.push('width='+o.width)
	v.push('height='+o.height)
	o.flashvars=v.join('&amp;')
	G.flash.render([o,a])
},
doPrev:function(){G.sifr.go(-1);return false},
doNext:function(){G.sifr.go(1);return false},
go:function(a){
	var m=this,v=m.on+a,n=m.items.length
	if(!(v>-1 && v<n))return
	if(v!=m.on)m.items[m.on].style.display='none'
	m.items[v].style.display='block'
	m.prev.style.visibility=(v>0)?'visible':'hidden'
	m.next.style.visibility=(v<n-1)?'visible':'hidden'
	m.on=v
},
setTitle:function(o){
	var m=this,ta=m.getStyle(o,'text-align'),c=m.getColor(o,'color'),so=$$('strong',o)[0],s=m.getColor(so||o,'color'),cn=o.className
	if(so)so.removeAttribute('style')
	if(cn=='BL'||cn=='BR'){
		if(o.offsetHeight==38)o.style.top=o.offsetTop+38+'px'
	}
	m.set(o,{css:'.sIFR-root{color:'+c+';font-weight:bold;text-align:'+ta+'}strong{color:'+s+'}',size:32},true)
},
getStyle:function(a,b){
	var m=this,v=''
	if(a.currentStyle)v=a.currentStyle[m.ieProp(b)]
	else if(window.getComputedStyle)v=document.defaultView.getComputedStyle(a,null).getPropertyValue(b)
	return v
},
ieProp:function(a){var v=a.split('-'),i;if(v.length<2)return a;for(i=1;i<v.length;i++)v[i]=v[i].substr(0,1).toUpperCase()+v[i].substr(1);return v.join('')},
colorHex:function(a){
	var m=this,v=a.replace(/[#| ]/g,'')
	if(v.indexOf('rgb(')==0){v=v.split('(')[1].split(')')[0].split(',');v=m.hex(v[0])+m.hex(v[1])+m.hex(v[2])}
	else if(v.length==3){v=v.split('');v=v[0]+v[0]+v[1]+v[1]+v[2]+v[2]}
	return '#'+v
},
hex:function(a){return (a*1).toString(16)},
getColor:function(a,b){var m=this;return m.colorHex(m.getStyle(a,b))}
}

/* newsticker */
G.ticker={speed:50,delay:2000,lines:[],num:0,chars:0,
init:function(){
	var m=this,os=$('nws'),v,i,ph,pn;if(!os)return
	v=$$('a',$('nwsi'));for(i=0;i<v.length;i++)m.lines.push({href:v[i].href,label:v[i].innerHTML})
	v=$$('li',$('nwsi'));for(i=0;i<v.length;i++)G.set(v[i],{onmouseover:m.over,onmouseout:m.out})
	v[v.length-1].style.padding=0
	G.append($('cnv'),m.makeDiv());m.y=os.offsetTop-110
	G.set($('nwst'),{onmouseover:m.tOver,onmouseout:m.tOut,onclick:m.tClick})
	m.render()
},
makeDiv:function(){var m=this,o=m.hDiv=G.create('div',{id:'nwsIm'});m.img=G.append(o,'img',{src:''});G.append(o,'div',{innerHTML:'&nbsp;'});return o},
render:function(){
	var m=G.ticker,p=m.lines[m.num],d=m.speed
	if(m.chars>p.label.length){m.chars=0;m.num++;if(m.num>=m.lines.length)m.num=0;d=m.delay}
	else $('nwst').innerHTML=p.label.substring(0,m.chars++)
	m.timer=setTimeout(m.render,d)
},
expand:function(){var m=G.ticker,q=m.expanded;$('nws').className=q?'':'exp';m.expanded=!q},
over:function(){var m=G.ticker,o=m.hDiv,p=this;m.img.src=$$('img',p)[0].src;G.set(o.style,{top:m.y+p.offsetTop+'px',display:'block'})},
out:function(){G.ticker.hDiv.style.display='none'},
tOver:function(){
	var m=G.ticker,p=$$('li',$('nwsi'))[m.num]
	$('nwst').className='hover'
	m.img.src=$$('img',p)[0].src
	G.set(m.hDiv.style,{top:m.y+p.offsetTop+'px',display:'block'})
	clearTimeout(m.timer)
	$('nwst').innerHTML=m.lines[m.num].label
},
tOut:function(){var m=G.ticker;$('nwst').className='';m.out();m.render()},
tClick:function(){var m=G.ticker;document.location=m.lines[m.num].href}
}


/* spotlight ads */
G.ads={
init:function(){
	var m=this,o=$('ads'),i,q=0,p,v=[0,0,0],pp,sp=3;if(!o)return
	if(!$('qlinks'))sp=4
	o=$$('li',o);if(o.length<sp+1)return
	if($('adU')){
		m.set=0;m.num=o.length;m.max=parseInt(m.num/sp)
		if((m.num%sp)!=0)m.max+=1
		for(i=0;i<o.length;i++){p=o[i].offsetHeight;if(p>q)q=p}
		$('ad2').style.height=q+'px'
		$('adU').style.position='absolute'
		$('adM').innerHTML=m.max
	}else{
		for(i=0;i<o.length;i++){p=o[i].offsetHeight;pp=parseInt(i/sp);if(p>v[pp])v[pp]=p}
		m.min=v[0];$('ad2').style.height=v[0]+'px'
		for(i=0;i<o.length;i++)o[i].style.height=v[parseInt(i/sp)]+'px'
	}
	$('adN').style.display='block'
},
go:function(a){
	var m=G.ads,v=m.set+a,x,xx,sp=3;
	if(!$('qlinks'))sp=4
	x=v*sp*194
	if(v<0||v>m.max-1)return
	xx=m.num-v*sp;if(xx<sp)x-=(sp-xx)*194
	m.set=v
	$('adP').innerHTML=v+1
	$('adL').className=(v>0)?'on':''
	$('adR').className=(v<m.max-1)?'on':''
	m.move(-x)
},
move:function(a){
	var m=G.ads,o=$('adU')
	G.set(o,{h1:o.offsetLeft,h2:a,start:G.time(),end:400})
	o.step=function(){
		var t=G.time()-o.start,q,g,g2
		if(t>o.end){o.style.left=o.h2+'px';clearInterval(o.timer);o.timer=null}
		else{q=G.fn(t/o.end);o.style.left=parseInt(o.h1+q*(o.h2-o.h1))+'px'}
	}
	o.timer=setInterval(o.step,10)
},
drop:function(a){
	var m=G.ads,o=$('ad2'),mm=o.scrollHeight
	G.set(o,{h1:a?m.min:mm,h2:a?mm:m.min,start:G.time(),end:400})
	$('adDD').style.display=a?'none':'block'
	$('adDU').style.display=a?'block':'none'
	o.step=function(){
		var t=G.time()-o.start,q,g,g2
		if(t>o.end){o.style.height=o.h2+'px';clearInterval(o.timer);o.timer=null}
		else{q=G.fn(t/o.end);o.style.height=parseInt(o.h1+q*(o.h2-o.h1))+'px'}
	}
	o.timer=setInterval(o.step,10)
}
}

G.search={
open:function(){G.set($('srch').style,{left:$('cnv').offsetLeft+421+'px',display:'block'});G.halt=true},
close:function(){$('srch').style.display='none';G.halt=false}
}

/* navigation */
G.nav={timer:null,base:'/sys/',
init:function(){
	var m=this,qn=$('nav'),q,i,o,n
	if(!qn)return
	m.sub=$('xNav')||G.append(document.body,'iframe',{frameBorder:0,allowTransparency:'true'})
	m.doc=(m.sub.contentWindow||m.sub).document;if(!m.doc)return
	m.doc.write('<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html><head></head><body></body></html>')
	m.doc.close()
	try{m.doc.execCommand('BackgroundImageCache', false, true)}catch(err){}
	G.append($$('head',m.doc)[0],'link',{rel:'stylesheet',type:'text/css',href:m.base+'v9_dd.css'})
	o=m.body=$$('body',m.doc)[0]
	m.div=G.append(o,'div',{id:'dd-1'})
	m.strip=G.append(o,'div',{id:'dds'})
	G.append(o,'div',{id:'dd-2'})
	m.set(o,m.subover,m.out)
	if(!window.JsTopMenu)return
	q=$$('a',qn);n=q.length
	for(i=0;i<n;i++){
		if(JsTopMenu[i].children.length>0){
			m.set(q[i],m.over,m.out,i)
			if(q[i].className=='on')m.subm=JsTopMenu[i].children
		}
	}
	m.max=n-1
	if(!m.sub)return
	qn=$('subnav');if(!qn)return
	if(qn.className=='final')return
	m.prepareSub()
	q=$$('a',qn);n=q.length;for(i=0;i<n;i++)m.set(q[i],m.over2,m.out,i)
},
prepareSub:function(){
	var m=this,o=m.subm,v=$$('a',$('subnav'))[0].innerHTML.replace('&amp;','&'),i
	for(i=0;i<o.length;i++){if(o[i].label==v){m.group=i;i=o.length}}
},
set:function(a,b,c,d){G.set(a,{onmouseover:b,onmouseout:c,index:d})},
over2:function(){
	var m=G.nav,q=this,o=m.subm[m.group+q.index].children,i,pos=G.pos(q),x=pos.x-1,y=pos.y+32;if(!o)return
	G.stop(m.timer)
	if($('subnav').className=='dbh')y+=13
	if(m.xx)m.xx.className=m.orig
	m.xx=q;m.orig=q.className;q.className='on'
	m.div.innerHTML=''
	m.body.className='nav2'
	G.set(m.sub.style,{top:y+'px',left:x+'px'})
	for(i=0;i<o.length;i++)G.append(m.div,m.item(o[i]))
	G.set(m.sub.style,{height:0,display:'block'})
	m.drop()
},
over:function(){
	var m=G.nav,q=this,p=q.parentNode,pos=G.pos(p),o,i,x=pos.x,y=pos.y+30,w=p.offsetWidth-2,s=1,ss
	G.stop(m.timer)
	if(m.xx)m.xx.className=m.orig
	m.xx=q;m.orig=q.className;q.className='on'
	ss=$('cnv').offsetLeft
	if(x-ss>634){s=x-ss-634;x=ss+634}
	else if(q.index>0) x-=1
	m.strip.className=(q.index==m.max)?'x':''
	w+=1
	G.set(m.sub.style,{top:y+'px',left:x+'px'})
	G.set(m.strip.style,{width:w+'px',left:s+'px'})
	m.body.className=''
	m.div.innerHTML=''
	o=JsTopMenu[q.index].children
	for(i=0;i<o.length;i++)G.append(m.div,m.item(o[i]))
	G.set(m.sub.style,{height:0,display:'block'})
	G.ieFix.hide()
	m.drop()
},
subover:function(){G.stop(G.nav.timer)},
out:function(){try{var m=G.nav;if(m.xx)m.timer=setTimeout(m.stop,250)}catch(e){}},
stop:function(){var m=G.nav;G.ieFix.hide(true);m.xx.className=m.orig;G.stop(m.timer);m.sub.style.display='none'},
item:function(a){var o=G.nav.doc,n=null;return(a.label=='--SEPARATOR--')?G.create('div',{innerHTML:'&nbsp;'},n,o):G.create('a',{href:a.url,target:'_parent',innerHTML:a.label},n,o)},
drop:function(){
	var m=G.nav,s=m.sub
	if(G.halt)return
	if(G.ie6){s.style.height=m.body.clientHeight+1+'px';return}
	G.set(s,{h2:m.body.clientHeight+1,start:G.time(),end:350})
	s.step=function(){
		var t=G.time()-s.start,q,g,g2
		if(t>s.end){s.style.height=s.h2+'px';G.stop(s.timer)}
		else{q=G.fn(t/s.end);s.style.height=parseInt(q*s.h2)+'px'}
	}
	s.timer=setInterval(s.step,10)
}
}

G.ieFix={hide:function(a){
	if(!G.ie6)return
	var o=$$('select'),i
	for(i=0;i<o.length;i++)o[i].style.visibility=a?'visible':'hidden'
}}

/* homepage showroom */
G.shr={
init:function(){
	var m=this,o=$('nav-sr'),i,p,x,pp,o2,f=parseInt;if(!o)return
	o=$$('a',o);p=m.last=o[o.length-1]
	x=f((770-p.offsetLeft-p.offsetWidth)/o.length)
	pp='8px '+f(x/2)+'px 11px '+f(.5+x/2)+'px'
	for(i=0;i<o.length-1;i++){G.set(o[i],{onmouseover:m.over,onmouseout:m.out,index:i},{padding:pp})}
	x=770-p.offsetLeft-p.offsetWidth;	if(G.ie6)x-=2.9
	p.style.padding='8px '+f(x/2)+'px 11px '+f(x/2)+'px'
	o2=$('cnav-dd');o2.style.zIndex=999;m.dds=o2=$$('li',o2)
	for(i=0;i<o2.length;i++){
		x=o[i].offsetLeft;x=(x<572)?x:572
		G.set(o2[i],{onmouseover:m.subover,onmouseout:m.subout},{left:x+'px'})
	}
},
over:function(){
	var m=G.shr,o=m.dds[this.index]
	if(G.halt)return
	G.stop(m.timer)
	if(m.a)m.a.style.display='none'
	o.style.display='block'
	m.a=o
},
out:function(){var m=G.shr;m.timer=setTimeout(m.stop,250)},
subover:function(){var m=G.shr;G.stop(m.timer)},
subout:function(e){
	var m=G.shr,t,o,q=window.event
	e=e||q
	t=(q)?e.srcElement:e.target
	o=(e.relatedTarget)?e.relatedTarget:e.toElement
	if(o){while(o.parentNode){if(o==m.a)return;o=o.parentNode}}
	m.stop()
},
stop:function(){var m=G.shr;if(m.a)m.a.style.display='none'}
}


/* quick fixes dede demo */
var Tme=G
G.get=$

G.fix={
init:function(){
	var o=$('subnav'),i,v=0;if(!o)return
	o=$$('a',o)
	for(i=0;i<o.length;i++)v=(o[i].offsetHeight>v)?o[i].offsetHeight:v
	if(v>40)$('subnav').className='dbh'
}
}

/* carconfig */
function open_fullscreen(lnk,wd,ht,wn,parms){
	var strSb='scrollbars', strSt='status', strRsz='resizable', n='no',y='yes',s=screen,sw=s.availWidth-0,sh=s.availHeight-0,l,t,wp,sb='no'
	
	if(parms){wp = parms;} 
	else if (parms == undefined) {wp = strRsz+'='+n+', '+strSb+'='+n;}
	l=parseInt((sw-s.availWidth-10)/2);	if(l<0){w=sw-10;l=0;sb=y}
	t=parseInt((sh-s.availHeight-10)/2);if(t<0){h=sh-35;t=0;sb=y}
	if (sb==y){
	  if(parms.indexOf(strSb)!=-1){wp=wp.replace((strSb+'='+y),(strSb+'='+y))}
	  else {wp+= ', '+strSb+'='+y}}

	if (wp.indexOf(strRsz+'='+y) !=-1){wp+=','+strSt+'='+y;h=h-26}
        else{wp+=','+strSt+'='+n}

	window.open(lnk,wn,wp+',width='+w+',height='+h+',left='+l+',top='+t)
}

/* Form in overlayer (testdrive form) */
G.extForm={
launch:function(a,b){
	var m=G.extForm,o=$('ctai')
	if(!o||m.open==true)return
	G.append(o,'iframe',{src:a,allowTransparency:'true',frameBorder:'0'},{width:'616px',height:(b||'300')+'px'})
	o.style.display='block'
	m.open=true
},
close:function(){var m=G.extForm,o=$('ctai');o.innerHTML='';o.style.display='none';m.open=false}
}

G.cta={
init:function(){
	var m=this,o=$$('a'),i,sDiv=false,s=G.sub(window.location.href,'form=','#')
	if(s!='')sDiv=true
	for(i=0;i<o.length;i++){if(o[i].className=='cta'){o[i].onclick=m.launch;sDiv=true}}
	if(sDiv)m.div=G.append(document.body,'div',{id:'ctai'})
	if(s!='')m.launch('auto',decodeURIComponent(s))
},
launch:function(a,b){
	var m=G.cta,v=m.link=(a=='auto')?b:this.href,o=m.div,ct=$('cta-curtain')
	if(!o)o=m.div=G.append(document.body,'div',{id:'ctai'})
	G.ieFix.hide()
	o.innerHTML=''
	m.frame=G.append(o,'iframe',{allowTransparency:'true',frameBorder:'0'})
	if(ct)G.set(ct.style,{filter:'alpha(opacity=20)',display:'block'})
	o.style.display='block'
	if($('cntop'))$('cntop').scrollIntoView()
	else if($('c'))$('c').scrollIntoView()
	m.frame.src=v
	return false
},
setSrc:function(){var m=G.cta;m.frame.src=m.link},
close:function(){
	var o=$('ctai'),ct=$('cta-curtain')
	G.ieFix.hide(true)
	try{G.flash.show()}catch(e){}
	o.innerHTML=''
	o.style.display='none'
	if(ct)ct.style.display='none'
}
}

G.flash={items:[],
init:function(){setTimeout(G.flash.exec,200)},
events:new function(){this.rendered=new G.event("rendered")},
exec:function(){
	if(G.noFlash)return
	var m=G.flash,o=m.items,i;if(o.length<1)return
	if(m._o.has(G.flashVer))for(i=0;i<o.length;i++)m.render(o[i],i)
	else m.alert()
},
add:function(a,b){this.items.push([a,b])},
doPop:function(a,b,c){window.open(a,b,c)},
setBw:function(a,b,c){
	var o=$('obj_flash_0'),oL=$('bwL'),oH=$('bwH'),v=a=='low'
	document.cookie='bandwidth='+a+';path=/;'
	if(oL)oL.className=v?'':'a'
	if(oH)oH.className=v?'a':''
	if(c)dcsTrk('WT.bw_md=Manual&WT.bw_tp='+v?'Low_bw':'Hi_bw')
	if(G.noFlash){
	document.cookie='acc_type=flash;path=/;'
		location=location
	}
	if(b && o){
		try{if(a=='low')o.setLowBandwidth();else o.setHighBandwidth()}catch(e){}
	}
},
getBw:function(){
	return(G.middle(document.cookie,'bandwidth=',';'))
},
testBw:function(){
	alert(G.flash.getBw())
},
hide:function(){
	var q=G.flash.items,i
	for(i=0;i<q.length;i++){if(q[i][0].wmode=='window')$(q[i][1]).style.visibility='hidden'}
},
show:function(){
	var q=G.flash.items,i
	for(i=0;i<q.length;i++){if(q[i][0].wmode=='window')$(q[i][1]).style.visibility='visible'}
},
render:function(a,b){
	var s=true,v=[],q=G.set({wmode:'transparent',quality:'high',AllowScriptAccess:'always',allowfullscreen:'true',bgcolor:'#000000'},a[0]),x,pp
	pp=location.search.substr(1);if(pp!='')q.flashvars+='&amp;'+pp
	if(G.gecko)q.flashvars+='&amp;agent=Firefox'
	v.push('<object type="application/x-shockwave-flash" id="obj_flash_'+b+'" width="'+q.width+'" height="'+q.height+'" data="'+q.movie+'">');for(x in q){switch(x){case'height':case'width':break;default:v.push('<param name="'+x+'" value="'+q[x]+'" />')}};v.push('</object>')
	if(a[1].tagName)a[1].innerHTML=v.join('\n')
	else try{$(a[1]).innerHTML=v.join('\n')}catch(e){s=false}
	if(s)this.events.rendered.fire(null,{elementid:a[1],id:'obj_flash_'+b})
},
alert:function(){
	var m=this,o=m._o,q,v='/sys/flash_html_detection.aspx',cnv
/*
	flash auto update
if(o.has('6.0.65')&&(o.ua.win||o.ua.mac)){m.render([{height:340,width:770,movie:'inc/expressInstall.swf',flashvars:'MMredirectURL='+location.href  },'flash1'])};else{} 
*/
	cnv=$('cnv')||$('c')
	q=G.append(cnv,'iframe',{id:'fl-install',src:v,allowTransparency:'true',frameBorder:'0'})
},
_o:function(){
var SF="Shockwave Flash",SX="ShockwaveFlash.ShockwaveFlash",win=window,D=document,B=navigator,N=null,F=false,T=true,
detectIEWin=function(a,b,c){
	var x
	try{x=new window.ActiveXObject(b)}catch(e){return}
	try{x.AllowScriptAccess='always'}catch(e){}
	try{a.num=x['GetVariable']('$version')}catch(e){if(c)a.num=c}
},
i$=function(a){return typeof a!='undefined'};var ua=function(){var w3cdom=i$(D.getElementById)&&i$(D.getElementsByTagName)&&i$(D.createElement)&&i$(D.appendChild)&&i$(D.replaceChild)&&i$(D.removeChild)&&i$(D.cloneNode),playerVersion=[0,0,0],d=N;
if(i$(B.plugins)&&typeof B.plugins[SF]=='object'){d=B.plugins[SF].description;if(d){d=d.replace(/^.*\s+(\S+\s+\S+$)/,"$1");playerVersion[0]=parseInt(d.replace(/^(.*)\..*$/,"$1"),10);playerVersion[1]=parseInt(d.replace(/^.*\.(.*)\s.*$/,"$1"),10);playerVersion[2]=/r/.test(d)?parseInt(d.replace(/^.*r(.*)$/,"$1"),10):0}}
else if(i$(win.ActiveXObject)){
	var v={},vv,i=20,x
	v.num=-1
	detectIEWin(v,SX)
	while((v.num==-1)&&(i>0)){
		switch(i){
			case 6:x='WIN 6,0,21,0';break
			case 3:x='WIN 3,0,18,0';break
			case 3:x='WIN 2,0,0,11';break
			default:x='WIN '+i+',0,0,0'
		}
		detectIEWin(v,SX+'.'+i,x)
		i--
	}
	if(v.num==-1)v.num=x
	vv=v.num.split(' ')[1].split(',')
	playerVersion=[vv[0],vv[1],vv[2]]
}
var u=B.userAgent.toLowerCase(),p=B.platform.toLowerCase(),webkit=/webkit/.test(u)?parseFloat(u.replace(/^.*webkit\/(\d+(\.\d+)?).*$/,"$1")):F,ie=F,windows=p?/win/.test(p):/win/.test(u),mac=p?/mac/.test(p):/mac/.test(u);/*@cc_on;ie=true;@if(@_win32)windows=true;@elif(@_mac)mac=true;@end;@*/
return {w3cdom:w3cdom,pv:playerVersion,webkit:webkit,ie:ie,win:windows,mac:mac}
}()

function hasPlayerVersion(rv){var pv=ua.pv,v=rv.split(".");v[0]=parseInt(v[0],10);v[1]=parseInt(v[1],10);v[2]=parseInt(v[2],10);return(pv[0]>v[0]||(pv[0]==v[0]&&pv[1]>v[1])||(pv[0]==v[0]&&pv[1]==v[1]&&pv[2]>=v[2]))?T:F};return{get:function(){return{major:ua.pv[0],minor:ua.pv[1],release:ua.pv[2]}},has:hasPlayerVersion,ua:ua}}()
}

/* glossary popup */
G.glos={
prepare:function(){
	var m=this,o=m.div=G.append($('cnv'),'div',{id:'glosWin'})
	m.win=G.append(o,'iframe',{allowTransparency:'true',frameBorder:'0'},{width:'886px',height:'500px',display:'block'})
},
launch:function(a){
	var m=this
	if(!m.win)m.prepare()
	G.set(m.div.style,{top:document.documentElement.scrollTop+100+'px',display:'block'})
	m.win.src=a
},
close:function(){G.glos.div.style.display='none'}
}
function pop_glos(a){G.glos.launch(a)}

G.remote={
_:function(){var w=window;return w.XMLHttpRequest?new XMLHttpRequest():w.ActiveXObject?new ActiveXObject('Microsoft.XMLHTTP'):null},
get:function(a){var r=this._();r.open('GET',a,false);r.send(null);return r},
post:function(a,b){var r=this._();r.open('POST',a,false);r.setRequestHeader('Content-Type','application/x-www-form-urlencoded');r.send(b);return r}
}

/* Ajax poll */
G.poll={
submit:function(a,b){
	var m=G.poll,o=$('x'+a),v=['resid='+a],r
	if(b)v.push(m.getAnswer(o))
	r=G.remote.post(document.URL,v.join('&'))
	o.innerHTML=r.responseText
},
getAnswer:function(a,b){
	var m=this,o=$$('input',a),i,q
	for(i=0;i<o.length;i++){
		q=o[i]
		if(q.type=='radio'){
			if(q.checked)return(m.esc(q.name)+'='+m.esc(q.value))
		}
	}
	return('')
},
esc:function(a){return encodeURIComponent(a)}
}


G.xlink={
init:function(){
	var o=$$('a'),i,p
	for(i=0;i<o.length;i++){
		p=o[i]
		if(p.className=='external')G.set(p,{target:'_blank',title:'New window: '+(p.title||p.href)})
	}
}
}

/* Overlayer */
G.ovl={movies:{},
init:function(){
	var m=this,s=G.sub(window.location.search,'ovl=','&')
	if(m.movies[s])m.show(s)
},
showCurtain:function(a){
	var m=G.ovl,o=$('overlayer'),b=document.body,q=a?'block':'none'
	m.hideElements(a)
	G.set(o.style,{width:b.scrollWidth+'px',height:b.scrollHeight+'px',display:q})
},
hideElements:function(a){
	var m=G.ovl,o=$$('select'),i,h=a?'hidden':'visible'
	for(i=0;i<o.length;i++)o[i].style.visibility=h
},
show:function(a){
	var m=G.ovl,o=m.movies[a],oo=$('ovl_flash'),v='',d=document.documentElement,x,y,p,wm='transparent'
	window.scrollTo(0,0)
	m.showCurtain(o.hide_page)
	o.flashvars=o.flashvars||''
	x=(d.clientWidth-o.width)/2
	if(o.anchor){
		p=$('color-main')||$('main-focus')||$('focus')||$('main_focus')||$('sm-main')
		if(p)y=p.offsetTop //+1
	}
	else y=o.top
	y=y||((d.clientHeight-o.height)/2)
	if(o.white_background)wm='window'
	if(o.flash_popup){
		o.flashvars+='&sub='+o.file+'&title='+o.title
		o.file='/Images/flash_popup_v9.swf'
		o.flash_popup=false
		o.height=o.height*1+34
	}
	G.flash.render([{movie:o.file,height:o.height,width:o.width,wmode:wm,flashvars:o.flashvars},'ovl_flash'],o.name)
	G.set(oo.style,{top:y+'px',left:x+'px',display:'block'})
	m.current=o.name
	m.track('WT.cg_n=E_Overlayer&WT.pn='+o.name+'&WT.pc=opened')
},
track:function(a){
	var m=G.ovl,o=m.movies[m.current]
	if(o.webtrends && window.dcsTrk)dcsTrk('WT.cg_n=E_Overlayer&WT.pc=flash&WT.cg_s='+o.name+'&WT.pn='+a)
},
exec:function(command,args,target){
	var o=G.ovl,oo=$('ovl_flash'),on=o.current,v,om=o.movies[on]
	switch(command){
	case'close':oo.innerHTML='&nbsp;';oo.style.display='none';o.track('closed');o.current='';if(om.hide_page)o.showCurtain(false);break
	case'jump':o.track(args,target);document.location.href=args;break
	case'tag':o.track(args);break
	case'moveTo':v=args.split(',');oo.style.left=v[0];oo.style.top=v[1];break
	}
}
}
function ovl_do(command,args,target){G.ovl.exec(command,args,target)}
function showOverLayer(fmn){G.ovl.show(fmn)}

G.opacity={
init:function(){
	var o=$$('div'),i,v;if(!o[0].currentStyle)return
	for(i=0;i<o.length;i++){v=o[i].currentStyle.opacity;if(v)o[i].style.filter='alpha(opacity='+v*100+')'}
}
}

/* accessible model discovery */
G.mdt={
init:function(){
	var m=G.mdt,o=m.panel=$('acc-focus'),i;if(!o)return
	o=$$('a',o);for(i=0;i<o.length;i++)o[i].onclick=m.click2
	o=$('exp-hotspots')
	if(o){m.hots(o);o=$$('li',o);for(i=0;i<o.length;i++){G.set(o[i],{onmouseover:m.over,onmouseout:m.out,onclick:m.click})}}
	o=$('bodies');if(o){o=$$('a',o);for(i=0;i<o.length;i++)o[i].onclick=m.click2}
	o=$('acc-buttons');if(o){o=$$('a',o);for(i=0;i<o.length;i++)G.set(o[i],{index:i,onclick:m.scroll})}
	o=$('spl-scroll');if(o){if(o.offsetTop>90)o.style.height=320-o.offsetTop+'px'}
},
hots:function(a){
	var o=$$('a',a),i,p,n
	for(i=0;i<o.length;i++){
		p=o[i].parentNode;n=p.offsetLeft+p.offsetWidth
		if(n>582){o[i].className='right';p.style.left=p.offsetLeft-p.offsetWidth+21+'px'}
	}
},
scroll:function(){
	var m=G.mdt,o=m.oldScroll||0,v=this.index
	$('ext-inner').style.left=-770*v+'px'
	$$('a',$('acc-buttons'))[o].className=''
	this.className='on'
	m.oldScroll=v
	return false
},
over:function(){this.className='over'},
out:function(){this.className=''},
click:function(){G.mdt.load($$('a',this)[0].href);return false},
click2:function(){G.mdt.load(this.href);return false},
load:function(a){
	var m=this,o=G.remote.get(a.replace(/\s/g,'%20')+'&ac_only=1')
	m.panel.innerHTML=o.responseText
	setTimeout(G.mdt.init,0)
}
}

/* old popup function */
function openpopup(sLink,iWidth,iHeight,left,name){
	var d=new Date(),s=d.getTime(),ir,winLeft,winUp
	if(left==null)left=0
	ir=(name==null)?parseInt(((s-(parseInt(s/1000,10)*1000))/10)/100*100000+1,10):name
	winleft=((screen.width-iWidth)/2)-left
	winUp=(screen.height-iHeight)/2
	window.open(sLink,ir,'resizable=yes,scrollbars=yes,width='+iWidth+',height='+iHeight+',left='+winleft+',top='+winUp)
}

/* touchclarity */
G.Tcl={init:function(){
	var w=window,o=this
	if(w.TcId){
		if (typeof tc_logging_active == 'undefined') tc_logging_active = true;
		tc_site_id=TcId
		tc_server_url='toyota.touchclarity.com'
		tc_log_path = '/applications/touchclarity'
		G.append($$('body',o.doc)[0],'script',{type:'text/javascript',src:tc_log_path+'/logging-code.js'})
	}
}}

/* print */
G.printer={
printPage:function(){
	window.open('/sys/print.aspx','Print','width=800,height=500,resizable=yes,status=yes,location=yes,scrollbars=yes,menubar=yes')
},
disableCombo:function(){
	var a,i
	if(window.document){
		a=window.document.getElementsByTagName('select')
		if(a){for(i=a.length-1;i>=0;i--){
			a[i].parentNode.innerHTML=a[i].options[a[i].selectedIndex].text
	}}}
},
disableCheckbox:function(){
	var a,i
	if(window.document){
		a=window.document.getElementsByTagName('input')
		if(a){for(i=0;i<a.length;i++){if(a[i].checked){
					if(a[i].parentNode.className == "AspNet-RadioButtonList-Item")
						a[i].parentNode.parentNode.innerHTML=a[i].nextSibling.innerHTML
					else
						a[i].parentNode.innerHTML=a[i].nextSibling.innerHTML
	}}}}
},
removeInlineStylesCollapsed:function(){
var a,i;
if(window.document){
if(window.document.getElementsByClassName){
		a=window.document.getElementsByClassName("sm-collapsed");
		if(a){for(i=0;i<a.length;i++){a[i].style.cssText="";}}}
else{a=document.getElementsByTagName("div");if(a){
		for(i=0;i<a.length;i++){if(a[i].className=="sm-collapsed"){a[i].style.cssText="";}}}}}
}}

Tme.net={
init:function(){try {Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(Tme.net.pageBeginRequest);Sys.WebForms.PageRequestManager.getInstance().add_endRequest(Tme.net.pageEndRequest)} catch(e){}},
pageBeginRequest:function(sender, args) {var o=$('loading-indicator'); if (o) o.style.display='block';},
pageEndRequest:function(sender, args) {var o=$('loading-indicator'); if (o) o.style.display='none';}
}

G.dlg={
showLoading:function(a){
	var m=G.dlg,o=m.div,q
	if(o)q=m.body
	else{
		o=m.div=G.append(document.body,'div',{className:'dlg'})
		q=m.body=G.append(G.append(o,'div',{className:'dlgA'}),'div',{className:'dlgC'})
		G.append(o,'div',{className:'dlgB'})
	}
	q.innerHTML='<div class="dlgL">'+(a||'Loading...')+'</div>'
	o.style.display='block'
},
hideLoading:function(){var m=G.dlg;m.div.style.display='none'}
}

G.youtube={
init:function(){
	var m=this,o=$$('a'),i
	for(i=0;i<o.length;i++)if(o[i].className=='youtube')m.set(o[i])
},
set:function(a){
	var o=a.parentNode,v=G.middle(a.href,'v=','&')
	v=(v)?('http://www.youtube.com/v/'+v):a
	G.flash.render([{movie:v,height:340,width:560},o])
}
}

/* promo overview */
G.pbo={
init:function(){
	var o=$('pboa');if(!o||!G.ie6)return
	o.style.backgroundImage='none'
	o.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(src='file://C:/_dev-2009/promo-overview/v9-promo/bgTitre1b.png');"
}
}

/* promo component */
G.promo={
init:function(){
	var m=this,o=$('prAcc'),p,q,i;if(!o)return
	o=$$('li',o);p=$$('a',o[0])[0]
	if(p.parentNode.className=='on'){q=$$('div',p.parentNode)[0];m.active=q;q.xx=p}
	for(i=0;i<o.length;i++)G.set($$('a',o[i])[0],{onclick:m.click,index:i})
},
click:function(){
	var q=this,o=$$('div',q.parentNode)[0]
	if(q.parentNode.className=='on')G.promo.fold(this,o);else G.promo.unfold(this,o)
	return false
},
fold:function(a,b){
	var m=G.promo
	m.active=null
	G.set(b,{h2:b.scrollHeight,start:G.time(),end:350,xx:a})
	b.style.overflow='hidden'
	b.step=function(){
		var t=G.time()-b.start,q,g,g2
		if(t>b.end){m.end(b)}
		else{q=G.fn(t/b.end);b.style.height=b.h2-parseInt(q*b.h2)+'px'}
	}
	b.timer=setInterval(b.step,10)
},
unfold:function(a,b){
	var m=G.promo,p=m.active
	if(p)m.fold(p.xx,p);m.active=b
	G.set(b,{h2:b.scrollHeight,start:G.time(),end:350,xx:a})
	b.step=function(){
		var t=G.time()-b.start,q,g,g2
		if(t>b.end){m.end(b,1)}
		else{q=G.fn(t/b.end);b.style.height=parseInt(q*b.h2)+'px'}
	}
	b.timer=setInterval(b.step,10)
},
end:function(b,a){
	b.style.height=a?b.h2+'px':0
	clearInterval(b.timer);b.timer=null
	b.xx.parentNode.className=a?'on':''
}
}

/* 404 highlight */
G.badLink={
init:function(){
	var m=this,v=G.middle(location.search,'blink=','&'),o,i;if(v=='')return
	o=$$('a')
	for(i=0;i<o.length;i++){
	if(m.path(o[i].href)==v)G.set(o[i].style,{background:'#ff9',padding:'5px',color:'#f00',border:'solid 1px #f00'})
	}
},
path:function(a){var o=a.match(/^.*?:\/\/.*?(\/.*)$/i);return (o&&o.length>0)?o[1]:''}
}

/* temporary test bandwidth switch */
function mdBandwidthChange(a){G.flash.setBw(a)}
