// take care variables are definitly defined
var technologyguideDatabasePath = false;
var confCountryTopic  = null;
var confLanguageTopic = null;
var modulnavHeightTeasermode = 274;

// Country-Language
confCountryId  = "FR";
confLanguageId = "fr";

// Perso Engine
confPersoEngineEnabled = false;
// confPersoEnginePid = 	"";
// confPersoEngineEventController = "";
// confPersoEngineProfileController = "";

// Perso Engine + SILO
confPersoEngineSSOEnabled = false;
confPersoEngineSSOController = "?pid=&sys=&lan=fr";
// confPersoSsoSalutation = "";
// confPersoSsoLogin = "";
// confPersoSsoLogout = "";

// Tracking
confTrackingEnabled = true;
gDcsId  = "dcsykt339pljwppe9rn0b25e7_8e4c";
gDomain = "statse.webtrendslive.com";

// Browser Check
confBrowserCheckEnabled = true;
confBrowserCheckHighendEnabled = true;
confIncompatibleBrowserUrl  = "/fr/fr/general/incompatible.html";

// Technology Guide
technologyguideEnabled = false;
// technologyguideDatabasePath = "";

// SEO
seoEnabled = true;

// ID-Modules
confIdModuleImage = "/fr/fr/_common/shared/img/id_moduls.png";
confIdModuleImageGrey = "/fr/fr/_common/shared/img/id_moduls_grey.png";
confIdModuleImageService = "/fr/fr/_common/shared/img/id_moduls_service.png";
// confIdModuleImageFinance = "";

// Height Of Modulnavigation
modulnavHeightTeasermode = 274;

// Date format
confDateFormat = "DD.MM.YYYY";

// Price and hundreds delimiter
confPriceDelimiter    = ",";
confHundredsDelimiter = ".";

// Topnavi X displayed
topnaviXenabled = "false";

// tracking page urls
var trackingPages=new Array();
trackingPages["download"] = new Array("/fr/fr/_common/shared/tracking_redirect/download.html", "download");
trackingPages["external"] = new Array("/fr/fr/_common/shared/tracking_redirect/external.html", "external");
trackingPages["small_standard"] = new Array("/fr/fr/_common/shared/tracking_redirect/teaser_small_standard.html", "teaser_small_standard");
trackingPages["large"] = new Array("/fr/fr/_common/shared/tracking_redirect/teaser_large.html", "teaser_large");
trackingPages["main"] = new Array("/fr/fr/_common/shared/tracking_redirect/teaser_main.html", "teaser_main");
trackingPages["small_text"] = new Array("/fr/fr/_common/shared/tracking_redirect/teaser_small_text.html", "teaser_small_text");
trackingPages["medium_label"] = new Array("/fr/fr/_common/shared/tracking_redirect/teaser_medium_label.html", "teaser_medium_label");
trackingPages["click_event"] = new Array("/fr/fr/_common/shared/tracking_redirect/click_event.html", "click_event");
trackingPages["3seriestouring"] = new Array("/fr/fr/_common/shared/tracking_redirect/experience/3seriestouring.html", "3seriestouring");
trackingPages["quicklinks"] = new Array("/fr/fr/_common/shared/tracking_redirect/quicklinks.html", "quicklinks");
trackingPages["bandwidth"] = new Array("/fr/fr/_common/shared/tracking_redirect/bandwidth.html", "bandwidth");


//Bandwidth layer
var bandwidth_bottomnavi_link_high = "Version haut débit";
var bandwidth_bottomnavi_link_low = "Version modem/RNIS";
var bandwidth_headline_high = "Version haut débit.";
var bandwidth_copy_high = "Le site Internet officiel de BMW France avec de nombreuses séquences vidéo, des animations et de la musique – spécialement conçu pour les bénéficiaires d’une connexion par large bande ou ADSL. ";
var bandwidth_link_high = "Version haut débit";
var bandwidth_headline_low = "Remarque relative à la version modem/RNIS.";
var bandwidth_copy_low = "Optimale pour les connexions par modem ou RNIS (ISDN), cette version met à votre disposition tous les contenus, les informations et les images du site Internet officiel de BMW France, à l’exception des séquences vidéo, des animations et de la musique.";
var bandwidth_link_low = "Démarrer la version modem/RNIS";
var bandwidth_save_headline = "Votre connexion Internet";
var bandwidth_save_copy = "À l’avenir, toujours choisir :";
var bandwidth_save_button = "Enregistrer";
var bandwidth_save_select = "Sélectionnez";
var bandwidth_save_highband = "Version haut débit";
var bandwidth_save_lowband = "Version modem/RNIS";
var bandwidth_save_auto = "Vérification automatique";
var bandwidth_save_confirm = "Votre sélection a été enregistrée.";
var bandwidth_save_error_no_selection = "Veuillez préciser votre choix.";
var bandwidth_save_error_no_cookies = "Désolé ! Nous ne pouvons enregistrer votre sélection car votre navigateur n’est pas paramétré pour accepter les cookies.";

// enhanced bandwidth detection
var enhanced_bandwidth_detection = false;
var enhanced_bandwidth_detection_threshold = "1000";

// share
var share_services_active = true;
var share_services_headline = "Partager avec:";
var share_services = new Array();
share_services[0] = new Array('Delicious','http://delicious.com/save?url={#share_url}&title={#share_title}','/_common/files/img/share_icons/delicious.gif');
share_services[1] = new Array('Digg','http://digg.com/submit?phase=2&url={#share_url}&title={#share_title}','/_common/files/img/share_icons/digg.gif');
share_services[2] = new Array('Facebook','http://www.facebook.com/sharer.php?u={#share_url}&t={#share_title}','/_common/files/img/share_icons/facebook.gif');
share_services[3] = new Array('Google','http://www.google.com/bookmarks/mark?op=edit&bkmk={#share_url}&title={#share_title}','/_common/files/img/share_icons/google.gif');
share_services[4] = new Array('Mister Wong','http://www.mister-wong.com/index.php?action=addurl&bm_url={#share_url}&bm_description={#share_title}','/_common/files/img/share_icons/mrwong.gif');
share_services[5] = new Array('Myspace','http://www.myspace.com/Modules/PostTo/Pages/?u={#share_url}&t={#share_title}','/_common/files/img/share_icons/myspace.gif');
share_services[6] = new Array('Reddit','http://www.reddit.com/submit?url={#share_url}&title={#share_title}','/_common/files/img/share_icons/reddit.gif');
share_services[7] = new Array('StumbleUpon','http://www.stumbleupon.com/submit?url={#share_url}&title={#share_title}','/_common/files/img/share_icons/stumbleupon.gif');
share_services[8] = new Array('Twitter','http://twitter.com/home/?status={#share_url}','/_common/files/img/share_icons/twitter.gif');
share_services[9] = new Array('Windows Live','https://favorites.live.com/quickadd.aspx?url={#share_url}&title={#share_title}','/_common/files/img/share_icons/windowslive.gif');
share_services[10] = new Array('Yahoo!Buzz','http://buzz.yahoo.com/submit/?submitUrl={#share_url}&submitHeadline={#share_title}','/_common/files/img/share_icons/yahoobuzz.gif');
share_services[11] = new Array('Yammer','https://www.yammer.com/home?status={#share_url}','/_common/files/img/share_icons/yammer.gif');
