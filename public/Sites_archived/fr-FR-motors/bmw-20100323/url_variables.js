var loadingBoxGif      = buildValidServerRelativeUrl( "/_common/html/img/standard_elements/loading_box.gif" );
var lensGif            = buildValidServerRelativeUrl( "/_common/html/img/standard_elements/lens.gif" );
var lensMouseOverGif   = buildValidServerRelativeUrl( "/_common/html/img/standard_elements/lens-h.gif" );

var transGif           = buildValidServerRelativeUrl( "/_common/html/img/palette/1x1_trans.gif" );
var p999999Gif         = buildValidServerRelativeUrl( "/_common/html/img/palette/1x1_999999.gif" );
var p003399Gif         = buildValidServerRelativeUrl( "/_common/html/img/palette/1x1_003399.gif" );
var p8c8c8cGif         = buildValidServerRelativeUrl( "/_common/html/img/palette/1x1_8c8c8c.gif" );
var pff0000Gif         = buildValidServerRelativeUrl( "/_common/html/img/palette/1x1_ff0000.gif" );
var pafafafGif         = buildValidServerRelativeUrl( "/_common/html/img/palette/1x1_afafaf.gif" );

var backgroundGif      = buildValidServerRelativeUrl( "/_common/html/img/modul_navigation/scroller_background.gif" );
var scrollerUpGif      = buildValidServerRelativeUrl( "/_common/html/img/modul_navigation/scroller_up.gif" );
var scrollerDownGif    = buildValidServerRelativeUrl( "/_common/html/img/modul_navigation/scroller_down.gif" );
var dropperGif         = buildValidServerRelativeUrl( "/_common/html/img/modul_navigation/dropper.gif" );
var dropperGifUp0      = buildValidServerRelativeUrl( "/_common/html/img/modul_navigation/dropper_up.gif" );
var dropperGifUp1      = buildValidServerRelativeUrl( "/_common/html/img/modul_navigation/dropper_up-h.gif" );
var dropperGifDown0    = buildValidServerRelativeUrl( "/_common/html/img/modul_navigation/dropper_down.gif" );
var dropperGifDown1    = buildValidServerRelativeUrl( "/_common/html/img/modul_navigation/dropper_down-h.gif" );

var pulldownGif        = buildValidServerRelativeUrl( "/_common/html/img/forms/pulldown.gif" );
var pulldownBlackGif   = buildValidServerRelativeUrl( "/_common/html/img/forms/pulldown_black.gif" );
var pulldownErrorGif   = buildValidServerRelativeUrl( "/_common/html/img/forms/pulldown_error.gif" );

var closeGif           = buildValidServerRelativeUrl( "/_common/html/img/standard_elements/close.gif" );
var close2Gif          = buildValidServerRelativeUrl( "/_common/html/img/standard_elements/close-h.gif" );

var highlightBoxSmall  = buildValidServerRelativeUrl("/_common/html/img/standard_elements/highlight_box_37x29.gif" );

var downloadJSP        = buildValidServerRelativeUrl( "/_common/shared/jsp/download.jsp" );

var videoTeaserFlashWrapper = buildValidServerRelativeUrl( "/_common/flash/wrapper_modules.swf" );
var videoTeaserFlashmodule = buildValidServerRelativeUrl( "/_common/flash/modules/teaser/video_teaser.swf" );
