if (teaser) {

  Math.randomize = function() {var minValue=0,maxValue=0;if(arguments.length==1){maxValue=((isNaN(Number(arguments[0])))?(0):(parseInt(arguments[0])));}else if(arguments.length==2){minValue=((isNaN(Number(arguments[0])))?(0):(parseInt(arguments[0])));maxValue=((isNaN(Number(arguments[1])))?(0):(parseInt(arguments[1])));}if(maxValue<minValue){var tmpValue=minValue;minValue=maxValue;maxValue=tmpValue;}Math.randomize.startValue=(((Math.randomize.startValue*Math.randomize.a)+Math.randomize.b)%Math.randomize.c);return((minValue==maxValue)?(Math.randomize.startValue/Math.randomize.c):(minValue+Math.floor((maxValue-minValue+1)*Math.randomize.startValue/Math.randomize.c)));};Math.randomize.a=4096;Math.randomize.b=150889;Math.randomize.c=714025;Math.randomize.startValue=(((new Date()).getTime())%Math.randomize.c);
  Array.prototype.remove = function(obj) {var arr=this,i=0;while(i<arr.length){if(arr[i]===obj){arr=arr.slice(0,i).concat(arr.slice(i+1,arr.length));--i;}++i;}for(i=0;i<arr.length;++i){this[i]=arr[i];}this.length=arr.length;};

  if (teaser.large) {

    teaser.large.weighting = 3;
    if((typeof firstLargeTeaserFixed)!= "undefined"){
      teaser.large.firstEntryIsFix = firstLargeTeaserFixed;
    }else{
      teaser.large.firstEntryIsFix = true
    }

    teaser.large.getSequence = function() {
      var arr = [];
      var teaserArr = teaser.large;
      var amplifier = teaserArr.weighting;
      var fixDifference = ((teaserArr.firstEntryIsFix) ? (0) : (1));
      var arrLength = (((teaserArr.length-1+fixDifference) <= (4+fixDifference)) ? (teaserArr.length-1+fixDifference) : (4+fixDifference));
      var fixEntries = ((teaserArr.firstEntryIsFix) ? ([teaserArr[0]]) : (null));
      var lotteryPot = [], i;
      for (i=(1-fixDifference); i<teaserArr.length; ++i) {
        lotteryPot[lotteryPot.length] = i;
      }
      while (arr.length < arrLength) {
        i = lotteryPot[Math.floor(Math.pow(Math.randomize(),amplifier)*lotteryPot.length)];
        lotteryPot.remove(i);
        arr[arr.length] = teaserArr[i];
      }
      if (fixEntries) {
        arr = fixEntries.concat(arr);
      }
      return arr;
    };
  }
  if (teaser.main) {

    teaser.main.weighting = 3;
    teaser.main.firstEntryIsFix = true;

    teaser.main.getSequence = function() {
      var arr = [];
      var teaserArr = teaser.main;
      var amplifier = teaserArr.weighting;
      var fixDifference = ((teaserArr.firstEntryIsFix) ? (0) : (1));
      var arrLength = (((teaserArr.length-1+fixDifference) <= (4+fixDifference)) ? (teaserArr.length-1+fixDifference) : (4+fixDifference));
      var fixEntries = ((teaserArr.firstEntryIsFix) ? ([teaserArr[0]]) : (null));
      var lotteryPot = [], i;
      for (i=(1-fixDifference); i<teaserArr.length; ++i) {
        lotteryPot[lotteryPot.length] = i;
      }
      while (arr.length < arrLength) {
        i = lotteryPot[Math.floor(Math.pow(Math.randomize(),amplifier)*lotteryPot.length)];
        lotteryPot.remove(i);
        arr[arr.length] = teaserArr[i];
      }
      if (fixEntries) {
        arr = fixEntries.concat(arr);
      }
      return arr;
    };
  }
  if (teaser.smallstandardLeft) {

    teaser.smallstandardLeft.weighting = 3;

    teaser.smallstandardLeft.getSequence = function() {
      var arr = [];
      var teaserArr = teaser.smallstandardLeft;
      var amplifier = teaserArr.weighting;
      var arrLength = ((teaserArr.length <= 6) ? (teaserArr.length) : (6));
      var lotteryPot = [], i;
      for (i=0; i<teaserArr.length; ++i) {
        lotteryPot[lotteryPot.length] = i;
      }
      while (arr.length < arrLength) {
        i = lotteryPot[Math.floor(Math.pow(Math.randomize(),amplifier)*lotteryPot.length)];
        lotteryPot.remove(i);
        arr[arr.length] = teaserArr[i];
      }
      return arr;
    };
  }
  if (teaser.smallStandardRight) {

    teaser.smallStandardRight.weighting = 3;

    teaser.smallStandardRight.getSequence = function() {
      var arr = [];
      var teaserArr = teaser.smallStandardRight;
      var amplifier = teaserArr.weighting;
      var arrLength = ((teaserArr.length <= 2) ? (0) : (((teaserArr.length-3) <= 3) ? (teaserArr.length-3) : (3)));
      var fixEntries = [], i;
      if (teaserArr.length <= 2) {
        for (i=0; i<teaserArr.length; ++i) {
          fixEntries[i] = teaserArr[i];
        }
      } else {
        fixEntries = [teaserArr[0],teaserArr[1],teaserArr[2]];
        var lotteryPot = [];
        for (i=3; i<teaserArr.length; ++i) {
          lotteryPot[lotteryPot.length] = i;
        }
      }
      while (arr.length < arrLength) {
        i = lotteryPot[Math.floor(Math.pow(Math.randomize(),amplifier)*lotteryPot.length)];
        lotteryPot.remove(i);
        arr[arr.length] = teaserArr[i];
      }
      arr = fixEntries.concat(arr);
      return arr;
    };
  }
}