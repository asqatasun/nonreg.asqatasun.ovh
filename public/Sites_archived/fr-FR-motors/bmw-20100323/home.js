var largeTeaserAutoRotateActiv = false;
checkClient();
scrollerCheckElements = new Array("largeTeaser", "largeTeaserBasic", "mainTeaser", "mainTeaserPreloader","largeTeaserFlash", "teaserBgLine");
window.onresize = checkWindowSize;

var newsActive                = false;
var loadPageCounter           = 0;
var currentLargeTeaser        = 0;
var currentLargeTeaserName    = "";
var currentlargeFlashTeasers  = 0;
var standardRightGroup        = 0;
var currentMainTeaser         = 0;
var standardLeftTeaserGroup   = 0;
var largeFlashTeasers         = new Array();
var largeBasicTeasers         = new Array();
var largeBasicTeasersPreload  = new Image();
var currentLargeTeaserImage   = "";
var nextLargeTeaserImage      = "";
var largeTeaserArray          = new Array();
var standardLeftTeaserArray   = new Array();
var mainTeaserArray           = new Array();
var standardRightTeaserArray  = new Array();
var nextLinkClicked = false;
var teaserSpecificRuntime = null;
var teaserTimeout = null;

function initHomepage(){
  standardLeftTeaserArray = teaser.smallstandardLeft.getSequence();
  teaserCounter = 0;
  for (var i=standardLeftTeaserGroup; i < standardLeftTeaserArray.length; i++){
    for (var j=0; j < teaser.smallstandardLeft.length; j++){
      if(teaser.smallstandardLeft[j] == standardLeftTeaserArray[i]){
        setVisibility("standardLeftTeaserlist" + j, null, 'block');
        teaserCounter++;
        break;
      }
    }
    if(teaserCounter==3){
      standardLeftTeaserGroup = 3;
      break;
    }
  }

  standardRightTeaserArray = teaser.smallStandardRight.getSequence();
  teaserCounter = 0;
  for (var i=standardRightGroup; i < standardRightTeaserArray.length; i++){
    for (var j=0; j < teaser.smallStandardRight.length; j++){
      if(teaser.smallStandardRight[j] == standardRightTeaserArray[i]){
        setVisibility("standardRightTeaserlist" + j, null, 'block');
        teaserCounter++;
        break;
      }
    }
    if(teaserCounter==3){
      standardRightGroup = 3;
      break;
    }
  }

  largeTeaserArray = teaser.large.getSequence();

  useLageFlashTeaser = false;
  if(highbandUser){
    for(i=0;i<largeFlashTeasers.length;i++){
      var testUrl1 = largeTeaserArray[currentLargeTeaser].substring(largeTeaserArray[currentLargeTeaser].indexOf("_teaserpool/"),largeTeaserArray[currentLargeTeaser].length);
      var testUrl2 = (largeFlashTeasers[i].teaserHtmlUrl.substring(largeFlashTeasers[i].teaserHtmlUrl.indexOf("_teaserpool/"), largeFlashTeasers[i].teaserHtmlUrl.length));
      var useSWFTeaser = (largeFlashTeasers[i].swfurl != "none");
      if(useSWFTeaser && testUrl1.indexOf(testUrl2) != -1){
        currentlargeFlashTeasers = i;
        useLageFlashTeaser = true;
        document.getElementById("largeTeaserBasic").style.display = "none";
        document.getElementById("largeTeaser").style.display = "none";
        document.getElementById("largeTeaserFlash").style.display = "block";
        prepareLargeTeaserImage();
        nextTeaser();
        break;
      }
    }
  }
  if(!useLageFlashTeaser){// init
    for (var j=0; j < teaser.large.length; j++){
      var testUrl1 = teaser.large[j].substring(teaser.large[j].indexOf("_teaserpool/"),teaser.large[j].length);
      var testUrl2 = largeTeaserArray[currentLargeTeaser].substring(largeTeaserArray[currentLargeTeaser].indexOf("_teaserpool/"),largeTeaserArray[currentLargeTeaser].length);
      if(testUrl1 == testUrl2){
        currentLargeTeaserImage = largeBasicTeasers[j];
        currentLargeTeaserName = "largeTeaserlist" + j;
        document.getElementById("largeTeaserFlash").style.display = "none";
        document.getElementById("largeTeaserBasic").style.display = "block";
        document.getElementById("largeTeaser").style.display = "block";

        prepareLargeTeaserImage();
        break;
      }
    }
    preLoadArray["12345"] = new Array();
    preLoadArray["12345"].push(largeBasicTeasers[0]);
    preloader("12345");
  }

  mainTeaserArray = teaser.main.getSequence();
  teaserCounter = 0;
  for (var i=0; i < mainTeaserArray.length; i++){
    for (var j=0; j < teaser.main.length; j++){
      if(teaser.main[j] == mainTeaserArray[i]){
        setVisibility("mainTeaserlist" + j, null, 'block');
        teaserCounter++;
        break;
      }
    }
    if(teaserCounter==1){
      break;
    }
  }

  if(largeTeaserArray.length > 1){
    setVisibility('nextLargeTeaserButton', 1);
  }
  if(mainTeaserArray.length > 1){
    setVisibility('mainTeaserContinueLink', 1);
  }
  if(standardLeftTeaserArray.length > 3){
    if(standardLeftTeaserArray.length == 4){
      setVisibility('standardLeftTeaserContinueLinkSingle', 1);
    }else{
      setVisibility('standardLeftTeaserContinueLinkMulti', 1);
    }
  }
  if((standardRightTeaserArray.length > 3)&&(!newsActive)){
    if(standardRightTeaserArray.length == 4){
      setVisibility('standardRightContinueLinkSingle', 1);
    }else{
      setVisibility('standardRightContinueLinkMulti', 1);
    }
  }
}



function changeTeaser(teaserType){
  //alert("ChangeTeaser");
  if(teaserType == "standardLeftTeaser"){
    for (var j=0; j < teaser.smallstandardLeft.length; j++){
      setVisibility("standardLeftTeaserlist" + j, null, 'none');
    }
    teaserCounter = 0;
    for (var i=standardLeftTeaserGroup; i < standardLeftTeaserArray.length; i++){
      for (var j=0; j < teaser.smallstandardLeft.length; j++){
        if(teaser.smallstandardLeft[j] == standardLeftTeaserArray[i]){
          setVisibility("standardLeftTeaserlist" + j, null, 'block');
          teaserCounter++;
          break;
        }
      }
      if(teaserCounter==3){
        break;
      }
    }
    setVisibility('standardLeftTeaserContinueLinkMulti', 0);
    setVisibility('standardLeftTeaserContinueLinkSingle', 0);
    if(standardLeftTeaserGroup == 0){
      standardLeftTeaserGroup = 3;
      if(standardLeftTeaserArray.length == 4){
        setVisibility('standardLeftTeaserContinueLinkSingle', 1);
      }else{
        setVisibility('standardLeftTeaserContinueLinkMulti', 1);
      }
    }else{
      standardLeftTeaserGroup = 0;
      setVisibility('standardLeftTeaserContinueLinkMulti', 1);
    }
  }

  if(teaserType == "standardRight"){
    for (var j=0; j < teaser.smallStandardRight.length; j++){
      setVisibility("standardRightTeaserlist" + j, null, 'none');
    }
    teaserCounter = 0;
    for (var i=standardRightGroup; i < standardRightTeaserArray.length; i++){
      for (var j=0; j < teaser.smallStandardRight.length; j++){
        if(teaser.smallStandardRight[j] == standardRightTeaserArray[i]){
          setVisibility("standardRightTeaserlist" + j, null, 'block');
          teaserCounter++;
          break;
        }
      }
      if(teaserCounter==3){
        break;
      }
    }
    setVisibility('standardRightContinueLinkMulti', 0);
    setVisibility('standardRightContinueLinkSingle', 0);
    if(!newsActive){
      if(standardRightGroup == 0){
        standardRightGroup = 3;
        if(standardRightTeaserArray.length == 4){
          setVisibility('standardRightContinueLinkSingle', 1);
        }else{
          setVisibility('standardRightContinueLinkMulti', 1);
        }
      }else{
        standardRightGroup = 0;
        setVisibility('standardRightContinueLinkMulti', 1);
      }
    }
  }

  if(teaserType == "mainTeaser"){
    setVisibility("mainTeaserlist" + currentMainTeaser, null, 'none');
    currentMainTeaser++;
    if(currentMainTeaser >= mainTeaserArray.length){
       currentMainTeaser = 0;
    }
    setVisibility("mainTeaserlist" + currentMainTeaser, null, 'block');
  }

  if(teaserType == "largeTeaser"){
    $('#largeTeaserOverlay').fadeIn(500);
    if(currentLargeTeaserName != ""){
      document.getElementById(currentLargeTeaserName).style.display = "none";
    }
    setTimeout("hideOldTeaser();",400);

    
  }
}


function showNextTeaser(){
  //alert("showNextTeaser");
    
    teaserSpecificRuntime = null;
    currentLargeTeaser++;
    if(currentLargeTeaser >= largeTeaserArray.length){
       currentLargeTeaser = 0;
    }
    useLageFlashTeaser = false;
    if(highbandUser){
      document.getElementById("largeTeaser").style.display = "none";
      document.getElementById("largeTeaserBasic").style.display = "none";
      for(i=0;i<largeFlashTeasers.length;i++){
        var testUrl1 = largeTeaserArray[currentLargeTeaser].substring(largeTeaserArray[currentLargeTeaser].indexOf("_teaserpool/"),largeTeaserArray[currentLargeTeaser].length);
        var testUrl2 = (largeFlashTeasers[i].teaserHtmlUrl.substring(largeFlashTeasers[i].teaserHtmlUrl.indexOf("_teaserpool/"), largeFlashTeasers[i].teaserHtmlUrl.length));
        var useSWFTeaser = (largeFlashTeasers[i].swfurl != "none");
        if(useSWFTeaser && testUrl1.indexOf(testUrl2) != -1){
          currentlargeFlashTeasers = i;
          useLageFlashTeaser = true;
          document.getElementById("largeTeaserFlash").style.display = "block";
          prepareLargeTeaserImage();
          nextTeaser();
          break;
        }
      }
    }
    if(!useLageFlashTeaser){ // change   
      document.getElementById("largeTeaser").style.display = "block";
      document.getElementById("largeTeaserBasic").style.display = "block";
      for (var j=0; j < teaser.large.length; j++){
        var testUrl1 = teaser.large[j].substring(teaser.large[j].indexOf("_teaserpool/"),teaser.large[j].length);
        var testUrl2 = largeTeaserArray[currentLargeTeaser].substring(largeTeaserArray[currentLargeTeaser].indexOf("_teaserpool/"),largeTeaserArray[currentLargeTeaser].length);
        if(testUrl1 == testUrl2){
          currentLargeTeaserImage = largeBasicTeasers[j];
          currentLargeTeaserName = "largeTeaserlist" + j;
          if(largeBasicTeasersPreload.src && largeBasicTeasersPreload.src != ""){
            currentLargeTeaserImage = largeBasicTeasersPreload.src;
          }
          prepareLargeTeaserImage();
          break;
        }
      }
      preloader("12345");
    } 
}

function hideOldTeaser(){
 document.getElementById("teaserPlaceholder").src = transGif;
 document.getElementById("teaserPlaceholder").style.display = "none";   
 document.getElementById("largeTeaserFlash").style.display = "none";
 showNextTeaser();
}
function prepareLargeTeaserImage(){
  nextCurrentLargeTeaser = currentLargeTeaser+1;
  if((nextCurrentLargeTeaser > 4)||(nextCurrentLargeTeaser >= largeTeaserArray.length)){
    nextCurrentLargeTeaser = 0;
  }
  for (var k=0; k < teaser.large.length; k++){
    var testUrl1 = teaser.large[k].substring(teaser.large[k].indexOf("_teaserpool/"),teaser.large[k].length);
    var testUrl2 = largeTeaserArray[nextCurrentLargeTeaser].substring(largeTeaserArray[nextCurrentLargeTeaser].indexOf("_teaserpool/"),largeTeaserArray[nextCurrentLargeTeaser].length);
    if(testUrl1 == testUrl2){
      if(nextCurrentLargeTeaser>4){
        nextLargeTeaserImage = largeBasicTeasers[0];
       }else{
        nextLargeTeaserImage = largeBasicTeasers[k];
       }
      break;
    }
  }
  preLoadArray["12345"] = new Array();
  preLoadArray["12345"].push(nextLargeTeaserImage);
}

function preLoadReady(ticketId){
 $('#largeTeaserOverlay').fadeIn(500,showTeasers);
}

function showTeasers(){ 

 if(currentLargeTeaserImage.indexOf('/bmw_edit/') != -1){
  document.getElementById("teaserPlaceholder").src = currentLargeTeaserImage;  
 }else{
  document.getElementById("teaserPlaceholder").src = getWcmsPrefix() + currentLargeTeaserImage;  
 }
 document.getElementById("teaserPlaceholder").style.display = "block";
 document.getElementById(currentLargeTeaserName).style.display = "block"; 
  $('#largeTeaserOverlay').fadeOut(500,autoChangeTeaser);   
}

function autoChangeTeaser(){
  //alert("autoChangeTeaser-"+largeTeaserAutoRotateActiv+"--"+nextLinkClicked);
 clearTimeout(teaserTimeout);
 if(largeTeaserAutoRotateActiv && !nextLinkClicked){
   if(typeof teaserSpecificRuntime!=undefined && teaserSpecificRuntime!=null){
    teaserTimeout = window.setTimeout("largeTeaserAutoRotate()", teaserSpecificRuntime * 1000);
   }else{
    teaserTimeout = window.setTimeout("largeTeaserAutoRotate()", defaultTime * 1000);
   }   
 }
}
function nextTeaser() {
  $('#largeTeaserOverlay').hide();
  var teaserSWFObject = new SWFObject(wrapper_modules, "wrapperModules", "1024", "291", "8", "#FFFFFF");
  teaserSWFObject.addParam("allowScriptAccess", "sameDomain");
  teaserSWFObject.addParam("wmode", "transparent");
  teaserSWFObject.addParam("quality", "BEST");
  teaserSWFObject.addVariable("defaultTimeToRun", defaultTime);
  teaserSWFObject.addVariable("nextTeaserFunction", "autoChangeTeaser()");
  if(!teaserSWFObject.write("largeTeaserFlash")){
    document.getElementById("largeTeaserFlash").style.display = "none";
    useLageFlashTeaser = false;
  }else{
    if ( browserId == "Firefox" && platform == "mac os x" && document.getElementsByTagName( 'embed' ).length > 0 ) {
      document.getElementById( 'teaserBgGrid1' ).style.MozOpacity = "1";
      alterCSSClass( '.largeTeaserLinkBackground, .largeTeaserLinkBackgroundActive', '-moz-opacity', '1' );
      alterCSSClass( '.mainTeaserLinkBackground, .mainTeaserLinkBackgroundActive', '-moz-opacity', '1' );
      alterCSSClass( 'div#moduleNavigation', 'opacity', '1' );
    }
  }
}

function getContent()  {
  return largeFlashTeasers[currentlargeFlashTeasers];
}

function trackLargeTeaser(trackingUrl, teaserIdentifier) {
  if(confTrackingEnabled) {
    trackTeaserClick(trackingPages["large"][0], trackingPages["large"][1], trackingUrl);
  }
  setTimeout('loadTeaserUrl' + teaserIdentifier + '()',500);
}

function loadQuickLinkUrl(Url){
  window.location.href = buildValidServerRelativeUrl(Url);
}

function trackQuicklinksTeaser(label, Url) {
  setOption(label,'','Quicklinks','','','none');
  if(confTrackingEnabled) {
    trackTeaserClick(trackingPages["quicklinks"][0], trackingPages["quicklinks"][1], Url);
  }
  setTimeout('loadQuickLinkUrl(\'' + Url + '\')',500);
  return false;
}


