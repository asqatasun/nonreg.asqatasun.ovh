
function reloadObjs()
{
      if (document.getElementsByTagName) 
      {
            var objs = document.getElementsByTagName("object");
            for (i = 0; i < objs.length; i++) objs[i].outerHTML = objs[i].outerHTML;
      }
}

function IsFlashAvailable()
{
	try { if (new ActiveXObject("ShockwaveFlash.ShockwaveFlash") != null) return true; } catch(e){}
	try { if (navigator.plugins && navigator.plugins["Shockwave Flash"]) return true; } catch(e){}
	return false;
}

function listObj(before_all, before, between, after, after_all, sep)
{
    this.toString=function()
    {
//        alert("list rendering");
        var result = new Array();
        result.push(before_all);
        for(var field in this)
        {
            if (typeof(this[field]) == 'function') continue;
//            alert(field+":"+this[field]);
	      result.push(before+field+between+this[field]+after);
        }
        result.push(after_all);
        return result.join(sep);
    }

}

function flashObj()
{
    this.attrList = new listObj("", "", "='", "'", "", " ");
    this.paramList = new listObj("", "<PARAM NAME='", "' VALUE='", "'>", "", "\n");
    this.emAttrList = new listObj("<EMBED ", "", "='", "'", ">", " ");

    this.toHTMLString = function()
        {
		 return "<OBJECT "+this.attrList+">\n"+this.paramList+this.emAttrList+"\n</OBJECT>";
        }

    this.renderFlashMovie = function()
    {
        document.write(this.toHTMLString());
    }
}