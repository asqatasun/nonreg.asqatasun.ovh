var BottomMenu = {
	anims : [],

	Init : function(){
		var checkStop = function(){
			if(BottomMenu.anims[oHTML.id].stopping)
			{
				BottomMenu.anims[oHTML.id].stopping = false;
				BottomMenu.anims[oHTML.id].open.stop();
				BottomMenu.anims[oHTML.id].close.start();
			}
		}

		if(document.getElementById('campaign_tab1'))
		{

		var max_tab;

	//  count of tabs checking
		for (max_tab = 5; max_tab > 0; max_tab--)
			if(document.getElementById('campaign_tab' + max_tab)) break;

	// Setup Campaign Tabs Slide Panel

		for (var i = 1; i < max_tab + 1; i++ ) 
		{
			this.anims["campaign_tab" + i] = {
				open : new Animation(15, "document.getElementById('campaign_tab"+i+"')").addSequence(0, new AnimationSequence("oHTML", "bottom", "oHTML.parentNode.offsetHeight - oHTML.offsetTop - oHTML.offsetHeight", -4, 1, 25, 20)),
				close : new Animation(15, "document.getElementById('campaign_tab"+i+"')").addSequence(0, new AnimationSequence("oHTML", "bottom", "oHTML.parentNode.offsetHeight - oHTML.offsetTop - oHTML.offsetHeight", -25, 1, 25, 20))
			}

			document.getElementById("campaign_tab" + i).onclick = function(){

					for (var j = 1; j < max_tab+1; j++ ) 
					{
						if (("campaign_tab" + j)==this.id)
						{
							this.className = 'campaign_tab_activ_chosen';
							document.getElementById("campaign_banner"+j).style.display = 'block';
							document.getElementById("campaign_subbanner"+j).style.display = 'block';
							document.getElementById('menu').style.background = "url(" + ImagesBG[j]+ ")";
							document.getElementById('menu').style.backgroundRepeat = "no-repeat";
							document.getElementById('menu').style.backgroundPositionX = "0px";
							document.getElementById('logo').className = "logo_" + LogoColor[j];
							document.getElementById('campaign_banner_link').linkIndex = j;
							document.getElementById('campaign_banner_link').onclick = function(){
								top.location.href = LinksBG[this.linkIndex];
	 						}
						}else
						{
							document.getElementById("campaign_tab" + j).className = 'campaign_tab';
							document.getElementById("campaign_tab" + j).onmouseout();
							document.getElementById("campaign_banner"+j).style.display = 'none';
							document.getElementById("campaign_subbanner"+j).style.display = 'none';
						}
					}

					if(parseInt(this.style.bottom) >= -4) return;
					if(BottomMenu.anims[this.id].close.isPlaying()) BottomMenu.anims[this.id].close.stop();
					BottomMenu.anims[this.id].open.start();
				}

				document.getElementById("campaign_tab"+ i).onmouseover = function(){
					if (this.className != 'campaign_tab_activ_chosen')
					{
						this.className = 'campaign_tab_activ';
						if(parseInt(this.style.bottom) >= -4) return;
						if(BottomMenu.anims[this.id].close.isPlaying()) BottomMenu.anims[this.id].close.stop();
						BottomMenu.anims[this.id].open.start();
					}
				}

				document.getElementById("campaign_tab"+ i).onmouseout = function(e){
					if (this.className != 'campaign_tab_activ_chosen')
					{
						this.className = 'campaign_tab';
						if(BottomMenu.anims[this.id].open.isPlaying()) BottomMenu.anims[this.id].open.stop();
						BottomMenu.anims[this.id].close.start();
					}
				}

			}

		}

	}
}