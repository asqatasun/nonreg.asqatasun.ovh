function insertSpotlightBlock (UnionID, SpotslightID) {
    var union = document.getElementById(UnionID);
    var spotslight = document.getElementById(SpotslightID);
    if(union && spotslight) {
        union.appendChild(spotslight);
    }
}
function setSpotlightsPosition (id) {
                var spotlights = document.getElementById(id);
                if(spotlights) {
                                spotlights.style.marginLeft = "-30px"; 
		isIE = (navigator.appName == "Microsoft Internet Explorer");
		var sp = spotlights.getElementsByTagName('div');
		for (var i = 0; i < sp.length; i++) {
			if (sp[i].className == 'spotlight') {
                                                                if(i == 0){
                                                                            sp[i].style.width = "152px";
                                                                } else {
				            sp[i].style.width = "153px";
                                                                }
			}
		}
                                
                                if(isIE) {
			spotlights.style.styleFloat = "none";
                                                spotlights.style.overflow = "hidden"; 
		}
                }
}
function setSpotlights (spotlights) {
	var sp_cont = document.getElementById(spotlights);
	if (sp_cont) {
		var sp = sp_cont.getElementsByTagName('div');
		for (var i = 0; i < sp.length; i++) {
			if (sp[i].className == 'spotlight') {
				sp[i].onmouseover = function () {this.id = 'spotlightover'}
				sp[i].onmouseout = function () {this.id = ''}
				//sp[i].onclick = function () {document.location.href = this.getElementsByTagName('a')[0].href}
			}
		}
	}
}

function setSearchresults () {
	var sr = document.getElementById('searchresults');
	if (sr) {
		var rows = sr.getElementsByTagName('tr');
		for (var i = 0; i < rows.length; i++) {
			if (rows[i].className != 'searchnav') {
				rows[i].onmouseover = function () {this.className = 'over'}
				rows[i].onmouseout = function () {this.className = ''}
				rows[i].onclick = function () {document.location.href = this.getElementsByTagName('a')[0].href}
			}
		}
	}
}

function setHighlights () {
	var rows = document.getElementsByTagName('td');
	for (var i = 0; i < rows.length; i++) {
		if (rows[i].className == 'vacancy') {
			rows[i].onmouseover = function () {this.id = 'over'}
			rows[i].onmouseout = function () {this.id = ''}
			rows[i].onclick = function () {document.location.href = this.getElementsByTagName('a')[0].href}
		}
	}
}

function setFAQs  () {
	var q_cont = document.getElementById('faqs');
	if (q_cont) {
		var qs = q_cont.getElementsByTagName('div');
		for (var i = 0; i < qs.length; i++) {
			if (qs[i].className == 'qa') {
				qs[i].onclick = function () {
					if (this.className == 'open') {
						this.className = 'qa';
						openFAQ = null;
						return;
					}
					this.className = 'open';
					if (openFAQ) openFAQ.className = 'qa';
					openFAQ = this;
				}
				qs[i].onmouseover = function () {
					if (this.className == 'qa') this.className = 'qa_over';
				}
				qs[i].onmouseout = function () {
					if (this.className == 'qa_over') this.className = 'qa';
				}
			}
		}
	}
}

function selectTab (i) {
	if (currentTab > -1) deSelect(currentTab);
	selected = document.getElementById('tabs').getElementsByTagName('li')[i];
	selected.className = 'selected';
	if (i > 0) {
		pre = document.getElementById('tabs').getElementsByTagName('li')[i - 1];
		pre.className = 'preselected';
	}
	if (i < 3) {
		post = document.getElementById('tabs').getElementsByTagName('li')[i + 1];
		post.className = 'postselected';
	}
	currentTab = i;
}

function deSelect(i) {
	selected = document.getElementById('tabs').getElementsByTagName('li')[i];
	selected.className = '';
	if (i > 0) {
		pre = document.getElementById('tabs').getElementsByTagName('li')[i - 1];
		pre.className = '';
	}
	if (i < 3) {
		post = document.getElementById('tabs').getElementsByTagName('li')[i + 1];
		post.className = '';
	}
	currentTab = -1;
}

function footerNavSelectHandler(selectElementID){
	var selectedIndex = document.getElementById(selectElementID).selectedIndex;
	var URL = document.getElementById(selectElementID).options[selectedIndex].value;
	if (selectedIndex!=0) { 
		pointer = window.open();
		pointer.location.href = URL;	
	}
}

function internalNavSelectHandler(selectElementID){
	var selectedIndex = document.getElementById(selectElementID).selectedIndex;
	var URL = document.getElementById(selectElementID).options[selectedIndex].value;
	if (selectedIndex!=0) { 
		document.location.href = URL;	
	}
}