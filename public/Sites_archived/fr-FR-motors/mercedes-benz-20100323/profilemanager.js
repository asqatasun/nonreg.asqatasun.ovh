/*
User profile API

Usage:  1. get user profile class via "var userProfile = embGetProfileManager()"
		2. get profile information via public methods of returned object, e.g.
		   "userProfile.getFirstName()" or directly via "embGetProfileManager().getFirstName()"

*/

// (hide everything except the things we make public explicitly)
(function() {

var embProfileManager = new EmbProfileManager();

// user profile is singleton
window.embGetProfileManager = function () {
  return embProfileManager;
};

function EmbProfileManager() {
	this.updateProfile = function(/*Function*/ callback) {
		var cookie;
		cookie = get_cookie(this.PROFILE_REF_COOKIE);
		if (cookie) {
			this.PROFILE_REF_COOKIE_VALUE = cookie;
		}
		cookie = get_cookie(this.CMA_PROFILES_COOKIE);
		if (cookie) {
			this.CMA_PROFILES_COOKIE_VALUE = cookie;
		}

		if (this.PROFILE_REF_COOKIE_VALUE && this.secureHost && this.signonHandle && this.locale) {
			var scriptTag = document.createElement("script");
			var srcAttribute = document.createAttribute("src");
			srcAttribute.nodeValue = this.secureHost + this.signonHandle +".profiledata." + this.PROFILE_REF_COOKIE_VALUE + ".js?locale=" + this.locale;
			scriptTag.setAttributeNode(srcAttribute);
			document.getElementsByTagName("head")[0].appendChild(scriptTag);
		}
		
		// call callback if set
		if (callback) {
			callback();
		}
	};
	
	//  private variables
	this.MODEL_COOKIE = "model";
	this.PROFILE_REF_COOKIE = "_pr";
	this.CMA_PROFILES_COOKIE = "cma_profiles";
	this.COOKIE_LAST_CC_BM_CODE = "lastCCBmCode";
	
	var userData = new Object();
	var metaData = new Object();

	this.PROFILE_REF_COOKIE_VALUE = get_cookie(this.PROFILE_REF_COOKIE);
	this.CMA_PROFILES_COOKIE_VALUE = get_cookie(this.CMA_PROFILES_COOKIE);
	
	// Save during first use of loadData for reuse in updateProfile
	this.secureHost = "";
	this.signonHandle = "";
	this.locale = "";
	
	userData.isLoggedIn = false;
	userData.isSoftLoggedIn = false;
	
	this.setUserData = function(data) {
		userData = data;
	};
	
	this.setMetaData = function(data) {
		metaData = data;
	};
	
	this.fixNumberFormatting = function(formattedNumber) {
		// Workaround which will only work in german market!
		var newval = formattedNumber;
		var dotIndex = formattedNumber.lastIndexOf(".");
		var commaIndex = formattedNumber.lastIndexOf(",");
		if (dotIndex > commaIndex) {
			newval = newval.replace(",", ".");
			newval = newval.substring(0, dotIndex) + "," + newval.substring(dotIndex + 1, newval.length);
		}
		return newval;
	};
	
	// public methods
	
	this.getUserId = function() {
		return userData.userId;
	};
	
	// DEPRECATED
/*	this.isRegistered = function () {
		return (null != this.PROFILE_REF_COOKIE_VALUE && this.PROFILE_REF_COOKIE_VALUE.length) > 0
				|| (null != this.CMA_PROFILES_COOKIE_VALUE && this.CMA_PROFILES_COOKIE_VALUE.length > 0); 
	};
*/
	this.isLoggedIn = function () {
		if(userData.isLoggedIn && !userData.isSoftLoggedIn){
			return true;
		}else{
			return false;
		}
	};
	
	this.isSoftLoggedIn = function () {
		if(userData.isLoggedIn && userData.isSoftLoggedIn){
			return true;
		}else{
			return false;
		}
	};

	this.isUnregistered = function () {
		if(!userData.isLoggedIn && !userData.isSoftLoggedIn){
			return true;
		}else{
			return false;
		}
	};

	this.getFirstName = function () {
		return userData.firstName;
	};

	this.getSecondFirstName = function () {
		return userData.secondFirstName;
	};
	
	this.getLastName = function () {
		return userData.lastName;
	};

	this.getSecondLastName = function () {
		return userData.secondLastName;
	};
	
	this.getTitle = function () {
		return userData.title;
	};
	
	this.getSalutation = function () {
		return userData.salutation;
	};
	
	this.getDealerName1 = function () {
		return userData.dealerName1;
	};
	
	this.getDealerName2 = function () {
		return userData.dealerName2;
	};
	
	this.getDealerStreet = function () {
		return userData.dealerStreet;
 	};
 	
 	this.getDealerZIP = function () {
 		return userData.dealerZIP;
 	};
 	
 	this.getDealerCity = function () {
 		return userData.dealerCity;
	};
	
 	this.getZipCode = function () {
		return userData.zipCode;
	};
	
	this.getBirthday = function () {
		return userData.birthday;
	};
	
	this.getFavoriteName = function () {
		return userData.favoriteName;
	};
	
	this.getFavoriteBmCode = function () {
		return userData.favoriteBmCode;
	};
	
	this.getFavoritePrice = function () {
		return this.fixNumberFormatting(userData.favoritePrice);
	};
	
	this.getFavoriteCurrency = function () {
		return userData.favoriteCurrency;
	};
	
	this.getFavoriteRate = function () {
		//return userData.favoriteRate;
		// Rate disabled, just return empty string.
		return "";
	};
	
	this.getFavoriteDate = function () {
		return userData.favoriteDate;
	};
	
	this.getFavoritePriceLabel = function () {
		if(userData.favoritePriceLabel == undefined || userData.favoritePriceLabel == null){
			return "";
		}else{
			return userData.favoritePriceLabel;
		}
	}

	this.getFavoriteRateLabel = function () {
		/*
		if(userData.getFavoriteRateLabel == undefined || userData.getFavoriteRateLabel == null){
			return "";
		}else{
			return userData.favoriteRateLabel;
		}*/
		// Rate disabled, just return empty string.
		return "";
	}
	

	this.getConfigurationBmCode = function () {
		if(get_cookie(this.COOKIE_LAST_CC_BM_CODE)){
			return get_cookie(this.COOKIE_LAST_CC_BM_CODE);
		}else{
			return get_cookie(this.MODEL_COOKIE);
		}
	};
	
	this.getFavoriteImgSmallUrl = function () {
		var imgUrl = userData.favoriteImgSmallUrl;
		if (metaData.crmServiceBypass) {
			if (!imgUrl.indexOf("/") == 0 && !metaData.crmServiceBypass.lastIndexOf("/") == metaData.crmServiceBypass.length - 1) {
				imgUrl = "/" + imgUrl;
			}
			imgUrl = metaData.crmServiceBypass + imgUrl;
		}
		return imgUrl;
	};
	
	this.getFavoriteImgLargeUrl = function () {
		var imgUrl = userData.favoriteImgLargeUrl;
		if (metaData.crmServiceBypass) {
			if (!imgUrl.indexOf("/") == 0 && !metaData.crmServiceBypass.lastIndexOf("/") == metaData.crmServiceBypass.length - 1) {
				imgUrl = "/" + imgUrl;
			}
			imgUrl = metaData.crmServiceBypass + imgUrl;
		}
		return imgUrl;
	};
	
	this.getCCiUrl = function () {
		return "/cci";
	};
	
	this.getSavedVehiclesUrl = function () {
		return "/saved_vehicles";
	};
	
	/**
	 * @returns either the bmcode of the vehicle the user is interested in or null if he has no vehicle interest yet.
	 */
	this.getVehicleInterestBmCode = function () {
		// read out model cookie value;
		return get_cookie(this.MODEL);
	};
	
	this.loadData = function(secureHost, signonHandle, locale) {
		this.secureHost = secureHost;
		this.signonHandle = signonHandle;
		this.locale = locale;

		this.PROFILE_REF_COOKIE_VALUE = get_cookie(this.PROFILE_REF_COOKIE);
		this.CMA_PROFILES_COOKIE_VALUE = get_cookie(this.CMA_PROFILES_COOKIE);
		
		
		if (this.PROFILE_REF_COOKIE_VALUE && this.PROFILE_REF_COOKIE_VALUE!= "null") {
			var scriptpath = this.secureHost + this.signonHandle +".profiledata." + this.PROFILE_REF_COOKIE_VALUE + ".js?locale=" + this.locale;
			document.write("<script src='"+scriptpath+"' type='text/javascript'></script>");
		}
	};
};

})();