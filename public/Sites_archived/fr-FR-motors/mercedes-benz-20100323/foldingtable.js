var /*HashMap*/ ms_foldingtable_elementsLoaded = new Object();
var /*HashMap*/ ms_foldingtable_maxSize = new Object();
var /*HashMap*/ ms_foldingtable_tdimage = new Object();
var /*HashMap*/ ms_foldingtable_tdclass = new Object();
var /*HashMap*/ ActiveBigID = new Object();
var /*HashMap*/ ActiveID = new Object();

function /*void*/ ms_foldingtable_handleRequest(status, data, param, type, statusmsg) {
	if(status == AJAXConnector.SUCCID_LOAD && type == AJAXConnector.RESPONSE_TEXT) {
		ms_foldingtable_printEntry(param, data);
	}
}

function /*void*/ ms_foldingtable_toggleTableEntry(/*Event*/ event) {
	// IE saves events within the window object
	if (!event) event = window.event;
	// IE saves the target for this event on different position
	elem = (event.target) ? event.target : event.srcElement;

	// Resolve the correct node, because the user may
	// click on the div, the p, the a or span tag.
	if (elem.nodeName && elem.nodeName.toLowerCase() == "p") {
		elem = elem.parentNode;
	} else if (elem.nodeName && elem.nodeName.toLowerCase() != "div") {
		elem = elem.parentNode.parentNode;
	}

	// Get values
	var /*String[]*/ values = elem.id.split("@");
	var /*String*/ elementId = values[0];
	var /*String*/ handle = values[1];
	ms_foldingtable_loadEntryData(elementId, handle);
	ms_foldingtable_doToggle(elementId);
}

function /*void*/ ms_foldingtable_loadEntryData(/*String*/ elementId, /*String*/ handle) {
	// Return, if no handle was given
	if (!handle || handle == "") {
		return;
	}

	// Process Ajax, if element has not been loaded yet
	if (ms_foldingtable_elementsLoaded[elementId] == undefined) {
		var /*AJAXConnector*/ ajaxConnector = new AJAXConnector( );

		// Send Ajax request
		ajaxConnector.registerDataHandler(ms_foldingtable_handleRequest);
		ajaxConnector.setMaxRequestTime(5000);
		ajaxConnector.sendRequest(handle + ".ajax.component.copyimagesystem.copyimagesystem_Single.copyimagesystem.html", "", AJAXConnector.REQUEST_GET, elementId);
	}
}

function /*void*/ ms_foldingtable_printEntry(/*String*/ elementId, /*String*/ data) {
	// Set load flag for this layer 
	ms_foldingtable_elementsLoaded[elementId] = "true";
	
	// Check layer existence
	if (getLayer(elementId) != undefined) {
		// Get layer
		var /*XMLNode*/ div = getLayer(elementId);
	
		// Parse result and extract JavaScript code. This step is necessary
		// to evaluate the JavaScript code in the result document!
		try {
			var /*RegExp*/ pattern = /<script[^>]*>([\w\W]*?)<\/script>/gi;
			var arrFlash = new Array();
			while (result = pattern.exec(data)) {
				if(result[1].indexOf("insertFlash") == -1) {
					eval(result[1]);
				} else {
					arrFlash[arrFlash.length] = result[1];
				}
			}
			
			// Replace JavaScript code in result document
			data = data.replace(pattern, "");
		} catch (e) {
		}
		
		// Put result into DOM structure
		div.innerHTML = data;
		
		//insert flash with innerhHTML
		for(var i = 0; i < arrFlash.length; i++) {
			eval(arrFlash[i]);
		}
		
		
		// CR 631: Replace SLT NG Links
		if (window.msSltLinkRewriter) {
			window.msSltLinkRewriter.replaceSltLinks();
		}
	}

}

function /*void*/ ms_foldingtable_doToggle(/*String*/ elementId) {
	if (getLayer(elementId + "TR")) {
		if (getLayer(elementId).style.display == "none"){
			var trElement = getLayer(elementId + "TR");

			// inner HTML abspeichern
			ms_foldingtable_tdimage[elementId] = trElement.lastChild.innerHTML;

			// Image TD loeschen
			trElement.removeChild(trElement.lastChild);

			// TD auf colspan 2 stellen
			trElement.firstChild.setAttribute("colSpan", "2");
			ms_foldingtable_tdclass[elementId] = trElement.className;
			trElement.className = "ms-tb3-row-active";
		}
		else if (getLayer(elementId).style.display == "block"){
			var trElement=getLayer(elementId + "TR");

			// TD auf colspan 1 stellen
			trElement.firstChild.removeAttribute("colSpan");
			trElement.className = ms_foldingtable_tdclass[elementId];

			// Altes TD wiederherstellen
			var tdElement = document.createElement("td");
			tdElement.innerHTML = ms_foldingtable_tdimage[elementId];

			// Fuege TD dem TR an
			trElement.appendChild(tdElement);
		}
	}

	if (getLayer(elementId)) {
		var /*String*/ style = getLayer(elementId).style;
		if (style.display == "none") {
			style.display = "block";
		} else if (style.display == "block") {
			style.display = "none";
		}
	}
}

function imageChange(elementId, step, isID, skipBigImage) {
	if (step == null) {
		step = 0;
	}

	if (!ActiveID[elementId]) {
		ActiveID[elementId] = 0;
	}

	if (!isID) {
		ActiveID[elementId] += step;
	} else {
		ActiveID[elementId] = step;
	}

	for (var i = 0; i < ms_foldingtable_maxSize[elementId]; i++) {
		var currentId = "medienelementImage" + i + elementId;
		if (getLayer(currentId)) {
			getLayer(currentId).style.display = 'none';
		}

		var bigImageId = "bigImageIcon" + i + elementId;
		if (getLayer(bigImageId)) {
			getLayer(bigImageId).style.display = 'none';
		}
	}

	if (ActiveID[elementId] == ms_foldingtable_maxSize[elementId]) {
		ActiveID[elementId] = 0;
	} else if (ActiveID[elementId]== -1) {
	 	ActiveID[elementId] = ms_foldingtable_maxSize[elementId] - 1;
	}

	var currentId = "medienelementImage" + ActiveID[elementId]+elementId;

	if (ms_foldingtable_maxSize[elementId] > 0) {
		var imgLayer = getLayer(currentId);
		if (imgLayer) {
			imgLayer.style.display = 'block';

			var bigImageId = "bigImageIcon" + ActiveID[elementId] + elementId;
			if (getLayer(bigImageId)) {
				getLayer(bigImageId).style.display = 'inline';
			}
			
			var parentUL = getElement("UL", imgLayer, "ms-agc");
			setActiveIcon(parentUL, ActiveID[elementId]);

			if (!skipBigImage) {
				var currentIdBig = "medienelementBigImage" + ActiveID[elementId] + elementId;
				setActiveIcon(getLayer(currentIdBig), ActiveID[elementId]);

			}
		}
	}
}

function setActiveIcon(parent, curStep) {
	if(parent != null) {
		var parent = parent.nodeName == "DIV" ? parent.getElementsByTagName("ul")[0] : parent;
		var ulClass = getClassName(parent);
		if(ulClass == "ms-agc") {
			var arrLinks = parent.getElementsByTagName("li");
			for(var i = 0, k  = 0; i < arrLinks.length; i++) {
				var liClass = getClassName(arrLinks[i]);
				if(liClass == "" || liClass == "ms-active") {
					if(k != curStep) {
						removeClass(arrLinks[i]);
					} else {
						setClass(arrLinks[i], "ms-active");
					}
					k++;
				}
			}
		}
	}
}

function toggleOverlay(showOverlay) {
	var div = document.getElementById("ms-overlay");
	if (showOverlay==true) {
		div.style.display = "block";
	} else {
		div.style.display = "none";
	}
}

function bigImage(elementId, step, isID){
	if(ActiveID[elementId] == undefined) {
		ActiveID[elementId] = 1;
	}

	if (step == null) step = 0;

	if(!isID)
		ActiveBigID[elementId] += step;
	else
		ActiveBigID[elementId] = step;

	for (var i = 0; i < ms_foldingtable_maxSize[elementId]; i++) {
		var currentIdBig = "medienelementBigImage" + i + elementId;
		if(getLayer(currentIdBig)){
			getLayer(currentIdBig).style.display = 'none';
		}
	}

	if (ActiveBigID[elementId] == -2) {
		toggleOverlay(false);
		return;
	} else {
		toggleOverlay(true);
	}
	if (ActiveBigID[elementId] == ms_foldingtable_maxSize[elementId]) ActiveBigID[elementId] = 0;
	if (ActiveBigID[elementId] == -1) ActiveBigID[elementId] = ms_foldingtable_maxSize[elementId] - 1;

	imageChange(elementId, step, isID, true);

	var currentIdBig = "medienelementBigImage" + ActiveBigID[elementId]+elementId;
	var objLayer = getLayer(currentIdBig);

	// Initial value for border
	var width = 4;
	var height = 4;
	
	// Get height of header and footer of the global layer
	var divs = objLayer.getElementsByTagName("div");
	if (divs && divs.length > 0) {
		// Set visible (IE BUG, does not return correct values of hidden elements)
		objLayer.style.display = 'block';
		height += divs[0].offsetHeight;
		objLayer.style.display = 'none';
	}
	if (divs && divs.length > 4) {
		// Set visible (IE BUG, does not return correct values of hidden elements)
		objLayer.style.display = 'block';
		height += divs[5].offsetHeight;
		objLayer.style.display = 'none';
	}
	
	// Check Flash width
	var object = objLayer.getElementsByTagName("object")[0];
	if (object) {
		height += parseInt(object.height);
		width += parseInt(object.width);
	}
	
	// Check Image width
	var image = objLayer.getElementsByTagName("img")[0];
	if (image) {
		// Set visible (IE BUG, does not return correct values of hidden elements)
		objLayer.style.display = 'block';
		height += parseInt(image.height);
		width += parseInt(image.width);
		objLayer.style.display = 'none';
	}
	
	// Set Paragraph width
	var currentIdBigP = "medienelementBigImageModalLayerP-" + ActiveBigID[elementId]+elementId;
	var currentIdBigUL = "medienelementBigImageModalLayerUL-" + ActiveBigID[elementId]+elementId;
	var currentElemBigOject = objLayer.getElementsByTagName("object")[0];
	if (!currentElemBigOject) {
		currentElemBigOject = objLayer.getElementsByTagName("img")[0];
	}
	var currentElemBigP = document.getElementById(currentIdBigP);
	var currentElemBigUL = document.getElementById(currentIdBigUL);
	var currentElemBigPMarginRight = 30; 
	var currentElemBigPPaddingLeft = 3;
	var currentElemBigULMarginLeft = 50;
	var currentElemBigULMarginRight = 10;
	
	if (currentElemBigOject && currentElemBigP && currentElemBigUL) {		
		// Set visible (IE BUG, does not return correct values of hidden elements)
		objLayer.style.display = 'block';		
		var currentElemBigOjectWidth = currentElemBigOject.offsetWidth;
		var currentElemBigPWidth = currentElemBigP.offsetWidth + currentElemBigPMarginRight + currentElemBigPPaddingLeft;
		var currentElemBigULWidth = currentElemBigUL.offsetWidth + currentElemBigULMarginLeft + currentElemBigULMarginRight;
		
		if ((currentElemBigPWidth + currentElemBigULWidth) > currentElemBigOjectWidth ) {
			var minPwidth = 300;
			var newPwidth = parseInt(currentElemBigOjectWidth - currentElemBigULWidth - currentElemBigPMarginRight - currentElemBigPPaddingLeft - 10);
			if (newPwidth<minPwidth) {
				newPwidth=minPwidth;
			}
			currentElemBigP.style.width = newPwidth + "px";
		}		
		objLayer.style.display = 'none';
	}
	
	// Calculate position
	var yOffset = (window.pageYOffset) ? window.pageYOffset : document.documentElement.scrollTop;
	var visibleHeight = (window.innerHeight) ? window.innerHeight : document.documentElement.clientHeight;
	if (visibleHeight < height) {
		visibleHeight = height;
	}
	var topPosition = Math.ceil((visibleHeight - height) / 2) + yOffset;
	var leftPosition = Math.ceil((1000 - width) / 2);
	
	// Set position
	objLayer.style.width = width + "px";
	objLayer.style.left = leftPosition + "px";
	objLayer.style.top = topPosition + "px";

	// Check IFrame resource and change size of IFrame
	try {
		var iframe = objLayer.getElementsByTagName("IFRAME")[0];
		iframe.style.height = (height - 4) + "px";
		iframe.style.width = width + "px";
	} catch (/*Exception*/e) {
		// No IFrame available, just ignore...
	}

	// Set visible
	objLayer.style.display = 'block';
	setActiveIcon(objLayer, ActiveBigID[elementId]);
}

function getElement(elemName, element, className) {
	var tag = element.nextSibling;
	while(tag != null && tag.nodeName != elemName) {
		tag = tag.nextSibling;
	}
	var elemClass = getClassName(tag);
	if(elemClass == className || elemClass.indexOf(" " + className)  || elemClass.indexOf(className + " ")) {
		return tag;
	} else {
		return null;
	}
}