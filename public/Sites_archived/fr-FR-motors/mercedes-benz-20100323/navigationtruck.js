/**
 * used to show the correct navigation within the truck area
 */
 
//used to save the active parts of the navigation
var activeLi1 = "";
var activeDiv = "";
var activeLi2 = "";

//shows the navigation with the correct highlighting of the path
//to the current page
function findCorrectTruckNavi(){
	try {
		var /*List*/ aTags = document.getElementById("ms-navi-main").getElementsByTagName("a");
		var /*boolean*/ hasFound1stLvlEntry = false;
		for (var i = 0; i < aTags.length; i++) {
			var /*String*/ currentHandle = aTags[i].pathname.substring(0, aTags[i].pathname.indexOf("."));
			//IE sometimes has the currentHandle without the '/'
			if(!currentHandle.indexOf("/") == 0){
				currentHandle = "/" + currentHandle;
			}
			var /*String*/ currentLocation = location.pathname.substring(0, location.pathname.indexOf("."));
			var /*String*/ currentPrefix = (currentHandle.length <= currentLocation.length) ? currentLocation.substring(0, currentHandle.length) : currentLocation;
			var /*char*/ charAfterPrefix = (currentPrefix.length < currentLocation.length) ? currentLocation.charAt(currentPrefix.length) : "";
			if(currentHandle == currentLocation || ((currentHandle == currentPrefix) && (charAfterPrefix == "/" || charAfterPrefix == "."))) {
				if (!hasFound1stLvlEntry) {
					setClass(aTags[i], "ms-active");
					setClass(aTags[i].parentNode, "ms-active");
					aTags[i].parentNode.getElementsByTagName("div")[0].style.display = "block";
					activeLi1 = aTags[i].parentNode.getAttribute("id", 1);
					activeDiv = aTags[i].parentNode.getElementsByTagName("div")[0].getAttribute("id", 1)
					hasFound1stLvlEntry = true;
				} else {
					setClass(aTags[i], "ms-active");
					activeLi2 = i;
					break;
				}
			}
		}
	} catch (e) {
		// Ignore: The navigation does not exist or must not be highlighted!
	}
}

function showCorrectTruckNavi(){
	if(activeLi1.length > 1){
		setClass(document.getElementById(activeLi1), "ms-active");
		setClass(document.getElementById(activeLi1).getElementsByTagName("a")[0], "ms-active");	
		if(activeDiv.length > 1){
			document.getElementById(activeDiv).style.display = "block";
			if(activeLi2.length > 0){
				setClass(document.getElementById("ms-navi-main").getElementsByTagName("a")[activeLi2], "ms-active");
			}
		}
	}
}
