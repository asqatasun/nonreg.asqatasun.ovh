/**
 * @filename Flyout2BeanClass.js
 * @fileoverview Class to handle data for the flyout 2 (core navigation)
 * @author sbechtold
 */
function Flyout2Bean () {
	/*XMLNode*/ this.data = null;
	/*String*/ this.headlineSrc = "";
	/*String*/ this.headlineAlt = "";
	/*String*/ this.subHeadline = "";
	/*ArrayList*/ this.links = new Array();
	
	this.readData = function(/*XMLNode*/ node) {
		// IllegalArgumentException: No data given!
		if (!node || node == undefined) {
			return;
		}
		
		var elements = node.getElementsByTagName("component");
		for (var /*int*/ i = 0; i < elements.length; i++) {
			if (elements[i].getAttribute("name") == Flyout2Bean.COMPONENT_ID) {
				this.data = elements[i];
				break;
			}
		}
		
		// IllegalArgumentException: No Data for Flyout2 found!
		if (!this.data || this.data == null) {
			return;
		}
		
		// Check on border tag
		var /*boolean*/ hasBorder = false;
		for (var /*int*/ i = 0; i < this.data.childNodes.length; i++) {
			var /*XMLNode*/ child = this.data.childNodes[i];
			if (child.nodeName == Flyout2Bean.BORDER_TAG) {
				hasBorder = true;
				this.data = child;
				break;
			}
		}
		
		// Get flyout data
		for (var /*int*/ i = 0; i < this.data.childNodes.length; i++) {
			var /*XMLNode*/ child = this.data.childNodes[i];
			
			// Use element nodes only
			if (child.nodeType != 1) {
				continue;
			}
			
			if (child.nodeName == Flyout2Bean.HEADLINE_TAG
					&& child.getAttribute("type") == "corenavigation2-headline") {
				// Get Headline
				if (child.firstChild) {
					this.headlineSrc = child.firstChild.getAttribute("src");
					if (child.firstChild.firstChild) {
						this.headlineAlt = child.firstChild.firstChild.data;
					}
				}
			} else if (child.nodeName == Flyout2Bean.HEADLINE_TAG
					&& child.getAttribute("type") == "corenavigation2-subheadline") {
				// Get SubHeadline
				if (child.firstChild) {
					this.subHeadline = child.firstChild.data;
				}
			} else if (child.nodeName == Flyout2Bean.LIST_TAG) {
				// Get Links
				var /*ArrayList*/ items = child.getElementsByTagName(Flyout2Bean.ITEM_TAG);
				for (var /*int*/ j = 0; j < items.length; j++) {
					this.links[j] = new Object();
					var /*XMLNode*/ item = items[j];
					for (var /*int*/ k = 0; k < item.childNodes.length; k++) {
						var /*XMLNode*/ link = item.childNodes[k];

						// Use element nodes only
						if (link.nodeType != 1) {
							continue;
						}
						
						if (link.nodeName == Flyout2Bean.REFERENCE_TAG) {
							this.links[j][Flyout2Bean.REFERENCE_TAG + "_href"] = link.getAttribute("href");
							this.links[j][Flyout2Bean.REFERENCE_TAG + "_onclick"] = link.getAttribute("onclick");
							if (link.firstChild) {
								this.links[j][Flyout2Bean.REFERENCE_TAG + "_text"] = link.firstChild.data;
							}
						} else if (link.nodeName == Flyout2Bean.HEADLINE_TAG) {
							if (link.firstChild) {
								this.links[j][Flyout2Bean.HEADLINE_TAG] = link.firstChild.data;
							}
						} else if (link.nodeName == Flyout2Bean.PARAGRAPH_TAG) {
							for (var /*int*/ l = 0; l < link.childNodes.length; l++) {
								var /*XMLNode*/ textNode = link.childNodes[l];
								if (textNode.nodeType == 1 && textNode.nodeName == "linebreak") {
									if (this.links[j][Flyout2Bean.PARAGRAPH_TAG]) {
										this.links[j][Flyout2Bean.PARAGRAPH_TAG] += "<br/>";
									} else {
										this.links[j][Flyout2Bean.PARAGRAPH_TAG] = "<br/>";
									}
								} else if (textNode.nodeType == 3) {
									if (this.links[j][Flyout2Bean.PARAGRAPH_TAG]) {
										this.links[j][Flyout2Bean.PARAGRAPH_TAG] += textNode.data;
									} else {
										this.links[j][Flyout2Bean.PARAGRAPH_TAG] = textNode.data;
									}
								}
							}
						} else if (link.nodeName == Flyout2Bean.MEDIA_TAG) {
							var /*XMLNode*/ image = link.getElementsByTagName(Flyout2Bean.IMAGE_TAG)[0];
							this.links[j][Flyout2Bean.IMAGE_TAG + "_src"] = image.getAttribute("src");
							if (image.firstChild) {
								this.links[j][Flyout2Bean.IMAGE_TAG + "_alt"] = image.firstChild.data;
							}
						} 
					}
				}
			}
		}
		
		// CR 631: Replace SLT NG Links
		if (window.msSltLinkRewriter) {
			window.msSltLinkRewriter.replaceSltLinks();
		}
	};
}

Flyout2Bean.COMPONENT_ID = "/apps/emb/ComponentsNG/corenavigation2";
Flyout2Bean.BORDER_TAG = "border";
Flyout2Bean.LIST_TAG = "list";
Flyout2Bean.ITEM_TAG = "item";
Flyout2Bean.HEADLINE_TAG = "heading";
Flyout2Bean.PARAGRAPH_TAG = "paragraph";
Flyout2Bean.REFERENCE_TAG = "reference";
Flyout2Bean.MEDIA_TAG = "media";
Flyout2Bean.IMAGE_TAG = "image";

