// Version       : 1.1
// Last modified : (K. Breynck) 18.11.2003
// Author        : BBDO InterOne Hamburg
// Copyright     : BMW Group 2002-2003

// Unique scripting solutions that are provided for  BMW Group sites are for use exclusively
// within  BMW Group projects.  No other use of these solutions is permitted.

this_url          = location.href;

if (this_url.indexOf("http://wcms20.bmwgroup.com/minimaster_prod") != -1 ) {
  rootpath    = "http://wcms20.bmwgroup.com/minimaster_prod";
  actualpath  = this_url.substr(0, this_url.lastIndexOf("/")+1);
  if(actualpath.indexOf("?") != -1) {
    actualpath = this_url.substr(0, this_url.lastIndexOf("?"));
    actualpath = actualpath.substr(0, actualpath.lastIndexOf("/")+1);
  }
} 
else {
  rootpath    = "";
  actualpath  = "";
}