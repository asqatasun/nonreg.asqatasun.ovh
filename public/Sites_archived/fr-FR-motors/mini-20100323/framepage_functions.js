/** Version       : 1.2.1
 * Last modified : (Soe. Soerries)  25.11.2008
 * Author        : BBDO InterOne Hamburg (W. Rauch)
 * Copyright     : BMW Group 2002-2004
 *
 * Unique scripting solutions that are provided for BMW Group sites are for use exclusively
 * within BMW Group projects. No other use of these solutions is permitted.
 *
 **/
var actualFramesetPath;
var contentFrameSource;

var framesetPage = "index.jsp"; //Name of the Frameset-Page(s)
var initContentURL;
var domainIndex = 0;
var allowedDomain = [
  "://www.mini.",
  "://projects",
  "://ecom.bmwgroup.",
  "://ecomi.bmwgroup.",
  "://localhost",
  "://172.23.56.123",
  "://webi.muc",
  "://wwwint.muc",
  "://siapp8.muc",
  "://accessories.mini",
  "://accessoriesi.mini",
  "://172.26.3.174",
  "://212.224.18.233",
  "://212.224.18.163",
  "://www.mini-partner.de",
  "://mail.bmwdialog.de",
  "://172.26.3.122",
  "://www.miniunited.",
  "://wcms20.bmwgroup.com",
  "://secure.mini.com",
  "://mini-herbststurm2005-pres.rz.interone.de",
  "://mini-herbststurm2005-live.rz.interone.de",
  "://originwww.mini.",
  "://212.224.18.141",
  "://mini-calculator-live.dc.interone.de",
  "://liintrab.muc",
  "://mini-de-pres.ham.interone.de",
  "://originwww-neu.mini.",
  "://local.bmw.fr",
  "://crm.mini.ca",
  "://test.crm.mini.ca"
];
var domainIsAllowed;

function checkForFrameset(indexDistanceIndicator){
  var myPath = self.location.href.split("?")[0];
  var myName = myPath;
  var newLocation = "";
  if (!(parent.frames.length > 2)) {
    if (!isNaN(parseInt(indexDistanceIndicator))) { // Use the indexDistanceIndicator to locate the nearest higher-level frameset
      for (i = 0; i <= parseInt(indexDistanceIndicator); i++) {
        myPath = myPath.substr(0, myPath.lastIndexOf("/"));
      }
      myPath += "/";
      myName = myName.substring(myPath.length, myName.length);
      newLocation = myPath + framesetPage + "?content=" + myName + escape(self.location.search);
    } else { // Extract the filename and append it to the frameset page in that folder
      myName = myName.substr(myName.lastIndexOf("/") + 1, myName.length);
      myPath = self.location.href.split("?")[0];
      myPath = myPath.substr(0, myPath.lastIndexOf("/") + 1);
      newLocation = myPath + framesetPage + "?content=" + myName + escape(self.location.search);
    }
    self.location.href = newLocation;
  }
}

function checkNscForFrameset(framesetPath){
  var urlArray = self.location.href.split("/");
  var tempUrl = urlArray[0] + '/' + urlArray[1] + '/' + urlArray[2] + '/';
  tempUrl += escape(self.location.href.substr(tempUrl.length, self.location.href.length));
  if (!(parent.frames.length > 2)) {
    top.location.href = framesetPath + '?content=' + tempUrl;
  }
}

function getFramesetPath(){
  var tempSubstr;
  if (location.host) { // server
    tempSubstr = location.href.substr(location.href.split("?")[0].substr(0, (location.href.split("?")[0].lastIndexOf(location.host) + location.host.length)).length, location.href.length);
    tempSubstr = tempSubstr.split("?")[0].substr(0, (tempSubstr.split("?")[0].lastIndexOf("/") + 1));
  } else { // local
    tempSubstr = location.href.split("?")[0];
    tempSubstr = tempSubstr.substr(0, (tempSubstr.lastIndexOf("/") + 1));
  }
  return tempSubstr;
}

/**
 @since secChange2008
 */
function checkUrl( requestedUrl, _allowedDomainsArray ){
  var _allowedDomains = ( _allowedDomainsArray === undefined )? allowedDomain:_allowedDomainsArray;
  for (var i = 0; i < _allowedDomains.length; i++) {
    var currentUrl = _allowedDomains[i];
    if ( requestedUrl.indexOf( '://' ) == -1 || (requestedUrl.indexOf( currentUrl ) != -1) && checkUrlPermutation(requestedUrl, currentUrl)) {
      return true;
    }
  }
  return false;
}

function checkUrlPermutation(requestedUrl, allowedDomain){
  // +5 for https, length 7 to get the : or the / 
  var relevantUrlString = requestedUrl.substr(allowedDomain.length + 5, 7);
  if ((relevantUrlString.indexOf("/") >= 0) || ((relevantUrlString.indexOf("/") >= 0) && (relevantUrlString.indexOf(":") >= 0))) {
    return true;
  }
  return false;
}


function pageHandler(){
  actualFramesetPath = ((contentFramePath.indexOf("://") != -1) ? "" : getFramesetPath());
  contentFramePath = ((contentFramePath.indexOf(location.host) != -1) ? contentFramePath.split(top.location.host)[1] : contentFramePath);
  contentFrameParam = (((!location.query.content) && (location.search != "")) ? (location.search + "&" + contentFrameParam.substr(1, contentFrameParam.length)) : contentFrameParam);
  contentFramePath = (((contentFramePath.indexOf("://") != -1)) ? contentFramePath : (((actualFramesetPath != "") && (contentFramePath.lastIndexOf(actualFramesetPath) != -1)) ? contentFramePath.substr((contentFramePath.lastIndexOf(actualFramesetPath) + actualFramesetPath.length), contentFramePath.length) : contentFramePath));
  contentFrameSource = ((flashCheckIsActive) ? flashCheckSource + "?contentFrameSource=" + actualFramesetPath + contentFramePath + escape(contentFrameParam) : actualFramesetPath + contentFramePath + contentFrameParam);
  
  
  if (location.query.content) {
    if (location.query.content.indexOf("://") != -1) {
      /* @since JIRA MINI-13 */
      domainIsAllowed = checkUrl(location.query.content, allowedDomain);
      /*for (var i=0; i < allowedDomain.length; i++) {
        if(location.query.content.indexOf(allowedDomain[i]) != -1) {
          domainIsAllowed = true;
          break;
        }
      }*/
      if (domainIsAllowed) {
        initContentURL = ((flashCheckIsActive) ? flashCheckSource + "?contentFrameSource=" + escape(location.query.content) : location.query.content);
      } else {
        initContentURL = contentFrameSource;
      }
    } else {
      actualFramesetPath = getFramesetPath();
      initContentURL = ((flashCheckIsActive) ? flashCheckSource + "?contentFrameSource=" + actualFramesetPath + escape(location.query.content) : actualFramesetPath + location.query.content);
    }
  } else {
    initContentURL = contentFrameSource;
  }
  
  
  //start ----- external content for mini.ca - replace the live url for minimaster_edit & minimaster_qa ------
  if((location.href.indexOf("minimaster_edit") != -1 || location.href.indexOf("minimaster_qa") != -1) && initContentURL.indexOf("crm.mini.ca") != -1){
    initContentURL = initContentURL.replace(/crm.mini.ca/, "test.crm.mini.ca");
  }
  //end ------------------------------------------------------------------------------------------------------
  
}
