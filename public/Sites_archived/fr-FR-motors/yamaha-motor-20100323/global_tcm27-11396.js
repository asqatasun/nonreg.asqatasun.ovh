﻿	var isIE  = (navigator.appVersion.indexOf("MSIE") != -1) ? true : false;
	var isWin = (navigator.appVersion.toLowerCase().indexOf("win") != -1) ? true : false;
	var isOpera = (navigator.userAgent.indexOf("Opera") != -1) ? true : false;
	
	// JavaScript helper required to detect Flash Player PlugIn version information
	function JSGetSwfVer(i){
		  // NS/Opera version >= 3 check for Flash plugin in plugin array
		  if (navigator.plugins != null && navigator.plugins.length > 0) {
				if (navigator.plugins["Shockwave Flash 2.0"] || navigator.plugins["Shockwave Flash"]) {
					  var swVer2 = navigator.plugins["Shockwave Flash 2.0"] ? " 2.0" : "";
							var flashDescription = navigator.plugins["Shockwave Flash" + swVer2].description;
							descArray = flashDescription.split(" ");
							tempArrayMajor = descArray[2].split(".");
							versionMajor = tempArrayMajor[0];
					  if ( descArray[3] != "" ) {
							tempArrayMinor = descArray[3].split("r");
					  } else {
							tempArrayMinor = descArray[4].split("r");
					  }
							versionMinor = tempArrayMinor[1] > 0 ? tempArrayMinor[1] : 0;
							flashVer = parseFloat(versionMajor + "." + versionMinor);
				} else {
					  flashVer = -1;
				}
		  }
		  // MSN/WebTV 2.6 supports Flash 4
		  else if (navigator.userAgent.toLowerCase().indexOf("webtv/2.6") != -1) flashVer = 4;
		  // WebTV 2.5 supports Flash 3
		  else if (navigator.userAgent.toLowerCase().indexOf("webtv/2.5") != -1) flashVer = 3;
		  // older WebTV supports Flash 2
		  else if (navigator.userAgent.toLowerCase().indexOf("webtv") != -1) flashVer = 2;
		  // Can't detect in all other cases
		  else {
				
				flashVer = -1;
		  }
		  return flashVer;
	}
	
	// If called with no parameters this function returns a floating point value 
	// which should be the version of the Flash Player or 0.0 
	// ex: Flash Player 6r65 returns 6.65
	// If called with reqMajorVer, reqMinorVer this function returns true if that version or greater is installed
	function DetectFlashVer(reqMajorVer, reqMinorVer) 
	{
		  reqVer = parseFloat(reqMajorVer + "." + reqMinorVer);
		  // loop backwards through the versions until we find the newest version      
		  for (i=25;i>0;i--) {      
				if (isIE && isWin && !isOpera) {;
						// This VBScript function is in the topnavigation include. Cant put it here because it is vbscript
					  versionStr = VBGetSwfVer(i);
				} else {
					  versionStr = JSGetSwfVer(i);
				}
				if (versionStr == -1) {
						return false;
				  } else if (versionStr != 0) {
					  if(isIE && isWin && !isOpera) {
							tempArray = versionStr.split(" ");
							tempString = tempArray[1];
							versionArray = tempString .split(",");
							
							versionMajor = versionArray[0];
							versionMinor   = versionArray[2];
							
							versionString = versionMajor + "." + versionMinor;
							versionNum = parseFloat(versionString);
					  } else {
							 versionNum = versionStr;
					  }
					  return (versionNum >= reqVer ? true : false );            
				}
		  }
		  
		  return (reqVer ? false : 0.0);
	}
	
	
	
	function WriteOutFlash(strLink, strFlash, strAltImage, width, height) {
		if ( DetectFlashVer(6,00)) {
			document.write('<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0" width="' + width.toString() + '" height="' + height.toString() + '" id="Flash_Visual" align="middle">\n')
			document.write('<param name="allowScriptAccess" value="sameDomain" />\n')
			document.write('<param name="movie" value="' + strFlash + '" />\n')
			document.write('<param name="FlashVars" value="TargetOne=' + strLink + '" />\n')
			document.write('<param name="wmode" value="transparent"/>\n')
			document.write('<param name="quality" value="high" />\n')
			document.write('<param name="bgcolor" value="#ffffff" />\n')
			document.write('<embed src="' + strFlash + '" width="' + width.toString() + '" height="'+ height.toString() + '" quality="high" bgcolor="#ffffff" wmode="transparent" FlashVars="TargetOne=' + strLink + '"  name="Flash_Visual" align="middle" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />\n')
			document.write('</object>\n');
		} else{
			// Show the alternative image
			document.write('<a href="' + strLink + '" target="_blank"><img src="' + strAltImage + '" border="0" /></a>');
		}				
	}
	
	
	function group_over(id, group) {
		document.getElementById("t" + id).className = group + '_group_selector';
		return;
	}
	
	function group_out(id) {
		document.getElementById("t" + id).className = 'group_selector';
		return;
	}
	
	function segment_over(id) {
		document.getElementById("t" + id).className = 'over_segment_selector';
		return;
	}
	
	function segment_out(id) {
		document.getElementById("t" + id).className = 'segment_selector';
		return;
	}
	
	function img_over(id) {
		document.getElementById("ig" + id).className = 'selected';
		return;
	}
	
	function img_out(id) {
		if (id != imgSelected)
			document.getElementById("ig" + id).className = 'gallery';
		return;
	}
	
	function showImage(id) {
		document.getElementById("ig" + imgSelected).className = 'gallery';
		document.getElementById("ig" + id).className = 'selected';
		document.getElementById("img_container").src = eval("img" + id).src
		imgSelected = id;
	
		return;
	}
	
	function showLarge() {
		imgPopup = window.open(arLargeVersions[imgSelected-1], "ImagePopup", "menubar=no,resizable=no,titlebar=yes,scrollbars=no,top=50,left=100,height=400,width=600,toolbar=no,location=no");
		imgPopup.document.write("<html>");
		imgPopup.document.write("<head>");
		imgPopup.document.write("<title>Yamaha Motor Europe</title>");
		imgPopup.document.write("</head>");
		imgPopup.document.write("<body leftmargin='0' topmargin='0' marginwidth='0' marginheight='0'>");
		imgPopup.document.write("<table width=100% border='0' cellpadding='0' cellspacing='0'>");
		imgPopup.document.write("  <tr bgcolor='#CCCCCC'>");
		imgPopup.document.write("    <td align=center><img src='" + arLargeVersions[imgSelected-1] + "'></td>");
		imgPopup.document.write("  </tr>");
		imgPopup.document.write("  <tr bgcolor='#CCCCCC'>");
		imgPopup.document.write("    <td height='30' align='right' valign='middle'><a href='javascript:window.close();' class='but_grey'>Close</a></td>");
		imgPopup.document.write("  </tr>");
		imgPopup.document.write("</table>");
		imgPopup.document.write("</body>");
		imgPopup.document.write("</html>");
		imgPopup.document.title = document.domain;
		setTimeout("imgPopup.focus()",255);
	
		return;
	}
	
	function showTab(id) {
		if (id == 'name') {
			document.getElementById("navname").className = 'dealer_tab_selected';
			document.getElementById("navloc").className = 'dealer_tab';
			document.getElementById("name").style.display = 'block';
			document.getElementById("loc").style.display = 'none';
		}
		else {
			document.getElementById("navname").className = 'dealer_tab';
			document.getElementById("navloc").className = 'dealer_tab_selected';
			document.getElementById("name").style.display = 'none';
			document.getElementById("loc").style.display = 'block';
		}
	
		return;
	}
	
	function showDD() {
		document.getElementById("ddlist").style.display = 'block';
	}
	
	function hideDD(obj) {
		document.getElementById("ddlist").style.display = 'none';
	}
	
	
	function hideDD_IE(obj) {
		if(!obj.contains(event.toElement)) {
			obj.style.display = 'none';
		}
	}
	
	function gotoURL(url) {
		document.location.href = url;
	}
	
	function createCookie(name,value,days) {
		if (days) {
	
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			var expires = "; expires="+date.toGMTString();
		}
		else var expires = "";
		document.cookie = name+"="+value+expires+"; path=/";
	}
	
	function readCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	}
	
	function eraseCookie(name) {
		createCookie(name,"",-1);
	}

	// format an amount
	// give it a 'precision': nr. of decimals
	// this function requires 'thousand_sep' & 'decimalpart', which should be published from Tridion pub metadata
	function formatFloat(aFloat, aPrecision){
		try {
			precision = default_precision;
			if(!isNaN(aPrecision))
			if(Math.abs(aPrecision)<=10)
			precision = aPrecision;
		} 
		catch(e) {
			precision = default_precision;
		}
		try {
			number = parseFloat(aFloat+'');
			if(isNaN(number))
			return "NaN";
		} 
		catch(e) {
			return "NaN";
		}
		
		number = Math.round(number * Math.pow(10, precision)) / Math.pow(10,precision);
		integerpart = '' + ((number<0) ? Math.ceil(number) : Math.floor(number));
		decimalpart = Math.abs(Math.round((number - integerpart)*(Math.pow(10,precision))));
		if(decimalpart<10) decimalpart="0"+decimalpart;
		if(decimalpart==0) decimalpart="00";
		var buff = "";
		for(j=-1, i=integerpart.length; i>=0; i--, j++){
			if((j%3) == 0 && j>1)
				buff = thousand_sep + buff;
				buff = integerpart.charAt(i) + buff;
		}
		if(precision>0)
			return buff+decimal_point+decimalpart;
			return buff;
	}
	
	function formatInt(aInt){
		return formatFloat(aInt,0);
	}

	// initialize thumbnail actions for PTW details CT
	function initThumbs(){
		
		// GET ALL THUMBNAILS
		thumbscollection = document.getElementById('img_thumbs').getElementsByTagName('a');
		nrofcollections = Math.ceil(thumbscollection.length/8);

		// initialize clicking of thumbnails
		initThumbClicking();
		
		thumbcontainer = document.getElementById('img_thumbs');
		
		btn_prev = document.getElementById('img_nav_prev');
		btn_next = document.getElementById('img_nav_next');
		btn_prev.onclick = function(){
			if(currentview=='grid') currentcollection--;
			if(currentview=='fullframe') currentthumb--;
			loadThumbs();
		}
		btn_next.onclick = function(){
			if(currentview=='grid') currentcollection++;
			if(currentview=='fullframe') currentthumb++;
			loadThumbs();
		}
		
		btn_gridview = document.getElementById('gridview');
		btn_fullframeview = document.getElementById('fullframeview');
		if(thumbscollection.length<2){
			btn_gridview.style.display='none';
			btn_fullframeview.style.display='none';
		}
		else{
			btn_gridview.onclick = function(){
				currentview = 'grid';
				loadThumbs();
				return false;
			}
			btn_fullframeview.onclick = function(){
				currentview = 'fullframe';
				loadThumbs();
				return false;
			}
		}
		
		loadThumbs();
	}	
	
	// when clicking on a thumbnail, go to fullframe view
	function initThumbClicking(){
		for(var i = 0;i<thumbscollection.length;i++){
			thumbscollection[i].position = i;
			thumbscollection[i].onclick = function(){
				currentview = 'fullframe';
				currentthumb = this.position;
				loadThumbs();
				return false;
			}
		}
	}
	
	// initialize expansion/collapse of 360 VR tour
	function init360Expansion(){
		var btn_expand,btn_collapse,expandDiv,layer360;
		
		btn_expand = document.getElementById('expand_360');
		btn_collapse = document.getElementById('collapse_360');
		expandDiv = document.getElementById('prod_swf');
		layer360 = document.getElementById('expand_360_layer');
		disclaimer360 = document.getElementById('disclaimer_360');
		btn_expand.onclick = function(){
			expandDiv.className='prod_swf_expanded';
			btn_expand.style.display='none';
			btn_collapse.style.display='block';
			disclaimer360.style.display='block';
			layer360.style.display='none';
			return false;
		}
		btn_collapse.onclick = function(){
			expandDiv.className='prod_swf';
			btn_expand.style.display='block';
			layer360.style.display='block';
			disclaimer360.style.display='none';
			btn_collapse.style.display='none';
			return false;
		}
		layer360.onclick = function(){
			expandDiv.className='prod_swf_expanded';
			btn_expand.style.display='none';
			layer360.style.display='none';
			disclaimer360.style.display='block';
			btn_collapse.style.display='block';
			return false;
		}

	}

	// change main image when clicking on a colour
	function changeColourImage(elmntIndex){
		if(!(isNaN(elmntIndex)) && !(elmntIndex<0) && !(elmntIndex>=thumbscollection.length)){
			document.getElementById('main_colourimg').src = thumbscollection[elmntIndex][0];
			document.getElementById('colour_name').firstChild.innerHTML = thumbscollection[elmntIndex][1];
			currentthumb = elmntIndex;
		}
	}

	function initColourThumbs(){
		elmnts = document.getElementById('colour_thumbs').getElementsByTagName('img'); 
		for(var i = 0;i<elmnts.length;i++){
			thumbscollection.push(Array(elmnts[i].getAttribute('rel'),elmnts[i].getAttribute('alt')));
		}
	}

	// function that handles display of thumbnails (grid view) or main image (fullframe view)
	function loadThumbs(){
		var bottomstart;
		var topend;
		var arrDwnld;
		
		if(currentview=='fullframe'){
			btn_gridview.className = 'enabled';
			btn_fullframeview.className = 'disabled';
			if(currentthumb>=thumbscollection.length-1){
				btn_next.style.display = 'none';
			}
			else{
				btn_next.style.display = 'block';
			}
			if(currentthumb<=0){
				btn_prev.style.display = 'none';
			}
			else{
				btn_prev.style.display = 'block';
			}
			document.getElementById('main_img').src = thumbscollection[currentthumb].href;
			document.getElementById('main_img').onclick = function(){
				currentview = 'grid';
				loadThumbs();
			}
			document.getElementById('main_img').style.display = 'block';
			if(document.getElementById('gallery_disclaimer')) document.getElementById('gallery_disclaimer').style.display = 'block';
			document.getElementById('img_thumbs').style.display = 'none';
			document.getElementById('counter_img').firstChild.innerHTML = '';
			var currentcounter = document.createElement('b');
			currentcounter.innerHTML = currentthumb+1;
			var pipe = document.createTextNode(' | ');
			var thumblength = document.createTextNode(thumbscollection.length);
			document.getElementById('counter_img').firstChild.appendChild(currentcounter);
			document.getElementById('counter_img').firstChild.appendChild(pipe);
			document.getElementById('counter_img').firstChild.appendChild(thumblength);

			arrDwnld = thumbscollection[currentthumb].getAttribute('rel').split('|');
			var spanelmnt = document.createElement('span');
			var dwnld = document.createElement('b');
				dwnld.innerHTML = lbl_download;
			var br = document.createElement('br');
			var dimensions = document.createTextNode(arrDwnld[0]);
			var mmsize = document.createTextNode(' ('+arrDwnld[2]+')');
			spanelmnt.appendChild(dwnld);
			spanelmnt.appendChild(br);
			spanelmnt.appendChild(dimensions);
			spanelmnt.appendChild(mmsize);
			
			document.getElementById('dwnld_img').innerHTML = '';
			document.getElementById('dwnld_img').appendChild(spanelmnt); 
			document.getElementById('dwnld_img').style.display='block';
			document.getElementById('dwnld_img').onclick = function(){
				window.open(arrDwnld[1]);
			}
		}
		else if(currentview=='grid'){
			btn_gridview.className = 'disabled';
			btn_fullframeview.className = 'enabled';
			bottomstart = (currentcollection-1)*8;
			topend = (currentcollection*8)-1;
			if(currentcollection>=nrofcollections){
				btn_next.style.display = 'none';
			}
			else{
				btn_next.style.display = 'block';
			}
			if(currentcollection<=1){
				btn_prev.style.display = 'none';
			}
			else{
				btn_prev.style.display = 'block';
			}

			for(var i = 0;i<thumbscollection.length;i++){
				if(i>=bottomstart && i<=topend && !(i>thumbscollection.length)){
					thumbscollection[i].style.display='block';
				}
				else{
					thumbscollection[i].style.display='none';
				}
			}
			document.getElementById('main_img').style.display = 'none';
			if(document.getElementById('gallery_disclaimer')) document.getElementById('gallery_disclaimer').style.display = 'none';
			document.getElementById('img_thumbs').style.display = 'block';
			if(topend>=thumbscollection.length) topend = thumbscollection.length-1;
			document.getElementById('counter_img').firstChild.innerHTML = '<b>' + (bottomstart+1) + ' - ' + (topend+1) + '</b> | ' + thumbscollection.length;
			document.getElementById('dwnld_img').style.display='none';
		}
	}

	// open/close techspecs section
	function toggleTechSpec(objTD,elmnt){
		if(document.getElementById(elmnt).style.display=='none'){
			document.getElementById(elmnt).style.display='block';
			objTD.innerHTML = '-';
		}
		else{
			document.getElementById(elmnt).style.display='none';
			objTD.innerHTML = '+';
		}
	}

