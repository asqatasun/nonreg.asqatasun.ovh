CMSMenu = function() {this.init();return this;}

CMSMenu.prototype.init = function()
{
	this.menu_conteneur = 'menu_navigation';
	this.li_on = null;
	this.class_on = '';
	this.menu_timeout = null;
	this.delay_timeout = 500;
	this.menu_display = false;
};


CMSMenu.prototype.search_next_element = function(elt)
{
	while ((elt != null) && (elt.nodeType == 3)) elt = elt.nextSibling;
	return elt;
};


CMSMenu.prototype.get_all_elements = function(elt)
{
	elts = new Array();
	if (!elt || !elt.hasChildNodes()) return null;
	p = this.search_next_element(elt.firstChild);
	while (p != null)
	{
		elts[elts.length] = p;
		p = this.search_next_element(p.nextSibling);
	}
	for (var i in elts)
	{
		if (typeof(elts[i]) != 'function')
			elts = elts.concat(this.get_all_elements(elts[i]));
	}
	return elts;
};

CMSMenu.prototype.resize_menu = function()
{
	cible = document.getElementById(this.menu_conteneur);
	node = this.search_next_element(cible.firstChild);
	total_max = document.getElementById(this.menu_conteneur).offsetWidth;
	total_reel = 0;
	elements_racine = 0;
	while(node!=null)
	{
		total_reel += parseInt(node.offsetWidth);
		if (node.className!='separateur') elements_racine++;
		node = this.search_next_element(node.nextSibling);
	}
	total_disponible = total_max - total_reel;
	if (total_disponible>0)
	{
		margin = Math.floor(total_disponible / (elements_racine * 2));
		node = this.search_next_element(cible.firstChild);	
		correction = false;

		while(node!=null)
		{
			if (node.className!='separateur')
			{
				node.style.marginRight = margin + 'px';
				node.style.marginLeft = margin + 'px';
			}
			node = this.search_next_element(node.nextSibling);
		}
	}
	
	if (navigator.appName.indexOf("Microsoft Internet Explorer")!=-1)
	{
		this.fixie6_setup();
		//this.resize_a();
	}
	else
	{
		this.add_delay();
	}
};


CMSMenu.prototype.resize_a = function()
{
	alertes = 0;
	arr_chars = 'zyxwvutsrqponmlkjihgfedcba�';
	separateurs = new Array();
	arr_elts = this.get_all_elements(document.getElementById(this.menu_conteneur));
	for (var i in arr_elts)
	{
		if (typeof(arr_elts[i]) != 'function')
		{
      if (arr_elts[i].tagName)
      {
        if (arr_elts[i].tagName.toLowerCase()=='a')
        {
          if (arr_elts[i].parentNode.parentNode != document.getElementById(this.menu_conteneur))
          {
            separateur = '';
            pos=0;
            
            while(arr_elts[i].firstChild.nodeValue.indexOf(separateur)!=-1)
            {
              if (pos>arr_chars.length) break;
              separateur = arr_chars.charAt(pos++);
            }
            separateurs[i] = separateur;
            
            while(arr_elts[i].firstChild.nodeValue.indexOf(' ')!=-1)
            {
                arr_elts[i].firstChild.nodeValue = arr_elts[i].firstChild.nodeValue.replace(' ', separateur);
            }				
          }
        }
       }
		}
	}

	for (var i in arr_elts)
	{
		if (typeof(arr_elts[i]) != 'function')
		{
      if (arr_elts[i].tagName)
      {
        if (arr_elts[i].tagName.toLowerCase()=='a')
        {
          if (arr_elts[i].parentNode.parentNode != document.getElementById(this.menu_conteneur))
          {
            arr_elts[i].style.width = arr_elts[i].parentNode.parentNode.offsetWidth+'px';
          }
        }
      }
		}
	}
	
	for (var i in arr_elts)
	{
		if (typeof(arr_elts[i]) != 'function')
		{
      if (arr_elts[i].tagName)
      {
        if (arr_elts[i].tagName.toLowerCase()=='a')
        {
          if (arr_elts[i].parentNode.parentNode != document.getElementById(this.menu_conteneur))
          {
            //if (++alertes<5) alert(arr_elts[i].firstChild.nodeValue)
            while(arr_elts[i].firstChild.nodeValue.indexOf(separateurs[i])!=-1)
            {
                arr_elts[i].firstChild.nodeValue = arr_elts[i].firstChild.nodeValue.replace(separateurs[i], ' ');
            }
          }
        }
      }
		}
	}
	
};


CMSMenu.prototype.fixie6_setup = function() {
	var aTmp2, i, j, oLI, aUL, aA;
	var aTmp = this.xGetElementsByClassName("fixie6", document, "ul");
	for (i=0;i<aTmp.length;i++) {
		aTmp2 = aTmp[i].getElementsByTagName("li");
		for (j=0;j<aTmp2.length;j++) {
			oLI = aTmp2[j];
			aUL = oLI.getElementsByTagName("ol");
			if (aUL && aUL.length) {
				oLI.UL = aUL[0];
				aA = oLI.getElementsByTagName("a");
				if (aA && aA.length)
					oLI.A = aA[0];
				oLI.onmouseenter = function() {
					this.className += " ie6hover";
					this.UL.className += " ie6hoverUL";
					if (this.A) this.A.className += " ie6hoverA";
					//if (WCH) WCH.Apply( this.UL, this, true );
				};
				oLI.onmouseleave = function() {
					this.className = this.className.replace(/ie6hover/,"");
					this.UL.className = this.UL.className.replace(/ie6hoverUL/,"");
					if (this.A) this.A.className = this.A.className.replace(/ie6hoverA/,"");
					//if (WCH) WCH.Discard( this.UL, this );
				};
			}
		}
	}
};


CMSMenu.prototype.xGetElementsByClassName = function(clsName, parentEle, tagName) {
	var elements = null;
	var found = new Array();
	var re = new RegExp('\\b'+clsName+'\\b');
	if (!parentEle) parentEle = document;
	if (!tagName) tagName = '*';
	if (parentEle.getElementsByTagName) {elements = parentEle.getElementsByTagName(tagName);}
	else if (document.all) {elements = document.all.tags(tagName);}
	if (elements) {
		for (var i = 0; i < elements.length; ++i) {
			if (elements[i].className.search(re) != -1) {
				found[found.length] = elements[i];
			}
		}
	}
	return found;
};


CMSMenu.prototype.add_delay = function()
{
	arr_elts = this.get_all_elements(document.getElementById(this.menu_conteneur));

	if (arr_elts != null)
	{
    for (var i in arr_elts)
    {
      if (arr_elts[i] != null && arr_elts[i].className != 'separateur')
      {
        if (arr_elts[i].tagName)
        {
          if (arr_elts[i].tagName.toLowerCase()=='li')
          {
            node = arr_elts[i];
            node._ = this;
            node.onmouseout = function(event)
            {
              this._.menu_display = false;
              obj = this;
              if (this.li_on!=null)
              {
                this._.hidemenu();
                clearTimeout(this._.menu_timeout);
              }
              this._.li_on = obj;
              this._.class_on = obj.className;
              obj.className = 'li_visible';
              this._.menu_timeout = setTimeout("myCMSMenu.hidemenu();", this._.delay_timeout);

              // Propagation stopp�e
              if (event)
              {
                if (event.stopPropagation)
                  event.stopPropagation();
                event.cancelBubble = true;
              }
              
              
              // On allume tout
              while (obj.parentNode!=document.getElementById(this._.menu_conteneur))
              {
                obj = obj.parentNode.parentNode;
                obj.className = 'li_visible';
              }
            };
            
            node.onmouseover = function()
            {
              this._.menu_display = true;
              obj = this;
              
              visibles = this._.xGetElementsByClassName('li_visible', document.getElementById(this._.menu_conteneur));
              for (var j in visibles)
              {
                visibles[j].className = this._.get_li_classname(visibles[j]);
              }
              
              this._.hidemenu();
            };
           }
         }
      }
    }
	}
};


CMSMenu.prototype.hidemenu = function()
{
	if (this.li_on!=null)
	{
		if (!this.menu_display)
		{
			this.li_on.className = this.get_li_classname(this.li_on);
			while(this.li_on.parentNode!=document.getElementById(this.menu_conteneur))
			{
				this.li_on = this.li_on.parentNode.parentNode;
				this.li_on.className = this.get_li_classname(this.li_on);
			}
			this.li_on = null;
			clearTimeout(this.menu_timeout);
		}
		else
		{
			
			this.li_on.className = this.get_li_classname(this.li_on);
			this.li_on = null;
		}
	}
};


CMSMenu.prototype.get_li_classname = function(obj)
{
	cn = '';
	if (obj.parentNode)
	{
		if (obj.parentNode!=document.getElementById(this.menu_conteneur))
		{
			if (obj.firstChild)
			{
				if (obj.firstChild.nextSibling)
				{
					if (this.search_next_element(obj.firstChild))
					{
						if (this.search_next_element(obj.firstChild).tagName.toLowerCase()=='ol') cn='li_w_c';
					}
					if (this.search_next_element(obj.firstChild.nextSibling))
					{
						if (this.search_next_element(obj.firstChild.nextSibling).tagName.toLowerCase()=='ol') cn='li_w_c';
					}
				}
			}
		
		}
	}
	return(cn)
};
