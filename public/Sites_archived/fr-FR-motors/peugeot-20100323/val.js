﻿function IsValidationGroupMatch(control, validationGroup) {
 if ((typeof(validationGroup) == "undefined")) {
    return true;
 }
 var group = validationGroup==null ? "" : validationGroup;
 var controlGroup = "";
 if (typeof(control.validationGroup) == "string") {
    controlGroup = control.validationGroup;
 }
 return (controlGroup == group);
}

