﻿// JavaScript Document

var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari"
		},
		{
			prop: window.opera,
			identity: "Opera"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]

};
BrowserDetect.init();

//Give the pixel number without the px
function getPixels(str){
   var ind = str.indexOf('px');
   str = str.substring(0, ind);
   var pixels = parseInt(str);
   return pixels;
}

function isDefinedElement(id){
	return document.getElementById(id)!=undefined;
}
// Fonction de roll-over
function Roll(id, chemin, arg){
	var adresse = "images/" + chemin + "" + id;
	if(arg==1){
		document.images[id].src = adresse + "_over.gif";
	}
	else{
		document.images[id].src = adresse + "_out.gif";
	}
}

// Fonction de changement de style
function chBgTR(tr, style) {
	tr.className = style;
}

function Switch(arg1, arg2) {
	var elements1 = arg1.split(",");
	for(i=0; i<elements1.length; i++) {
		document.getElementById(elements1[i]).style.display = '' ;
	}
	
	var elements2 = arg2.split(",");
	for(i=0; i<elements2.length; i++) {
		document.getElementById(elements2[i]).style.display = 'none' ;
	}
}

function openPopUp( url, titre, largeur, hauteur) 
{ 
   window.open(url, titre, 'width=' + largeur + ', height=' + hauteur + ', toolbar=0, location=0, directories=0, menuBar=0, scrollbars=1');
} 

//FAQ display-hide behavior
var tempFAQ;
function toggleMe(a,element){
	var elList;
	elList = document.getElementsByTagName('div');
	for (var i = 0; i < elList.length; i++){
		if (elList[i].className == 'answer'){elList[i].style.display='none';}
    }
	
	aList = document.getElementsByTagName('a');
	for (var i = 0; i < aList.length; i++){
		if (aList[i].className == 'faqOn'){aList[i].className=''; }
    }

	var e=document.getElementById(a);
	
	if(!e){return true;}
	if(tempFAQ==a){e.style.display='none'; tempFAQ=''; element.className='';}
	else {e.style.display='block'; tempFAQ=a; element.className='faqOn'; }

	return true;
}


function redirect(arg){
	location.href = arg;
}



function findPosX(obj) {
	return findEltPosition(obj,'x','Left');
}

function findPosY(obj) {
	return findEltPosition(obj,'y','Top');
}

//helper don't call it directly.
function findEltPosition(obj,xOry,TopOrLeft) {
	var cur = 0;
	if (!obj) {
		return;
	}
	if (obj.offsetParent) {
		while (obj.offsetParent) {
			cur += obj['offset'+TopOrLeft];
			obj = obj.offsetParent;
		}
	} else if (obj[xOry]) cur += obj[xOry];
	return cur;
}

function addElement(elem, defUrl, defWidth, defHeight, closePicture) {
	if(document.getElementById('realSizePicture')) {
	    removeElement();
	}

	var newdivglobal = document.createElement('div');
	var divIdNaleGlobal = 'realSizePictureGlobal';
	var divClassNameGlobal = 'ZoombigGlobal'
	newdivglobal.setAttribute('id',divIdNaleGlobal);
	newdivglobal.className = divClassNameGlobal;
	document.body.appendChild(newdivglobal);

    var divTopRight = document.createElement('div');
	divTopRight.className='borderTopRight';
	if(BrowserDetect.browser == 'Explorer' && BrowserDetect.version == '6')
	{
	    divTopRight.style.left = 22 + parseInt(defWidth);
	}

    var img = document.createElement('img');
    img.setAttribute('alt','');
    img.setAttribute('src',AppBasePath+"images/common/dot.gif");
    divTopRight.appendChild(img)
    
	var divBottomLeft = document.createElement('div');
	divBottomLeft.className='borderBottomLeft';

    img = document.createElement('img');
    img.setAttribute('alt','');
    img.setAttribute('src',AppBasePath+"images/common/dot.gif");
    
    divBottomLeft.appendChild(img)

    newdivglobal.appendChild(divTopRight);
    newdivglobal.appendChild(divBottomLeft);
	
	var newdiv = document.createElement('div');
	var divIdName = 'realSizePicture';
	var divClassName = 'Zoombig';
	newdiv.setAttribute('id',divIdName);
	newdiv.className = divClassName; 
	
	newdiv.innerHTML = '<a alt="" href="javascript:removeElement()"><span>'+closePicture+'</span></a>';
	newdiv.innerHTML += '<img alt="" src="'+defUrl+'" width="'+defWidth+'" height="'+defHeight+'" border="0"/>';

	var linkObj = document.getElementById('pictCell');
	
	if (findPosX(elem.parentNode.parentNode) > 485)
	{   
	    var widthzoom =  defWidth;
	    var positionX = findPosX(elem.parentNode.parentNode) - widthzoom +  100;
	    newdivglobal.style.left =positionX + 'px';
	}
	else
	{
	    newdivglobal.style.left =  findPosX(elem.parentNode.parentNode) + 100 + 'px';
	}
	
	newdivglobal.style.top = findPosY(elem.parentNode.parentNode) + 'px';
	newdivglobal.appendChild(newdiv);	 
}

function removeElement() {
	var rmdivGlobal = document.getElementById('realSizePictureGlobal');
	var rmdiv = document.getElementById('realSizePicture');
	
	rmdivGlobal.removeChild(rmdiv);
	document.body.removeChild(rmdivGlobal);
}

//Simple Cell : Vote
var starNumber = 4;

function doVote(voteId)
{
    document.getElementById(voteId+"_voteResult").style.display = 'none';
    document.getElementById(voteId+"_voteInput").style.display = 'block';
    document.getElementById(voteId+"_voteLabel").innerHTML = '';
}

function onStar(voteId,label){
	document.getElementById(voteId+"_voteLabel").innerHTML = label;
}

function outStar(voteId){
	document.getElementById(voteId+"_voteLabel").innerHTML = "";
}

function initMenus2(elementId) {
    var nMenu = document.getElementById(elementId);
    var leMenu=nMenu.getElementsByTagName('ul');
    for(var i=0;i<leMenu.length;i++){
		var liparent=leMenu[i].parentNode;
        addClassName(liparent,"submenu");
    }
}

function blockSitemap()
{
    closeNode = false;
}

var closeNode = false;

function ShowHide(element){
    if(hasClassName(element, "on"))
    {
  	    if(closeNode)
        {
            deleteClassName(element, "on");
  	    }
        else
        {
            closeNode = true;
        }

        if(hasClassName(element.parentNode.parentNode, "on"))
        {
            closeNode = false;
        }
    }
    else
    {
  	    addClassName(element, "on");
  	    if(hasClassName(element.parentNode.parentNode, "on"))
        {
            closeNode = false;
        }
    }
}
function ShowHideAll(numberOfSubmenu)
{ var i;
  var className;
  for(i=1;i<=numberOfSubmenu;i++)
  {
    className = document.getElementById("SubmenuSiteMap" + i).className
    className = className.substr(0,4)
    if (className == "last")
    {
    document.getElementById("SubmenuSiteMap" + i).className = "last submenu";
    }
    else
    {
    document.getElementById("SubmenuSiteMap" + i).className = "submenu";
    }
  }
}
function HideFluxRss(elementId){
    var RSS = document.getElementById(elementId);
    var leRSS=RSS.getElementsByTagName('a');
    for(var i=0;i<leRSS.length;i++){
		var liparent=leRSS[i].parentNode;
		liparent.className="off"; 
    }
}

function ShowHideRSS(element){
	HideFluxRss("RSSList");
	element.parentNode.className="";
}

function Menu2(elementId){
    var menu2 = document.getElementById(elementId);
    var Lemenu2=menu2.getElementsByTagName('a');
    for(var i=0;i<Lemenu2.length;i++){
		var liparent=Lemenu2[i].parentNode;
		var ulchild = Lemenu2[i].parentNode.getElementsByTagName('ul');
		if(liparent.className!="on"){addClassName(liparent,"off");}
		if(ulchild.length==0){addClassName(liparent,"only"); }
    }
}

function ShowHideMenu2(element){
	var tempo = element.parentNode;
	var tempo2 = tempo.parentNode.getElementsByTagName('li');
	var sw = "";
		if(hasClassName(tempo,"on")){sw="on"; }
		for(var j=0;j<tempo2.length;j++) {
			deleteClassName(tempo2[j], "on"); 
			addClassName(tempo2[j],"off");
		}
		if(sw!="on"){addClassName(tempo,"on"); }
}


function HideSearchElement(arg){
	var divelement= document.getElementById("searchPageAdvanced");
	var linkelemnt = document.getElementById("advancedSearch");
	if(arg==0){divelement.className="searchPageAdvancedOff"; linkelemnt.style.display="none"; }
	else if(arg==1){linkelemnt.style.display=""; }
	else{
		if(divelement.className=="searchPageAdvanced"){divelement.className="searchPageAdvancedOff"; }
		else {divelement.className="searchPageAdvanced";}
	}
}

function Menu2b(elementId){
    var menu2 = document.getElementById(elementId);
	addClassName(menu2,"widthJavascript");
}

function chStyle(element){
	var tempo = element.parentNode;

	deleteClassName(tempo,"off"); 
	addClassName(tempo,"on");
}

function chStyleOut(element){
	var tempo = element.parentNode;
	var n3 = element.parentNode.parentNode.parentNode;
	if(n3.id=="menu2h"){
		deleteClassName(tempo, "on"); 
		addClassName(tempo,"off");
	}

	deleteClassName(tempo, "on"); 
	addClassName(tempo,"off");
}

function enlargeImage(elementId, closeLink, width, height, defaultElemId)
{
	var adress = elementId;

	if(document.getElementById('GlobalEnlarge')) {
	   	var rmdiv = document.getElementById('GlobalEnlarge');
			rmdiv.parentNode.removeChild(rmdiv);
	}
	if(document.getElementById('GlobalImage')) {
	   	var rmdiv = document.getElementById('GlobalImage');
			rmdiv.parentNode.removeChild(rmdiv);
	}
	if(document.getElementById('EnlargeIt')) {
	   	var rmdiv = document.getElementById('EnlargeIt');
			rmdiv.parentNode.removeChild(rmdiv);
	}
	if(document.getElementById('EnlargeItBis')) {
	    var rmdiv = document.getElementById('EnlargeItBis');
			rmdiv.parentNode.removeChild(rmdiv);
	}

	var newdivglobal = document.createElement('div');
	var divIdName1 = 'GlobalEnlarge';
	var divClassName1 = 'off';
	newdivglobal.setAttribute('id',divIdName1);
	newdivglobal.className = divClassName1;
	
	var newdiv = document.createElement('div');
	var divIdName2 = 'EnlargeIt';
	var divClassName2 = 'off';
	newdiv.setAttribute('id',divIdName2);
	newdiv.className = divClassName2;

	var newdiv2 = document.createElement('div');
	var divIdName3 = 'EnlargeItBis';
	var divClassName3 = 'off';
	newdiv2.setAttribute('id',divIdName3);
	newdiv2.className = divClassName3;
	
	var newdivGlobimage = document.createElement('div');
	var divIdName4 = 'GlobalImage';
	var divClassName4 = 'off';
	newdivGlobimage.setAttribute('id',divIdName4);
	newdivGlobimage.className = divClassName4;
	
	var divTopRight = document.createElement('div');
	divTopRight.className='borderTopRight';
	if(BrowserDetect.browser == 'Explorer' && BrowserDetect.version == '6')
	{
	    divTopRight.style.left = 16 + parseInt(width);
	}

    var img = document.createElement('img');
    img.setAttribute('alt','');
    img.setAttribute('src',AppBasePath+"images/common/dot.gif");
    divTopRight.appendChild(img)
    
	var divBottomLeft = document.createElement('div');
	divBottomLeft.className='borderBottomLeft';

    img = document.createElement('img');
    img.setAttribute('alt','');
    img.setAttribute('src',AppBasePath+"images/common/dot.gif");
    
    divBottomLeft.appendChild(img)
	

    var content  = document.getElementById("Idcontent");
    var divContentSize = content.offsetHeight; 
    
    content.appendChild(newdivglobal);
    content.appendChild(newdivGlobimage);
    
    content.style.cssText=('position:relative');
    newdivglobal.style.cssText=('height:' + divContentSize + 'px');	
    newdivGlobimage.style.cssText =('height:' + divContentSize + 'px');	
    
    if(document.getElementById('brId')) {
	     var rmdiv = document.getElementById('brId');
			rmdiv.parentNode.removeChild(rmdiv);
	}

	var newdivBr = document.createElement('br');
	var brId  = "brId";
	var brClassBr = 'Clear';
	newdivBr.setAttribute('id',brId);
	newdivBr.className = brClassBr;
	content.appendChild(newdivBr);	

	newdivglobal.appendChild(newdiv);	 
	newdivGlobimage.appendChild(newdiv2);	
	
	newdiv2.appendChild(divTopRight);
    newdiv2.appendChild(divBottomLeft);

 

    var popup = document.getElementById("EnlargeIt");
    var popupBis = document.getElementById("EnlargeItBis");
    var popupGlobal = document.getElementById("GlobalEnlarge");
    var popupGlobalImage = document.getElementById("GlobalImage");
    popup.className = "on";
    popupBis.className = "on";
    popupGlobal.className= "on";
    popupGlobalImage.className = "on";
    popupGlobal.style.cssText = ('height:' + divContentSize + 'px');
    popup.style.cssText = ('height:' + divContentSize + 'px');
    
    if(width != null){
        popupBis.style.width=eval(width)+20+"px";
    }
    if(height != null){
        popupBis.style.height=eval(height)+25+"px";
    }	
	
    popupBis.innerHTML += "<div class=\"pictureZoom\" id=\"pictureZoom\"><a href=\"#\" onclick=\"javascript:closeAllImageDiv();\">"+ closeLink +"</a><img style=\"width:480px;margin-left:8px;\" onerror=\"setDefaultThumbnailImg(this,'"+defaultElemId+"');\" src=\""+adress+"\" alt=\"\" /><br class=\"clear\" /></div>";
    
    if(width != null){
        document.getElementById("pictureZoom").style.width = eval(width) + 20 + "px";
    }
    if(height != null){
        document.getElementById("pictureZoom").style.height = eval(height) + 25 + "px";
    }	
    
    window.scrollTo(0,0);
}


var xpi=null;

function detectActiveXPlugIn(classical,plugin,currentNode) {
	var Found=false;
	//If it is not firefox
	if (navigator.userAgent.indexOf('Firefox')==-1)
		return true; 

	//Loop to list the installed plugins
	for (i in navigator.plugins) {
		var nv=new Object;
		nv = navigator.plugins[i];
		try {
			//Check if it is the type we need
			if (nv[0].type.toLowerCase()=="application/x-oleobject") {
				Found=true;
				break;
			}
		} 
		catch (ex) {
			Found=false;
		}
	}
	//If it is not found, Save the link to download the correct plugin
	if (!Found)
	 {
		if (navigator.userAgent.indexOf('Firefox/2.0'))
			xpi = {'Mozilla Firefox 2.0 ActiveX Plug-in':'http://www.iol.ie/~locka/mozilla/mozactivex-ff-15.xpi'};
		else if (navigator.userAgent.indexOf('Firefox/1.5'))
			xpi = {'Mozilla Firefox 1.5 ActiveX Plug-in':'http://www.iol.ie/~locka/mozilla/mozactivex-ff-15.xpi'};
		else if (navigator.userAgent.indexOf('Firefox/1.0.7'))
			xpi = {'Mozilla Firefox 1.0.7 ActiveX Plug-in':'http://www.iol.ie/~locka/mozilla/mozactivex-ff-107.xpi'};
		else if (navigator.userAgent.indexOf('Firefox/1.0.6'))
			xpi = {'Mozilla Firefox 1.0.6 ActiveX Plug-in':'http://www.iol.ie/~locka/mozilla/mozactivex-ff-106.xpi'};
		else if (navigator.userAgent.indexOf('Firefox/1.0.4'))
			xpi = {'Mozilla Firefox 1.0.4 ActiveX Plug-in':'http://www.iol.ie/~locka/mozilla/mozactivex-ff-104.xpi'};
		else {
			xpi = false;
		}
			
		//For classical video
		var videoCount = 0;
		var videoIndex = new Array();
		
		if (classical == true)
		{
		    elList = document.getElementsByTagName('div');
	        for (var i = 0; i < elList.length; i++){
		        if (elList[i].className == 'videoBloc')
		        {
		            videoIndex[videoCount++] = i;
		        }
            }
            
            if(videoCount > 0)
            {
                var newdiv = document.createElement('div');
                newdiv.className = 'pluginLink';
                newdiv.innerHTML = "<a href='javascript:InstallXPI();'>"+ plugin +"</a>";
                elList[videoIndex[videoCount - 1]].appendChild(newdiv);
            }   
		}
		
		return false;
	}
	else
	 return true;
}

function InstallXPI() {
	if (xpi==false)
		alert("Attention, aucune version du plugin ne semble compatible avec votre navigateur");
	else
		InstallTrigger.install(xpi);
}

function closeAllImageDiv() {

    if(document.getElementById('GlobalImage')) {
	   	var rmdiv = document.getElementById('GlobalImage');
			rmdiv.parentNode.removeChild(rmdiv);
	}
	if(document.getElementById('GlobalEnlarge')) {
	   	var rmdiv = document.getElementById('GlobalEnlarge');
			rmdiv.parentNode.removeChild(rmdiv);
	}
	
	
	if(document.getElementById('EnlargeIt')) {
	    var rmdiv = document.getElementById('EnlargeIt');
			document.body.removeChild(rmdiv);
	}
	
	if(document.getElementById('EnlargeItBis')) {
	   	var rmdiv = document.getElementById('EnlargeItBis');
			document.body.removeChild(rmdiv);
	}
}


function closeAllVideoDiv(){

    var objectvideo = document.getElementById('videoObject');
    if (navigator.appName!="Microsoft Internet Explorer"){
        if(document.getElementById('videoObject'))
        {
      	    objectvideo.innerHTML =  "<object width='" + 0 + "' height='" + 0 + "' classid=\"\" longdesc=\"\" title=\"\" alt=\"\" id=\"videoObject\"> <param value=\"\" name=\"height\"/> <param value=\"" + 0 + "\" name=\"\"/> <param value='' name=\"src\"/> <param value='' name=\"url\"/> <param value=\"\" name=\"autostart\"/><param value=\"true\" name=\"controller\"/> <param value=\"\" name=\"loop\"/><param name=\"wmode\" value=\"transparent\" /> <!--[if gte IE 7]> <!--><object width=\"" + 0 + "\" height=\"" + 0 + "\"  type=\"\" data='' longdesc=\"\" 	title=\"\" alt=\"\"> <param value='' name=\"src\"/> <param value=\"\" name=\"autostart\"/> <param name=\"wmode\" value=\"transparent\" /> <param value=\"1\" name=\"showControls\"/> 	<param value=\"\" name=\"loop\"/> 	<param value=\"1\" name=\"ShowStatusBar\"/> 	<param value=\"1\" name=\"ShowDisplay\"/> 	<img alt=\"\" style=\"width: " + 0 + "px; height: " + 0 + "px;\" src=\"\"/> 	</object><!--<![endif]--> <!--[if lt IE 7]><img src=\"\" style=\"width:250px;height:250px;\" alt=\"\" /><![endif]--></object>" ;	
       	    objectvideo.parentNode.removeChild(objectvideo);
    
        }
        
    }
    else
    {
       objectvideo.controls.stop();
    
    }
    
	if(document.getElementById('GlobalVideoLayer')) {
	   	var rmdiv = document.getElementById('GlobalVideoLayer');
			rmdiv.parentNode.removeChild(rmdiv);
	}
	if(document.getElementById('GlobalVideoDiv')) {
	   	var rmdiv = document.getElementById('GlobalVideoDiv');
			rmdiv.parentNode.removeChild(rmdiv);
	}
	
	
	if(document.getElementById('VideoLayer')) {
	    var rmdiv = document.getElementById('VideoLayer');
			document.body.removeChild(rmdiv);
	}
	
	if(document.getElementById('VideoDiv')) {
	   	var rmdiv = document.getElementById('VideoDiv');
			document.body.removeChild(rmdiv);
	}
}

function enlargeVideo(videoLink, imgLink, closeLink, classId, type, longDesc, title, altVideo, altImg, width, height, autoStart, loop, plugin) {
	
	if(document.getElementById('GlobalVideoLayer')) {
	   	var rmdiv = document.getElementById('GlobalVideoLayer');
			rmdiv.parentNode.removeChild(rmdiv);
	}
	if(document.getElementById('GlobalVideoDiv')) {
	   	var rmdiv = document.getElementById('GlobalVideoDiv');
			rmdiv.parentNode.removeChild(rmdiv);
	}
	
	
	if(document.getElementById('VideoLayer')) {
	    var rmdiv = document.getElementById('VideoLayer');
			document.body.removeChild(rmdiv);
	}
	
	if(document.getElementById('VideoDiv')) {
	   	var rmdiv = document.getElementById('VideoDiv');
			document.body.removeChild(rmdiv);
	}

	var newdivGlobalVideoLayer = document.createElement('div');
	var divIdGlobalVideoLayer = 'GlobalVideoLayer';
	var divClassGlobalVideoLayer = 'off';
	newdivGlobalVideoLayer.setAttribute('id',divIdGlobalVideoLayer);
	newdivGlobalVideoLayer.className = divClassGlobalVideoLayer;
	
	
	var newdivGlobalVideoDiv = document.createElement('div');
	var divIdGlobalVideoDiv = 'GlobalVideoDiv';
	var divClassGlobalVideoDiv = 'off';
	newdivGlobalVideoDiv.setAttribute('id',divIdGlobalVideoDiv);
	newdivGlobalVideoDiv.className = divClassGlobalVideoDiv;
	
	var newdiv = document.createElement('div');
	newdiv.setAttribute('id','VideoDiv');
	newdiv.className = divClassGlobalVideoDiv;
	//newdiv.innerHTML = '<div class=\"videoZoom\" id=\"videoZoom\"><a onclick="closeAllVideoDiv();" href="#">' + closeLink + '</a><br class="clear"/></div>';

	var newdiv2 = document.createElement('div');
	newdiv2.setAttribute('id', 'VideoLayer');
	newdiv2.className = divClassGlobalVideoDiv;
	
	var content  = document.getElementById("Idcontent");
    content.style.cssText=('position:relative');
    
    content.appendChild(newdivGlobalVideoLayer);
    content.appendChild(newdivGlobalVideoDiv);

	var divTopRight = document.createElement('div');
	divTopRight.className='borderTopRight';
	if(BrowserDetect.browser == 'Explorer' && BrowserDetect.version == '6')
	{
	    divTopRight.style.left = 18 + parseInt(width);
	}

    var img = document.createElement('img');
    img.setAttribute('alt','');
    img.setAttribute('src',AppBasePath+"images/common/dot.gif");
    divTopRight.appendChild(img)
    
	var divBottomLeft = document.createElement('div');
	divBottomLeft.className='borderBottomLeft';

    img = document.createElement('img');
    img.setAttribute('alt','');
    img.setAttribute('src',AppBasePath+"images/common/dot.gif");
    
    divBottomLeft.appendChild(img)

    newdiv.appendChild(divTopRight);
    newdiv.appendChild(divBottomLeft);
    
    if(document.getElementById('brId')) {
	    var rmdiv = document.getElementById('brId');
			rmdiv.parentNode.removeChild(rmdiv);
	}
	

	var newdivBr = document.createElement('br');
	var brId  = "brId";
	var brClassBr = 'Clear';
	newdivBr.setAttribute('id',brId);
	newdivBr.className = brClassBr;
	
	
	content.appendChild(newdivBr);	
	var divContentSize = content.offsetHeight; 

    newdiv2.style.cssText = ('height:' + divContentSize + 'px');

	newdivGlobalVideoLayer.appendChild(newdiv2);
	newdivGlobalVideoLayer.style.cssText = ('height:' + divContentSize + 'px');
	
	newdivGlobalVideoDiv.appendChild(newdiv);	 

    var popup = document.getElementById("VideoLayer");
    var popupBis = document.getElementById("VideoDiv");
    var popupGlobalLayer = document.getElementById("GlobalVideoLayer");
    var popupGlobalDiv = document.getElementById("GlobalVideoDiv");
    popup.className = "on";
    popupBis.className = "on";
    popupGlobalLayer.className="on";
    popupGlobalDiv.className = "on";
	
    
    if(width != null){
        popupBis.style.width=eval(width)+20+"px";
    }
    if(height != null){
        popupBis.style.height=eval(height)+25+"px";
    }

    popupBis.innerHTML += "<div class=\"videoZoom\" id=\"videoZoom\"><a href=\"#\" onclick=\"closeAllVideoDiv();\">" + closeLink + "</a><object width='" + width + "' height='" + height + "' classid=\""+ classId +"\" longdesc=\""+ longDesc +"\" title=\"" + title + "\" alt=\"" + altVideo + "\" id=\"videoObject\"> <param value=\"" + height + "\" name=\"height\"/> <param value=\"" + width + "\" name=\"width\"/> <param value='" + videoLink + "' name=\"src\"/> <param value='" + videoLink + "' name=\"url\"/> <param value=\"" + autoStart + "\" name=\"autostart\"/><param value=\"true\" name=\"controller\"/> <param value=\"" + loop + "\" name=\"loop\"/><param name=\"wmode\" value=\"transparent\" /> <!--[if gte IE 7]> <!--><object width=\"" + width + "\" height=\"" + height + "\"  type=\"" + type + "\" data='" + videoLink + "' longdesc=\""+ longDesc +"\" 	title=\"" + title + "\" alt=\"" + altVideo + "\"> <param value='" + videoLink + "' name=\"src\"/> <param value=\"" + autoStart + "\" name=\"autostart\"/> <param name=\"wmode\" value=\"transparent\" /> <param value=\"1\" name=\"showControls\"/> 	<param value=\"" + loop + "\" name=\"loop\"/> 	<param value=\"1\" name=\"ShowStatusBar\"/> 	<param value=\"1\" name=\"ShowDisplay\"/> 	<img alt=\"" + altImg + "\" style=\"width: " + width + "px; height: " + height + "px;\" src=\""+ imgLink +"\"/> 	</object><!--<![endif]--> <!--[if lt IE 7]><img src=\""+ imgLink +"\" style=\"width:250px;height:250px;\" alt=\"" + altImg + "\" /><![endif]--></object><br class=\"clear\" /></div>";

    if(plugin && detectActiveXPlugIn(false,plugin,null)== false)
    {
    	var newdivPluging = document.createElement('div');
	    var divPlugingId  = "iddivplugin";
	    var divPlugingClass = 'classdivPluging';
	    newdivPluging.setAttribute('id',divPlugingId);
	    newdivPluging.className = divPlugingClass;
        newdivPluging.innerHTML="<a href='javascript:InstallXPI();'>"+ plugin +"</a>";
        var Divzoom = document.getElementById("videoZoom");
        Divzoom.style.paddingBottom='17px';
        Divzoom.appendChild(newdivPluging);
        var videodiv = document.getElementById("VideoDiv");
        videodiv.style.paddingBottom='17px';
    }

    if(width != null){
        document.getElementById("videoZoom").style.width = eval(width) + 12 + "px";
    }
    if(height != null){
        document.getElementById("videoZoom").style.height = eval(height) + 25 + "px";
    }
    
    window.scrollTo(0,0);
}

var loaded;

function fullScreen2(videoLink, imgLink, closeLink, classId, type, longDesc, title, altVideo, altImg, width, height, autoStart, loop, alertMessage) {

	if(document.getElementById('GlobalVideoLayer')) {
	   	var rmdiv = document.getElementById('GlobalVideoLayer');
			rmdiv.parentNode.removeChild(rmdiv);
	}
	if(document.getElementById('GlobalVideoDiv')) {
	   	var rmdiv = document.getElementById('GlobalVideoDiv');
			rmdiv.parentNode.removeChild(rmdiv);
	}

	if(document.getElementById('VideoLayer')) {
	    var rmdiv = document.getElementById('VideoLayer');
			document.body.removeChild(rmdiv);
	}
	
	if(document.getElementById('VideoDiv')) {
	   	var rmdiv = document.getElementById('VideoDiv');
			document.body.removeChild(rmdiv);
	}

	var newdivGlobalVideoLayer = document.createElement('div');
	var divIdGlobalVideoLayer = 'GlobalVideoLayer';
	var divClassGlobalVideoLayer = 'off';
	newdivGlobalVideoLayer.setAttribute('id',divIdGlobalVideoLayer);
	newdivGlobalVideoLayer.className = divClassGlobalVideoLayer;

	var newdivGlobalVideoDiv = document.createElement('div');
	var divIdGlobalVideoDiv = 'GlobalVideoDiv';
	var divClassGlobalVideoDiv = 'off';
	newdivGlobalVideoDiv.setAttribute('id',divIdGlobalVideoDiv);
	newdivGlobalVideoDiv.className = divClassGlobalVideoDiv;
	
	var newdiv = document.createElement('div');
	newdiv.setAttribute('id','VideoDiv');
	newdiv.className = divClassGlobalVideoDiv;
	//newdiv.innerHTML = '<div class=\"videoZoom\" id=\"videoZoom\"><a onclick="closeAllVideoDiv();" href="#">' + closeLink + '</a><br class="clear"/></div>';

	var newdiv2 = document.createElement('div');
	newdiv2.setAttribute('id', 'VideoLayer');
	newdiv2.className = divClassGlobalVideoDiv;
	
	var content  = document.getElementById("Idcontent");
    content.style.cssText=('position:relative');
    
    content.appendChild(newdivGlobalVideoLayer);
    content.appendChild(newdivGlobalVideoDiv);
    
    if(document.getElementById('brId')) {
	    var rmdiv = document.getElementById('brId');
			rmdiv.parentNode.removeChild(rmdiv);
	}
	
    var divTopRight = document.createElement('div');
	divTopRight.className='borderTopRight';
	if(BrowserDetect.browser == 'Explorer' && BrowserDetect.version == '6')
	{
	    divTopRight.style.left = 18 + parseInt(width);
	}

    var img = document.createElement('img');
    img.setAttribute('alt','');
    img.setAttribute('src',AppBasePath+"images/common/dot.gif");
    divTopRight.appendChild(img)
    
	var divBottomLeft = document.createElement('div');
	divBottomLeft.className='borderBottomLeft';

    img = document.createElement('img');
    img.setAttribute('alt','');
    img.setAttribute('src',AppBasePath+"images/common/dot.gif");
    
    divBottomLeft.appendChild(img)

    newdiv.appendChild(divTopRight);
    newdiv.appendChild(divBottomLeft);
    
    if(document.getElementById('brId')) {
	    var rmdiv = document.getElementById('brId');
			rmdiv.parentNode.removeChild(rmdiv);
	}

	var newdivBr = document.createElement('br');
	var brId  = "brId";
	var brClassBr = 'Clear';
	newdivBr.setAttribute('id',brId);
	newdivBr.className = brClassBr;

	content.appendChild(newdivBr);	
	var divContentSize = content.offsetHeight; 

    newdiv2.style.cssText = ('height:' + divContentSize + 'px');

	newdivGlobalVideoLayer.appendChild(newdiv2);
	newdivGlobalVideoLayer.style.cssText = ('height:' + divContentSize + 'px');
	
	newdivGlobalVideoDiv.appendChild(newdiv);	 

    var popup = document.getElementById("VideoLayer");
    var popupBis = document.getElementById("VideoDiv");
    var popupGlobalLayer = document.getElementById("GlobalVideoLayer");
    var popupGlobalDiv = document.getElementById("GlobalVideoDiv");
    popup.className = "on";
    popupBis.className = "on";
    popupGlobalLayer.className="on";
    popupGlobalDiv.className = "on";
	
    
    if(width != null){
        popupBis.style.width=eval(width)+20+"px";
    }
    if(height != null){
        popupBis.style.height=eval(height)+25+"px";
    }

    popupBis.innerHTML += "<div class=\"videoZoom\" id=\"videoZoom\"><a href=\"#\" onclick=\"closeAllVideoDiv();\">" + closeLink + "</a><object width='" + width + "' height='" + height + "' classid=\""+ classId +"\" longdesc=\""+ longDesc +"\" title=\"" + title + "\" alt=\"" + altVideo + "\" id=\"videoObject\"> <param value=\"" + height + "\" name=\"height\"/> <param value=\"" + width + "\" name=\"width\"/> <param value='" + videoLink + "' name=\"url\"/> <param value=\"" + autoStart + "\" name=\"autostart\"/><param value=\"true\" name=\"controller\"/> <param value=\"" + loop + "\" name=\"loop\"/><param value=\"transparent\" name=\"wmode\"/> <!--[if gte IE 7]> <!--><object width=\"" + width + "\" height=\"" + height + "\"  type=\"" + type + "\" data='" + videoLink + "' longdesc=\""+ longDesc +"\" 	title=\"" + title + "\" alt=\"" + altVideo + "\"> 	<param value=\"" + autoStart + "\" name=\"autostart\"/> 	<param value=\"1\" name=\"showControls\"/> 	<param value=\"" + loop + "\" name=\"loop\"/> 	<param value=\"1\" name=\"ShowStatusBar\"/> <param value=\"transparent\" name=\"wmode\"/> <param value=\"1\" name=\"ShowDisplay\"/> 	<img alt=\"" + altImg + "\" style=\"width: " + width + "px; height: " + height + "px;\" src=\""+ imgLink +"\"/> 	</object><!--<![endif]--> <!--[if lt IE 7]><img src=\""+ imgLink +"\" style=\"width:250px;height:250px;\" alt=\"" + altImg + "\" /><![endif]--></object><br class=\"clear\" /></div>";

    if(width != null){
        document.getElementById("videoZoom").style.width = eval(width) + 12 + "px";
    }
    if(height != null){
        document.getElementById("videoZoom").style.height = eval(height) + 25 + "px";
    }

    var myObject = document.getElementById('videoObject');
    
    window.scrollTo(0,0);	
    
    loaded = window.setTimeout('autoVideoLoading()', 2000);    
}

function autoVideoLoading()
{
    var myObject = document.getElementById('videoObject');
    if (myObject && myObject.playState  == 3)
    {
        myObject.controls.play();
        myObject.fullScreen = true;
        window.clearTimeout(loaded);
    }
    else{
        window.clearTimeout(loaded);
        loaded = window.setTimeout('autoVideoLoading()', 2000);
    }
}

function openLink(elementId)
{
    
	var page = document.getElementById(elementId).value;   
    
    if (page != 'DEFAULT')
    {
       var arrayOfParams = page.split("|");
      
       var type=arrayOfParams[0]
       var popupCode=arrayOfParams[1]
       var url=arrayOfParams[2]
       var width=arrayOfParams[3]
       var height=arrayOfParams[4]
       var posX=arrayOfParams[5]
       var posY=arrayOfParams[6]
       var statusbar=arrayOfParams[7]
       var dimensionbar=arrayOfParams[8]
       var scrollbar=arrayOfParams[9]
       var menubar=arrayOfParams[10]
       if(type == 'External')
       {
           if(popupCode == '_popup')
           {
               var windowOption = "";
	                windowOption = "scrollbars=" + scrollbar
							        + ", status=" + statusbar
							        + ", resizable=" + dimensionbar
							        + ", menubar=" + menubar
							        + ", width=" + width
							        + ", height=" + height
						            + ", left=" + posX
						            + ", top=" + posY
							        + "";
			        window_handle = window.open(url, popupCode, windowOption);
			        window_handle.focus(); // focus sur popup
	        }
	        if(popupCode == '_self')
	        { 
		       window.location.href = url;
	        }
	        if(popupCode == '_blank')
	        { 
		        window_handle = window.open(url,'');
		        window_handle.focus(); // focus sur popup   
	        }
	   }
	   else
	   {
	        window.location.href = url;
	   }
    } 
}

// We get the object from its attribute name
function getObj(name)
{
   if (document.getElementById)
   {
      return document.getElementById(name);
   }
   else if (document.all)
   {
      return document.all[name];
   }
   else if (document.layers)
   {
      return document.layers[name];
   }
   else return false;
}

var stateTimer = null;

// display the window in full screen for IE
function fullScreen(videoId)
{  
    if (3 == document.getElementById(videoId).playState) 
    {
        document.getElementById(videoId).fullScreen = true; 
    }
    else
    {
        document.getElementById(videoId).controls.play();
        if(stateTimer == null) stateTimer = window.setInterval(function (){checkState(videoId)}, 500); 
    }
}

function checkState(videoId) 
{
    if (3 == document.getElementById(videoId).playState) 
    {
        window.clearInterval(stateTimer);
        stateTimer = null;
        document.getElementById(videoId).fullscreen = true;
    }
}


function setDefaultThumbnailImg(img,urlDefault)
{
   img.onerror=null;
   img.src = urlDefault;
}

function printWin (formElem,action){
    var win=openPopUp('about:blank','dialogPrint',"1024","768","","",1,1,1,1); 
    var form=formElem;
    while (form.nodeType!=1 || (form.tagName.toLowerCase()!='form' && form.tagName.toLowerCase()!='body') ){
        form=form.parentNode;
    }
    if (form.tagName.toLowerCase()!='form') return true;
    formElem.oldAction=form.action; 
    formElem.oldTarget=form.target; 
    if (form.action.indexOf("?")>0){ form.action=form.action+"&ActivPrint=on"; }
    else { form.action=form.action+"?ActivPrint=on"; }
    form.target='dialogPrint'; 
    form.submit(); 
    form.action=formElem.oldAction; 
    form.target=formElem.oldTarget; 
    return false;
}