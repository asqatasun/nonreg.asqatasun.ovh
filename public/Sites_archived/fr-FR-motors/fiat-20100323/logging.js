/* =====================================================================
 * ===============================sophus3===============================
 * logging script 'logging.js'

 * for 
 * Fiat Corporate websites
  
 * Issued on 05/03/2010
 * Copyright (c) Sophus Ltd All rights reserved. Patent Pending.
 * http://www.sophus3.com
  * =====================================================================*/
 
if (typeof tc_logging_active == 'undefined') tc_logging_active = true;
// If you want to switch off the tracking of the site use the following line instead
// tc_logging_active = false;

// adds the query string to page aliases
if (typeof tc_page_alias != 'undefined') {
	if (location.search != null && location.search.length > 1) {
		if (tc_page_alias.indexOf('?') == -1) {
			tc_page_alias = tc_page_alias + location.search;
		}
		else {
			tc_page_alias = tc_page_alias + "&" + location.search.substring(1);
		}
	}
}

tc_site_id = s3_get_site_id();
tc_netmining_url = s3_get_netmining_url(tc_site_id);
tc_server_url = "fiat.touchclarity.com";
tc_log_path = "/FIAT_ITALIA/touchclarity";
document.write("<scr"+"ipt language='JavaScript' type='text/javascript' src='"+tc_log_path+"/logging-code.js'></scr"+"ipt>");

if (tc_netmining_url != null) document.write("<scr"+"ipt language='JavaScript' type='text/javascript' defer='defer' src='"+tc_netmining_url+"'></scr"+"ipt>\n");

// custom function
function s3_get_site_id() {
	var domain = document.location.hostname;
	domain = domain.toLowerCase();
	domain = domain.substring((domain.lastIndexOf(".")+1), domain.length);
	if (domain.toLowerCase() == "de") tc_site_id = 76;
	else if (domain.toLowerCase() == "es") tc_site_id = 78;
	else if (domain.toLowerCase() == "fr") tc_site_id = 77;
	else if (domain.toLowerCase() == "com") {
		tc_site_id = s3_COM_siteID();
		}
	else if (domain.toLowerCase() == "it") tc_site_id = 79;
	else if (domain.toLowerCase() == "nl") tc_site_id = 374;
	else if (domain.toLowerCase() == "pl") tc_site_id = 470;
	else if (domain.toLowerCase() == "uk") tc_site_id = 75;
	else if (domain.toLowerCase() == "pt") tc_site_id = 558;
	else if (domain.toLowerCase() == "ie") tc_site_id = 556;
	else if (domain.toLowerCase() == "dk") tc_site_id = 554;
	else if (domain.toLowerCase() == "be") tc_site_id = 553;
	else if (domain.toLowerCase() == "lu") tc_site_id = 553;
	else if (domain.toLowerCase() == "hu") tc_site_id = 555;
	else tc_site_id = 608;  // Fiat Default
	return tc_site_id;
}

// custom function
function s3_get_netmining_url(site) {
	if (site == 76) return "http://de.fiat.netmining.com/";
	else if (site == 79) return "http://it.fiat.netmining.com/";
	else if (site == 78) return "http://es.fiat.netmining.com/";
	else return null;
}

// custom function
function s3_COM_siteID() {
	var varURL = document.URL;
	var s3SiteID = 608;
	
	if (varURL.indexOf("/FIAT_GERMANY/") >=0)   s3SiteID = 76;
	else if (varURL.indexOf("/FIAT_SPAIN/") >=0)   s3SiteID = 78;
	else if (varURL.indexOf("/FIAT_FRANCE/") >=0)   s3SiteID = 77;
	else if (varURL.indexOf("/FIAT_ITALIA/") >=0)   s3SiteID = 79;
	else if (varURL.indexOf("/FIAT_UK/") >=0)   s3SiteID = 75;
	else if (varURL.indexOf("/FIAT_PORTUGAL/") >=0)   s3SiteID = 558;
	else if (varURL.indexOf("/FIAT_IRELAND/") >=0)   s3SiteID = 556;
	else if (varURL.indexOf("/FIAT_DENMARK/") >=0)   s3SiteID = 554;
	else if (varURL.indexOf("/FIAT_BELGIUM/") >=0)   s3SiteID = 553;
	else if (varURL.indexOf("/FIAT_HUNGARY/") >=0)   s3SiteID = 555;
	else s3SiteID = 484;//FIAT_COM
	
	return s3SiteID;
}
/*=================================end=================================*/