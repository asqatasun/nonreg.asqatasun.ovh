Scroll = function( target , duration )
{
	this.to = 0;
	this.from = 0;
	
	if( !duration )
		duration = Scroll.DEFAULT_DURATION;
		
	this.duration = ( duration * Scroll.INTERVAL_TIME );
	this.target = ( !target ) ? document.body : target;
}

Scroll.DEFAULT_DURATION = 1;
Scroll.INTERVAL_TIME = 10;

Scroll.prototype = {

	center: function()
	{
		this.from = this.getPosition();
		this.to = Math.round( this.getMaxScrollHeight() / 2 );
		
		this.count = 0;
		var o = this;
		clearInterval( this.interval );
		this.interval = setInterval( function() { o.update(); } , Scroll.INTERVAL_TIME );
	},
	
	reset: function()
	{
		this.to = this.from;
		this.from = this.getPosition();
		this.count = 0;
		var o = this;
		clearInterval( this.interval );
		this.interval = setInterval( function() { o.update(); } , Scroll.INTERVAL_TIME );
	},
	
	update: function()
	{
		var pos = this.ease( this.count++, this.from, this.to - this.from, this.duration );
		this.setPosition( pos );
		if( this.count > this.duration )
		{
			this.kill();
		}
	},
	
	ease: function( t , b , c , d )
	{ 
		return c * t / d + b; // Linear
	},
	
	kill: function()
	{
		clearInterval( this.interval );
		this.setPosition( this.to );
	},
	
	setPosition: function( pos )
	{
		window.scrollTo( 0 , pos );
	},
	
	getPosition: function()
	{
		if( !window.pageYOffset )
		{ // IE
			return Math.max( document.body.scrollTop , document.documentElement.scrollTop ); 
		}
		else
		{ //W3C
			return window.pageYOffset;
		}
	},
	
	getWindowHeight: function()
	{
		var windowHeight = 0;

		if( typeof( window.innerHeight ) == 'number' )
		{
			//Non-IE
			windowHeight = window.innerHeight;
		}
		else if( document.documentElement && ( document.documentElement.clientHeight ) )
		{
			//IE 6+ in 'standards compliant mode'
			windowHeight = document.documentElement.clientHeight;
		}
		else if( document.body && ( document.body.clientHeight ) )
		{
			//IE 4 compatible
			windowHeight = document.body.clientHeight;
		}

		return windowHeight;
	},
	
	getDocumentHeight: function()
	{
		return Math.round(documentHeight);
	},

	getMaxScrollHeight: function()
	{
		return this.getDocumentHeight() - this.getWindowHeight();
	}
};