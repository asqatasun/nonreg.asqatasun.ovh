/*
	ED - 12.07.2006
	Fonction AddEvent de John Resig
	http://ejohn.org/projects/flexible-javascript-events/
*/
 function addEvent( obj, type, fn ) {
   if ( obj.attachEvent ) {
     obj['e'+type+fn] = fn;
     obj[type+fn] = function(){obj['e'+type+fn]( window.event );}
     obj.attachEvent( 'on'+type, obj[type+fn] );
   } else
     obj.addEventListener( type, fn, false );
 }

 function removeEvent( obj, type, fn ) {
  if ( obj.detachEvent ) {
    obj.detachEvent( 'on'+type, obj[type+fn] );
    obj[type+fn] = null;
  } else
    obj.removeEventListener( type, fn, false );
}

/* Affichage d'un calendrier pour les formulaires avec recherche par date */
function affiche_calendrier(nom, form) {
	if (document.getElementById) {
		document.write("<a href=\"#\" onclick=\"window.open('/adminsite/calendrier/calendrierjs.html?champ=" + nom + "&form=" + form +"', 'calendrier', 'width=135, height=138, toolbar=no, location=no, status=yes, resizable=yes'); return false;\"><img src=\"/images/calendrier.png\" border=\"0\"  alt=\"choisir une date\"></a>");
	}
}

/* Activation du service selectionn� dans la liste des services */
function activerService() {

	var index = window.document.getElementById('selectservices').selectedIndex;
	if( index == 0)
		return;
	var url = window.document.getElementById('selectservices').options[index].value;

	/* Analyse target */
	var indexTarget = url.indexOf(';');
	if (indexTarget > 0) {
		var target =  url.substring(0,indexTarget);
		url = url.substring(indexTarget + 1);
		window.open(url, target);
	} else {
		window.location.href=url.substring(1);
	}
}

/* Affichage d'une image dans une popup */
function afficheImage(source) {
	// Ouverture du pop-up
	window.open(source,'pop','status=no,directories=no,toolbar=no,location=no,menubar=no,scrollbars=yes,resizable=yes');
}

function atteindreAncre(ancre) {
	if (ancre != 'null'){
		window.location.href += '#' + ancre;
	}
}

/* Fonction utilis�e dans la recherche avanc�e pour r�initialiser les formulaires */
function viderFormulaire(criteres) {
	criteres = criteres.split(";");
	var champReinit = "";
	var valeurChamp = "";

	for (var i=0; i < (criteres.length); i++) {
		champReinit = eval("document.forms['recherche_avancee']." + criteres[i].substring(0, criteres[i].indexOf("=")));
		valeurChamp = criteres[i].substring(criteres[i].indexOf("=")+1);

		if (champReinit) {

			var sType = champReinit.type;
			//bouton radio
			if (!sType) {
				for (var i = 0; i < champReinit.length; i++)
					champReinit[i].checked = false;
			}
			//checkbox
			else if(sType == 'checkbox')
				champReinit.checked = false;
			//autres
			else
			champReinit.value = valeurChamp;
		}
	}
}


/* Fonction permettant d'afficher le bouton imprimer (donc uniquement si javascript est actif) */
function afficherBoutonImprimer(intitule) {
	document.write('<span id="imprimer" onclick="window.print(); return false;">' + intitule + '</span>');
}


/* Fonction permettant d'intervertir l'id d'un �l�ment avec un autre */
function switchId(ancienIdItem, nouvelIdItem) {
	var itemSwitch = window.document.getElementById(ancienIdItem);

	if (itemSwitch != null) {
		itemSwitch.id = nouvelIdItem;
	}
}


/* met la couleur de bordure du menu principal
 * en correspondance avec celle de l'element de menu selectionne
 * dependence : Prototype librairie
 * */

/** toCamelCase(input)
 * Converts string input to a camel cased version of itself.
 * For example:
 * toCamelCase("z-index"); // returns zIndex
 * toCamelCase("border-bottom-style"); // returns borderBottomStyle.
 */
function toCamelCase(s) {
	for(var exp = toCamelCase.exp;
		exp.test(s); s = s.replace(exp, RegExp.$1.toUpperCase()) );
	return s;
}
toCamelCase.exp = /-([a-z])/;


// thanks PPK (http://www.quirksmode.org/dom/getstyles.html)
function getStyle(el,styleProp) {
  var x = document.getElementById(el);
  if (x.currentStyle){
    var y = x.currentStyle[toCamelCase(styleProp)];
  }
  else if (window.getComputedStyle)
    var y = document.defaultView.getComputedStyle(x,null).getPropertyValue(styleProp);
  return y;
}

document.observe("dom:loaded", function() {
  var selectedEmItem = $$('#menu_principal > li em')[0];
  if(selectedEmItem){
    var selectedItemId = selectedEmItem.parentNode.id;
    var color = getStyle(selectedItemId,"background-color");
    $$('h2')[0].setStyle({backgroundColor: color});
  }
});






