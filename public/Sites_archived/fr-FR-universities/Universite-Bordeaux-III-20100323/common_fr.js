// create print link in list ul id="print"
aPrint = function() {
  if(document.getElementById && document.getElementById('print')) {

    printTxt = document.createTextNode('Imprimer la page');

    printLink = document.createElement('a');
    printLink.href = 'javascript:window.print()';
    printLink.className = 'print';

    printLink.appendChild(printTxt);

		printLi = document.createElement('li');
		printLi.appendChild(printLink);
		
    listTools=document.getElementById('print');
    listTools.appendChild(printLi);
  } 
}

// Accessibility links
aFocus = function() {
  if(document.getElementById && document.getElementById("access_nav")) {
    var aEls = document.getElementById("access_nav").getElementsByTagName("A");
    for (var i=0; i<aEls.length; i++) {
      aEls[i].className="hidden";
      aEls[i].onfocus=function() {
        this.className="";
      }
    }
  }
}
// events onload
function addLoadEvent(func) {
  if (window.addEventListener) 
    window.addEventListener("load", func, false);
  else if (window.attachEvent) 
    window.attachEvent("onload", func);
}
addLoadEvent(aPrint);
addLoadEvent(aFocus);