/*showMessageChamp
effacerTextChamp*/


var timer = "";

function montre(id) {
	 cachetout();

	 if (document.getElementById) {
		 document.getElementById(id).style.display="block";
	 } else if (document.all) {
		 document.all[id].style.display="block";
	 } else if (document.layers) {
		 document.layers[id].display="block";
	 }
	 
	 clearTimeout(timer);
}

function cache(id) {
	
	if (document.getElementById) {
		document.getElementById(id).style.display="none";
	} else if (document.all) {
		document.all[id].style.display="none";
	} else if (document.layers) {
		document.layers[id].display="none";
	}
	   
	timer=setTimeout("cache(id)",3000);
	
}

function cachetout() {
	cache('smenuprincipal1');
	cache('smenuprincipal2');
	cache('smenuprincipal3');
}

/* Demande d'affichage d'une fen�tre au niveau du front office */
function ouvrirFenetrePlan(url, nom) {
   window.open(url, nom, "width=520,height=500,scrollbars=yes, status=yes");
}

/* Fonctions Eurodoc*/

var rech_suppDivShown = false;
var rech_doctDivShown = false;

/* Fonction permettant d'afficher ou masquer les criteres supplementaires */

function showOrHideCriteria(){

	var rech_suppDiv = document.getElementById('rech_supp');
	
	var linkA = document.getElementById('linkCriteriaA');
	var linkB = document.getElementById('linkCriteriaB');

	if (rech_suppDivShown){
		rech_suppDiv.style.display='none';
		linkA.style.display='inline';
		linkB.style.display='none';
		//linkA.value = "Plus de crit�res";
	}
	else{
		rech_suppDiv.style.display='block';
		linkA.style.display='none';
		linkB.style.display='inline';
		//linkA.value = "Moins de crit�res";
	}
	
	rech_suppDivShown = !rech_suppDivShown;
}

function showOrHideDoctorant(){

	var rech_etudDiv = document.getElementById('rech_etud');
	var rech_doctDiv = document.getElementById('rech_doct');	
	
	var linkA = document.getElementById('a_rech_doct');

	if (rech_doctDivShown){
		rech_etudDiv.style.display='block';	
		rech_doctDiv.style.display='none';
		linkA.firstChild.nodeValue = "Rechercher sur doctorants";
	}
	else{
		rech_etudDiv.style.display='none';	
		rech_doctDiv.style.display='block';
		linkA.firstChild.nodeValue = "Rechercher sur etudiants";
	}
	
	rech_doctDivShown = !rech_doctDivShown;
}

function showMessageFieldEurodoc(typeAide, f1, f2) {
	showMessageFieldEurodoc2(typeAide, f1, f2, '');
}

function showMessageFieldEurodoc2(typeAide, f1, f2, form) {
	numToolbox = '';
	nomForm = form;
	field1 = f1;
	field2 = f2;
	fieldRequete = '';
	texte = '';
	nomApplet = '';
	// Type d'insertion (liste)
	typeInsertion = '';

	//AM 200309 : L'arbre des structures doit prendre en compte la langue courante dans le front office

	   	var indexSlash1 = typeAide.indexOf('/');
		var lg = '';
		var filtre = '';
		if (indexSlash1 != -1)
		{
			var indexSlash2 = typeAide.indexOf('/', indexSlash1 + 1);
			if (indexSlash2 != -1)
			{
				lg = typeAide.substring(indexSlash1 + 1, indexSlash2);
				filtre = typeAide.substring(indexSlash2 + 1);
			}
			else
			{
				lg = typeAide.substring(indexSlash1 + 1);
			}
		}
//		alert("'/adminsite/menu/menu.jsp?MODE=STRUCTURE&LANGUE='+lg+'&FILTRE='+filtre, 'menu2', 'scrollbars=yes, resizable=yes, status=yes, width=600, height=400, top=320, left=320'");
//		alert("'/adminsite/menu/menu.jsp?MODE=GROUPE&CODE='+value, 'menu2', 'scrollbars=yes, resizable=yes, status=yes, width=600, height=400, top=320, left=320'");
		//sList = window.open('/adminsite/menu/menu.jsp?MODE=GROUPE&CODE='+value, 'menu2', 'scrollbars=yes, resizable=yes, status=yes, width=600, height=400, top=320, left=320');
	//sList = window.open('/adminsite/menu/menu.jsp?MODE=STRUCTURE&LANGUE='+lg+'&FILTRE='+filtre, 'menu2', 'scrollbars=yes, resizable=yes, status=yes, width=600, height=400, top=320, left=320');
//	alert('/adminsite/menu/menu_diplomes.jsp?MODE=GROUPE&CODE=ETUDIANTS_ORACLE&LANGUE='+lg+'&FILTRE='+filtre, 'menu2', 'scrollbars=yes, resizable=yes, status=yes, width=600, height=400, top=320, left=320');
	sList = window.open('/adminsite/menu/menu_diplomes.jsp?MODE=GROUPE&CODE=ETUDIANTS_ORACLE&LANGUE='+lg+'&FILTRE='+filtre, 'menu2', 'scrollbars=yes, resizable=yes, status=yes, width=600, height=400, top=320, left=320');

}