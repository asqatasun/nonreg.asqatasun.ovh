//------------------------------------------------------------------------------
// FICHIER : $RCSfile: gs_scroll.js,v $
// AUTEUR  : $Author: yvan $
// VERSION : $Revision: 1.6 $
// DATE    : $Date: 2007-09-26 13:40:10 $
//------------------------------------------------------------------------------

/* -----------------------------------------------------------------------------
* File Name: gs_scroll.js
*
* GS_Scroll Class: represents an Object to scroll html information 
* automatically.
* 
* Constructor variables :
*   - h_fen  : visible height of the window <div>  which contains 
*             the marquee <div>.
*
*   - marqId : Identifiant of the marquee div.
*   - fenId  : Identifiant of the window wich contains the information to scroll.
*   - pas    : step of the scrolling. ('0': no scrolling).
*
*   - mode   : if 'alert' display an error message if an error occurs else no 
*             is displayed.
*   - fromtop: The scrolling will begin from the top if fromtop=0
*              The scrolling will begin from 50 px from the top if fromtop=50 
*              The scrolling will begin from the bottom if fromtop='' ...
*
* ------------------------------------------------------------------------------
* How start the scrolling ?
*
* - 1) Include this file in the web page.
*
* - 2) Put two overlap <div> tags in the web page around the html information
*      wich must be scrolling with the two Id property to pass in the GS_Scroll
*      parameters.
*
* - 3) Declare a new GS_Scroll object in the html page where the information
*      to scroll must be displayed. This Object must be in an global variable
*      in the javascript code (i.e: declare out of any javascript functions).
*
* - 4) Build a new function which must :
*      2.1) Init this GS_Scroll object (call init_mrq () method);
*      2.2) lauch an timer wich call the scrollmrq () method of this object.
* 
* Example:
*   {-- JAVASCRIPT SIDE --}
*
*   <script src="gs_scroll.js" type="text/javascript"></script>
*   
*   <script type="text/javascript">
*   // begin from the top
*   var sScroll1 = new GS_Scroll ("100px", "MARQUEE", "FENETRE",    1, '',0);
*   // begin from the bottom
*   var sScroll1 = new GS_Scroll ("100px", "MARQUEE", "FENETRE",    1, '','');
*
*
*   function gsOnload () {
* 
*     sScroll1.init_mrq ();
*     setInterval ("sScroll1.scrollmrq ()", sScroll1.interval);
*   }
* 
*   window.onload=gsOnload;
*   </script>
*
*   {-- HTML SIDE --}
*
*   <div style="padding:5px 5px 5px;text-align:center">
*   <div id="FENETRE" onmouseover="sScroll1.stoc=sScroll1.pas;sScroll1.pas=0;" 
*   onmouseout="sScroll1.pas=sScroll1.stoc;">
*   <div id="MARQUEE">  
*   Code html to scrolling.... 
*   </div>
*   </div>
*   </div>
* 
* Note:
*  To load specific css syle to the two div used by the GS_Scroll Object, all 
*  you need to decalre two style wich the name is : #{divname}.
*  For example, #FENETRE { color:FFFF00 } in the previous example.
*  
*
*-------------------------------------------------------------------------------
*/
first_time  = true;
second_div_move = false;
first_div_move  = true;
last_diff        = 0;
need_second = false;
view_pas = false;
go_div = 0;
unique = true;
function GS_Scroll_double (h_fen, marqId, fenId, pas, mode,fromtop) {
  
  this.h_fen    = h_fen;
  this.marqId   = marqId;
  this.fenId    = fenId;
  this.mode     = mode;
		this.fromtop  = fromtop;
  this.stoc     = 0;
  this.pas      = pas;
  this.h_mrq    = 0;
  this.interval = 100;
  this.mrq      = null;
  this.fen      = null;
  this.error    = false;
}

//------------------------------------------------------------------------------
// Init GS_SCroll object.
//------------------------------------------------------------------------------
GS_Scroll_double.prototype.init_mrq = function () {

  this.mrq = $(this.marqId);
  this.fen = $(this.fenId);
  
  if (   (this.mrq = $(this.marqId))
      && (this.fen = $(this.fenId) )) {
    
    
  if (this.mrq.style.display == 'none') {this.mrq.style.display = 'block';}
      
    //this.fen.onmouseover  = function(){this.stoc=this.pas;this.pas=0};
    //this.fen.onmouseout   = function(){this.pas=this.stoc};
    this.fen.style.height = this.h_fen;
    
    with (this.fen.style) {
      position        = "relative";
      overflow        = "hidden";
      verticalAlign   = "top";
      height          = this.h_fen;
    } 
    
    this.h_mrq=this.mrq.offsetHeight;
    //alert(Object.inspect(this.mrq));
    //this.mrq.morph('position:absolute; top:0px;');
    //absolutize(this.h_mrq);
    //this.mrq.style.position = "absolute";
    //this.mrq.style.top = "0px";
    if(this.marqId == "MARQUEE"){
      this.mrq.setStyle({
        position: 'absolute',
        top: '0px'
      });
    }else this.mrq.setStyle({
        position: 'absolute'
      });

   /*with(this.mrq.style) {
        position  = "absolute";
        top       = "0px;"    
    }*/
    
    //setInterval ("this.scrollmrq ()", this.interval);
  
  } else {
    if (this.mode == 'alert') alert ('Error: GS_Scroll::init_mrq failed.');
    this.error = true;
  }

}

//------------------------------------------------------------------------------
// Scroll this GS_SCroll object.
//------------------------------------------------------------------------------
GS_Scroll_double.prototype.scrollmrq = function () {

  if (this.error) return;
		
  if (this.mrq) {
  
//-- les compteurs pour s'y retrouver...
//if(this.marqId == "MARQUEE") $('move').update(Math.abs(parseInt(this.mrq.style.top)));
//if(this.marqId == "MARQUEE") $('hauteur').update(parseInt(this.h_mrq));
//if(this.marqId == "MARQUEE_BIS") $('move2').update(Math.abs(parseInt(this.mrq.style.top)));
//if(this.marqId == "MARQUEE_BIS") $('hauteur2').update(parseInt(this.h_mrq));

// Si le d�filement est > au 2/3 de la hauteur de la fen�tre et que le tiers restant
// est inf�rieur � la hauteur de la fen�tre de visualisation alors on lance la fen�tre suivante

diff = Math.abs(parseInt(this.mrq.style.top)) - (this.h_mrq*2/3);

//$('diff').update(Math.abs(diff));
//alert (diff);
  // au premier passage, this.mrq.style.top doit �tre n�gatif...
  if ( parseInt(this.mrq.style.top)<0 && Math.abs(diff) < 2 && Math.abs(last_diff) != 1) {
          
        if(second_div_move == false && unique ) {
          //alert ('premier lancement du deuxi�me div '+this.marqId);
          setInterval ("sScroll_double_2.scrollmrq ()", sScroll_double_2.interval);
          unique = false;
        }
        second_div_move = true;
        if(this.marqId == "MARQUEE_BIS") {
          second_div_move = false;
          need_second=true;
        }  
            
      }
      
      if((go_div == 1 || go_div == 2) && Math.abs(diff) < 2){

        if (go_div == 2 && this.marqId == "MARQUEE") {
         //alert ('lancement du second '+this.marqId);
         //alert("hauteur MARQUEE = "+Math.abs(parseInt(this.mrq.style.top)));
         //alert("taille MARQUEE = "+Math.abs(this.h_mrq));
          go_div =0;
          sScroll_double_2.pas=sScroll_double_2.stoc;
        }
        if (go_div == 1 && this.marqId == "MARQUEE_BIS") {
          //alert ('lancement du premier '+this.marqId);
          //alert("hauteur MARQUEE_BIS = "+Math.abs(parseInt(this.mrq.style.top)));
          go_div =0;
          sScroll_double_1.pas=sScroll_double_1.stoc;
        }
        //alert('ici');
      }
    if ((styleVisibility = this.mrq.style.visibility) && (styleVisibility != 'visible')) {
      this.mrq.style.visibility = 'visible';
				
    }
    
    if ( parseInt(this.mrq.style.top) > -this.h_mrq ) {
			  if (!isNaN(this.fromtop) && this.fromtop<this.h_mrq && first_time){
				
					 this.mrq.style.top = this.fromtop+"px";
					 first_time = false;
					 
				}else this.mrq.style.top = parseInt(this.mrq.style.top)-this.pas+"px";
      
      
    
    } else {
      this.mrq.style.top=parseInt(this.h_fen)+"px";
      
      //bloque le premier div
      if(this.marqId == "MARQUEE" && second_div_move && !first_time){
        sScroll_double_1.stoc=sScroll_double_1.pas;
        sScroll_double_1.pas=0;
        second_div_move = true;
        go_div = 1;
         
      //bloque le second div
      }
      
      if(this.marqId == "MARQUEE_BIS" && second_div_move == false){
        sScroll_double_2.stoc=sScroll_double_2.pas;
        sScroll_double_2.pas=0;
        go_div = 2;
      }

    }
    this.first_time = false;
    last_diff = diff;
  } else {
    if (this.mode == 'alert') alert ('Error: GS_Scroll nit initialized.');
    this.error = true;
  }
}

//------------------------------------------------------------------------------
// $Log: gs_scroll.js,v $
// Revision 1.6  2007-09-26 13:40:10  yvan
// use the 6th parameter of the constructor "new GS_Scroll "
// in the pattern.html file if you want that scrolling begin from the top
//
// Revision 1.5  2005/07/26 17:53:16  autran
// - Correction [2] on last update on this.mrq.display=none dans initmrq
// - Update the styl.visibility of <marquee> to 'visible' in the scrollmrq function.
//
// NOTE:
// - The goal of this update it's to avoid to the 'marquee' div display without
// overflow hidden while the page is not loaded.
// - In the page which contains the marquee, the style.display of the <marquee> div
// must be equal to 'none', and visibility to 'hidden'.
//
// Revision 1.4  2005/07/26 17:20:16  autran
// - Add a test on this.mrq.display=none dans scrollmrq function.
// (allow to display the div of the marque on the first call by the timer
// of this function).
// NOTE:
// - The goal of this update it's to avoid to the 'marquee' div display without
// overflow hidden while the page is not loaded.
// - In the page which contains the marquee, the style.display of the <marquee> div
// must be equal to 'none'.
//
// Revision 1.3  2005/07/01 15:11:53  autran
// Add revision version information.
//
//-- End of source  ------------------------------------------------------------