//-- Une fois l'objet window charg�, la fonction gereDivSurgissant est appel�e
Event.observe(this,'load',gereDivSurgissant);
cpt = 0;

//-- Cache tous les div surgissants
//-- Appelle la fonction affiche_div apr�s un click sur un �l�ment de classe "lien_contact"
function gereDivSurgissant(event){
    $$('.contact_surgissant').each(function(div){
	    div.hide();
	});
    $$('.title1_contact').each(function(div){
	    Event.observe(div,'click', affiche_div);
	});
    $$('.title1_contact1').each(function(div){
	    Event.observe(div,'click', affiche_div);
	});
    $$('.lien_contact').each(function(div){
	    Event.observe(div,'click', affiche_div);
	});
}
function affiche_div(event){
    var position = 0;
    // le div ou le h1 o� l'internaute clique
    var container_div = Event.findElement(event,'div');
    // Sa position parmi les autres div cliquables
    $$('.lien_contact,.title1_contact,.title1_contact1').each(function(div){
	    //alert(Object.inspect(div));
	    if (div === container_div) position = cpt;
	    cpt++;
	});
    cpt = 0;
    $$('.contact_surgissant').each(function(div){
	    if (cpt == position) {

		div.style.position="absolute";
		//-- position de l'objet sur lequel on clique
		initial_position = Element.positionedOffset(container_div);
		div.style.left="50%";
    		div.style.top=initial_position[1]+"px";  
        
		div.show();
        
		//-- Position du div surgissant
		position_div = div.positionedOffset();
		x_img = position_div[0];
		y_img = position_div[1];
					
		//-- largeur du div surgissant :
		var dimensions = div.getDimensions();
		width_div = dimensions.width;
					
		//-- Position de l'image de fermeture
		left_position = width_div-50+"px";
					
		//-- Cr�ation  de l'image de fermeture
		var img_vide = new Element('img', { 'class': 'fermeture', src: 'Local/vide.gif', width:'50', height:'20'});
        
		//-- Fermeture du div surgissant au click sur l'image vide
		Event.observe(img_vide,'click', cacheDiv);
        
		//-- Insertion de l'image vide, positionnement et affichage
		div.insert(img_vide);
		img_vide.style.position="absolute"; 		
		img_vide.style.left=left_position;  
		img_vide.style.top="0px"; 
		img_vide.show();  
	    }
	    cpt ++;
	});
    cpt = 0;
 
}
function cacheDiv(event){
    var div_surgissant = Event.findElement(event,'div');
    div_surgissant.hide();
}

