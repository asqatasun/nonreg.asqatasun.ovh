//
// Affichage des menus
//
$(document).ready(function() {
	
	// lien pdf
	$("a[href$='pdf']").each(function(){
		$(this).attr("target", "_blank");
		//console.log($(this).attr("href"));
	});
	
	
	var local = document.location.hostname;
	var lien_http = new RegExp("^http://");
	var lien_interne = new RegExp("^http://"+local);
	
	$("a").each(function(){				
		if(lien_http.test($(this).attr("href")) && !lien_interne.test($(this).attr("href")))
		{	
			$(this).attr("target", "_blank");
			//console.log($(this).attr("href"));
		}
	});
	
	// Ouverture du menu
	$('.inactive').hover(function () {
                
		// hover	
		$(this).find("a.m_title").css('background-color','#003366');
		$(this).find("a.m_title").css('color','#ffffff');
		$(this).find("ul.n2").fadeIn();
        },
        function () {
                    // out
            $(this).find("ul.n2").toggle();
                    $(this).find("a.m_title").css('background-color','#fff');
                    $(this).find("a.m_title").css('color','#003366');
        });

        // Moteur de recherche
        $('#searchInput').focus(function () {
            if($(this).val() == 'Rechercher')
            {
                $(this).val('');
            }
        });
        $('#searchInput').blur(function () {
            if($(this).val() == '')
            {
                $(this).val('Rechercher');
            }
        })

	// diaporama photo
	$('ul#animated-portfolio').animatedinnerfade({
		speed: 1000,
		timeout: 50000,
		type: 'random',
		containerheight: '180px',
		containerwidth: '275px',
		animationSpeed: 0,
		animationtype: 'fade',
		bgFrame: 'none',
		controlBox: 'yes',
		displayTitle: 'yes',
		controlButtonsPath: 'squelettes/charte'
	});
        
		
});