   accessTo = function(form){
	var compo = form.ufr; 
	var serv =  form.services;
	var centres =  form.centres;

	if(compo[compo.selectedIndex].value != "#") {
		document.location = compo[compo.selectedIndex].value;
		return false;
	}
	if(serv[serv.selectedIndex].value != "#") {
		document.location = serv[serv.selectedIndex].value;
		return false;
	}
	if(centres[centres.selectedIndex].value != "#") {
		document.location = centres[centres.selectedIndex].value;
		return false;
	}
	return false;
}

var formfocus = function(){
	var f = document.accessto; 
	if(f != undefined){
		var it = ["services","ufr","centres"];
		for(var i in it) if(f[it[i]].name != this.name) f[it[i]].selectedIndex = 0;
	}
}
initAccessTo = function(){
	var f = document.accessto;
	if(f != undefined){
	var it = ["services","ufr","centres"];
		f.ufr.selectedIndex=0;
		f.ufr.onfocus = formfocus;
		f.services.selectedIndex=0;
		f.services.onfocus = formfocus;
		f.centres.selectedIndex=0;
		f.centres.onfocus = formfocus;
	}
}

if(document.all){
    window.attachEvent('initAccessTo', externalLinks) ;
}
