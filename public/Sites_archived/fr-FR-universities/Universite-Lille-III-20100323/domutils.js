function addEvent(obj, type, fn){
    if(obj.attachEvent){
        // pour IE
/*
        obj['e' + type + fn] = fn ;
        obj[type + fn] = function(){
            obj['e' + type + fn](
                window.event
            ) ;
        }
        obj.attachEvent('on' + type, obj[type + fn]) ;
*/
        obj.attachEvent('on' + type, fn) ;
    }else{
        // pour les vrais browsers
        obj.addEventListener(type, fn, false) ;
    }
}

function removeEvent(obj, type, fn){
    if(obj.detachEvent){
        obj.detachEvent('on' + type, obj[type + fn]) ;
        obj[type + fn] = null ;
    }else{
        obj.removeEventListener(type, fn, false) ;
    }
}

function cancelDefaultEvent(e){
    if(e.preventDefault){
        e.preventDefault() ;
    }else{
        e.returnValue = false ;
    }
}

function $(el){
    return document.getElementById(el) ;
}
        
function $t(el, tag){
    return el.getElementsByTagName(tag) ;
}

function $c(oElm, sTagName, sClassName) {
    if(oElm == undefined){
        oElm = document ;
    }

    if(sTagName == "*" && oElm.all){
        var aElements = oElm.all ;
    }else{
        var aElements = oElm.getElementsByTagName(sTagName) ;
    }

    var aReturnElements = [] ;
    var sClassName = sClassName.replace(/\-/g, "\\-") ;
    var oRegExp = new RegExp("(^|\\s)" + sClassName + "(\\s|$)") ;
    var oElement ;

    for(var i = 0; i < aElements.length; i++){
        oElement = aElements[i] ;
        if(oRegExp.test(oElement.className)){
            aReturnElements.push(oElement) ;
        }
    }
    return aReturnElements ;
}


function sendRequest(url,callback,postData) {
    var req = createXMLHTTPObject() ;
    if (!req) return ;
    var method = (postData) ? "POST" : "GET" ;
    req.open(method, url, true) ;
    req.setRequestHeader('User-Agent','XMLHTTP/1.0') ;
    if(postData){
        req.setRequestHeader('Content-type','application/x-www-form-urlencoded') ;
    }

    req.onreadystatechange = function (){
        if(req.readyState != 4) return ;
        if(req.status != 200 && req.status != 304){
            return ;
        }
        callback(req) ;
    }

    if(req.readyState == 4){
        return ;
    }

    req.send(postData) ;
}

var XMLHttpFactories = [
    function(){ return new XMLHttpRequest() },
    function(){ return new ActiveXObject("Msxml2.XMLHTTP") },
    function(){ return new ActiveXObject("Msxml3.XMLHTTP") },
    function(){ return new ActiveXObject("Microsoft.XMLHTTP") }
] ;

function createXMLHTTPObject() {
    var xmlhttp = false ;
    for(var i = 0; i < XMLHttpFactories.length; i++){
        try{
            xmlhttp = XMLHttpFactories[i]() ;
        }
        catch(e){
            continue ;
        }
        break;
    }
    return xmlhttp ;
}

