var par = window.parent.invisibleFrame;
var numToolbox = "0";
var field1 = "";
var field2 = "";
var fieldRequete = "";
var texte = "";
var nomApplet = "";
var nomForm = "";
var pageTete = false;
//AM200501 : parcours LMD
var ajoutAuto="", modifAuto="";

// JSS 20020612-001 Type d'insertion (liste)
var typeInsertion="";

/* Demande d'affichage d'une fen�tre par un applet */
function showMessage(s) {
	
   numToolbox = s;
   field1 = "";
   field2 = "";
   fieldRequete = "";
   texte = "";
   nomApplet = "";
// JSS 20020612-001 Type d'insertion (liste)
   typeInsertion="";
   sList = window.open("/adminsite/menu/menu.jsp", "list", "width=200,height=210, status=yes");
}

/* ouverture d'une fen�tre centr�e */
function ouvrirPhoto(urlPhoto,largeur,hauteur) {
  x = (screen.availWidth - largeur)/2;
  y = (screen.availHeight-30 - hauteur)/2;
  attrs = "height="+ hauteur +", width="+ largeur +", left="+ x +", top="+ y;
  fenetre = window.open(urlPhoto,'photo',attrs);
}

/* Demande d'affichage d'une fen�tre au niveau du front office */
function ouvrirFenetrePlan(url, nom) {
   window.open(url, nom, "width=520,height=500,scrollbars=yes, status=yes, resizable=1");
}

/* Affichage de la phototh�que */
function showPhototheque( ) {
   window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_PHOTO&ACTION=RECHERCHER&MODE_PHOTOTHEQUE=ADMINISTRATION", "photo", "width=650,height=535,top=20,left=100,scrollbars=yes, status=yes");
}


/* Affichage de la documenth�que */
function showDocumentheque() {
   window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_DOCUMENT&ACTION=RECHERCHER&MODE_DOCUMENTHEQUE=ADMINISTRATION", "document", "width=650,height=350,top=20,left=100,scrollbars=yes, status=yes");
}


function showMessageField( typeAide, f1, f2)	{
	showMessageField2( typeAide, f1, f2, "");
}

/* Demande d'affichage d'une fen�tre par un champ */
function showMessageField2( typeAide, f1, f2, form) {
   numToolbox = "";
   nomForm = form;
   field1 = f1;
   field2 = f2;
   fieldRequete = "";
   texte = "";
   nomApplet = "";
   // JSS 20020612-001 Type d'insertion (liste)
	 typeInsertion="";

  //AM 200309 : L'arbre des structures doit prendre en compte la langue courante dans le front office
   if( typeAide == 'structure' || typeAide.indexOf('structure') != -1) {
		 if(typeAide == 'structure') {
			 sList = window.open('/adminsite/menu/menu.jsp?MODE=STRUCTURE', 'menu2', 'scrollbars=yes,width=350,height=210,top=320,left=320, status=yes');
		 }else{
			 var lg = typeAide.substring(typeAide.indexOf('structure')+9,typeAide.length);
			 if(lg != null && lg.length>0)
				 sList = window.open('/adminsite/menu/menu.jsp?MODE=STRUCTURE&LANGUE='+lg, 'menu2', 'scrollbars=yes,width=350,height=210,top=320,left=320, status=yes');
			 else
				 sList = window.open('/adminsite/menu/menu.jsp?MODE=STRUCTURE&LANGUE=', 'menu2', 'scrollbars=yes,width=350,height=210,top=320,left=320, status=yes');
		 }
	 }

	//JSS 20020606-005
	if( typeAide == 'rubrique' || typeAide.indexOf('rubrique') != -1) {
		if(typeAide == 'rubrique') {
			 sList = window.open('/adminsite/menu/menu.jsp?MODE=RUBRIQUE', 'menu2', 'scrollbars=yes,width=450,height=210,top=320,left=320, status=yes');
		 }else{
			 var lg = typeAide.substring(typeAide.indexOf('rubrique')+8,typeAide.length);
			 if(lg != null && lg.length>0)
				 sList = window.open('/adminsite/menu/menu.jsp?MODE=RUBRIQUE&LANGUE='+lg, 'menu2', 'scrollbars=yes,width=450,height=210,top=320,left=320, status=yes');
			 else
				 sList = window.open('/adminsite/menu/menu.jsp?MODE=RUBRIQUE', 'menu2', 'scrollbars=yes,width=450,height=210,top=320,left=320, status=yes');
		 }
	}
	//JSS 20040419 : affichage arbre rubrique en fonction du p�rim�tre (back-office)
	if( typeAide.indexOf('rubbo/') != -1)	{
		// formatt� comme suit rubbo/type/objet/action

		var indexSlash1 = typeAide.indexOf('/');
		var indexSlash2 = typeAide.indexOf('/', indexSlash1 + 1);
		var indexSlash3 = typeAide.indexOf('/', indexSlash2 + 1);

		var permission = typeAide.substring(indexSlash1+1,typeAide.length);
		sList = window.open('/adminsite/menu/menu.jsp?MODE=RUBRIQUE&PERMISSION='+permission, 'menu2', 'scrollbars=yes,width=450,height=210,top=320,left=320, status=yes');
	}

		//JSS 20040419 : affichage arbre structure en fonction du p�rim�tre (back-office)
	if( typeAide.indexOf('strbo/') != -1)	{

		// formatt� comme suit strbo/type/objet/action/langue
		var indexSlash1 = typeAide.indexOf('/');
		var indexSlash2 = typeAide.indexOf('/', indexSlash1 + 1);
		var indexSlash3 = typeAide.indexOf('/', indexSlash2 + 1);
		var indexSlash4 = typeAide.indexOf('/', indexSlash3 + 1);

		var permission = typeAide.substring(indexSlash1+1,indexSlash4);
		var lg = typeAide.substring(indexSlash4+1,typeAide.length);

		sList = window.open('/adminsite/menu/menu.jsp?MODE=STRUCTURE&PERMISSION='+permission+'&LANGUE='+lg, 'menu2', 'scrollbars=yes,width=350,height=210,top=320,left=320, status=yes');
	}

   if( typeAide == 'annuaire')
      sList = window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_ANNUAIRE&ACTION=RECHERCHER&TOOLBOX=LIEN_INTERNE&LANGUE_ARTICLE=-1", "annuaire2", "width=500,height=330,top=210,left=310,scrollbars=yes, status=yes");

   if( typeAide.indexOf('fichefil') != -1) {
		 	var proc = typeAide.substring(typeAide.indexOf('fichefil')+8,typeAide.length);
			proc=proc.toUpperCase();
      sList = window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC="+proc+"&ACTION=RECHERCHER&TOOLBOX=LIEN_INTERNE", "fichefil", "width=500,height=330,top=210,left=310,scrollbars=yes, status=yes");
	 }
   /* JSS 20020530-001 */
   if( typeAide == 'pagelibre')
      sList = window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_PAGELIBRE&ACTION=RECHERCHER&TOOLBOX=LIEN_INTERNE&LANGUE_ARTICLE=0", "annuaire2", "width=500,height=330,top=210,left=310,scrollbars=yes, status=yes");
		
	 /* AM 200309 cr�ation de page libre*/
	 if( typeAide == ('pagelibre_creation')){
		 field2="LIBELLE_CODE_PAGE_TETE";
		 sList = window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=TRAITEMENT_PAGELIBRE&ACTION=AJOUTER&LANGUE=0&CODE_RUBRIQUE="+f2, "pagelibre_creation", "width=500,height=330,top=210,left=310,scrollbars=yes, status=yes");
	 }
   /* FIN JSS 20020530-001 */

   /* JSS 20021226 */
   // JSS 20040419 : arbre des groupes
   if( typeAide == 'groupe_dsi')
		 sList = window.open('/adminsite/menu/menu.jsp?MODE=GROUPE', 'menu2', 'scrollbars=yes,width=350,height=210,top=320,left=320, status=yes');
	//JSS 20040419 : affichage arbre groupe en fonction du p�rim�tre (back-office)
	if( typeAide.indexOf('groupebo/') != -1)	{
		// formatt� comme suit groupebo/type/objet/action

		var indexSlash1 = typeAide.indexOf('/');
		var indexSlash2 = typeAide.indexOf('/', indexSlash1 + 1);
		var indexSlash3 = typeAide.indexOf('/', indexSlash2 + 1);

		var permission = typeAide.substring(indexSlash1+1,typeAide.length);
		sList = window.open('/adminsite/menu/menu.jsp?MODE=GROUPE&PERMISSION='+permission, 'menu2', 'scrollbars=yes,width=350,height=210,top=320,left=320, status=yes');
	}



      //sList = window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=RECHERCHE_GROUPE_DSI&ACTION=RECHERCHER_TYPE", "groupe_dsi", "width=480,height=300,top=220,left=300,scrollbars=yes, status=yes");
  /* FIN JSS 20021226-001 */

  /* JSS 20030110 */
   if( typeAide == 'public_vise_dsi')
      sList = window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=RECHERCHE_PUBLIC_VISE_DSI&ACTION=RECHERCHER", "public_vise_dsi", "width=480,height=300,top=210,left=290,scrollbars=yes, status=yes");

   // JSS 20040419 : arbre des groupes
	if( typeAide.indexOf('publicbo/') != -1)	{
		// formatt� comme suit publicbo/type/objet/action

		var indexSlash1 = typeAide.indexOf('/');
		var indexSlash2 = typeAide.indexOf('/', indexSlash1 + 1);
		var indexSlash3 = typeAide.indexOf('/', indexSlash2 + 1);

		var permission = typeAide.substring(indexSlash1+1,typeAide.length);
        sList = window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=RECHERCHE_PUBLIC_VISE_DSI&ACTION=RECHERCHER&PERMISSION="+permission, "public_vise_dsi", "width=480,height=300,top=210,left=290,scrollbars=yes, status=yes");
	}



  /* FIN JSS 20030110 */

   if( typeAide == 'utilisateur')
      sList = window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=RECHERCHE_UTILISATEUR&ACTION=RECHERCHER", "annuaire2", "width=400,height=300,top=210,left=290,scrollbars=yes, status=yes");

   if(typeAide == 'formation')
     sList = window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_FORMATION&ACTION=RECHERCHER&TOOLBOX=LIEN_INTERNE&LANGUE_ARTICLE=-1", "formation", "width=500,height=330,top=150,left=150,scrollbars=yes, status=yes");
   if(typeAide == 'ueup')
     sList = window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_UEUP&ACTION=RECHERCHER&TOOLBOX=LIEN_INTERNE&LANGUE_ARTICLE=-1", "ueup", "width=500,height=330,top=150,left=150,scrollbars=yes, status=yes");

	 //AM 200501 : parcours LMD
   if(typeAide == 'parcours')
     sList = window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_UEUP&ACTION=RECHERCHER&TOOLBOX=LIEN_INTERNE&TYPE=PARCOURS&LANGUE_ARTICLE=-1", "parcours", "width=500,height=330,top=150,left=150,scrollbars=yes, status=yes");
   if(typeAide.indexOf('parcoursM') != -1){
   	 var codeUe = typeAide.substring(9);
     sList = window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=TRAITEMENT_PARCOURS&ACTION=MODIFIER&CODE_UEUP_PARCOURS="+codeUe+"&TOOLBOX=LIEN_INTERNE&LANGUE_ARTICLE=-1", "parcours", "width=500,height=330,top=150,left=150,scrollbars=yes, status=yes");
	}
	
   /* JSs 20030325 */   
   if(typeAide == 'cours')
     sList = window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_COURS&ACTION=RECHERCHER&TOOLBOX=LIEN_INTERNE&LANGUE_ARTICLE=-1", "ueup", "width=500,height=330,top=150,left=150,scrollbars=yes, status=yes");

  if( typeAide == 'image')
       sList = window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_PHOTO&ACTION=RECHERCHER&MODE_PHOTOTHEQUE=SELECTION", "photo", "width=650,height=535,top=20,left=100,scrollbars=yes, status=yes");
		 
  if( typeAide == 'document')
      sList = window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=TRAITEMENT_DOCUMENT&ACTION=INSERER", "document", "width=400, height=200, top=20, left=100, scrollbars=yes, status=yes, status=yes");
	
  if( typeAide == 'documentfront')
      sList = window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=TRAITEMENT_DOCUMENT_FRONT&ACTION=INSERER&SAISIE_FRONT=true", "document", "width=400, height=130, top=20, left=100, scrollbars=yes, status=yes");

   if(typeAide == 'evenements')
     sList = window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_EVENEMENTS&ACTION=RECHERCHER&TOOLBOX=LIEN_INTERNE&LANGUE_ARTICLE=-1", "evenements", "width=350,height=300,top=150,left=150,scrollbars=yes, status=yes");

   //AM 2003 04 23
   if(typeAide == 'actualites')
     sList = window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_ACTUALITE&ACTION=RECHERCHER&TOOLBOX=LIEN_INTERNE&LANGUE_ARTICLE=-1", "actualites", "width=400,height=380,top=150,left=150,scrollbars=yes, status=yes");
 
   if(typeAide == 'sites')
     sList = window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_SITESLIEUX&ACTION=RECHERCHER&TOOLBOX=LIEN_INTERNE&LANGUE_ARTICLE=-1", "sites", "width=350,height=300,top=150,left=150,scrollbars=yes, status=yes");

    if( typeAide == 'pagetete') {
      sList = window.open("/adminsite/toolbox/choix_objet.jsp?TOOLBOX=RUBRIQUE_EXTERNE", "list", "width=500,height=330,top=100,left=100,scrollbars=yes,status=yes");
      pageTete = true;
    }
		
    if( typeAide == 'commentaire') {
    	sList = window.open("/adminsite/toolbox/choix_objet.jsp?TOOLBOX=COMMENTAIRE", "list", "width=500,height=330,top=100,left=100,scrollbars=yes,status=yes");
	pageTete = true;
    }
		
   if( typeAide == 'requete')	{
	   field1 = "";
		field2 = f2;
	nomForm = "";
	   fieldRequete = f1;

		if(field2 == 'STATS') {
			field2 = "";
			sList = window.open("/adminsite/toolbox/choix_objet.jsp?TOOLBOX=LIEN_REQUETE&RESTRICTION=STATS", "list", "width=500,height=330,top=100,left=100,scrollbars=yes,status=yes");
		}
		else {
			field2 = "";
       sList = window.open("/adminsite/toolbox/choix_objet.jsp?TOOLBOX=LIEN_REQUETE&RESTRICTION=XML", "list", "width=500,height=330,top=100,left=100,scrollbars=yes,status=yes");
		}
   }

}

/* Demande d'affichage d'une fen�tre par un champ */

function showMessageChamp( typeAide, f1, f2, nomFormulaire) {

	 showMessageField2( typeAide, f1, f2, nomFormulaire);
	 
}


/* Demande d'affichage d'une fen�tre par un champ */

function ouvrirFenetreRechercheParProcessus( processus, f1, f2) {
   numToolbox = "";
   field1 = f1;
   field2 = f2;
   nomForm = "";
   fieldRequete = "";
   texte = "";
   nomApplet = "";
   // JSS 20020612-001 Type d'insertion (liste)
	typeInsertion="";

	sList = window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC="+processus+"&ACTION=RECHERCHER&TOOLBOX=LIEN_INTERNE&LANGUE_ARTICLE=-1", "Recherche", "width=500,height=330,top=150,left=150,scrollbars=yes");
}


/* Demande d'affichage d'une fen�tre par un textarea */
function showMessageTextArea( typeAide, langue, monTexte) {
	
   numToolbox = "";
   nomApplet = "";
   // JSS 20020612-001 Type d'insertion (liste)
   typeInsertion="";
   texte = monTexte;
   field1 = "";
   field2 = "";
   nomForm = "";
   fieldRequete = "";

   // JSS 20020612-001 Type d'insertion (liste)
   if( typeAide == 'liste')	{
	    sList =  window.open("/adminsite/toolbox/choix_objet.jsp?TOOLBOX=LIEN_REQUETE&LISTE_INCLUSE=1", "list", "width=500,height=330,top=100,left=100,scrollbars=yes,status=yes");
		typeInsertion = "liste";
   }

   if( typeAide == 'lien')
       sList = window.open("/adminsite/toolbox/choix_lien.jsp?LANGUE_ARTICLE="+langue, "list", "width=500,height=330,top=100,left=100,scrollbars=yes,status=yes");
   if( typeAide == 'mailto')
       sList = window.open("/adminsite/toolbox/mailto.jsp?LANGUE_ARTICLE="+langue, "list", "width=500,height=330,top=100,left=100,scrollbars=yes,status=yes");
   if( typeAide == 'structure')
       sList = window.open("/adminsite/menu/menu.jsp", "menu2", "width=350,height=210,top=320,left=320,scrollbars=yes");
   if( typeAide == 'image')
       sList = window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_PHOTO&ACTION=RECHERCHER&MODE_PHOTOTHEQUE=INSERTION", "photo", "width=650,height=535,top=20,left=100,scrollbars=yes");
   if( typeAide == 'document') 
       sList = window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_DOCUMENT&ACTION=RECHERCHER&MODE_DOCUMENTHEQUE=INSERTION", "document", "width=650,height=535,top=20,left=100,scrollbars=yes");
   
}

/* Traitement des toolboxs */
function donneesResultats(s) {
   alert(s);
}

/* Demande de lecture des toolboxs */
function flush( ) {
	window.parent.invisibleFrame.location = "/adminsite/toolbox/invisiblepage2.html?flush=yes";
}



function inBackOffice( ) {

	for( i = 0; i < window.document.forms.length; i++)	{
		if (( window.document.forms[i].name == 'RECHERCHE_WEB' || window.document.forms[i].name == nomForm )
			&& window.document.forms[i].name != "")
			return false;
	}
	return true;
}


// JSS 20020612-001 Type d'insertion (liste)
/* Remplacement des & par des # (plantage NetWord) */
function conversionRequete( objet,  code)	{

	do	{
	  i =  code.indexOf('&');
	  if( i != -1)	{
		code = code.substring(0,i) + "#" + code.substring(i+1, code.length);
	  }
	}
	while( i != -1);

	param = "objet="+objet;
	if( code.length > 0)
	   param = param + "#" + code;

	return param;


}

/* Effacement du libell� d'un champ de recherche */
function effacerTextField( zoneSaisie, zoneLibelle, value, libelle)	{

	if( inBackOffice( ) )	{
		/* Cas de l'administration */
		eval( "window.document.forms[0]."+zoneSaisie+".value = value;");
		eval( "window.document.forms[0]."+zoneLibelle+".value = libelle;");
	} else	{
		eval( "window.document.RECHERCHE_WEB."+zoneSaisie+".value = value;");
		eval( "window.document.RECHERCHE_WEB."+zoneLibelle+".value = libelle;");
	}
}

/* Effacement du libell� d'un champ de recherche */
function effacerTextChamp( zoneSaisie, zoneLibelle, value, libelle, nomForm)	{
		eval( "window.document."+nomForm+"."+zoneSaisie+".value = value;");
		eval( "window.document."+nomForm+"."+zoneLibelle+".value = libelle;");
}


/* Renvoyer des valeurs � la fen�tre fille */
function renvoyerNewLien(nouveauLien)	{

	window.opener.modifieLien(nouveauLien);
	window.close();

}

/* Renvoyer des valeurs � la fen�tre fille */
function renvoyerValeurs( objet, code,libelle)	{

	if(window.opener && !window.opener.closed && window.opener.liendanskt == true)
		window.opener.modifieLien(objet, code, libelle);
	else if (window.opener && !window.opener.closed && (window.opener.field1 != "" || window.opener.field2 != "" || window.opener.fieldRequete !="") )
		window.opener.saveField(objet, code,libelle);
	else if (window.opener && !window.opener.closed)
		window.opener.save(objet, code,libelle);
 /*if (window.opener && !window.opener.closed)
	window.opener.save(objet, code,libelle);
	*/
	window.close();

}

/* Traitement des donn�es r�sultats renvoy�es par la fen�tre m�re */
function saveField( objet,code, libelle) {
	
	/* Cas de l'administration */
	if( inBackOffice( ) )	{
		if(!pageTete){
				/* Cas de l'administration */
				if (field1 != "")
					eval( "window.document.forms[0]."+field1+".value = code;");
				if (field2 != "")
					eval( "window.document.forms[0]."+field2+".value = libelle;");
				
				//AM 200501: parcours LMD
				if(ajoutAuto != ""){
					eval("ajouter"+ajoutAuto+"();");
					ajoutAuto = "";
				}
				//AM 200501: parcours LMD
				if(modifAuto != ""){
					eval("validmodif"+modifAuto+"();");
					modifAuto = "";
				}
				
			}else{
				/* Cas d'une page de tete : on ajoute le type d'objet au code*/
				if (field1 != "")
					eval( "window.document.forms[0]."+field1+".value = code+',TYPE='+objet;");
				if (field2 != "")
					eval( "window.document.forms[0]."+field2+".value = objet + ' : ' +libelle;");
			}

	} else	{
		/* Cas particulier du WEB */
			var formCourant = "RECHERCHE_WEB";
			if( nomForm.length > 0)
				formCourant = nomForm;

		if (field1 != "")
			eval( "window.document."+formCourant+"."+field1+".value = code;");
		if (field2 != "")
			eval( "window.document."+formCourant+"."+field2+".value = libelle;");
	}
	
	if( fieldRequete != "") {
		chaine = "\"OBJET="+objet;
		if( code.length > 0)
		{
			chaine = chaine +"&"+code;
		}
		chaine = chaine +"\"";

		eval( "window.document.forms[0]."+fieldRequete+".value = "+ chaine );
	}

  field1 = "";
  field2 = "";

  nomForm = "";

  fieldRequete = "";

  numToolbox = "";

  texte = "";

  nomApplet = "";
  
  // JSS 20020612-001 Type d'insertion (liste)
  typeInsertion="";	
	
}


/* Renvoyer des valeurs � la fen�tre fille */
function renvoyerImage( id, height, width, alt, border, hspace, vspace, align)	{
	
	if (window.opener && !window.opener.closed && window.opener.nomApplet != ""){
	
	if (window.opener && !window.opener.closed && window.opener.imgdanskt)
			window.opener.modifieImage( id, height, width, alt, border, hspace, vspace, align);
	if (window.opener && !window.opener.closed)
			window.opener.saveImage( id, height, width,alt, border, hspace, vspace, align);
	
	} else if(window.opener && !window.opener.closed)
			window.opener.saveFormImage( id, height, width, alt, border, hspace, vspace, align);
	
	window.close();
}

function renvoyerFormulaire(code,style)	{
	
	if (window.opener && !window.opener.closed && window.opener.nomApplet != "")
	{
		window.opener.saveFormulaire(code,style);
	}	
	window.close();
}

function renvoyerPageLibre(code, titre, langue) {
	if (window.opener && !window.opener.closed)
		window.opener.savePageLibre(code, titre, langue);
	window.close();
}

function renvoyerDocument(id, titre, NOM_FICHIER_JOINT, POIDS_FICHIER_JOINT, FORMAT_FICHIER_JOINT, PATH_FICHIER_JOINT, NOMFORM) {
	if(window.opener && !window.opener.closed && window.opener.liendanskt == true)
		window.opener.modifieLien("", id, "");
	else if (window.opener && !window.opener.closed && window.opener.nomApplet != "")
		window.opener.saveDocument(id, titre, NOM_FICHIER_JOINT, POIDS_FICHIER_JOINT, FORMAT_FICHIER_JOINT, PATH_FICHIER_JOINT, NOMFORM);
	else if(window.opener && !window.opener.closed)
		window.opener.saveFormDocument(id, titre, NOM_FICHIER_JOINT, POIDS_FICHIER_JOINT, FORMAT_FICHIER_JOINT, PATH_FICHIER_JOINT, NOMFORM);
	window.close();
}



function saveFormDocument(id, titre, NOM_FICHIER_JOINT, POIDS_FICHIER_JOINT, FORMAT_FICHIER_JOINT, PATH_FICHIER_JOINT, NOMFORM)	{
	var nomForm = '0';
	if (NOMFORM)
		nomForm=NOMFORM;

	if (field1 != "") {
  		eval( "window.document.forms['"+nomForm+"']."+field1+".value = id;");
  		eval( "window.document.forms['"+nomForm+"']."+field2+".value = titre;");
  		eval( "window.document.forms['"+nomForm+"'].NOM_"+field1+".value = NOM_FICHIER_JOINT;");
  		eval( "window.document.forms['"+nomForm+"'].POIDS_"+field1+".value = POIDS_FICHIER_JOINT;");
  		eval( "window.document.forms['"+nomForm+"'].FORMAT_"+field1+".value = FORMAT_FICHIER_JOINT;");
  		eval( "window.document.forms['"+nomForm+"'].PATH_"+field1+".value = PATH_FICHIER_JOINT;");
	}

	if( texte != "") {
		var codeHtml = '<A';

		codeHtml += " HREF=\"[id-document];"+id+"[/id-document]\" >";
		codeHtml += titre+"</A>";

		insererTexte( texte, codeHtml);
	}

	field1 = "";
	field2 = "";
	nomForm = "";
	fieldRequete = "";
	numToolbox = "";
	texte = "";
	nomApplet = "";

}


/* Traitement des donn�es r�sultats renvoy�es par la fen�tre m�re */
function saveFormImage( id, height, width, alt, border, hspace, vspace, align)	{
	
	if (field1 != "") {
		eval( "window.document.forms[0]."+field1+".value = id;");
		eval( "window.document.forms[0]."+field2+".value = alt;");
	}

	if( texte != "") {
		var codeHtml = '<img ';
	 
		codeHtml += " alt=\""+alt+"\"";
		codeHtml += " title=\""+alt+"\"";
		codeHtml += " border=\""+border+"\"";
		codeHtml += " width=\""+width+"\"";
		codeHtml += " height=\""+height+"\"";
		codeHtml += " hspace=\""+hspace+"\"";
		codeHtml += " vspace=\""+vspace+"\"";
		codeHtml += " align=\""+align+"\"";
		codeHtml += " src=\"[id-image]"+id+"[/id-image]\" >";

		insererTexte( texte, codeHtml);
	}

	field1 = "";
	field2 = "";
	nomForm = "";
	fieldRequete = "";
	numToolbox = "";
	texte = "";
	nomApplet = "";

}

/* Ouverture d'une nouvelle fen�tre fille */
function ouvrir_fen (url) {

	Xmas95 =new Date();
	secs = Xmas95.getSeconds();
	var name = "win" + secs;

	window.open(url,name,'status=yes,toolbar=no,scrollbars=yes,width=600,height=400');
}

function ouvrir_fen_web (url) {

	Xmas95 =new Date();
	secs = Xmas95.getSeconds();
	var name = "win" + secs;

	window.open(url,name,'status=yes,menubar=yes, toolbar=yes, resizable=yes, scrollbars=yes, width=600, height=400');
}

function ouvrir_fen_x_y (url, x, y) {

	Xmas95 =new Date();
	secs = Xmas95.getSeconds();
	var name = "win" + secs;

	window.open(url,name,'status=yes,toolbar=no,scrollbars=yes,width='+x+',height='+y);
}

function ouvrir_fen_w_h_name(url, width, height, name){
	window.open(url,name,'status=yes,toolbar=no,scrollbars=yes,width='+width+',height='+height);
}


 /* Nettoyage du code HTML avant de l'envoyer dans l'url */
function nettoyerCodeHTML ( s) {

	/* Suppression des caracteres dont code > 255 : fait planter url 
	   (peuvent etre ins�r�s par coller)
	*/
	s2 = s;
	for (i=0; i<s2.length;i++){
		var charCode = s2.charCodeAt ( i);
		if ( charCode > 255)	{

			// Traitement sp�cial pour caract�re 8217
			if( charCode == 8217)
				s2 = s2.substring( 0, i) + "'" + s2.substring( i+1, s2.length);
			else if( charCode == 8364)
				s2 = s2.substring( 0, i) + "&euro;" + s2.substring( i+1, s2.length);
			else if( charCode == 8211)
				s2 = s2.substring( 0, i) + "-" + s2.substring( i+1, s2.length);
			else if( charCode == 8230)
				s2 = s2.substring( 0, i) + "..." + s2.substring( i+1, s2.length);			
			else if( charCode == 339) 
				s2 = s2.substring( 0, i) + "&oelig;" + s2.substring( i+1, s2.length);
			else
				s2 = s2.substring( 0, i) + s2.substring( i+1, s2.length);

			//JSS 20020923-001
			i=i-1;
		}
	}
	return s2;
}

function soumettreSaisieFormulaire(code,numeroAction) {
	var forms = window.document.forms;
	for(i=0;i<forms.length;i++)
	{
		if(forms[i].name == code)
		{
			forms[i].CODE.value=code;
			forms[i].NUMERO_ACTION.value=numeroAction;
			forms[i].submit();
		}
	}
}

// RP 20041611 ajout fonctions collaboratif 

/* Gestion des fichiers joints */

var arrayFichiergw = new Array(); 
var fichierJointUnique = null;

// constructeur de la classe
function Fichiergw(p1, p2, p3, p4, p5)
{
	this.id = p1;
	this.nom = p2;
	this.format = p3;
	this.version = p4;
	this.date = p5;
}

// ajout d'un fichier
// appel du processus TRAITEMENT_FICHIERGW_FRONT
function ajouterFichiergw(indice)
{
	if (indice){
		eval( "window.document.forms[0].NO_FICHIER_JOINT.value = "+indice+"");
	}
	else{
		indice = window.document.forms[indiceForm].NB_FICHIER_JOINT.value;
		eval('window.document.forms[indiceForm].NO_FICHIER_JOINT.value = '+indice+'');
	}
	if (modeFichier == 'version'){
		window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=TRAITEMENT_FICHIERGW_FRONT&ACTION=INSERER&SAISIE_FRONT=true&NO_FICHIER_JOINT="+indice+"&MODE="+modeFichier+"", "fichiergw", "width=400, height=330,  status=no");
	}
	else{
		window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=TRAITEMENT_FICHIERGW_FRONT&ACTION=INSERER&SAISIE_FRONT=true&NO_FICHIER_JOINT="+indice+"&MODE="+modeFichier+"", "fichiergw", "width=400, height=200,  status=no");
	}
}

// suppression d'un fichier uniquement javascript
// traitement physique � la validation
function supprimerFichiergw(indice){
	reponse=confirm("Supprimer d�finitivement le fichier?");
	if (reponse){
		arrayFichiergw[indice] = null;
		preparerFichiergw();
	}
}

// renvoi du fichier � la fin du processus TRAITEMENT_FICHIERGW_FRONT
function renvoyerFichiergw(ID_FICHIER_JOINT, NOM_FICHIER_JOINT, FORMAT_FICHIER_JOINT, VERSION_FICHIER_JOINT, DATE_FICHIER_JOINT, NO_FICHIER_JOINT,MODE) {
	window.opener.saveFormFichiergw(ID_FICHIER_JOINT, NOM_FICHIER_JOINT, FORMAT_FICHIER_JOINT, VERSION_FICHIER_JOINT, DATE_FICHIER_JOINT, NO_FICHIER_JOINT,MODE);
	window.close();
}

function saveFormFichiergw(ID_FICHIER_JOINT, NOM_FICHIER_JOINT, FORMAT_FICHIER_JOINT, VERSION_FICHIER_JOINT, DATE_FICHIER_JOINT, NO_FICHIER_JOINT,MODE)	{
		var fic = new Fichiergw(ID_FICHIER_JOINT,NOM_FICHIER_JOINT,FORMAT_FICHIER_JOINT,VERSION_FICHIER_JOINT,DATE_FICHIER_JOINT);
		if (MODE == "simple"){
			fichierJointUnique = fic;
			preparerFichiergwUnique();
		}
		else{
			if (album && fic.format.indexOf("image")==-1){		
				alert("Ce fichier n'est pas une image !");
			}else {
			arrayFichiergw[NO_FICHIER_JOINT]=fic;
			preparerFichiergw();
			if( window.document.forms[indiceForm].NB_FICHIER_JOINT){
				t = parseInt(window.document.forms[indiceForm].NB_FICHIER_JOINT.value)+1;
				eval('window.document.forms[indiceForm].NB_FICHIER_JOINT.value = '+t+'');
			
				}
			}
		}
}

// pr�paration pr�-validation
// concat�nation du tableau de fichiers pour sauvegarde
function preparerFichiergw(){
	// traitement normal
	if (erreur.length==0){
		var temp = '';
		for (i=0;i<arrayFichiergw.length;i++){	
			if(arrayFichiergw[i]){
				if (temp.length>0){
					temp += "|";
				}
				temp += arrayFichiergw[i].id+";"+arrayFichiergw[i].nom+";"+arrayFichiergw[i].format+";"+arrayFichiergw[i].version+";"+arrayFichiergw[i].date;
			}
		}
		window.document.forms[indiceForm].TOTAL_FICHIER_JOINT.value = temp;
	}
	// restauration de la saisie en cas d'erreur
	else{
		arrayFichiergw = new Array();
		chaineFichier = window.document.forms[indiceForm].TOTAL_FICHIER_JOINT.value;
		if (chaineFichier.length>0){
			tabFichiers = chaineFichier.split("|");
			for (j=0;j<tabFichiers.length;j++){
				fic = tabFichiers[j].split(";");
				arrayFichiergw[j] = new Fichiergw(fic[0],fic[1],fic[2],fic[3],fic[4]);
			}
			// On r�initialise le compteur pour les listes
			if (window.document.forms[indiceForm].NB_FICHIER_JOINT){
				window.document.forms[indiceForm].NB_FICHIER_JOINT.value = j+1;
			}
		}
		erreur='';
	}
	affichageFichiergw();
}

// affichage des fichiers
// soit une liste si n fichiers
// soit un fichier unique (ex:logo)
function affichageFichiergw(){

	// liste de fichiers par d�faut
	if (modeFichier == ''){
		var temp = '';
		var div = getElt("listeFichier");
		temp += '<table>';
		for (i=0;i<arrayFichiergw.length;i++){	
			if(arrayFichiergw[i]){
				temp += "<tr>";
				temp += "<td><input type=\"input\" class=\"ChampSaisie\" readonly name=\"LIBELLE_FICHIER_JOINT_"+i+"\" size=\"30\" value=\""+arrayFichiergw[i].nom+"\"></td>";
				temp += "<td><input type=\"button\" class=\"bouton\" value=\"Supprimer\" onClick=\"supprimerFichiergw('"+i+"');\"></td>";
				temp += "</tr>";
			}
		}
		temp += '</table>';
		div.innerHTML = temp.toString();
	}
	// sp�cifique pour les fichiers avec gestion de version
	else if (modeFichier == 'version')
	{
		var temp = '';
		var div = getElt("listeFichier");
		temp += '<table>';
		for (i=0;i<arrayFichiergw.length;i++){	
			if(arrayFichiergw[i]){
				temp += "<tr>";
				temp += "<td><input type=\"input\" class=\"ChampSaisie\" readonly name=\"VERSION_FICHIER_JOINT_"+i+"\" size=\"1\" value=\""+arrayFichiergw[i].version+"\"></td>";
				temp += "<td><input type=\"input\" class=\"ChampSaisie\" readonly name=\"DATE_FICHIER_JOINT_"+i+"\" size=\"6\" value=\""+arrayFichiergw[i].date+"\"></td>";
				temp += "<td><input type=\"input\" class=\"ChampSaisie\" readonly name=\"LIBELLE_FICHIER_JOINT_"+i+"\" size=\"30\" value=\""+arrayFichiergw[i].nom+"\"></td>";
				temp += "<td><input type=\"button\" class=\"bouton\" value=\"Supprimer\" onClick=\"supprimerFichiergw('"+i+"');\"></td>";
				temp += "</tr>";
			}
		}
		temp += '</table>';
		div.innerHTML = temp.toString();
	}
}

// pr�paration pr�-validation
// concat�nation du fichier joint
function preparerFichiergwUnique(){
	// traitement normal
	if (erreur2.length==0){
		if (fichierJointUnique){
		var temp = fichierJointUnique.id+";"+fichierJointUnique.nom+";"+fichierJointUnique.format+";"+fichierJointUnique.version+";"+fichierJointUnique.date;
		window.document.forms[indiceForm].FICHIER_UNIQUE.value = temp;
	}
	}
	// restauration de la saisie en cas d'erreur
	else{
		fichierJointUnique = null;
		chaineFichier = window.document.forms[indiceForm].FICHIER_UNIQUE.value;
		if (chaineFichier.length>0){
				fic = chaineFichier.split(";");
				fichierJointUnique = new Fichiergw(fic[0],fic[1],fic[2],fic[3],fic[4]);
		}
		erreur2='';
	}
	affichageFichiergwUnique();
}

// affichage du fichier unique (ex:logo ou photo)
function affichageFichiergwUnique(){
	if(fichierJointUnique){
		if(fichierJointUnique.format.indexOf("image")==-1){
			alert("Ce fichier n'est pas une image !");
			fichierJointUnique=null;
		}
		else{
			window.document.forms[indiceForm].LIBELLE_FICHIER.value = fichierJointUnique.nom;
			window.document.forms[indiceForm].FICHIER_UNIQUE.value = fichierJointUnique.id+";"+fichierJointUnique.nom+";"+fichierJointUnique.format+";"+fichierJointUnique.version;
		}
	}
}

//sp�cifique fichier unique ex:logo espace collaoboratif
// attention l'index du formulaire est variable selon les page
function effacerFichier(){
	window.document.forms[indiceForm].LIBELLE_FICHIER.value = 'Cliquer sur parcourir';
	window.document.forms[indiceForm].FICHIER_UNIQUE.value = '';
	if(fichierJointUnique){
		fichierJointUnique=null;
	}
}

/* Gestion des dossiers */

var arrayDossiergw = new Array(); 

//constructeur de la classe
function Dossiergw(p1, p2, p3, p4, p5)
{
	this.id = p1;
	this.code = p2;
	this.parent = p3;
	this.nom = p4;
	this.espace = p5;
}

function visualiserDossiergw()
{
	window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=TRAITEMENT_DOSSIERGW_FRONT&ACTION=VISUALISER&SAISIE_FRONT=true&ESPACE="+espace+"&ID_DOCUMENTGW="+documentgw+"", "dossiergw", "width=400, height=400, status=no");
}

function ajouterDossiergw()
{
	if (d.aNodes[d.selectedNode]){
		codeParent = d.aNodes[d.selectedNode].id;
		window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=TRAITEMENT_DOSSIERGW_FRONT&ACTION=INSERER&SAISIE_FRONT=true&ESPACE="+espace+"&CODE_PARENT="+codeParent+"", "Dossiergw", "width=400, height=200, status=no");
	}else{
		alert("Veuillez s�lectionner un emplacement!");
	}
}

function supprimerDossiergw()
{
	if (d.aNodes[d.selectedNode]){
		code = d.aNodes[d.selectedNode].id;
		if (code==0){
			alert("Vous ne pouvez pas supprimer la racine!");
		}else{
			window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=TRAITEMENT_DOSSIERGW_FRONT&ACTION=SUPPRIMER&SAISIE_FRONT=true&ESPACE="+espace+"&CODE_DOSSIER="+code+"&ID_DOCUMENTGW="+documentgw+"", "Dossiergw", "width=400, height=200, status=no"); 
		}
	}else{
		alert("Veuillez s�lectionner un dossier!");
	}

}

function modifierDossiergw()
{
	if (d.aNodes[d.selectedNode]){
		code = d.aNodes[d.selectedNode].id;
		if (code==0){
			alert("Vous ne pouvez pas modidier la racine!");
		}else{
			window.open("/servlet/com.jsbsoft.jtf.core.SG?PROC=TRAITEMENT_DOSSIERGW_FRONT&ACTION=MODIFIER&SAISIE_FRONT=true&ESPACE="+espace+"&CODE_DOSSIER="+code+"", "Dossiergw", "width=400, height=200, status=no"); 
		}
	}else{
		alert("Veuillez s�lectionner un dossier!");
	}
}

function synchroniserDossiergw() 
{
	if (d.aNodes[d.selectedNode]){
		code =d.aNodes[d.selectedNode].id;
		window.opener.arrayDossiergw = arrayDossiergw;
		window.close();
		if (code==0){
			window.opener.document.forms[indiceForm].LIBELLE_DOSSIER.value="Racine (par d�faut)";
			window.opener.document.forms[indiceForm].NOM_DOSSIER.value="";
			window.opener.document.forms[indiceForm].DOSSIER.value="";
		}else{
			window.opener.document.forms[indiceForm].DOSSIER.value=code;
			if (arrayDossiergw.length>0)
			{
				for (i=0;i<arrayDossiergw.length;i++)
				{
					if(arrayDossiergw[i])
					{
						if (arrayDossiergw[i].code==code)
						{
							window.opener.document.forms[indiceForm].LIBELLE_DOSSIER.value=arrayDossiergw[i].nom;
							window.opener.document.forms[indiceForm].NOM_DOSSIER.value=arrayDossiergw[i].nom;
						}
					}
				}
			}
		}
	}else{
		window.opener.arrayDossiergw = arrayDossiergw;
		window.close();
		window.opener.document.forms[indiceForm].DOSSIER.value="";
		window.opener.document.forms[indiceForm].LIBELLE_DOSSIER.value="Racine (par d�faut)";
		window.opener.document.forms[indiceForm].NOM_DOSSIER.value="";
	}
}

function fermerDossiergw()
{
	window.opener.arrayDossiergw = arrayDossiergw;
	window.close();
	if (window.opener.document.forms[indiceForm].NOM_DOSSIER.value=="")
	{
		window.opener.document.forms[indiceForm].LIBELLE_DOSSIER.value="Racine (par d�faut)";
		window.opener.document.forms[indiceForm].NOM_DOSSIER.value="";
	}

}

function affichageDtreeDossiergw()
{
	d = new dTree('d');		
	d.add(0,-1,'Racine','#','','','/adminsite/utils/dtree/img/dd.gif','/adminsite/utils/dtree/img/dd.gif');
	if (arrayDossiergw.length>0)
	{
		for (i=0;i<arrayDossiergw.length;i++)
		{
			if(arrayDossiergw[i])
			{
				if (arrayDossiergw[i].parent != "")
				{
					d.add(arrayDossiergw[i].code,arrayDossiergw[i].parent,arrayDossiergw[i].nom,'#','','','/adminsite/utils/dtree/img/folder.gif','/adminsite/utils/dtree/img/folderopen.gif');
				}
				else
				{
					d.add(arrayDossiergw[i].code,0,arrayDossiergw[i].nom,'#','','','/adminsite/utils/dtree/img/folder.gif','/adminsite/utils/dtree/img/folderopen.gif');
				}
			}
		}
				}
	document.write(d);
	d.openAll();
}

/* Gestion de la palette des couleurs */

function popup_color_picker(nom1,nom2)
{
	titreCouleur = nom1;
	titreExemple = nom2;
	var width = 400;
	var height = 260;	
	window.open('/adminsite/utils/colpick/color_picker.jsp?COULEUR='+titreCouleur+'&EXEMPLE='+titreExemple+'&FORM='+indiceForm+'', 'cp', 'resizable=no, location=no, width='
				+width+', height='+height+', menubar=no, status=yes, scrollbars=no, menubar=no');
}

function effacerCouleur(nom1,nom2)
{
	window.document.forms[indiceForm].elements[nom2].style.borderColor = '';
	window.document.forms[indiceForm].elements[nom2].style.backgroundColor = '';
	window.document.forms[indiceForm].elements[nom1].value = '';
}

/* Gestion des documents */

var arrayDocumentgw = new Array(); 

//constructeur de la classe
function Documentgw(p1, p2, p3, p4, p5)
{
	this.id = p1;
	this.code = p2;
	this.url = p3;
	this.libelle = p4;
	this.dossier = p5
}



function affichageDtreeDocumentgw(){
	ddoc = new dTree('ddoc');		
	ddoc.add(0,-1,'Liste des fichiers','#','','','','');

	// gestion des dossiers
	if (arrayDossiergw.length>0)
	{
		for (i=0;i<arrayDossiergw.length;i++)
		{
			if(arrayDossiergw[i])
			{
				if (arrayDossiergw[i].parent != "")
				{
					ddoc.add(arrayDossiergw[i].code,arrayDossiergw[i].parent,arrayDossiergw[i].nom,'#','','','/adminsite/utils/dtree/img/folder.gif','/adminsite/utils/dtree/img/folderopen.gif');
				}
				else
				{
					ddoc.add(arrayDossiergw[i].code,0,arrayDossiergw[i].nom,'#','','','/adminsite/utils/dtree/img/folder.gif','/adminsite/utils/dtree/img/folderopen.gif');
				}
			}
		}
	}

	// gestion des documents
	if (arrayDocumentgw.length>0)
	{
		for (i=0;i<arrayDocumentgw.length;i++)
		{
			if (arrayDocumentgw[i])
			{
				ddoc.add(arrayDocumentgw[i].code,arrayDocumentgw[i].dossier,arrayDocumentgw[i].libelle,arrayDocumentgw[i].url,'','','/adminsite/utils/dtree/img/page.gif','/adminsite/utils/dtree/img/page.gif');
			}
		}
	}


	document.write(ddoc);
	ddoc.openAll();
	
}

// albumgw

// Fonction d'ouverture et d'alimentation du pop-up avec une image

function afficheImage(source) {
	affichage = '';

// Code html du pop-up
	affichage += '<html><head><title>Photo</title></head><body onLoad="window.focus();">';

// On appelle la source de l'image correspondant a l'emplacement et au numero de lot
	affichage += '<img src="'+source+ '">';
	affichage += '</body></html>';

// Ouverture du pop-up
	pop = window.open('','pop','status=no,directories=no,toolbar=no,location=no,menubar=no,scrollbars=yes,resizable=yes');

// Nettoyage du pop-up
	pop.document.close();

// Insertion du code HTML du pop-up
	pop.document.write(affichage);
}
