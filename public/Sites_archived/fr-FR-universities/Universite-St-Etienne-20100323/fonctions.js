$(document).ready(function() {
 
 
  settings = {
	  tl: { radius: 0 },
	  tr: { radius: 0 },
	  bl: { radius: 2 },
	  br: { radius: 2 },
	  antiAlias: false,
	  autoPad: false
  }
  
  var myBoxObject = new curvyCorners(settings, "encadre_generique");
  myBoxObject.applyCornersToAll();

  myBoxObject = new curvyCorners(settings, "encadre_nav_auto");
  myBoxObject.applyCornersToAll();
  
  myBoxObject = new curvyCorners(settings, "encadre_rubrique");
  myBoxObject.applyCornersToAll(); 
  
  myBoxObject = new curvyCorners(settings, "contenu_tab");
  myBoxObject.applyCornersToAll();    
  
  settings = {
	  tl: { radius: 2 },
	  tr: { radius: 2 },
	  bl: { radius: 2 },
	  br: { radius: 2 },
	  antiAlias: false,
	  autoPad: false
  }
  
  var myBox2Object = new curvyCorners(settings, "encadre_fiche");
  myBox2Object.applyCornersToAll();
  
  var MainObject = new curvyCorners(settings, "content");
  MainObject.applyCornersToAll();

  var   myBoxObject = new curvyCorners(settings, "encadre_structure");
  myBoxObject.applyCornersToAll();  
 
  var   myBoxObject = new curvyCorners(settings, "encadre_lien");
  myBoxObject.applyCornersToAll();  
 
  var   myBoxObject = new curvyCorners(settings, "encadre_rubrique_sanstitre");
  myBoxObject.applyCornersToAll();  
 
  
});

$(document).ready(function() {
	
	jQuery("#navigation ul.niveau2").hide();
	//jQuery("#form_centrale .champs_form:last").css({border-bottom: '1px solid #38af00'});
	jQuery("#vert #form_centrale .champs_form:last").css({border: '1px solid #38af00'});
	jQuery("#vert #form_centrale .ligne_form span:last").css("border-bottom", "1px solid #38af00");
	
	jQuery("#rouge #form_centrale .champs_form:last").css({border: '1px solid #d30003'});
	jQuery("#rouge #form_centrale .ligne_form span:last").css("border-bottom", "1px solid #d30003");
	
	jQuery("#orange #form_centrale .champs_form:last").css({border: '1px solid #ff9c00'});
	jQuery("#orange #form_centrale .ligne_form span:last").css("border-bottom", "1px solid #ff9c00");
	
	jQuery("#jaune #form_centrale .champs_form:last").css({border: '1px solid #ffb500'});
	jQuery("#jaune #form_centrale .ligne_form span:last").css("border-bottom", "1px solid #ffb500");
	
	jQuery("#rose #form_centrale .champs_form:last").css({border: '1px solid #cd2f9a'});
	jQuery("#rose #form_centrale .ligne_form span:last").css("border-bottom", "1px solid #cd2f9a");
	
	jQuery("#violet #form_centrale .champs_form:last").css({border: '1px solid #7b55c9'});
	jQuery("#violet #form_centrale .ligne_form span:last").css("border-bottom", "1px solid #7b55c9");
	
	jQuery("#vertC #form_centrale .champs_form:last").css({border: '1px solid #93c205'});
	jQuery("#vertC #form_centrale .ligne_form span:last").css("border-bottom", "1px solid #93c205");
	
	jQuery("#vertF #form_centrale .champs_form:last").css({border: '1px solid #0ba78d'});
	jQuery("#vertF #form_centrale .ligne_form span:last").css("border-bottom", "1px solid #0ba78d");
	
	jQuery("#bleu #form_centrale .champs_form:last").css({border: '1px solid #62b6ff'});
	jQuery("#bleu #form_centrale .ligne_form span:last").css("border-bottom", "1px solid #62b6ff");
	
	//jQuery("#navigation a.selected").parent().find("ul.niveau2").show();
								
	jQuery("#navigation ul.niveau1 li").hover(	
		
		  function () {
			
//				jQuery(this).find("ul.niveau2").show();
				jQuery(this).find("a:first").addClass("survol");
		  }, 
		  function () {
			//if ( !jQuery(this).find("a").hasClass("selected") ){
	//		   jQuery(this).find("ul.niveau2").hide();
			   jQuery(this).find("a:first").removeClass("survol");
			//}
		  }
    );

	
	/*jQuery("#navigation ul.niveau1 li").click(function () { 										  
		  jQuery("#navigation ul.niveau2").hide();
		  jQuery(this).find("ul.niveau2").show();
		}
    );	*/
	
	jQuery("#menu_principal ul li").hover(					  
      function () {
		jQuery(this).addClass("selected");
      }, 
      function () {
	   jQuery(this).removeClass("selected");
      }
    );		
	
   	/*jQuery("#contenu_tab").corner("bottom 10px");*/
	
	

});

$(document).ready(function(){ 
  $("a.targetblank").attr("target", "_blank");
  
  $("a.printlink").click(function(){
  	try{
	  	$('.contenu_titreprint').css({display:'block'});
		$('.contenu_onglet1').css({display:'block'});
		$('.contenu_onglet1 .contenu_tab').css({display:'block'});
		$('.contenu_onglet2').css({display:'block'});
		$('.contenu_onglet2 .contenu_tab').css({display:'block'});
		$('.contenu_onglet3').css({display:'block'});
		$('.contenu_onglet3 .contenu_tab').css({display:'block'});
		$('.contenu_onglet4').css({display:'block'});
		$('.contenu_onglet4 .contenu_tab').css({display:'block'});
		$('.contenu_onglet5').css({display:'block'});
		$('.contenu_onglet5 .contenu_tab').css({display:'block'});
	}catch(e){}
  	window.print();
  	try{
	  	$('.contenu_titreprint').css({display:'none'});
		$('.contenu_onglet1 .contenu_tab').css({display:'none'});
		$('.contenu_onglet1').css({display:'none'});
		$('.contenu_onglet2 .contenu_tab').css({display:'none'});
		$('.contenu_onglet2').css({display:'none'});
		$('.contenu_onglet3 .contenu_tab').css({display:'none'});
		$('.contenu_onglet3').css({display:'none'});
		$('.contenu_onglet4 .contenu_tab').css({display:'none'});
		$('.contenu_onglet4').css({display:'none'});
		$('.contenu_onglet5 .contenu_tab').css({display:'none'});
		$('.contenu_onglet5').css({display:'none'});
		
	}catch(e){}
	$('#onglet_actif').show();
	$('#onglet_actif.contenu_tab').show();
  	});
  	
  	var page = $("p.pagination:first").text(); 
  	if(jQuery.trim(page) == '1') {
  		$("p.pagination").html("&nbsp;");
  	}
  
});

// Redirection lors du onchange sur les selects des raccourcis
function changeSelectbox(elem) {
	var s = $("select#"+elem).val();
	if('-1' != s ) {
		 $.ajax({
		   type: "GET",
		   url: "/ujm/template/getrubriquefromcode.jsp?code="+s,
		   success: function(msg){
		     document.location.href=msg;
		   }
		 });
	 }
}

// Listes des rubriques du haut � ouvrir dans une nouvelle fenetre
// () = 'rub-' + code de la rubrique
$(document).ready(function() {
  $("a.rub-ENT").attr("target", "_blank");
});