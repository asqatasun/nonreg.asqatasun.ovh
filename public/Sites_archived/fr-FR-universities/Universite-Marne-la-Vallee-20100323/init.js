$(function() {
		$("#accordion").accordion({
			icons: {
    			header: "ui-icon-circle-arrow-e",
   				headerSelected: "ui-icon-circle-arrow-s"
			},
			event: "mouseover"
		});
		$("div.scrollable").scrollable({
		size: 2,
		items: '#thumbs',  
		hoverClass: 'hover'
		});
		$("#latest_news_block p img").tooltip({position: ['top', 'right'], offset: [10, -10], opacity: 0.9});
});
	$(document).ready(
			function (){
					$("#open").click(function(){
		$("div#panel").slideDown("slow");
	
	});	
	
	// Collapse Panel
	$("#close").click(function(){
		$("div#panel").slideUp("slow");	
	});		
	
	// Switch buttons from "Log In | Register" to "Close Panel" on click
	$("#toggle a").click(function () {
		$("#toggle a").toggle();
	});
	$("#pikame").PikaChoose();
					$("#pikame img").tooltip({position: ['top', 'center'], offset: [9, 50], opacity: 0.9});
					$("#pageflip").hover(function() {
					$("#pageflip img , .msg_block").stop()
						.animate({
						width: '257px', 
						height: '267px'
						}, 500); 
					} , function() {
					$("#pageflip img").stop() 
						.animate({
						width: '85px', 
						height: '85px'
					}, 220);
					$(".msg_block").stop() 
					.animate({
					width: '83px', 
					height: '83px'
					}, 200);
				});
				$('#tri-select ul').hover( function(){
		$('.inactive', this).show();
		$(this).addClass('hover');
	},
	function() {
		$('.inactive', this).hide();
		$(this).removeClass('hover');
	}),
				$("a[rel='example1']").colorbox();
				$("a[rel='example2']").colorbox({transition:"fade"});
				$("a[rel='example3']").colorbox({transition:"none", width:"75%", height:"75%"});
				$("a[rel='example4']").colorbox({slideshow:true});
				$(".single").colorbox({}, function(){
					alert('Howdy, this is an example callback.');
				});
				$(".colorbox").colorbox();
				$(".youtube").colorbox({iframe:true, width:650, height:550});
				$(".iframe").colorbox({width:950, height:630, iframe:true});
				$(".inline").colorbox({width:"50%", inline:true, href:"#inline_example1"});
				
				//Example of preserving a JavaScript event for inline calls.
				$("#click").click(function(){ 
					$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
					return false;
				});
			});