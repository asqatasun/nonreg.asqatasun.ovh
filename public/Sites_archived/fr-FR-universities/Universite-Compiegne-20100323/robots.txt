http://www.utc.fr/

User-agent: *
Disallow: /iso_admin/
Disallow: /iso_album/
Disallow: /iso_icons/
Disallow: /iso_misc/
Disallow: /iso_resource/
Disallow: /iso_scripts/
Disallow: /utc_html/
Disallow: /utc_html/international
