/*
	ED - 12.07.2006
	
	Script pour gerer les menus deroulants structures comme des listes <ul> imbriquees.
	Les listes <ul> parentes (ou le menu de plus haut niveau) doient avoir une classe dont
	la valeur est definie ici par la variable : classeMenuDeroulant
	
	initMenus() parcour le document a la recherche des listes <ul> possedant cette classe
	et attache des gestionnaires d evenements aux <li> qui contiennent des sous menus
	pour les faire apparaitre et disparaitre au survol
	
	Attention: la fonction addEvent() definie dans defaut.js est requise !

*/
var classeMenuDeroulant = /menu_deroulant/;
var classMenuDeroule = /sous_menu_actif/;

/*
	Attache les gestionnaires d evenements aux lis qui ont des sous menus
*/
function initMenus() {
	var menu, menus, lis, liSousMenus;
	
	if(document.getElementById && document.getElementsByTagName){
		menus = document.getElementsByTagName("ul");
		for(var j=0; j<menus.length; j++){
			if(menus[j].className.match(classeMenuDeroulant)){
				menu = menus[j];
				lis = menu.getElementsByTagName("li");
				for(var i=0; i<lis.length; i++){
					liSousMenus = lis[i].getElementsByTagName("ul");
					if(liSousMenus.length > 0){
						if (lis[i].id.match("^menu_")) {
							addEvent(lis[i],"mouseover", montrePremierSousMenu);
							addEvent(lis[i],"focus", montrePremierSousMenu);
							addEvent(lis[i],"mouseout", cachePremierSousMenu);	
							addEvent(lis[i],"blur", cachePremierSousMenu);
						} else {
							addEvent(lis[i],"mouseover", montreSecondSousMenu);
							addEvent(lis[i],"focus", montreSecondSousMenu);
							addEvent(lis[i],"mouseout", cacheSecondSousMenu);	
							addEvent(lis[i],"blur", cacheSecondSousMenu);
						}
					}
				}
			} 
		}
	}
}

/*
	Montre et cache les sous menus
*/
function montrePremierSousMenu(){
	var sousMenu = document.getElementById("sous_" + this.id);
	
	if (sousMenu != null) {
		sousMenu.style.display="block";
	}
	
	var sousMenus = document.getElementsByTagName("ul");
	for(var j=0; j < sousMenus.length; j++){
		if(sousMenus[j].id != "sous_" + this.id && sousMenus[j].className.match(classMenuDeroule)){
			sousMenus[j].style.display="none";
			break;
		}
	}
}

function cachePremierSousMenu(){
	var sousMenu = document.getElementById("sous_" + this.id);
	if (sousMenu != null) {
		sousMenu.style.display="none";
	}
	
	var sousMenus = document.getElementsByTagName("ul");
	for(var j=0; j < sousMenus.length; j++){
		if(sousMenus[j].className.match(classMenuDeroule)){
			sousMenus[j].style.display="block";
			break;
		}
	}
}


function montreSecondSousMenu(){
    this.getElementsByTagName("ul")[0].style.display = "block";
}

function cacheSecondSousMenu(){
    this.getElementsByTagName("ul")[0].style.display = "none";
}


/*
	Appelle initMenus() au chargement de la page
*/
addEvent(window,"load", initMenus);