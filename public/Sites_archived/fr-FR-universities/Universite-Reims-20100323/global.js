function MM_swapImgRestore()
{
	var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n,d)
{
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && document.getElementById) x=document.getElementById(n); return x;
}

function MM_swapImage()
{
	var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
	if((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function popup(url, type, height, width,full){
	var top=(screen.height-height)/2;
	var left=(screen.width-width)/2;
	var nsNav = (document.layers) ? 1 : 0;
	var ieNav = (document.all) ? 1 : 0;
	
	// popup plein ecran
	if(full)
	{
		height = screen.availHeight - 30;
		width = screen.availWidth - 10;
		top=0;
		left=0;
	}
  
	wintype="toolbar="+type+",menubar="+type+",location='no',scrollbars="+type+",top="+top+",left="+left;
	wintype=wintype + ",height=" + height + ",width=" + width;
	var newwin = window.open(url,"popup"+type,wintype);
	newwin.focus();
}

function popupscroll(url, height, width,full,name){
	var top=(screen.height-height)/2;
	var left=(screen.width-width)/2;
	var nsNav = (document.layers) ? 1 : 0;
	var ieNav = (document.all) ? 1 : 0;
	
	// popup plein ecran
	if(full)
	{
		height = screen.availHeight - 30;
		width = screen.availWidth - 10;
		top=0;
		left=0;
	}
    if (name)
    {
        winname = name;
    }
    else
    {
        winname = "popupscroll";
    }
	wintype="toolbar=no,menubar=no,location='no',scrollbars=yes,top="+top+",left="+left;
	wintype=wintype + ",height=" + height + ",width=" + width;
	var newwin = window.open(url,winname,wintype);
	newwin.focus();
}

function popupImg(url, height, width)
{
	Fenetre = window.open('','_blank','width='+width+',height='+height+',top=0,left=0,scrollbars=no');
	Fenetre.document.write("<html><body leftmargin=0 topmargin=0><a href=\"\" onclick=window.close()><img src=");
	Fenetre.document.write(url);
	Fenetre.document.write(" alt=\"Cliquez pour fermer la fen�tre\" border=0></a></body>");
}

function popupImgAuto(url, titre)
{
	w = window.open('','chargement','width=10,height=10');
	w.document.write( "<html><head><title>"+titre+"</title>\n" ); 
	w.document.write( "<script type=\"text/javascript\">\n"); 
	w.document.write( "function autoSize() {\n"); 
	w.document.write( "self.resizeTo(document.images[0].width+10,document.images[0].height+29)\n"); 
	w.document.write( "self.focus();\n"); 
	w.document.write( "}\n");
	w.document.write( "</script>\n"); 
	w.document.write( "</head><body leftmargin=0 topmargin=0 marginwidth=0 marginheight=0 onLoad='javascript:autoSize()'>" );
	w.document.write( "<a href='javascript:window.close()'><img src='"+url+"' border=0 alt='"+titre+"'></a>" ); 
	w.document.write( "</body></html>" );
	w.document.close(); 
}

function showHide(id) {
	if (!document.layers) {
		var _divHide= document.getElementById("hide_" + id);
		var _divShow= document.getElementById("show_" + id);
		if (_divHide.style.display=="none") {
			_divHide.style.display="block";
			_divShow.style.display="none";
		}else {
			_divHide.style.display="none";
			_divShow.style.display="block";
		}
	}else {
		if (document.layers["hide_" + id].visibility=="hide") {
			document.layers["hide_" + id].visibility="show";
			document.layers["show_" + id].visibility="hide";
		}else {
			document.layers["hide_" + id].visibility="hide";
			document.layers["show_" + id].visibility="show";
		}
	}
}

function SwitchMenu(obj){
	var el = document.getElementById(obj);
	var ar = document.getElementById("cont").getElementsByTagName("DIV");
	if(el.style.display == "none"){
		for (var i=0; i<ar.length; i++){
			ar[i].style.display = "none";
		}
		el.style.display = "block";
	}else{
		el.style.display = "none";
	}
}

function search(obj,value)
{
	for (i=0; i < obj.length; i++)
	{
		if(obj[i].value==value)
		return i;
	}
}

function postform()
{
	postwizard();
}

function webEditor(field)
{
	window.open('service/webeditor/word.php?name='+field,'WebEditor','personalbar=no,toolbar=no,status=no,scrollbars=no,location=no,resizable=no,menubar=no,titlebar=no,Top='+20+',Left='+20+',Width='+621+',Height='+638);
}

function webColor(value)
{
	return showModalDialog('service/color/color.htm',value,'status=no,toolbar=no,scroll:no;resizable:no;dialogWidth:248px;dialogHeight:280px');
}

function webCalendar(name,hidden,form,theme)
{
	if (form==null){form='wizard';}
	if (theme==null){theme='';}
	window.open('service/calendar'+theme+'/calendar.php?name='+name+'&hidden='+hidden+'&form='+form+'&theme='+theme,'calendar_'+name,'status=no,toolbar=no,scrollbars=no,resizable=no,width=172,height=153,left=20,top=20');
}

function validation()
{
	alert(document.form.webeditor.value);
}

function imageGallery(obj,img)
{
  document.wizard.obj.value=img;
}

function display()
{
	var name = document.wizard.template.value;
	if(name) document.image.src = "layers/images/"+name+".gif";
	else document.image.src = "layers/images/empty.gif";
}

//function replace
function replace(string,text,by) {
// Replaces text with by in string
    var strLength = string.length, txtLength = text.length;
    if ((strLength == 0) || (txtLength == 0)) return string;

    var i = string.indexOf(text);
    if ((!i) && (text != string.substring(0,txtLength))) return string;
    if (i == -1) return string;

    var newstr = string.substring(0,i) + by;

    if (i+txtLength < strLength)
        newstr += replace(string.substring(i+txtLength,strLength),text,by);

    return newstr;
}

//Paste the textareavalue in another area value field1 => field2
function paste(field1, field2, size){
	var2 = eval("document.wizard."+field2+".value");
	if(var2 == ""){
		var1 = eval("document.wizard."+field1+".value");
		var1 = replace(var1,'"',"''");
		var1 = replace(var1,'\r',"<br>");
		var1 = replace(var1,'\n',"<br>");
		if(size!=""){
			if(var1.length > size){
				var3 = var1.substring(0,size-4)+"...";
			}else{
				var3 = var1;
			}
		}else{
			var3 = var1;
		}
		vareval = "document.wizard."+field2+".value=\""+var3+"\";";
		eval(vareval);
	}

}

//format the date of the art_date_begin of the day if null
function formatdatebegin(){
	var today = new Date();

	datebegin = document.wizard.art_date_begin.value;
	if (datebegin == ""){
		daY = today.getYear();
		daM = today.getMonth()+1;
		daD = today.getDate();
		if(daM < 10){
	      	daM ="0" + daM;
   		}
	   	if(daD < 10){
    	  	daD ="0" + daD;
	   	}
			document.wizard.art_date_begin.value = daY+"-"+daM+"-"+daD;
	}
}

//format the date of the art_date_begin of the day if null
function formatdateend(){
	var dateend;
	dateend = document.wizard.art_date_end.value;
	if (dateend == ""){
		document.wizard.art_date_end.value = "2030-12-31";
	}
}

function listBoxValid()
{
  var rub = document.wizard.dir_name.value;
  var tem = document.wizard.art_form.value;
  if (rub != "" && tem != ""){
  	document.wizard.submit();
  }else{
  	alert("Le gabarit ou la rubrique ne sont pas s�lectionn�s");
  }
}

//format the number if the value is a number
function formatNumeric(fieldname){
	var val = StrString = eval("document.wizard."+ fieldname +".value");
	valreturn = "";
	//alert(val);
	if(numericValid(val)){
		if(val == 0 || val == "00" || val == "0 " || val == " 0"){
			valreturn = '';
			eval("document.wizard."+ fieldname +".value='';")
		}else{
			if(val < 10 && val.length==1){
				valreturn = "0"+val;
			}else{
				valreturn = val;
			}
			//alert();
			eval("document.wizard."+ fieldname +".value='"+valreturn+"';")
		}
	}
}

function getRadioIndex(obj)
{
	for (i=0; i < obj.length; i++)
	{
		if(obj[i].checked==true)
		return i;
	}
	return -1;
}

//add your validation function here
function myValid(value)
{
    if(value=="")
    {
        return false
    }else
    {
        return true
    }
}
//END OF FORM VALIDATION

//SUBMIT forms
//list_rubrik
function postformrubrik(action)
{
	document.list_rubrik.action = action
	document.list_rubrik.submit()
}

var allchecked = true
function checkall(formulaire)
{
	for(var i=0;i<formulaire.elements.length;i++)
	{
		if(formulaire.elements[i].disabled==false)
		{
			formulaire.elements[i].checked = allchecked
		}
	}
	allchecked = !allchecked
}

function checkallsearch(formulaire,chaine)
{
	var nom_champ = chaine.substring(0,chaine.length - 1);
	var init_field = formulaire.elements[nom_champ];

	for (var i=0; i<formulaire.elements.length; i++)
	{
    	var e = formulaire.elements[i];
	    if (e.name.search(chaine) != -1)
		{
			if( init_field.checked>0  )
			{
				e.checked = 1;
			}else e.checked = 0;		
		}
	}
}

function checkRights(formulaire,init_field,tab_id)
{
	for (i=0;i<tab_id.length;i++)
	{
		var e = formulaire.elements[tab_id[i]];
		if( formulaire.elements[init_field].checked>0  )
			e.checked = 1;
		else
			e.checked = 0;
	}
}

function checkRightsForRight(formulaire,init_field,tab_id,parent)
{
	if (formulaire.elements[init_field].checked>0 && init_field!='ch_1')
	{
		var e = formulaire.elements['ch_1'];
		e.checked=0;

		if(parent && parent!='1')
		{
			var e = formulaire.elements['ch_'+parent];
			e.checked=0;
		}
	}

	for (i=0;i<tab_id.length;i++)
	{
		var e = formulaire.elements[tab_id[i]];
		
		if(formulaire.elements[init_field].checked>0) e.checked = 0;
	}

}

function hide(element,is_minimizable,id,couleur)
{	
	if(is_minimizable==1)
	{
		var comp=document.getElementById(element);
		var label=document.getElementById('comp_'+id+'_minimized_label');	
		if(comp.style.display == "")	
		{
			label.innerHTML="<img src=\""+images_path+"portlet/ouvrir_"+couleur+".gif\" border=\"0\" width=\"11\" height=\"22\">";
			comp.style.display = "none";			
		}else
		{
			label.innerHTML="<img src=\""+images_path+"portlet/fermer_"+couleur+".gif\" border=\"0\" width=\"11\" height=\"22\">";
			comp.style.display = "";			
		}
	}
}

function setUp(form,select,text) {
obj1 = new SelObj(form,select,text);
obj1.bldInitial(); 
}

function SelObj(formname,selname,textname,str) {
    this.formname = formname;
    this.selname = selname;
    this.textname = textname;
    this.select_str = str || '';
    this.selectArr = new Array();
    this.initialize = initialize;
    this.bldInitial = bldInitial;
    this.bldUpdate = bldUpdate;
    this.bldAddOption = bldAddOption;
    this.bldDelOption = bldDelOption;
}

function initialize() {
    if (this.select_str =='') {
        for(var i=0;i<document.forms[this.formname][this.selname].options.length;i++) {
            this.selectArr[i] = document.forms[this.formname][this.selname].options[i];
            this.select_str += document.forms[this.formname][this.selname].options[i].value+":"+
            document.forms[this.formname][this.selname].options[i].text+",";
        }
    }
    var tempArr = this.select_str.split(',');
    this.selectArr = new Array();
    for(var i=0;i<tempArr.length;i++) {
        if (tempArr[i]!='') {
            var prop = tempArr[i].split(':');
            this.selectArr[i] = new Option(prop[1],prop[0]);
        }
    }
    return;
}

function bldInitial() {
    this.initialize();
    for(var i=0;i<this.selectArr.length;i++) {
        if (this.selectArr[i]!=null) {
            document.forms[this.formname][this.selname].options[i] = this.selectArr[i];
        }
    }
    document.forms[this.formname][this.selname].options.length = this.selectArr.length;
    return;
}

function bldUpdate() {
    var str = document.forms[this.formname][this.textname].value.replace('^\\s*','');
    var reg = new RegExp('^-*', 'g');
    if(str == '') {this.bldInitial();return;}
    this.initialize(); //lwf
    var j = 0;
    pattern1 = new RegExp("^"+str,"i");
    for(var i=0;i<this.selectArr.length;i++)
        if(pattern1.test(this.selectArr[i].text.replace(reg,'')))
            document.forms[this.formname][this.selname].options[j++] = this.selectArr[i];
    document.forms[this.formname][this.selname].options.length = j;
    if(j==1){
        document.forms[this.formname][this.selname].options[0].selected = true;
    }
}

function bldAddOption(id,name)
{
    var strToAdd = id+":"+name+",";
    this.select_str += strToAdd;
}

function bldDelOption(id,name)
{
    var strToDel = id+":"+name+",";
    this.select_str = this.select_str.substr(0,this.select_str.indexOf(strToDel)) + this.select_str.substring(this.select_str.indexOf(strToDel)+strToDel.length,this.select_str.length);
}

function isArray(obj) {
   if (obj.constructor.toString().indexOf("Array") == -1)
      return false
   else
      return true
}

//*********************************
//	TEXTSIZER
//*********************************

//Specify affected tags. Add or remove from list:
var tgs = new Array('div','table','p','td','font');

//Specify spectrum of different font sizes:
var szs = new Array('10px','12px','14px','18px','23px');

var startSz = 0;

function ts( trgt,inc ) {
	if (!document.getElementById) return
	var d = document,cEl = null,sz = startSz,i,j,cTags;
	
	sz += inc;
	if ( sz < 0 ) sz = 0;
	if ( sz > 4 ) sz = 4;
	startSz = sz;
		
	if ( !( cEl = d.getElementById( trgt ) ) ) cEl = d.getElementsByTagName( trgt )[ 0 ];

	cEl.style.fontSize = szs[ sz ];

	for ( i = 0 ; i < tgs.length ; i++ ) {
		cTags = cEl.getElementsByTagName( tgs[ i ] );
		for ( j = 0 ; j < cTags.length ; j++ ) cTags[ j ].style.fontSize = szs[ sz ];
	}
}