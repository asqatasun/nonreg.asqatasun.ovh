function f_ChangeLocale (szLocale)
{
       var date_exp = new Date();
       date_exp.setTime(date_exp.getTime()+(365*24*3600*1000));

       document.cookie = "LAFARGE_COM_LOCALE=" + szLocale + ";expires=" + date_exp.toGMTString() + ";path=/";
       document.location.reload ();
}


function rememberCaseStudies (id)
{
       var date_exp = new Date();
       date_exp.setTime(date_exp.getTime()+(3600*1000));
       document.cookie = "CASE_STUDIES=" + id + ";expires=" + date_exp.toGMTString() + ";path=/";
}