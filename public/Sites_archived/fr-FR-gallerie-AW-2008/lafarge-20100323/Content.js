function openEncart (eEncart, eEncart2)
{
document.getElementById(eEncart).style.display = "block";
document.getElementById(eEncart2).style.display = "none";
}

function closeEncart (eEncart, eEncart2)
{
document.getElementById(eEncart).style.display = "none";
document.getElementById(eEncart2).style.display = "block";
}

function openEncart (eEncart, eEncart2, eEncart3)
{
document.getElementById(eEncart).style.display = "block";
document.getElementById(eEncart3).style.display = "block";
document.getElementById(eEncart2).style.display = "none";
}

function closeEncart (eEncart, eEncart2, eEncart3)
{
document.getElementById(eEncart).style.display = "none";
document.getElementById(eEncart3).style.display = "none";
document.getElementById(eEncart2).style.display = "block";
}

function ouvertureEncart (eEncart, eEncart2, eEncart3, eEncart4)
{
document.getElementById(eEncart).style.display = "block";
document.getElementById(eEncart2).style.display = "none";
document.getElementById(eEncart3).style.display = "block";
document.getElementById (eEncart4).style.cursor = "default";
}

function fermetureEncart (eEncart, eEncart2, eEncart3, eEncart4)
{
document.getElementById(eEncart).style.display = "none";
document.getElementById(eEncart2).style.display = "block";
document.getElementById(eEncart3).style.display = "none";
document.getElementById (eEncart4).style.cursor = "pointer";
}

function mettreMarginBottom(eEncart)
{
	document.getElementById(eEncart).style.marginBottom = "10px";
}

function enleverMarginBottom(eEncart)
{
	document.getElementById(eEncart).style.marginBottom = "10px";
}

//CODE JAVASCRIPT FLASH PLAYER
var clientFlash  = ((navigator.appName=='Microsoft Internet Explorer')?'ie':(((navigator.appVersion).indexOf('Safari')!=-1)?'sa':((navigator.appName=='Opera')?'op':'ff')));
var accessLinkId = "";

function new_image_flash (file)
	{
	var image = new Image();
	image.src = file;
	return image;
	}

var lesImages = [];
	lesImages[0] = new_image_flash ('/wps/themes/html/CommonFiles/img/pause_off.gif');
	lesImages[1] = new_image_flash ('/wps/themes/html/CommonFiles/img/pause_on.gif');
	lesImages[2] = new_image_flash ('/wps/themes/html/CommonFiles/img/play_off.gif');
	lesImages[3] = new_image_flash ('/wps/themes/html/CommonFiles/img/play_on.gif');

function changeImageOn (image, span, pauseLabel, playLabel)
{
	var innerHTMLSpan = document.getElementById (span).innerHTML;

	if (innerHTMLSpan.indexOf (playLabel) != -1)
	{
	 	document.getElementById (image).src  = lesImages[0].src;
	}

	if (innerHTMLSpan.indexOf (pauseLabel) != -1)
	{
	 	document.getElementById (image).src  = lesImages[2].src;
	}
}

function changeImageOut (image, span, pauseLabel, playLabel)
{
	var innerHTMLSpan = document.getElementById (span).innerHTML;

	if (innerHTMLSpan.indexOf (pauseLabel) != -1)
	{
	 	document.getElementById (image).src  = lesImages[1].src;
	}

	if (innerHTMLSpan.indexOf (playLabel) != -1)
	{
	 	document.getElementById (image).src  = lesImages[3].src;
	}
}


function playFlash (id_flash, image, span, pauseLabel, playLabel)
{
	var innerHTMLSpan = document.getElementById (span).innerHTML;

	if (innerHTMLSpan.indexOf (playLabel) != -1)
	{
		document.getElementById (span).innerHTML  = pauseLabel;
	}

	if (innerHTMLSpan.indexOf (pauseLabel) != -1)
	{
		document.getElementById (span).innerHTML  = playLabel;	
	}

	innerHTMLSpan = document.getElementById (span).innerHTML;

	if (innerHTMLSpan.indexOf (pauseLabel) != -1)
	{
		document.getElementById(id_flash).playSWF();
	 	document.getElementById (image).src  = lesImages[1].src;
	}

	if (innerHTMLSpan.indexOf (playLabel) != -1)
	{
		document.getElementById(id_flash).pauseSWF();
	 	document.getElementById (image).src  = lesImages[3].src;
	}
}

function outFlash()
{
	if (accessLinkId != "")
	{
		var elm = document.getElementById (accessLinkId);

		if (elm)
			elm.focus();
	}
} 
    
function thisMovie(movieName) 
{
    if (navigator.appName.indexOf("Microsoft") != -1) 
    {
	    return window[movieName];
    }
    else 
    {
	    return document[movieName];
    }
} 
    
function getFocusFlash(id, idAccessLink)
{   
	accessLinkId = idAccessLink;

	thisMovie (id).focus ();
	thisMovie (id).putFocus();
}

function showAndHideListenButton (id)
{
	var elm = document.getElementById (id);
	
	if (elm.style.display == "block")
		elm.style.display = "none";
	else
		elm.style.display = "block";
}

var selectedString="";

function getSelectedHTML() {
  selectedString="";
  var rng=undefined;
  if (window.getSelection) {
    selobj = window.getSelection();
    if (!selobj.isCollapsed) {
      if (selobj.getRangeAt) {
        rng=selobj.getRangeAt(0);
      }
      else {
        rng = document.createRange();
        rng.setStart(selobj.anchorNode,selobj.anchorOffset);
        rng.setEnd(selobj.focusNode,selobj.focusOffset);
      }
      if (rng) {
        DOM = rng.cloneContents();
        object = document.createElement('div');
        object.appendChild(DOM.cloneNode(true));
        selectedString=object.innerHTML;
      }
      else {
        selectedString=selobj;
      }
    }
  }
  else if (document.selection) {
    selobj = document.selection;
    rng = selobj.createRange();
    if (rng && rng.htmlText) {
      selectedString = rng.htmlText;
    }
    else if (rng && rng.text) {
      selectedString = rng.text;
    }
  }
  else if (document.getSelection) {
    selectedString=document.getSelection();
  }
}

function copyselected()
{
  setTimeout("getSelectedHTML()",50);
  return true;
}

document.onmouseup = copyselected;
document.onkeyup = copyselected;


/* The expanding function */

function readspeaker(rs_call)
{
  if (selectedString.length>0) {
    rs_call=rs_call.replace("/cgi-bin/rsent?","/enterprise/rsent_wrapper.php?");
  }
  savelink=rs_call+"&save=1";
  start_rs_table="<table style='border:1px solid #aeaeae; font-size: 10px;'><tr><td>";
 rs_embed="<object type='application/x-shockwave-flash' data='http://media.readspeaker.com/flash/readspeaker20.swf?mp3="+escape(rs_call)+"&autoplay=1&rskin=bump&tips=0&ricon=0&rlink=0&rprogress=1&rloading=1&c1=0xDCE4E9&c2=0x607A80&c3=0xFFFFFF&c4=0xFFFFFF&c5=0xDCE4E9&c7=0xC1C5C6&c8=0x607A80&c9=0x354241&c12=0x5898AE&c13=0x73B0BF' height='20' width='200'><param name='movie' value='http://media.readspeaker.com/flash/readspeaker20.swf?mp3="+escape(rs_call)+"&autoplay=1&rskin=bump' /><param name='quality' value='high' /><param name='SCALE' value='exactfit' /><param name='wmode' value='transparent' /><embed wmode='transparent' src='http://media.readspeaker.com/flash/readspeaker20.swf?mp3="+escape(rs_call)+"&autoplay=1&&rskin=bump&tips=0&ricon=0&rlink=0&rprogress=1&rloading=1&c1=0xDCE4E9&c2=0x607A80&c3=0xFFFFFF&c4=0xFFFFFF&c5=0xDCE4E9&c7=0xC1C5C6&c8=0x607A80&c9=0x354241&c12=0x5898AE&c13=0x73B0BF' quality='high' pluginspage='http://www.macromedia.com/go/getflashplayer' type='application/x-shockwaveflash' scale='exactfit' height='20' width='200' /></embed></object>";
  rs_downloadlink="<br />Speech-enabled by <a href='http://www.readspeaker.com'>ReadSpeaker</a><br /><a href='"+savelink+"'>Download audio</a>";
  close_rs="<br /><a href='#' onclick='close_rs_div(); return false;'>Close window</a>";
  end_rs_table="</td></tr></table>";

  var x=document.getElementById('rs_div');
  
  if (x.innerHTML != "")
  	x.innerHTML="";
  else
	x.innerHTML=rs_embed;
}

function close_rs_div()
{
  var x=document.getElementById('rs_div');
  x.innerHTML="";
}


