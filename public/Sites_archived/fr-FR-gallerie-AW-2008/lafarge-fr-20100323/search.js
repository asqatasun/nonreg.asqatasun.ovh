function f_Aposition()
{
	var apo = document.getElementById("aposition");
	
	if (apo)
	{
		apo.style.display = "block";
	}
}

function f_HideAposition()
{
	var apo = document.getElementById("aposition");
	
	if (apo)
	{
		apo.style.display = "none";
	}
}

function f_submitAposition(id)
{
	document.getElementById('searchField').value=document.getElementById(id).innerHTML;
	document.getElementById('formsearch').submit();
}