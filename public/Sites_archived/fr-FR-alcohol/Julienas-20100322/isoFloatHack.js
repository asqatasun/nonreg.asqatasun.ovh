/******************************************************************************
* isoFloatHack.js
*******************************************************************************

*******************************************************************************
*                                                                             *
* Copyright 2008
*                                                                             *
******************************************************************************/

function isoHackFloat()
{
	// IE bug : a float having the clear property only clears preceding floats of its same direction (left/right.)
	if (!!(window.attachEvent && !window.opera))	// browser is IE
	{
		var els = document.body.getElementsByTagName("*");
		var elsLen = els.length;
		var pattern1 = new RegExp("(^|\\s)isoBlockPosition-float(Left|Right)(\\s|$)");
		var pattern2 = new RegExp("(^|\\s)isoFloatClear-(both|left|right)(\\s|$)");
		var c = new Array();
		for (var i=0,j=0;i<elsLen;i++)
		{
			var e = els[i];
			var cn = e.getAttribute("className",0);
			if (cn == null) continue;
			if (typeof(cn) != "string") continue;
			var m1 = cn.match(pattern1);
			if (m1 != null)	// float element
			{
				var m2 = cn.match(pattern2);
				if (m2 != null)	// having clear property
					if (m1[2].toUpperCase() != m2[2].toUpperCase())	// not same direction
						c[j++] = {"e":e,"f":m1[2],"c":m2[2]};
			}
		}
		for (var i=0;i<j;i++)
		{
			var m = c[i];
			var e = m.e;
			var div = window.document.createElement("div");
			var dc = "isoFloatClear-"+m.c;
			div.className = dc;
			e.className = e.className.replace(dc,"");
			var p = e.parentNode;
			p.insertBefore(div,e);
		}
	}
}
addLoadAction(isoHackFloat);