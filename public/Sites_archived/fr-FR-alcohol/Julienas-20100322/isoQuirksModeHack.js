/******************************************************************************
* isoQuirksModeHack.js
*******************************************************************************

*******************************************************************************
*                                                                             *
* Copyright 2008
*                                                                             *
******************************************************************************/

function isoHackPosition()
{
	if (	window.document.compatMode &&
				window.document.compatMode == "BackCompat" &&
				window.document.body != null &&
				window.navigator &&
				window.navigator.appName &&
				window.navigator.appName == "Microsoft Internet Explorer"	)
	{
		var els = document.body.getElementsByTagName("*");
    var elsLen = els.length;
    var pattern = new RegExp("(^|\\s)isoBlockPosition-(left|center|right)(\\s|$)");
		var c = new Array();
    for (var i=0,j=0;i<elsLen;i++)
		{
			var e = els[i];
			var tagName = e.tagName.toUpperCase();
			if ( tagName == "FORM" ||
					 tagName == "CAPTION" )
					 continue;
			var m = e.className.match(pattern);
			if (m != null)		
				c[j++] = {"e":e,"a":m[2]};
		}
		for (var i=0;i<j;i++)
		{
			var m = c[i];
			var e = m.e;
			e.style.textAlign = e.currentStyle.textAlign;
			var div = window.document.createElement("div");
			div.style.textAlign = m.a;
			var p = e.parentNode;
			p.insertBefore(div,e).appendChild(p.removeChild(e));
		}
	}
}
addLoadAction(isoHackPosition);