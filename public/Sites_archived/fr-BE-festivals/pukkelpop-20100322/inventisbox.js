/*!
	Slimbox v1.64 - The ultimate lightweight Lightbox clone
	(c) 2007-2008 Christophe Beyls <http://www.digitalia.be>
	MIT-style license.
	Modified by Dieter Verjans at Inventis Web Architects @ 11/09/2008
*/

var Slimbox;

(function() {
	var state = 0, options, images, activeImage, prevImage, nextImage, top, fx, preload, preloadPrev = new Image(), preloadNext = new Image(), overlay, center, image, prevLink, nextLink, bottomContainer, bottom, caption, number;

	/*
		Translation
	*/
	
	var t = {
		closeHtml 	: 'sluiten',
		closeTitle 	: 'Sluit dit venster',
		nextHtml 	: '',
		nextTitle	: 'Bekijk volgende afbeelding',
		prevHtml	: '',
		prevTitle	: 'Bekijk vorige afbeelding',
		saveHtml	: 'download',
		saveTitle	: 'Download deze afbeelding',
		bottomText	: 'Afbeelding {x} van {y}'
	};
	
	var tFR = {
		closeHtml 	: 'fermer',
		closeTitle 	: 'Fermer la fenetre',
		nextHtml 	: '',
		nextTitle	: "Voir l'image suivante",
		prevHtml	: '',
		prevTitle	: "Voir l'image precedente",
		saveHtml	: 'download',
		saveTitle	: "Telecharger l'image",
		bottomText	: 'Image {x}  {y}'
	};
	
	var tEN = {
		closeHtml 	: 'close',
		closeTitle 	: 'Close this window',
		nextHtml 	: '',
		nextTitle	: 'View next photo',
		prevHtml	: '',
		prevTitle	: 'View previous photo',
		saveHtml	: 'download',
		saveTitle	: 'Download this photo',
		bottomText	: 'Photo {x} of {y}'
	};
	
	//fetch lang
	var urlParts = window.location.href.split("//").pop().split('/');
	var DOMAIN    = urlParts.shift();
	var LANG    = urlParts.shift();
	if(!LANG){
		LANG = 'nl';
	}
	
	if(LANG == 'fr') t = tFR;
	if(LANG == 'en') t = tEN;
	
	

	/*
		Initialization
	*/

	window.addEvent("domready", function() {
		// Append the Slimbox HTML code at the bottom of the document
		$(document.body).adopt(
			$$([
				overlay = new Element("div", {id: "lbOverlay"}).addEvent("click", close),
				center = new Element("div", {id: "lbCenter"}),
				bottomContainer = new Element("div", {id: "lbBottomContainer"})
			]).setStyle("display", "none")
		);

		image = new Element("div", {id: "lbImage"}).injectInside(center);
		//caption = new Element("div", {id: "lbCaption"}).injectTop(center);
		
		bottom = new Element("div", {id: "lbBottom"}).injectInside(bottomContainer).adopt(			
			caption = new Element("div", {id: "lbCaption"}),
			number = new Element("div", {id: "lbNumber"}),
			new Element("div", {styles: {clear: "both"}}),
			new Element("a", {id: "lbCloseLink", href: "#", html:t.closeHtml, title:t.closeTitle}).addEvent("click", close),
			saveLink = new Element("a", {id: "lbSaveLink", href: "#", html:'<span>' + t.saveHtml + '</span>', title:t.saveTitle}),
			prevLink = new Element("a", {id: "lbCaptionPrevLink", href: "#", html:'<span>' + t.prevHtml + '</span>', title:t.prevTitle}).addEvent("click", previous),
			nextLink = new Element("a", {id: "lbCaptionNextLink", href: "#", html:'<span>' + t.nextHtml + '</span>', title:t.nextTitle}).addEvent("click", next),
			new Element("div", {styles: {clear: "both"}})
		);

		fx = {
			overlay: new Fx.Tween(overlay, {property: "opacity", duration: 500}).set(0),
			image: new Fx.Tween(image, {property: "opacity", duration: 500, onComplete: nextEffect}),
			bottom: new Fx.Tween(bottom, {property: "margin-top", duration: 400})
		};
	});


	/*
		API
	*/

	Slimbox = {
		open: function(_images, startImage, _options) {
			options = $extend({
				loop: false,
				overlayOpacity: 0.4,
				resizeDuration: 400,
				resizeTransition: false,
				initialWidth: 250,
				initialHeight: 250,
				animateCaption: true,
				showCounter: true,
				counterText: t.bottomText
			}, _options || {});

			if (typeof _images == "string") {
				_images = [[_images,startImage]];
				startImage = 0;
			}

			images = _images;
			options.loop = options.loop && (images.length > 1);
			position();
			setup(true);
			top = window.getScrollTop() + (window.getHeight() / 15);
			fx.resize = new Fx.Morph(center, $extend({duration: options.resizeDuration, onComplete: nextEffect}, options.resizeTransition ? {transition: options.resizeTransition} : {}));
			center.setStyles({top: top, width: options.initialWidth, height: options.initialHeight, marginLeft: -(options.initialWidth/2), display: ""});
			fx.overlay.start(options.overlayOpacity);
			state = 1;
			return changeImage(startImage);
		}
	};

	Element.implement({
		slimbox: function(_options, linkMapper) {
			$$(this).slimbox(_options, linkMapper);

			return this;
		}
	});

	Elements.implement({
		slimbox: function(_options, linkMapper, linksFilter) {
			linkMapper = linkMapper || function(el) {
				return [el.href, (el.title?el.title:el.name)];
			};

			linksFilter = linksFilter || function() {
				return true;
			};

			var links = this;

			links.removeEvents("click").addEvent("click", function() {
				var filteredLinks = links.filter(linksFilter, this);
				return Slimbox.open(filteredLinks.map(linkMapper), filteredLinks.indexOf(this), _options);
			});

			return links;
		}
	});


	/*
		Internal functions
	*/

	function position() {
		overlay.setStyles({top: window.getScrollTop(), height: window.getHeight()});
	}

	function setup(open) {
		/*["object", window.ie ? "select" : "embed"].forEach(function(tag) {
			Array.forEach(document.getElementsByTagName(tag), function(el) {
				if (open) el._slimbox = el.style.visibility;
				el.style.visibility = open ? "hidden" : el._slimbox;
			});
		});*/

		overlay.style.display = open ? "" : "none";

		var fn = open ? "addEvent" : "removeEvent";
		window[fn]("scroll", position)[fn]("resize", position);
		document[fn]("keydown", keyDown);
	}

	function keyDown(event) {
		switch(event.code) {
			case 27:	// Esc
			case 88:	// 'x'
			case 67:	// 'c'
				close();
				break;
			case 37:	// Left arrow
			case 80:	// 'p'
				previous();
				break;	
			case 39:	// Right arrow
			case 78:	// 'n'
				next();
		}

		return false;
	}

	function previous() {
		return changeImage(prevImage);
	}

	function next() {
		return changeImage(nextImage);
	}

	function changeImage(imageIndex) {
		if ((state == 1) && (imageIndex >= 0)) {
			state = 2;
			activeImage = imageIndex;
			prevImage = ((activeImage || !options.loop) ? activeImage : images.length) - 1;
			nextImage = activeImage + 1;
			if (nextImage == images.length) nextImage = options.loop ? 0 : -1;

			$$(prevLink, nextLink, image, bottomContainer).setStyle("display", "none");
			fx.bottom.cancel().set(0);
			fx.image.set(0);
			center.className = "lbLoading";

			preload = new Image();
			preload.onload = nextEffect;
			preload.src = images[imageIndex][0];
		}

		return false;
	}

	function nextEffect() {
		switch (state++) {
			case 2:
				center.className = "";
				image.setStyles({backgroundImage: "url(" + images[activeImage][0] + ")", display: ""});
				$$(image, bottom).setStyle("width", preload.width);
				$$(image).setStyle("height", preload.height);
				//$$(image).setStyle("height", preload.height + 30);
				
				//captionHtml = '<a href="'+images[activeImage][0]+'" target="_blank" class="save">save</a>';
				captionHtml = images[activeImage][1] || "";
				
				$('lbSaveLink').set('href', images[activeImage][0]);
				$('lbSaveLink').set('target', '_blank');
				
				caption.set('html', (captionHtml ? '<span>' + captionHtml + '</span>' : ''));
				if(Cufon)
					Cufon.refresh();
					
				number.set('html', (options.showCounter && (images.length > 1)) ? options.counterText.replace(/{x}/, activeImage + 1).replace(/{y}/, images.length) : "");
				number.set('html', '');

				if (prevImage >= 0) preloadPrev.src = images[prevImage][0];
				if (nextImage >= 0) preloadNext.src = images[nextImage][0];

				if (center.clientHeight != image.offsetHeight) {
					fx.resize.start({height: image.offsetHeight});
					break;
				}
				state++;
			case 3:
				if (center.clientWidth != image.offsetWidth) {
					fx.resize.start({width: image.offsetWidth, marginLeft: -image.offsetWidth/2});
					break;
				}
				state++;
			case 4:
				bottomContainer.setStyles({top: top + center.clientHeight, marginLeft: center.style.marginLeft, visibility: "hidden", display: ""});
				fx.image.start(1);
				break;
			case 5:
				if (prevImage >= 0) prevLink.style.display = "";
				if (nextImage >= 0) nextLink.style.display = "";
				if (options.animateCaption) {
					fx.bottom.set(-bottom.offsetHeight).start(0);
				}
				bottomContainer.style.visibility = "";
				state = 1;
		}
	}

	function close() {
		if (state) {
			state = 0;
			preload.onload = $empty;
			for (var f in fx) fx[f].cancel();
			$$(center, bottomContainer).setStyle("display", "none");
			fx.overlay.chain(setup).start(0);
		}

		return false;
	}

})();

Slimbox.scanPage = function() {
	var links = $$("a").filter(function(el) {
		return el.rel && el.rel.test(/^lightbox/i);
	});
	$$(links).slimbox({}, null, function(el) {
		return (this == el) || ((this.rel.length > 8) && (this.rel == el.rel));
	});
};
window.addEvent("domready", Slimbox.scanPage);
