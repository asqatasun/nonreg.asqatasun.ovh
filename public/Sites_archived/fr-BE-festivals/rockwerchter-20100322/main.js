﻿/**
*	Javascript Tools based on jquery
*
*	@date		2009-02-12
*	@author		Michal Gondar
*	@copyright	Live Nation (Music) UK
*   @require    jquery
*   @jquery     jquery-1.3.1.min.js
*
*   This is main file with javascript outside class.
*
*   @author French
*   @date 2009-12-17
*   
*   Removed flash gallery refrerences  
*
*   
*/


/*
 *  check all input[type=text] elements,
 *  remember default value
 *  make onFocus, onBlur functions
 *  // make focus on first found input
 */
$(document).ready(function() {
    $("input[type=text]").focus(function() {
        if( this.defVal == null ) {
            this.defVal = $(this).val();
        }
        if( this.value == this.defVal ) {
            this.value = "";
        }
    });
    $("input[type=text]").blur(function() {
        if( this.value == "" ) {
            this.value = this.defVal;
        }
    });

    /* select first input box */
/*
    if( $("input[type=text]")[0] ) {
        $("input[type=text]")[0].focus();
    }
*/
});



/*
 *  Sitemap
 *  show / hide sitemap
 */
$(document).ready(function() {
    $("li.sitemap a").click(function () {
        $("#sitemap").toggle("slow");
        return false;
    });
});


/* A-Z bands select */
/* makes option clickable */
$(document).ready(function() {
    $("#azbands select").change(function () {
        if( this.value ) {
            window.location = "artist.aspx?aid=" + this.value;
        }
    });
});

/*
 *  Prevent to have opened more than 1 footer box
 *
 */
 
function hideOthersFooterBoxes(what) {

    if (what != "#sendToAfriend iframe") {
        $("#sendToAfriend iframe:visible").toggle("slow");
        $("a#sendToAfriendButton").removeClass("selected");
    }
    if (what != "#copyright div") {
        $("#copyright div:visible").toggle("slow");
        $("a#copyrightButton").removeClass("selected");
    }
}

/*
 *  Send To A Friend footer button
 *  show / hide sendToAfriend
 */
$(document).ready(function() {

    $("a#sendToAfriendButton").click(function() {
    
        // hide others
        hideOthersFooterBoxes("#sendToAfriend iframe");
        
        // function to make flash size if flash gallery si opened
        $("#sendToAfriend iframe").toggle("slow");

        $("a#sendToAfriendButton").toggleClass("selected");
        return false;
    });
});


/*
 *  Copyright footer button
 *  show / hide copyright
 */
$(document).ready(function() {
    $("a#copyrightButton").click(function () {
        
        // hide others
        hideOthersFooterBoxes( "#copyright div" );
        // function to make flash size if flash gallery si opened
        $("#copyright div").toggle("slow");
        
        $("a#copyrightButton").toggleClass("selected");
        return false;
    });
});


/*
 *  Background images
 */
$(document).ready(function() {

    // repair background img in IE 6
	if(jQuery.browser.msie && parseInt(jQuery.browser.version)<7){
        $("#backgroundImage").css("position", "absolute");
        $("#backgroundImage").css("height", document.documentElement.clientHeight + "px");
        $(window).resize(function(){
            $("#backgroundImage").css("height", document.documentElement.clientHeight + "px");
        });
		$(window).scroll(function(){
		    $("#backgroundImage").css("top", document.documentElement.scrollTop + "px");
		});
    }

    $("div.ooooo a").each(function(i){
        this.imgID = i + 1;
    });
 
    $("div.ooooo a").click(function () {
        $("#backgroundImage img").attr("src", "/_Resources/img/background/background" + this.imgID +  ".jpg");
        $("div.ooooo a").removeClass("selected");
        $(this).addClass("selected");
        $.cookie( "mainBackgroundImage", this.imgID, {path: '/', expires: 10} );
        return false;
    });

    // load last used image from cookie
    var last = $.cookie("mainBackgroundImage");
    if( last ) {
        $("#backgroundImage img").attr("src", "/_Resources/img/background/background" + last +  ".jpg");
        $("div.ooooo a").removeClass("selected");
        $("div.ooooo a").eq(last - 1).addClass("selected");
    }
});

$(document).ready(function() {
    // only runs if the twitter column is available
    if ($("#twitterFeed ul").length > 0) {
        $.getJSON("http://search.twitter.com/search.json?callback=?&rpp=6&q=%23rw10", function(data) {
        $("#twitterFeed ul").html("");
            if (data.results.length > 0) {
                $.each(data.results, function(i, item) {
                    // finds all @'s, hastags and links and converts to markup
                    tweet = item.text.replace(/http:\/\/\S+/g, '<a href="$&" target="_blank">$&</a>')
                        .replace(/(@)(\w+)/g, ' <a href="http://twitter.com/$2" target="_blank">@$2</a>')
                        .replace(/(#)(\w+)/g, ' <a href="http://search.twitter.com/search?q=%23$2" target="_blank">#$2</a>');

                    $("#twitterFeed ul").append("<li><a href='http://www.twitter.com/" + item.from_user + "' title='' class='redText'>" + item.from_user + ":</a> " + tweet + "</li>");
                });
            } else {
                $("#twitterFeed ul").append("<li class='redText'>no tweets to show...</li>");
            }
        });
    }
});