/*PAR DEFAULT, l'unit� des images est en PIXEL*/
function SlideShow(vitesse, directions, photos, html_conteneur)
{
	this.animManager = new AnimationManager( vitesse, this );
	this.current_slide = 0;
	this.slides = [];
	this.directions = directions;
	this.imageContainer = html_conteneur;
	this.images = photos;
	this.animManager.on_finished = function(parent)
	{
		//alert ("9: DEBUT animationManager.on_finished @current_slide:"+parent.current_slide+"@slides:"+parent.slides.length);
		/*var i=1000;
		while (i> parent.slides.length-1)
		{	
			i=Math.round(Math.random()*100);
		}*/
		parent.current_slide++;
		if ( parent.current_slide >= parent.slides.length )
			parent.current_slide = 0;
		//alert ("10: FIN animationManager.on_finished @current_slide:"+parent.current_slide);
		parent.slides[ parent.current_slide ].start();
		//parent.slides[ i ].start();
	}
	
	
	
}
function rnd ( start, end )
{
  return ( Math.random() * ( end - start ) ) + start;
}


SlideShow.prototype.load_slides = function ()
{
	//alert ("11: DEBUT SlideShow.prototype.load_slides@current_slide:"+this.current_slide);
  //var ic = document.getElementById( 'imgContainer' );
  var ic = document.getElementById( this.imageContainer );  
  //alert ("nom conteneur:" + imageContainer);
  for( var i in this.images )
  {
    var img = this.images[i];
	//var nameId="image"+parseInt(Math.round(Math.random()*1000));
    //var imgObj = document.createElement( 'div' );
	//var nameId="image"+parseInt(Math.round(Math.random()*1000));
	//imgObj.id = nameId;
	var imgObj = document.createElement( 'img' );
	ic.appendChild( imgObj );	
	//imgObj = document.getElementById(nameId);
    imgObj.style.position = 'absolute';
    imgObj.style.left = '0em';
    imgObj.style.top = '0em';
    imgObj.style.visibility = 'hidden';
	imgObj.style.backgroundRepeat="no-repeat";
	//alert (imgObj.style.visibility);
    //ic.appendChild( imgObj );
	//alert ("Image Info:"+"Width:"+img.width+"height:"+img.height+"");
    var ii = new ImageInfo( img.src, img.width, img.height, imgObj );
	
  var szoom = rnd( 50, 100 );
  var ezoom = rnd( 70, 120 );

  var d = parseInt( ( Math.random() * this.directions.length ).toString() );
  var di = this.directions[ d ];
  var sx = rnd( di.sx[0], di.sx[1] );
  var sy = rnd( di.sy[0], di.sy[1] );
  var ex = rnd( di.ex[0], di.ex[1] );
  var ey = rnd( di.ey[0], di.ey[1] );

    /*g_slides.push( 
      new Animation( g_animationManager, ii, 10,
    [ new KenBurnsZoomer( ii, szoom, ezoom, ic.clientWidth, ic.clientHeight ),
      new KenBurnsMover( ii, sx, sy, ex, ey, ic.clientWidth, ic.clientHeight ),
          new KenBurnsFader( ii, 30 ) ] )
    );*/
	this.slides.push( 
      new Animation( this.animManager, ii, 10,
    [ new KenBurnsFader( ii, 20 ) ] )
    );
  }
  //alert ("12: FIN SlideShow.prototype.load_slides@current_slide:"+this.current_slide);
}

SlideShow.prototype.start_slides = function ()
{
	//alert ("8:fonction start_slide@current_slide:"+this.current_slide);
  this.slides[ this.current_slide ].start();
}
