function ImageInfo( src, width, height, htmlObj )
{
  this.src = src;
  this.width = width;
  this.height = height;
  this.current_width = width;
  this.current_height = height;

  this.htmlObj = htmlObj;
  /*this.htmlObj.style.backgroundImage="url(\""+this.src+"\")";
  this.htmlObj.style.backgroundRepeat="no-repeat";
  this.htmlObj.style.width = this.current_width+"em";
  this.htmlObj.style.height = this.current_height+"em";*/
  this.htmlObj.src=this.src;
  this.htmlObj.style.width = this.current_width+"em";
  this.htmlObj.style.height = this.current_height+"em";
  /*this.htmlObj.width = "100%";
  this.htmlObj.height = "100%";*/
}

ImageInfo.prototype.set_opacity = function( opacity )
{
  this.htmlObj.style.MozOpacity = opacity / 100;
  this.htmlObj.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(opacity='+opacity+')';
}

ImageInfo.prototype.set_position = function( x, y )
{
  this.htmlObj.style.left = x+'em';
  this.htmlObj.style.top = y+'em';
}

ImageInfo.prototype.set_size = function( w, h )
{
  this.current_width = w;
  this.current_height = h;
  
  this.htmlObj.width = this.current_width;
  this.htmlObj.height = this.current_height;
}

ImageInfo.prototype.get_image = function()
{
  return this.htmlObj;
}

ImageInfo.prototype.hide = function()
{
  this.htmlObj.style.visibility = 'hidden';
}

ImageInfo.prototype.show = function()
{
  this.htmlObj.style.visibility = 'visible';
}
