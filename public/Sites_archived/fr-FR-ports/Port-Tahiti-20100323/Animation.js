function Animation( am, img, seconds, effects )
{
  this.img = img;
  this.animationManager = am;
  this.seconds = seconds;
  this.effects = effects;
  this.startMS = 0;
}

Animation.prototype.start = function()
{
  this.animationManager.add( this );
  this.startMS = 0;

  this.img.hide();
  for( var e in this.effects )
  {
    this.effects[e].apply( 0 );
  }
  this.img.show();
}

Animation.prototype.animate = function()
{
  var d = new Date();
  if ( this.startMS == 0 )
    this.startMS = d.valueOf();

  var p = (((d.valueOf()-this.startMS)/1000)/this.seconds)*100;
  for( var e in this.effects )
    this.effects[e].apply( p );
}

Animation.prototype.done = function()
{
  var d = new Date();
  return ( ( d.valueOf() - this.startMS ) / 1000 ) > this.seconds;
}

function AnimationManager( speed, parent )
{
   this.animations = [];
   this.parent = parent;
   var self = this;
   window.setInterval( function() { self.idle(); }, speed );
}

AnimationManager.prototype.add = function( anim )
{
  this.animations.push( anim );
}

AnimationManager.prototype.idle = function()
{
  if ( this.animations.length > 0 )
  {
    this.animations[0].animate();
    if ( this.animations[0].done() )
      this.animations.shift();
    if ( this.animations.length == 0 )
      this.on_finished(this.parent);
  }
}

AnimationManager.prototype.on_finished = function()
{
}
