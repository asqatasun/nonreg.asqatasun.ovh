////////////////////////////// Browser-Check ////////////////////////////////
function checkBrowser(){
  this.ver=navigator.appVersion
  this.agent=navigator.userAgent
  this.dom=document.getElementById?1:0
  this.opera5=this.agent.indexOf("Opera 5")>-1
  this.opera6=this.agent.indexOf("Opera 6")>-1
  this.opera7=this.agent.indexOf("Opera 7")>-1
  this.ie5=(this.ver.indexOf("MSIE 5")>-1 && this.dom && !this.opera5)?1:0; 
  this.ie6=(this.ver.indexOf("MSIE 6")>-1 && this.dom && !this.opera5)?1:0;
  this.ie7=(this.ver.indexOf("MSIE 7")>-1 && this.dom && !this.opera5)?1:0;
  this.ie8=(this.ver.indexOf("MSIE 8")>-1 && this.dom && !this.opera5)?1:0;
  this.ie4=(document.all && !this.dom && !this.opera5)?1:0;
  this.ie=this.ie4||this.ie5||this.ie6||this.ie7||this.ie8
  this.mac=this.agent.indexOf("Mac")>-1
  this.safari=(this.ver.indexOf("Safari")>-1 && this.dom && this.mac && !this.opera5)?1:0;
  this.ns6=(this.dom && parseInt(this.ver) >= 5) ?1:0; 
  this.ns4=(document.layers && !this.dom)?1:0;
  this.bw=(this.ie8||this.ie7||this.ie6||this.ie5||this.ie4||this.ns4||this.ns6||this.opera5||this.opera6||this.opera7||this.safari)
	return this
}
var bw = new checkBrowser()
//In most cases you want to check if the user is using a supported browser in this step
//and send them to a page saying "sorry you need a 4.x+ browser to see this page" or similar
//Just uncomment the next line and place in the url to your "sorry" page:
//if(!bw.bw) alert("Sorry you need a 4.x+ browser to see this page correctly.") //location.href='sorry.html'

////////////////////////////// Stylesheet Installation ////////////////////////////////
var stylePath = 'http://' + location.hostname + '/css/';
function installStyleSheets() {
	if (bw.mac) {document.write('<link href="' + stylePath + 'mac.css" rel="StyleSheet" type="text/css">');}
	else {
		if (bw.ie) {document.write('<link href="' + stylePath + 'ie.css" rel="StyleSheet" type="text/css">');}
		else {document.write('<link href="' + stylePath + 'ns.css" rel="StyleSheet" type="text/css">');}
	}
}
if (bw.bw) installStyleSheets();

// NetscapeStylesheets Bug Begin
// store window size for Netscape resize fix
if (bw.ns4){
	origWidth = innerWidth;
	origHeight = innerHeight;
	onresize = netscapeResizeFix;
}
var myWindowResizetime = 400;
if(!window.saveInnerWidth) {
	window.onresize = netscapeResizeFix;
	window.saveInnerWidth = window.innerWidth;
	window.saveInnerHeight = window.innerHeight;
}
function netscapeResizeFix(){
	if(bw.ns4){
		if (saveInnerWidth != window.innerWidth || saveInnerHeight != window.innerHeight) {
			location.reload(); //window.history.go(0);
			saveInnerWidth = window.innerWidth;
			saveInnerHeight = window.innerHeight;
		}
	}
	if  (bw.ie || bw.dom) {
		if (typeof (origWidth) != "number" || typeof (origHeight) != "number") return;
		if (document.body.clientWidth != origWidth || document.body.clientHeight != origHeight) location.reload();
		myWindowResize=setTimeout("netscapeResizeFix()", myWindowResizetime);
	}
}
function ieWindowSize(){
	if  (bw.ie || bw.dom) {
		onresize = netscapeResizeFix;
		origWidth = document.body.clientWidth;
		origHeight = document.body.clientHeight;
	}
}
// NetscapeStylesheets Bug End


// Check the frameset begin
// NEVER TOUCH THIS FUNCTION!!!
function checkHomeBase(url) {
	var localName = 'saint-pierre-oleron';
	var frameName = 'pierre';
	if(location.hostname != localName) {
		var home = 'http://'+location.hostname;
		if(this.name != frameName) { top.location.href=home+url; }
	}
}
// Check the frameset end


//Divers begin
function openGallery(num,folder) { startImg=num; newWindow = window.open('../_gallery/gallery.php?folder='+folder, 'gallery','width=428,height=324,toolbar=no,location=no,directories=no,menubar=no,resizable=no,scrollbars=no'); }

//Divers end

// Rollover + Layer show/hide begin
//Usage: onload="loadImages('path1/image1','path2/image2', ....);"
function loadImages() {
  var d=document; if(d.images){ if(!d.p) d.p=new Array();
    var i,j=d.p.length,a=loadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.p[j]=new Image; d.p[j++].src=a[i];}}
}
//Usage: onmouseout="swapImgRestore();" (no params)
function swapImgRestore() {
  var i,x,a=document.sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
//Usage: onmouseover="swapImage('image_name','','imagepath/over_image',1);"
function swapImage() {
  var i,j=0,x,a=swapImage.arguments; document.sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=findObj(a[i]))!=null){document.sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//Usage for show layer: onmouseover="showHideLayers('layername','','show');"
//Usage for hide layer: onmouseout="showHideLayers('layername','','hide');"
function showHideLayers() {
  var i,p,v,obj,args=showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}
//For internal use only - don't use it
function findObj(n, d) {
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}
// Rollover + Layer show/hide end

//Open Window Script
function openBrWindow(theURL,winName,features) {
  window.open(theURL,winName,features);
}
