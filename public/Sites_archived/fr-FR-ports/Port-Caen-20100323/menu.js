function montreg(id, titre) {
	ligne = document.getElementById(id);
	liste = ligne.parentNode;
	elements = liste.childNodes;
	
	for( i=0; i<elements.length; i++ ){
		if( elements[i])
		if( elements[i].id && elements[i].id != 'undefined' ){
			if( elements[i].id.match(/sous_menug_/) ){
				cache( elements[i].id );
			}
		}else if( elements[i].nodeType == 1 ){
			elements[i].firstChild.style.color = '#030303';
		}
	}
	if( ligne.hasChildNodes() ){
		ligne.style.display="block";
	}
	titre.firstChild.style.color = '#030303';
	
}

function montre(id, test){
	document.getElementById(id).style.display="block";
	document.getElementById(test).style.backgroundImage="url(/layout/img/bt_menu_on.gif)";
	document.getElementById(test).style.backgroundRepeat="repeat-x";
	document.getElementById(test).firstChild.style.color="#FFFFFF";

}

function cache(id,test) {
	document.getElementById(id).style.display="none";
	document.getElementById(test).style.backgroundImage="none";
	document.getElementById(test).firstChild.style.color="#0e457e";
}
