/**********************************
Copyright (c): Yves Dahan, 08/2003
e-mail: ygda.free.fr

Utilisation libre pour tout usage
sous r�serve du maintien de la
pr�sente mention de copyright.
***********************************/
function ScrollDiv(id){

	var elt=document.getElementById(id);
  var w=elt.offsetWidth;
  var s=elt.parentNode.offsetWidth;
	if(!w || !s || s>w){
		this.scroll=Function(""); return;}
	var divStyle=elt.style;
	var offset=0;
	var maxOffset=s-w;
	var timer=null;
	var self=this;
 
	this.scroll= function(dir,speed){
	var coef=1, S=speed;
		while(S--)coef*=2;
		if (timer)window.clearTimeout(timer);
		switch (dir){
			case 's': //stop
				break;
			case 'l': //left
				if(offset>maxOffset){
					offset-=coef;
					divStyle.left=Math.min(offset,0)+"px";
					timer=window.setTimeout(function(){self.scroll('l',speed)},25)}
				break;
			case 'r': //right
				if(0>offset){
					offset+=coef;
					divStyle.left=Math.max(offset,maxOffset)+"px";
					timer=window.setTimeout(function(){self.scroll('r',speed)},25)}
		}
	}
}
// Assignement des �v�nements clavier (code 573xx pour Opera)
onload=function(){
	document.onkeydown=function(e){
		e=e||event;
		myDiv.scroll('s');
		switch(e.keyCode){
			//fl�ches gauche et haut
			case 40:
			case 57386:
			case 57388:
			case 39: myDiv.scroll('l',2);break;
			//PgUp
			case 57384:
			case 34: myDiv.scroll('l',3);break;
			//Home
			case 57382:
			case 35: myDiv.scroll('l',4);break;
			//fl�ches droit et bas
			case 38:
			case 57385:
			case 57387:
			case 37: myDiv.scroll('r',2);break;
			//pgDown
			case 57383:
			case 33: myDiv.scroll('r',3);break;
			//End
			case 57381:
			case 36: myDiv.scroll('r',4);break;
		}
	}
	document.onkeyup=function(){myDiv.scroll('s');}
}

