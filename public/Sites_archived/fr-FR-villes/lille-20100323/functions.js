var msie5 = (navigator.userAgent.indexOf('MSIE 5') != -1);

//***********************************************************
// FCKEditor

var popup_editor_form = 'popup_fckeditor_form';
var width = 640;
var height = 520;

function popup_rte(input_id, label_edit) {
    var args, value;
    value = document.getElementById(input_id).value;
    args = '?input_id='+input_id+'&amp;label_edit='+escape(label_edit);
    str_window_features = 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=1,dependent=1,width=' + width + ',height=' + height;
    popup = window.open(popup_editor_form + args, input_id, str_window_features);
    if (!popup.opener) {
        popup.opener = window;
    }
    return false;
}


//************************************************************
// Folder content
var isSelected = false;

function toggleSelect(toggleSelectButton, selectAllText, deselectAllText) {
  formElements = toggleSelectButton.form.elements;

  if (isSelected) {
    for (i = 0; i < formElements.length; i++) {
      formElements[i].checked = false;
    }
    isSelected = false;
    toggleSelectButton.value = selectAllText;
  } else {
    for (i = 0; i < formElements.length; i++) {
      formElements[i].checked = true;
    }
    isSelected = true;
    toggleSelectButton.value = deselectAllText;
  }
}

//************************************************************
/**
 * Toggles an element's visibility.
 * Function to show tooltips.
 * If your element is a span, you must use inline display instead of block
 * XXX recognize div and span then automaticly choose the best display rule
 */
function toggleElementVisibility(id) {
  element = document.getElementById(id);
  if (element) {
    //window.alert(element.tagName)
    if (element.style.visibility == 'hidden') {
      element.style.visibility = 'visible';
      if (element.tagName == 'DIV') {
        element.style.display = 'block';
      } else if (element.tagName == 'SPAN') {
        element.style.display = 'inline';
      } else {
        element.style.display = 'block';
      }
    } else {
      element.style.visibility = 'hidden';
      element.style.display = 'none';
    }
  }
}

function showElement(show, id) {
  element = document.getElementById(id);
  if (element) {
    //window.alert(element.tagName)
    if (show) {
      element.style.visibility = 'visible';
      if (element.tagName in ['DIV', 'P']) {
        element.style.display = 'block';
      } else {
        element.style.display = 'inline';
      }
    } else {
      element.style.visibility = 'hidden';
      element.style.display = 'none';
    }
  }
}

//************************************************************
function trim(s) {
  if (s) {
    return s.replace(/^\s*|\s*$/g, "");
  }
  return "";
}

//************************************************************
function checkEmptySearch(formElem) {
  var query = trim(formElem.SearchableText.value);
  if (query != '') {
    formElem.SearchableText.value = query;
    return true;
  }
  formElem.SearchableText.value = query;
  formElem.SearchableText.focus();
  return false;
}

//************************************************************
/**
 * Sets focus on <input> elements that have a class attribute
 * containing the class 'focus'.
 * Examples:
 * <input type="text" id="username" name="__ac_name" class="focus"/>
 * <input type="text" id="searchableText" class="standalone giant focus"/>
 *
 * This function does not work on crappy MSIE5.0 and MSIE5.5.
 */
function setFocus() {
  if (msie5) {
    return false;
  }
  var elements = document.getElementsByTagName('input');
  for (var i = 0; i < elements.length; i++) {
    var nodeClass = elements[i].getAttributeNode('class');
    //alert("nodeClass = " + nodeClass);
    if (nodeClass) {
      var classes = nodeClass.value.split(' ');
      for (var j = 0; j < classes.length; j++) {
        if (classes[j] == 'focus') {
          elements[i].focus();
          return true;
        }
      }
    }
  }
}

/**
 * Validates that the input fields designated by the given ids are not empty.
 * It returns true if all the input fields are not empty, it returns false
 * otherwise.
 * Example:
 * <form onsubmit="return validateRequiredFields(['field1', 'field2'],
 * ['Title', 'Comments'], 'are empty while they are required fields.')">
 */
function validateRequiredFields(fieldIds, fieldLabels, informationText) {
  for (i = 0; i < fieldIds.length; i++) {
    element = document.getElementById(fieldIds[i]);
    if (element && !element.value) {
      window.alert("'" + fieldLabels[i] + "' " + informationText);
      return false;
    }
  }
  return true;
}

//************************************************************
function getSelectedRadio(buttonGroup) {
  if (buttonGroup[0]) {
    for (var i=0; i<buttonGroup.length; i++) {
      if (buttonGroup[i].checked) {
        return i
      }
    }
  } else {
    if (buttonGroup.checked) { return 0; }
  }
  return -1;
}

function getSelectedRadioValue(buttonGroup) {
  var i = getSelectedRadio(buttonGroup);
  if (i == -1) {
    return "";
  } else {
    if (buttonGroup[i]) {
    return buttonGroup[i].value;
    } else {
    return buttonGroup.value;
    }
  }
}

function getSelectedRadioId(buttonGroup) {
  var i = getSelectedRadio(buttonGroup);
  if (i == -1) {
    return "";
  } else {
    if (buttonGroup[i]) {
      return buttonGroup[i].id;
    } else {
      return buttonGroup.id;
    }
  }
}

/**
 * Return the label content corresponding to a radio selection
 */
function getSelectedRadioLabel(buttonGroup) {
  var id = getSelectedRadioId(buttonGroup);
  if (id == "") {
    return "";
  } else {
    for (var i=0; i<document.getElementsByTagName("label").length; i++) {
      var element_label = document.getElementsByTagName("label")[i];
      if (element_label.htmlFor == id) {
        return element_label.firstChild.nodeValue;
      }
    }
  }
}


//************************************************************
/*
    Used to highlight search terms
    from Geir B�kholt, adapted for CPS

 */
function highlightSearchTerm() {
  var query_elem = document.getElementById('searchGadget')
  if (! query_elem){
    return false
  }
  var query = query_elem.value
  // _robert_ ie 5 does not have decodeURI
  if (typeof decodeURI != 'undefined'){
    query = unescape(decodeURI(query)) // thanks, Casper
  }
  else {
    return false
  }
  if (query){
    queries = query.replace(/\+/g,' ').split(/\s+/)
    // make sure we start the right place and not higlight menuitems or breadcrumb
    searchresultnode = document.getElementById('searchResults')
    if (searchresultnode) {
      for (q=0;q<queries.length;q++) {
        // don't highlight reserved catalog search terms
        if (queries[q].toLowerCase() != 'not'
          && queries[q].toLowerCase() != 'and'
          && queries[q].toLowerCase() != 'or') {
          climb(searchresultnode,queries[q]);
        }
      }
    }
  }
}

function climb(node, word){
  // traverse childnodes
  if (! node){
    return false
  }
  if (node.hasChildNodes) {
    var i;
    for (i=0;i<node.childNodes.length;i++) {
      climb(node.childNodes[i],word);
    }
    if (node.nodeType == 3) {
      checkforhighlight(node, word);
      // check all textnodes. Feels inefficient, but works
    }
  }
}

function checkforhighlight(node,word) {
  ind = node.nodeValue.toLowerCase().indexOf(word.toLowerCase())
  if (ind != -1) {
    if (node.parentNode.className != "highlightedSearchTerm"){
      par = node.parentNode;
      contents = node.nodeValue;
      // make 3 shiny new nodes
      hiword = document.createElement("span");
      hiword.className = "highlightedSearchTerm";
      hiword.appendChild(document.createTextNode(contents.substr(ind,word.length)));
      par.insertBefore(document.createTextNode(contents.substr(0,ind)),node);
      par.insertBefore(hiword,node);
      par.insertBefore(document.createTextNode( contents.substr(ind+word.length)),node);
      par.removeChild(node);
    }
  }
}

/************************************************************
/**
 * searchLanguage widget functions
 * used to auto select languages checkbox/radio
 * cf CPSSchemas/skins/cps_schemas/widget_searchlanguage_render.pt
 */
function searchLanguageCheckSelected(languages, no_language, language) {
  var count=0;
  for (var i=0; i<languages.length; i++) {
    if (languages[i].checked) {
      count++;
    }
  }
  no_language.checked = (count <= 0);
  language.checked = (count > 0);
}

function searchLanguageClearSelected(languages) {
  for (i=0; i<languages.length; i++) {
    languages[i].checked = 0;
  }
}

function toggleLayers(more_block, more_items) {
  var objMoreBlock = document.getElementById(more_block).style;
  var objMoreItems = document.getElementById(more_items).style;
  if(objMoreBlock.display == "block")
    objMoreBlock.display = "none";
  if(objMoreItems.display == "none")
    objMoreItems.display = "block";
}

//-----------------------------------------------------------------------------
//Funtion previewPrint
//-----------------------------------------------------------------------------
function previewPrint()
{
if (navigator.appName  != 'Microsoft Internet Explorer') {
window.print();
}
else
{
document.body.insertAdjacentHTML('beforeEnd', '<OBJECT ID="nav" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></OBJECT>');
nav.ExecWB(7,1);
nav.outerHTML = "";
}
}


//-----------------------------------------------------------------------------
//Function rolloverMap
//-----------------------------------------------------------------------------
function rollOverMap(mapName, mapOver){
	document[mapName].src=mapOver;
}

//-----------------------------------------------------------------------------
//Function font control
//-----------------------------------------------------------------------------
function fontCtrl(action){

	if (!document.body.style.fontSize) { 
        document.body.style.fontSize = '100%';
    }
    else if(document.body.style.fontSize=='10px'){
        document.body.style.fontSize = '65%';
    }

	if (action=='increase'){
		document.body.style.fontSize = parseInt(document.body.style.fontSize)+10+'%';
	}
	else if (action =='decrease'){
		document.body.style.fontSize = parseInt(document.body.style.fontSize)-10+'%';
	}
	else {
		alert("unknown action !");
	}
	return false
}


pad = function(int) {
    return int=(int<10)?"0"+int:int
}

function buildCal(m, y, cM, cH, cDW, cD, brdr, lang, root_section_url, over_day){
    var mn_en=['January','February','March','April','May','June','July','August','September','October','November','December'];
    var mn_fr=['Janvier','F�vrier','Mars','Avril','Mai','Juin','Juillet','Ao�t','Septembre','Octobre','Novembre','D�cembre'];
    var previous_month_fr = 'Voir le calendrier du mois pr�c�dent'
    var next_month_fr = 'Voir le calendrier du mois suivant'
    var see_events_fr = 'Voir les �v�nements du '
    var previous_month_en = 'See the previous month calendar'
    var next_month_en = 'See the next month calendar'
    var see_events_en = 'See events of the '
    var mn = mn_fr
    var previous_month = previous_month_fr
    var next_month =  next_month_fr
    var see_events =  see_events_fr
    var dim=[31,0,31,30,31,30,31,31,30,31,30,31];
    var day = "LMMJVSD"

    if(lang == 'en'){
        mn = mn_en
        day = "MTWTFSS"
        previous_month = previous_month_en
        next_month =  next_month_en
        see_events =  see_events_en
    }

    var oD = new Date(y, m-1, 1); //DD replaced line to fix date bug when current day is 31st
    oD.od=oD.getDay(); //DD replaced line to fix date bug when current day is 31st
    if (oD.od == 0) oD.od=7
    var todaydate=new Date() //DD added
    var scanfortoday=(y==todaydate.getFullYear() && m==todaydate.getMonth()+1)? todaydate.getDate() : 0 //DD added
    var scanforoverday=

    dim[1]=(((oD.getFullYear()%100!=0)&&(oD.getFullYear()%4==0))||(oD.getFullYear()%400==0))?29:28;
    var t='<table summary="Calendrier mensuel"><caption>Calendrier</caption>';
    t+='<thead>';
    t+='<tr>';
    t+='<td><a id="moisPrecedent" href="#" title="' + previous_month +  '" onclick="javascript:rewriteCalendar(' + (m - 1) +', ' + y + ', \'' + cM + '\', \'' + cH + '\', \'' + cDW + '\', \'' + cD + '\', ' + brdr + ', \'' + lang + '\', \'' + root_section_url + '\', \'' + over_day + '\');return false;"><span class="noDisplay">Mois pr�c�dent</span></a></td>';
    t+='<th scope="col" colspan="5">' + mn[m-1].toUpperCase() + ' ' + oD.getFullYear() + '</th>';
    t+='<td><a id="moisSuivant" href="#" title="' + next_month +  '" onclick="javascript:rewriteCalendar(' + (m + 1) +', ' + y + ', \'' + cM + '\', \'' + cH + '\', \'' + cDW + '\', \'' + cD + '\', ' + brdr + ', \'' + lang + '\', \''+ root_section_url + '\', \'' + over_day + '\');return false;"><span class="noDisplay">Mois suivant</span></a></td>';
    t+='</tr>';
    t+='</thead>';
    t+='<tbody>';
    t+='<tr id="jours">';
    for(s=0;s<7;s++)t+='<td class="'+cDW+'">'+day.substr(s,1)+'</td>';
    t+='</tr><tr>';

    for(i=1;i<=42;i++){
        var x=((i-oD.od>=0)&&(i-oD.od<dim[m-1]))? i-oD.od + 1 : '&nbsp;';
        var class_a = ''
        var str_date = pad(i-oD.od+1) +'/'+pad(m)+'/'+oD.getFullYear();
        if (x==scanfortoday){
            //x='<span id="today">'+x+'</span>';} //DD added
            class_a = 'class="today"';
        }
        if (str_date==over_day){
            class_a = 'class="highlight"';
        }
        if (x != '&nbsp;'){
            t+='<td><a href="'+root_section_url+'/events_search?startDate=' + str_date + '&endDate=' + str_date + '&over_day=' + str_date + '&f=" ' + class_a + ' title="' + see_events + (i-oD.od+1) + ' ' + mn[m-1] + '">'+x+'</a></td>';
        }else{
            t+='<td>'+x+'</td>';
        }
        if(((i)%7==0)&&(i<36))t+='</tr><tr>';
    }
    return t+='</tr></tbody></table>';
}


function rewriteCalendar(m, y, cM, cH, cDW, cD, brdr, lang, root_section_url, over_day){
    if (m < 1){
        m =12;
        y = y - 1;
    }
    else if (m > 12){
        m =1;
        y = y + 1;
    }
    document.getElementById('calendarspace').innerHTML = buildCal(m ,y, cM, cH, cDW, cD, brdr, lang, root_section_url, over_day)
}


function writeCalendar(year, lang, root_section_url, over_day){
    var todaydate = new Date()
    var curmonth = todaydate.getMonth()+1 //get current month (1-12)
    var curyear = todaydate.getFullYear() //get current year
    if (year != ''){
        curyear = year;
    }
    if (over_day != ''){
        var reg = new RegExp("/", "g");
        var tmp = over_day.split(reg);
        curmonth = Number(tmp[1]);
        curyear = Number(tmp[2]);
    }
    return buildCal(curmonth, curyear, 'main', 'month', 'daysofweek', 'days', 1, lang, root_section_url, over_day)
}

function writeTodayCalendar(lang, root_section_url, over_day){
    var todaydate=new Date()
    var curmonth=todaydate.getMonth()+1 //get current month (1-12)
    var curyear=todaydate.getFullYear() //get current year
    if (over_day != ''){
        var reg = new RegExp("/", "g");
        var tmp = over_day.split(reg);
        curmonth = Number(tmp[1]);
        curyear = Number(tmp[2]);
    }
    return buildCal(curmonth, curyear, 'main', 'month', 'daysofweek', 'days', 1, lang, root_section_url, over_day)
}

function centerPopup(url,name,width,height,options) {
� � var top = (screen.height-height)/2;
� � var left = (screen.width-width)/2;
� � popup = window.open(url,name,"top="+ top +",left="+ left
+",width="+ width +",height="+ height +","+options);
    popup.focus();
}

function show_theme_selection(){
	ts = document.getElementById('theme_selection');
	ts.style.display="block";
}
