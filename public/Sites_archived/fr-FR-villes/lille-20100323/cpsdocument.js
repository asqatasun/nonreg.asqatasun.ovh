var CPSDocumentEditor = Class.create();

CPSDocumentEditor.prototype = {
  initialize: function(form_id, form_action, rendered_id, buttons,
                       reloadbuttons, title_id, translations, last_error,
                       blockers) {
    // if any blocker is found, we don't ajaxify
    if (blockers) {
      for (var i = 0; i < blockers.length; i += 1) {
        var blocker = blockers[i];
        if ($(blocker)) {
          this.enabled = false;
          return;
        }
      }
    }
    this.form_id = form_id;
    this.blockers = blockers;
    this.effects = Effect;
    this.rendered_id = rendered_id;
    this.title_id = title_id;
    this.last_error = last_error;
    this.sender = null;
    this.translations = translations;

    var edit_form = $(form_id);
    var rendered_node = $(rendered_id);
    if (!edit_form || !rendered_node) {
      this.enabled = false;
    }
    else {

      // getting the action value
      this.edit_form = edit_form;

      if (!this.canSendForm()) {
        this.enabled = false;
      }
      else
      {
        this.rendered_node = rendered_node;
        this.buttons = buttons;
        this.reloadbuttons = reloadbuttons;
        this.form_action = form_action;
        // linking action to buttons
        var count = 0;
        if (buttons)
        {
            for (var i = 0; i < buttons.length; i += 1) {
            var button = buttons[i];
            var button_node = $(button);
            if (button_node) {
                button_node.type = 'button';
                button_node.onclick = this.sendForm.bindAsEventListener(this);
                count += 1;
            }
            }
        }
        if (reloadbuttons)
        {
            for (var i = 0; i < reloadbuttons.length; i += 1) {
            var button = reloadbuttons[i];
            var button_node = $(button);
            if (button_node) {
                button_node.type = 'button';
                button_node.onclick = this.sendFormReload.bindAsEventListener(this);
                count += 1;
            }
            }
        }
        if (count>0) {
            this.enabled = true;
            edit_form.action = ''; // disabling direct sending
        }
        else {
            this.enabled = false;
        }
      }
    }
  },

  onCreate: function() {
    if ($('ajax_psm')) {
      var elt = $('ajax_psm');
      elt.innerHTML = this.translations.working;
      Effect.Appear('ajax_psm', {duration:1, queue:'end'});
    }
  },

  onComplete: function() {
    if ($('ajax_psm')) {
      Effect.Fade('ajax_psm', {duration:1.5, queue:'end'});
    }
  },

  sendForm: function(evt) {
    if (this.enabled) {
      this.onCreate();
      // let's serialize the content of the form
      // and send it to the server
      params = Form.serialize(this.edit_form);
      if (params!='') {
        params += '&ajax_edit=1';
      }
      var sendFormCompletedFn = this.sendFormCompleted.bind(this);
      var options = {parameters: params, onComplete: sendFormCompletedFn};
      if (!this.sender) {
        this.sender = new Ajax.Request(this.form_action, options);
      }
      else {
        this.sender.setOptions(options);
        this.sender.request(this.form_action);
      }
    }
  },

  sendFormReload: function(evt) {
    if (this.enabled) {
      this.onCreate();
      // let's serialize the content of the form
      // and send it to the server
      params = Form.serialize(this.edit_form);
      if (params!='') {
        params += '&cpsdocument_edit_and_view_button=1';
        params += '&ajax_edit=1';
      }
      var sendFormCompletedFn = this.sendFormCompleted.bind(this);
      var options = {parameters: params, onComplete: sendFormCompletedFn};
      if (!this.sender) {
        this.sender = new Ajax.Request(this.form_action, options);
      }
      else {
        this.sender.setOptions(options);
        this.sender.request(this.form_action);
      }
    }
  },

  sendFormCompleted: function(originalRequest) {
    var result = originalRequest.responseXML;

    // result comes in three node, under ajax-response
    result = result.childNodes[0];
    var action = '';
    var layout = '';
    var form_result = false;

    for (var i = 0; i < result.childNodes.length; i++) {
      var node = result.childNodes[i];
      if (node.nodeName=='result') {
        if (node.childNodes[0].nodeValue=='True')
          form_result = true;
      }
      if (node.nodeName=='layout') {
        layout = node.childNodes[0].nodeValue;
      }
      if (node.nodeName=='action') {
        if (node.childNodes.length>0) {
          action = node.childNodes[0].nodeValue;
        }
      }
    }

    if (form_result) {
      psm = this.translations.content_changed;
      if (this.last_error)
        this.rendered_node.innerHTML = layout;
    }
    else {
      psm = this.translations.content_error;
      // need to reload form here
      this.rendered_node.innerHTML = layout;
      // we also want to scroll to the first error
      this.scrollTo('error');
    }

    // feedback, in any case
    feedback = $('ajax_psm');
    if (feedback) {
     feedback.innerHTML = psm;
    }

    // reload elements
    if (!action) {
      this.initialize(this.form_id, this.form_action, this.rendered_id,
                      this.buttons, this.reloadbuttons, this.title_id,
                      this.translations, !form_result, this.blockers);
      if (form_result)
        this.reloadTitleFromForm();

    }
    else {
      this.gotoUrl(action);
    }
    this.onComplete();
  },

  gotoUrl: function(dest) {
    self.location.href = dest;
  },

  canSendForm: function() {
    // trying to see if there are files in the form
    var inputs = this.edit_form.getElementsByTagName('input');
    for (var i = 0; i < inputs.length; i++) {
      var input = inputs[i];
      // if (input.type == 'file' && input.value != '') {
      if (input.type == 'file') {
        return false;
      }
    }
    return true;
  },

  reloadTitleFromForm: function() {
    if ($('widget__Title')) {
        this.reloadTitle($('widget__Title').value);
      }
  },

  reloadTitle: function(new_title) {
    // reloading title
    title_node = $(this.title_id);
    if (title_node)
      title_node.innerHTML = new_title;
    document.title = new_title;
  },

  scrollTo: function(classname) {
    elements = document.getElementsByClassName(classname, document);
    if (elements.length>0) {
      var element = elements[0];
      var x = element.x ? element.x : element.offsetLeft,
          y = element.y ? element.y : element.offsetTop;
      window.scrollTo(x, y);
    }
  }
}

/*--------------------------------------------------------------------------*/

if (!window.CPSFlexibleEdit) {
  var CPSFlexibleEdit = new Object();
  }

Object.extend(CPSFlexibleEdit, {
  buttonClick: function(button, message) {
  /*
  this function will simulate a click on a submit button, by setting a hidden
  field with the name/value of the clicked button and then submitting the form
  */
    if (message) {
      if (window.confirm(message))
        flag = true;
    }
    else
      flag = true;
    if (flag) {
      buttonName = button.name;
      buttonValue = button.value;
      myform = button.form;
      placeholder = document.getElementById('button_placeholder');
      if (document.all) { // what follows should work
                          // with Opera/IE but doesn't in Firefox
        placeholder.name = buttonName;
        placeholder.value = buttonValue;
      }
      else if (document.getElementById) { // so here is the
                                          // Firefox workaround
        placeholder.setAttribute('name', buttonName);
        placeholder.setAttribute('value', buttonValue);
      }
      myform.submit();
    }
  }
});

