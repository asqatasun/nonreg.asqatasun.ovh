/* BREVE DEROULANTES (MARQUEE) */
var end = 0;
var compteur = 0;
var stopstart = true;
var tm = false;
function initscroll(divid, speed){
	var t = document.getElementById(divid);
	compteur = t.parentNode.offsetWidth;
	end = t.offsetWidth;
	end = end-(end*2);
	
	// arret du d�filement au survol de la souris
	t.onmouseover = function(){
	  window.clearTimeout(tm);
	}
	// reprise du d�filement apr�s survol de la souris
	t.onmouseout = function(){
    movescroll(divid, speed);
	}
	movescroll(divid, speed);
	t.style.visibility = 'visible';
}

function movescroll(divid, speed){
	// reprise du d�filement apr�s une boucle compl�te
	if(compteur==end){
		initscroll(divid, speed);
	}else{
		compteur--;
		// d�placement du texte
		document.getElementById(divid).style.left = compteur+"px";
		// lancement du programme
		tm = setTimeout("movescroll('"+divid+"',"+speed+")", speed);
	}
}
