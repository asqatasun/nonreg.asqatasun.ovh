var id_submenu = null;
var id_menu = null;
var timers = new Array();

function appear_submenu(e) {
	var el = Event.findElement(e, 'li');
	// aucun menu n'est ouvert
	if (el.hasClassName('submenu') == true && el.id != '' && id_submenu == null) {
		id_menu = el.id;
		id_submenu = 'sub_'+ el.id;
		if (id_submenu.search('_actif') < 0) $(id_submenu).toggle();
	}
	// menu d�j� ouvert, on repasse sur meme menu
	if (el.hasClassName('submenu') == true && el.id != '' && id_submenu != null && id_menu == el.id) {
		reset_timer();
	}
	// menu d�j� ouvert, on change de menu
	if (el.hasClassName('submenu') == true && el.id != '' && id_submenu != null && id_menu != el.id) {
		if (id_submenu.search('_actif') < 0) $(id_submenu).toggle();
		id_menu = el.id;
		id_submenu = 'sub_'+ el.id;
		if (id_submenu.search('_actif') < 0) $(id_submenu).toggle();
	}
}

function desappear_submenu(e) {
	timers.push(window.setTimeout('fermer()', 2500));
}

function fermer() {
	if (id_submenu.search('_actif') < 0) $(id_submenu).toggle();
	id_menu = null;
	id_submenu = null;
	reset_timer();
}

function reset_timer() {
	while (timers.length != 0) window.clearTimeout(timers.pop());
	timers.clear();
}