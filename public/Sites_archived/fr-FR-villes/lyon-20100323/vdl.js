function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function setFocus() {
   field = document.getElementById('field_focus')
   if (field) {
      field.focus();
   }
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function autoOpen(link) {
	h = 400; w = 700; t = 50; l = 50;
	x=window.open(link.href, 'popup', 'height='+h+', width='+w+', top='+t+', left='+l+', resizable=yes, status=yes');
	x.focus();
}	

// permet de ne d�clencher l action que sur la touche Enter 
function ouvreKeypress(KeyPressEvenement, url) {
    if(KeyPressEvenement.keyCode==13) {
        ouvre(url);
        return false;
    }
    return true;
}
function ouvre(pageok) {
	if (!pageok) {
		pageok = this.href;
	}
    x=window.open(pageok,"zoom",'width=583,height=450,top=5,left=5,toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,left=0,top=0');
    x.focus();
}

function ouvre_photo(pageok,titre,l,h) {
    x=window.open(pageok,"zoom",'width='+l+',height='+h+',top=5,left=5,toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,left=0,top=0');
    x.focus();
}


function ouvre_blank(pageok) {
	if (!pageok) {
		pageok = this.href;
	}
    x=window.open(pageok,"_blank",'width=800, height=600, top=0, left=0,toolbar=yes,location=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes');
    x.focus();
}

// permet de ne d�clencher l action que sur la touche Enter 
function ouvre2Keypress(KeyPressEvenement, url) {
    if(KeyPressEvenement.keyCode==13) {
        ouvre2(url);
        return false;
    }
    return true;
}
function ouvre2(pageok, id) {
	if (!pageok) {
		pageok = this.href;
	}
    x=window.open(pageok, id,'width=583,height=450,toolbar=no,location=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,left=20,top=20');
    x.focus();
}

// permet de ne d�clencher l action que sur la touche Enter 
function ouvre_imageKeypress(KeyPressEvenement, image, titre, l, h) {
    if(KeyPressEvenement.keyCode==13) {
        ouvre(image,titre,l,h);
        return false;
    }
    return true;
}
function ouvre_image(image,titre,l,h) {
    if (titre=="") {
          titre="Zoom";
    }
    w = window.open('/static/vide.html','zoom_image','toolbar=no,location=no,directories=no,menubar=yes,scrollbars=auto,resizable=yes,copyhistory=no,width='+l+',height='+h+',left=0,top=0');
    w.document.write("<HTML><HEAD><TITLE>"+titre+"</TITLE></HEAD>");
    w.document.write("<BODY leftMargin=0 topMargin=0 marginwidth=0 marginheight=0><IMG src='"+image+"' border=0></body>");
    w.document.write("</HTML>");
    w.document.close();
}

function ouvre_flash(flash, titre, l, h) {
    if (titre=="") {
          titre="Zoom";
    }
    w = window.open(titre,'zoom_flash','toolbar=no,location=no,directories=no,menubar=yes,scrollbars=auto,resizable=yes,copyhistory=no,width='+l+',height='+h+',left=0,top=0');
    w.document.write("<HTML><HEAD><TITLE>"+titre+"</TITLE></HEAD>");
    w.document.write("<BODY leftMargin=0 topMargin=0 marginwidth=0 marginheight=0>");
    w.document.write("<object width='" + l + "' height='" + h + "' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0' classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'>");
    w.document.write("<param value='" + flash + "' name='movie'/>");
    w.document.write("<param value='high' name='quality' />");
    w.document.write("<embed width='" + l + "' height='" + h + "' type='application/x-shockwave-flash' pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash' quality='high' src='" + flash + "' />");
    w.document.write("</object>");
    w.document.write("</BODY>");
    w.document.write("</HTML>");
    w.document.close();
}

// permet de ne d�clencher l action que sur la touche Enter 
function cartoKeypress(KeyPressEvenement, url) {
    if(KeyPressEvenement.keyCode==13) {
        carto(url);
        return false;
    }
    return true;
}
function carto(url) {
    x=window.open(url,"carto","toolbar=no,location=no,status=no,directories=no,menubar=yes,scrollbars=no,resizable=no,copyhistory=no,width=680,height=570,left=50,top=50");
    x.focus();
}


function deroule(x, h, l) {
	if (typeof(x)=='string') {
		x = MM_findObj(x);
	}
	x.style.zIndex=4;
	x.style.height=h;
	x.style.visibility='visible';
	if (l) {
	    x.style.width=l;   
	}
}



function enroule(x,h, l) {
	if (typeof(x)=='string') {
		x = MM_findObj(x);
	}
	x.style.zIndex=1;
	x.style.height=h;
	if (l) {
	    x.style.width=l;   
	}
}

function roArrdt(a) {
	MM_findObj('vdlcarte').src='/static/vdl/images/a'+a+'.gif'
}

function reArrdt() {
	MM_findObj('vdlcarte').src='/static/vdl/images/carte.gif'
}

//fonction de controle en fonction des types de champs

var lastFocus = null;

function controle(formulaire,champ,format,mini,maxi) {	
var  ok = 0;
var  RE="";
// N : num�rique
if ( format == "N" ) { RE = /^\d+$/;}
// CP : code postal fran�ais (5 chiffres)
if ( format == "CPT" ) { RE = /^\d{5}$/;}
//TEL  : telephone et telecopie
if ( format == "TEL" ) { RE = /^\d{10}$/;}
// EMAIL : email
if ( format == "EMAIL" ) { RE = /^[A-Za-z0-9\.\-_]+[@][A-Za-z0-9\-\.]+[\.][A-Za-z][A-Za-z][A-Za-z]?$/;}
var controle = eval('document.' + formulaire + '.' + champ);
message = '';
if (controle.value.length > 0) {
if (!RE.test(controle.value))
{
message = message+'Votre saisie est incorrecte.\n';
ok = 1; }}
if ( mini != 0 ) {
if (controle.value.length < mini ) {
message = message+'Vous devez saisir au moins ' + mini + ' caracteres.\n';
ok = 1;}
if ( maxi != 0 )
{
if (controle.value.length > maxi )
{
message = message+'Vous ne devez pas saisir plus de ' + maxi + ' caracteres.\n';
ok = 1;}}
if ( ok == 1 ) {
alert(message);
lastFocus = controle;
}}}


function getFocus() {
	if (lastFocus) {
		lastFocus.focus();
		lastFocus.select();
		lastFocus = null;
	}
}

// permet de ne d�clencher l action que sur la touche Enter 
function AjoutFavorisKeypress(KeyPressEvenement, theURL, theTITLE) {
    if(KeyPressEvenement.keyCode==13) {
      window.external.AddFavorite(theURL, theTITLE);
    }
}
function AjoutFavoris(theURL, theTITLE) {
  window.external.AddFavorite(theURL, theTITLE);
}


//Fonction spécifique Plan Interactif Ville
function ouvrir_mappy(liste_trouvee) {
  map_way = liste_trouvee.options[liste_trouvee.selectedIndex].value;
  iframe_rafraichir('plan_mappy', map_way);
}

function iframe_rafraichir(iframe_id, lien, rafraichir) {
  if (rafraichir != '') {
    parent.document.getElementById(iframe_id).src = lien;
  }
}

htmlareapopup = false;

function roll_over(x) {
  x.src=x.src.replace('off','over')
}

function roll_off(x) {
  x.src=x.src.replace('over','off')
}

// Pour menu
var to;
var currentMenu;
currentMenu="";
function montre(id) {
	clearTimeout(to);
	if (id=='') {
		tempo=500;
	}
	else {
		tempo=100;
	}
	to = setTimeout("immediateMontre(\""+id+"\")",tempo);
}

function findPosX(obj)
{
	var curleft = 0;
	if (obj.offsetParent)
	{
		while (obj.offsetParent)
		{
			curleft += obj.offsetLeft
			obj = obj.offsetParent;
		}
	}
	else if (obj.x)
		curleft += obj.x;
	return curleft;
}

function immediateMontre(id) {
	if (id!=currentMenu) {
		var d = document.getElementById(id);
		if (currentMenu!="") {
			document.getElementById(currentMenu).style.display='none';
		}
		if (d) {
			d.style.display='block';
			divmenu = document.getElementById('menu');
			x = findPosX(divmenu)+divmenu.offsetWidth - (d.offsetLeft+d.offsetWidth);
			if (x<=0) {
				var m = document.getElementById(id+'_title');
				left = findPosX(m) + m.offsetWidth - d.offsetWidth +1;
				d.style.left=left+'px';
			}
		}
		currentMenu=id;
	}
}

// permet de ne d�clencher l action que sur la touche Enter 
function locationKeypress(KeyPressEvenement, urlToOpen) {
    if(KeyPressEvenement.keyCode==13) {
        location.href = urlToOpen;
        return false;
    }
    return true;
}
function closeKeypress(KeyPressEvenement) {
    if(KeyPressEvenement.keyCode==13) {
        window.close();
    }
}
function printKeypress(KeyPressEvenement) {
    if(KeyPressEvenement.keyCode==13) {
        window.print();
    }
}


/* fake hover IE
***************************************************************************/
function hover(obj){

var pageOffset = document.getElementById('page').offsetLeft+document.getElementById('page').offsetWidth - 163;
if (document.getElementById('itemEnd')) {document.getElementById('itemEnd').style.left=pageOffset+"px";}
if (document.getElementById('itemNotReallyEnd')) {document.getElementById('itemNotReallyEnd').style.left=pageOffset-document.getElementById('tdEnd').offsetWidth+"px";}
 

  if(document.all){
    UL = obj.getElementsByTagName('ul');
    if(UL.length > 0){
      sousMenu = UL[0].style;
      if(sousMenu.display == 'none' || sousMenu.display == ''){
        sousMenu.display = 'block';
      }else{
        sousMenu.display = 'none';
      }
    }
  }
}

function setHover(id){
  LI = document.getElementById(id).getElementsByTagName('td');
  nLI = LI.length;
  for(i=0; i < nLI; i++){
    LI[i].onmouseover = function(){
      hover(this);
    }
    LI[i].onmouseout = function(){
      hover(this);
    }
  }
}


/* fake getElementsByName IE
***************************************************************************/
function getElementsByName_iefix(tag, name) {
    
     var elem = document.getElementsByTagName(tag);
     var arr = new Array();
     for(i = 0,iarr = 0; i < elem.length; i++) {
          att = elem[i].getAttribute("name");
          if(att == name) {
               arr[iarr] = elem[i];
               iarr++;
          }
     }
     return arr;
}


 function getElementsByClass(maClass){
	 var tabRetour = new Array();
	 var tabTmp = new Array();
	 tabTmp = document.getElementsByTagName("*");
	 j=0;
	 for (i=0; i<tabTmp.length; i++) {
		if (tabTmp[i].className==maClass){
			tabRetour[j]=tabTmp[i];
			j++;
		}
	}
	return tabRetour;
} 

function PopupImage(img) {
titre="Popup";
w=open("",'image','width=400,height=400,toolbar=no,scrollbars=no,resizable=no');
w.document.write("<HTML><HEAD><TITLE>"+titre+"</TITLE></HEAD>");
w.document.write("<SCRIPT language=javascript>function checksize() { if (document.images[0].complete) { window.resizeTo(document.images[0].width+10,document.images[0].height+45); window.focus();} else { setTimeout('checksize()',250) } }</"+"SCRIPT>");
w.document.write("<BODY onload='checksize()' onblur='window.close()' onclick='window.close()' leftMargin=0 topMargin=0 marginwidth=0 marginheight=0>");
w.document.write("<TABLE width='100%' border='0' cellspacing='0' cellpadding='0' height='100%'><TR>");
w.document.write("<TD valign='middle' align='center'><IMG src='"+img+"' border=0 alt=''>");
w.document.write("</TD></TR></TABLE>");
w.document.write("</BODY></HTML>");
w.document.close();
}

//************************************************************
// Folder content
var isSelected = false;

function toggleSelect(toggleSelectButton, selectAllText, deselectAllText) {
formElements = toggleSelectButton.form.elements;

if (isSelected) {
for (i = 0; i < formElements.length; i++) {
formElements[i].checked = false;
}
isSelected = false;
toggleSelectButton.value = selectAllText;
} else {
for (i = 0; i < formElements.length; i++) {
formElements[i].checked = true;
}
isSelected = true;
toggleSelectButton.value = deselectAllText;
}
}
