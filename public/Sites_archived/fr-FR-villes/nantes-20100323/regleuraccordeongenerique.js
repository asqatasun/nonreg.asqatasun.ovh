var defaultexpanded = [];

(function($){
	var $nav = $("#nav");
	/* NAVIGATION : INIT MENU */
	$("div.menutiroir", $nav)
		.each(function(i){
			$(this).addClass("menu" + (i + 1));
		})
	;
	/* NAVIGATION : INIT EXPANDED */
	var $activeElem = $("div.activepoignee", $nav);
	var $items = $("div.poigneetiroir", $nav);
	var index = $items.index($activeElem);
	if (index > -1){
		defaultexpanded[0] = index;
	}
})(jQuery);

ddaccordion.init({
	headerclass: "poigneetiroir", //Shared CSS class name of headers group
	contentclass: "menutiroir", //Shared CSS class name of contents group
	revealtype: "mouseover", //Reveal content when user clicks or onmouseover the header? Valid value: "click" or "mouseover
	mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
	collapseprev: true, //referme le contenu precedent (so only one open at any time)? true/false
	defaultexpanded: defaultexpanded, //index of content(s) open by default [index1, index2, etc] [] denotes no content 0 ouvre CULTURE, 1 ouvre DECOUVERTE etc
	onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
	animatedefault: false, //Should contents open by default be animated into view?
	persiststate: false, //persist state of opened contents within browser session? true: conserve le dernier over pendant la session
	toggleclass: ["", "selected"], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
	togglehtml: ["", "", ""], //Additional HTML added to the header when it's collapsed and expanded, respectively ["position", "html1", "html2"] (see docs)
	animatespeed: "slow", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
	oninit:function(headers, expandedindices){
		//custom code to run when headers have initalized
		//do nothing
	},
	onopenclose:function(header, index, state, isuseractivated){
		//custom code to run whenever a header is opened or closed
		//do nothing
		var nav = document.getElementById('nav');
		var content = document.getElementById('content');
	}
})