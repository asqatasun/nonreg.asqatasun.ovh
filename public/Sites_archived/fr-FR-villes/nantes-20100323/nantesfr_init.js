(function($){
	/* EXTRA : LIENS RAPIDES */
	var $container = $("#extra-liens");
	$("#extra-texte-plus", $container).click(function(){
		$("#doc").css("font-size", "111%");
		return false;
	});
	$("#extra-texte-moins", $container).click(function(){
		$("#doc").css("font-size", "82.73%");	
		return false;
	});
	$("#extra-imprimer", $container).click(function(){
		window.print();	
		return false;
	});
	$("img", $container)
		.each(function(i){
			$(this).css("padding-bottom", (i*2) + "px");
		})
	;
})(jQuery);
(function($){
	var mediaFolderPath = pathToStatic + "/flvplayer2/pi1/"
	$.fn.media.defaults.flvPlayer = mediaFolderPath + "mediaplayer.swf",
	$.fn.media.defaults.mp3Player = mediaFolderPath + "mediaplayer.swf"
	$('a.mediaMp3')
		.media({
			width: 230,
			height: 20
		})
	;
	$('a.mediaVideo')
		.media()
	;
})(jQuery);
(function($){
	$(".formGeneratorEdit")
	.before("<a href='#'>Afficher / cacher champs</a>")
	.prev()
		.click(function(){
			$(this)
				.next()
					.toggle()
				.end()
			;
		})
	.end()
;
	$(".formGeneratorBoEdit")
		.before("<a href='#'>Afficher / cacher meta</a>")
		.prev()
			.click(function(){
				$(this)
					.next()
						.toggle()
					.end()
				;
			})
		.end()
	;
})(jQuery);
(function($){
	$("div.csc-sitemap ul")
		.filter(function(){
			return $(this).children().length == 0;
		})
			.remove()
		.end()
	;
})(jQuery);
