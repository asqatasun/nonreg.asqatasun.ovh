// unobtrusive javascript

$(document).ready(function(){ 

	// Tabs Examples
	//$("#tabs").tabs();
	
	// Accordion Examples
	//$("#accordion").accordion();

	// Colorbox Examples
	//$("a[rel='galerie']").colorbox({transition:"elastic", contentCurrent:"{current} / {total}"});

	$('#slideshow').cycle({ 
		fx:     'fade', 
		timeout: 10000, 
		speed: 600
	});
	
}); // end dom ready