var msie5 = (navigator.userAgent.indexOf('MSIE 5') != -1);

//************************************************************
// Folder content
var isSelected = false;

function toggleSelect(toggleSelectButton, selectAllText, deselectAllText) {
formElements = toggleSelectButton.form.elements;

if (isSelected) {
for (i = 0; i < formElements.length; i++) {
formElements[i].checked = false;
}
isSelected = false;
toggleSelectButton.value = selectAllText;
} else {
for (i = 0; i < formElements.length; i++) {
formElements[i].checked = true;
}
isSelected = true;
toggleSelectButton.value = deselectAllText;
}
}

//************************************************************
/**
* Toggles an element's visibility.
* Function to show tooltips.
*/
function toggleElementVisibility(id) {
element = document.getElementById(id);
if (element) {
if (element.style.visibility == 'hidden') {
element.style.visibility = 'visible';
} else {
element.style.visibility = 'hidden';
}
}
}

function showElement(show, id) {
element = document.getElementById(id);
if (element) {
if (show) {
element.style.visibility = 'visible';
} else {
element.style.visibility = 'hidden';
}
}
}

//************************************************************
function trim(s) {
if (s) {
return s.replace(/^\s*|\s*$/g, "");
}
return "";
}

//************************************************************
function checkEmptySearch(formElem) {
var query = trim(formElem.SearchableText.value);
if (query != '') {
formElem.SearchableText.value = query;
return true;
}
formElem.SearchableText.value = query;
formElem.SearchableText.focus();
return false;
}

//************************************************************
/**
* Sets focus on <input> elements that have a class attribute
* containing the class 'focus'.
* Examples:
* <input type="text" id="username" name="__ac_name" class="focus"/>
* <input type="text" id="searchableText" class="standalone giant focus"/>
*
* This function does not work on crappy MSIE5.0 and MSIE5.5.
*/
function setFocus() {
if (msie5) {
return false;
}
var elements = document.getElementsByTagName('input');
for (var i = 0; i < elements.length; i++) {
var nodeClass = elements[i].getAttributeNode('class');
//alert("nodeClass = " + nodeClass);
if (nodeClass) {
var classes = nodeClass.value.split(' ');
for (var j = 0; j < classes.length; j++) {
if (classes[j] == 'focus') {
elements[i].focus();
return true;
}
}
}
}
}

function validateRequiredFields(fieldIds, fieldLabels, informationText) {
for (i = 0; i < fieldIds.length; i++) {
element = document.getElementById(fieldIds[i]);
if (element && !element.value) {
window.alert("'" + fieldLabels[i] + "' " + informationText);
return false;
}
}
return true;
}

//************************************************************
function getSelectedRadio(buttonGroup) {
if (buttonGroup[0]) {
for (var i=0; i<buttonGroup.length; i++) {
if (buttonGroup[i].checked) {
return i
}
}
} else {
if (buttonGroup.checked) { return 0; }
}
return -1;
}

function getSelectedRadioValue(buttonGroup) {
var i = getSelectedRadio(buttonGroup);
if (i == -1) {
return "";
} else {
if (buttonGroup[i]) {
return buttonGroup[i].value;
} else {
return buttonGroup.value;
}
}
}

function getSelectedRadioId(buttonGroup) {
var i = getSelectedRadio(buttonGroup);
if (i == -1) {
return "";
} else {
if (buttonGroup[i]) {
return buttonGroup[i].id;
} else {
return buttonGroup.id;
}
}
}
