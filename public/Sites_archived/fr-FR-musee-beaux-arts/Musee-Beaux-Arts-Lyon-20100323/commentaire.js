var commentaireLocked = false;

function commentaireInit() {		
    var commentaireRules = {
        'div.commentaire' : function(element) {
            element.onmouseover = commentaireShowActions;
            element.onmouseout = commentaireHideActions;
        }
    };
  
    	if ($('commentaire')) {
    
    		if (LireCookie('displayComment') == 'non') {
    			Element.hide('commentaireClose');
					Element.hide('commentaire');
				}
				else {
					Element.show('commentaireClose');
					Element.show('commentaire');
				}
			}
	
    
    Behaviour.register(commentaireRules);
    Behaviour.apply();
}

function commentaireShowActions() {
   Element.show('commentaire-actions');
   $('commentaire').style.backgroundColor = "White";
}

function commentaireHideActions() {
   if (!commentaireLocked) {
       Element.hide('commentaire-actions');
       $('commentaire').style.backgroundColor = "transparent";
   }
}
