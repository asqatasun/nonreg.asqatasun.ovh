
var myRules = {
    '.glossaire' : function(element) {
        element.onmouseover = function(){
                askGlossary(this);
        }
        element.onclick = function(){
            lockGlossary();
        }
        element.onmouseout = function(){
            hideGlossary(this.innerHTML);
        }
    }
};

Behaviour.register(myRules);

function lockGlossary() {
    Element.show('glossaryClose');
}

function askGlossary(item) {

	var content = document.getElementById("content");
  pContent = Position.cumulativeOffset(content);
  p = Position.cumulativeOffset(item);

  t1 =  p[1] - pContent[1];  
  text = item.innerHTML;
  var glossaryContent = document.getElementById("glossaryContent");
  glossaryContent.innerHTML = "Recherche de la définition de \""+text+"\"...";
  obj = document.getElementById("glossary");


	// 12 : hauteur du span .glossaire
	obj.style.display = 'block';
	obj.style.top = (parseInt(p[1]))+12+"px";
	
	
	yShift = parseInt(obj.style.width) - (parseInt(obj.style.width)/2);
	
	//alert(pContent[0]);
	
  obj.style.left = parseInt(p[0])-yShift+"px";
  obj.style.zIndex = 100;
  
	Element.show('glossary'); 
  my_url = "/mba/sections/displayGlossaryItems/";
  params = "title="+text;
  var myAjax = new Ajax.Request(
			my_url, 
			{
				method: 'get', 
				parameters: params,
				onComplete: displayGlossary
			});

}


function displayGlossary(request) {
	var obj = $('glossaryContent');
  obj.innerHTML = request.responseText;  

 	var g = obj.parentNode;
 	var currentPosition = Position.cumulativeOffset(g);
 	var currentTop = currentPosition[0];
 	var windowHeight=getWindowHeight();  
  
  var cumulContentHeight = currentTop+obj.offsetHeight;
  
  if (cumulContentHeight > windowHeight) {
  	g.style.top = (parseInt(g.style.top) - (obj.offsetHeight) - 12) + 'px';
  }

}
function hideGlossary(request) {
    if (!(Element.visible('glossaryClose'))) {
        Element.hide('glossary');
    }
}
