var pathname=location.pathname;
var myDomain=pathname.substring(0,pathname.lastIndexOf('/')) +'/';
var date_exp = new Date();
var adserver_test=true;
date_exp.setTime(date_exp.getTime()+(365*24*3600*1000));

var qs = new Querystring();
var id_lien=qs.get('id_op');
if (id_lien != null) SetCookie('id_lien', id_lien);
var regImage=qs.get('img');
if (regImage != null) SetCookie('regImage', regImage);

function Querystring(qs) { // optionally pass a querystring to parse
	this.params = new Object()
	this.get=Querystring_get
	
	if (qs == null)
		qs=location.search.substring(1,location.search.length)
	
	if (qs.length == 0) return

// Turn <plus> back to <space>
// See: http://www.w3.org/TR/REC-html40/interact/forms.html#h-17.13.4.1
	qs = qs.replace(/\+/g, ' ')
	var args = qs.split('&') // parse out name/value pairs separated via &
	
// split out each name=value pair
	for (var i=0;i<args.length;i++) {
		var value;
		var pair = args[i].split('=')
		var name = unescape(pair[0])

		if (pair.length == 2)
			value = unescape(pair[1])
		else
			value = name
		
		this.params[name] = value
	}
}

function Querystring_get(key, default_) {
	// This silly looking line changes UNDEFINED to NULL
	if (default_ == null) default_ = null;
	
	var value=this.params[key]
	if (value==null) value=default_;
	
	return value
}

function getCookieVal(offset) {
	var endstr=document.cookie.indexOf (";", offset);
	if (endstr==-1)
      	endstr=document.cookie.length;
	return unescape(document.cookie.substring(offset, endstr));
}
function GetCookie (name) {
	var arg=name+"=";
	var alen=arg.length;
	var clen=document.cookie.length;
	var i=0;
	while (i<clen) 
	{
		var j=i+alen;
		
		if (document.cookie.substring(i, j)==arg)
			return getCookieVal (j);
        i=document.cookie.indexOf(" ",i)+1;
		if (i==0) break;
	}
	return false;
}
function SetCookie (name, value, expireParam)
{
	var expires = new Date(); 
	if (expireParam)
		expires = expireParam;
	else
		expires.setTime(expires.getTime() + 99999999999); 
	var path="/";
	var domain=false;
	var secure=false;
	var curCookie = name + "=" + escape(value) + 
		((expires) ? "; expires=" + expires.toGMTString() : "") + 
		((path) ? "; path=" + path : "") + 
		((domain) ? "; domain=" + domain : "") + 
		((secure) ? "; secure" : ""); 
	 document.cookie = curCookie; 
}

function ClearCookie(nom)
{
	var date = new Date;
	var date_del = date.setFullYear(date.getFullYear() - 1000);
	document.cookie = nom + "=" + escape('') + "; expires=" + date_del;
}
