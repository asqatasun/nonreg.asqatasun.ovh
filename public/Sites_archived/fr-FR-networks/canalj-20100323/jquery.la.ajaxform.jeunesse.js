/**
 * AJAXFORM $.la.verifyParrainage(nom,prenom,mail,m1,m2,m3,idForm)
 * 
 * MUSILINE $.la.radio.popUpRadio() $.la.radio.lancePopUpRadio()
 * 
 * PROGRAMME $.la.programme.display_en_ce_moment()
 * $.la.programme.display_prochaine_diffusion()
 * 
 * MODALBOX $.la.modalbox.truc()
 */
(function($){

    $.la = $.la||{};
    $.extend(true,$.la,
    {
        /* AJAXFORM */
        ajaxForm:{
            /* propre à Jeunesse */
            settings:{
                /*****************************************/
                /** Setting for register/edit formulars **/
                /*****************************************/
                'modal_register_form':{
                    id_loading:'modal_register_loading',
                    /**
                     * Set step in siteaccess
                     * Ex:
                     * 
                     * step:[{
                     *   fields:['sexe','first_name','last_name','date_de_naissance','email','code_postal'],
                     *   success: function(){
                     *       jQuery.la.ajaxForm.gotoStep('modal_register_form',1)
                     *   }
                     * },
                     * {
                     *   fields:['login','password','confirmmotdepasse'],
                     *   success: function(){
                     *       jQuery.la.ajaxForm.gotoStep('modal_register_form',2)
                     *   }
                     * },
                     * {
                     *   fields:['newsletter_tiji'],
                     *   success: function(){
                     *       jQuery.la.ajaxForm.gotoStep('modal_register_form',3)
                     *   }
                     * },
                     * {
                     *   fields:['cgu'],
                     *   success: function(){
                     *       jQuery.la.ajaxForm.gotoStep('modal_register_form',4)
                     *   }
                     * }],
                     **/
                    messages: {
                    	sexe:"Gar&ccedil;on ou fille ?"//,
                    	/**
                    	 *  Overload in siteaccess for newsletter
                    	 *  Ex:
                    	 *  
                    	 *  newsletter_gulli: {
                    	 *  required: "choix obligatoire"
                    	 *  }
                    	 **/
                        
                    }
                },
                'full_register_form':{
                    id_loading:'full_register_loading',
                    rules: {
                        newsletter_gulli: "required",
                        newsletter_tiji: "required",
                        newsletter_canalj: "required",
                        newsletter_gulliland: "required",
                        offres_tiji: "required",
                        email_parent: {
                            //'validateEmailParent':true,
                            'email': true
                        },
                        portable: {
                            'validatePhone': true,
                            required: false
                        }
                    },
                    messages: {
                        newsletter_gulli: {
                            required: "choix obligatoire"
                        },
                        newsletter_tiji: {
                            required: "choix obligatoire"
                        },
                        newsletter_canalj: {
                            required: "choix obligatoire"
                        },
                        newsletter_gulliland: {
                            required: "choix obligatoire"
                        },
                        offres_tiji: {
                            required: "choix obligatoire"
                        },
                        email_parent: "email incorrect",
                        sexe:"Gar&ccedil;on ou fille ?"
                    }
                },
                
                'full_edit_form':{
                    id_loading:'full_edit_loading',
                    rules: {
                        newsletter_gulli: "required",
                        newsletter_tiji: "required",
                        newsletter_canalj: "required",
                        newsletter_gulliland: "required",
                        offres_tiji: "required",
                        email_parent: {
                            //'validateEmailParent':true,
                            'email': true
                        },
                        portable: {
                            'validatePhone': true,
                            required: false
                        }
                    },
                    messages: {
                        newsletter_gulli: {
                            required: "choix obligatoire"
                        },
                        newsletter_tiji: {
                            required: "choix obligatoire"
                        },
                        newsletter_canalj: {
                            required: "choix obligatoire"
                        },
                        newsletter_gulliland: {
                            required: "choix obligatoire"
                        },
                        offres_tiji: {
                            required: "choix obligatoire"
                        },
                        email_parent: "email incorrect",
                        sexe:"Gar&ccedil;on ou fille ?"
                    }                    
                },
                /*********************************/
                /** Setting for login formulars **/
                /*********************************/
                'modal_login_form':{
                    id_loading:'modal_login_loading'
                },
                'full_login_form':{
                    id_loading:'full_login_loading'
                },
                /***************************************/
                /** Setting for first visit formulars **/
                /***************************************/
                'modal_first_visit_form':{
                    id_loading:'modal_first_loading',
                    submit:{
                        'url':jQuery.la.prefixAjaxReturnUrl + '/action/register/Modal',
                        'handler': function(json){
                            if(json.res !=0 ){
                                if(json.return_url != ''){
                                    try {
                                	jQuery.la.modalbox.call( jQuery.la.prefixAjaxReturnUrl + json.return_url, {
                                        title: '',
                                        height: jQuery.la.ajaxForm.commonSettings.modalBox.register.height,
                                        width: jQuery.la.ajaxForm.commonSettings.modalBox.register.width
                                        });
                                    } catch(err) {
                                    	callModalBox(jQuery.la.prefixAjaxReturnUrl + json.return_url, {
                                            title: '',
                                            height: jQuery.la.ajaxForm.commonSettings.modalBox.register.height+20,
                                            width: jQuery.la.ajaxForm.commonSettings.modalBox.register.width+50
                                            });
                                    }	
                                }
                            }else{
                                jQuery.la.ajaxForm.hide_loader_picture(jQuery.la.ajaxForm.settings['modal_first_visit_form'].id_loading);
                                jQuery('#UserEmail').val(jQuery('#NewUserEmail').attr('value'));
                                jQuery.la.ajaxForm.alertForm('modal_first_visit_form',json.errors);
                            }
                        }
                    }
                },
                'full_first_visit_form':{
                    id_loading:'full_first_loading'
                },
                /*******************************************/
                /** Setting for forget password formulars **/
                /*******************************************/
                'modal_forget_password_form':{
                    id_loading:'modal_forgot_loading'
                },
                'full_forget_password_form':{
                    id_loading:'full_forgot_loading'
                },
                /**************************************/
                /** Setting for activation formulars **/
                /**************************************/
                'modal_activation_login_form':{
                    id_loading:'modal_activate_loading',
                    submit:{
                        'handler': function(json){
                			if(json.res != 0){
                				if(json.return_url == null) {
                            		if(window.location.search.indexOf('iframe=1') != -1){
                                        parent.location.href='/';
                                    }
                                    else{
                                    	location.reload(true);
                                    }
                            	} else if(json.return_url != ""){
                                    if(window.location.search.indexOf('iframe=1') != -1){
                                        parent.location.href=json.return_url;
                                    }
                                    else{
                                        document.location.href=json.return_url;
                                    }
                                }else{
                                    if(window.location.search.indexOf('iframe=1') != -1){
                                        parent.location.href='/';
                                    }
                                    else{
                                        document.location.href='/';
                                    }
                                }
                            }else{
                                jQuery.la.ajaxForm.hide_loader_picture(jQuery.la.ajaxForm.settings['modal_activation_login_form'].id_loading);
                                jQuery.la.ajaxForm.alertForm('modal_activation_login_form',1);
                            }
                        }
                    }
                },
                'full_activation_login_form':{
                    id_loading:'full_activate_loading',
                    submit:{
                        'handler': function(json){
                			if(json.res != 0){
                				if(json.return_url == null) {
                            		if(window.location.search.indexOf('iframe=1') != -1){
                                        parent.location.href='/';
                                    }
                                    else{
                                        document.location.href='/action/edit?account=activated';
                                    }
                            	} else if(json.return_url != ""){
                                    if(window.location.search.indexOf('iframe=1') != -1){
                                        parent.location.href=json.return_url;
                                    }
                                    else{
                                        document.location.href=json.return_url;
                                    }
                                }else{
                                    if(window.location.search.indexOf('iframe=1') != -1){
                                        parent.location.href='/';
                                    }
                                    else{
                                        document.location.href='/';
                                    }
                                }
                            }else{
                                jQuery.la.ajaxForm.hide_loader_picture(jQuery.la.ajaxForm.settings['full_activation_login_form'].id_loading);
                                jQuery.la.ajaxForm.alertForm('full_activation_login_form',1);
                            }
                        }
                    }
                }
            },
            getAlertForm:function(id_form,error){
                var message = "jQuery(\'#" + id_form + " .\' + jQuery.la.ajaxForm.settings[\'"+id_form+"\'].errorMessageClass).hide();";

                switch(id_form){
                    /***********/
                    /** Login **/
                    /***********/
                    case 'modal_login_form':
                        switch(error){
                            case 1:
                                message += "jQuery.la.modalbox.ldapOpenCloseLoginBoxRegistered(\'ldapLoginBoxForgetPassword\');";
                                message = 'Mot de passe oubli&eacute;? <a href="javascript:void(0);" onclick=" '+ message + '">clique ici</a> et nous te renverrons ton pseudo et ton mot de passe';
                                break;
                        }
                        break;
                    case 'full_login_form':
                        switch(error){
                            case 1:
                                message = 'Mot de passe oubli&eacute;? Indique ton adresse email dans le champs E-mail en bas du formulaire et nous te renverrons ton pseudo et ton mot de passe';
                                break;
                            case 100:
                                message = 'L\'adresse email existe prise. Veuillez en choisir une autre.';
                                break;
                        }
                        break;
                    /*****************/
                    /** First Visit **/
                    /*****************/
                    case 'modal_first_visit_form':
                        switch(error){
                            case 1:
                            	message += "jQuery.la.modalbox.ldapOpenCloseLoginBoxRegistered(\'ldapLoginBoxForgetPassword\');";
                                message = 'Mot de passe oubli&eacute;? <a href="javascript:void(0);" onclick=" '+ message + '">clique ici</a> puis sur "Envoyer" et nous te renverrons ton pseudo et ton mot de passe';
                                break;
                            case 2:
                                message = 'Tu es d&eacute;j&agrave; inscrit sur un de nos sites (www.tiji.fr, www.gulli.fr, www.canalj.fr). Tu peux utiliser les m&ecirc;mes identifiants, pseudo et mot de passe.';
                                break;
                            case 101:
                            	message = 'Il semblerait que ton adresse e-mail n\'existe pas. Nous ne pourrons pas t\'avertir si tu gagnes &agrave un de nos jeux concours si tu ne renseignes pas ton e-mail correctement.';
                            	break;
                        }
                        break;
                    case 'full_first_visit_form':
                        switch(error){
                            case 1:
                                message = 'Mot de passe oubli&eacute;? Clique sur "Envoyer" en bas du formulaire et nous te renverrons ton pseudo et ton mot de passe';
                                break;
                            case 2:
                                message = 'Tu es d&eacute;j&agrave; inscrit sur un de nos sites (www.tiji.fr, www.gulli.fr, www.canalj.fr). Tu peux utiliser les m&ecirc;mes identifiants, pseudo et mot de passe.';
                                break;
                            case 101:
                            	message = 'Il semblerait que ton adresse e-mail n\'existe pas. Nous ne pourrons pas t\'avertir si tu gagnes &agrave un de nos jeux concours si tu ne renseignes pas ton e-mail correctement.';
                            	break;
                        }
                        break;
                    /**************/
                    /** Activate **/
                    /**************/
                    case 'full_activation_login_form':
                        switch(error){
                            case 1:
                                message = 'Mot de passe oubli&eacute;? <a href="javascript:void(0);" onclick="javascript:document.location.href=\'\/action\/login\'">Clique ici</a> pour recevoir ton login et ton mot de passe.';
                                break;
                        }
                        break;
                    case 'modal_activation_login_form':
                        switch(error){
                            case 1:
                                message = 'Mot de passe oubli&eacute;? <a href="javascript:void(0);" onclick="jQuery.la.ajaxForm.getLoginModal();">Clique ici</a> puis remplis le champs E-mail dans le bloc "Code d\'acc&egrave;s oubli&eacute;s ?" pour recevoir ton login et ton mot de passe.';
                                break;
                        }
                        break;
                    /*********************/
                    /** Forgot Password **/
                    /*********************/
                    case 'modal_forget_password_form':
                        switch(error){
                            case 1:
                            	message = 'Un email va t\'&ecirc;tre envoy&eacute; sous peu';
                                break;
                            case 2:
                            	message += "jQuery.la.modalbox.ldapOpenCloseLoginBoxRegistered(\'ldapLoginBoxFirstVisit\');";
                            	message = 'Ton adresse n\'est pas enregistr&eacute;e dans notre base. <a href="javascript:void(0);" onclick=" '+ message + '">clique ici</a> puis sur "Inscription" pour t\'inscrire';
                                break;
                        }
                        break;
                    case 'full_forget_password_form':
                        switch(error){
                            case 1:
                                message = 'Un email va t\'&ecirc;tre envoy&eacute; sous peu';
                                break;
                            case 2:
                                message = 'Ton adresse n\'est pas enregistr&eacute;e dans notre base. Clique sur le bouton "Inscription" pour t\'inscrire';
                                break;
                        }
                        break;
                    /**************/
                    /** Register **/
                    /**************/
                    case 'modal_register_form':
                        switch(error){
                            case 1:
                                message = 'OK';
                                break;
                            case 2:
                                message = 'PAS OK';
                                break;
                            case 100:
                                message = 'L\'adresse email est d&eacute;j&agrave; prise. Veuillez en choisir une autre.';
                                break;


                        }
                        break;
                    case 'full_register_form':
                        switch(error){
                            case 1:
                                message = 'OK';
                                break;
                            case 2:
                                message = 'PAS OK';
                                break;
                            case 100:
                                message = 'L\'adresse email est d&eacute;j&agrave; prise. Veuillez en choisir une autre.';
                                break;
                        }
                        break;
                    /************/
                    /** Defaut **/
                    /************/   
                    default:message = 'default';
                }
                return message;
            },

            gotoStep:function(id_form,step){
                for (var i=0; i< step; i++){
                    for (var j=0; j<jQuery.la.ajaxForm.settings[id_form].step[i].fields.length; j++){
                        jQuery('[name=' + jQuery.la.ajaxForm.settings[id_form].step[i].fields[j]+ ']').attr('class','');
                    }
                }
                switch(id_form){
                   
                    case 'modal_register_form':{
                        switch(step) {
                            case 0:{
                                jQuery.la.modalbox.ldapOpenCloseLoginBoxRegistered('ldapRegisterBoxInfos');
                                break;
                            }
                            case 1:{
                                jQuery.la.modalbox.ldapOpenCloseLoginBoxRegistered('ldapRegisterBoxAccess');
                                break;
                            }
                            case 2:{
                                jQuery.la.modalbox.ldapOpenCloseLoginBoxRegistered('ldapRegisterBoxNewsletters');
                                break;
                            }
                            case 3:{
                                jQuery.la.modalbox.ldapOpenCloseLoginBoxRegistered('ldapRegisterBoxCgu');
                                break;
                            }
                            case 4:{
                                jQuery.la.ajaxForm.validateForm(id_form);
                                break;
                            }
                        }
                        break;
                    }
                }
            },
            
            getLoginModal:function(){
            	try {
                	jQuery.la.modalbox.call( jQuery.la.prefixAjaxReturnUrl + '/action/login/Modal', {
                        title: '',
                        height: jQuery.la.ajaxForm.commonSettings.modalBox.register.height,
                        width: jQuery.la.ajaxForm.commonSettings.modalBox.register.width
                        });
                    } catch(err) {
                    	callModalBox(jQuery.la.prefixAjaxReturnUrl + '/action/login/Modal', {
                            title: '',
                            height: jQuery.la.ajaxForm.commonSettings.modalBox.register.height+20,
                            width: jQuery.la.ajaxForm.commonSettings.modalBox.register.width+50
                            });
                    }
            }
        }
    /* FIN FORM */
    });
})(jQuery);