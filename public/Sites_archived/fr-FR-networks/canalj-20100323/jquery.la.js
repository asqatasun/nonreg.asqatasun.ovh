/**
 * VARS
 * $.la.sHost
 *
 * SLIDER
 * $.la.slider.getMaxScroll(id)
 * $.la.slider.getScroll(id,val)
 * $.la.slider.afficheScroll(id,id_contener)
 *
 * MODALBOX
 * $.la.modalbox.defaultOptions
 * $.la.modalbox.call(url, ModalBoxOptions, handlePub)
 * $.la.modalbox.close(handlePub)
 * $.la.modalbox.show(html,width,height)
 * $.la.modalbox.resize(params)
 * $.la.modalbox.accordion(obj,callback){
 * $.la.modalbox.ldapOpenCloseLoginBoxRegistered(id)
 *
 * ACTION
 * $.la.action.messages
 * $.la.action.settings
 * $.la.action.onUserLogged()
 * $.la.action.isLogged()
 * $.la.action.verifyAuthentification( loginUrl, formId )
 * $.la.action.getLoginBoxByCookie(LayerId, editUrl, forgotUrl, registerUrl, logoutUrl, loginUrl, BaseUrl, BaseImg )
 * $.la.action.verifyRules( loginUrl, formName )
 * $.la.action.loginUserBox(loginUrl, sLoginContent, idForm)
 * $.la.action.loginUser(LayerId, editUrl, forgotUrl, registerUrl, logoutUrl, loginUrl, BaseUrl, BaseImg, sLoginContent, idForm ,bSeSouvenir )
 * $.la.action.RefreshLoginBoxAvatar()
 * $.la.action.loginUserCommentaire(loginUrl,idForm, noeud)
 * $.la.action.addFavori( url ,loginUrl , formName, div)
 * 
 *
 * COREG
 * $.la.coreg.messages
 * $.la.coreg.rq_check_form(param, action)
 *
 * AJAX
 * $.la.ajax.updater(url,div)
 *
 * HFPVOTE
 * $.la.hfpvote.voteover(param,id)
 * $.la.hfpvote.switchDiv( divDisplay, divNone )
 * $.la.hfpvote.reload( iAverage, iWeight )
 * $.la.hfpvote.reloadCount(iCount,id)
 *
 * UTILS
 * $.la.utils.messages
 * $.la.utils.var_dump(v, recursif, indent)
 * $.la.utils.addFavorite()
 * $.la.utils.httpGet(key_str)
 * $.la.utils.isNumeric(val)
 * $.la.utils.isDate(date)
 * $.la.utils.verifMail(emailString)
 * $.la.utils.checknumber(atester)
 * $.la.utils.isMineur(sD,sM,sY)
 * $.la.utils.querystring_get(key, default_)
 * $.la.utils.querystring(qs)
 * $.la.utils.getTimeStamp()
 * $.la.utils.verifyUpload( value_image,value_video,type, formId )
 * $.la.utils.switchTab(div1,div2,classe)
 * $.la.utils.showHide(div1,div2)
 * $.la.utils.sortOptionFromSelect(select_id)
 * $.la.utils.escapeHtml(stringToEscape)
 * $.la.utils.trim(string)
 * $.la.utils.stripslashes(string)
 * $.la.utils.checkImageSrc(classe)
 * $.la.utils.str_split(string, split_length)
 * 
 * $.la.utils.list.add(aListArray, oElement, push) 
 * $.la.utils.list.alreadyIn(listArray, id)
 * $.la.utils.list.find(listArray, id)
 * $.la.utils.list.order(aListArray, bIsRandom)
 *
 * COOKIES
 * $.la.cookie.init()
 * $.la.cookie.getVal(offset)
 * $.la.cookie.get(name)
 * $.la.cookie.set(name, value, expireParam, domain)
 * $.la.cookie.clear(name)
 *
 * PROMO
 * $.la.promo.showSkin(params)
 * $.la.promo.localCss(params)
 * $.la.promo.localCssAfter(params)
 * $.la.promo.wreportRefresh()
 * $.la.promo.bannerRefresh(uri)
 * $.la.promo.adLoad(index)
 * $.la.promo.adsProcess()
 * $.fn.adRegister(id,defer)
 *** GOOGLE
 ***** ADSENSE
 * $.la.promo.google.adsense._createHtml.container(sId)
 * $.la.promo.google.adsense.execute()
 * $.la.promo.google.adsense.display(google_ads, google_info)
 * 
 * TRACKING
 * $.la.tracking.webo.init(iWRP_ID, sWRP_SECTION, sWRP_SUBSECTION, sWRP_CHANNEL,sWRP_CONTENT)
 * $.la.tracking.webo.track();
 *
 * GAME
 * $.la.game.verifchek(checkBoxName, objId, message)
 * $.la.game.verifOpenQuestion(id, objId, message)
 *
 * WEB
 * $.la.w.go(url)
 * $.la.w.decrypt(url)
 *
 */

try{
    if(typeof jQuery.la != 'object'){
        (function($){
            $.la = $.la||{};

            $.extend(true, $.la,
            {
                /* VARS */
                master:true,
                sHost:location.protocol + '//' + location.hostname,

                /* SLIDER */
                slider:
                {
                    /**
             * Recupere la difference entre la grandeur totale et la grandeur du div
             * @name $.la.slider.getMaxScroll(id)
             * @param id string Id du div à scroller
             * @return int Scroll max du slider
             */

                    getMaxScroll:function (id)
                    {
                        var maxScroll = $("#" + id).attr("scrollHeight") - $("#" + id).height();
                        return maxScroll;
                    },

                    /**
             * Recupere le pourcentage avec lequel le curseur bouge
             * @name $.la.slider.getScroll(id,val)
             * @param string id Id du div à scroller
             * @param int val deplacement du curseur en px
             * @return int % para rapport à la hauteur totale
             */
                    getScroll:function (id,val)
                    {
                        return 	parseInt((val/this.getMaxScroll(id))*100,'10');
                    },

                    /**
             * Affiche ou non la Scroll bar
             * @name $.la.slider.afficheScroll(id,id_contener)
             * @param string id Id du div la scrollbar
             * @param string id_contener Id du div a scroller
             * @return void
             */
                    afficheScroll:function(id,id_contener)
                    {
                        if (this.getMaxScroll(id_contener)>0)
                        {
                            $('#' + id).show();
                        }
                    }
                },

                /* MODALBOX */
                // REQUIRED : /extension/lajavascript/design/standard/javascript/jquery/thickbox/thickbox-compressed.js

                modalbox:{

                    //UTILISATION
                    /*
    			appel : jQuery.la.modalbox.call(url, {params});
    			Exemple: jQuery.la.modalbox.call('http://www.elle.fr', {title:'le titre de la modal', height: 400, width: 200, mode: 'iframe'});
                 */

                    //PARAMETRES
                    /*
               	title		 : Titre de la modalbox
    			width/height : Dimensions de la modal. Par defaut 500*500
    			mode		 : Type d'affichage.
    							   ajax (reponse HTML dans la modal) Par défaut 
    							   iframe (resultat HTML dans une iframe dans la modal)
    							   inline (affichage d'un div caché dans la modal /!\ Seul les enfants du div sont affich�s.
    								   OK : <div id="divID"><p>contenu affiche</p></div>
    								   Erreur : <div id="divID">contenu non affiche</div>
    			divID		 : Nom du DIV pour l'appel Inline
    			fixed		 : Desactive le clique sur le fond pour desactiver la modal
                 */

                    defaultOptions:{
                        /* A surcharger pour chaque site éventuellement */
                        iWidth: 500,
                        iHeight: 500,
                        sMode: '',
                        sFixed:'',
                        sTitle:''
                    },

                    call:function(url, ModalBoxOptions,handlePub){

                        // hack ie6
                        document.location.href = '#';
                        if(url.indexOf('/')!= 0){
                            if(url.substring(0,7) != 'http://' && url.substring(0,8) != 'https://'){
                                url = '/' + url;
                            }
                        }

                        var iWidth  = $.la.modalbox.defaultOptions.iWidth;
                        var iHeight = $.la.modalbox.defaultOptions.iHeight;
                        var sMode   = $.la.modalbox.defaultOptions.sMode;
                        var sFixed  = $.la.modalbox.defaultOptions.sFixed;
                        var sTitle  = $.la.modalbox.defaultOptions.sTitle;
                        var sParams;
                        if(url.indexOf('?')== -1){
                            sParams = url + '?';
                        }else{
                            sParams = url + '&';
                        }

                        if( typeof(ModalBoxOptions.title) != 'undefined' ) sTitle = ModalBoxOptions.title;
                        if( typeof(ModalBoxOptions.width) != 'undefined' ) iWidth = ModalBoxOptions.width;
                        if( typeof(ModalBoxOptions.height) != 'undefined' ) iHeight = ModalBoxOptions.height;
                        if( typeof(ModalBoxOptions.mode) != 'undefined')
                        {
                            if( ModalBoxOptions.mode == 'iframe' ) sMode = '&TB_iframe=true';
                            else if( ModalBoxOptions.mode == 'inline' ) sParams = '#TB_inline?inlineId=' + ModalBoxOptions.divID + '&';
                        }
                        if( typeof(ModalBoxOptions.fixed) != 'undefined' && ModalBoxOptions.fixed ) sFixed = '&modal=true';

                        sParams += 'height=' + iHeight + '&width=' + iWidth + sMode + sFixed;
				
                        /// OLD, plus nécessaire
                        // Pour l'appel en Iframe, il faut ajouter TB_iframe=1 dans l'url EN DERNIER
                        // exemple : http://www.gulli.fr/action/login/External?ReturnUrl=http://toto.com&TB_iframe=1

                        //gestion de la pub
                        //s' il y a une pub en fond, on la cache momentanément car certaines pubs en flash passent devant la box
                        if(handlePub){
                            if($(".pub").length>0){
                                $(".pub").hide();
                            }
                        }
				
                        //affichage de la box
                        tb_show( sTitle, sParams, false);
                    },

                    close: function(handlePub){
                        //gestion de la pub
                        //s' il y a une pub en fond, on la r�affiche
                        if(handlePub){
                            if($(".pub").length>0){
                                $(".pub").show();
                            }
                        }
                        //on cache la box
                        tb_remove();
                    },

                    show:function(html, width, height)
                    {
                        document.location.href = '#';
                        $("body").append("<div id='TB_overlay'></div><div id='TB_window'></div>");
                        $("#TB_overlay").click(tb_remove);
                        $("#TB_window").append("<div id='TB_ajaxContent' class='TB_modal' style='width:510px;height:300px;'></div>");
                        var TB_WIDTH = (width*1) + 30 ;
                        var TB_HEIGHT = (height*1) + 40	;
                        $("#TB_window").css({
                            marginLeft: '-' + parseInt((TB_WIDTH / 2),10) + 'px',
                            width: TB_WIDTH + 'px'
                        });

                        if ( !(jQuery.browser.msie && typeof XMLHttpRequest == 'function')) { // take away IE6
                            $("#TB_window").css({
                                marginTop: '-' + parseInt((TB_HEIGHT / 2),10) + 'px'
                            });
                        }

                        $("#TB_window").show();
                        $("#TB_ajaxContent").html(html);
                    },

                    /*
				 * ex : $.la.modalbox.resize({height:356});
				 */
                    resize:function(params){

                        if(typeof params.width !== 'undefined'){
                            var TB_WIDTH = (params.width*1) + 30 ;
                        }

                        if(typeof params.height !== 'undefined'){
                            var TB_HEIGHT = (params.height*1) + 40	;
                        }

                        if(typeof params.width !== 'undefined'){
                            $("#TB_window").css({
                                marginLeft: '-' + parseInt((TB_WIDTH / 2),10) +'px',
                                width: TB_WIDTH+'px'
                            });
                        }

                        if(typeof params.height !== 'undefined'){
                            if ( !(jQuery.browser.msie && typeof XMLHttpRequest == 'function')) { // take away IE6
                                $("#TB_window").animate({
                                    marginTop: '-' + parseInt((TB_HEIGHT / 2),10)
                                });
                            }
                            $("#TB_ajaxContent").animate({
                                height: params.height
                            });
                        }
                    },
            
                    /*
				 * onclick="$.la.modalbox.ldapOpenLoginBoxRegistered();
				 * 
				 */	
                    accordionSpeed: 'fast',
                    accordion: function(obj,callback){
                        // {'id1':false,'id2':true,etc}
                        var id;
                        for(id in obj) {
                            $('#' + id).slideUp($.la.modalbox.accordionSpeed);
                            $('#' + id).prev('h3').children('a').removeClass("opened");
                        }
                        for(id in obj) {
                            if(obj[id] == true){
                                $('#'+ id).slideDown($.la.modalbox.accordionSpeed);
                                $('#' + id).prev('h3').children('a').addClass("opened");
                            }
                        }
                    },
                    ldapOpenCloseLoginBoxRegistered:function(id){
                        // context : ldap, action : Open, quelle modal ? : LoginBox quel onglet ? : Registered
                        // h3 a + #ldapLoginBoxRegistered
                        var obj = {};
                        $("h3 + div").each(function(i){
                            var currentId = $(this).attr('id');
                            if(currentId !=''){
                                obj[currentId] = false;
                                if(currentId == id){
                                    if($('#' + id).css('display') == 'none'){
                                        obj[currentId] = true;
                                    }
                                }
                            }
                        });
                        $.la.modalbox.accordion(obj,null);
                    }
                },
        
                /* ACTION */
                action:{
                    messages:{
                        notLogged:'Nous n\'avons pas pu t\'identifier. Vérifie ton pseudo et ton mot de passe.',
                        bookmarkAdded:'Ce contenu a été ajouté aux favoris'
                    },

                    /* valeurs par défaut : à surcharger dans le siteaccess */
                    settings:{
                        loginBox:{
                            title:'Login',
                            width:800,
                            height:600
                        }
                    },

                    /* Callback appelé quand un user est loggué
                 * A définir dans la page concernée
                 */
                    onUserLogged:null,

                    isLogged: function(){
                        if($.la.cookie.get("user_logged") == "oui"){
                            return true;
                        }
                        return false;
                    },

                    verifyAuthentification: function ( loginUrl, formId, params )
                    {
                        if(typeof params === 'undefined'){
                            params = {};
                        }

                        $.extend($.la.action.settings.loginBox,params);
                        if($.la.action.isLogged())
                        {
                            if(typeof $.la.action.settings.loginBox.returnUrl != 'undefined'){
                                if(loginUrl.indexOf('iframe=1') != -1){
                                    parent.location=$.la.action.settings.loginBox.returnUrl;
                                }
                                else{
                                    document.location=$.la.action.settings.loginBox.returnUrl;
                                }
                            }
                            else{
                                $('#' + formId).submit();

                            }

                        }
                        else
                        {
                            $.la.action.baseIdForm = formId;
                            $.la.modalbox.call(loginUrl, $.la.action.settings.loginBox);
                        }
                    },
                    // à garder pour la compatibilté
                    verfifyAuthentification: function ( loginUrl, formId, params ){
                        this.verifyAuthentification( loginUrl, formId, params );
                    },

                    getLoginBoxByCookie: function(LayerId, editUrl, forgotUrl, registerUrl, logoutUrl, loginUrl, BaseUrl, BaseImg )
                    {
                    // dans le siteaccess
                    },

                    verifyRules: function( loginUrl, formName )
                    {
                    //siteaccess
                    },

                    loginUserBox: function(loginUrl, sLoginContent, idForm)
                    {
                        $.ajax({
                            url: loginUrl,
                            async: true,
                            type: 'POST',
                            data: $('#'+idForm).serialize(),
                            success:function(html){
                                if($.la.action.isLogged())
                                {
                                    $.la.action.getLoginBoxByCookie('identification','/action/edit','/action/forgotpassword','/action/register','/action/logout?ReturnUrl='+$.la.sHost, '/action/login','/','/design/gulli/images/');
                                    if($.la.action.baseIdForm.substring(0,10) == 'addFavoris')
                                    {
                                        $.ajax({
                                            url: $.la.sHost+'/action/bookmark',
                                            async: true,
                                            type: 'POST',
                                            data: $('#'+$.la.action.baseIdForm).serialize(),
                                            success:function(){
                                                alert($.la.action.messages.bookmarkAdded);
                                                $.la.modalbox.close();
                                            }
                                        });
                                    }
                                    else
                                    {
                                        $('#' + $.la.action.baseIdForm).submit();
                                        $.la.modalbox.close();
                                    }
                                }
                                else
                                {
                                    $('#TB_ajaxContent').html(html);
                                }
                            }
                        }
                        );
                    },

                    /* fonction par défaut : à surcharger dans le siteaccess */

                    loginUser:function(LayerId, editUrl, forgotUrl, registerUrl, logoutUrl, loginUrl, BaseUrl, BaseImg, sLoginContent, idForm ,bSeSouvenir ) {
                        if( typeof(idForm) == 'undefined' ){
                            idForm = 'mon_compte';
                        }
                        $('#'+LayerId).hide();
                        $.ajax({
                            url: loginUrl,
                            async: true,
                            type: 'POST',
                            data: $('#'+idForm).serialize(),
                            success:function(){
                                if($.la.action.isLogged()){
                                    $.la.action.getLoginBoxByCookie(LayerId, editUrl, forgotUrl, registerUrl, logoutUrl, loginUrl, BaseUrl, BaseImg );
                                    $('#'+LayerId).show();
                                } else {
                                    alert($.la.action.messages.notLogged);
                                    $('#'+LayerId).show();
                                    document.location.href = loginUrl;
                                }
                            }
                        });
                        if($.la.action.onUserLogged != null) {
                            $.la.action.onUserLogged();
                        }
                    },


                    /* Merci de corriger et de mettre un r minuscule refreshLoginBoxAvatar*/
                    RefreshLoginBoxAvatar: function() {
                    // siteaccess
                    },

                    loginUserCommentaire: function(loginUrl,idForm, noeud)
                    {
                        $.ajax({
                            url: loginUrl,
                            async: true,
                            type: 'POST',
                            data: $('#'+idForm).serialize(),
                            success:function(html)
                            {
                                if($.la.action.isLogged())
                                {
                                    $.la.action.getLoginBoxByCookie('identification','/action/edit','/action/forgotpassword','/action/register','/action/logout?ReturnUrl='+$.la.sHost, '/action/login','/','/design/gulli/images/');

                                    $.ajax({
                                        url: $.la.sHost+'/commanage/popup_comment/'+noeud,
                                        async: true,
                                        success:function(html_comment){
                                            $('#TB_ajaxContent').html(html_comment);
                                        }
                                    });
                                }
                                else
                                {
                                    $('#TB_ajaxContent').html(html);
                                }
                            }
                        });
                    },

                    addFavori: function( url ,loginUrl , formName, div)
                    {
                    //siteaccess
                    }

                },

                /* COREG */
                coreg:{
                    messages:{
                        completeForm: 'Veuillez compléter le formulaire\n pour valider votre participation'
                    },
                    rq_check_form: function(param, action){
                        var radio_flag = false;
                        var entry_radio = false;
                        var name_radio;
                        var complete_form = false;

                        var tabElement = document.getElementsByClassName('qualif_check');

                        $('.qualif_check').each(function(i){

                            if( $(this).attr('type') == 'radio' )
                            {
                                var obl_elt = $(this).attr('name') + '_obligatoire';
                                if ($('#' + obl_elt).val() == 1)
                                {
                                    var label_radio = $(this).attr('name') + '_label';
                                    if ( $('#oui_' + $(this).attr('name')).attr('checked') || $('#non_'+$(this).attr('name')).attr('checked'))
                                    {
                                        $('#' + label_radio).css('color','black') ;
                                    }
                                    else
                                    {
                                        $('#' + label_radio).css('color','red') ;
                                        complete_form=true;
                                    }
                                }
                            }
                            else
                            {
                        /* ASUIVRE...'
					    	var obl_elt = tabElement[i].name + '_obligatoire';
					    	alert(obl_elt);
					    	if (document.getElementById(obl_elt).value!=null && document.getElementById(obl_elt).value == 1)
					      	{
						    	var label_input = tabElement[i].name + '_label';
						    	if(tabElement[i].value=='')
						        {
								  	$(label_input).style.color='red' ;
								    complete_form=true;
						        }
						      	else
							    {
							      	$(label_input).style.color='black' ;
							    }
						    }*/
                        }

                        });
                    }
                },

                ajax:{
                    updater:function(url,div)
                    {
                        $.ajax({
                            url: url,
                            success:function(html){
                                $('#'+div).html(html);
                            }
                        });
                    }
                },

                /* HFP VOTE */
                /* jquery.la.hfpvote.js */
                hfpvote:{
                    voteover: function(param,id){
                        if(id){
                            $('#' + id).css('width', (19*param)+'px');
                        }
                        else{
                            $('#fdvote').css('width', (19*param)+'px');
                        }
                    },

                    switchDiv: function( divDisplay, divNone ){
                        $('#' + divDisplay).css('display', 'block');
                        $('#' + divNone).css('display','none');
                    },

                    reload: function( iAverage, iWeight ){
                        $.la.hfpvote.voteover( iAverage );
                        var arr =[];
                        for( var i = 0; i< iWeight; i++){
                            arr[i] = i+1;
                        }
                        $.each(arr, function(i){
                            $('#' + 'etoileLink' + arr[i]).mouseout(function(){
                                $.la.hfpvote.voteover( iAverage );
                            });
                        });
                    },

                    reloadCount: function( iCount, id ){
                        var newCount = iCount;
                        if(typeof newCount != "undefined"){

                            if (newCount>1){
                                $('#'+id).html('/ '+newCount+' votes');
                            }
                            else{
                                $('#'+id).html('/ '+newCount+' vote');
                            }
                        }
                    }
                },

                /* UTILS */
                utils:{
                    messages:{
                        fileNeeded: 'Il manque le fichier !',
                        notAVideoFile : 'Ce n\' est pas une vidéo'
                    },
                
                    debug:function(text) {
                        if (window.console && window.console.log){
                            window.console.log(text);
                        }
                    },

                    debugConsole:function(text) {
                        if (typeof text == 'object') {
                            $.la.utils.debug($.la.utils.var_dump(text, true, 1));
                        }
                        else {
                            var today=new Date();
                            var h=today.getHours();
                            var m=today.getMinutes();
                            var s=today.getSeconds();
                            if (typeof text == 'string') {
                                $.la.utils.debug(h + ':' + m + ':' + s + ' - ' + text);
                            }
                            else {
                                $.la.utils.debug(h + ':' + m + ':' + s);
                                $.la.utils.debug(text);
                            }
                        }
                    },
                
                    var_dump: function(v, recursif, indent){
                        recursif = typeof recursif == 'undefined'?false:true;
                        indent = typeof indent == 'undefined'?0:indent;
                        res = '';
                        for(i in v){
                            for(var j=0; j < indent;j++){
                                res += '>> ';
                            }
                            if(recursif && typeof v[i] == 'object'){
                                res +=  i + ':\n' + this.var_dump(v[i], recursif, indent + 1)+ '\n';
                            }
                            else{
                                res += i + ' : '+ v[i] + '\n';
                            }
                        }
                        if(!indent){
                            alert(res);
                        }
                        else{
                            return res;
                        }
                        return res;
                    },

                    addFavorite:function ()
                    {
                        var url = window.location;
                        var titre = document.title;
                        if (window.sidebar){
                            window.sidebar.addPanel(titre,url,'');
                        }
                        else{
                            window.external.AddFavorite(url,titre);
                        }
                        return false;
                    },

                    httpGet:function(key_str)
                    {
                        if(window.location.search) {
                            var query = window.location.search.substr(1);
                            var pairs = query.split("&");
                            for(var i = 0; i < pairs.length; i++) {
                                var pair = pairs[i].split("=");
                                if(unescape(pair[0]) == key_str){
                                    return unescape(pair[1]);
                                }
                            }
                        }
                        return '';
                    },

                    getKey:function(key_str, str)
                    {
                        if(str != '') {
                            var query = str.substr(1);
                            var pairs = query.split("&");
                            for(var i = 0; i < pairs.length; i++) {
                                var pair = pairs[i].split("=");
                                if(unescape(pair[0]) == key_str){
                                    return unescape(pair[1]);
                                }
                            }
                        }
                        return '';
                    },

                    isNumeric:function(val){
                        var exp = new RegExp("^[0-9]+$","g");
                        return exp.test(val);
                    },
            
                    isDate:function(date)
                    {
               
                        // On sépare la date en 3 variables pour vérification, parseInt() converti du texte en entier
                        var tabDate = date.split('/');
               
                        //on vérifie que le jour soit une valeur numérique
                        if(!$.la.utils.isNumeric(tabDate[0])){
                            return false;
                        }
                        else{
                            var j = parseInt(tabDate[0],"10");
                        }
               
                        //on vérifie que le mois soit une valeur numérique
                        if(!$.la.utils.isNumeric(tabDate[1])){
                            return false;
                        }
                        else{
                            var m = parseInt(tabDate[1],"10");
                        }
               
                        //on vérifie que l année soit une valeur numérique
                        if(!$.la.utils.isNumeric(tabDate[2])){
                            return false;
                        }
                        else{
                            var a = parseInt(tabDate[2],"10");
                        }
                
                        if(isNaN(a)||isNaN(m)||isNaN(j)){
                            return false;
                        }
                        else{
            	   
                            // Définition du dernier jour de février
                            // Année bissextile si annnée divisible par 4 et que ce n'est pas un siècle, ou bien si divisible par 400
                            var fev;
                            if ((a%4 === 0 && a%100 !== 0) || (a%400 === 0)) {
                                fev = 29;
                            }
                            else {
                                fev = 28;
                            }


                            // Nombre de jours pour chaque mois
                            var nbJours;
                            if(fev==28){
                                nbJours = [31,28,31,30,31,30,31,31,30,31,30,31];
                            }
                            else if(fev==29){
                                nbJours = [31,29,31,30,31,30,31,31,30,31,30,31];
                            }

                            // Enfin, retourne vrai si le jour est bien entre 1 et le bon nombre de jours, idem pour les mois, sinon retourn faux
                            return ( (m >= 1) &&(m <=12) && (j>= 1) && (j <= nbJours[m-1]) );
                        }
                    },

                    verifMail: function (emailString)
                    {
                        var pass = false;
                        for(var j=1;j<(emailString.length);j++){
                            if(emailString.charAt(j)=='@'){
                                if(j<(emailString.length-4)){
                                    for(var k=j;k<(emailString.length-2);k++){
                                        if(emailString.charAt(k)=='.'){
                                            pass=true;
                                        }
                                    }
                                }
                            }
                        }
                        return pass;
                    },

                    checknumber: function(atester)
                    {
                        var anum=/(^\d+$)|(^\d+\.\d+$)/;
                        if (anum.test(atester)){
                            pass=true;
                        }
                        else{
                            pass=false;
                        }
                        return pass;
                    },

                    isMineur: function(sD,sM,sY)
                    {
                        //Date du jour
                        var d = new Date();
                        var curr_dayOfMonth = d.getDate();
                        var curr_month = d.getMonth()+1;
                        var curr_year = d.getFullYear();

                        if(curr_year-sY>18 || (curr_year-sY==18 && curr_month>=sM && curr_dayOfMonth>=sD)){
                            return false;
                        }
                        else{
                            return true;
                        }
                    },

                    querystring_get: function(key, default_)
                    {
                        // This silly looking line changes UNDEFINED to NULL
                        if (default_ === null){
                            default_ = null;
                        }

                        var value = this.params[key];
                        if (value === null){
                            value=default_;
                        }
                        return value;
                    },

                    querystring: function(qs)
                    { // optionally pass a querystring to parse
                        this.params = {};
                        this.get = $.la.utils.querystring_get;

                        if (qs === null || typeof qs === 'undefined'){
                            qs = location.search.substring(1, location.search.length);
                        }

                        if (qs.length === 0){
                            return;
                        }

                        // Turn <plus> back to <space>
                        // See: http://www.w3.org/TR/REC-html40/interact/forms.html#h-17.13.4.1
                        qs = qs.replace(/\+/g, ' ');
                        var args = qs.split('&'); // parse out name/value pairs separated via &

                        // split out each name=value pair
                        for (var i=0;i<args.length;i++) {
                            var value;
                            var pair = args[i].split('=');
                            var name = unescape(pair[0]);

                            if (pair.length == 2){
                                value = unescape(pair[1]);
                            }
                            else{
                                value = name;
                            }

                            this.params[name] = value;
                        }
                    },

                    getTimeStamp: function()
                    {
                        var currentTime = new Date();
                        return currentTime.getTime();
                    },

                    verifyUpload: function( value_image,value_video,type, formId )
                    {
                        if(type == 1){
                            if(value_image === ''){
                                alert($.la.utils.messages.fileNeeded);
                            }else{
                                $('#' + formId).submit();
                            }
                        }else{
                            if(value_video === ''){
                                alert($.la.utils.messages.fileNeeded);
                            }else{
                                if (value_video.indexOf('.mov') === -1 && value_video.indexOf('.wmv') === -1 && value_video.indexOf('.mpeg') === -1 && value_video.indexOf('.avi') === -1 && value_video.indexOf('.3gp') === -1 && value_video.indexOf('.mp4') === -1 && value_video.indexOf('.ram') === -1 && value_video.indexOf('.flv') === -1){
                                    alert($.la.utils.messages.notAVideoFile);
                                }else{
                                    $('#' + formId).submit();
                                }
                            }
                        }
                    },

                    switchTab:function(div1,div2,classe)
                    {
                        $('#'+div1).addClass(classe);
                        $('#'+div2).removeClass(classe);
                    },

                    showHide:function(div1,div2)
                    {
                        $('#'+div1).show();
                        $('#'+div2).hide();
                    },

                    sortOptionFromSelect: function(select_id)
                    {
                        var Liste = new Array();
                        var selected_item ="";
                        var all_theme_value ="";

                        // Recuperation
                        $('#'+select_id).find("option").each(function(i){
                            Liste[i] = new Array();
                            Liste[i][0]=$(this).text();
                            Liste[i][1]=$(this).val();

                            if($(this).attr("selected") == true){
                                selected_item = $(this).val();
                            }

                            if($(this).attr("id") == "all_theme"){
                                all_theme_value = $(this).val();
                            }
                        });

                        // Tri
                        Liste=Liste.sort();

                        // Reaffectation
                        $('#'+select_id).find("option").each(function(i){
                            $(this).attr('text',Liste[i][0]);
                            $(this).attr('value',Liste[i][1]);

                            if(Liste[i][1] == selected_item ){
                                $(this).attr('selected','selected');
                            }

                            if($(this).val() == all_theme_value){
                                $('#all_theme').attr('id','');
                                $(this).attr('id','all_theme');
                            }
                        });

                        // Remonter "Tous les themes"
                        $('#'+select_id).prepend($("#all_theme"));
                    },

                    escapeHtml: function(stringToEscape)
                    {
                        var newString=stringToEscape.replace(/"/g,"&quot;");
                        newString=newString.replace(/</g,"&lt;");
                        newString=newString.replace(/>/g,"&gt;");
                        newString=newString.replace(/&/g,"&amp;");
                        return newString;
                    },

                    trim: function(string)
                    {
                        var newString=string.replace(/(?:^\s+|\s+$)/g, "");
                        return newString;
                    },

                    digital:function(digi){
                        digi = parseInt(digi,10);
                        if (digi <= 9){
                            return "0" + digi;
                        }
                        return digi;
                    },
            
                    stripslashes: function (string) {
                        return (string+'').replace(/\\(.?)/g, function (s, n1) {
                            switch (n1) {
                                case '\\':
                                    return '\\';
                                case '0':
                                    return '\0';
                                case '':
                                    return '';
                                default:
                                    return n1;
                            }
                        });
                    },
            
                    checkImageSrc:function(classe){
            	
                        var src_ok = 1;

                        $('.'+classe).error(function(){
                            var src_ok = 0;
                        });
            	
                        return src_ok;
                    },
            
                    str_split: function (string, split_length) {
                        if (string === undefined || !string.toString || split_length < 1) {
                            return false;
                        }
                        return string.toString().match(new RegExp('.{1,' + (split_length || '1') + '}', 'g'));
                    },
                    implode:function (glue, pieces) {
                        var i = '', retVal='', tGlue='';
                        if (arguments.length === 1) {
                            pieces = glue;
                            glue = '';
                        }
                        if (typeof(pieces) === 'object') {
                            if (pieces instanceof Array) {
                                return pieces.join(glue);
                            }
                            else {
                                for (i in pieces) {
                                    retVal += tGlue + pieces[i];
                                    tGlue = glue;
                                }
                                return retVal;
                            }
                        }
                        else {
                            return pieces;
                        }
                    },
                    /* UTILS / LIST */
                    list:{
                        add: function (aListArray, oElement, push) {
                            if(push){
                                aListArray.push(oElement);
                            }else{
                                aListArray.unshift(oElement);
                            }
                            return aListArray;
                        },
	        	
                        alreadyIn:function(listArray, id){
                            return (this.find(listArray, id).length > 0);
                        },
	        	
                        find:function(listArray, id){
                            arr = jQuery.grep(listArray, function(n){
                                return n.element.id == id;
                            });
                            return arr;
                        },
	            
                        order:function(aListArray, bIsRandom){
                            aOrderArray = [];
                            for(x in aListArray){
                                obj = aListArray[x];
                                aOrderArray.push(obj.id);
                            }
                            if(bIsRandom == 'on'){
                                aOrderArray.sort(function(){
                                    return 0.5 - Math.random()
                                });
                            }
                            return aOrderArray;
                        }
                    }
          
                },

                /* COOKIES */
                cookie:{
                    init: function(){
                        var pathname=location.pathname;
                        var myDomain=pathname.substring(0,pathname.lastIndexOf('/')) +'/';
                        var date_exp = new Date();
                        var adserver_test=true;
                        date_exp.setTime(date_exp.getTime()+(365*24*3600*1000));
                        var qs = new $.la.utils.querystring();
                        var id_lien=qs.get('id_op');
                        if (id_lien !== null){
                            $.la.cookie.set('id_lien', id_lien);
                        }
                        var regImage=qs.get('img');
                        if (regImage !== null){
                            $.la.cookie.set('regImage', regImage);
                        }
                    },

                    getVal: function(offset)
                    {
                        var endstr=document.cookie.indexOf (";", offset);
                        if (endstr==-1){
                            endstr=document.cookie.length;
                        }
                        return unescape(document.cookie.substring(offset, endstr));
                    },

                    get: function(name)
                    {
                        var arg=name+"=";
                        var alen=arg.length;
                        var clen=document.cookie.length;
                        var i=0;
                        while (i<clen)
                        {
                            var j=i+alen;
                            if (document.cookie.substring(i, j)==arg){
                                return $.la.cookie.getVal(j);
                            }
                            i=document.cookie.indexOf(" ",i)+1;
                            if (i === 0){
                                break;
                            }
                        }
                        return false;
                    },

                    set: function(name, value, expireParam, domain)
                    {
                        domain = domain||false;
                        var expires = new Date();
                        if (expireParam && typeof expireParam == 'object'){
                            expires = expireParam;
                        }
                        else if (typeof expireParam == 'number'){
                            expires.setTime(((expires.getTime() / 1000) + expireParam) * 1000); // expireParam en secondes
                        }
                        else{
                            expires.setTime(expires.getTime() + 99999999999);
                        }
                        var path="/";
                        var secure=false;
                        var curCookie = name + "=" + escape(value) +
                        ((expires) ? "; expires=" + expires.toGMTString() : "") +
                        ((path) ? "; path=" + path : "") +
                        ((domain) ? "; domain=" + domain : "") +
                        ((secure) ? "; secure" : "");
                        document.cookie = curCookie;
                    },

                    clear: function(name)
                    {
                        var date = new Date();
                        var date_del = date.setFullYear(date.getFullYear() - 1000);
                        document.cookie = name + "=" + escape('') + "; expires=" + date_del;
                    }

                },

                /* AUDIENCE, GA, WEBO, etc. */
                tracking:{
                    activeConsole:function(){
                        if($.la.utils.httpGet('la_console') == '1'||$.la.utils.httpGet('la_console') == '0' || $.la.cookie.get('la_console') == '1'){
                            $.la.tracking.console();
                        }
                    },
                    console:function(){
                        try{
                            $.getScript('http://www.infobebes.com/extension/lajavascript/design/standard/javascript/jquery/jquery.la.console.js', function(){
                                $.la.tracking._activeConsole();
                            });
                        }
                        catch(e){
                            alert('Erreur de chargement de la console : ' + e.message);
                        }
                    },
                
                    init:function(obj){
                        /* exemple d'implémentation
                	
                	$.la.tracking.init({
                		WRP_SECTION:'section',
                		WRP_SUBSECTION:'sub_section',
                		WRP_CHANNEL:'channel',
                		WRP_CONTENT:'content',

                		CM_NIVEAU1: '',
                		CM_NIVEAU2: '',
                		CM_NIVEAU3: '',
                		CM_NIVEAU4: '',

                	});
                	*/
                        if(typeof obj.CM_SERIAL == 'undefined'){
                            obj.CM_SERIAL = $.la.tracking.cybermonitor.ids.CM_SERIAL;
                        }
                        if(typeof obj.CM_CLIENT == 'undefined'){
                            obj.CM_CLIENT = $.la.tracking.cybermonitor.ids.CM_CLIENT;
                        }
                        if(typeof obj.CM_TAG == 'undefined'){
                            obj.CM_TAG = $.la.tracking.cybermonitor.ids.CM_TAG;
                        }
                        if(typeof obj.WRP_ID == 'undefined'){
                            obj.WRP_ID = $.la.tracking.webo.ids.WRP_ID;
                        }

                        $.la.tracking.webo.init(obj.WRP_ID, obj.WRP_SECTION, obj.WRP_SUBSECTION, obj.WRP_CHANNEL,obj.WRP_CONTENT);
                        $.la.tracking.cybermonitor.init(obj.CM_SERIAL,obj.CM_CLIENT,obj.CM_NIVEAU1, obj.CM_NIVEAU2,obj.CM_NIVEAU3,obj.CM_NIVEAU4,obj.CM_TAG);
                    },

                    track:function(){
                        $.la.tracking.webo.track();
                        $.la.tracking.cybermonitor.track();
                        $.la.tracking.ga.track();
                    },
                    ga:{
                        _ua:null, /* js local */
                        _domain:null, /* js local */
                        track:function(){
                            var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
                            $.ajax({
                                type: "GET",
                                url: gaJsHost + "google-analytics.com/ga.js",
                                dataType: 'script',
                                cache:true,
                                success: function(){
                                    try {
                                        var pageTracker = _gat._getTracker( $.la.tracking.ga._ua);
                                        pageTracker._setDomainName($.la.tracking.ga._domain);
                                        pageTracker._trackPageview();
                                    } catch(err) {}
                                }
                            });
                        }
                    },
                    cybermonitor:{
                	
                        ids:{
                            CM_SERIAL:null,	// à surcharger en local
                            CM_CLIENT:null,	// à surcharger en local
                            CM_TAG:'ml'		// à surcharger à l'appel dans le HTML si Frames (voir DOC)
                        },
                	
                        init:function(CM_SERIAL, CM_CLIENT, CM_NIVEAU1, CM_NIVEAU2, CM_NIVEAU3, CM_NIVEAU4,CM_TAG){
                            $.la.tracking.cybermonitor.ids.CM_SERIAL = CM_SERIAL;
                            $.la.tracking.cybermonitor.ids.CM_CLIENT = CM_CLIENT;
                            $.la.tracking.cybermonitor.ids.CM_NIVEAU1 = CM_NIVEAU1;
                            $.la.tracking.cybermonitor.ids.CM_NIVEAU2 = CM_NIVEAU2;
                            $.la.tracking.cybermonitor.ids.CM_NIVEAU3 = CM_NIVEAU3;
                            $.la.tracking.cybermonitor.ids.CM_NIVEAU4 = CM_NIVEAU4;
                            $.la.tracking.cybermonitor.ids.CM_TAG = CM_TAG;

                        },
	                
                        // Permet de changer le type de marker à la volée (ex : interstitiel);
                        // $.la.tracking.cybermonitor.setMarker('mr4') sur la home avec un interstitiel
                        setMarker:function(marker){
                            $.la.tracking.cybermonitor.ids.CM_TAG = marker;
                        },
	                
                        track:function(){
                            $.ajax({
                                type: "GET",
                                url: 'http://prof.estat.com/js/'+$.la.tracking.cybermonitor.ids.CM_SERIAL+'.js',
                                dataType: 'script',
                                cache:true,
                                success: function(){
                                    eStat_id.cmclient($.la.tracking.cybermonitor.ids.CM_CLIENT);
                                    eStat_id.niveau(1,$.la.tracking.cybermonitor.ids.CM_NIVEAU1);
                                    eStat_id.niveau(2,$.la.tracking.cybermonitor.ids.CM_NIVEAU2);
                                    eStat_id.niveau(3,$.la.tracking.cybermonitor.ids.CM_NIVEAU3);
                                    eStat_id.niveau(4,$.la.tracking.cybermonitor.ids.CM_NIVEAU4);
                                    eStat_tag.post($.la.tracking.cybermonitor.ids.CM_TAG);
                                }
                            });
                        }

                    },

                    webo:{
                        defaultChannel:'editorial',
                        ids:{
                            WRP_ID:null
                        },

                        track:function(){
                            if($.la.cookie.get('la_console') == '1' || $.la.cookie.get('la_console') == 1){
                            //
                            }
                            else{
                                var WRP_SECTION = $.la.tracking.webo.ids.WRP_SECTION;
                                var WRP_SUBSECTION = $.la.tracking.webo.ids.WRP_SUBSECTION;
                                var WRP_ID = $.la.tracking.webo.ids.WRP_ID;
                                var WRP_ACC = $.la.tracking.webo.ids.WRP_ACC;
                                var WRP_CHANNEL = $.la.tracking.webo.ids.WRP_CHANNEL;
                                var WRP_SECTION_GRP = $.la.tracking.webo.ids.WRP_SECTION_GRP;
                                var WRP_SUBSECTION_GRP = $.la.tracking.webo.ids.WRP_SUBSECTION_GRP;
                                var WRP_CONTENT = $.la.tracking.webo.ids.WRP_CONTENT;

                                var w_counter = new wreport_counter(WRP_SECTION, WRP_SUBSECTION, WRP_ID, WRP_ACC, WRP_CHANNEL, WRP_SECTION_GRP, WRP_SUBSECTION_GRP);
                                w_counter.add_content(WRP_CONTENT);
                                w_counter.count();
                            }
                        },

                        /**
    		 * $.la.tracking.webo.init(iWRP_ID, sWRP_SECTION, sWRP_SUBSECTION, sWRP_CHANNEL,sWRP_CONTENT);
    		 */

                        init:function(iWRP_ID, sWRP_SECTION, sWRP_SUBSECTION, sWRP_CHANNEL,sWRP_CONTENT){
                            $.la.tracking.webo.ids.WRP_ID = iWRP_ID;
                            $.la.tracking.webo.ids.WRP_SECTION = sWRP_SECTION;
                            $.la.tracking.webo.ids.WRP_SUBSECTION = sWRP_SUBSECTION;
                            $.la.tracking.webo.ids.WRP_SECTION_GRP= iWRP_ID;
                            $.la.tracking.webo.ids.WRP_SUBSECTION_GRP= sWRP_SECTION;
                            $.la.tracking.webo.ids.WRP_CONTENT= sWRP_CONTENT;
                            $.la.tracking.webo.ids.WRP_CHANNEL= sWRP_CHANNEL;
                            if ($.la.tracking.webo.ids.WRP_CHANNEL == '{'+"wrp_channel"+'}'){
                                $.la.tracking.webo.ids.WRP_CHANNEL = $.la.tracking.webo.defaultChannel;
                            }
                            var WRP_ACC;
                            $.la.tracking.webo.ids.WRP_ACC = WRP_ACC;
                        }
                    }
                },

                /* PROMO */
                promo: {
                    container:'#container',
                    containerBody:'body',

                    localCss:function(obj){
                        return '';
                    },
                    localCssAfter:function(obj){
                        return '';
                    },
                

                    showSkin:function(obj){
                        /*
        		 * PARAMS
        		 * obj.bodybgcolor
        		 * obj.bodybgimage
        		 * obj.top
        		 * obj.url
        		 * obj.newwindow
        		 * obj.gabarit
        		 * 
        		 * $.la.promo.showSkin({bodybgcolor:'#123456',bodybgimage:'http://www.image.com/toto.jpg',top:'143px',url:'http://www.google.com',newwindow:true})
        		 * 
        		 */
                        var container = obj.container;
                        if(typeof container == 'undefined'){
                            container = $.la.promo.container;
                        }
                        var containerBody = obj.containerBody;
                        if(typeof containerBody == 'undefined'){
                            containerBody = $.la.promo.containerBody;
                        }
                        $('head').append('<style type="text/css">' + $.la.promo.localCss(obj) + '\
    				body{\
    					background:' + obj.bodybgcolor + ' url(' + obj.bodybgimage + ') center top no-repeat !important;\
    				}\n\
    				'+containerBody+'{\n\
    					padding:' + obj.top + ' 0 0 0 !important;\n\
    					position:relative !important;\n\
    					z-index:1 !important;\n\
    				}\n\
    				' + container + '{\
    					position:relative;\
    					z-index:3;\
    				}\
    				' + $.la.promo.localCssAfter(obj) + '</style>');

                        if (obj.url){
                            var o = obj;
                            $(
                                function(){
                                    $(container).css({
                                        position:'relative'
                                    });
                                    if(o.newwindow){
                                        $(containerBody).prepend('<a href="'+o.url+'" style="z-index:2;position:absolute;top:0;left:0;width:100%;height:'+$(document).height()+'px;" target="_blank"></a>');
                                    }
                                    else{
                                        $(containerBody).prepend('<a href="'+o.url+'" style="z-index:2;position:absolute;top:0;left:0;width:100%;height:'+$(document).height()+'px;"></a>');
                                    }
                                }
                                );
                        }
                    },

                    wreportRefresh: function(){
                        if(wreport_ok==1){
                            var w_counter = new wreport_counter(WRP_SECTION, WRP_SUBSECTION, WRP_ID, WRP_ACC, WRP_CHANNEL, WRP_SECTION_GRP, WRP_SUBSECTION_GRP);
                            w_counter.add_content(WRP_CONTENT);
                            w_counter.count();
                        }
                    },

                    bannerRefresh: function(uri,gestionHauteur){
                        if(typeof gestionHauteur == 'undefined'){
                            gestionHauteur = 'gestionHauteur';
                        }
                        if($('#'+gestionHauteur).length  > 0){
                            $('#'+gestionHauteur).html('<iframe scrolling="no" frameborder="0" width="728" height="90"  border="0" marginheight="0" marginwidth="0" noresize="true" vspace="0" framespacing="0"  src="' + $.la.sHost + '/ajaxext/pub?publicite=banniere&uri='+uri+'"></iframe>');
                        }
                    },

                    adRefresh: function(){
                    // TODO : récupérer l'élément de la zone
                    // injecter l'iframe avec le bon id et un nouveau random dans toutes les zones
                    },
                    // Ads
                    a2dToRegister:{},
                
                    _adList:[],
                    _adListForConsole:[],
                    _a2dRandom:Math.random(),
                
                    // * à surcharger dans son siteacces si besoin*/
                    _adDefaultDefer:false,
                    _adDefaultZoneDefer:{},
                    _adDefaultForceDefer:null,
                    _adDefaultForceZoneDefer:{},
                
                    _adHost:'http://fr.a2dfp.net',
                    _testA2d:{},
               

                    // obsolete remplacé par adSetZone
                    prependRegister:function(placement,id,defer){
                        if(id != ''){
                            $.la.promo.a2dToRegister[placement]={
                                id:id,
                                defer:defer
                            };
                        }
                    },

                    adSetZone:function(placement,id){
                        if(id != ''){
                            $.la.promo.a2dToRegister[placement]={
                                id:id
                            };
                        }
                    },

                    adLoad:function(index){
                        if(typeof $.la.promo._adList[index] != 'undefined'){
                            $.la.promo._adWrite($.la.promo._adList[index].id);
                        }
                    },
            
                    adsProcess:function() {

                        for(var i = 0; i< $.la.promo._adList.length;i++){
                            if(typeof $.la.promo._adList[i] != 'undefined'){
                                if($.la.promo._adList[i].defer == 'ready'){
                                    var index = i;
                                    $.la.promo._deferProcess(index);

                                }else{
                                    $('#'+$.la.promo._adList[i].elt)._adProcess(i);
                                }
                            }
                        }
                    },

                    testA2d:function(idCampaign){
                        var url = document.location.href.replace('&testA2d=' + idCampaign,'');
                        url = url.replace('\?testA2d=' + idCampaign,'\?');
                        url = url.replace('?testA2d=' + idCampaign,'?');

                        if(document.location.href.indexOf('?') != -1){
                            document.location.href = url + '&testA2d=' + idCampaign;
                        }
                        else{
                            document.location.href = url + '?testA2d=' + idCampaign;
                        }
                    },

                    removeTestA2d:function(idCampaign){
                        document.location.href = document.location.href.replace(document.location.search,'');
                    },

                    _adRegister:function(id, defer){

                        /**
    	             * settings :
    	             * defer :
    	             *   'fasle' ou '' : chargement immédiat;
    	             *  'bottom' : en bas de page (default)
    	             * 'ready' : on document ready
    	             */

                        // compatibilité version précédente
                        if(typeof id != 'object'){
                            settings = {};
                            settings.id = id;
                            settings.defer = defer;
                            settings.zone = null;
                        }
                        // new implementation
                        else if(typeof id == 'object'){
                            settings = id; // first param
                        }
                   
                    
                        // on récupère les infos dans l'objet stocké dans
                        // a2dToRegister de la zone
                        if(typeof settings.zone != 'undefined' && typeof $.la.promo.a2dToRegister[settings.zone] == 'object' ){
                            settings.id = $.la.promo.a2dToRegister[settings.zone].id;
                            settings.defer = $.la.promo.a2dToRegister[settings.zone].defer;
                        }

                        if(typeof settings.defer == 'undefined' || (settings.defer == '' && settings.defer !== false) || settings.defer == '{defer}'){
                            // le defer n'est pas pr�cis� dans l'appel de la
                            // fonction
                            if(typeof $.la.promo._adDefaultZoneDefer[settings.zone] != 'undefined'){
                                settings.defer = $.la.promo._adDefaultZoneDefer[settings.zone];
                            }
                            else  if($.la.promo._adDefaultDefer != null){
                                settings.defer = $.la.promo._adDefaultDefer;
                            }
                            // Rien n'est d�fini : on passe tout � false'
                            else{
                                settings.defer = false;
                            }
                           
                        }

                   

                        // valeurs par d�faut si non d�fini
                        if(typeof settings.defer == 'undefined' || settings.defer == '' || settings.defer == '{defer}'){
                            // on a d�fini un defer par d�faut pour une zone
                            if(typeof $.la.promo._adDefaultZoneDefer[settings.zone] != 'undefined'){
                                settings.defer = $.la.promo._adDefaultZoneDefer[settings.zone];
                            }
                            // on a d�fini un defer par d�faut pour toutes les zones
                            else if(typeof $.la.promo._adDefaultDefer != 'undefined'){
                                settings.defer = $.la.promo._adDefaultDefer;
                            }
                            // on a rien d�fini
                            else{
                                settings.defer = $.la.promo._adDefaultDefer;
                            }
                        }
                        // Urgence : on force la valeur sur une zone
                        if(settings.zone != null && $.la.promo._adDefaultForceZoneDefer != null && typeof $.la.promo._adDefaultForceZoneDefer[settings.zone] != 'undefined'){
                            settings.defer = $.la.promo._adDefaultForceZoneDefer[settings.zone];
                        }
                        // Mega urgence : on force la valeur partout !
                        else if($.la.promo._adDefaultForceDefer != null){
                            settings.defer = $.la.promo._adDefaultForceDefer;
                        }

                        // on nettoye un peu
                        // 'false' => false
                        if(settings.defer == 'false'){
                            settings.defer = false;
                        }

                        settings.elt = $(this).attr('id');

                        // un test a2d ?
                        if($.la.utils.httpGet('testA2d') && typeof $.la.promo._testA2d[$.la.utils.httpGet('testA2d')] != 'undefined'){
                            if($.la.promo._testA2d[$.la.utils.httpGet('testA2d')][settings.zone] != ''){
                                settings.id = $.la.promo._testA2d[$.la.utils.httpGet('testA2d')][settings.zone];
                            }
                        }

                        if(settings.defer == false){
                            $.la.promo._adWrite(settings.id);
                            $.la.promo._adListForConsole[$.la.promo._adListForConsole.length] = settings;
                        }
                        else{
                            $.la.promo._adList[$.la.promo._adList.length] = settings;
                            $.la.promo._adListForConsole[$.la.promo._adListForConsole.length] = settings;
                        }
                    

                    },

                    _adProcess:function(id){
                        var prefix = 'preload_ad_';
                        try{
                            var me = $(this)[0];
                            me.appendChild($('div[id="'+prefix+id+'"]')[0]);
                        }
                        catch(e){
                            return;
                        }
                    },

                    _adWrite:function(id){
                        try{
                            document.write('<scr' + 'ipt type="text\/javascript" src="' + $.la.promo._adHost + '\/ad?s=' + id + '&m=js&ncb=' + $.la.promo._a2dRandom + '"><\/scr'+'ipt>');
                        }
                        catch(e){
                    	
                        }
                    },

                    _deferProcess:function(index){
                        var i = index;
                        $(function(){
                            $('#'+$.la.promo._adList[i].elt)._adProcess(i);
                        });
                    },

                    /* GOOGLE */
                    google: {
                        adsense: {
                            _scriptUrl: 'http://pagead2.googlesyndication.com/pagead/show_ads.js',
            		
                            _params: {
                                google_ad_client: null,
                                google_ad_channel: null,
                                google_ad_output: "js",
                                google_max_num_ads: null,
                                google_ad_type: "text",
                                google_language: null,
                                google_encoding: "utf8",
                                google_feedback: "on",
                                google_adtest: "off"
                            },
            		
                            _createHtml: {
                                container: function (sId) {
                                    return '<div id="' + sId + '"></div>';
                                },
                                title: null,
                                flash: null,
                                image: null,
                                html: null,
                                text: null
                            },
            		
                            _sContainerId: 'googleAdsense',
            		
                            _ads: null,
                            _info: null,
            		
                            _iDisplayNumber: 0,
                            _iBlocNumber: 0,
            		
                            execute: function() {
                                if (this._iBlocNumber == 0) {
                                    document.write('<script type="text/javascript">var google_adnum = 0;</script>');
                                    document.write('<script type="text/javascript">//<![CDATA[\n'
                                        + 'function google_ad_request_done(google_ads) {\n'
                                        + '  jQuery(document).ready(function() { $.la.promo.google.adsense.display(google_ads, google_info); });\n'
                                        + '}\n'
                                        + '//]]></script>');
                                }
                                document.write('<script type="text/javascript">\n'
                                    + 'google_ad_output = "' + this._params.google_ad_output + '";\n'
                                    + 'google_ad_type = "' + this._params.google_ad_type + '";\n'
                                    + 'google_encoding = "' + this._params.google_encoding + '";\n'
                                    + 'google_feedback = "' + this._params.google_feedback + '";\n'
                                    + 'google_language = "' + this._params.google_language + '";\n'
                                    + 'google_skip = ' + ( this._iBlocNumber * this._params.google_max_num_ads ) + ';\n'
                                    + '\n'
                                    + 'google_ad_client = "' + this._params.google_ad_client + '";\n'
                                    + 'google_ad_channel = "' + this._params.google_ad_channel + '";\n'
                                    + 'google_max_num_ads = "' + this._params.google_max_num_ads + '";\n'
                                    + 'google_adtest = "' + this._params.google_adtest + '";\n'
                                    + '</script>');
                                document.write('<script type="text/javascript" src="' + this._scriptUrl + '?iNumber=' + this._iBlocNumber + '"></script>');
            			
                                document.write( this._createHtml.container( this._sContainerId + this._iBlocNumber ) );
            			
                                this._iBlocNumber++;
                            },
            		
                            display: function(google_ads, google_info) {
                                this._ads = google_ads;
                                this._info = google_info;
            			
                                if (this._ads.length == 0) {
                                    return ;
                                }
            		    
                                var s = '';
            		    
                                if (this._createHtml.title != null) s += this._createHtml.title( this );
            		    
                                switch (this._ads[0].type) {
                                    case 'flash':
                                        if (this._createHtml.flash != null) s += this._createHtml.flash( this );
                                        break;
                                    case 'image':
                                        if (this._createHtml.image != null) s += this._createHtml.image( this );
                                        break;
                                    case 'html':
                                        if (this._createHtml.html != null) s += this._createHtml.html( this );
                                        break;
                                    case 'text':
                                        if (this._createHtml.text != null) s += this._createHtml.text( this );
                                        break;
                                }
                                if (this._ads[0].bidtype == "CPC") {
                                    google_adnum = google_adnum + this._ads.length;
                                }
                                $( '#' + this._sContainerId + this._iDisplayNumber ).html(s);
                                this._iDisplayNumber++;
                            }
                        }
                    }

                },
        
                /* GAME */
                game:{
                    verifchek: function (checkBoxName, objId, message)
                    {
                        if(typeof message === 'undefined'){
                            message = 'Il faut selectionner une reponse avant de continuer !';
                        }
                        var valid=0;
                        $("input[name='" + checkBoxName + "']").each(function(i){
                            if($(this).attr('checked'))

                            {
                                valid = 1;
                            }
                        });
                        // action si le champs est vide
                        if (valid == 0)
                        {
                            alert(message);
                        }
                        else
                        {
                            $('#form_question_' + objId ).submit();
                        }
                    },

                    verifOpenQuestion: function(id, objId, message){
                        if(typeof message === 'undefined'){
                            message = 'Il faut saisir un texte !';
                        }
                        var i = 0;
                        var valid=0;
                        if ($("#" + id).val() !== '')
                        {
                            valid=1;
                        }
                        // action si le champs est vide
                        if (valid == 0){
                            alert(message);
                        }
                        else {
                            $('#form_question_' + objId ).submit();
                        }
                    }
                },

                replay:{
                // Appeler /extension/lajavascript/design/standard/javascript/jquery/replay/jquery.la.replay.js
                },
            
            
            
                w: {
                    go: function(url) {
                        /*var newWindow = parseInt(url.charAt(0),10);
                    url = url.substring(1, url.length);
                    var tmp = '';
                    var c = '';
                    for(var i=0; i<url.length; i++){
                        c = url.charCodeAt(i);
                        tmp += '' + String.fromCharCode(c - 14);
                    };//	*/
                        var newWindow = parseInt(url.charAt(0),10);
                        var tmp = this.decrypt(url);
                        if(newWindow){
                            // alert('newWindow=1 & ' + tmp);
                            window.open(tmp);
                        }
                        else{
                            // alert('newWindow=0 & ' + tmp);
                            document.location.href = tmp;
                            if (document.all) {
                                window.event.returnValue = false;
                            }
                        }
                    },
                    decrypt: function(url) {
                        url = url.substring(1, url.length);
                        var tmp = '';
                        var c = '';
                        for(var i=0; i<url.length; i++){
                            c = url.charCodeAt(i);
                            tmp += '' + String.fromCharCode(c - 14);
                        }
                        return tmp;
                    }
                }
            });
        })(jQuery);

        // TODO : supprimer sHost à terme dans les tpl.
        var sHost = jQuery.la.sHost;

        // plugins
        jQuery.fn.adRegister = jQuery.la.promo._adRegister;
        jQuery.fn._adProcess = jQuery.la.promo._adProcess;
        jQuery(document).ready(jQuery.la.tracking.activeConsole);

        function KSL(){
            jQuery.la.tracking.console();
        }
    }
}
catch(e){
    if (window.console && window.console.log){
        window.console.log(e.message);
    }
}