/**
 * ACTION $.la.verifyParrainage(nom,prenom,mail,m1,m2,m3,idForm)
 * 
 * MUSILINE $.la.radio.popUpRadio() $.la.radio.lancePopUpRadio()
 * 
 * PROGRAMME $.la.programme.display_en_ce_moment()
 * $.la.programme.display_prochaine_diffusion()
 * 
 * MODALBOX $.la.modalbox.truc()
 */
(function($){

    $.la = $.la||{};
    $.la.decalage_horaire = $.la.decalage_horaire || 3600;
    $.extend(true,$.la,
    {
    	/* MODALBOX */
    	modalbox:{
    	/*
    	 * onclick="$.la.modalbox.ldapOpenLoginBoxRegistered();
    	 * 
    	 */	
		accordionSpeed: 'fast',
        	accordion:function(obj,callback){
    			//{'id1':false,'id2':true,etc}

			for(var id in obj) {
				$('#' + id).slideUp($.la.modalbox.accordionSpeed);
				$('#' + id).prev('h3').children('a').removeClass("opened");
			}
			for(var id in obj) {
			     if(obj[id] == true){
				 $('#'+ id).slideDown($.la.modalbox.accordionSpeed);
					$('#' + id).prev('h3').children('a').addClass("opened");
			     }
			}
    		},
    		
    		ldapOpenCloseLoginBoxRegistered:function(id){
    			// context : ldap, action : Open,  quelle modal ? : LoginBox quel onglet ? : Registered
    			//h3 a  + #ldapLoginBoxRegistered
			var obj = {};
			$("h3 + div").each(function(i){
				var currentId = $(this).attr('id');
				if(currentId !=''){
					obj[currentId] = false;
					if(currentId == id){
						if($('#' + id).css('display') == 'none'){
							obj[currentId] = true;
						}
					}
				}
			});
    			$.la.modalbox.accordion(obj,null);
    		}
	},

    	
    	/* ACTION */
        action:{
            verifyParrainage: function( nom, prenom, mail, m1, m2, m3, idForm)
            {
                if($('#' + nom).val() === '' || $('#' + prenom).val() === '' || $('#' + mail).val() === '' )
                {
                    alert('Tu dois mettre ton prénom, ton nom et ton email');
                    return false;
                }
                if (! $.la.utils.verifMail($('#' + mail).val()))
                {
                    alert('Ton email n\'est pas valide');
                    return false;
                }

                if($('#' + m1).val() === '' && $('#' + m2).val() === '' && $('#' + m3).val() === '' )
                {
                    alert('Tu dois parrainer au moins un copain');
                    return false;
                }

                if($('#' + m1).val() !== '' && (! $.la.utils.verifMail($('#' + m1).val())) )
                {
                    alert('Le premier mail n\'est pas valide');
                    return false;
                }
                if($('#' + m2).val() !== '' && (! $.la.utils.verifMail($('#' + m2).val()))  )
                {
                    alert('Le deuxieme mail n\'est pas valide');
                    return false;
                }
                if($('#' + m3).val() !== '' && (! $.la.utils.verifMail($('#' + m3).val()))  )
                {
                    alert('Le troisième mail n\'est pas valide');
                    return false;
                }
                $('#' + idForm).submit();
                return false;
            }
        },

        /* MUSILINE */
        radio:
        {
            /* pop  up Radio musiline */
            winRadio:null,

            popUpRadio:function(){
                /* appel : $.la.radio.popUpRadio() */
                if( $.la.radio.winRadio != null){
                    try{
                        for(var i in  $.la.radio.winRadio){}//ffx
                        $.la.radio.winRadio.focus();
                    }
                    catch(e)
                    {
                        $.la.radio.lancePopUpRadio();
                    }
                }
                else{
                    $.la.radio.lancePopUpRadio();
                }
            },

            lancePopUpRadio:function(){
                $.la.radio.winRadio = window.open(sHost+'/modalbox/radio','playerradio','width=923,height=353,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,copyhistory=no,resizable=no');
                $.la.radio.winRadio.focus();
            }
        },


        programme:{
            display_en_ce_moment:function()
            {
                var timestamp = Math.floor( (new Date().getTime())/1000 );
                timestamp = timestamp + $.la.decalage_horaire;
                var mode = 'recherche_current';
                var ts = 0;
                $('div.cache').each(function(i){
                    tmp = $(this).attr('id').split('-');
                    ts = tmp[1];
                    tsend = tmp[2];
                    if (mode == 'recherche_next'  && $(this).hasClass('next'))
                    {
                        $(this).show();
                        mode = 'efface_le_reste';
                    }
                    else if (
                        mode == 'recherche_current' && $(this).hasClass('current') && ts <= timestamp && tsend >= timestamp)
                        {
                        $(this).show();
                        mode = 'recherche_next';
                    }
                    else
                    {
                        $(this).hide();
                    }
                });
                window.setTimeout($.la.programme.display_en_ce_moment,6000);
            },


            display_prochaine_diffusion:function()
            {
                var timestamp = Math.floor( new Date().getTime()/1000 );
                timestamp = timestamp + $.la.decalage_horaire;
                var mode = 'recherche_current';
                var cpt = 0;
                var ts = 0;
                $('span.cache').each(function(i){
                    tmp = $(this).attr('id').split('-');
                    ts = tmp[1];

                    if ( mode == 'recherche_current' && $(this).hasClass('current') && ts >= timestamp)
                    {
                        $(this).show();
                        mode = 'recherche_next';
                    }
                    else
                    {
                        $(this).hide();
                    }
                });

                $('span.cache').each(function(i){
                    if ($(this).css('display') == 'none')
                    {
                        cpt++;
                    }
                });

                if ( cpt == $('span.cache').length)
                {
                    if($('span.message:first')){
                        $('span.message:first').show();
                    }
                }
                window.setTimeout($.la.programme.display_prochaine_diffusion,60000);
            }

        }
    });
})(jQuery);