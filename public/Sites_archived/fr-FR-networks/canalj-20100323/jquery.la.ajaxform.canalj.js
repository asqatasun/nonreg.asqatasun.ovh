(function($){

    $.la = $.la||{};
    $.extend(true,$.la,
    {
        /* AJAXFORM */
        ajaxForm:{
            /* propre à Canal J */
            settings:{
                /*****************************************/
                /** Setting for register/edit formulars **/
                /*****************************************/
                'modal_register_form':{
                    id_loading:'modal_register_loading',
                    step:[{
                        fields:['sexe','first_name','last_name','date_de_naissance','email','code_postal'/*,'pseudo','password','confirmmotdepasse'*/],
                        success: function(){
                            jQuery.la.ajaxForm.gotoStep('modal_register_form',1)
                        }
                    },
                    {
                        fields:['login','password','confirmmotdepasse'],
                        success: function(){
                            jQuery.la.ajaxForm.gotoStep('modal_register_form',2)
                        }
                    },
                    {
                        fields:['newsletter_canalj'],
                        success: function(){
                            jQuery.la.ajaxForm.gotoStep('modal_register_form',3)
                        }
                    },
                    {
                        fields:['cgu'],
                        success: function(){
                            jQuery.la.ajaxForm.gotoStep('modal_register_form',4)
                        }
                    }],
                    rules: {
                        newsletter_canalj: "required"
                    },
                    messages: {
                        newsletter_canalj: {
                            required: "choix obligatoire"
                        }
                    }
                }
            }
        }
    /* FIN FORM */
    });
})(jQuery);