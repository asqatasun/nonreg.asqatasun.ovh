blankImg = '/argosSites/tf6/images/blank.gif';

/* @Rubrique : Show / Hide details function
************************************/
window.addEvent('domready',function() {
	$$('.detailsbloc').each(function(item) {
		item.setStyle('display','none');
	});
});
function fx(bloc_id) {
	/* @Get all blocs to hide them */
	$$('.detailsbloc').each(function(item) {
		if(item.getProperty('id')!=bloc_id) {
			item.setStyle('display','none');
		}
		else {
			item.setStyle('display','block');
		}
	});
}


/* @Homepage : Hide Pre-Home
************************************/
function hidePreHome() {
	$('shadowPreHome').destroy();
	$$('body')[0].removeClass('preHome');
	return false;
}
window.addEvent('domready', function() {
	if($('hidePreHome')) {
		$('hidePreHome').addEvent('click', hidePreHome);
	}
});