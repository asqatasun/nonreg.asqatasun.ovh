var LA_LibVersion="4076";
var LA_DEBUG=""; // preprod or full
var LA_MODE="FIX";
var LA_USERDRAG="false";
var LA_DRAGBUTTONCALL="no";
var LA_PATH = "/design/gulli/images/LAData/";
var LA_SCENARIONAME="scenario.xml";
var LA_cd = location.href.substr(7);
LA_cd = LA_cd.substr(0,LA_cd.indexOf("/"));
if(location.href.indexOf("file")!=-1){LA_cd="";}
var LA_FLASH_INIT_SIZE = [157,202];
function LA_GetFlashVer(){
var fv = 0;var np=navigator.plugins;
if (np && np.length){x = np["Shockwave Flash"];
 if (x){if (x.description){y = x.description;fv = y.charAt(y.indexOf('.')-1);}}}
else{for(var i=9; i>0; i--){fv = 0;try{	var flash = new ActiveXObject("ShockwaveFlash.ShockwaveFlash." + i);fv = i;return fv;}
catch(e){}}}
return fv;
}
function LA_GetE(ane){return document.getElementById(ane);}
var LA_isIE=document.all;
var LAPObject;
var LADocIsReady=false;
LA_STATE=new Array();
var LA_ddok=false;
var ACTOR=new LA_Actor();

function LA_Integration(){
	if(LA_MODE=="DRAG"){
		addEvent(window,"load",LA_Do_Integration);
	}
	else if(LA_MODE=="FIX"){
		LA_Do_Integration();
		addEvent(window,"load",function(){setTimeout(function(){LADocIsReady=true;},400);});
	}
}

function LA_Do_Integration() {
	sdom=(LA_cd!="")? "&TO_SD="+LA_cd : "";
	LA_FlashVars = "TO_ROOT="+LA_PATH+sdom+"&TO_MODE="+LA_MODE+"&TO_PAGE="+location.href.substr(location.href.lastIndexOf("/")+1)+"&TO_SCENARIONAME="+LA_SCENARIONAME+"&TO_DEBUG="+LA_DEBUG;
	var fn = (LA_GetFlashVer()<8)? "LivingActorPlayer64001.swf" : "LivingActorPlayer84001.swf";
	//var fn="LivingActorPlayer62004.swf";
	dc = "";
  dc += "<object classid=\"clsid:d27cdb6e-ae6d-11cf-96b8-444553540000\" codebase=\"http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,79,0\" width=\""+LA_FLASH_INIT_SIZE[0]+"\" height=\""+LA_FLASH_INIT_SIZE[1]+"\"  id=\"LIVINGACTOR_FLASH\">"
	+"<param name=\"allowScriptAccess\" value=\"always\" />"
	+"<param name=\"movie\" value=\""+LA_PATH+fn+"\" />"
	+"<param name=\"wmode\" value=\"transparent\" />"
	+"<param name=\"FlashVars\" value=\""+LA_FlashVars+"\" />"
	+"<embed src=\""+LA_PATH+fn+"\" width=\""+LA_FLASH_INIT_SIZE[0]+"\" height=\""+LA_FLASH_INIT_SIZE[1]+"\"  quality=\"high\" salign=\"l\" wmode=\"transparent\" bgcolor=\"#ffaaff\" name=\"LIVINGACTOR_FLASH\"  id=\"LIVINGACTOR_FLASH\" FlashVars=\""+LA_FlashVars+"\"  allowScriptAccess=\"always\"  type=\"application/x-shockwave-flash\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" />"
	+"</object>\n";
	if(LA_isIE){
		dc+='<SCRIPT event=FSCommand(command,args) for=LIVINGACTOR_FLASH>\n lfc(command,args);</SCRIPT>';
	}
	else{
		if(LA_MODE=="DRAG"){
			var he=document.createElement("script");	
			he.type="text/javascript";
			he.innerHTML="function LIVINGACTOR_FLASH_DoFSCommand(a,b){lfc(a,b);}";
			document.body.appendChild(he);
		}
		else{
			dc+= "<scr"+"ipt language=\"JavaScript\" >"+"function LIVINGACTOR_FLASH_DoFSCommand(a,b){lfc(a,b);}\n </scr"+"ipt>\n";
		}
	}
	if(LA_MODE=="DRAG"){
		lad=cdiv("LA_DIV",9000,false);
		lad.innerHTML = dc;
		if (LA_DRAGBUTTONCALL == "no") {
			lad.style.left = 0 - LA_FLASH_INIT_SIZE[0] + 5 + "px";
		}
		document.body.appendChild(lad);
		if(LA_USERDRAG=="true"){
			if(LA_isIE){ladbb=cdiv("LA_DIVBB",9001,true,1);}else{ladbb=cdiv("LA_DIVBB",9001,false,0);}
			ladbb.style.cursor="move";
			lad.appendChild(ladbb);
			addEvent(document,"mousedown",ddInit);
			addEvent(document,"mouseup",myomu);
		}
		LADocIsReady=true;
	}
	else{
		document.write(dc);
	}
	LAPObject=LA_isIE?document.all["LIVINGACTOR_FLASH"] : document.LIVINGACTOR_FLASH;
}
function addEvent( obj, type, fn ) {if ( obj.attachEvent ) {obj['e'+type+fn] = fn;obj[type+fn] = function(){obj['e'+type+fn]( window.event );};obj.attachEvent( 'on'+type, obj[type+fn] );} else obj.addEventListener( type, fn, false );}
function removeEvent( obj, type, fn ) {if ( obj.detachEvent ) {obj.detachEvent( 'on'+type, obj[type+fn] );obj[type+fn] = null;} else obj.removeEventListener( type, fn, false );}
function cdiv(id,z,b,op){var dt=document.createElement('div');dt.id = id;dts=dt.style;if(b){dts.backgroundColor="#FFFFFF";}dts.position = "absolute";dts.left = "0px";dts.top = "0px";dts.width = "1px";dts.height = "1px";dts.zIndex = z;if(op){dts.filter = "alpha(opacity="+op+")";dts.MozOpacity =op/100;dts.khtmlOpacity=op/100;} return dt}
function lfc(a,b){if(a=="checkCon"){if(LADocIsReady){LAPObject.SetVariable("JS_CHECKCON","check");}}if(a=="exec"){eval(b);}if(a=="superexec"){setTimeout(function(){LA_exec(b);},300);}if (a == "AS_Callback") {if (LA_AS_CallbackXML != null) {LA_AS_CallbackXML(b);}
}if(a=="ONQUIT"){if(typeof(LA_OnQuit)!="undefined"){LA_OnQuit();}}}
function LA_createScriptNode(str){var e=document.createElement("script");e.type = "text/javascript";if("text" in e)e.text=str;else if("textContent" in e)e.textContent=str;else if("innerHTML" in e)e.innerHTML=str;else e.appendChild(document.createTextNode(str));return e;}
function LA_exec(b){sc=LA_createScriptNode(b);document.body.appendChild(sc);}
function sui(){LAPObject.SetVariable("JS_USERINPUT",LA_GetE("ASUserInput").value);LA_GetE("ASUserInput").value="";}
function ddInit(e){we=LA_isIE ?document.all.LA_DIV : document.getElementById("LA_DIV");we2=LA_isIE ?document.all.LA_DIVBB : document.getElementById("LA_DIVBB");we2.onmouseover=function(){document.LIVINGACTOR_FLASH.SetVariable("JS_EVENTBBDIV","mouseover");};we2.onmouseout=function(){document.LIVINGACTOR_FLASH.SetVariable("JS_EVENTBBDIV","mouseout");};he=LA_isIE ? event.srcElement : e.target;while (he.id!="LA_DIVBB"&&he.tagName!="BODY"&&he.tagName!="HTML"){he=LA_isIE ? he.parentElement : he.parentNode;}if (he.id=="LA_DIVBB"){ox=LA_isIE ? event.clientX : e.clientX;oy=LA_isIE ? event.clientY : e.clientY;nx=parseInt(we.style.left);ny=parseInt(we.style.top);nx2=parseInt(we2.style.left);ny2=parseInt(we2.style.top); LA_ddok=true;LA_isIE ?document.onmousemove = dd :document.addEventListener('mousemove',dd,false);}}
function dd(e){if (!LA_ddok) return;if(LA_USERDRAG=="false") return;we.style.left=LA_isIE ? nx+event.clientX-ox : nx+e.clientX-ox+"px"; we.style.top=LA_isIE ? ny+event.clientY-oy : ny+e.clientY-oy+"px";return false; }
function myomu(){if(LA_ddok){if(top.LA_OnDragEnd){top.LA_OnDragEnd();}}LA_ddok=false;}
function LA_GetState(astate){return LA_STATE[astate];}
function LA_UpdateState(k,v){document.LIVINGACTOR_FLASH.SetVariable("JS_UPDSTATE",(k+","+v));}
function LA_SyncStateFrom(s){LA_STATE=new Array();ar=s.split(",");for(i=0;i<ar.length;i+=2){LA_STATE[ar[i]]=ar[i+1];}}
function createUInputText(dim){if(LA_GetE("uitext")==null){var LAFparent;uidiv = cdiv("uitext", 905);st = uidiv.style;st.position = "relative";st.left = "-3000px";st.top = "0px";uidiv.innerHTML = "<textarea style=\"border:0px;overflow:auto;width:" + dim[0] + "px;height:" + dim[1] + "px;\" id=\"ASUserInput\" rows=\"2\" onkeypress=\"javascript:if(event.keyCode==13){sui();return false;}\"></textarea>";if (LA_MODE == "DRAG") {LAFparent = LA_GetE("LA_DIV");}else {LA_isIE?LAFparent = document.LIVINGACTOR_FLASH.parentNode :LAFparent = document.LIVINGACTOR_FLASH.parentNode.parentNode;}if (typeof(LAFparent) != undefined) {LAFparent.appendChild(uidiv);}}}
function LA_CheckUserInput(){if(LA_GetE("uitext")!=null){LA_GetE("uitext").style.left="-3000px";}}
function LA_showDebugVersions(p,x){ladp=cdiv("LA_DebugPane",9005,true);ladp.style.width="350px";ladp.style.height="20px";ladp.innerHTML="<i>LA_Flash Versions: </i>Player: <b>"+p+"</b> JS:<b> "+LA_LibVersion+"</b> XML:<b>"+x+"</b>";document.body.appendChild(ladp);}
function LA_AS_CallbackXML(anXMLNode){/*alert(unescape(anXMLNode));*/}
function getpp(el){var pos=[0,0];pos[0]=el.offsetLeft;pos[1]=el.offsetTop;while((el=el.offsetParent) != null){pos[0]+=el.offsetLeft;pos[1]+=el.offsetTop;}return pos;}
function LA_Align(a,b,c,d,e,f){el=LA_GetE(a);if(el!=null){oc=getpp(el);ox=oc[0];oy=oc[1];ow=el.offsetWidth;oh=el.offsetHeight; ofh=parseInt(c);ofv=parseInt(e);lw=LA_FLASH_INIT_SIZE[0];lh=LA_FLASH_INIT_SIZE[1]; /*lw=LA_GetE(\"LA_DIV\").offsetWidth;lh=LA_GetE(\"LA_DIV\").offsetHeight;*/rx=0;ry=0;if(b=="LEFT"){rx = ox - (lw/2) - ofh;}else if(b=="RIGHT"){rx = ox + ow + (lw/2) + ofh;}else if(b=="MIDDLE"){rx = ox + (ow/2) + ofh;}if(d=="TOP"){ry = oy - ofv;}else if(d=="BOTTOM"){ry = oy + oh + lh + ofv;}else if(d=="MIDDLE"){ry = oy + (oh/2) + (lh/2) + ofv;}return (rx+","+ry);}}
function LA_resizePlayer(aSizeTxt){var s = aSizeTxt.split(",");if (LAPObject != null) {LAPObject.width = s[0];LAPObject.height = s[1];}}
function LA_placeUIText(aPosTxt){var p=aPosTxt.split(",");el=LA_GetE("uitext");if(el!=null){el.style.left=p[0]+"px";el.style.top=p[1]+"px";}}
function LA_hideUIText(){el=LA_GetE("uitext");if(el!=null){el.style.left="-3000px";}}
function LA_updateBBox(aRectTxt){dbb=LA_GetE("LA_DIVBB");if(dbb!=null){r=aRectTxt.split(",");dbb.style.left=r[0]+"px";dbb.style.top=r[1]+"px";dbb.style.width=r[2]+"px";dbb.style.height=r[3]+"px";}}
function LA_placePlayer(aPosTxt){var p=aPosTxt.split(",");lad=LA_GetE("LA_DIV");if(lad!=null){dx=p[0]-lad.offsetLeft;dy=p[1]-lad.offsetTop;lad.style.left=p[0]+"px";lad.style.top=p[1]+"px";}} // check dx dy ?
function LA_Actor(){this.sc = new Array();this.ex0=function(n){return this.sc.push(n +"()");};this.ex1=function(n,p1){return this.sc.push(n +"(" + p1 + ")");};this.ResetCookies = function(){return this.ex0("ResetCookies");};this.Hide = function(){return this.ex0("Hide");};this.SetLang = function(l){return this.ex1("SETLANG",l);};this.Play = function(anim){return this.ex1("Play",anim);};this.Scale = function(factor){return this.ex1("SetScale",factor);};this.GetAnimations = function(){return this.ex0("GETANIMATIONS");};this.Show = function(){return this.ex0("Show");};this.Speak = function(txt){return this.sc.push('Speak("' + txt  + '")');};this.Stop = function(){return this.ex0("Stop");};this.Flush = function(){if(document.all){fl=document.all["LIVINGACTOR_FLASH"];}else{fl=document.LIVINGACTOR_FLASH;}fl.SetVariable("JS_SCRIPT",ACTOR.sc.join("<&#&>"));ACTOR.sc = new Array;};} 
function LA_hideActorDiv(){if(LA_GetE("LA_DIV")!=null){LA_GetE("LA_DIV").style.left="-3000px";}}
function LA_PlaySequence(aName){LAPObject.SetVariable("JS_SEQUENCEPLAY",aName);}
function LA_OnInit(){LA_CheckUserInput();if(typeof ASol_OnInit == 'function'){ASol_OnInit();}} 
if(console==undefined){var console={};console.log=function(){};}
function LA_AddAsolParameter(key, value){
	document.LIVINGACTOR_FLASH.SetVariable("JS_ASADDKEY",(key+","+value));
}
function ModeEmploi(){LA_PlaySequence("ModeEmploi");}
LA_Integration();
