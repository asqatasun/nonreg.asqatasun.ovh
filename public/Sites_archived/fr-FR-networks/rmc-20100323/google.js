var allready_called;

/**
 * pr�charge les pubs google et appel en callback google_ad_request_done()
 * 
 * @param client nom du client ex: ca-01net-radios
 * @param channel nom du channel ex: accueil_RMC
 */
function gAfcInit( client, channel ) {
  if (allready_called != 1)
    {
    google_ad_client = client;
    g_ad_channel = channel;
    google_ad_channel = channel + ' format2';
    google_ad_output = 'js';
    google_max_num_ads = '4';
    google_language = 'fr';
    google_safe = 'high';
    google_adtest = 'off';
    google_ad_section = 'default';
    
    document.write( '<scr'+'ipt language="Java'+'Scr'+'ipt" src="http://pagead2.googlesyndication.com/pagead/show_ads.js?"></scr'+'ipt>' );
    allready_called = 1
    }
  }


/**
 * Callback que google va appeler
 */
function google_ad_request_done( google_ads ) {
  var s = '';
  var i;
  zSr = new Array();
  
  if (google_ads.length == 0) 
    return;
  else
    {
    for(i=0;i<6;i++)
      zSr[i] = "";
      
    for(i=0; i < google_ads.length; ++i)
      {
      zSr[6+(i*6)] = google_ads[i].line2 + ' ' + google_ads[i].line3;
      zSr[7+(i*6)] = "";
      zSr[8+(i*6)] = google_ads[i].url;
      zSr[9+(i*6)] = google_ads[i].line1;
      zSr[10+(i*6)] = google_ads[i].visible_url;
      zSr[11+(i*6)] = "";
      }
    }
  
  return;
  }


/**
 * Affichage des pub google pour rmc dans un ID
 * 
 * @param n nombre de pub � afficher
 * @param id du html
 */
function rmc_google( n, id ) {
  var tmp = "", gAfcText = "", gAfcText2 = "";
  var sep = 0;
  var k;

  if( zSr )
    {
    lmax = 350;

    if (zSr.length > 6)
      {
      gAfcText += '<div style="padding: 0 5px;"><div style="border: 1px solid rgb(75, 94, 137); margin: 10px 0px; width: 100%;"><div style="background: transparent url(http://www.rmc.fr/images/google/gris.gif) repeat-x scroll 0% 0%; font-size: 13px; font-family: arial; font-weight: bold; color: black; height: 20px; padding-top: 3px;">&nbsp;<a href="https://www.google.com/adsense/support/bin/request.py?contact=abg_afc" target=_blank><span style="font-size: 13px; font-family: arial; font-weight: bold; color: black;">Liens sponsoris&eacute;s</span></a></div>';

      for( k = 6; k < zSr.length; k += 6 )
        {
        if ( k < ((6*n)+1))
          {
          lmax = 350;
          lprise = zSr[k+3].length + zSr[k+4].length;
          lreste = lmax - lprise;
          ldepart = zSr[k].length;
          zSr[k] = zSr[k].substring(0,lreste);

          if (ldepart > zSr[k].length)
            {
            zSr[k] = zSr[k].substring(0,(zSr[k].lastIndexOf( " " ))) + "...";
            }

          gAfcText += '<div style="background:#ffffff;"><div style="padding: 8px; font-family: arial; font-size: 12px; width: 100%;">';
          gAfcText += '<a href="'+zSr[k+2]+'" style="font-size: 13px; font-family: arial; color: #223BB3; text-decoration: none; font-weight: bold;" target="_blank">'+zSr[k+3]+'</a><br />';
          gAfcText += '<a href="'+zSr[k+2]+'" style="font-size: 12px; font-family: arial; text-decoration: none; color: rgb(137, 137, 137);" target="_blank">'+zSr[k]+'</a><br />';
          gAfcText += '<a href="'+zSr[k+2]+'" style="font-size: 11px; color: rgb(214, 48, 48); text-decoration: none; font-family: arial;" target="_blank">'+zSr[k+4]+'</a>';
          gAfcText += '</div></div>';
          }
        }

      gAfcText += '</div></div>';
      }
    }

  document.getElementById( id ).innerHTML = gAfcText;
  }
