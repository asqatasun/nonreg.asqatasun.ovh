/*
 * Cherie FM, Fr�quences
 * Tous droits r�serv�s (c) Copyright 2008, NRJ GROUP
 */

var MaxCoeursH = 5;

function TransmitVoteH(PK, Position, Type, Header)
{
	if( !Header ) Header = '_h';

	Nrj_Library.Ajax(
			url_base + "/vote/register",
			'!tool' + Position + Header, 
			{note: Position, key: PK, type: Type}
	); 
}

function CoeurPositionH(Position, Header)
{
	if( !Header ) Header = '_h';
	
	CoeurFlushH(Header);

	// Coeurs pleins
	for (i=1; i <= Position; i++)
	{
		document.getElementById('coeur_' + i + Header).className = "coeur_plein";
	}
}

function CoeurReinitH(Header, NbCoeur)
{
	if( !Header ) Header = '_h';
	if( typeof NbCoeur == 'undefined' ) NbCoeur = InitialCoeursH;
	
	CoeurFlushH(Header)

	for (i=1; i <= NbCoeur; i++)
	{
		document.getElementById('coeur_' + i + Header).className = "coeur_plein";
	}
}

function CoeurFlushH(Header)
{
	if( !Header ) Header = '_h';
	
	for (i=1; i <= MaxCoeursH; i++)
	{
		document.getElementById('coeur_' + i + Header).className = "coeur_vide";
	}
}