/*
 * Cherie FM, Fr�quences
 * Tous droits r�serv�s (c) Copyright 2008, NRJ GROUP
 */

var MaxCoeurs = 5;

function TransmitVote(PK, Position, Type)
{
	Nrj_Library.Ajax(url_base + "/vote/register", "tool" + Position, {note: Position, key: PK, type: Type});
	//nrj_xmlRequest.ajaxRequest({type:"POST", url: url_base + "/vote/register", flux: "note="+Position+"&key="+PK+"&type="+Type, indicator: url_base + "/images/ajax_loader.gif", id_dest: document.getElementById('tt'+Position), start: " ", callback: callback});
}

function TransmitVoteNRJPlayer(PK, Position, Type)
{
	Nrj_Library.Ajax(url_base + "/vote/register", "player_vote", {player: 'actived', note: Position, key: PK, type: Type});
	//nrj_xmlRequest.ajaxRequest({type:"POST", url: url_base + "/vote/register", flux: "player=actived&note="+Position+"&key="+PK+"&type="+Type, indicator: url_base + "/images/ajax_loader.gif", id_dest: "player_vote", start: " ", callback: callback });
}

function CoeurPosition(Position)
{
	CoeurFlush();

	// Coeurs pleins
	for (i=1; i <= Position; i++)
	{
		document.getElementById('coeur_'+i).className = "coeur_plein";
	}
}

function CoeurReinit()
{
	CoeurFlush()

	for (i=1; i <= InitialCoeurs; i++)
	{
		document.getElementById('coeur_'+i).className = "coeur_plein";
	}
}

function CoeurFlush()
{
	for (i=1; i <= MaxCoeurs; i++)
	{
		document.getElementById('coeur_'+i).className = "coeur_vide";
	}
}