var nrj_mea = {
	currentElement:null,
	currentElementContainer:null,
	timer:null,
	nextElement:0,
	lastElement:0,
	animationEnCours: false,
	currenti: 0,
	changed: 0,
	
	init:function(){

		var blocMea = document.getElementById("items_mea");
		if(blocMea){
		
			var listeItems = blocMea.getElementsByTagName("li");
			var listeDivs = document.getElementsByClassName("mea_blockers");
	
			for (var i=0; i < listeItems.length; i++) {

				Event.observe(listeItems[i], 'click',  function(event) { nrj_mea.openBloc(Event.element(event), listeDivs[Event.element(event).id]); } );
				
				if(Prototype.Browser.IEVersion == 6 ) listeDivs[i].style.display = 'none';
				listeDivs[i].setStyle({ opacity: '0' });
				
			}
			
			nrj_mea.openBloc(listeItems[0], listeDivs[0]);
		}
		
		new PeriodicalExecuter( 
			function() { 
			
				var demain=new Date();
				
				if((demain.getTime() - nrj_mea.changed) > 5500)
				{
					nrj_mea.openBloc(listeItems[nrj_mea.nextElement], listeDivs[nrj_mea.nextElement]);
				}
				else
				{
					return false;
				}
			}, 
			7);
	},
	openBloc:function(e, toShow){

		var tempThis = this;
		if(e.nodeName != undefined){tempThis = e;}
		
		if(tempThis != nrj_mea.currentElement)
		{
			tempThis.addClassName("active");
			if(nrj_mea.currentElement != null) nrj_mea.currentElement.removeClassName("active");
			
			var demain=new Date(); 
			nrj_mea.changed = demain.getTime();
			
			nrj_mea.showMe(toShow);
			nrj_mea.currentElement = tempThis;
			nrj_mea.currentElementContainer = toShow;
			nrj_mea.launchAnim();
		}
	},
	launchAnim:function(){
		var currentId=0;
		var blocMea = document.getElementById("items_mea");
		var listeItems = blocMea.getElementsByTagName("li");
		var listeDivs = document.getElementsByClassName("mea_blockers");
		
		for (var i=0; i < listeDivs.length; i++) {
			if(listeDivs[i] == nrj_mea.currentElementContainer){
				currentId = i;
				break;
			}
		};
		
		var nextElement;
		
		// Last, Next, Elements
		if((currentId + 1) >= listeDivs.length){nextElement = 0; nrj_mea.nextElement = 0; }else{nextElement = (currentId + 1); nrj_mea.nextElement = (currentId + 1);}
	
		if((nrj_mea.nextElement - 2) == 0){ nrj_mea.lastElement = 0; }
		if((nrj_mea.nextElement - 2) > 0){ nrj_mea.lastElement = (nrj_mea.nextElement - 2 ); }
		if((nrj_mea.nextElement - 2) < 0){ nrj_mea.lastElement = listeDivs.length -1; }
		if(nrj_mea.nextElement == 0){ nrj_mea.lastElement = listeDivs.length - 2; }
	},
	showMe:function(element)
	{
		var blocMea = document.getElementById("items_mea");
		var listeItems = blocMea.getElementsByTagName("li");
		var listeDivs = document.getElementsByClassName("mea_blockers");

		if(nrj_mea.currentElementContainer != null)
		{
			nrj_mea.currentElementContainer.removeClassName("active");
			//nrj_mea.currentElementContainer.anim = new Anim(nrj_mea.currentElementContainer, {opacity:{to:0}}, 0.5);
			nrj_mea.animationEnCours = true;
			
			$(nrj_mea.currentElementContainer).morph("opacity:0", {duration: 0.5, afterFinish: function() 
				{
					nrj_mea.animationEnCours = false;  
					// Correction: tout doit être bien fermé (les autres)
					for (var i=0; i < listeDivs.length; i++) {
						if(listeDivs[i].id != nrj_mea.currentElementContainer.id)
						{
							$(listeDivs[i]).style.display = 'none';
						}
					}
				} });
			
			if(Prototype.Browser.IEVersion == 6) $(nrj_mea.currentElementContainer).style.display = 'none';
		}
		
		element.addClassName( "active");
		element.morph("opacity:1", {duration: 0.5});
	
		element.setStyle({display: "block"} );
	}
}
