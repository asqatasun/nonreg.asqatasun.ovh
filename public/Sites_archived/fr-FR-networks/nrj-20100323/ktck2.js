/**
 * @copyright Keyade, 2008-2009
 */
var ___kCk = {
	set : function( n, v, d, dmn ) {
		var exp = '';
		if ( d != undefined && d != -1 ) {
			var dt = new Date();
			dt.setTime( dt.getTime() + ( 86400000 * parseFloat( d ) ) );
			exp = '; expires=' + dt.toGMTString();
		}
		if ( dmn == undefined ) {
			var loc = ( window.top != window ) ? window.parent.location : window.location;
			dmn = loc.hostname.replace( /^www\./i, '.' );
		}
		return ( document.cookie = escape( n ) + '=' + escape( v || '' ) + exp + '; domain=' + dmn + '; path=/' );
	},
	get : function( n ) {
		var c = document.cookie.match( new RegExp( '(^|;)\\s*' + escape( n ) + '=([^;\\s]*)' ) );
		return ( c ? unescape( c[2] ) : null );
	},
	erase : function( n ) {
		var c = ___kCk.get( n ) || true;
		___kCk.set( n, '', -1 );
		return c;
	},
	accept : function() {
		if ( typeof navigator.cookieEnabled == 'boolean' ) {
			return navigator.cookieEnabled;
		}
		___kCk.set( '_test', '1' );
		return ( ___kCk.erase( '_test' ) ===  '1' );
	}
};

var ___kPQs = function() {
	var a = this;
	a.params = {};
	a.qs = '';
		
	a.get = function( key, default_ ) {
		var value = a.params[key];
		return ( value != null ) ? value : default_ ;
	};
	
	a.contains = function( key ) {
		var value = a.params[key];
		return ( value != null );
	};
	
	a.loc = ( window.top != window ) ? window.parent.location : window.location;
	a.qs = a.loc.search.substring( 1, a.loc.search.length ).replace( /\?/g, '&' );
	
	if ( a.qs.length == 0 ) return;

	a.qs = a.qs.replace(/\+/g, ' ');
	
	var args = a.qs.split( '&' ); 
	
	for (var i = 0; i < args.length; i++) {
		var pair = args[i].split( '=' );
		var name = decodeURIComponent( pair[0] );
		
		var value = ( pair.length == 2 )
			? decodeURIComponent( pair[1] )
			: name;
		
		a.params[name] = value;
	}
};

var ___kTpv = function( cId, tckDomain, ckeDomain ) {
	var a = this;
	a.cIdTag = 'cId';
	a.eIdTag = 'eId';
	a.tckIdTag = 'trackingId';
	a.urlTag = 'url';
	a.eVal1Tag = 'kaEvVal1';
	a.eVal2Tag = 'kaEvVal2';
	a.eVal3Tag = 'kaEvVal3';
	
	a.inSession = -1;
	
	a.cId = ( cId !== undefined && cId != 'undefined' ) ? cId : 0;
	a.eId = 0;
	a.tckId = null;
	a.eVal1 = 0;
	a.eVal2 = 0;
	a.eVal3 = 0;
	
	a.tckDomain = ( tckDomain !== undefined && tckDomain != 'undefined' ) ? tckDomain : '';
	a.loc = ( window.top != window ) ? window.parent.location : window.location;
	a.host = a.loc.hostname.replace( /^www\./ig, '' );
	a.ckeDomain = ( ckeDomain !== undefined && tckDomain != 'undefined' ) ? ckeDomain.replace( /^www\./i, '.' ) : '.' + a.host;
	
	a.url = '';

	a.imgPath = '/kpv/0/';
	
	a.prevCalls = new Array();
	
	a.foo = function() {
		return;
	};
	
	a.setEventId = function( id ) {
		a.eId = id;
	};
	
	a.setClientId = function( id ) {
		a.cId = id;
	};

	a.setTckId = function( id ) {
		a.tckId = id;
	};
	
	a.setEvals = function( val1, val2, val3 ) {
		a.eVal1 = val1;
		if ( val2 !== undefined && val2 != 'undefined' ) {
			a.eVal2 = val2;
		}
		if ( val3 !== undefined && val3 != 'undefined' ) {
			a.eVal3 = val3;
		}		
	};
	
	a.setDomain = function( ckeDomain ) {
		ckeDomain = ckeDomain.replace( /^www\./i, '.' );
		if ( ckeDomain.indexOf( '.' ) == 0 ) {
			a.ckeDomain = ckeDomain;
		}
		else {
			a.ckeDomain = '.' + ckeDomain;
		}
	};
	
	a.setTckDomain = function( tckDomain ) {
		a.tckDomain = tckDomain;
	};
	
	a.addTckIdToLink = function( anc ) {
		a.checkSession();
		if ( a.tckId !== null ) {
			anc.href += ( ( anc.href.indexOf( '?' ) > -1 ) ? '&' : '?' ) + 'kTckId=' + encodeURIComponent( a.tckId );
		}
	};
	
	a.trackPageView = function( eId, lEid ) {	
		a.checkSession();
		
		if ( a.inSession == 0 ) {
			if ( lEid !== undefined && a.tckId != null ){
				eId = lEid;
			}
			else {
				return true;
			}
		}
		
		if (  a.loc.protocol == 'https:' ) {
			return true;
		}

		var qs = '';
		
		if ( eId === undefined && eId != 'undefined' ) {
			eId = a.eId;
		}
		
		var currentCall = a.cId + eId;
		for( call in a.prevCalls ) {
			if ( a.prevCalls[call] == currentCall ) {
				return true;
			}
		}
		a.prevCalls.push( currentCall );
		
		qs += '?' + a.cIdTag + '=' + encodeURIComponent( a.cId );
		qs += '&' + a.eIdTag + '=' + encodeURIComponent( eId );
		qs += '&' + a.tckIdTag + '=' + encodeURIComponent( a.tckId );
		if ( a.eVal1 != 0 ) {
			qs += '&' + a.eVal1Tag + '=' + encodeURIComponent( a.eVal1 );
		}
		if ( a.eVal2 != 0 ) {
			qs += '&' + a.eVal2Tag + '=' + encodeURIComponent( a.eVal2 );
		}
		if ( a.eVal3 != 0 ) {
			qs += '&' + a.eVal3Tag + '=' + encodeURIComponent( a.eVal3 );
		}
		qs += '&' + a.urlTag + '=' + encodeURIComponent(
			a.loc.toString().replace( /https?:\/\//g, '' )
					.replace( /^www\./g, '' ).replace( /&?(trackingId|kTckId|slid)=[0-9]+/gi, '' )
					.replace( /&?(xtor|xts|xtdt|gclid)=[^&]+/gi, '' )
					.replace( /\?&/g, '?' )
					.replace( /#\s*$/g, '' )
					.replace( /\?\s*$/g, '' )
					.replace( /\/\s*$/g, '' )
		);
		
		a.url = '';
		if ( a.tckDomain == '' ) {
			a.url = 'imp.keyade.com';
		}
		else if ( a.tckDomain != '' ) {
			a.url = a.tckDomain;
		}
		
		a.url = a.loc.protocol + '//' + a.url + a.imgPath + qs;
	
		var img = new Image( 1, 1 );
		img.src = a.url;
		img.onload = function() { a.foo() };
		return true;
	}
	
	a.checkSession = function() {
		if ( a.inSession == -1 ) {
			if ( !_kTck.idCatched ) {
				var qs = new ___kPQs();
				if ( qs.contains( 'trackingId' ) ) {
					a.tckId = qs.get( 'trackingId' );
				}
				else if ( qs.contains( 'kTckId' ) ) {
					a.tckId = qs.get( 'kTckId' );
				}
				else if ( qs.contains( 'slid' ) ) {
					a.tckId = qs.get( 'slid' );
				}
			
				_kTck.idCatched = true;
			}
		
			if ( a.tckId == null ) {
				var tck1 = ___kCk.get( 'K_' + a.cId );
				var tck2 = ___kCk.get( 'K_lm_' + a.cId );
				var tck3 = ___kCk.get( 'K_sess_' + a.cId );
					
				if ( tck1 == null || tck2 == null || tck3 == null ) {
					if ( tck1 != null ) {
						a.tckId = tck1;
						_kTck.tckId = tck1;
					}
					else {
						_kTck.tckId = null;
					}
					a.inSession = 0;
				}
				else if ( tck1 == tck2 && tck2 == tck3 ) {
					a.tckId = tck1; 
					_kTck.tckId = tck1;
					a.inSession = 1;
					___kCk.set( 'K_lm_' + a.cId, a.tckId, 1 / 24 / 2, a.ckeDomain );
				}
				else {
					a.inSession = 0;
				}
			}
			else {
				_kTck.tckId = a.tckId;
				___kCk.set( 'K_' + a.cId, a.tckId, 30, a.ckeDomain );
				___kCk.set( 'K_lm_' + a.cId, a.tckId, 1 / 24 / 2, a.ckeDomain );
				___kCk.set( 'K_sess_' + a.cId, a.tckId, -1, a.ckeDomain );
				a.inSession = 1;
			}
		}
	}
}

var _kTck = {
	tckId: null,
	nav : '',
	getNav : function() {
		if ( _kTck.nav == '' ) {
			if ( navigator.userAgent ) {
				if ( navigator.userAgent.indexOf( 'MSIE' ) != -1 ) {
					_kTck.nav = 'ie';
				}
				else if ( navigator.userAgent.indexOf( 'AppleWebKit' ) != -1
					|| navigator.userAgent.indexOf( 'Safari' ) != -1
					|| navigator.userAgent.indexOf( 'KHTML' ) != -1
					|| navigator.userAgent.indexOf( 'Chrome' ) != -1
					|| navigator.userAgent.indexOf( 'OmniWeb' ) != -1
					|| navigator.userAgent.indexOf( 'iCab' ) != -1 ) {
					_kTck.nav = 'sf'
				}
				else if ( navigator.userAgent.indexOf( 'Firefox' ) != -1
					|| navigator.userAgent.indexOf( 'Camino' ) != -1
					|| navigator.userAgent.indexOf( 'Netscape' ) != -1 
					|| navigator.userAgent.indexOf( 'Gecko' ) != -1 ) {
					_kTck.nav = 'ns';
				}
				else {
					_kTck.nav = 'ot';
				}
			}
			else {
				_kTck.nav = 'ot';
			}
		}
		return _kTck.nav;
	},
	idCatched : false,
	addEventCallback : function( obj, eventName, callback ) {
		if( obj.addEventListener ) { 
			obj.addEventListener( eventName, callback, false );
		}
		else if( obj.attachEvent ) {
			obj.attachEvent( 'on' + eventName, callback );
		}
		return;
	},
	getViewTracker : function( cId, tckDomain, ckeDomain ) {
		return new ___kTpv( cId, tckDomain, ckeDomain );	
	},
	catchId : function( cId, lifeTime, ckeDomain ) {
		if ( _kTck.idCatched ) {
			return _kTck.tckId;
		}
		
		if ( lifeTime === undefined || lifeTime == 'undefined' ) {
			lifeTime = 30;
		}
		if ( ckeDomain === undefined || ckeDomain == 'undefined' ) {
			var loc = ( window.top != window ) ? window.parent.location : window.location;
			ckeDomain = loc.hostname.replace( /^www\./ig, '.' );
		}
		else if ( ckeDomain.indexOf( '.' ) != 0 ) {
			ckeDomain = '.' + ckeDomain;
		}
	
		var qs = new ___kPQs();
		
		if ( qs.contains( 'trackingId' ) ) {
			_kTck.tckId = qs.get( 'trackingId' );
		}
		else if ( qs.contains( 'kTckId' ) ) {
			_kTck.tckId = qs.get( 'kTckId' );
		}
		else if ( qs.contains( 'slid' ) ) {
			_kTck.tckId = qs.get( 'slid' );
		}
	
		if ( _kTck.tckId != null ) {
			___kCk.set( 'K_' + a.cId, a.tckId, 30, a.ckeDomain );
			___kCk.set( 'K_lm_' + a.cId, a.tckId, 1 / 24 / 2, a.ckeDomain );
			___kCk.set( 'K_sess_' + a.cId, a.tckId, -1, a.ckeDomain );
		}
		else {
			_kTck.tckId = ___kCk.get( 'K_' + cId );
		}
		_kTck.idCatched = true;
		return _kTck.tckId;
	}
};

