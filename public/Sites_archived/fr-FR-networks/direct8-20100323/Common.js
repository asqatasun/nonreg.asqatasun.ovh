// global web path
var webhost = "";

// set javascript settings
function Loading(www) {
	webhost = www;
}

// common javascript actions
var CommonAction = {
	
	rules :				{
		
		'.ajax' : function(el) {
			el.onsubmit = function() {
				ajaxOnsubmit(el);
				
			},
			el.onclick = function() {
				//ajaxOnclick(el);
			}
		},
		
		'.ajaxLink' : function(el) {
			el.onclick = function() {
				ajaxOnclick(el);
			}
		},
		
		'lastbutnotleast': {}
	}
}

// resgister actions
Behaviour.register(CommonAction.rules);

function findParamInClass(param, el) {
   var regexp = new RegExp(param + '_([A-Za-z0-9/:?&\-\._]+)');
   var mymatch = regexp.exec(el.className);
   if(mymatch) {
       return mymatch[1];
   }
   return false;
}

function ajaxOnsubmit(el) {
	var params = "";
	
	if (findParamInClass('id', el)) {
		var id = findParamInClass('id', el);
		//alert('id = ' + id);
	}
	if (findParamInClass('object', el)) {
		var object = findParamInClass('object', el);
		//alert('object = ' + object);
	}
 	if (findParamInClass('action', el)) {
		params += "&action=" + findParamInClass('action', el);
	}
	if (findParamInClass('url', el)) {
		params += "&url=" + findParamInClass('url', el);
	}
	
 	var options = {
        url: webhost + 'ajax/?object=' + object + params,
        method: 'post',
        update: $(id), // element to update
        enctype: 'multipart/form-data',
        evalScripts: true,
        onRequest: function() { // when request is done
        },
        onComplete: function() { // when request is complete
        	Behaviour.apply(CommonAction.rules);
        }
    }
    
    var AjaxObject = new Request.HTML(options);
    AjaxObject.send(el);
    // If element is form
	if (el.nodeName == 'FORM') {
		// display loading 
		var obj;
		obj = el.getElementsByTagName("input");
		for(i = 0;i < obj.length; i++){
			if (obj[i].type == "submit") {
				obj[i].parentNode.innerHTML = '<img src="' + webhost + 'javascript/loading.gif" height="16px" border="0">';
			}
		}
	}
}

function ajaxOnclick(el) {
	
	var params = "";
	
	if (findParamInClass('id', el)) {
		var id = findParamInClass('id', el);
	}
	if (findParamInClass('object', el)) {
		var object = findParamInClass('object', el);
		params += "object=" + object;
	}
	if (findParamInClass('action', el)) {
		var action = findParamInClass('action', el);
		params += "&action=" + action;
	}
	if (findParamInClass('code', el)) {
		var code = findParamInClass('code', el);
		params += "&code=" + code;
	}
	if (findParamInClass('url', el)) {
		params += "&url=" + findParamInClass('url', el);
	}
	
	if (findParamInClass('confirm', el)) {
		if (!confirm("Supprimer ?")) { // Clic sur OK
			return 0;
		}
	}
	
	//alert(params);
	var options = {
        url: webhost + 'ajax/?' + params,
        method: 'post',
        update: $(id), // element to update
        evalScripts: true,
        onRequest: function() { // when request is done
        },
        onComplete: function() { // when request is complete
        	Behaviour.apply(CommonAction.rules);
        }
    }
    
    var AjaxObject = new Request.HTML(options);
    AjaxObject.send(el);
}

function deleteProgram(id) {
	document.getElementById(id).innerHTML = '';
}

function deleteBrowserObject(id) {
	document.getElementById(id).innerHTML = '';
}