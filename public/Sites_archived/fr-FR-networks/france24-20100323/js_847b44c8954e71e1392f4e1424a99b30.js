/**
 * Helper function to parse a querystring.
 * Ovverriding the function in views/js/base.js !
 */

Drupal.Views = Drupal.Views || {};

Drupal.Views.parseQueryString = function (query) {
  var args = {};
  var pos = query.indexOf('?');
  if (pos != -1) {
    query = query.substring(pos + 1);
  }
  var pairs = query.split('&');
  for(var i in pairs) {
    var pair = pairs[i].split('=');
    // Ignore the 'q' path argument, if present.
    // if (pair[0] != 'q' && pair[1]) {   <--   original views code
    if (pair[0] != 'q') {
      args[pair[0]] = decodeURIComponent(pair[1].replace(/\+/g, ' '));
    }
  }
  return args;
};
;// Begin Sitestat code
function sitestat(ns_l){// FromUrl v1.5 Copyright (c) 2001-2008 Nedstat B.V. All rights reserved.
  var ns_type=''// leave empty for normal/ppc measurement, fill in for clickin, clickout or pdf
  var r=''// yes=only parse url when there is a real document.referrer,
          // no=only parse url when there is no real document.referrer, empty=always parse url
  var t='?'// tag in url where parameters follow; default '?' could be replaced by "#"
  var p=new Array();var w='';var l='';var d=document;var n=navigator;var ns_0=''
  ;if(top!=self){if('\u0041'=='A'){var u=n.userAgent;if(u.indexOf('Safari')==-1)
  {var b=u.indexOf('Opera');if(b==-1||(u.charAt(b+6)+0)>5){b=u.indexOf('Mozilla'
  );var xb=b!=-1?u.charAt(b+8)>4:1;if(u.indexOf('compatible')!=-1||xb){var c=
  'try{ns_0=top.document.referrer}catch(e){}';eval(c);c=
  'try{l=top.document.location.href}catch(e){}';eval(c);}}}}}else{ns_0=
  d.referrer;l=d.location.href;}if(ns_0.lastIndexOf('/')==ns_0.length-1){ns_0=
  ns_0.substring(ns_0.lastIndexOf('/'),0);}var f=ns_l.indexOf('?');if(f!=-1){
  var q=ns_l.substring(f+1);ns_l=ns_l.substring(0,f);if(q){var m=q.indexOf('&');
  w=q.substring(0,m==-1?q.length:m);if(w.indexOf('=')!=-1){w='';}if(w){q=
  q.substring(m==-1?q.length:m+1);q+=(q?'&':'')+'ns_name='+w;}if(ns_0.length>0){
  q+=(q?'&':'')+'ns_referrer='+escape(ns_0);}var s=0;var e=0;while(q.length){e=
  q.indexOf('&');if(e==-1){e=q.length;}var o=q.substring(s,e);if(o.substring(0,4
  )=='amp;'){o=o.substring(4);}if(o)p[p.length]=o;q=q.substring(e+1);}}}var a=
  l.indexOf(t);a=a==-1?0:l.substring(a+1);var j;if(r=='yes')j=ns_0.length;else
  if(r=='no')j=!ns_0.length;else if(r=='')j=1;if(a&&j){while(a.length){var e=
  a.indexOf('&');if(e==-1){e=a.length;}var k=a.substring(0,a.substring(0,e)
  .indexOf('='));var v=a.substring(a.substring(0,e).indexOf('=')+1,e);if(
  k.substring(0,4)=='amp;'){k=k.substring(4);}while(v.substring(0,1)=='='){v=
  v.substring(1);}if(k=='ns_name'){w=v;}else if(k=='ns_or'){var g='ns_referrer='
  ;for(var z=0;z<p.length;z++){if(p[z].substring(0,g.length)==g){p[z]=
  'ns_referrer='+v;}}}else{if(k.substring(0,3)=='ns_'&&v&&k){var h=0;for(var x=0
  ;x<p.length;x++){if(p[x].substring(0,p[x].indexOf('='))==k){p[x]=k+"="+v;h=1}}
  if(!h){p[p.length]=k+"="+v;}}}a=a.substring(e+1);}}if(!w){return;}var s='';
  var y='';for(var i=0;i<p.length;i++)if(p[i].substring(0,8)!='ns_name='){if(p[i
  ].substring(0,12)!='ns_referrer='){s+='&'+p[i];}else{y='&'+p[i];}}s+=ns_type?
  '&ns_type='+ns_type+'&ns_action=view':'';ns_pixelUrl=ns_l+'?'+w+"&ns__t="+(
  new Date()).getTime();ns_l=ns_pixelUrl+s+y;if(d.images){ns_1=new Image();
  ns_1.src=ns_l;}else{d.write('<img src='+ns_l+' width="1" height="1">');}}

// End Sitestat code
;function sitestat_stream(u){var d=document,l=d.location;ns_pixelUrl=u+"&ns__t="+(new Date().getTime());u=ns_pixelUrl+"&ns_c="+((d.characterSet)?d.characterSet:d.defaultCharset)+"&ns_ti="+escape(d.title)+"&ns_jspageurl="+escape(l&&l.href?l.href:d.URL)+"&ns_referrer="+escape(d.referrer);(d.images)?new Image().src=u:d.write('<'+'p><img src="'+u+'" height="1" width="1" alt="*"><'+'/p>');};
//sitestat_stream(ns_url_stream);
// End Sitestat code

;$(document).ready(function(){

  // add click handler on tabs, to refresh the tab content
  var blocksModules = Drupal.settings.aefPlayer.remote.blocks; 
  for (key in blocksModules) {
    $('#block-aef_player-'+ blocksModules[key] +' .tabs a.tab').click(function(context) {
      aef_remote_set_tab(this.id);
      return false;
    });
  }
  // We fake a click on the active tab to bind the play action
  //$('#block-aef_player-'+ blocksModules[key] +' .tabs a.active').click();
  // regularly refresh tab content
  $(document).everyTime(600000, function(i) {aef_remote_refresh_blocks();});

  // create a slave FlashBridge instance
  window.FlashBridgeInstance = new window.FlashBridgeClass();
  window.FlashBridgeInstance.swfPath = Drupal.settings.aefPlayer.flashBridgePath;
  //window.FlashBridgeInstance.jsCallbackPrefix = Drupal.settings.aefPlayer.playerId;
  window.FlashBridgeInstance.connectionNamePrefix = Drupal.settings.aefPlayer.playerId;
  window.FlashBridgeInstance.setCallbacks({
    "called":function(method,params) {
      window.france24_slave_remoteAPI[method].apply(window.FlashBridgeInstance,params);
    },
    "ready":function() {
      //console.log("ready!");
      if (window.location.hash == '#player' || Drupal.settings.aefPlayer.autoOpen) {
        $('.block-aef_player h3.title a').click();
      }
    }
  });

  var params = {};
  if(Drupal.settings.aefPlayer.flashBridgeColor != undefined)
    params = {bgcolor: Drupal.settings.aefPlayer.flashBridgeColor};
  window.FlashBridgeInstance.insert(false, params);

  
  // insert slider @TODO: use better method than a timer
  $(document).oneTime(2000, function(i) {
    aef_remote_build_controls();
    if (window.FlashBridgeInstance.masterConnected) {
      $('#block-aef_player-'+ blocksModules[key] +' .controls').show();
      window.FlashBridgeInstance.sendToMaster("getData", []);
    }
  });
  
  // security: if the controlbar is not closed when closing popup: check regularly if master is connecter
  $(document).everyTime(5000, function() {
    if (!window.FlashBridgeInstance.masterConnected) {
      window.france24_slave_remoteAPI.close();
    }
  });
  
  // add a click handler on links containing the site URL, and finishing with #player
  $('a[href$=#player]').live('click', function () {
    if (this.href.match(String(window.location))) {
      $('.block-aef_player h3.title a').click();
    }
  });
  
});

function aef_remote_refresh(tabId, newTab, blockDelta) {
  if (!tabId) {
    //alert('error: tabId is empty');
  }
  if (!blockDelta) {
    blockDelta = $('#'+ tabId).parents('.block-aef_player').eq(0).attr('id').substr(17);
  }

  if (newTab || Drupal.settings.aefPlayer.remote.tabs[tabId].server_refresh) { // server refresh
    var data = $.getJSON(Drupal.settings.basePath + 'aef_player_remote', { 'module' : blockDelta, 'id': tabId },
      function(data) {
        //alert(String(data) === "[object Object]");
        //if (String(data) === "[object Object]") {
          for (key in data) {
            $('#block-aef_player-'+ blockDelta +' .content '+ key).html(data[key]);
          }
        /*}
        else {
          $('#block-aef_player-'+ blockDelta +' .content .tab-content').html(data);
          
          // get the id of the tab
          var linkid = tabId.match(/aef_remote_tab_(.*)?/);
          $('.tab-content .'+linkid[1]).click(function () {
            var tab = Drupal.settings.aefPlayer.remote.tabs[linkid[1]];
            
            window.FlashBridgeInstance.api_play(tab['url'], tab['type'], tab['title'], tab['streamer']);
            return false;
          });

        }*/
      }
    );
  }/*
  else { // client refresh: ask the player what it is playing
    // @TODO
  }*/
}

function aef_remote_set_tab(tabId) {
  aef_remote_refresh(tabId, 1);
  $('#'+ tabId).parents('.tabs').eq(0).children('.tab.active').removeClass('active');
  $('#'+ tabId).addClass('active');
}

function aef_remote_refresh_blocks() {
  var blocksModules = Drupal.settings.aefPlayer.remote.blocks; 
  for (var i = 0; i < blocksModules.length; i++) {
    tabId = $('#block-aef_player-'+ blocksModules[i] +' .tabs a.tab.active').attr('id');
    aef_remote_refresh(tabId, 0, blocksModules[i]);
  }
}

function aef_remote_build_controls() {
  $('.controlbar #progress .slider').slider({'animate': true, 'range': 'min'});
  $('.controlbar #volume .slider').slider({'range': 'min', 'value': 100});

  $('.controlbar #progress .slider').bind('slide', function (event, ui) {
  //console.log(ui);
    window.FlashBridgeInstance.api_seek(ui.value);
  });
  $('.controlbar #volume .slider').bind('slidechange', function (event, ui) {
    window.FlashBridgeInstance.api_volume(ui.value);
  });
  $('.controlbar #play a').click(function() {
    window.FlashBridgeInstance.api_pause();
    return false;
  });
  $('.controlbar #stop a').click(function() {
    window.FlashBridgeInstance.api_stop();
    return false;
  });
  $('.controlbar #mute a').click(function() {
    window.FlashBridgeInstance.api_mute();
    return false;
  });
  
}
;