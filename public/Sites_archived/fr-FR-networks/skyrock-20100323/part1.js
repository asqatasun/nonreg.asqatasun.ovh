
if(typeof(pub_sky_page) == 'undefined')
{
    var pub_sky_page = 'home';
}

var OAS_url = 'http://oas.skyregie.com/RealMedia/ads/';
var OAS_query = '?langue=fr';
var OAS_sitepage = 'Skyrock.FM/' + pub_sky_page;
var OAS_rn  = new String(Math.random());
var OAS_rns = OAS_rn.substring (2, 11);

if(typeof OAS_listpos != 'undefined' && OAS_listpos != '')
{
	document.write('<script type="text/javascript" src="' + OAS_url + 'adstream_mjx.ads/' + OAS_sitepage + '/1' + OAS_rns + '@' + OAS_listpos + OAS_query + '"></script>');
}

function OAS_NORMAL(pos)
{
	document.write('<a href="' + OAS_url + 'click_nx.ads/' + OAS_sitepage + '/1' + OAS_rns + '@' + pos + OAS_query + '" onclick="window.open(this.href); return false;"><img src="' + OAS_url + 'adstream_nx.ads/' + OAS_sitepage + '/1' + OAS_rns + '@' + pos + OAS_query + '" alt="Pub" /></a>');
}

function OAS_AD(pos)
{
	if (typeof(OAS_RICH) != 'undefined')
		return OAS_RICH(pos);
	return OAS_NORMAL(pos);
}

function publog_is_megabanpage_v2()
{
	if(typeof pub_no_megaban != 'undefined' && pub_no_megaban == true)
	{
		return false;
	}
	return true;
}

function publog_megaban_v2()
{
	return publog_is_megabanpage_v2();
}

function publog_is_megabanpage()
{
	return publog_is_megabanpage_v2();
}

if (typeof OasInnerHtml == 'undefined')
{
    function OasInnerHtml(element, content)
    {
        if(typeof document.getElementById(element) != 'object')
        {
            return;
        }
        document.getElementById(element).innerHTML = content;
    }
}
