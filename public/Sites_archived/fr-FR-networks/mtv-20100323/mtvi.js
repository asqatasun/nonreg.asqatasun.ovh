com.mtvi.reporting.Account={
	name:'viamtvfr',
	dynamicAccountSelection:'true',
	dynamicAccountList:'viamtvfrdev=mtv-fr.scenic-l.mtvi.com,mtv-fr.scenic-q.mtvi.com'	
};

var MTVNI = window.MTVNI || {};
MTVNI.qsData=com.mtvi.util.queryStringToHash(window.location.search);

com.mtvi.reporting.Controller.initialize();
com.mtvi.reporting.Controller.sendCall(
	{
			pageName:	com.mtvi.metadata.getDefaultPageName(),	
			channel:	com.mtvi.metadata.getDefaultChannel(),
			hier1:		com.mtvi.metadata.getDefaultPageName(),
			campaign: MTVNI.qsData.s_cid
	}
);

if (Flux) var _fLog = Flux.Context.isUserAuthenticated()? "1":"0";
var _adKeyValuesAddOn = "msite=flux_community;media=other;";

com.mtvi.ads.AdManager.setDartSite("france.mtv/community"); 
com.mtvi.ads.AdManager.setAdClass("InternationalAd");
com.mtvi.ads.AdManager.setPositionThreshold(2);
com.mtvi.ads.AdManager.setKeyValues("mpu=0;sky=0;overlay=0;"+_adKeyValuesAddOn);
