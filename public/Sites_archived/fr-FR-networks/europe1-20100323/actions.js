var unknown_error = function(){
	alert('Une erreur est survenue.');
	closeModal();
};


var europe1 = function(){ 
    $('#subscription').html("<div id='message'></div>");
    $('#connect').html("");
    $('#message').html("<h2>Vous &ecirc;tes &agrave; pr&eacute;sent membre du club Europe1. Merci de votre inscription !</h2>")
    .append("<p>Vous allez recevoir un mail avec vos identifiants de connexion.</p>")
    .append("<p>Avec votre compte Club Europe1, vous pouvez donner votre avis en ligne et participer aux op&eacute;rations du club.</p>")
    .append("<h2>D&eacute;couvrez le club des auditeurs !</h2>")
    .append("<p>Il vous est possible d'intervenir &agrave; l'antenne d'Europe1 pour donner votre avis sur un sujet d'actualit&eacute; qui vous int&eacute;resse</p>")
    .append("<p>Nous avons besoin de quelques informations suppl&eacute;mentaires afin de pouvoir vous contacter lorsque l'actualit&eacute; le permettra.</p>")        
    .append("<a href=''>Cliquez ici</a>")        
    .hide()
    .fadeIn(1500, function() {
      $('#message').append("<img id='checkmark' src='http://team.dev.lejdd.fr/jqform/GUI/images/check.png' />");
    });
}

var connected = function(user){
	var output = '\
		<div class="success">\
			<p>Vous &ecirc;tes d&eacute;sormais connect&eacute; sur le site Europe1.fr.</p>\
			<p>Cette page va se recharger.</p>\
		</div>';
	$('#modal-header h2').text('Bienvenue ' + user[0]['pseudo']);
	$('#modal-content').html(output);
	if( _.quizz && $('#qFormulaire').length ){
		window.location.replace(window.location + '&step=99');
	}
	else{
		closeModal({ refresh: true });
	}
	$('#modal-close,#modal-mask').live('click',function(){ location.reload(true); });
}

var disconnect = function(){ location.reload(true); };

var select_boxes = function(boxes){ 

	if(boxes[0]['pays']!='' && boxes[0]['jobs']!=''){
		$('#countries').html(boxes[0]['pays']);
		$('#jobs').html(boxes[0]['jobs']);
		$('#birthdate').html(boxes[0]['birthdate']);
	}

}
/*
var autolog = function(){
	user_info = sso.readCookie('dc_user_info');
	if(user_info['email']!=''){
		alert(user_info['email']);
	}
}
*/
var auditeurs = function(user){

			if(user[0]['pseudo']!=''){
				    pseudo = user[0]['pseudo'];
			}else{
					pseudo = '!';
			}
			var offset = 0;
			$('html, body').animate({ scrollTop: offset }, 500);
			$('#inscription').html("<div id='message'><h1>Merci " + pseudo + "</h1>\
			<p><img src=\"/includes/cobrand/img/ok.gif\" style=\"vertical-align:sub\" /> Votre inscription au club des auditeurs a bien &eacute;t&eacute; prise en compte.</p>\
			<p>Vous allez recevoir un email avec un rappel de vos identifiants de connexion.</p>\
			<p>Nous vous contacterons dans les meilleurs d&eacute;lais sur le num&eacute;ro de t&eacute;l&eacute;phone que vous avez renseign&eacute;.</p>\
			<p>Europe 1</p></div>");
			
	        /*
			$('#inscription').html("<div id='message'></div>");
	        $('#message').html("<h1>Merci " + pseudo + "</h1>")
	        .append("<p>Votre inscription au club des auditeurs a bien &eacute;t&eacute; prise en compte.</p>")      
	        .append("<p>Vous allez recevoir un email avec un rappel de vos identifiants de connexion.</p>")
	        .append("<p>Nous vous contacterons dans les meilleurs d&eacute;lais sur le num&eacute;ro de t&eacute;l&eacute;phone que vous avez renseign&eacute;.</p>")
	        .append("<p>Europe 1</p>")
	        .hide()
	        .fadeIn(1500, function() {
	          $('#message').append("<img id='checkmark' src='http://team.dev.lejdd.fr/jqform/GUI/images/check.png' />");
	          $('#connect').hide();
	        });
			*/
}

var club = function(user){
	var offset = $('#content').offset().top;
	$('html, body').animate({ scrollTop: offset }, 500);
	
	if(user[0]['pseudo']!=''){
		    pseudo = user[0]['pseudo'];
	}else{
			pseudo = '!';
	}
    $('#inscription').html("<div id='message' class='success'></div>");
    $('#message').html("<h1>Merci " + pseudo + "</h1>")
    .append("<p>Votre inscription au club Europe 1 a bien &eacute;t&eacute; prise en compte.</p>")      
    .append("<p>Un email de confirmation vient d'&ecirc;tre envoy&eacute; &agrave; l'adresse que vous avez renseign&eacute;. Merci d'en prendre connaissance afin de valider la cr&eacute;ation de votre compte.</p>")
    //.append("<p>Retrouvez toutes les op&eacute;rations du moment : <a href=\"operations_en_cours.html\">cliquez ici</a>.</p>")
    //.append("<p>Pour &ecirc;tre tenu au courant en avant-premi&egrave;re des nouvelles op&eacute;rations du club Europe 1,</p>")
    //.append("<p>Inscrivez vous &agrave; la newsletter Europe1 : <form style=\"float:left;\" name=\"\" action=\"\" method=\"post\"><input type=\"text\" class=\"sweet-field\" name=\"newsletter\" /><span style=\"float:right\" class=\"sweet-button\"><input type=\"submit\" name=\"newsletter\"></span></form></p>")
    //.append("<p>Europe 1</p>")
    .hide()
    .fadeIn(1500, function() {
      $('#message').append("<div class=\"clear\"></div>");
      $('#connect').hide();
    });
}

var update_ok = function(){
	//var offset = $('#content').offset().top;
	var offset = 0;
	$('html, body').animate({ scrollTop: offset }, 500);
	$('#inscription').html("<div id='message'><h1>Vous avez modifi&eacute; votre compte Europe1.</h1><p><img src=\"/includes/cobrand/img/ok.gif\" style=\"vertical-align:sub\" /> La mise &agrave; jour a &eacute;t&eacute; effectu&eacute;e avec succ&egrave;s.</p></div>");
}

var user_load = function(user){
		
	$('#current').val(user[0]['id_user']);

	if(user[0]['email']!='' && user[0]['pseudo']!=''){
	
		switch(user[0]['civilite']){
			case "Monsieur":
			$("#civilite_mr").attr("checked", true);
			break;
			case "Madame":
			$("#civilite_mme").attr("checked", true);
			break;
			case "Mademoiselle":
			$("#civilite_melle").attr("checked", true);
			break;
			default:
			break;
		}

		switch(user[0]['joignable']){
			case "matin":
			$("#matin").attr("checked", true);
			break;
			case "midi":
			$("#midi").attr("checked", true);
			break;
			case "journee":
			$("#journee").attr("checked", true);
			break;
			case "soiree":
			$("#soiree").attr("checked", true);
			break;
			default:
			break;
		}

		var wholedate = user[0]['birthdate'];
		if( wholedate != '' ){
			var str = wholedate.split("-");
			var test = str[1];
			var testday = str[2];
	
			if(test[0]=="0"){
				var mymonth = str[1].split("0");
				mymonth = mymonth[1];
			}else{
				var mymonth = str[1];
			}
			
			if(testday[0]=="0"){
				var myday = str[2].split("0");
				myday = myday[1];
			}else{
				var myday = str[2];
			}
			$('[name=year]').val(str[0]);
			$('[name=month]').val(mymonth);
			$('[name=day]').val(myday);
		}
		
		$('#pseudo').val(user[0]['pseudo']);
		$('#email').val(user[0]['email']);
		$('#nom').val(user[0]['nom']);
		$('#prenom').val(user[0]['prenom']);
		$('#ville').val(user[0]['ville']);
		$('#telephone').val(user[0]['telephone']);
		$('#cp').val(user[0]['cp']);
		$('#adresse').val(user[0]['adresse']);
		$('#joignable').val(user[0]['joignable']);
		
		$('#motivation').val(user[0]['quote']);
		
		if( user[0]['newsletter'] != '0' ){
			$("#optin_newsletter").attr("checked", true);
		}else{
			$("#optin_newsletter").attr("checked", false);
		}

		if( user[0]['alertes'] != '0' ){
			$("#optin_flash").attr("checked", true);
		}else{
			$("#optin_flash").attr("checked", false);
		}

		if( user[0]['optin'] != '0' ){
			$("#optin_offres").attr("checked", true);
		}else{
			$("#optin_offres").attr("checked", false);
		}
		
		// Interests
		var checkedInterests = user[0].interests;
		for( key in checkedInterests ){
			$('#int' + checkedInterests[key]).attr('checked','checked');
		}
		/*
if(user[0]['politique']==1){
			$("#int1").attr("checked", true);
		}

		if(user[0]['economie']==1){
			$("#int2").attr("checked", true);
		}
		
		if(user[0]['lecture']==1){
			$("#int3").attr("checked", true);
		}

		if(user[0]['automobile']==1){
			$("#int4").attr("checked", true);
		}

		if(user[0]['videogames']==1){
			$("#int5").attr("checked", true);
		}

		if(user[0]['musique']==1){
			$("#int6").attr("checked", true);
		}

		if(user[0]['cinema']==1){
			$("#int7").attr("checked", true);
		}

		if(user[0]['sports']==1){
			$("#int8").attr("checked", true);
		}
*/

        $('#job option[value=' + user[0]['profession'] + ']').attr("selected", "selected");

		$('#pays option[value=' + user[0]['pays'] + ']').attr("selected", "selected");

	}

}

var quickuser_load = function(user){ 
		
	$('#current').val(user[0]['id_user']);
		
	if(user[0]['email']!='' && user[0]['pseudo']!=''){

		$('#name').val(user[0]['pseudo']);
		$('#email').val(user[0]['email']);

		if(user[0]['newsletter']==1){
			$("#newsletter_club").attr("checked", true);
		}else{
			$("#newsletter_club").attr("checked", false);
		}

		if(user[0]['optin']!=""){
			$("#optin_club").attr("checked", true);
		}

	}

}

var update_user = function(){
			
	var offset = 0;
	$('html, body').animate({ scrollTop: offset }, 500);
	$('#inscription').html("<div id='message'><h1>Vous avez modifi&eacute; votre compte Europe1.</h1><p><img src=\"/includes/cobrand/img/ok.gif\" style=\"vertical-align:sub\" /> La mise &agrave; jour a &eacute;t&eacute; effectu&eacute;e avec succ&egrave;s.</p></div>");

}

var quick_to_club_upgraded = function(){ 

	        $('#inscription').html("<div id='message'></div>");
	        $('#connect').html("");
	        $('#message').html("<h2>Vous avez modifi&eacute; votre compte Europe1.</h2>")
	        .append("<p>Votre inscription est confirm&eacute;e et vous fa&icirc;tes d&eacute;sormais partie du club Europe 1.</p>")      
	        .append("<a href=''>Cliquez ici</a>")        
	        .hide()
	        .fadeIn(1500, function() {
	          $('#message').append("<img id='checkmark' src='http://team.dev.lejdd.fr/jqform/GUI/images/check.png' />");
	        });

}

var password_sent = function(user){
	if(user[0]['email']!=''){
		$('#modal-inner').removeClass('loading');
		var output  = '\
			<div class="success">\
				<p>Un nouveau mot de passe vous a &eacute;t&eacute; envoy&eacute; &agrave; l\'adresse suivante : ' + user[0]['email'] + '</p>\
				<p>Cette fen&ecirc;tre va se fermer automatiquement dans quelques secondes.</p>\
			</div>\
		';
		$('#modal-inner #form-forgetpass').empty().html(output);
		closeModal();
	}
}

var club_errors = function(user){
	if(user[0]['pseudo'] != 'undefined' && user[0]['pseudo'] != ''){
		//console.log('pseudo');
		$("#pseudo_exists_error").show();
		//output += '<li>' + user[0]['pseudo'] + '</li>';
	}
	if(user[0]['email'] != 'undefined'){
		//$("#email_exists_error").html("<p>Cette adresse email est d&eacute;j&agrave; utilis&eacute;e.</p>");
		$("#email_exists_error").show();
		//output += '<li>' + user[0]['email'] + '</li>';
	}
	if(user[0]['password'] != 'undefined' && user[0]['password'] != ''){
		//console.log('pass');
	    //$("#email_exists_error").html("<p>L&apos;identification a &eacute;chou&eacute;e.</p>");
	    $("#email_exists_error").show();
	    //output += '<li>' + user[0]['password'] + '</li>';
	}
	$("#submit").attr('disabled', '');
}

//--------- Begin - Errors occuring during comment add -----------//

var comment_errors = function(user){
	var context = ($('#modal-inner').length) ? $('#modal-inner') : '' ;
	$('.ajout-commentaire.loading', context).removeClass('loading');
	var output = '';
	for(i in user[0]){ output += '<li>' + user[0][i] + '</li>'; }
	$('.ajout-commentaire .errors', context).find('ul').empty().append(output).end().css({ display: 'block' });
	$('.ajout-commentaire', context).find('input[type=submit]').attr('disabled', '');
}


var comment_invalid_mail = function(user){ 
	if(user[0]['email']!=''){
		$('.loading').removeClass('loading');
		var output = '<li>Votre email n\'est pas valide.</li>';
		$('#modal-inner #form-login .errors ul, #modal-inner #form-forgetpass .errors ul, #modal-inner div[id^=ajout-commentaire] .errors ul').html(output);
		$('#modal-inner #form-login .errors,#modal-inner  #form-forgetpass .errors, #modal-inner div[id^=ajout-commentaire] .errors').css({ display: 'block' });
	}
	$('.ajout-commentaire', context).find('input[type=submit]').attr('disabled', '');
}

var comment_invalid_pseudo = function(user){ 

	if(user[0]['pseudo']!=''){
		$("#invalid_pseudo_error").show();
		$("#name").focus();
	}
	$('.ajout-commentaire', context).find('input[type=submit]').attr('disabled', '');
}


var comment_invalid_message_length = function(elem){ 
	// Nothing
}

var invalid_unknown_error = function(){
	if( $('#modal').length ){
		var output = '\
			<p>D&eacute;sol&eacute; une erreur est survenue.</p>\
			<p>Cette fen&ecirc;tre va se fermer automatiquement.</p>\
		';
		$('#modal-content').empty().append(output);
		closeModal();
	}
};

var session_error = function(user){
	alert( 'Session non valide' );
	window.location.reload();
}

//--------- End - Errors occuring during comment add -----------//

var comment_created = function(user){
	var output  = '\
		<div class="success">\
			<p>Votre commentaire a bien &eacute;t&eacute; envoy&eacute;.</p>\
			<p>Il va passer en mod&eacute;ration.</p>\
		</div>\
	';
	if( $('#modal').length ){
		//console.log(user[0].connect);
		$('#modal-inner').removeClass('loading');
		$('#modal-content').empty().html(output);
		if(user[0].connect == '1'){
			$('#modal-content div.success').append('<p>Vous &ecirc;tes connect&eacute;, cette page va se recharger dans quelques secondes.</p>');
			setTimeout(function(){ location.reload(true); },4000);
		}
	}
	else{
		$('[id^=ajout-commentaire]').removeClass('loading').find('> div').empty().append(output);
		if(user[0].connect == '1'){
			$('[id^=ajout-commentaire] div.success').append('<p>Vous &ecirc;tes connect&eacute;, cette page va se recharger dans quelques secondes.</p>');
			setTimeout(function(){ location.reload(true); },4000);
		}
	}
	
};

var invalid_mail_user = function(user){ 
	if(user[0]['email']!=''){
		$('.loading').removeClass('loading');
		var output = '<li>Votre email n\'est pas valide.</li>';
		$('#modal-inner #form-login .errors ul, #modal-inner #form-forgetpass .errors ul, #modal-inner div[id^=ajout-commentaire] .errors ul').html(output);
		$('#modal-inner #form-login .errors,#modal-inner  #form-forgetpass .errors, #modal-inner div[id^=ajout-commentaire] .errors').css({ display: 'block' });
	}
	$("#submit").attr('disabled', '');
}

var invalid_pseudo_user = function(user){ 

	if(user[0]['pseudo']!=''){
		$("#invalid_pseudo_error").show();
		$("#name").focus();
	}
	$("#submit").attr('disabled', '');
}

var invalid_password_length = function(user){ 

	if(user[0]['pwd']!=''){
		    $("#invalid_pwd_error").show();
		    $("#content").focus();
	}
	$("#submit").attr('disabled', '');
}

var wrong_combination = function(user){
	if(user[0]['email']!=''){
		$('#modal-inner').removeClass('loading');
		var output = '<li>Il y a une erreur dans vos identifiants.</li>';
		$('#modal-inner #form-login .errors ul, #modal-inner #form-forgetpass .errors ul').empty().append(output);
		$('#modal-inner #form-login .errors, #modal-inner #form-forgetpass .errors').css({ display: 'block' });
	}

}


var invalid_account = function(user){
	if(user[0]['email']!=''){
		$('#form-login p.pwd').find(' + p.error').remove().end().after('<p class="error" style="color: #E10;">L\'email ' + user[0]['email'] + ' n\'a pas &eacute;t&eacute; valid&eacute;.</p>');
	}
}

var invalid_pwd_user = function(user){ 

	if(user[0]['email']!=''){
		    $("#invalid_pwd_error2").show();
		    //$("#pwd").focus();
	}
	$("#submit").attr('disabled', '');
}

var invalid_civ_user = function(user){ 

	if(user[0]['email']!=''){
		    $("#civilite_error2").show();
		    $("#civilite_mr").focus();
	}
	$("#submit").attr('disabled', '');
}

var invalid_optin_user = function(user){ 

	if(user[0]['email']!=''){
		    $("#optin_error").show();
		    $("#optin_club").focus();
	}
	$("#submit").attr('disabled', '');
}

var invalid_lastname_user = function(user){ 

	if(user[0]['email']!=''){
		    $("#nom_error").show();
		    $("#nom").focus();
	}
	$("#submit").attr('disabled', '');
}

var invalid_firstname_user = function(user){ 

	if(user[0]['email']!=''){
		    $("#prenom_error").show();
		    $("#prenom").focus();
	}
	$("#submit").attr('disabled', '');
}

var invalid_address_user = function(user){ 

	if(user[0]['email']!=''){
		    $("#adresse_error").show();
		    $("#adresse").focus();
	}
	$("#submit").attr('disabled', '');
}

var invalid_phone_user = function(user){ 

	if(user[0]['email']!=''){
		    $("#telephone_error").show();
		    $("#telephone").focus();
	}
	$("#submit").attr('disabled', '');
}

var invalid_ville_user = function(user){ 

	if(user[0]['email']!=''){
		    $("#ville_error2").show();
		    $("#ville").focus();
	}
	$("#submit").attr('disabled', '');
}

var invalid_pays_user = function(user){ 

	if(user[0]['email']!=''){
		    $("#pays_error2").show();
		    $("[name=pays]").focus();
	}

}

var invalid_zipcode_user = function(user){ 

	if(user[0]['email']!=''){
		    $("#cp_error2").show();
		    $("#cp").focus();
	}
	$("#submit").attr('disabled', '');
}

var invalid_job_user = function(user){ 

	if(user[0]['email']!=''){
		    $("#job_error2").show();
		    $("[name=job]").focus();
	}
	$("#submit").attr('disabled', '');
}

var invalid_birthdate_user = function(user){ 

	if(user[0]['email']!=''){
		    $("#date1_error2").show();
		    $("[name=day]").focus();
	}
	$("#submit").attr('disabled', '');
}

var invalid_newsletter_user = function(user){ 

	if(user[0]['email']!=''){
		    $("#newsletter_error2").show();
		    $("#newsletter").focus();
	}
	$("#submit").attr('disabled', '');
}

var invalid_joignable_user = function(user){ 

	if(user[0]['email']!=''){
		    $("#joignable_error2").show();
		    $("#joignable").focus();
	}
	$("#submit").attr('disabled', '');
}

var invalid_motivations_user = function(user){ 

	if(user[0]['email']!=''){
		    $("#motivation_error2").show();
		    $("#motivation").focus();
	}
	$("#submit").attr('disabled', '');
}

var invalid_interests_user = function(user){ 

	if(user[0]['email']!=''){
		    $("#interets_error2").show();
		    $("#interets1").focus();
	}
	$("#submit").attr('disabled', '');
}

// Disponblit&eacute; du pseudo
var invalid_pseudo_result = function(user){
	$('#pseudo_test').removeClass('loading').css({ display: 'none' });
	if(user[0]['length']){
		$('#pseudo_resko').html("<p>Votre pseudo " + user[0]['pseudo'] + " doit contenir entre 5 et 15 caract&egrave;res.</p><span></span>");
	}
	if(user[0]['not_available']){
		$('#pseudo_resko').html("<p>Votre pseudo " + user[0]['not_available'] + " n\'est pas disponible. Veuillez en choisir un autre.</p><span></span>");
	}
	if(user[0]['error']=="wrong_chars"){
		$('#pseudo_resko').html("<p>Certains caract&egrave;res dans le pseudo " + user[0]['pseudo'] + " ne sont pas autoris&eacute;s. Veuillez en choisir un autre.</p><span></span>");
	}
	$('#pseudo_resko').css({ display: 'block' });
}
var valid_pseudo_result = function(user){
	$('#pseudo_test').removeClass('loading').css({ display: 'none' });
	$('#pseudo_resok').css({ display: 'block' });
}

// Update membre fail
var load_failure = function(){
	$.ajaxSetup({ processData: false });
	$.getScript('http://ezteam.europe1.fr/club/account/disconnect/?p=test');
	window.location.replace('/');
};

// suppression du compte
var account_removed = function(user){
	if(user[0].status === "1"){
		$('#user-delete').html('<div class="success"><p>Votre compte a bien &eacute;t&eacute; supprim&eacute;, cette page va se recharger dans quelques secondes.</p></div>');
		setTimeout(function(){ location.reload(true); }, 3000);
	}
	else{
		$('#user-delete').html('<div class="error"><p>La suppression n\'a pas pu &ecirc;tre effectu&eacute;e.</p></div>');
	}
};

// Confirmation d'alerte sur un commentaire
var confirm_alert = function(){
	var output = '\
		<div class="success">\
			<p>L\'alerte a bien été envoyée. Merci de votre vigilance.</p>\
			<p>Cette fenêtre va se fermer automatiquement.</p>\
		</div>';
	$('#modal-content').html(output);
	closeModal();
};

// Erreur d'alerte sur un commentaire
var error_alert = function(){
	var output = '\
		<p>Désolé, une erreur est intervenue lors de l\'envoie de l\'alerte.</p>\
		<p>Cette fenêtre va se fermer automatiquement.</p>';
	$('#modal-content').html(output);
	closeModal();
}


var invalid_old_password = function(user){
	$('#op').notice('error').css({ display: 'block' });
	$("#submit").attr('disabled', '');
};
var invalid_new_password = function(user){
	$('#np').notice('error').css({ display: 'block' });
	$("#submit").attr('disabled', '');
};
var invalid_confirm_password = function(user){
	$('#cfp').notice('error').css({ display: 'block' });
	$("#submit").attr('disabled', '');
};