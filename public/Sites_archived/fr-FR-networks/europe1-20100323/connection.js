// Cookie functions
function session_expired(){
	if( readCookie( "ezteam_user_info" ) != null ) window.location.reload();
}

function cookie_created(){
	if( readCookie( "ezteam_user_info" ) != null ) window.location.reload();
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

// Set login in global variable '_'
(function(){

	// Function
	var processConnect = function(){
		var login = '', cookieName = 'ezteam_user_info';
		if( readCookie( cookieName ) != null){
			if( readCookie( cookieName ) != 'notlogged' ){
				var ca = readCookie( cookieName ).split('#'), len = ca.length;
				for(var i=0; i<len; i++){
					var tmp = ca[i].split(':');
					_[tmp[0]] = tmp[1];
				}
				_.userLogged = true;
			}
		}
		else{
			var head = document.getElementsByTagName('head')[0];
            var script = document.createElement('script'); 
            script.src = 'http://ezteam.europe1.fr/club/cookie/regenerate';
            script.type = 'text/javascript';
            head.appendChild(script);
		}
	};
	
	// Core
	if(document.cookie){ processConnect(); }
	else{
		document.cookie = 'cookieName=cookieValue';
		if(document.cookie){ processConnect(); }
	}
})();