/************************************************
**     LIBRAIRIE DE FONCTIONS STANDARD SIGMA   **
**	Fonction de gestion des navigateurs    **
*******************************************************************************
**  Version 1.40 - 14/06/2006 - SBARBAS - Ajax - Récupération de ReponseXml (méthode lancerXml)
**  Version 1.30 - 30/01/2006 - SBARBAS - Fonctionnalités AJAX... num version rien a voir avec CVS :(
**  Version 1.20 - 30/05/2002 - NGOURDON - Ajout de fonctions pour manipuler
                                           les lignes et colonnes d'un tableau
**  Version 1.10 - 30/05/2002 - RGUEGAN - corrections pour les incompatibilités
			       avec HM_Menu
			       Suppression de std_initialiserDOM
**  Version 1.01 - 13/11/2001 - RGUEGAN - corrections mineures sur les noms des
			       variables mal référencées
**  Version 1.00 -  9/11/2001 - RGUEGAN - Première diffusion de la bibliothèque
**
*******************************************************************************/

///////////////////////////////////////////
// Variables de détection des navigateurs
///////////////////////////////////////////
///////////////////////////////////////////

// isNetscape4 : seul netscape 4 renverra true, les versions supérieurs de netscape renverrons false.
//		e.g. isNetscape4 renverra false pour Netscape 6

// iscompatibleXxx :  indique si le browser est compatible avec la version demandée,
//		e.g. isCompatibleInternetExplorer4 renverra true pour MSIE6

std_isNetscape	= ("Netscape"==navigator.appName)
std_isInternetExplorer	= ("Microsoft Internet Explorer"==navigator.appName)


std_isNetscape4 	   = (document.layers)? true:false
std_isCompatibleNetscape6  = (document.getElementById  && std_isNetscape )? true:false

std_isIE4	     = (document.all && !document.getElementById) ? true:false
std_isCompatibleIE4  = (document.all)? true:false
std_isCompatibleIE55 = (document.getElementById && std_isInternetExplorer)? true:false

std_isDHTML = (std_isNetscape4 || std_isCompatibleNetscape6 || std_isCompatibleIE4 || std_isCompatibleIE55)
std_isDOM   = (std_isCompatibleNetscape6 || std_isCompatibleIE55)


///////////////////////////////////////////
// Fonction de manipulation du DOM
///////////////////////////////////////////
///////////////////////////////////////////

function std_getReference(idObjet)
{
  if (std_isIE4 == true)
  {
     return document.all[idObjet] || null;
  }
  else if (std_isNetscape4)
  {
     var lyr = eval("document." + idObjet);
     lyr.style = lyr;
     return lyr;
  }  
  else
  {    
     return document.getElementById(idObjet)
  }
}

function std_getParent(idObjet)
{
  if (std_isInternetExplorer)
  {
    return idObjet.parentElement;
  }
  else if (std_isNetscape)
  {
    return idObjet.parentNode;
  }
  else
  {
    return idObjet.parentElement;
  }
}


/*
function setSourceHTML(id, valeur)
{
  if (!std_isNetscape4)
  {
     getReference(id).innerHTML = valeur
  }
  else
  {
      with(document.layers[id].document)
      {
	  open();
	  write(valeur);
	  close();
      }
  }

}
*/

function std_setSourceHTML(referenceObjet, valeur)
{
  if (!std_isNetscape4)
  {
     referenceObjet.innerHTML = valeur
  }
  else
  {
      with(referenceObjet.document)
      {
	  open();
	  write(valeur);
	  close();
      }
  }

}
function std_getSourceHTML(referenceObjet)
{
  return referenceObjet.innerHTML
}

function std_setStyle(referenceObjet, nomStyle)
{
  referenceObjet.className = nomStyle;
}

function std_cacherObjet(referenceObjet)
{
  referenceObjet.style.visibility = 'hidden'
}

function std_montrerObjet(referenceObjet)
{
  referenceObjet.style.visibility = 'visible'
}

function std_creerTableauLigne(table)
{
  var referenceLigne;
  if (std_isInternetExplorer)
  {
    referenceLigne = table.insertRow()
  }
  else if (std_isNetscape)
  {
    referenceLigne = document.createElement('TR');
    table.appendChild(referenceLigne);
  }
  return referenceLigne;
}

function std_supprimerTableauLigne(ligne)
{
    std_getParent(ligne).removeChild(ligne);
}

function std_creerTableauCellule(ligne, texteCellule)
{
  if (std_isInternetExplorer)
  {
    cellule = ligne.insertCell();
    cellule.innerHTML = texteCellule;
  }
  else if (std_isNetscape)
  {
    cellule = document.createElement('TD');
    cellule.innerHTML = texteCellule;
    ligne.appendChild(cellule);
  }
  return cellule;
}

/***************
 **    AJAX   **
 **	          **
 ***************/
 
function executerService(url, params, handler)
{
	var monService = new SigmaService();
	monService.url = url;
	monService.params = params;
	monService.callback = handler;
	monService.lancer();
}

function executerServiceXml(url, params, handler)
{
	var monService = new SigmaService();
	monService.url = url;
	monService.params = params;
	monService.callback = handler;
	monService.lancerXml();
}

function SigmaService() 
{
	this.url = "";
	this.params = null;
	this.callback = null;
	
	this.reponseText = "";
	this.reponseXml = "";
} 

function SigmaService(_url) 
{
	this.url = _url;
	this.params = null;
	this.callback = null;
	
	this.reponseText = "";
	this.reponseXml = "";
} 

SigmaService.prototype={
	
	//La fonction de lancement de la requete
	lancer : function ()
	{
		this.lancer(false, false);
	},
	lancerXml : function ()
	{
		this.lancer(false, true);
	},

	lancerSynchrone : function ()
	{
		this.lancer(true, false);
	},

	lancerSynchroneXml : function ()
	{
		this.lancer(true, true);
	},

        lancer : function (synchrone, xml)
        {
               var maRequete;
		var textRetour = "";
		
		var handler = this.callback;
		
		if(std_isCompatibleIE55) // Spécifique IE
		{
			//maRequete = new ActiveXObject("Msxml2.XMLHTTP");
			//sbs - 31/10/2006 - msxml2 pose pb avec ie5.5
			maRequete = new ActiveXObject("Microsoft.XMLHTTP");
		}
		else // Spécifique Mozilla
		{
			maRequete = new XMLHttpRequest();
		}
		
		//Gestion du callback (handler) si mode asynchrone
		if(!synchrone)
		{
			maRequete.onreadystatechange = function() {
					if(maRequete.readyState==4)
					{
						if(xml)
						{
                                                  handler(maRequete.responseXml);
                                                  this.reponseXml = maRequete.responseXml;
                                                }
                                                else
                                                {
                                                    handler(maRequete.responseText);
                                                    this.reponseText = maRequete.responseText;
                                                }
					}
				}
		}
		
		//Exceptions sur l'execution de la requete
		try
		{	
			if(this.params != null)
			{
				//lancement de la requete
				maRequete.open("POST",this.url,false);
				maRequete.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');	
			}
			else
			{
				maRequete.open("GET",this.url,true);
			}
			
			maRequete.send(this.params);

			//Gestion du mode synchrone
			if(synchrone)
			{

				if (maRequete.status == 200)
				{
                                   if(xml)
                                   {
                                     this.reponseXml = maRequete.reponseXml;
                                   }
                                   else
                                   {
					this.reponseText = maRequete.responseText;
                                   }
				}
			}
		}
		catch(e)
		{
			var message = e + "\r\n" + "HTTP - " + maRequete.status;
			alert(e);
		}
        },

	ajouterParam : function (nomParam, valeurParam)
	{
		var aInserer = nomParam + "=" + valeurParam;
		this.params = ((this.params == null) || (this.params == "")) ? aInserer : this.params + "&" + aInserer;
	}
}

function getResponseService(url, params) 
{	
	var monService = new SigmaService();

	monService.url = url;
	monService.params = params;
	monService.lancerSynchrone();
	
	return monService.reponseText;
}