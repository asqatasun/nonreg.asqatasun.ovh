/************************************************
**     LIBRAIRIE DE FONCTIONS STANDARD SIGMA   **
**       Fonction de gestion des types         **
********************************************************************************************
**  version 1.38  - 21/04/2004 - RGUEGAN - modif : bug convertirNombreEntier "01","" renvoyait NaN
**  version 1.37  - 14/01/2004 - RGUEGAN - modif : bug verifNombreEntier "1-1" renvoyait true
**  version 1.36  - 03/11/2003 - RGUEGAN - modif : bug sur convertirStringToInteger, pour les nombres commen�ant par 0
**  version 1.35  - 12/11/2002 - RGUEGAN - modif : remplacement de . par globale_SeparateurDecimal
**  version 1.34  - 05/11/2002 - RGUEGAN - modif : remplacement de / par globale_SeparateurDate
**
*******************************************************************************************/

///////////////////////////////////////////
// Variables globales
///////////////////////////////////////////
///////////////////////////////////////////

var globale_SeparateurMilliers = " ";
var globale_SeparateurDecimal  = "."
var globale_SeparateurDate     = "/";
var globale_MessageErreur      = "";
var globale_AnneePivot         = 49;
var globale_dernierCodeErreur = ""; // Codes internes, qui sont amen�s � changer //var globale_MessageErreur = "";
var globale_FormatDate        = "dd/mm/yyyy"
var globale_codePays          = "fr";
var CONSTANTE_FORMAT_DATE_FR  = "dd/mm/yyyy";
var CONSTANTE_JAVASCRIPT_SEPARATEUR_MILLIERS = "";
var CONSTANTE_JAVASCRIPT_SEPARATEUR_DECIMAL  = ".";
var CONSTANTE_JAVASCRIPT_SEPARATEUR_NEUTRE   = "#";  // n'apparait jamais dans un chiffre, n'est pas un s�parateur possible
var CONSTANTE_ALPHABET_ALPHANUMERIQUE = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
var CONSTANTE_ALPHABET_NUMERIQUE      = "0123456789";

///////////////////////////////////////////
// Fonctions de V�rification
///////////////////////////////////////////
///////////////////////////////////////////
//
// std_XX : fonction utilisateurs
// prv_XX : fonction interne (private)

function std_verifierIntegerBornes(Chaine, valeurMinimum, valeurMaximum, SeparateurMilliers)
{
  retour = true;
  nombreATester = std_convertirStringToInteger(Chaine, SeparateurMilliers);
  if ( ! isNaN (nombreATester) )
  {
     if ((nombreATester >= valeurMinimum) && (nombreATester <= valeurMaximum))
     {
       retour = true;
     }
     else
     {
       retour = false;
     }
  }
  else
  {
     retour = false;
  }
  return retour;
}

function std_verifierInteger(Chaine, SeparateurMilliers)
{
  valeurInteger = std_convertirStringToInteger(Chaine, SeparateurMilliers);
  if (isNaN (valeurInteger) )
  {
    return false;
  }
  else
  {
    return true;
  }
}

function std_verifierNombre(Chaine, SeparateurMilliers, SeparateurDecimal)
{
  retour = true;
  parametreValide = true;
  if (arguments.length == 1)
  {
     SeparateurMilliers = globale_SeparateurMilliers
     SeparateurDecimal  = globale_SeparateurDecimal
     parametreValide = true;
  }
  else if (arguments.length == 2)
  {
     SeparateurDecimal = globale_SeparateurDecimal
     parametreValide = true;
  }
  else if (arguments.length == 3)
  {
     parametreValide = true;
  }
  else
  {
     globale_dernierCodeErreur = "SF01" // S = systeme, F = Appel de fonction, 01 = mauvais nombre de parametres dans l'appel de la fonction
     parametreValide = false;
  }

  if (parametreValide == true)
  {
    retour = prv_verifierNombre(Chaine, SeparateurMilliers, SeparateurDecimal )
  }
  else
  {
    retour = false;
  }
  return retour;
}

// Permet de tester si la valeur est un nombre (entier ou decimal)
function prv_verifierNombre(Chaine, SeparateurMilliers, SeparateurDecimal)
{
  resultat = false;
  Chaine = std_trimDebut(Chaine);
  Chaine = std_trimFin(Chaine);
  var re = new RegExp("(^-?[0-9]{1,3}(["+SeparateurMilliers+"][0-9]{3})*(["+SeparateurDecimal+"][0-9]*)?$)|(^-?[0-9][0-9]*(["+SeparateurDecimal+"][0-9]*)?$)|(^-?(["+SeparateurDecimal+"][0-9]*)?$)","ig");
  if (re.test(Chaine))
  {
     resultat = true;
  }
  else
  {
     globale_dernierCodeErreur = "TN01" // T = V�rification de type, N = num�rique, 01 = La valeur fournie n'est pas un nombre correct
     resultat = false;
  }
  return resultat;
}

function std_verifierNombreEntier(Chaine, SeparateurMilliers)
{
  if (arguments.length == 1)
  {
     SeparateurMilliers = globale_SeparateurMilliers
  }
  else
  {
     // S'il y a 0 param�tre il y a une erreur javascript
     // Sinon, c'est qu'il y en a trop
  }
  return prv_verifierNombreEntier(Chaine, SeparateurMilliers )
}
function prv_verifierNombreEntier(Chaine, SeparateurMilliers)
{
  resultat = false;
  Chaine = std_trimDebut(Chaine);
  Chaine = std_trimFin(Chaine);

//  var re = new RegExp("(^-?[0-9]{1,3}(["+SeparateurMilliers+"][0-9]{3})*?$)|(^-?[0-9][0-9]*$)","ig");
  if (Chaine[0] == "-")
  {
  	Chaine = Chaine.substring(0, Chaine.length);
  }
  if (std_verifierStringDansAlphabet(Chaine, CONSTANTE_ALPHABET_NUMERIQUE+SeparateurMilliers))
  {
     resultat = true;
  }
  else
  {
     globale_MessageErreur = "La valeur fournie n'est pas un entier."
     resultat = false;
  }
  return resultat;
}

//** verifierNombreDecimal **
// Permet de tester si la valeur est num�rique
// et si le nombre de chiffre avant et apr�s la virgule correspond
// au format desire (renvoie true si c'est bon)
function std_verifierNombreDecimal(chaineNombre, longueurPartieEntiere, longueurPartieDecimale)
{
  if ( std_verifierNombre(chaineNombre) == true )
  {
    // Suppression du separateur de millier
    chaineNombreSimple = std_remplacerTexte(chaineNombre, globale_SeparateurMilliers, "");
    // Suppression du signe -
    chaineNombreSimple = std_remplacerTexte(chaineNombreSimple, "^-", "");

    len = chaineNombreSimple.length;
    pos = chaineNombreSimple.indexOf(globale_SeparateurDecimal,0);

    // Si la position de la virgule est superieure au nb
    // de chiffres attendus avant la virgule alors ERREUR
    if (pos >= 0)
    {
      if (pos > longueurPartieEntiere)
      {
         globale_MessageErreur = "La partie enti�re est trop grande, elle doit contenir " + longueurPartieEntiere + " chiffres maximum";
         return false;
      }
    }
    else
    {
      // il n'ya pas de .
      if (len > longueurPartieEntiere)
      {
       // globale_MessageErreur = "La partie enti�re est trop grande, il doit y avoir " + longueurPartieEntiere + " chiffres maximum";
        return false;
      }
    }
    if ((pos >= 0) && ((len - pos - 1) > longueurPartieDecimale))
    {
      globale_MessageErreur = "La partie d�cimale est trop grande, elle doit contenir " + longueurPartieDecimale + " chiffres d�cimaux maximum";
      return false;
    }
    return true;
  }
  globale_MessageErreur = "La valeur fournie n'est pas un nombre";
  return false;
}

/*
function verifierInt(Chaine)
{
}

function verifierFloat(Chaine)
{
}
*/

///////////////////////////////////////////
// Date
///////////////////////////////////////////

function std_comparerDatesFr(stringDate1, stringDate2)
{
  alert("Ne plus utiliser std_comparerDatesFr ! ");
  dateDebut = std_convertirStringToDate(stringDate1, CONSTANTE_FORMAT_DATE_FR)
  dateFin = std_convertirStringToDate(stringDate2, CONSTANTE_FORMAT_DATE_FR)
  return dateDebut.valueOf() - dateFin.valueOf();
}

function std_comparerDates(stringDate1, stringDate2)
{
  dateDebut = std_convertirStringToDate(stringDate1, globale_FormatDate)
  dateFin = std_convertirStringToDate(stringDate2, globale_FormatDate)
  return dateDebut.valueOf() - dateFin.valueOf();
}

function std_verifierDate(Chaine, formatDate)
{
  parametreValide = true;

  if (arguments.length == 0)
  {
     parametreValide = false;
  }
  if (arguments.length == 1)
  {
     formatDate = CONSTANTE_FORMAT_DATE_FR;
  }

  if (parametreValide == true)
  {
    retour = prv_verifierDate(Chaine, formatDate)
  }
  else
  {
    retour = false;
  }
  return retour;

}

function prv_verifierDate(chaine, formatDate)
{
  if (std_convertirStringToDate(chaine, formatDate) != null)
  {
     return true;
  }
  else
  {  // globale_MessageErreur est renseign� par std_convertirStringToDate
     return false;
  }
}

function std_verifierHeure(Chaine)
{
// Explication de l'expression reguliere:
// ^ => debut de la chaine
// [0-9]{1,2} => il y a une suite d'au plus 2 chiffres
// : => separateur heures / minutes
// [0-9]{1,2} => il y a une suite d'au plus 2 chiffres
// : => separateur minutes / secondes
// [0-9]{1,2} => il y a une suite d'au plus 2 chiffres
// (:[0-9]{1,2})? => la serie separateur minutes / secondes + chiffres est present au plus une fois
// $ => fin de la chaine
  var re = new RegExp("^([0-9]{1,2}):([0-9]{1,2})(:([0-9]{1,2}))?$","ig");
  if (re.test(Chaine))
  {
    heure = Chaine.replace(re,"$1");
    minute = Chaine.replace(re,"$2");
    seconde = Chaine.replace(re,"$4");
    if (heure > 23)
    {
      return false;
    }
    if (minute > 59)
    {
       return false;
    }
    if ((seconde != "") && (seconde > 59))
    {
       return false;
    }
    return true;
  }
  return false;
}

function std_verifierStringDansAlphabet(chaine, alphabet)
{
        var indiceCaractere = 0;
        var caractere = '';
        var nombreCaracteresChaine = chaine.length;

        for (indiceCaractere = 0; indiceCaractere < nombreCaracteresChaine; indiceCaractere++)
        {
                caractere = chaine.charAt(indiceCaractere);
                if (alphabet.indexOf(caractere) == -1)
                {
                   return false;
                }
        }
        return true;
}

function std_verifierEmail(chaineEmail)

{
testm = false ;

 for (var j=1 ; j<(chaineEmail.length) ; j++) 
 {
	if (chaineEmail.charAt(j)=='@') 
	{
		if (j<(chaineEmail.length-4))
		{
		for (var k=j ; k<(chaineEmail.length-2) ; k++) 
			{
			if (chaineEmail.charAt(k)=='.') testm = true;
			}
		}
	}
 }

if (testm==false) alert('Votre adresse e-mail est incorrecte.');

else 

return testm ;

}

/*
function std_verifierEmail(chaineEmail)
{
        if (chaineEmail == "")
        {
          return true;
        }
        var alphabet = CONSTANTE_ALPHABET_ALPHANUMERIQUE+".-_";

      // chercher le caract�re @
        var pos1 = chaineEmail.indexOf('@');
        if (pos1 < 0)
        {
           return false; // pas de @
        }
        var pos2 = chaineEmail.lastIndexOf('@');
        if (pos1 != pos2)
        {
          return false; // plusieurs @
        }

      // � gauche, c'est la partie compte de l'email
        var compteEmail = chaineEmail.substring(0, pos1-1);
        if (compteEmail == "")
        {
           return false; // il faut un compte
        }
        if (!std_verifierStringDansAlphabet(compteEmail, alphabet))
        {
           return false; // caract�res invalides ?
        }

      // � droite, c'est la partie domaine
        var domaineEmail = chaineEmail.substring(pos2+1, chaineEmail.length-1);
        if (domaineEmail == "")
        {
           return false; // il faut un domaine
        }
        if (!std_verifierStringDansAlphabet(domaineEmail, alphabet)) return false; // caract�res invalides ?

        return true;
}*/

///////////////////////////////////////////
// Fonctions de traitement de cha�nes
///////////////////////////////////////////
///////////////////////////////////////////

function std_remplacerTexte(Chaine,Source,Cible)
{
  if (Source == ".")
  {
     Source = "\\.";
  }
  var expressionReguliere = new RegExp(Source,"ig");
  return Chaine.replace(expressionReguliere, Cible);
}

//** TrimDebut **
// Supprime les espaces se trouvant en d�but de cha�ne.
// Renvoie la chaine trait�e
function std_trimDebut(Chaine)
{
  var expressionReguliere = new RegExp("^ +","ig");
  chaineTraitee = Chaine.replace(expressionReguliere, "");
  return chaineTraitee;
}

//** TrimFin **
// Supprime les espaces se trouvant en fin de cha�ne.
// Renvoie la chaine trait�e
function std_trimFin(Chaine)
{
  var expressionReguliere = new RegExp(" +$","ig");
  chaineTraitee = Chaine.replace(expressionReguliere, "");
  return chaineTraitee;
}


function std_trim(Chaine)
{
  chaineTraitee = std_trimDebut( Chaine );
  chaineTraitee = std_trimFin( chaineTraitee );
  return chaineTraitee;
}

//** TrimDebut **
// Supprime le caract�re (2� param�tre) se trouvant en d�but de cha�ne.
// e.g.
//   "aaaab"  ==> "b"
//   "aaaa"   ==> ""
// Renvoie la chaine trait�e
function std_trimDebutCaractere(chaine, caractereASupprimer )
{
  chaineTraitee = std_trim(chaine);
  var expressionReguliere = new RegExp("^"+caractereASupprimer+"+","ig");
  chaineTraitee = chaineTraitee.replace(expressionReguliere, "");
	return chaineTraitee ;
}

function std_completerTexteADroite(chaineInitiale, chaineComplementaire, longueurFinale)
{
    while (chaineInitiale.length < longueurFinale)
    {
        chaineInitiale += chaineComplementaire
    }
    return chaineInitiale
}

function std_completerTexteAGauche(chaineInitiale, chaineComplementaire, longueurFinale)
{
    chaineInitiale = ""+chaineInitiale;
    while (chaineInitiale.length < longueurFinale)
    {
        chaineInitiale = chaineComplementaire + chaineInitiale
    }
    return chaineInitiale
}

///////////////////////////////////////////
// Fonctions de Conversion
///////////////////////////////////////////
///////////////////////////////////////////

//** convertirStringToFloat**
// Transforme une valeur chaine en numerique si possible
// sinon retourne NaN
function std_convertirStringToFloat(Chaine)
{
  var nombreFlottant;

  if ( std_verifierNombreDecimal(Chaine) == true)
  {
    chaineNombreSimple = std_remplacerTexte(Chaine, globale_SeparateurMilliers, "");
    nombreFlottant = parseFloat(chaineNombreSimple);
  }
  else
  {
    nombreFlottant = NaN;
  }
  return nombreFlottant;
}

// Explication de l'expression reguliere:
// ^ => debut de la chaine
// [0-9]{1,2} => il y a une suite d'au plus 2 chiffres
// / => separateur jour / mois
// [0-9]{1,2} => il y a une suite d'au plus 2 chiffres
// / => separateur mois / annee
// [0-9]{2} => il y a une suite de 2 chiffres pour l'annee
// [0-9]{4} => il y a une suite de 4 chiffres pour l'annee
// ([0-9]{2})|([0-9]{4}) => il y a une suite de 2 ou 4 chiffres pour l'annee
// ((/([0-9]{2})|([0-9]{4})))? => la serie separateur mois / annee + chiffres est present au plus une fois
// $ => fin de la chaine
function std_convertirStringToDate(Chaine, formatDate)
{
  formatDate = formatDate.toLowerCase();

  if (!std_verifierStringDansAlphabet(Chaine, CONSTANTE_ALPHABET_NUMERIQUE + globale_SeparateurDate))
  {
    return null;
  }

  var tableauElementsDate = Chaine.split( globale_SeparateurDate );
  positionJour  = formatDate.indexOf('d',0);
  positionMois  = formatDate.indexOf('m',0);
  positionAnnee = formatDate.indexOf('y',0);
  ordreJour  = 0;
  ordreMois  = 0;
  ordreAnnee = 0;
  decalage = 5;

  if (positionJour < positionMois)
  {
    ordreAnnee = 2;
    ordreJour  = 0;
    ordreMois  = 1;
  }
  else
  {
    ordreAnnee = 2;
    ordreJour  = 1;
    ordreMois  = 0;
  }

  if (positionAnnee < positionJour)
  {  // finalement l'ann�e est au d�but
    ordreAnnee = 0;
    ordreJour  = ordreJour + 1;
    ordreMois  = ordreMois + 1;
  }

  jour = tableauElementsDate[ordreJour];
  mois = tableauElementsDate[ordreMois];
  annee = tableauElementsDate[ordreAnnee];

  if (typeof(annee) == "undefined")
  {
    datejour = new Date();
    annee = datejour.getYear();
  }
  else if (annee.length==2)
  { // Si l'ann�e est cod�e sur 2 chiffres
    // on la convertit en 4 chiffres
    if (parseInt(annee) > globale_AnneePivot)
    {
       annee=parseInt(annee)+1900;
    }
    else
    {
      annee=parseInt(annee)+2000;
    }
  }
  objetDate = new Date(annee, mois-1, jour);
  // en javascript, New Date("32 janvier 2000") ==> "1 f�vrier 2000"
  valide = (objetDate.getDate() == jour) && (objetDate.getMonth() == (mois-1)) && (objetDate.getFullYear() == annee)
  if (valide == true)
  {
     return objetDate;
  }
  else
  {
     return null;
  }
}

function std_convertirDateToString(objetDate, codePays)
{
  retour = true;
  parametreValide = true;
  if (arguments.length == 1)
  {
     codePays = globale_codePays;
     parametreValide = true;
  }
  else if (arguments.length == 2)
  {
     parametreValide = true;
  }
  else
  {
    globale_dernierCodeErreur = "SF01" // 01 = mauvais nombre de parametres dans l'appel de la fonction
    parametreValide = false;
  }

  if (parametreValide == true)
  {
    retour = prv_convertirDateToString(objetDate, codePays)
  }
  else
  {
    retour = false;
  }
  return retour;
}

function prv_convertirDateToString(objetDate, codePays)
{
  retour = "";
  if (objetDate != null)
  {
    codePays = codePays.toLowerCase();
    if (codePays == "fr")
    {  // dd/mm/yyyy
      retour = "" + std_completerTexteAGauche(objetDate.getDate(), "0", 2)  + globale_SeparateurDate +
                    std_completerTexteAGauche((objetDate.getMonth()+1), "0", 2) + globale_SeparateurDate + objetDate.getFullYear()
    }  // yyyy/mm/dd
    else if (codePays == "iso")
    {
      retour = "" + objetDate.getFullYear() + globale_SeparateurDate + std_completerTexteAGauche((objetDate.getMonth()+1), "0", 2) + globale_SeparateurDate + std_completerTexteAGauche(objetDate.getDate(), "0", 2)
    } // mm/dd/yyyy
    else if (codePays == "gb")
    {
      retour = "" + std_completerTexteAGauche((objetDate.getMonth()+1), "0", 2) + globale_SeparateurDate +std_completerTexteAGauche(objetDate.getDate(), "0", 2) + globale_SeparateurDate + objetDate.getFullYear()
    }
    else
    {
      globale_dernierCodeErreur = "TS01"; // Valeur de la String incorrecte
      retour = "";
    }
  }
  else
  {
    globale_dernierCodeErreur = "SF10"; // Param�tre null
    retour = "";
  }
  return retour;
}

function std_convertirStringToInteger(Chaine, SeparateurMilliers)
{
  retour = NaN;
  parametreValide = true;
  if (arguments.length == 1)
  {
     SeparateurMilliers = globale_SeparateurMilliers
     parametreValide = true;
  }
  else if (arguments.length == 2)
  {
     parametreValide = true;
  }
  else
  {
     globale_dernierCodeErreur = "SF01" // S = systeme, F = Appel de fonction, 01 = mauvais nombre de parametres dans l'appel de la fonction
     parametreValide = false;
  }

  if (parametreValide == true)
  {
    retour = prv_convertirStringToInteger(Chaine, SeparateurMilliers )
  }
  else
  {
    retour = NaN;
  }
  return retour;
}

function prv_convertirStringToInteger(Chaine, SeparateurMilliers )
{
  retour = NaN;

  ChaineConvertie = std_trimDebutCaractere(Chaine , "0");
  if (ChaineConvertie == "")
  {
     ChaineConvertie = "0";
  }

  if (SeparateurMilliers != CONSTANTE_JAVASCRIPT_SEPARATEUR_MILLIERS)
  {
    ChaineConvertie = std_remplacerTexte(ChaineConvertie,SeparateurMilliers, CONSTANTE_JAVASCRIPT_SEPARATEUR_MILLIERS);
  }

  // On convertie la valeur en integer
  valeurInteger = parseInt(ChaineConvertie);
  if (isNaN(valeurInteger))
  {
      retour = NaN;
  }
  else
  {
      if ((""+valeurInteger) == ChaineConvertie)
      {
        retour = valeurInteger;
      }
      else
      {
        retour = NaN;
      }
  }

  return retour;
}

function std_arrondi(nombre, nombreApresVirgule)
{
  multiplicateur=1;
  for (i=0;i < nombreApresVirgule; i++)
  {
    multiplicateur = multiplicateur*10
  }

  // exemple avec 2 chiffres apr�s la virgule : 3.1485*100=314.85
  // l'arrondi sera ex�cut� sur ce nombre
  nombre = nombre * multiplicateur;
  // on arrondi le nombre
  resultat=Math.round(nombre);
  
  // on r�cup�re la valeur exacte
  resultat=resultat/multiplicateur;
  
  // ajout de z�ro en fonction du nombre de chiffre apr�s la virgule
  // calcul du nombre de z�ro � rajouter
  resultat=resultat.toString();
  k = resultat.indexOf('.');
  if ( k!= -1)
  { 
    reste=resultat.substring(k+1,resultat.length).length;
  }
  else 
  { // nombre entier donc on rajoute '.' 
    reste = 0;
    resultat = resultat+'.';
  }
  
  // ajout des z�ros
  for (i=reste; i < nombreApresVirgule; i++)
  { 
	   resultat=resultat+0;
  }

  return resultat;
}


