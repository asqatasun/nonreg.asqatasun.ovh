/**
 * $Id$
 */

/*
	20050721 cpetit : ajouté formGenererZoneMulti
	20050721 cpetit : ajouté formGenererBouton
	20060127 cpetit : ajouté formHex et formEscape car le escape de JS mélange les + et les espaces
	20060131 cpetit : verifierChamp - ajouté test champ à null
*/

var CONSTANTE_DATE_MINI = new Date(1900,  1-1,  1);
var CONSTANTE_DATE_MAXI = new Date(2099, 12-1, 31)

var CONSTANTE_ENTIER_MINI = -999999999;
var CONSTANTE_ENTIER_MAXI =  999999999;

function std_verifierDateBornes(Chaine, valeurMinimum, valeurMaximum, formatDate)
{
	parametreValide = true;

	if (arguments.length == 0)
	{
		parametreValide = false;
	}
	if (arguments.length <= 1)
	{
		valeurMinimum = CONSTANTE_DATE_MINI;
	}
	if (arguments.length <= 2)
	{
		valeurMaximum = CONSTANTE_DATE_MAXI;
	}
	if (arguments.length <= 3)
	{
		formatDate = globale_FormatDate;
	}

	if (parametreValide == true)
	{
		retour = prv_verifierDateBornes(Chaine, valeurMinimum, valeurMaximum, formatDate)
	}
	else
	{
		retour = false;
	}
	
	return retour;
}

/**
 * @private
 */
function prv_verifierDateBornes(Chaine, valeurMinimum, valeurMaximum, formatDate)
{
	var tableauElementsDate = Chaine.split(globale_SeparateurDate);
	laDate = std_convertirStringToDate(Chaine, formatDate);
	if (laDate != null)
	{
		if (laDate>valeurMinimum)
		{
			if (laDate<=valeurMaximum)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	else
	{
		// globale_MessageErreur est renseigné par std_convertirStringToDate
		return false;
	}
}

function std_verifierString(Chaine, longueurMinimum, longueurMaximum)
{
	return (Chaine.length>=longueurMinimum && Chaine.length<=longueurMaximum);
}

function std_verifierEmails(Emails, separateur)
{
	liste = Emails.split(separateur);
	for(i=0; i<=liste.length; i++)
	{
		if (!std_verifierEmail(liste[i]))
		{
			return false;
		}
	}
	
	return true;
}

/**
 * Valider un champ de formulaire, renvoie vrai si le champ est valide, faux sinon
 * @param type        string     type de la donnée à valider
 * @param champ       object     zone du formulaire
 * @param valeur      string     valeur de la zone
 * @param obligatoire bool       vrai si obligatoire 
 * @param mini        int/regExp valeur minimum (ou expression régulière si type="regexp")
 * @param maxi        int        valeur maximum (ou explication si type="regexp")
 * @param description string     nom en clair de la zone
 * @returns bool vrai si OK, faux sinon
 */
function verifierChamp(type, champ, valeur, obligatoire, mini, maxi, description)
{
	var flag = true;
	var message = "";

	if (champ==null)
	{
		message = "La zone '" + description + "' n'a pas été trouvée dans le formulaire !";
		return false;
	}

	if (!obligatoire && valeur=="")
	{
		flag = true;
	}
	else
	{
		message = "La zone '" + description + "' ";
		if (valeur=="" && obligatoire)
		{
			flag = false;
			message += "doit être renseignée.";
		}
		else if (type=="string" || type=="chaine")
		{
			flag = std_verifierString(valeur, mini, maxi);
			if (mini!=maxi)
				message += "doit comprendre de " + mini + " à " + maxi + " caractères.";
			else
				message += "doit comprendre " + mini + " caractères.";
		}
		else if (type=="regexp")
		{
			flag = mini.exec(valeur)!=null;
			message += "doit correspondre " + maxi;
		}
		else if (type=="integer" || type=="entier")
		{
			flag = std_verifierIntegerBornes(valeur, CONSTANTE_ENTIER_MINI, CONSTANTE_ENTIER_MAXI, globale_SeparateurMilliers);
			if (flag)
			{
				valeur = std_convertirStringToInteger(valeur, globale_SeparateurMilliers);
				if (valeur<mini)
				{
					flag = false;
					message += "doit être un nombre sans virgule supérieur à " + mini + ".";
				}
				else if (valeur>maxi)
				{
					flag = false;
					message += "doit être un nombre sans virgule inférieur à " + maxi + ".";
				}
			}
			else
				message += "doit être un nombre sans virgule.";
		}
		else if (type=="quantity" || type=="quantite")
		{
			if (valeur=="")
				flag = true;
			else
				flag = std_verifierIntegerBornes(valeur, mini, maxi, globale_SeparateurMilliers);
			message += "doit être vide ou comprise entre " + mini + " et " + maxi + ".";
		}
		else if (type=="date")
		{
			if (mini==null) mini = CONSTANTE_DATE_MINI;
			if (maxi==null) maxi = CONSTANTE_DATE_MAXI;
			flag = std_verifierDateBornes(valeur, mini, maxi, globale_FormatDate);
			message += "doit être une date comprise entre le " + std_convertirDateToString(mini) + " et le " + std_convertirDateToString(maxi) + ".";
		}
		else if (type=="email")
		{
			flag = std_verifierString(valeur, mini, maxi) && std_verifierEmail(valeur);
			message += "doit être une adresse de courrier électronique.";
		}
		else if (type=="emails")
		{
			flag = std_verifierString(valeur, mini, maxi) && std_verifierEmails(valeur, ',');
			message += "doit être une suite d'adresses de courrier électronique séparées par des virgules.";
		}
		else if (type=="reference")
		{
			flag = isReference(valeur, mini, maxi);
			if (mini==maxi)
				message += "doit être composée de " + maxi + " caractères parmi '" + refAlphabet + "'.";
			else
				message += "doit être composée de " + mini + " à " + maxi + " caractères parmi '" + refAlphabet + "'.";
		}
		else if (type=="reference1")
		{
			flag = isReference1(valeur, mini, maxi);
			if (mini==maxi)
				message += "doit être composée de " + maxi + " caractères parmi '" + refAlphabet1 + "'.";
			else
				message += "doit être composée de " + mini + " à " + maxi + " caractères parmi '" + refAlphabet1 + "'.";
		}
		else if (type=="float" || type=="nombre")
		{
			// si maxi=999999.99, renvoie 6 et 2 dans gauche et droite
			var gauche = Math.floor(Math.abs(maxi)).toString().length;
			var droite = (Math.abs(maxi)-Math.floor(Math.abs(maxi))).toString().length;
			flag = std_verifierNombre(valeur, mini, maxi) && std_verifierNombreDecimal(valeur, gauche, droite);
			if (!flag)
			{
				// réessayer avec un point à la place de la virgule
				valeur = std_remplacerTexte(valeur, ",", ".");
				flag = std_verifierNombre(valeur, mini, maxi) && std_verifierNombreDecimal(valeur, gauche, droite);
			}
			message += "doit être comprise entre " + mini + " et " + maxi + ".";
		}
		else
			flag = true;
	}

	if (!flag)
	{
		alert(message);
		//champ.focus();
		formSelectionner(champ);
	}

	return flag;
}

function formEcrireChaine(valeur)
{
	var tmp = "";
	if (valeur!=null)
	{
		tmp = valeur.toString();
	}
	return tmp;
}
function formPutString(valeur)
{
	return formEcrireChaine(valeur);
}

/**
 * Lire une chaîne dans un contrôle de saisie
 * @param chaine string valeur (en général un document.FmXxxx.EdYyyy.value)
 * @param defaut string valeur si erreur
 * @param vide   string valeur si vide
 * @returns int ou null si vide
 */
function formLireChaine(chaine, defaut, vide)
{
	var valeur;

	valeur = chaine;

	return valeur;
}
function formGetString(chaine, defaut, vide)
{
	return formLireChaine(chaine, defaut, vide);
}


/**
 * Lire un entier dans un contrôle de saisie
 * @param chaine string       valeur (en général un document.FmXxxx.EdYyyy.value)
 * @param defaut int,null,NaN valeur si erreur (NaN)
 * @param vide   int,null,NaN valeur si vide
 * @returns int ou null si vide
 */
function formLireEntier(chaine, defaut, vide)
{
	var valeur;

	if (chaine=="")
	{
		valeur = vide;
	}
	else
	{
		valeur = std_convertirStringToInteger(chaine);
		if (isNaN(valeur))
		{
			valeur = defaut;
		}
	}

	return valeur;
}
function formGetInteger(chaine, defaut, vide)
{
	return formLireEntier(chaine, defaut, vide);
}

function formEcrireEntier(valeur)
{
	var tmp = "";
	if (valeur!=null)
	{
		tmp = valeur.toString();
	}
	return tmp;
}
function formPutInteger(valeur)
{
	return formEcrireEntier(valeur);
}

// lit un nombre dans une contrôle de saisie
// @param string       chaine valeur (en général un document.FmXxx.EdYyy.value)
// @param int,null,NaN defaut valeur si erreur (NaN)
// @param int,null,NaN vide   valeur si vide
// @returns int ou null si vide
function formLireNombre(chaine, defaut, vide)
{
	var valeur;
	
	if (chaine=="")
	{
		valeur = vide;
	}
	else
	{
		valeur = std_convertirStringToFloat(chaine);
		if (isNaN(valeur))
		{
			valeur = defaut;
		}
	}

	return valeur;
}
function formGetNumber(chaine, defaut, vide)
{
	return formLireNombre(chaine, defaut, vide);
}

// sélectionne (si possible) et met le curseur dans un champ de formulaire
// @param object controle champ à sélectionner (document.FmXxx.EdYyyy ou document.getElementById("idYyyy"))
// @ returns boolean vrai si sélection ou focus OK, faux sinon
function formSelectionner(controle)
{
	if (!controle.disabled)
	{
		controle.focus();
		if (controle.select)
		{
			controle.select();
		}
		return true;
	}
	
	return false;
}

// sélectionne (si trouvée) la valeur d'une combo
// @param object combo combo à sélectionner (document.FmXxx.LiYyyy ou document.getElementById("idYyyy"))
// @param string valeur valeur à sélectionner
// @ returns boolean vrai si valeur trouvée, faux sinon
function formSetCombo(combo, valeur)
{
	var i;

	combo.selectedIndex = 0;
	for (i=0; i<combo.options.length; i++)
	{
		if (combo.options[i].value==valeur)
		{
			combo.selectedIndex = i;
			return true;
		}
	}
	
	return false;
}

// récupère la valeur d'une combo
// @param object combo combo à lire (document.FmXxx.LiYyyy ou document.getElementById("idYyyy"))
// @ returns string valeur sélectionnée
function formGetCombo(combo)
{
	if (combo.selectedIndex>=0 && combo.selectedIndex<=combo.options.length)
	{
		return combo.options[combo.selectedIndex].value;
	}
	
	return null;
}

// sélectionne (si trouvée) la valeur d'un ensemble de boutons radio
// @param object radio groupes de boutons radio à sélectionner (document.FmXxx.RgYyyy)
// @param string valeur valeur à sélectionner
// @ returns boolean vrai si valeur trouvée, faux sinon
function formSetRadio(radio, valeur)
{
	var i;

	//radio[0].checked = 0;
	try{
	
	for (i=0; i<radio.length; i++)
	{
		if (radio[i].value==valeur)
		{
			radio[i].checked = true;
			return true;
		}
	}
}
catch(e)
{}
	
	return false;
}

// récupère la valeur d'un ensemble de boutons radio
// @param object radio groupes de boutons radio à lire (document.FmXxx.RgYyyy)
// @ returns string valeur sélectionnée
function formGetRadio(radio)
{
	var i;
try
{
	for (i=0; i<radio.length; i++)
	{
		if (radio[i].checked)
		{
			return radio[i].value;
		}
	}
}
catch(e)
{}	
	return null;
}

// récupère la valeur d'un bouton check
// @param object check bouton check à lire (document.FmXxx.CbYyyy ou document.getElementById("idYyyy"))
// @ returns string valeur sélectionnée
function formGetCheck(check)
{
	if (check.checked)
	{
		return check.value;
	}
	
	return null;
}

// récupère les valeurs sélectionnées d'un ensemble de boutons check
// @param object checks boutons check à lire (document.FmXxx.CbYyyy)
// @param string sep séparateur entre les valeurs sélectionnées
// @ returns string valeur sélectionnée
function formGetChecks(checks, sep)
{
	var tmp = "";
	
	for (var i=0; i<checks.length; i++)
	{
		if (checks[i].checked)
		{
			if (tmp=="")
			{
				tmp = checks[i].value;
			}
			else
			{
				tmp += sep + checks[i].value;
			}
		}
	}
	
	return tmp;
}

// fixer les valeurs sélectionnées d'un ensemble de boutons check
// @param object checks boutons check à initialiser (document.FmXxx.CbYyyy)
// @param string sep séparateur entre les valeurs
// @param values valeurs à sélectionner avec le séparateur
// @ returns rien
function formSetChecks(checks, sep, values)
{
	values = sep + values + sep;
	for (var i=0; i<checks.length; i++)
	{
		// |A|B|C| contient |Z| ?
		checks[i].checked = (values.indexOf(sep + checks[i].value + sep)>=0);
	}
}

function formGenererZone(tag, type, id, nom)
{
	var input;
		
	if (std_isInternetExplorer)
	{
		// IE ne met le nom que si on crée l'élément de cette façon
		input = document.createElement("<" + tag + " NAME=\"" + nom + "\">");
		if (type!=null) input.type = type;
	}
	else
	{
		// les autres fonctionnent normalement...
		input = document.createElement(tag);
		if (type!=null) input.type = type;
		input.name = nom;
	}
	input.id           = id;

	return input;
}

// générer une zone de texte
// @param id          string  identifiant de la zone (id)
// @param nom         string  nom de la zone (name)
// @param nomClasse   string  classe(s) de la zone (class)
// @param taille      integer taille de la zone (size)
// @param longueurMax integer longueur maximum de la zone (maxlength)
// @param valeur      string  valeur de la zone (value)
// @returns object
function formGenererZoneTexte(id, nom, nomClasse, taille, longueurMax, valeur)
{
	var input = formGenererZone("INPUT", "text", id, nom);
		
	input.className    = nomClasse;
	input.size         = taille;
	input.maxLength    = longueurMax;
	input.value        = valeur;
	input.defaultValue = valeur;

	return input;
}

// générer une zone de texte multi-lignes
// @param id          string  identifiant de la zone (id)
// @param nom         string  nom de la zone (name)
// @param nomClasse   string  classe(s) de la zone (class)
// @param largeur     integer largeur de la zone (cols)
// @param hauteur     integer hauteur de la zone (rows)
// @param valeur      string  valeur de la zone (value)
// @returns object
function formGenererZoneMulti(id, nom, nomClasse, largeur, hauteur, valeur)
{
	var input = formGenererZone("TEXTAREA", null, id, nom);
		
	input.id           = id;
	input.className    = nomClasse;
	//input.size         = taille;
	input.cols         = largeur;
	input.rows         = hauteur;
	input.value        = valeur;
	input.defaultValue = valeur;

	return input;
}

function formGenererZoneCoche(id, nom, nomClasse, valeur, coche)
{
	var input;
		
	if (std_isInternetExplorer)
	{
		// IE ne met le nom que si on crée l'élément de cette façon
		// ainsi que l'attribut checked
		if (!coche)
			input = document.createElement(
				"<INPUT TYPE=\"checkbox\" NAME=\"" + nom + "\">"
			);
		else
			input = document.createElement(
				"<INPUT TYPE=\"checkbox\" NAME=\"" + nom + "\" CHECKED=\"checked\">"
			);
	}
	else
	{
		// les autres fonctionnent normalement...
		input = document.createElement("INPUT");
		input.type    = "checkbox";
		input.name    = nom;
		input.checked = coche;
	}
	input.id             = id;
	input.className      = nomClasse;
	input.value          = valeur;
	input.defaultChecked = coche;

	return input;
}

function formGenererZoneRadio(id, nom, nomClasse, valeur, coche)
{
	var input;
		
	if (std_isInternetExplorer)
	{
		// IE ne met le nom que si on crée l'élément de cette façon
		// ainsi que l'attribut checked
		if (!coche)
			input = document.createElement(
				"<INPUT TYPE=\"radio\" NAME=\"" + nom + "\">"
			);
		else
			input = document.createElement(
				"<INPUT TYPE=\"radio\" NAME=\"" + nom + "\" CHECKED=\"checked\">"
			);
	}
	else
	{
		// les autres fonctionnent normalement...
		input = document.createElement("INPUT");
		input.type    = "radio";
		input.name    = nom;
		input.checked = coche;
	}
	input.id             = id;
	input.className      = nomClasse;
	input.value          = valeur;
	//input.defaultValue   = valeur;
	input.defaultChecked = coche;

	return input;
}

function formGenererZoneCache(id, nom, valeur)
{
	var input = formGenererZone("INPUT", "hidden", id, nom);
		
	input.value          = valeur;
	input.defaultValue   = valeur;

	return input;
}

function formGenererZoneListe(id, nom, nomClasse, taille, options, valeur)
{
	var input = formGenererZone("SELECT", null, id, nom);
		
	if (std_isInternetExplorer)
	{
		// IE ne met le nom que si on crée l'élément de cette façon
		input = document.createElement("<SELECT NAME=\"" + nom + "\">");
	}
	else
	{
		// les autres fonctionnent normalement...
		input = document.createElement("SELECT");
		input.name = nom;
	}
	input.id        = id;
	input.size      = taille;
	input.className = nomClasse;
	for (var i=0; i<options.length; i++)
	{
		input.options[i] = options[i];
		if (input.options[i].value==valeur)
		{
			input.options[i].selected        = true;
			input.options[i].defaultSelected = true;
		}
	}

	return input;
}

function formGenererZoneLabel(nomClasse, idPour, texte)
{
	var label;
		
	label = document.createElement("LABEL");
	label.className = nomClasse;
	label.htmlFor   = idPour;
	label.appendChild(document.createTextNode(texte));

	return label;
}

function formHex(nombre, largeur)
{
	var HEX_DIGITS = "0123456789ABCDEF";
	var tampon = "";
	var chiffre;
	
	while (nombre>0)
	{
		chiffre = nombre % 16;
		tampon = HEX_DIGITS.charAt(chiffre) + tampon;
		nombre = (nombre-chiffre) / 16;
	}
	
	while (tampon.length<largeur)
	{
		tampon = "0" + tampon;
	}

	return tampon;
}

function formEscape(chaine)
{
	//return escape(std_remplacerTexte(""+chaine, "\\+", "%2B"));
	var i;
	var tampon = "";
	var c;
	chaine = "" + chaine;
	for (i=0; i<chaine.length; i++)
	{
		c = chaine.charAt(i);
		// tout encoder sauf 0-9, A-Z, a-z, - et _ !
		if ((c>="0" && c<="9") || (c>="A" && c<="Z") || (c>="a" && c<="z") || (c=="-") || (c=="_"))
			tampon += c;
		else
		{
			tampon += "%" + formHex(c.charCodeAt(0), 2);
		}
	}
	return tampon;
}

function formInitialiserReformatage(zone, func)
{
	if (zone)
	{
		zone.onblur = func;
	}
}

function formReformaterNumeroTelephone(zone)
{
	var zone = this;
	// enlever les blancs
	var tmp = zone.value.replace(/\s/g, "");
	// seulement 10 chiffres ?
	if (tmp.match(/^[0-9]{10}$/))
	{
		zone.value = 
			tmp.substr(0, 2) + " " + 
			tmp.substr(2, 2) + " " + 
			tmp.substr(4, 2) + " " + 
			tmp.substr(6, 2) + " " + 
			tmp.substr(8, 2)
		;
	}
}
