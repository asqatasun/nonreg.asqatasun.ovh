if(typeof FO_ANA == "undefined") FO_ANA={};
FO_ANA.SigmaPage_class = function() {};
Object.extend(FO_ANA.SigmaPage_class.prototype, Object.extend(new AjaxPro.AjaxClass(), {
	getMessageAttente: function() {
		return this.invoke("getMessageAttente", {}, this.getMessageAttente.getArguments().slice(0));
	},
	transformerXML: function(sXmlPath, sXslPath) {
		return this.invoke("transformerXML", {"sXmlPath":sXmlPath, "sXslPath":sXslPath}, this.transformerXML.getArguments().slice(2));
	},
	url: '/ajaxpro/FO_ANA.SigmaPage,ANA.ashx'
}));
FO_ANA.SigmaPage = new FO_ANA.SigmaPage_class();

