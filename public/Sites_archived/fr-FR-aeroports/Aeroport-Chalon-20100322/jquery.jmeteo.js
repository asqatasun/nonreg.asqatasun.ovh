jQuery.fn.jMeteo = function (settings) {		
	var el = this;
	var settings = jQuery.extend({
			turl:'',
			turldatas:'',
			usecache:true,
			cacheurl:'/dyn_actions.php',
			mode:'small',
			htmltpl	: ' \
	<div class="content"> \
		<div class="date">date</div> \
	<div class="temperatures"> \
			Min : <span class="temp-1"> T&deg;</span> | \
			Max : <span class="temp-2">T&deg;</span>  | \
		</div> \
			<div class="picto">icon</div> \
	</div> \
	<div class="clear"></div> \
	<div class="lien">link</div>',
			loadingmsg	: '<small>chargement de la m�t�o...</small>',
			daynames : ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi']
		}, settings || {});	

	//console.log(settings);
	
	$(this).html(settings.loadingmsg);

	var curdate=new Date();

	this.init = function () {
		//console.log(settings);
		var datas = { action:'loadurl', url : settings.turl, contentType : 'text/xml' };
		
		if(settings.turldatas) {
			var datas0	= settings.turldatas;
			datas.url	= datas.url+'?'+jQuery.param(datas0);
		}
		
		jQuery.ajax({
		  type: "GET",
		  url: '/dyn_actions.php',
		  data: datas,
		  dataType: "xml",
		  success: function(msg){
			  
			  
			 var title 	= jQuery('rss > channel > item > title',msg).text();
		 
			 var description	= jQuery('rss > channel > description',msg).text();
			 var desc	= jQuery('rss > channel > item > description',msg).text();
			 var picto	= jQuery('rss > channel > item > description > img',msg);
			 var temperature1	= jQuery('rss > channel > item > description',msg).next().attr('low');
			 var temperature2	= jQuery('rss > channel > item > description',msg).next().attr('high');
			 var lien			= jQuery('rss > channel > link',msg).text();
			 //console.log(temperature1);
			 var exp1	= new RegExp('<img src="(.*)"\/>',"gi");
			 var pics 	= desc.match(exp1); 
			 var mypic	= pics[0];
			 
			 // cherche une icone plus jolie
			 var exp1	= new RegExp('([0-9]+)\.gif',"g");
			 var pics 	= mypic.match(exp1); 
			 mypic		= pics[0];
			 var exp1	= new RegExp('[.]',"g");
			 var numpic = mypic.split(exp1); 
			 numpic		= numpic[0];
			 // XXXn.png nuit (n:night) (250px)
			 // XXXd.png jour (d:day) (250px)
			 switch(settings.mode) {
				 case 'night':
			 		mypic		= 'http://us.i1.yimg.com/us.yimg.com/i/us/nws/weather/gr/'+numpic+'n.png';				 
				 break;
				 
				 case 'day':
			 		mypic		= 'http://us.i1.yimg.com/us.yimg.com/i/us/nws/weather/gr/'+numpic+'d.png';				 
				 break;
				 
				 default:
			 		mypic		= 'http://us.i1.yimg.com/us.yimg.com/i/us/nws/weather/gr/'+numpic+'s.png';
				 break;
			 }
			 
			 //console.log(numpic);
			 
			 //console.log(pics);
			 jQuery(el).html(settings.htmltpl);
			 jQuery(".picto",el).html('<img src="'+mypic+'" alt="" \/>');
			 jQuery(".temp-1",el).html(temperature1+"&deg;");
			 jQuery(".temp-2",el).html(temperature2+"&deg;");
			 
			 jQuery(".date",el).html(settings.daynames[curdate.getDay()]+' '+curdate.getDate());
			 
			 var exp1=new RegExp('Weather for',"gi");
			 var description = description.replace(exp1,'M�t�o pour'); 
			 jQuery(".lien",el).html('<a href="'+lien+'" target="_blank">'+description+'</a>');
			 //console.log(title);
	
			 // si on doit mettre en cache les r�sultats
			 if(settings.usecache) {
				 var content = jQuery(el).html();
				 console.log(content);
				
				jQuery.ajax({
				  type: "GET",
				  url: settings.cacheurl,
				  data: { 'content' : content },
				  success: function(msg){
					  console.log(msg);
				  }
				});
				
			 }
		 
		   } // fin success
		}); // fin ajax

	};	
	
	this.init();

	return this;
}