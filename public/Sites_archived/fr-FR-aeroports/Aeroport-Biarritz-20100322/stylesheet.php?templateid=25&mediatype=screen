/* Start of CMSMS style sheet 'Accessibility and cross-browser tools' */
/* accessibility */

/* 
menu links accesskeys 
*/
span.accesskey {
   text-decoration:none;
}

/* 
accessibility divs are hidden by default 
text, screenreaders and such will show these 
*/
.accessibility, hr {
   position: absolute;
   top: -999em;
   left: -999em;
}

/* 
definition tags are also hidden 
these are also used for menu links    
*/
dfn {
   position: absolute;
   left: -1000px;
   top: -1000px;
   width: 0;
   height: 0;
   overflow: hidden;
   display: inline;
}
/* end accessibility */


/* wiki style external links */
/* external links will have "(external link)" text added, lets hide it */
a.external span {
  position: absolute;
  left: -5000px;
  width: 4000px;
}

/* make some room for the image */
a.external {
/* css shorthand rules, read: first top padding 0 then right padding 12px then bottom then right */
  padding: 0 12px 0 0;
}
/* colors for external links */
a.external:link {
  color: #18507C;
/* background image for the link to show wiki style arrow */
  background: url(external.gif) no-repeat 100% 0;
}
a.external:visited {
  color: #18507C; /* a different color can be used for visited external links */

/* 
Set the last 0 to -100px to use that part of the external.gif image for different color for active links 
external.gif is actually 300px tall, we can use different positions of the image to simulate rollover image changes.
*/
  background: url(external.gif) no-repeat 100% 0; 
}

a.external:hover {
  color: #18507C;
/* Set the last 0 to -200px to use that part of the external.gif image for different color on hover */
  background: url(external.gif) no-repeat 100% 0; 
  background-color: #C3D4DF;
}
/* end wiki style external links */


/* clearing */
/* 
clearfix is a hack for divs that hold floated elements. it will force the holding div to span all the way down to last floated item.
We strongly recommend against using this as it is a hack and might not render correctly but it is included here for convenience.
Do not edit if you dont know what you are doing
*/
	.clearfix:after {
		content: "."; 
		display: block; 
		height: 0; 
		clear: both; 
		visibility: hidden;
	}
	* html>body .clearfix {
		display: inline-block; 
		width: 100%;
	}
	
	* html .clearfix {
		/* Hides from IE-mac \*/
		height: 1%;
		/* End hide from IE-mac */
	}

/* end clearing */
/* End of 'Accessibility and cross-browser tools' */

/* Start of CMSMS style sheet 'Navigation: Simple - Horizontal' */
/********************
MENU
*********************/

/* hack for Internet Explorer */
* html div#menu_horiz {
   /* hide ie/mac \*/
   height: 1%;
   /* end hide */
}

/* 
background-color for the entire menu row, 
covering 100% of the width and text center-aligned 
*/
div#menu_horiz {
   background-color: #549E10;  /* background color for the entire menu row */
   width: 100%;
   height: 40px;
   margin: 0;
}

/* 
the menu ul box has top and left border, 
right border is provided by the li elements 
*/
div#menu_horiz ul {
   margin: 0;
   padding: 0;
   height: 40px;
}


/* menu list items */
div#menu_horiz li {
   float: left; /* makes the list horizontal */
   list-style: none; /* hides the list bullet */ 
   margin: 0 ;
   height: 40px;
   background: url(fond-menu.jpg) no-repeat left;
}


/* the links, that is each list item */
div#menu_horiz a {
   padding: 1.1em 1em .6em 1.4em; /* padding inside the list item box */
   margin: 0; /* margin outside each list item box */
   text-decoration: none; /* no underline for links */
   color: #fff;
   display: block; /* IE has problems with this, fixed above */
}


/* hover state for links */
div#menu_horiz li a:hover {
   
}


/* active parent, that is the first-level parent of a child page that is the current page */
div#menu_horiz li.activeparent a {

   color: #fff;
}

div#menu_horiz h3 {
   padding: 1em 1em .6em 1.4em; /* padding inside the list item box */
   margin: 0; /* margin outside each list item box */
   text-decoration: none; /* no underline for links */
   color: #fff;
   display: block; /* IE has problems with this, fixed above */
   font-size: 1.1em;                           /* instead of the normal font size for <h3> */
}

/* End of 'Navigation: Simple - Horizontal' */

/* Start of CMSMS style sheet 'Navigation: Simple - Vertical' */
/********************
MENU
*********************/
#menu_vert {
   padding-top: 1em;
   padding-left: 0;
   margin-left: 1em;
}


/* third level has some padding to have it stand out */
div#menu_vert ul ul ul {
   padding-bottom: 0.5em;
}

/* menu li block */
#menu_vert li {
   list-style: none;
   margin: 0;
   
   display: block;
}

#menu_vert ul ul li {
   border: none;
}

/** fix stupid ie bug with display:block; **/
* html #menu_vert li a { height: 1%; } 
* html #menu_vert li hr { height: 1%; }
/** end fix **/

/* first level links */
div#menu_vert a {
   text-decoration:none; /* no underline for links */
   display: block; /* IE has problems with this, fixed above */
   padding: 0.8em 0.5em 0.8em 1.5em; /* some air for it */
   color: #22aac9; /* this will be link color for all levels */
   background: url(arrow-right.gif) no-repeat 0.5em center; 
   min-height:1em; /* Fixes IE7 whitespace bug */ 
}

/* next level links, more padding and smaller font */
div#menu_vert ul ul a {
   font-size: 90%;
   padding: 0.5em 0.5em 0.5em 2.8em;
   background-position: 1.5em center;
}

/* third level links, more padding */
div#menu_vert ul ul ul a {
   padding: 0.3em 0.5em 0.3em 4.3em;
   background: url(dash.gif) no-repeat 2.8em center; 
}

/* hover state for all links */
div#menu_vert a:hover {
   background-color: #22aac9;
   color: #fff;
}
div#menu_vert a.activeparent:hover {
   background-color: #C3D4DF;
   color: #fff;
}

/* 
active parent, that is the first-level parent 
of a child page that is the current page 
*/
div#menu_vert li a.activeparent {
   background: url(arrow-down.gif) no-repeat 0.4em center; 
   background-color: #385C72;
   color: #fff;
}

div#menu_vert ul ul li a.activeparent {
   background-position: 1.5em center; 
   background-color: transparent;
   color: #18507C;
}


/* 
current pages in the default Menu Manager 
template are unclickable. This is for current page on first level 
*/
div#menu_vert ul h3 {
   background: url(arrow-right-active.gif) no-repeat 0.4em center;
   background-color: #385C72;
   display: block; 
   padding: 0.8em 0.5em 0.8em 1.5em;  /* some air for it */
   color: #fff;                                 /* this will be link color for all levels */
   font-size: 1em;                           /* instead of the normal font size for <h3> */
   margin: 0;                                  /* as <h3> normally has some margin by default */
}

/* 
next level current pages, more padding, 
smaller font and no background color or bottom border 
*/
div#menu_vert ul ul h3 {
   font-size: 1.1em;
   padding: 0.3em 0.5em 0.3em 2.8em;
   background-position: 1.4em center;
   background-color: transparent;
   border-bottom: none;
   color: #00348A;     
}

/* current page on third level, more padding */
div#menu_vert ul ul ul h3 {
   padding: 0.3em 0.5em 0.3em 4.3em;
   background: url(arrow-right-active.gif) no-repeat 2.7em center; 
}

/* section header */
div#menu_vert li.sectionheader {
   border-right: none;
   font-size: 130%;
   font-weight: bold;
   padding: 1.5em 0 0.8em 0;
   background-color: #fff;
   line-height: 1em;
   margin: 0;
   text-align:center;
}



/* separator */
div#menu_vert li.separator {
   height: 1px !important;
   margin-top: -1px;
   margin-bottom: 0;
   padding:2px 0 2px 0;
   background-color: #000;
   overflow:hidden !important;
   line-height:1px !important;
   font-size:1px; /* for ie */
}

div#menu_vert li.separator hr {
   display: none; /* this is for accessibility */
}
/* End of 'Navigation: Simple - Vertical' */

/* Start of CMSMS style sheet 'Module: News' */
div#news {
  margin: 2em 0 1em 1em;  /* margin for the entire div surrounding the news list */
  border: 1px solid #000; 
  background: #FFE9AF; 
}

div#news h2 {
   line-height: 2em;
   background: #fff;
}

.NewsSummary {
    padding: 0.5em 0.5em 1em; /* padding for the news article summary */
    margin: 0 0.5em 1em 0.5em; /* margin to the bottom of the news article summary */
    border-bottom: 1px solid #ccc; 
}

.NewsSummaryPostdate {
  font-size: 90%;
  font-weight: bold;
 }

.NewsSummaryLink {
//  color: #006db7;
    color: #64ad1d;
    font-size: 1.2em;
    font-weight: bold;
    padding-top: 0.2em;
}

.NewsSummaryCategory {
  font-style: italic;
  margin: 5px 0;
 }

.NewsSummaryAuthor {
  font-style: italic;
  padding-bottom: 0.5em;
}

.NewsSummarySummary, .NewsSummaryContent {
  line-height: 140%;
 }

.NewsSummaryMorelink {
  padding-top: 0.5em;
}

#NewsPostDetailDate {
  font-size: 90%;
  margin-bottom: 5px;
  font-weight: bold;
  }

#NewsPostDetailSummary {
   line-height: 150%;
   }

#NewsPostDetailCategory {
  font-style: italic;
  border-top: 1px solid #ccc;
  margin-top: 0.5em;
  padding: 0.2em 0;
}

#NewsPostDetailContent {
  margin-bottom: 15px;
  line-height: 150%;
  }

#NewsPostDetailAuthor {

  padding-bottom: 1.5em;
  font-style: italic;
}


/* to add specific style to the below divs, uncomment them. */

/* 
#NewsPostDetailTitle {}
#NewsPostDetailHorizRule {}
#NewsPostDetailPrintLink {}
#NewsPostDetailReturnLink {}
*/
/* End of 'Module: News' */

/* Start of CMSMS style sheet 'Layout: 3cols' */
/*****************
browsers interpret margin and padding a little differently, 
we'll remove all default padding and margins and
set them later on
******************/
* {
margin:0;
padding:0;
}

/*
Set initial font styles
*/
body {
   text-align: left;
   font-family: Verdana, Geneva, Arial, Helvetica, sans-serif;
   font-size: 75.01%;
   line-height: 1em;
}

/*
set font size for all divs,
this overrides some body rules
*/
div {
   font-size: 1em;
}

/*
if img is inside "a" it would have 
borders, we don't want that
*/
img {
   border: 0;
}

/*
default link styles
*/
/* set all links to have underline and bluish color */
a,
a:link 
a:active {
   text-decoration: underline;
/* css validation will give a warning if color is set without background color. this will explicitly tell this element to inherit bg colour from parent element */
   background-color: inherit; 
   color: #18507C; 
}

a:visited {
   text-decoration: underline;
   background-color: inherit;
  color: #18507C;                /* a different color can be used for visited links */
}


/* remove underline on hover and change color */
a:hover {
   text-decoration: none;
   
   color: #385C72;
}

/*****************
basic layout 
*****************/
body {
   background: #fff url(fond.jpg) repeat-x;
   color: #333;
   
}

div#fondcarte {
   background: url(fond-carte.jpg) no-repeat;
}

/* center wrapper, min max width */
div#pagewrapper {
   margin: 0 auto;     /* this centers wrapper */
   max-width: 782px; /* IE wont understand these, so we will use javascript magick */
   min-width: 782px;
   background-color: #fff;;
   color: black;
}


/*** header ***
we will hide text and replace it with a image
we need to assign a height for it
*/

div#header {
   background: url(logov.jpg) no-repeat;
   height: 255px; /* adjust according your image size */
   width: 782px;     
}

div#toplink {
   font-size: 1.3em;
   text-align: center;
   float: left;
   margin-top: 165px;
   height: 90px;
   width: 127px;
}

div#bandeau {
   float: right;
   width: 528px;
}

object.bandeau {
   display: right;
   height: 255px;           /* adjust according your image size */
   text-indent: -999em;  /* this hides the text */
   text-decoration:none; /* old firefox would have shown underline for the link, this explicitly hides it */
}

a.toplink {
   font-size: 1.1em;
   text-decoration: none;
   color: #fff;
}

a.toplink:visited {
   text-decoration: none;
   color: #fff;
}

a.toplink:hover {
   text-decoration: none;
   color: #fff;
}

div#search {
   float: right;
   width: 23em;     /* enough width for the search input box */
   text-align: right;
   padding: 0.6em 0 0.2em 0;
   margin: 0 1em;
}

div.breadcrumbs {
   padding: 1em 0 1.2em 0; /* CSS short hand rule first value is top then right, bottom and left */
   font-size: 90%;             /* its good to set fontsizes to be relative, this way viewer can change his/her fontsize */
   margin: 0 1em;              /* css shorthand rule will be opened to be "0 1em 0 1em" */
   border-bottom: 1px dotted #000;
}

 div.breadcrumbs span.lastitem { 
   font-weight:bold; 
 } 

div#enregistrement {
   float: left;
   margin-left: 40px;
   margin-top: 10px;
   background: url(fond.jpg) repeat-x;
}

div#publicite {
   padding-bottom: 1em;      /* some air for footer */
   text-align: right; /* centered text */
   margin:0;
   background: #fff url(fond.jpg) repeat-x;
}

div#content {
   margin: 1.5em auto 2em 0; /* some air above and under menu and content */
}

div#main {
   float: left;
   width: 45%;
}

div#sidebarleft {
   float: left; 
   width: 33%;    /* sidebar width, if you change this please also change #main margins */
   display: inline;  /* FIX ie doublemargin bug */
   margin-left: 0;
}

div#sidebarleft.hascontent {
   padding: 0 1%;
   width: 32%;  /* make width smaller if there's padding, or it will get too wide for the floated divs in IE */
}

div#sidebarright {
   float: right;
   width: 18%;    /* sidebar width, if you change this please also change #main margins */
   display: inline;  /* FIX ie doublemargin bug */
   margin-right: 0;
}

div#sidebarright.hascontent {
   padding: 0 1%;
   width: 18%;  /* make width smaller if there's padding, or it will get too wide for the floated divs in IE */
}

div#footer {
   clear: both;
}

div#footer p a {
   color: #000;
}

div#footer p {
   font-size: 0.8em;
   padding: 1.5em;      /* some air for footer */
   text-align: center; /* centered text */
   margin:0;
}

div#footer p a {
   color: #fff; /* needed becouse footer link would be same color as background otherwise */
}

/* as we hid all hr for accessibility we create new hr with extra div element */
div.hr {
   height: 1px;
   margin: 1em;
   border-bottom: 1px dotted black;
}

/* relational links under content */
div.left49 {
  float: left;
  width: 49%;  /* 50% for both left and right might lead to rounding error on some browser */
}

div.right49 {
  float: right;
  width: 49%;
  text-align: right;
}


/********************
CONTENT STYLING
*********************/
div#content {

}

/* HEADINGS */
div#content h1 {
   font-size: 2em;  /* font size for h1 */
   line-height: 1em;
   margin: 0;
}
div#content h2 {
   color: #294B5F; 
   font-size: 1.5em; 
   text-align: left; 
/* some air around the text */
   padding-top: 0.5em;
   padding-bottom: 1px;
/* and some air under the border */
   margin: 0 0 0.5em 0;
}
div#content h3 {
   color: #294B5F; 
   font-size: 1.3em;
   line-height: 1.3em;
   margin: 0 0 0.5em 0;
}
div#content h4 {
   color: #294B5F; 
   font-size: 1.2em;
   line-height: 1.3em;
   margin: 0 0 0.25em 0;
}
div#content h5 {
   font-size: 1.1em;
   line-height: 1.3em;
   margin: 0 0 0.25em 0;
}
h6 {
   font-size: 1em;
   line-height: 1.3em;
   margin: 0 0 0.25em 0;
}
/* END HEADINGS */

/* TEXT */
p {
   font-size: 1em;
   margin: 0 0 1.5em 0;  /* some air around p elements */
   line-height:1.4em;
   padding: 0;
}
blockquote {
   border-left: 10px solid #ddd;
   margin-left: 10px;
}
pre {
   font-family: monospace;
   font-size: 1.0em;
}
strong, b {
/* explicit setting for these */
   font-weight: bold;
}
em, i {
/* explicit setting for these */
   font-style:italic;
}

/* Wrapping text in <code> tags. Makes CSS not validate */
code, pre {
 white-space: pre-wrap;       /* css-3 */
 white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
 white-space: -pre-wrap;      /* Opera 4-6 */
 white-space: -o-pre-wrap;    /* Opera 7 */
 word-wrap: break-word;       /* Internet Explorer 5.5+ */
 font-family: "Courier New", Courier, monospace;
 font-size: 1em;
}

pre {
   border: 1px solid #000;  /* black border for pre blocks */
   background-color: #ddd;
   margin: 0 1em 1em 1em;
   padding: 0.5em;
   line-height: 1.5em;
   font-size: 90%;   /* smaller font size, as these are usually not so important data */
}

/* END TEXT */

/* LISTS */
div#main ul,
div#main ol,
div#main dl {
  font-size: 1.0em;
   line-height:1.4em;
   margin: 0 0 1.5em 0;
}
div#main ul li,
div#main ol li {
   margin: 0 0 0.25em 3em;
}

div#dl dt {
   font-weight: bold;
   margin: 0 0 0.25em 3em;
}
div#dl dd {
   margin: 0 0 0 3em;
}
/* END LISTS */

/* Tableaux */

.transport {cellpadding: 0px; cellspacing: 0px; border-collapse: collapse;}
.transport .titre {vertical-align: enter; text-align: center; background: #fff; border: 0px; color: #64baea; font-size: 1.2em; font-weight: bold; height: 50px; padding-top: 20px;}
.transport .legende {vertical-align: top; text-align: left; background: #fff; border: 0px; font-size: 0.9em; font-style: italic;}
.transport td {height: 1.5em;}
.transport .col1 {text-align: center; width: 120px; background: #fff;}
.transport .col2 {width: 250px;}
.transport .col3 {width: 100px;}
.transport  .pair {background: #b7def5; border: 0px;}
.transport  .impair {background: #64baea; border: 0px;}

/* compagnie */

.compagnie {cellpadding: 0px; cellspacing: 0px; border-collapse: collapse; width: 138px; height: 549px; background-image: url(fondcie.jpg); text-align: center;}
.compagnie .titre {font-size: 1.1em; font-weight: bold; color: #006db7; height: 39px;}
.compagnie .impair {height: 53px;}
.compagnie .pair {height: 49px;}


.soustitre {font-size: 1.2em; font-weight: bold; color: #64ad1d;}
.titre-departs {font-size: 1.2em; font-weight: bold; }
.titre-arrivees {font-size: 1.2em; font-weight: bold; color: #006db7;}

div#strike { font-size: 1.1em; font-weight: bold; background: #b80b0b; color: #fff; width: 350px;}
div#strike a {text-decoration: none;  color: #fff; }
.greve { font-size: 1.1em; font-weight: bold; background: #b80b0b; color: #fff; width: 350px;}

/* End of 'Layout: 3cols' */

/* Start of CMSMS style sheet 'Horaires' */
.titre-departs { color: #64ad1d;}
.titre-arrivees { color: #006db7;}

/* Tableaux */

/* Arrivées */

.arrivees {cellpadding: 0px; cellspacing: 0px; border-collapse: collapse; width: 250px;}
.arrivees .col1 {text-align: left; width: 15px; height: 18px;}
.arrivees .col2 {text-align: right; width: 40px;}
.arrivees .col3 {text-align: center; width: 60px;}
.arrivees .col4 {text-align: left; width: 140px;}
.arrivees  .impair {background: #64baeb;}
.arrivees  .pair {background: #b7e0f4;}


/* Départs */

.departs {cellpadding: 0px; cellspacing: 0px; border-collapse: collapse; width: 250px;}
.departs .col1 {text-align: left; width: 15px; height: 18px;}
.departs .col2 {text-align: right; width: 40px;}
.departs .col3 {text-align: center; width: 60px;}
.departs .col4 {text-align: left; width: 140px;}
.departs  .impair {background: #c9ce00;}
.departs  .pair {background: #e4e683;}
/* End of 'Horaires' */

