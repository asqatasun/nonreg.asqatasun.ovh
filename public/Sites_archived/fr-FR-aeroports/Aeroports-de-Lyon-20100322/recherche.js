function $(t) { 
	return document.getElementById(t); 
}

var _sTableauDepart='';
var _sTableauArrivee='';
var _sensPrecedent=''; //document.getElementById('sensRecherche')
var _sens='';

var _sLien1Depart='';
var _sLien2Depart='';
var _sLien3Depart='';
var _sLien4Depart='';
var _sLien5Depart='';
var _sLien6Depart='';

var _sLien1Arrivee='';
var _sLien2Arrivee='';
var _sLien3Arrivee='';
var _sLien4Arrivee='';
var _sLien5Arrivee='';
var _sLien6Arrivee='';


function departArrivee(cible) {

	var  sens = cible.parentNode.id;
	
	document.getElementById('sensRecherche').value=sens;
	if(_sensPrecedent!=sens){//--- On a bien chang� le sens de la recherche
		if(sens=='arrivee'){
			document.getElementById('containerSuggestions_depart').style.display='none';
			document.getElementById('containerSuggestions_arrivee').style.display='block';
			document.getElementById('listeResultats_arrivee').style.display='block';
			document.getElementById('listeResultats_depart').style.display='none';
			document.getElementById('tableau_vols_arrivee').style.display='block';
			document.getElementById('tableau_vols_depart').style.display='none';
			document.getElementById('containerChargementResultat_arrivee').style.display='block';
			document.getElementById('containerChargementResultat_depart').style.display='none';
			

		}
		else{
			document.getElementById('containerSuggestions_arrivee').style.display='none';
			document.getElementById('containerSuggestions_depart').style.display='block';
			
			document.getElementById('listeResultats_arrivee').style.display='none';
			document.getElementById('listeResultats_depart').style.display='block';
			document.getElementById('tableau_vols_arrivee').style.display='none';
			document.getElementById('tableau_vols_depart').style.display='block';

			document.getElementById('containerChargementResultat_arrivee').style.display='none';
			document.getElementById('containerChargementResultat_depart').style.display='block';
		}
	}
	
	_sensPrecedent=sens;
	_sens=sens;
	
	if (sens == 'depart')
	{
		document.getElementById('containerTitreResultatDepart').style.display = "block";
		document.getElementById('containerTitreResultatArrivee').style.display = "none";
	}
	else
	{
		document.getElementById('containerTitreResultatDepart').style.display = "none";
		document.getElementById('containerTitreResultatArrivee').style.display = "block";
	}

	
	
	
	
	
	if (cible.parentNode.className=="departNoSelected floatLeft") {
		cible.parentNode.className="departSelected floatLeft";
		$('arrivee').className="arriveeNoSelected floatLeft";
		$('recherche_vol_depart').className="recherche_vol";
		$('recherche_vol_arrivee').className="invisible recherche_vol";
		$('para_depart').className="";
		$('para_arrivee').className="invisible";
	}


	if (cible.parentNode.className=="arriveeNoSelected floatLeft") {
		cible.parentNode.className="arriveeSelected floatLeft";
		$('depart').className="departNoSelected floatLeft";
		$('recherche_vol_depart').className="invisible recherche_vol";
		$('recherche_vol_arrivee').className="recherche_vol";
		$('para_depart').className="invisible";
		$('para_arrivee').className="";
	}
	
}

var bulleSelected = "";
var inputStyle = "";
var inputColor = "";

function toogleBulle(cible, sens) {

	if (sens == 1 && cible != undefined) {

		inputStyle = cible.style.borderStyle;
		inputColor = cible.style.borderColor;

		cible.parentNode.style.backgroundColor = "#e5edf7";
		cible.style.borderColor = "#81a4d7";
		cible.style.borderStyle = "solid";
		cible.parentNode.getElementsByTagName('div')[0].className = "bulle";

		bulleSelected = cible.parentNode.getElementsByTagName('div')[0];

	} else if (bulleSelected != undefined) {

		bulleSelected.className = "bulle invisible";
		bulleSelected.parentNode.style.backgroundColor = "transparent";

		var el = bulleSelected.parentNode.getElementsByTagName('input');
		for (var i = 0; i < el.length; i++) {

			el[i].style.borderStyle = inputStyle;
			el[i].style.borderColor = inputColor;

		}

		var el = bulleSelected.parentNode.getElementsByTagName('select');
		for (var i = 0; i < el.length; i++) {

			el[i].style.borderStyle = inputStyle;
			el[i].style.borderColor = inputColor;

		}

		bulleSelected = "";

	}

}


function toogleSuggest(cible, sens) {

	if (sens == 1) {

		cible.parentNode.getElementsByTagName('select')[0].className="selection";

	} else {

		cible.parentNode.getElementsByTagName('select')[0].className="selection invisible";

	}

}


function selectVille(cible) {

	cible.parentNode.getElementsByTagName('input')[0].value = cible.options[cible.selectedIndex].value;
	toogleSuggest(cible.parentNode.getElementsByTagName('input')[0], 0);

}


function toogleInfoVol(target) {

	if ($("ligne_"+target).style.display == "none") {

		$("ligne_"+target).style.display = "";
		$("closelink_"+target).style.display = "block";
		$("showlink_"+target).style.display = "none";

	} else {

		$("ligne_"+target).style.display = "none";
		$("closelink_"+target).style.display = "none";
		$("showlink_"+target).style.display = "block";

	}

}



function toogleTooltip(t)
{

	if ($(t).style.display == "block") {

		$(t).style.display = "none";

	} else {

		$(t).style.display = "block";
		$(t).style.top = -($(t).offsetHeight + 15) + "px";

	}

}


function detailVol(lien, numvol, ville, code, heure, sens){
//console.log(lien + numvol+ '&ville=' + ville + '&code=' + code, '_top');
	// on stoppe le rafraichissement
	clearInterval(_objRefresh);

	// affichage de la page detail
	window.open(lien + numvol+ '&ville=' + ville + '&code=' + code + '&go=form' + '&heure=' + heure  + '&sens=' + sens, '_top');
}


function toggleBulle(cible, numero) {


	if (numero && cible != undefined) {

		if(numero == 1 || numero == 5){
//console.log('NUMERO : '+numero+' :: CIBLE : '+cible.Code);
//console.log('SELECT ');
			cible.select();
		}

		cible.parentNode.style.backgroundColor = "#e5edf7";
		cible.style.borderColor = "#81a4d7";
		cible.style.borderStyle = "solid";

		for (i = 1; i <= 8; ++i) {

			if (numero == i) {
				bulle = document.getElementById('bulle_'+i);
				bulle.style.display = "block";
			} else {
				bulle = document.getElementById('bulle_'+i);
				bulle.parentNode.style.backgroundColor = "transparent";
				bulle.style.display = "none";
			}
		}

		bulleSelected = cible;
		numBulle = numero;
	}
	else {

		for (i = 1; i <= 8; ++i) {

			bulle = document.getElementById('bulle_'+i);
			bulle.parentNode.style.backgroundColor = "transparent";
			bulle.style.display = "none";
		}
	}

}

function showDetailVol(target, numvol, datevol, legID, escaleLegID) {

	sendData ('module=cms_portlet_vols&desc=consultationdetailvol_ajax&action=detail_vol&target='+target+'&numvol='+numvol+'&date='+datevol+'&lang=fr&legID='+legID+'&escaleLegID='+escaleLegID, 'index.php', 'GET', 'info_vol_'+target);
	document.getElementById("ligne_"+target).style.display = "";
	document.getElementById("closelink_"+target).style.display = "block";
	document.getElementById("showlink_"+target).style.display = "none";
}

function closeDetailVol(target) {

	document.getElementById("ligne_"+target).style.display = "none";
	document.getElementById("closelink_"+target).style.display = "none";
	document.getElementById("showlink_"+target).style.display = "block";
}
