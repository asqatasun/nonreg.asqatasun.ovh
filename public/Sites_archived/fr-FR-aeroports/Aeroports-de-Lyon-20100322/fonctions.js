/****************************************************/
/*          Fonctions pour les formulaires          */
/****************************************************/

//Envoie de formualaire
function submitForm (nomFormulaire){
	window.document.getElementById(nomFormulaire).submit();}
//Vide un champ
function viderChamps(nomChamp, default_value){
	if (nomChamp.value == default_value){
		nomChamp.value = "";
	}
}
//Rempli un champ avec un valeur
function remplirChamps(nomChamp,valeur){
	//si le champs est vide, on le rempli avec sa valeur par default.
	if (nomChamp.value == ""){
		nomChamp.value = valeur;
	}
}


/****************************************************/
/*             Fonctions pour les images            */
/****************************************************/

//preload
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

//restauration d'une image
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

//trouve une image
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

//change une image
function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

//ouverture de page
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

//fermeture de page
function closeWindows(){window.close();}

/****************************************************/
/* PO-19/03/08     Fonctions Flash Campagne prix    */
/****************************************************/
    function cacheFlash()
    {
        document.getElementById("flash").style.visibility="hidden";
    }
	
	

/****************************************************/
/*        Fonctions Studio - DG - le 26/09/2008     */
/****************************************************/

function gambit() {
	window.addEvent('domready', function() {
		//alert('ici');
		//survolTR();
	});

	//survolTR();
	//badmarquee();
}

// survol des lignes du tableau --------------------------------
function survolTRover(ligne) {
	ligne.className = 'over';
	$('alttag').style.display = 'block';
}	


function survolTRout(ligne, nomClasse) {
	ligne.className = nomClasse;
	$('alttag').style.display = 'none';
}	



// Faux Marquee -----------------------------------------------
function badmarquee() {
	
	var obj = $$('.mooquee');
	var nbLiens = $$('.mooquee').length;
	
	for (a = 0; a < nbLiens; a++) {
		var nbchar = obj[a].innerHTML.length;
		if (nbchar > 28) {
			obj[a].innerHTML = '<span id="marquee'+ a +'" class="badmarquee">'+ obj[a].innerHTML +'</span>';
			defiling(a);
		}
	}
}

// Effet defilant --------------------------------------------
function defiling(a) {
	var defile = $('marquee'+a);
	var widthMarquee = $('marquee'+a).offsetWidth;
	var initMarquee = 0;
	var posMarquee = initMarquee;
	
	
	function texteDefile() {
		if (defile) {
			if (posMarquee < widthMarquee) {
				posMarquee = posMarquee+1;
			}
			else {
				posMarquee =  initMarquee;
			}
		}
		defile.style.left = -posMarquee+"px";
	}
	setInterval(texteDefile,20); // delai de deplacement 
}

// Bouton Recherche : Envoi du formulaire si tout est rempli --------------------------------------------
function submitvol() {
	if (document.getElementById('destination') == '' && document.getElementById('num_vol') == '' && document.getElementById('dadate') == '') {
		alert('veuillez saisir TOUTES les informations du vol');
	}
	else {document.getElementById('recherche_vol_depart').submit();}
}
	