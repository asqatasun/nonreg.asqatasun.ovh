// JavaScript Document

// Pile des commandes sendData � ex�cuter
var sendDataPile = new Array();
var xmlreq = new Array(); //--- JT


// retourne un objet xmlHttpRequest.
// m�thode compatible entre tous les navigateurs (IE/Firefox/Opera)
function getXMLHTTP(){
  var xhr=null;
  if(window.XMLHttpRequest) // Firefox et autres
  xhr = new XMLHttpRequest();
  else if(window.ActiveXObject){ // Internet Explorer
    try {
      xhr = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
      try {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (e1) {
        xhr = null;
      }
    }
  }
  else { // XMLHttpRequest non support� par le navigateur
    alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
  }
  xmlreq[xmlreq.length]=xhr;
  return xhr;
}


/**
* Permet d'envoyer des donn�es en GET ou POST en utilisant les XmlHttpRequest
*/
function sendData(data, page, method, conteneur)
{
	if(document.all && !window.opera)    
	{
        //Internet Explorer
        var XhrObj = new ActiveXObject("Microsoft.XMLHTTP") ;
    }//fin if
    else
    {
        //Mozilla
        var XhrObj = new XMLHttpRequest();
    }//fin else
    
    //d�finition de l'endroit d'affichage:
    var content = document.getElementById(conteneur);
    
    //si on envoie par la m�thode GET:
    if(method == "GET")
    {
        if(data == 'null')
        {
            //Ouverture du fichier s�lectionn�:
            XhrObj.open("GET", page);
        }//fin if
        else
        {
            //Ouverture du fichier en methode GET
            XhrObj.open("GET", page+"?"+data);
        }//fin else
    }//fin if
    else if(method == "POST")
    {
        //Ouverture du fichier en methode POST
        XhrObj.open("POST", page);
    }//fin elseif

    //Ok pour la page cible
    XhrObj.onreadystatechange = function()
    {
        if (XhrObj.readyState == 4 && XhrObj.status == 200) {
        	
        	// R�cup�ration des blocList
        	var regle = /(blocList.push\()(\d+)/g;
        	resultat = XhrObj.responseText.match(regle);
        	if(resultat) {
        		for(i=0 ; i< resultat.length ; i++) {
        			blocList.push(resultat[i].replace('blocList.push(',''));
        		}
        	}
        	
        	// Remplacement du contenu
        	content.innerHTML = XhrObj.responseText;
        	
        	// On d�pile le tableau des sendData pour voir s'il y en a encore � ex�cuter
        	if (sendDataPile.length) {
        		var sendSuivant = sendDataPile.shift();
        		sendData(sendSuivant[0],sendSuivant[1],sendSuivant[2],sendSuivant[3]);
        	}
		}
    }    

    if(method == "GET")
    {
        XhrObj.send(null);
    }//fin if
    else if(method == "POST")
    {
        XhrObj.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
        XhrObj.send(data);
    }//fin elseif
}//fin fonction SendData


/**
*/
var _sentSynchrone = false; //--- JT
var xhr_synch;		//--- JT table_ 
function sendSynchrone(url, nom_div)
{
	if(window.console) console.log("Annulation : ", annulerRecherches);
	//if(_sentSynchrone==true){//--- JT
		//alert('Annulation - sendSynchrone');
		//xhr_synch.abort();//--- JT
	//}//--- JT
	_sentSynchrone=true;//--- JT
	var obj = document.getElementById(nom_div);

	xhr_synch = getXMLHTTP();
	//var xhr_synch = getXMLHTTP();
	if (xhr_synch&&(annulerRecherches==false))
	{
		// appel AJAX
		xhr_synch.open("GET", url, false);
		xhr_synch.send(null);

		// copie du retour dans le DIV
		
		if(annulerRecherches==false){
			obj.innerHTML = xhr_synch.responseText;
		}
	}
	_sentSynchrone=false;//--- JT
}


/**
* Permet de r�cup�rer les donn�es d'un fichier via les XmlHttpRequest:
*/
function getFile(page)
{
    sendData('null', page, 'GET')
}//fin fonction getFile   


// Fonction permettant de soumettre un formulaire avec un appel de page en ajax
function ajaxSubmitForm(myform, page, method, conteneur) {
	
	var maChaine = "";
	
	// On parcourt le formulaire
	for(i=0 ; i<document.forms[myform].length ; i++) {
		maChaine = maChaine + "&" + document.forms[myform].elements[i].name + "=" + document.forms[myform].elements[i].value;
	}

	sendData('ajax=1' + maChaine, page, method, conteneur);
}


//--- Annulation des toutes les requ�tes en cours
function abortAllReq(tRequetesEnCours)
{
	while(tRequetesEnCours.length>0){
	tRequetesEnCours[0].abort();
	//tRequetesEnCours.shift();
	}
}