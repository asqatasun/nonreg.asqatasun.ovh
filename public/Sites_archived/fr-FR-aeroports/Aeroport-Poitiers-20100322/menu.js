function menuDyn(basediv) {
   this.uls = basediv.getElementsByTagName('ul');
   this.ulslen = this.uls.length;
   this.ulsidx = new Array();
   this.keeplev = 1; //Correspond aux niveaux qui resteront toujours visible
   this.onlyone = true; //Si vrai : l'affichage d'une sous rubrique masque toutes les sous rubriques de meme niveau
   this.clickable = true; 		//D�finit si le click sur un item de rubrique d�clenche l'affichage ou le masquage des items de la rubrique
   this.listLi(this.uls[0],0);	//(Remarque : Si 1 item de rubrique est un lien vers une page this.clickable devient 'false')
   this.basediv = basediv;
}

/*Boucle r�cursivement sur les li du menu*/
menuDyn.prototype.listLi = function(ul,level) {
   var display = 'block';									//On r�cupere le dislay ('block' ou 'none') 
   if(ul.currentStyle) display = ul.currentStyle.display;	//de l'ul fille
   else if(window.getComputedStyle) display = window.getComputedStyle(ul, null).display;
   this.setDisplay(ul, display);
   for(var i=0; i<ul.getElementsByTagName('li').length; i++) {	//Pour chaque li de l'ul courante
       if(ul.getElementsByTagName('li')[i]						//on d�fini son comportement
	   && ul.getElementsByTagName('li')[i].parentNode == ul) {
           var li = ul.getElementsByTagName('li')[i];
           this.setBehavior(li,level);
           if(li.getElementsByTagName('ul').length > 0) {				//et on rappelle la fonction si l'element
              this.listLi(li.getElementsByTagName('ul')[0], level+1);	//contient une sous rubrique
           }  
       }
   }
}

/*D�finit l'affichage d'un �l�ment*/
menuDyn.prototype.setDisplay = function(el, disp) {
	el.style.display = disp;	
}

/*Definit les niveau devant toujours etre affich�s*/
menuDyn.prototype.setLevel2show = function(n) {
	this.keeplev = n;
}

/*Affiche les images d'arriere plan en fonction de l'affichage de la rubrique fille*/
menuDyn.prototype.checkDisplay = function() { 
	for(var i=1; i<this.ulslen; i++) {
		if(this.uls[i].style.display == 'block') this.uls[i].parentNode.getElementsByTagName('h2')[0].className = 'on';
		else this.uls[i].parentNode.getElementsByTagName('h2')[0].className = '';
	}
}

/*Renvoie l'ul parent de niveau 'lev' de 'el' */
menuDyn.prototype.getElParentLevel = function(el, lev) {
if(this.getLevel(el) <= lev) {/*debug*/ debug('in getElParentLevel: thislev'+this.getLevel(el)+' <> klev'+lev); return; }
	var puls = this.getUlOfLevel(lev);
	var inList = function(ul) {
		for(var i=0; i<puls.length; i++) {
			if(puls[i] == ul) {
				return puls[i];
			}
		}
		return false;
	}
	var ul = el; while(ul.tagName.toUpperCase() != 'UL') {
		ul = ul.parentNode;
	}
	while(!inList(ul) && ul.parentNode.parentNode.tagName.toUpperCase() == 'UL') {
		ul = ul.parentNode.parentNode;
	}
	/*debug*/ try {debug('in getElParentLevel: '+ul.parentNode.getElementsByTagName('a')[0].innerHTML);} catch(e) {debug('in getElParentLevel: error:'+e);}
	return ul;
}	

menuDyn.prototype.getLevel = function(el) { 
	var ul = el; /*debug*/ //try { debug('>'+ul.tagName+' - '+ul.getElementsByTagName('a')[0].innerHTML);} catch(e) {;}
	while(ul.tagName.toUpperCase() != 'UL') {
		ul = ul.parentNode;
	}
	if(ul == this.uls[0]) { return 0;}
	var lev = 1;
	while(ul.parentNode.parentNode != this.uls[0]) {
		ul = ul.parentNode.parentNode;
		lev++;
	} 
	/*debug*/ debug('in getLevel: lev = '+(lev));
	return lev;	
}

menuDyn.prototype.getNoHrefBal = function(li) {
	var el = li;
	while(el.firstChild.nodeValue == null)
		el = el.firstChild;
	return el.tagName;
}

/*D�finition du comportement (masquage et affichage)
des �ventuelles sous rubriques*/
menuDyn.prototype.setBehavior = function(li, level) {  
  var assrub = li.getElementsByTagName('ul').length > 0;
  var a = li.getElementsByTagName('a')[0];
  if(!a || a.parentNode.parentNode.parentNode == li) { //Cas ou l'item n'est pas un lien (a correspondont ici a un item de la sous rubrique)
  	a = li.getElementsByTagName(this.getNoHrefBal(li))[0];} 
  var islink = (a.tagName != 'A' || (a.getAttribute('href') != '#'						//Si le lien est interne a la page 
			 && a.getAttribute('href').indexOf('javascript:') < 0						//ou que l'url est similaire a l'url (comment� car bug
  			 && !this.iscurl(a.getAttribute('href'))									//de la page : on squize le lien
			 ));
				
  //this.clickable = this.clickable?((assrub && !islink) || !assrub):this.clickable; 
  var onlyone = this.onlyone;
  var _this = this; 
  /*Gestion du click*/
  a.onclick = function() { /*debug*/ debug(assrub.toString()+' - '+(level > _this.keeplev-1).toString()+' lev: '+level);
	  if(assrub && level >= _this.keeplev && _this.clickable) { 
        var liul = li.getElementsByTagName('ul')[0];
        var cdisplay = (liul.style.display == 'block')?true:false;
		if(onlyone) _this.hideAllIn(_this.getParentUl(this));
        _this.showTree(liul, !cdisplay);
		_this.checkDisplay(); 
      }
      return islink;
  }
    /*Gestion du mouseover*/
  li.onmouseover = function() { /*debug*/ debug(assrub.toString()+' - '+(level > _this.keeplev-1).toString()+' lev: '+level);
	  if(assrub && level >= _this.keeplev) { 
        var liul = li.getElementsByTagName('ul')[0];
        liul.style.display = 'block';
		_this.checkDisplay(); 
      }
  }
  /*Gestion du mouseout*/
  li.onmouseout = function() { /*debug*/ debug(assrub.toString()+' - '+(level > _this.keeplev-1).toString()+' lev: '+level);
	  if(assrub && level >= _this.keeplev)
	  	this.getElementsByTagName('ul')[0].style.display = 'none';

  }
}


/* retourne l'ul parente d'un �l�ment*/
menuDyn.prototype.getParentUl = function(el) {
	var ul = el;
	while(ul.tagName.toUpperCase() != 'UL') {
		ul = ul.parentNode;
	}
	return ul;
}

/* retourne la li parente d'un �l�ment*/
menuDyn.prototype.getParentLi = function(el) {
	var li = el;
	while(li.tagName.toUpperCase() != 'LI') {
		li = li.parentNode;
	}
	return li;
}

menuDyn.prototype.iscurl = function(hrf) {
	var hrflen = hrf.length;
	var url = window.location.href.toString();
	var urllen = url.length;//alert(url.substr(urllen - hrflen)+' - '+hrf);
	return (url.substr(urllen - hrflen) == hrf);
}

menuDyn.prototype.showActivItem = function() {
	var basediv = this.basediv;
	var _this = this;
	var getActivLink = function() {
		var as = basediv.getElementsByTagName('a');
		var aslen = as.length;
		//on recherche les liens de classe 'on'
		for(var i=0; i<aslen; i++)
			if(as[i].className == 'on') 
				return as[i];
		//Si pas de lien activ (cas de pages horsmenu) on recherche par li de classe 'on'
		var getOnChild = function(el) {
			var tmplis = el.getElementsByTagName('li');
			var tmplislen = tmplis.length;
			for(var i=0; i<tmplislen; i++)
				if(tmplis[i].className == 'on') 
					return tmplis[i];
			return false;
		}
		var tmp = basediv;
		while(getOnChild(tmp) != false)
			tmp = getOnChild(tmp);
		return (tmp.getElementsByTagName('ul').length > 0)?tmp.getElementsByTagName('ul')[0].getElementsByTagName('li')[0]:tmp;
		//Si toujours pas de lien activ on compare le href des lien a l'url de la page
		/*for(var i=0; i<aslen; i++) { 
			if(_this.iscurl(as[i].getAttribute('href').toString())) {
				return _this.getParentLi(as[i]).getElementsByTagName('li')[0].getElementsByTagName('a')[0];
			}
		}*/
		//Si toujours pas de lien activ trouv� on retourne 'false' 
		return false;
	}
	
	this.hideAll();
	var actlink = getActivLink(); 
	if(actlink) {
		var ul = this.getParentUl(actlink);
		this.showTree(ul, true);
	}
	this.checkDisplay();
}

/*Affiche ou masque la sous rubrique courante et
affiche r�cursivement les rubriques parentes*/
menuDyn.prototype.showTree = function(ul, b) {
   ul.style.display = b?'block':'none';
   if(ul.parentNode.parentNode && ul != this.uls[0])
      this.showTree(ul.parentNode.parentNode, true);
}

/*Masque toutes les rubriques des niveaux > 1*/
menuDyn.prototype.hideAll = function() {
  for(var i=1; i<this.ulslen; i++) { 
		if(this.getLevel(this.uls[i]) > this.keeplev) {
			this.uls[i].style.display = 'none';
		}
  }
}

/*Masque toutes les sous rubriques de la rubrique pass�e en parametre*/
menuDyn.prototype.hideAllIn = function(ul) {
  var uls = ul.getElementsByTagName('ul');
  var ulslen = uls.length;
  for(var i=0; i<ulslen; i++) { 
	uls[i].style.display = 'none';
  }
}

/*Renvoie un tableau comportant les noeuds ul de la liste*/
menuDyn.prototype.getUlOfLevel = function(n) { /*debug*/ //debug('in getUlOfLevel');
    var levuls = new Array();
    var uls0 = this.uls[0];
    var checkLev = function(tmpul) {
        var clev = 0;
        var checked = false;
        while(clev < n) {
            try {tmpul = tmpul.parentNode.parentNode} catch(e) { tmpul = null;}
            clev++;
        }
        return tmpul == uls0;
    }
    for(var i=1; i<this.ulslen; i++) {
        if(checkLev(this.uls[i])) levuls.push(this.uls[i]);
    }
    return levuls;
}

/*Affiche l'arboresscence en fonction de strid
(ex pour strid: 1-2-1) */
menuDyn.prototype.showId = function(strid) {
    var idx = strid.split('-');
    var tmpul = this.uls[0];
    this.hideAll();
    for(var i=0; i<idx.length; i++) { 
		for(var j=0; j<=idx[i]; j++) { 
			if(tmpul.getElementsByTagName('li')[j].parentNode !== tmpul) {
				++idx[i];
			}
		}
        if(tmpul.getElementsByTagName('li')[idx[i]].getElementsByTagName('ul')[0]) {
            tmpul = tmpul.getElementsByTagName('li')[idx[i]].getElementsByTagName('ul')[0];
            tmpul.style.display = 'block';
        }
    }
}

function debug(str) { return;
	if(!document.getElementById('debug')) {
		var debug = document.createElement('div');
		document.body.appendChild(debug);
		debug.style.position = 'absolute';
		debug.style.top = '50px';
		debug.style.left = '500px';
		debug.style.width = '450px';
		debug.style.fontSize = '10px';
		debug.style.backgroundColor = '#dddddd';
		debug.setAttribute('id','debug');
	}
    document.getElementById('debug').innerHTML = document.getElementById('debug').innerHTML+' <br /> '+str;
}
function clearDebug() {document.getElementById('debug').innerHTML = ''; }