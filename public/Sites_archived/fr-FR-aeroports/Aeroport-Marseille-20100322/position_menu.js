msg = new Array();

function affiche_objet(nom_obj, obj_div) {
  var champ;
  for (champ in obj_div) {
    if (! isNaN(parseInt(obj_div[champ]) ) || (typeof obj_div[champ] == 'object' && obj_div[champ] != null)) 
      msg.push(nom_obj + "." + champ + '=' + obj_div[champ]);
  }
}

function affiche_style(nom_obj, obj_div) {
  var champ;
  // msg.push(nom_obj + '=' + obj_div);
  for (champ in obj_div.style) {
    if (! isNaN(parseInt(obj_div.style[champ]) ) || (typeof obj_div.style[champ] == 'object' && obj_div.style[champ] != null))
      msg.push(nom_obj + ".style." + champ + '=' + obj_div.style[champ]);
  }
}
 


function positionner_menu() {
  var DBG = false;

  if (DBG) {
    alert('positionner_menu');
    var menu_groupe = document.getElementById("depart");
    affiche_objet('menu_groupe', menu_groupe);
    affiche_style('menu_groupe', menu_groupe);
    
    msg.sort(); // pour avoir les champs dans l'ordre alphabétique.
    msg.push("\n");
  }
  //alert(document.body.offsetHeight);
 // alert(window.innerHeight);
  var milieu = "parseInt(document.body.clientWidth) / 2";

  if(parseInt(document.body.offsetHeight) > parseInt(window.innerHeight))
  var milieu_netscape = "(parseInt(window.innerWidth) / 2) - 6";
  else
  var milieu_netscape = "parseInt(window.innerWidth) / 2";
  if(DBG) msg.push(milieu + "=" + eval(milieu));
  if(DBG) msg.push(milieu_netscape + "=" + eval(milieu_netscape));

  milieu = eval(milieu);
  milieu_netscape = eval(milieu_netscape);
  
  if (isNaN(milieu)) {
    milieu = milieu_netscape;
	
  }

  if(DBG) msg.push("milieu=" + milieu);
 
  //Menu Principal
  document.getElementById("vols").style.left = milieu - 330;
  document.getElementById("acces").style.left = milieu - 290;
  document.getElementById("parcs").style.left = milieu - 240; 
  document.getElementById("services").style.left = milieu - 157;
  document.getElementById("evasion").style.left = milieu - 93;
  document.getElementById("provence").style.left = milieu - 10;
  document.getElementById("aeroport").style.left = milieu + 83;

  if (DBG) alert(msg.join("\n"));
}

/*function sous_menu_over(nom_id) {
  var obj_sous_menu;

  obj_sous_menu = document.getElementById(nom_id);
  obj_sous_menu.className = "sous-menu-on";

  obj_sous_menu = document.getElementById(nom_id + '_lien');
  obj_sous_menu.className = "sous-menu-on";
}

function sous_menu_out(nom_id) {
  var obj_sous_menu;
  obj_sous_menu = document.getElementById(nom_id);
  obj_sous_menu.className = "sous-menu-off";

  obj_sous_menu = document.getElementById(nom_id + '_lien');
  obj_sous_menu.className = "sous-menu-off";
}*/

//******************************//
// POPUP POUR LES PLANS         //
//******************************//
function open_fullscreen(location,name){
	var largeur=840;  // Math.floor((screen.width)*80/100);
	var hauteur=580; // Math.floor((screen.height)*80/100);
	var param='height='+hauteur+',width='+largeur+',top=' + ((screen.height-hauteur)/2) + ',left=' + ((			screen.width-largeur)/2) + ',scrollbars=yes';
	window.open(location,name,param);
}


function open_popup(lien,fenetre,width,height,param){
	var left=(screen.availWidth-width)/2;
	var top=(screen.availHeight-height)/2;
	if (top <0) top=0;
	if (param!='') param=','+param;
	var fen=window.open(lien,fenetre,'top='+top+',left='+left+',width='+width+',height='+height+param);
	if(!fen.opener) fen.opener = self;
	return false;
}



