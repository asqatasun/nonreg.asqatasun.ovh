/***
 * CLASS menu_dyn
 * A mettre en haut de la page
 * <script type="text/javascript">
 * 	var menu_haut = new menudyn();
 * 	menu_haut.ajout_menu("btn1","menu1",false,false,false,false);
 * 	menu_haut.ajout_menu("btn2","menu2",false,false,false,false);
 * 	menu_haut.ajout_menu("btn3","menu3",false,false,false,false);
 * 	menu_haut.lancer();
 * </script>
 */


function menudyn (){
	this.num_menu_courant = undefined;
	this.temps_affichage = 200;
	this.time_out = undefined ;
	this.menu = new Array();
}
menudyn.prototype.setTimeOut = function (timeout){
	this.temps_affichage = timeout;
}
menudyn.prototype.ajout_menu = function(id_lien, id_menu, lien_on_mouse_over, lien_on_mouse_out, menu_on_mouse_over, menu_on_mouse_out){
	var index = this.menu.length;
	this.menu[index] = new Array();
	this.menu[index]["lien"] = id_lien;
	this.menu[index]["menu"] = id_menu;
	this.menu[index]["lien_on_mouse_over"] = lien_on_mouse_over;
	this.menu[index]["lien_on_mouse_out"] = lien_on_mouse_out;
	this.menu[index]["menu_on_mouse_over"] = menu_on_mouse_over;
	this.menu[index]["menu_on_mouse_out"] = menu_on_mouse_out;
}
menudyn.prototype.lancer = function(){
	for(var i in this.menu){
		var le_lien = document.getElementById(this.menu[i]["lien"]);
		if (this.menu[i]["menu"]!=undefined){
			var le_menu = document.getElementById(this.menu[i]["menu"]);
		}

		le_lien.instance_menu_dyn = this;
		if (this.menu[i]["menu"]!=undefined){
			le_menu.instance_menu_dyn = this;
		}

		le_lien.numero_menu = i;
		if (this.menu[i]["menu"]!=undefined){
			le_menu.numero_menu = i;
		}

		if(document.addEventListener){
			le_lien.addEventListener("mouseout",function(){this.instance_menu_dyn.lien_mouse_out(this.numero_menu);},false);
			le_lien.addEventListener("mouseover",function(){this.instance_menu_dyn.lien_mouse_over(this.numero_menu);},false);
			if (this.menu[i]["menu"]!=undefined){
				le_menu.addEventListener("mouseout",function(){this.instance_menu_dyn.menu_mouse_out(this.numero_menu);},false);
				le_menu.addEventListener("mouseover",function(){this.instance_menu_dyn.menu_mouse_over(this.numero_menu);},false);
			}
		} else {
			le_lien.onmouseout=function(){this.instance_menu_dyn.lien_mouse_out(this.numero_menu);};
			le_lien.onmouseover=function(){this.instance_menu_dyn.lien_mouse_over(this.numero_menu);};
			if (this.menu[i]["menu"]!=undefined){
				le_menu.onmouseout=function(){this.instance_menu_dyn.menu_mouse_out(this.numero_menu);};
				le_menu.onmouseover=function(){this.instance_menu_dyn.menu_mouse_over(this.numero_menu);};
			}
		}
	}
}
menudyn.prototype.lien_mouse_over = function(numero_lien){
	eval(this.menu[numero_lien]["lien_on_mouse_over"]);
	if (this.time_out != undefined){
		window.clearTimeout(this.time_out);
		this.time_out = undefined;
	}
	if(this.num_menu_courant != undefined && this.num_menu_courant!=numero_lien){
		this.hide(this.num_menu_courant);
	}
	if (this.menu[numero_lien]["menu"]!=undefined){
		document.getElementById(this.menu[numero_lien]["menu"]).style.display="block";
	}
	this.num_menu_courant=numero_lien;
}
menudyn.prototype.lien_mouse_out = function(numero_lien){
	eval(this.menu[numero_lien]["lien_on_mouse_out"]);
	var ce_menu_dyn = this;
	this.time_out=window.setTimeout(function(){ce_menu_dyn.hide(numero_lien)},this.temps_affichage);

}
menudyn.prototype.menu_mouse_over = function(numero_menu){
	eval(this.menu[numero_menu]["menu_on_mouse_over"]);
	if (this.time_out != undefined){
		window.clearTimeout(this.time_out);
		this.time_out = undefined;
	}
}
menudyn.prototype.menu_mouse_out = function(numero_menu){
	eval(this.menu[numero_menu]["menu_on_mouse_out"]);
	var ce_menu_dyn = this;
	this.time_out=window.setTimeout(function(){ce_menu_dyn.hide(numero_menu)},this.temps_affichage);

}
menudyn.prototype.hide = function(numero_lien){
	if (this.time_out != undefined){
		window.clearTimeout(this.time_out);
		this.time_out = undefined;
	}
	if (this.menu[numero_lien]["menu"]!=undefined){
		document.getElementById(this.menu[numero_lien]["menu"]).style.display="none";
	}
}
menudyn.prototype.cacher = function(){
	if (this.num_menu_courant != undefined && this.time_out != undefined){
		if (this.menu[this.num_menu_courant]["menu"]!=undefined){
			document.getElementById(this.menu[this.num_menu_courant]["menu"]).style.display="none";
		}
		this.num_menu_courant = undefined ;
		window.clearTimeout(this.time_out);
		this.time_out = undefined;
	}
}