function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}


function init_select(name, value) {
  var i;
  for (i=0; i < name.options.length; i++){
    if (name.options[i].value == value) {
      name.selectedIndex = i;
      break;
    }
  }
} /// function init_select

function init_multiselect(name, value) {
  var i;
  for (i=0; i < name.options.length; i++){
    if (name.options[i].value == value) {
      name.options[i].selected = true;
    }
  }
} /// function init_select


function get_select_value(name) {
  return name.options[name.selectedIndex].value;
}

function init_radio(a_name, a_value) {
  // alert('init_radio name=' + a_name + ' value=' + a_value);
  var i;
  for (i=0; i < a_name.length; i++){
    if (a_name[i].value == a_value) {
      a_name[i].checked = true;
    }
  }
} /// function init_radio

function get_radio_value(obj_radio) {
  var i;
  var res = false;
  for (i=0; i < obj_radio.length; i++){
    if (obj_radio[i].checked) {
      res = obj_radio[i].value;
    }
  }
  return res;
} /// get_radio_value




function Question(newlocation) {
if(confirm("Etes-vous s�r de vouloir supprimer ces donn�es?")) self.location = newlocation;

}

function lets_go_to(newlocation){
    self.location = newlocation;
}; /// end func lets_go_to


function confirm_del(newlocation){
    if(confirm("Confirmez-vous la suppression ?")) self.location = newlocation;
}; /// end func confirm


function verifier_date(select_jour, select_mois, select_annee) {

  var DBG = false;
  var jour   = parseInt(get_select_value( select_jour ));
  var mois   = parseInt(get_select_value( select_mois ));
  var annee  = parseInt(get_select_value( select_annee));

  if (DBG) alert('verifier_date '+ jour + "/"+ mois + "/" + annee );

  var nb_jour_mois = 0;
  switch ( mois) {
  case 1 : case 3 : case 5 : case 7 : case 8 : case 10 : case 12 : nb_jour_mois = 31; break;
  case 4: case 6 : case 9 : case 11 : nb_jour_mois = 30; break;
  case 2 : 
    if (annee % 4 == 0) 
      nb_jour_mois = 29; 
    else 
      nb_jour_mois = 28;
    break;
  } // switch
  
  if (nb_jour_mois < jour) {
    alert('Ce jour du mois n\'existe pas !');
    if (DBG) alert('mois=' + mois + ' => nb_jour_mois < jour : ' + nb_jour_mois + ' < '+ jour);
    init_select(select_jour, nb_jour_mois);
    return false;
  }

  return true;
}


