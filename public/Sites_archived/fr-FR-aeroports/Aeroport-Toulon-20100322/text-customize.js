
	// JavaScript Document
	
	function ndeSetTextSize(chgsize,rs) 
	{
		if(!document.getElementById('middle')) return;
				
		var newSize;
		var startSize = parseInt(ndeGetDocTextSize());
		
		if(!startSize) startSize = 11;

		switch(chgsize)
		{
			case 'incr':
			{
				newSize = startSize + 2;
				break;
			}
			
			case 'decr':
			{
				newSize = startSize - 2;
				break;
			}
			
			case 'reset':
			{
				if(rs) newSize = rs;
				else newSize = 11;
				break;
			}
			
			default:
			{
				//newSize = parseInt(ndeReadCookie('nde-textsize',true));
				
				//if(!newSize) newSize = startSize;
				newSize = startSize;
				break;
			}
		}
		
		if(newSize < 10) newSize = 10;
		newSize += 'px';
		
		//document.documentElement.style.fontSize = newSize;
		//document.body.style.fontSize = newSize;
		
		document.getElementById('middle').style.fontSize=newSize;
		//ndeCreateCookie('nde-textsize',newSize,365,true);
	}
	
	function ndeGetDocTextSize() 
	{
		if(!document.getElementById('middle')) return 0;
		
		var size = 0;
				
		if(document.getElementById('middle').style && document.getElementById('middle').style.fontSize) size = document.getElementById('middle').style.fontSize;
		//else if(typeof(getComputedStyle) != 'undefined') size = getComputedStyle(body,'').getPropertyValue('font-size');
		//else if(body.currentStyle) size = body.currentStyle.fontSize;
		return size;
	}
	
	function ndeCreateCookie(name,value,days,useLang) 
	{
		var langString = (useLang)?ndeGetLang():'';
		
		var cookie = name + langString + '=' + value + ';';
		
		if(days) 
		{
			var date = new Date();
			var ndeMilliSecondsInDay = 86400000; // 24*60*60*1000
			date.setTime(date.getTime() + (days*ndeMilliSecondsInDay));
			cookie += ' expires=' + date.toGMTString() + ';';
		}
		cookie += ' path=/';
		
		document.cookie = cookie;
	}
	
	function ndeReadCookie(name,useLang) 
	{
		var langString = (useLang)?ndeGetLang():'';
		
		var nameEQ = name + langString + '=';
		var ca = document.cookie.split(';');
		
		for(var i=0;i<ca.length;i++) 
		{
			var c = ca[i];
			while(c.charAt(0) == ' ') 
			{
				c = c.substring(1, c.length);
			}
			
			if(c.indexOf(nameEQ) == 0) 
			{
				return c.substring(nameEQ.length,c.length);
			}
		}
		return null;
	}
	
	function ndeSetTheme()
	{
		ndeSetTextSize();
		return true;
	}

	function sp(s,str)
	{
		alert(s)
		p = s.indexOf(str);
		alert(p)
		if(p != -1) return s.substr(0,p);
		return false;
	}
	
	function ndeGetLang()
	{
		var langString = '';
		
		if(document.documentElement)
		{
			langString = document.documentElement.lang;
			if(langString != '')
			{
				langString = '-' + langString;
			}
		}  
		return langString;
	}
	
	 ndeSetTheme();
