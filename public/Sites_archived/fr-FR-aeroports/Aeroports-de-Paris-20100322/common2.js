/*
	change font size for whole page
	@param 	diff 		a value that will be added into font size of each node
*/
function changeFontSize(diff) {
	// if we already use this value, do nothing
	if (changeFontSize.diff == diff) {
		return;
	}
	
	// store default font size of page
	if (!changeFontSize.defaultSize) {
		changeFontSize.defaultSize = parseInt($$('body')[0].getStyle("font-size"));
		
		// init
		changeFontSize.diff = 0;
	}
	
	// calculate the new difference - depend on old value
	var newDiff = diff - changeFontSize.diff;
	
	// store new value
	changeFontSize.diff = diff;
	
	// all nodes and its subnodes that have following class will not be changed
	var exceptClass = "nochangeText";	
	
	// start change from body
	changeSize($$("body")[0]);	
	
	// this function will be called recursive to update all nodes
	function changeSize(node) {
		// go through all child nodes of this node
		var child = node.getFirst();
		while (child) {
			if (!child.hasClass(exceptClass)) {
				// must change font size of child node first
				changeSize(child);
				
				// change font size of this node
				if (child.getStyle("font-size")) {
					child.setStyle("font-size", parseInt(child.getStyle("font-size")) + newDiff);
				} else {
					child.setStyle("font-size", changeFontSize.defaultSize + newDiff);
				}
			}

			// next child
			child = child.getNext();
		}
	}
}

/*
	create by Hoang.Dang
	Init scroll top text 
*/

function appendBlank(times){
	var ret = '';
	while(times-- > 0){
		ret += '&nbsp;';		
	}
	return ret;
}
function initAutoScrollText() {
	if ($$("div.scrollText").length < 0) {
		return;
	}
	
	// validate condition
	var aDivNode = $$("div.scrollText")[0];
	if (!aDivNode) {
		return;
	}
	
	// detect moving distance
	var aSpanNode = aDivNode.getFirst();
	var textWidth = aSpanNode.getCoordinates().width + 40;
	var containerWidth = aDivNode.getCoordinates().width;	
	if (textWidth < containerWidth ) {
		aSpanNode.innerHTML += appendBlank(15) + aSpanNode.innerHTML;		
		aSpanNode.innerHTML += appendBlank(15) + aSpanNode.innerHTML;		
	} else {
		aSpanNode.innerHTML += appendBlank(15) + aSpanNode.innerHTML;		
	}
	// create a fx object
	var fx = new Fx({			
		onComplete: function() {
			// start next animation
			fx.start(0, - textWidth);		
		},
		
		duration: textWidth * 30,
		//duration: 10000,
		transition: Fx.Transitions.linear
	});		
	// listene for each step
	fx.set = function(value) {		
		aSpanNode.setStyle("left", value);		
	}	
	// start first animation
	fx.start(0, - textWidth);
	//On MouseOver
	aSpanNode.addEvent('mouseover', function() {
		fx.pause();
	});
	//On MouseOut
	aSpanNode.addEvent('mouseout', function() {
		fx.resume();
	});
}

///////////
function initSlidersQuestions() {
	var slide = $$("div.highLightAccesParkings")[0];
	if (!slide) return;
	var elements = slide.getElements("dd");
	var togglers = slide.getElements("dt");
	togglers.each(function(el) {
		el.addEvent('click', function(e) {new Event(e).stop();});
	});
	elements.setStyle("display", "block");
	
	new Accordion(togglers, elements, {
		duration: 300,
		show: 1,
		opacity: false,
		alwaysHide: true,
		onActive: function(el) {		
			el.addClass("active");
		},
		onBackground: function(el) {			
			el.removeClass("active");
		}
	});
}
function initActive(){
	var monCompte = $$("#myspace");
	if (monCompte.length == 0) {
		return;
	}
	
	var h4 = monCompte.getElement('h4');
	new Accordion([h4], [h4.getNext()], {
		duration: 300,
		show: 1,
		opacity: false,
		alwaysHide: true,
		onActive: function(el) {		
			el.addClass("activeMon");
		},
		onBackground: function(el) {			
			el.removeClass("activeMon");
		}
	});
}
// show layer
var ShowLayer = new Class({
	Implements: [Options],
	
	options:{
		//
	},
	
	initialize: function(element, options) {
		this.element = $(element);
		this.setOptions(options);
		this.intShowLayer();
	},
	intShowLayer: function(){
		var that = this;
		if(!this.element) {
			return;
		}
		$('showLayer').addEvent('click', function(e){
			e.stop();
			
			var aLinkClose = that.element.getElement('a');
			aLinkClose.addEvent('click', function(ev){
				ev.stop();
				that.element.setStyles({
					'display': 'none'
				});
			});
			
			that.element.setStyles({
				'position': that.options.position,
				'left': that.options.left,
				'top': that.options.top,
				'z-index': that.options.zIndex,
				'display': 'block'
			});
		});
	}
});




//Active Images for terminalPlan 
function activePress() {
	//global var 
	var group1Index = 0;
	var group2Index = 0;
	var group1I = 0;
	var group2I = 0;
	var group2I2 = 0;
	
	//Index 
	/*
		var ground1Clicked = false;
		var ground2Clicked = false;
		var ground3Clicked = false;
		var ground4Clicked = false;
	*/
	//////////////////////////////////////////
	var group1 = $$("img.group1");
	var group2 = $$("img.group2");	
	var group3 = $$("img.group3");	
	
	var yearClicked = false;
	
	if (!group1.length) {
		return;
	}
	
	['01','02','03'].each(function(v,i){

		$$('.contentContainer_'+v+'00, .contentContainer_'+v+'01, .contentContainer_'+v+'02, .contentContainer_'+v+'03, .contentContainer_'+v+'04, .contentContainer_'+v+'05, .contentContainer_'+v+'06, .contentContainer_'+v+'07, .contentContainer_'+v+'08, .contentContainer_'+v+'09, .contentContainer_'+v+'010, .contentContainer_'+v+'011').each(function(c,i){
																																																																																																																																																																																																																																																																																																																																																																																																																																																																							if(c.innerHTML != ''){
	group2I2 = i;																																																																																																											
	group2I =  '0' + i;																																																																																																																}
else { group1I = v; group1Index = v;return;}
																																																																																																																							   });

});


group1.each(function(image,i){
	image.src = image.src.replace('active.gif','normal.gif');
	if (i == parseInt (group1I)){
		image.src = image.src.replace('normal.gif','active.gif');
	}
});

group3.each(function(image,i){
	image.src = image.src.replace('small_a','small');
	if (i == group2I2){
		image.src = image.src.replace('small','small_a');
	}
});



showContentPress(group1I, group2I);


	group1.each(function(image, index){		
		image.addEvent('click', function(e) {
			e.stop();					
			yearClicked = true;
			
		
			
			
			group1.each(function(item, i){
					if(i != index){
						var img = item.src.substr(item.src.lastIndexOf("/")+1);
						item.src = item.src.substr(0, item.src.lastIndexOf("/")+1)+img.replace("_active.", "_normal.").replace("_active.", "_normal.");
						item.isClicked = false;						
					}					
				});
				
			group1Index = '0' + index;								
				//Call Show Content Function
			group2Index = 0;
				$$('.contentContainer_'+group1Index+'00, .contentContainer_'+group1Index+'01, .contentContainer_'+group1Index+'02, .contentContainer_'+group1Index+'03, .contentContainer_'+group1Index+'04, .contentContainer_'+group1Index+'05, .contentContainer_'+group1Index+'06, .contentContainer_'+group1Index+'07, .contentContainer_'+group1Index+'08, .contentContainer_'+group1Index+'09, .contentContainer_'+group1Index+'010, .contentContainer_'+group1Index+'011').each(function(c,i){
																																																																																																																																																																																																																																						
																																																																																																																																																																																																																																			if(c.innerHTML != ''){
																																																																																																																
	group2Index++;																																																																																																																}
else { return;}
																																																																																																																							   });
	
				group2Index--;
				seli = group2Index;
				group2Index = '0' + group2Index;
				group3.each(function(image,i){
					image.src = image.src.replace('small_a','small');
					if (i == seli){
						image.src = image.src.replace('small','small_a');
					}
				});
				
				
				showContentPress(group1Index, group2Index);

			this.isClicked = true;
			var img = this.src.substr(this.src.lastIndexOf("/")+1);
			this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_normal.", "_active.").replace("_normal.", "_active.").replace("_N.", "_ACTIVE.");									
			ground1Clicked = true;			
		});
		image.addEvent('mouseover', function(e) {	
			var img = this.src.substr(this.src.lastIndexOf("/")+1);			
			this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_normal.", "_active.");
		});
		image.addEvent('mouseout', function(e) {							
			if(this.isClicked){                             
				var img = this.src.substr(this.src.lastIndexOf("/")+1);
				this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_normal.", "_active.").replace("_normal.", "_active.");
			} else {
				var img = this.src.substr(this.src.lastIndexOf("/")+1);
				this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_active.", "_normal.");
			}			
		});
	});
	/*/////For Arrival Terminal 2 : Letter/////////////////*/
	
		if (!group3.length) {
		return;
	}	
	group3.each(function(image, index){		
		image.addEvent('click', function(e) {
			e.stop();
			if (!yearClicked){
				
				$$('.noyearselected').each(function(e,i){
					e.style.display = 'block';
					//e.show();
				});
			}
			if(image.getParent().id == 'depature02'){
			
				group3.each(function(item, i){
					if(i != index){
						var img = item.src.substr(item.src.lastIndexOf("/")+1);
						item.src = item.src.substr(0, item.src.lastIndexOf("/")+1)+img.replace("_small_a.", "_small.").replace("_small_a.", "_small.");
						item.isClicked = false;						
					}									
				});
			
				//ground3Clicked = true;
				//ground4Clicked = false;				
				group2Index = '01';
				
				//Call Show Content Function
				showContentPress(group1Index, group2Index)
			} else {
				group3.each(function(item, i){
					
					if(i != index){
						var img = item.src.substr(item.src.lastIndexOf("/")+1);
						item.src = item.src.substr(0, item.src.lastIndexOf("/")+1)+img.replace("_small_a.", "_small.").replace("_small_a.", "_small.");
						item.isClicked = false;						
					}					
				});
				
				group2Index = '0' + index;					
				//Call Show Content Function
				showContentPress(group1Index, group2Index);
			}
			this.isClicked = true;
			var img = this.src.substr(this.src.lastIndexOf("/")+1);
			this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_small_a.", "_small.").replace("_small.", "_small_a.").replace("_N.", "_ACTIVE.");									
			ground3Clicked = true;
			
		});
		image.addEvent('mouseover', function(e) {	
			var img = this.src.substr(this.src.lastIndexOf("/")+1);			
			this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_small.", "_small_a.");
		});
		image.addEvent('mouseout', function(e) {							
			if(this.isClicked){                             
				var img = this.src.substr(this.src.lastIndexOf("/")+1);
				this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_small_a.", "_small.").replace("_small.", "_small_a.");
			} else {
				var img = this.src.substr(this.src.lastIndexOf("/")+1);
				this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_small_a.", "_small.");
			}			
		});
		
	});



}
//Show contentTerminal for each Clicked.
function showContentPress(group1Index, group2Index) {
	
	//Hide Content
	/*	
		var aContainerNode = $$('div.contentContainer').getElements('div.contentTerminal');
		aContainerNode.each(function(_item, _index){
			_item.addClass('terminal');		
		});
	*/
	
	hiddenSelection();
	
	
	var contentContainerId = 'contentContainer_' + group1Index + group2Index;
	
	//alert(contentContainerId);
	//Show - Hide Warning

	if (group1Index != '12' || group1Index != '0') {			
			//get Element to show content	
			$$('div.' + contentContainerId).setStyle('display','block');
			var cont = $$('div.' + contentContainerId).setStyle('display','block');
		
	}
	
	else {
			
			//get Element to show content	
			$$('div.' + contentContainerId).setStyle('display','block');			
	}
}







//Hidden All Sélectionnez
function hiddenSelection() {
	var divTags = $$('div.contentTerminal');
	divTags.each(function (content, index) {
		if (content.getStyle('display') == 'block') {
			content.setStyle('display', 'none');
		}
	});	
};

//Active Images for terminalPlan 
function activeImagesTerminal() {
	//global var 
	var group1Index = 0;
	var group2Index = 0;
	//Index 
	/*
		var ground1Clicked = false;
		var ground2Clicked = false;
		var ground3Clicked = false;
		var ground4Clicked = false;
	*/
	//////////////////////////////////////////
	var group1 = $$("img.group1");
	var group3 = $$("img.group3");	
	var group2 = $$("img.group2");	
	var group4 = $$("img.group4");	
	if (!group1.length) {
		return;
	}
	
	group1.each(function(image, index){		
		image.addEvent('click', function(e) {
			e.stop();					
			if(image.getParent().id == 'arrival02'){
				$$('ul.lstTerminalDetailArrival')[0].setStyle('display', 'block');
				group1.each(function(item, i){
					if(i != index){
						var img = item.src.substr(item.src.lastIndexOf("/")+1);
						item.src = item.src.substr(0, item.src.lastIndexOf("/")+1)+img.replace("_o.", "_normal.").replace("_active.", "_normal.");
						item.isClicked = false;						
					}									
				});
				//ground1Clicked = true;
				//ground2Clicked = false;
				
				//set normal for all Letter in sub Terminal 2
				group2.each(function(item, i){				
					
					var img = item.src.substr(item.src.lastIndexOf("/")+1);
					item.src = item.src.substr(0, item.src.lastIndexOf("/")+1)+img.replace("_o.", "_normal.").replace("_active.", "_normal.");
					item.isClicked = false;
					
					
				});	
				group1Index = '10';
				//Call Show Content Function
				showContentTerminal(group1Index, group2Index)
			} else {
				group1.each(function(item, i){
					$$('ul.lstTerminalDetailArrival')[0].setStyle('display', 'none');
					if(i != index){
						var img = item.src.substr(item.src.lastIndexOf("/")+1);
						item.src = item.src.substr(0, item.src.lastIndexOf("/")+1)+img.replace("_o.", "_normal.").replace("_active.", "_normal.");
						item.isClicked = false;						
					}					
				});
				
				group1Index = '0' + index;								
				//Call Show Content Function
				showContentTerminal(group1Index, group2Index);
			}
			this.isClicked = true;
			var img = this.src.substr(this.src.lastIndexOf("/")+1);
			this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_o.", "_active.").replace("_normal.", "_active.").replace("_N.", "_ACTIVE.");									
			ground1Clicked = true;			
		});
		image.addEvent('mouseover', function(e) {	
			var img = this.src.substr(this.src.lastIndexOf("/")+1);			
			this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_normal.", "_o.");
		});
		image.addEvent('mouseout', function(e) {							
			if(this.isClicked){                             
				var img = this.src.substr(this.src.lastIndexOf("/")+1);
				this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_o.", "_active.").replace("_normal.", "_active.");
			} else {
				var img = this.src.substr(this.src.lastIndexOf("/")+1);
				this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_o.", "_normal.");
			}			
		});
	});
	/*/////For Arrival Terminal 2 : Letter/////////////////*/
	
	if (!group2.length) {
		return;
	}
	
	group2.each(function(image, index){		
		image.addEvent('click', function(e) {
			e.stop();								
			group2.each(function(item, i){				
				if(i != index){
					var img = item.src.substr(item.src.lastIndexOf("/")+1);
					item.src = item.src.substr(0, item.src.lastIndexOf("/")+1)+img.replace("_o.", "_normal.").replace("_active.", "_normal.");
					item.isClicked = false;
				}
				
			});			
			this.isClicked = true;
			var img = this.src.substr(this.src.lastIndexOf("/")+1);
			this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_o.", "_active.").replace("_normal.", "_active.").replace("_N.", "_ACTIVE.");		
			
			//ground2Clicked = true;
			group1Index = (index + 1) + '1';
			
			//Call Show Content Function
			showContentTerminal(group1Index, group2Index);	
		});
		image.addEvent('mouseover', function(e) {	
			var img = this.src.substr(this.src.lastIndexOf("/")+1);			
			this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_normal.", "_o.");
		});
		image.addEvent('mouseout', function(e) {							
			if(this.isClicked){                             
				var img = this.src.substr(this.src.lastIndexOf("/")+1);
				this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_o.", "_active.").replace("_normal.", "_active.");
			} else {
				var img = this.src.substr(this.src.lastIndexOf("/")+1);
				this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_o.", "_normal.");
			}			
		});
	});
	/*For Arrival Terminal*/
	
	
	if (!group3.length) {
		return;
	}	
	group3.each(function(image, index){		
		image.addEvent('click', function(e) {
			e.stop();					
			if(image.getParent().id == 'depature02'){
				$$('ul.lstTerminalDetailDepature')[0].setStyle('display', 'block');
				group3.each(function(item, i){
					if(i != index){
						var img = item.src.substr(item.src.lastIndexOf("/")+1);
						item.src = item.src.substr(0, item.src.lastIndexOf("/")+1)+img.replace("_o.", "_normal.").replace("_active.", "_normal.");
						item.isClicked = false;						
					}									
				});
				//set normal for all Letter in sub Terminal 2
				group4.each(function(item, i){				
					
					var img = item.src.substr(item.src.lastIndexOf("/")+1);
					item.src = item.src.substr(0, item.src.lastIndexOf("/")+1)+img.replace("_o.", "_normal.").replace("_active.", "_normal.");
					item.isClicked = false;
					
				});
				//ground3Clicked = true;
				//ground4Clicked = false;				
				group2Index = '01';
				
				//Call Show Content Function
				showContentTerminal(group1Index, group2Index)
			} else {
				group3.each(function(item, i){
					$$('ul.lstTerminalDetailDepature')[0].setStyle('display', 'none');
					if(i != index){
						var img = item.src.substr(item.src.lastIndexOf("/")+1);
						item.src = item.src.substr(0, item.src.lastIndexOf("/")+1)+img.replace("_o.", "_normal.").replace("_active.", "_normal.");
						item.isClicked = false;						
					}					
				});
				
				group2Index = '0' + index;					
				//Call Show Content Function
				showContentTerminal(group1Index, group2Index);
			}
			this.isClicked = true;
			var img = this.src.substr(this.src.lastIndexOf("/")+1);
			this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_o.", "_active.").replace("_normal.", "_active.").replace("_N.", "_ACTIVE.");									
			ground3Clicked = true;
			
		});
		image.addEvent('mouseover', function(e) {	
			var img = this.src.substr(this.src.lastIndexOf("/")+1);			
			this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_normal.", "_o.");
		});
		image.addEvent('mouseout', function(e) {							
			if(this.isClicked){                             
				var img = this.src.substr(this.src.lastIndexOf("/")+1);
				this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_o.", "_active.").replace("_normal.", "_active.");
			} else {
				var img = this.src.substr(this.src.lastIndexOf("/")+1);
				this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_o.", "_normal.");
			}			
		});
		
	});

	/*/////For Depature Terminal 2 : Letter/////////////////*/
		
	if (!group4.length) {
		return;
	}	
	group4.each(function(image, index){		
		image.addEvent('click', function(e) {
			e.stop();								
			group4.each(function(item, i){				
				if(i != index){
					var img = item.src.substr(item.src.lastIndexOf("/")+1);
					item.src = item.src.substr(0, item.src.lastIndexOf("/")+1)+img.replace("_o.", "_normal.").replace("_active.", "_normal.");
					item.isClicked = false;
				}
				
			});			
			this.isClicked = true;
			var img = this.src.substr(this.src.lastIndexOf("/")+1);
			this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_o.", "_active.").replace("_normal.", "_active.").replace("_N.", "_ACTIVE.");		
			
			//ground4Clicked = true;
			group2Index = (index + 1) + '1';
			//Call Show Content Function
			showContentTerminal(group1Index, group2Index)
			
			
		});
		image.addEvent('mouseover', function(e) {	
			var img = this.src.substr(this.src.lastIndexOf("/")+1);			
			this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_normal.", "_o.");
		});
		image.addEvent('mouseout', function(e) {							
			if(this.isClicked){                             
				var img = this.src.substr(this.src.lastIndexOf("/")+1);
				this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_o.", "_active.").replace("_normal.", "_active.");
			} else {
				var img = this.src.substr(this.src.lastIndexOf("/")+1);
				this.src = this.src.substr(0, this.src.lastIndexOf("/")+1)+img.replace("_o.", "_normal.");
			}			
		});
		
	});	
}
//Show contentTerminal for each Clicked.
function showContentTerminal(group1Index, group2Index, group3Index, group4Index) {
	//Hide Content
	/*	
		var aContainerNode = $$('div.contentContainer').getElements('div.contentTerminal');
		aContainerNode.each(function(_item, _index){
			_item.addClass('terminal');		
		});
	*/
	hiddenSelection();
	
	var contentContainerId = 'contentContainer_' + group1Index + group2Index;
	//alert(contentContainerId);
	//Show - Hide Warning
	if (group1Index == '0') {
		$$('p.warningSelectArrival')[0].setStyle('display', 'none');
		if (group2Index == '01') {			
			$$('p.warningSelectDepature')[0].setStyle('display', 'block');
		} else { 
			$$('p.warningSelectDepature')[0].setStyle('display', 'none');
		}		
	} else if (group1Index == '10') {
		$$('p.warningSelectArrival')[0].setStyle('display', 'block');
		if (group2Index == '01') {			
			$$('p.warningSelectDepature')[0].setStyle('display', 'block');
		} else { 
			$$('p.warningSelectDepature')[0].setStyle('display', 'none');
		} 
		
	} else if (group1Index != '10' || group1Index != '0') {
		//Depature
		$$('p.warningSelectArrival')[0].setStyle('display', 'none');
		if (group2Index == '0') {
			$$('p.warningSelectDepature')[0].setStyle('display', 'none');
		} else if (group2Index == '01') {			
			$$('p.warningSelectDepature')[0].setStyle('display', 'block');
			//alert(0);
		} else {
			//Show								
			$$('p.warningSelectArrival')[0].setStyle('display', 'none');
			$$('p.warningSelectDepature')[0].setStyle('display', 'none');
			//get Element to show content	
			$$('div.' + contentContainerId).setStyle('display','block');
		}
	} else {
			//Show								
			$$('p.warningSelectArrival')[0].setStyle('display', 'none');
			$$('p.warningSelectDepature')[0].setStyle('display', 'none');
			//get Element to show content	
			$$('div.' + contentContainerId).setStyle('display','block');			
	}
}


/////////Start Hover Function///////////////
function initImages() {	
	//
	document.imageOut = new Object();
	document.imageOver = new Object();
	// PNG Fix for IE<7
	var png_fix = "../images/png_fix.gif";
	var pngRegExp = new RegExp("\\.png$", "i")
	var f = "DXImageTransform.Microsoft.AlphaImageLoader";
	//
	var imageArray = $$("img", "input");
	imageArray.each(function(item){
		var image = item.src.substr(item.src.lastIndexOf("/")+1);
		var id = item.id || image.replace("_n.", "").replace("_N.", "");
		var hover = (image.toLowerCase().lastIndexOf("_n.") !=-1);
		//
		if (hover && document.imageOut && document.imageOver) {
			document.imageOut[id] = new Image();
			document.imageOut[id].src = item.src;
			document.imageOver[id] = new Image();
			document.imageOver[id].src = item.src.substr(0, item.src.lastIndexOf("/")+1)+image.replace("_n.", "_o.").replace("_N.", "_O.");
		}
		// PNG Fix for IE<7
		if (window.ie && !window.ie7 && image.test(pngRegExp)) {
			item.style.width = item.offsetWidth+"px";
			item.style.height = item.offsetHeight+"px";
			item.style.filter = "progid:"+f+"(src='"+item.src+"', sizingMethod='scale');";
			item.src = png_fix;
		}
		//
		if (hover && document.imageOut && document.imageOver) {
			item.onmouseover = function(){
				
				if (document.imageOver && document.imageOver[this.id]) setImage(this, document.imageOver[this.id].src);
			}
			item.onmouseout = function(){
				if (document.imageOut && document.imageOut[this.id]) setImage(this, document.imageOut[this.id].src);
			}
			item.id = id;
			//
			function setImage(imageObject, src) {
				if (window.ie && !window.ie7) {
					if (imageObject.filters[f] && imageObject.filters[f].src.test(pngRegExp)) {
						imageObject.filters[f].src = src;
					} else {
						imageObject.src = src;
					}
				} else {
					imageObject.src = src;
				}
			}
		}
	});
}
/////////End Hover Function///////////////
//
window.addEvent("domready", function(){
	//Hide  All Content in Sélectionnez
	//hiddenSelection();	

	initAutoScrollText();
	initSlidersQuestions();	
	initActive();
	
	var _left = window.getWidth() / 2 - 50;
	new ShowLayer('popupDetail', {
		position: 'absolute',
		left: _left,
		top: '450px',
		zIndex: '9999'
	});
});
window.addEvent("load", function(){
	//initImages();
	//Active Sélectionnez
	activeImagesTerminal();	
	activePress();	
});