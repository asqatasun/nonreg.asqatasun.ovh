

// roll-over d'une image
function swapImg(imgName, imgFile) {
	document.images[imgName].src = imgFile;
}

// ouverture d'une fen�tre version "accessible"
function OpenWindowByTarget(target, url, width, height, top, left) {
	var wdw;
	var options = "menubar=yes,toolbar=yes,resizable=yes,scrollbars=yes";
	wdw = window.open(url, target, "top="+top+",left="+left+",width="+width+",height="+height+","+options);
	wdw.focus();
	return false;
}
