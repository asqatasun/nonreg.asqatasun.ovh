

function writeScroll() {
	document.write('<div id="bandeau" style="position:relative; width:207px; height:100px;"></div>');
}

// resize automatique
window.onresize = reloadScroller;

// cr�ation du scroller
var myScroller = new Scroller(0, 0, 207, 100, 0, 4, "center");
myScroller.setColors("#000000", "#EAEBEC", "#D8D8D8");

// ajout des informations
myScroller.addItem('<img src="http://www.nimes-aeroport.fr/img/FR/pert_icon1.gif" alt="Ic�ne information"><br><h3><a href="http://www.nimes-aeroport.fr/perturbation/index.asp?rub_code=17&amp;pert_id=18">Enregistrement en ligne RYANAIR</a></h3>');


function startScroll() {
	var InfoLayer;
	var lx, ly;
	// Locate placeholder layer so we can use it to position the scrollers.
	InfoLayer = getLayer("bandeau");
	lx = getPageLeft(InfoLayer);
	ly = getPageTop(InfoLayer);
	// d�calages MAC
	if (isIE && isMac) {
		x += 10;
		y += 15;
	} else if (isSF) {
		x += 8;
		y += 8;
	}

//	alert(lx + ', ' + ly);
	
	// Create the first scroller and position it.
	myScroller.create();
	myScroller.hide();
	myScroller.moveTo(lx, ly);
	myScroller.setzIndex(100);
	myScroller.show();
}

function reloadScroller() {
	// Locate new placeholder layer so we can use it to position the scrollers.
	var InfoLayer = getLayer("bandeau");
	var lx = getPageLeft(InfoLayer);
	var ly = getPageTop(InfoLayer);

//	alert(lx + ', ' + ly);

	// d�calages MAC
	if (isIE && isMac) {
		x += 10;
		y += 15;
	} else if (isSF) {
		x += 8;
		y += 8;
	}
	// Move the scroller
	myScroller.moveTo(lx, ly);
}

