
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />

<script language="javascript" src="tsm.js" type="text/javascript"></script>
<style>
body {margin-left:0px; margin-top:0px;}

.codeA {
	font-family: tahoma, arial;
	font-size: 10px;
	color :#ffffff;
	padding: 0px;
	margin: 0 0 0 0;
	solid: #3F5B6D;
	background-color: #3F5B6D;
	text-align:center
}

.codeArr {
	font-family: tahoma, arial;
	font-size: 10px;
	color :#FF6600;
	padding: 0px;
	margin: 0 0 0 0;
	solid: #3F5B6D;
	background-color: #3F5B6D;
	text-align:center
}

.codeDep {
	font-family: tahoma, arial;
	font-size: 10px;
	color :#C7E4F8;
	padding: 0px;
	margin: 0 0 0 0;
	solid: #3F5B6D;
	background-color: #517186;
	text-align:center
}
.codeCren {
	font-family: tahoma, arial;
	font-size: 10px;
/*	color :#3F5B6D; */
    color: #FFFFFF;
	padding: 0px;
	margin: 0 0 0 0;
	solid: #3F5B6D;
	background-color: #3F5B6D;
	text-align:center
}
.Style1 {
	font-family: Arial, Helvetica, sans-serif;
	color: #FFFFFF;
}
</style>

<script>
function tsmInitAll() {
	with(otherStyleOn = new style) {
		$font_family        = "tahoma, arial";
		$font_color         = "#CCFF00";
		$font_size          = "11px";
		$font_style         = "normal";
		$background_color   = "#2D4C5F";
		$border_size        = "0px";
		$border_style       = "solid";
		$border_color       = "#3F5B6D";
		$padding            = "2px";
	}
	
	with(otherStyleOff = new style) {
		$font_family        = "tahoma, arial";
		$font_color         = "#FFFFFF";
		$font_size          = "11px";
		$font_style         = "normal";
		$background_color   = "#3F5B6D";
		$border_size        = "0px";
		$border_style       = "solid";
		$border_color       = "#eeeeee";
		$padding            = "3px";
	}
	
	with(atsm = new tabStripMenu("showMe",220,260)) {
		addTab("<b>Ce jour</b>",
		"<table width=100% ><tr class=codeA><td width=15%><font color=#FF6600>Arr.</font></td><td width=15%>D&eacute;p.</td><td width=70%>Trafic A&eacute;roport</td></tr>"
+ "<tr class=codeArr><td>10:30</td><td>-</td><td>Entrainement - ERJ-190-100STD</td></tr>"
+ "<tr class=codeCren><td>10:33</td><td>-</td><td>D�but Ent. creneau 1 - ERJ-190-100STD</td></tr>"
+ "<tr class=codeArr><td>15:28</td><td>-</td><td>Fret</td></tr>"
+ "<tr class=codeArr><td>15:30</td><td>-</td><td>Entrainement - TBM-700</td></tr>"
+ "<tr class=codeCren><td>15:40</td><td>-</td><td>D�but Ent. creneau 1 - TBM-700</td></tr>"
+ "<tr class=codeCren><td>-</td><td>15:42</td><td>Fin Ent. creneau 1 - TBM-700</td></tr>"
+ "<tr class=codeDep><td>-</td><td>15:45</td><td>Entrainement - TBM-700</td></tr>"
+ "<tr class=codeArr><td>16:00</td><td>-</td><td>Entrainement - A-300ST Super Transporter, Beluga</td></tr>"
+ "<tr class=codeCren><td>16:03</td><td>-</td><td>D�but Ent. creneau 1 - A-300ST Super Transporter, Beluga</td></tr>"
+ "<tr class=codeCren><td>-</td><td>16:27</td><td>Fin Ent. creneau 1 - ERJ-190-100STD</td></tr>"
+ "<tr class=codeDep><td>-</td><td>16:30</td><td>Entrainement - ERJ-190-100STD</td></tr>"
+ "<tr class=codeCren><td>-</td><td>16:37</td><td>Fin Ent. creneau 1 - A-300ST Super Transporter, Beluga</td></tr>"
+ "<tr class=codeDep><td>-</td><td>16:40</td><td>Entrainement - A-300ST Super Transporter, Beluga</td></tr>"
+ "<tr class=codeArr><td>20:30</td><td>-</td><td>Fret</td></tr>"
+ "</table>");
		addTab("&nbsp;Mar&nbsp",
		"<table width=100% ><tr class=codeA><td width=15%><font color=#FF6600>Arr.</font></td><td width=15%>D&eacute;p.</td><td width=70%>Trafic A&eacute;roport</td></tr>"
+ "<tr class=codeArr><td>2:30</td><td>-</td><td>Fret</td></tr>"
+ "<tr class=codeDep><td>-</td><td>9:00</td><td>Fret</td></tr>"
+ "<tr class=codeArr><td>15:00</td><td>-</td><td>Fret</td></tr>"
+ "<tr class=codeArr><td>16:40</td><td>-</td><td>Aviation Generale</td></tr>"
+ "</table>");
		addTab("&nbsp;Mer&nbsp",
		"<table width=100% ><tr class=codeA><td width=15%><font color=#FF6600>Arr.</font></td><td width=15%>D&eacute;p.</td><td width=70%>Trafic A&eacute;roport</td></tr>"
+ "<tr class=codeArr><td>10:20</td><td>-</td><td>Entrainement - A-330-200</td></tr>"
+ "<tr class=codeCren><td>11:00</td><td>-</td><td>D�but Ent. creneau 1 - A-330-200</td></tr>"
+ "<tr class=codeArr><td>13:00</td><td>-</td><td>Entrainement - A-330-200</td></tr>"
+ "<tr class=codeCren><td>13:03</td><td>-</td><td>D�but Ent. creneau 1 - A-330-200</td></tr>"
+ "<tr class=codeCren><td>-</td><td>14:57</td><td>Fin Ent. creneau 1 - A-330-200</td></tr>"
+ "<tr class=codeDep><td>-</td><td>15:00</td><td>Entrainement - A-330-200</td></tr>"
+ "<tr class=codeCren><td>-</td><td>19:20</td><td>Fin Ent. creneau 1 - A-330-200</td></tr>"
+ "<tr class=codeDep><td>-</td><td>19:40</td><td>Entrainement - A-330-200</td></tr>"
+ "</table>");
		addTab("&nbsp;Jeu&nbsp",
		"<table width=100% ><tr class=codeA><td width=15%><font color=#FF6600>Arr.</font></td><td width=15%>D&eacute;p.</td><td width=70%>Trafic A&eacute;roport</td></tr>"
+ "<tr class=codeDep><td>-</td><td>13:00</td><td>Fret</td></tr>"
+ "<tr class=codeArr><td>17:50</td><td>-</td><td>Passagers</td></tr>"
+ "</table>");
		addTab("&nbsp;Ven&nbsp",
		"<table width=100% ><tr class=codeA><td width=15%><font color=#FF6600>Arr.</font></td><td width=15%>D&eacute;p.</td><td width=70%>Trafic A&eacute;roport</td></tr>"
+ "<tr class=codeDep><td>-</td><td>10:15</td><td>Passagers</td></tr>"
+ "</table>");
		addTab("&nbsp;Sam&nbsp",
		"<table width=100% ><tr class=codeA><td width=15%><font color=#FF6600>Arr.</font></td><td width=15%>D&eacute;p.</td><td width=70%>Trafic A&eacute;roport</td></tr>"
+ "<tr class=codeArr><td>2:10</td><td>-</td><td>Passagers</td></tr>"
+ "<tr class=codeDep><td>-</td><td>15:00</td><td>Passagers</td></tr>"
+ "</table>");
		addTab("&nbsp;Dim&nbsp",
		"<table width=100% ><tr class=codeA><td width=15%><font color=#FF6600>Arr.</font></td><td width=15%>D&eacute;p.</td><td width=70%>Trafic A&eacute;roport</td></tr>"
+ "<tr class=codeArr><td>10:00</td><td>-</td><td>Fret</td></tr>"
+ "<tr class=codeDep><td>-</td><td>23:00</td><td>Fret</td></tr>"
+ "</table>");
		setStyles(otherStyleOn,otherStyleOff);
		build();
	}
}
</script>

</head>

<body onLoad="tsmInitAll(); tsmLoadAll();" style="background-color: #333333;">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
   <!--	<tr>
		<td height="41"><center><font color="#FFFFFF" size="1" face="tahoma, Arial, Helvetica, sans-serif"><b>Les Pr&eacute;visions de vols <sup>(*)</sup><br>pour les 7 prochains jours...</b></font></td>
	</tr>  -->
	<tr>
		<td align="center"><div id='showMe'></div></td>
	</tr>
	<tr>
		<td style="background-color: #333333;">
			<center>
				<font color="#C7E4F8" size="1" face="tahoma, Arial, Helvetica, sans-serif"><strong>Horaires d'ouverture  :
				08h - 20h</strong><br>
			  </font>
				<font size="1" class="Style1">
		En dehors de ces horaires <b>pr&eacute;avis 2h</b><br>
		aupr&egrave;s du 33 (0) 6 80 62 70 72 <br>
		(*) sujettes &agrave modifications sans pr&eacute;avis </font>
		                    </center>
		</td>
	</tr>
</table>

</body>
</html>
