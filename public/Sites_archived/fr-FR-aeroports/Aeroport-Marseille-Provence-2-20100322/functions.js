// Menu pour ie //
function IEHoverPseudo() {
	var navItems = document.getElementById("primary-nav").getElementsByTagName("li");
	for (var i=0; i<navItems.length; i++) {
		if(navItems[i].className == "menuparent") {
			navItems[i].onmouseover=function() { this.className += " over"; }
			navItems[i].onmouseout=function() { this.className = "menuparent"; }
		}
	}
}
window.onload = function() {IEHoverPseudo();}

///////////////////// liste déroulante /////////////////////////////
function Lien() {
	i = document.Choix.Liste.selectedIndex;
	if (i == 0) return;
	url = document.Choix.Liste.options[i].value;
	parent.location.href = url;
} 

// afficher chacher une div //
function toggle(obj) {
	var el = document.getElementById(obj);
	var imgMinus = document.images[obj+'_minus'];
	var imgPlus = document.images[obj+'_plus'];

	if ( el.style.display != 'none' ) { 
		el.style.display = 'none';
		imgMinus.style.display = 'none';
		imgPlus.style.display = '';
	}
	else { 
		el.style.display = '';
		imgMinus.style.display = '';
		imgPlus.style.display = 'none';
	}
}

// focus du champs de saisie newsletters //
function changeVal(id, param)
{
	if(param == '')
	{	// on focus
		document.getElementById(id).value = param;
	}
	else
	{	// on blur
		if(document.getElementById(id).value == '')
			document.getElementById(id).value = param;
	}
}

function loadIframe(iframeName, url) 
{
	if ( window.frames[iframeName] ) {
	window.frames[iframeName].location = url;   
	return false;
	}

	  else return true;
 }

function ouvrir(url, l, h) 
{
	hauteur = Math.round((screen.availHeight-h)/2);
	largeur =Math.round((screen.availWidth-l)/2);
	window.open(url, "mp2Skyscanner", "toolbar=0, location=0, directories=0, status=0, scrollbars=1, resizable=1, menubar=0, top="+hauteur+", left="+largeur+", width="+l+", height="+h);
}
