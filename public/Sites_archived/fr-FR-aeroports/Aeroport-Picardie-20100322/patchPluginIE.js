
/**
 * Permet de rendre r�actif un objet flash sans que l'utilisateur ai � activer l'objet en cliquant dessus
 * Cette m�thode �crit le code html pour ins�rer l'objet.
 * Exemple d'utilisation:
 *
 * <script type="text/javascript" src="js/patchPluginIE.js"></script>
 * <script type="text/javascript">
 *	aParams = new Array();
 *	aParams['wMode'] = 'transparent';
 *	aParams['scalar'] = 'noborder';
 *	printFlashObject("test.swf", "testId", "550", "400", "#ffffff" , 'position: relative;', aParams);
 *	aParams = null;
 * </script>
 *
 * @param sMovie le nom du fichier flash, ce qui doit aller dans l'attribut value de <param name="movie" value="" />
 * @param sName l'identifiant de l'objet (attribut id de <object />)
 * @param sWidth largeur de l'objet
 * @param sHeight hauteur de l'objet
 * @param sColor couleur de fond de l'objet, par d�faut #ffffff
 * @param sStyle la valeur de l'attribut style, vide par d�faut
 * @param array aParams param�tres additionnels pour l'objet
 * @return void
 * @version 1.0
 */
 
function printFlashObject(sMovie, sName, sWidth, sHeight, sColor, sStyle, aParams)  {

	if(sColor == undefined){
		sColor = '#ffffff';
	}//end if
	
	if(sName == undefined){
		sName = '';
	}//end if
	
	if(sStyle == undefined){
		sStyle = '';
	}//end if
	
	if(aParams == undefined){
		aParams = new Array();
	}//end if
	
	if(aParams['quality'] == undefined){
		aParams['quality'] = 'best';
	}//end if
	
	if(aParams['loop'] == undefined){
		aParams['loop'] = 'true';
	}//end if
	
	if(aParams['wmode'] == undefined){
		aParams['wmode'] = 'transparent';
	}//end if
	
	var sHtmlContent = '<object id="' + sName + '" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" ';
	sHtmlContent += ' width="' + sWidth + '" height="' + sHeight + '" align="middle" style="' + sStyle + '">';

	sHtmlContent += '<param name="movie" value="' + sMovie + '" />';
	sHtmlContent += '<param name="bgcolor" value="' + sColor + '" />';
	
	for(var sIndex in aParams){
		sHtmlContent += '<param name="'+ sIndex + '" value="' + aParams[sIndex] + '" />';
	}//end for

	sHtmlContent += '<embed src="' + sMovie + '" bgcolor="' + sColor + '" width="' + sWidth + '" ';
	sHtmlContent += 'height="' + sHeight + '" name="' + sMovie + '" '; 
	for(var sIndex in aParams){
		sHtmlContent += sIndex + '="' + aParams[sIndex] + '" ';
	}//end for

	sHtmlContent += 'type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer">';
	sHtmlContent += '</object>';
	document.write(sHtmlContent);

	// On vide le tableau pour ne pas les animations suivantes
	aParams = null;
	
}//end function