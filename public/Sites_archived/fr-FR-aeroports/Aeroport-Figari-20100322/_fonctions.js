//fonction utiliser par les menus deroulants pour afficher et cacher, fournir l'id du menu a montrer et la partie de l'id commune a tous les menus
function montrer(id,menu) 
{
	var d = document.getElementById(id);
	for (var i = 1; i<=20; i++) {
		if (document.getElementById(menu+''+i)) {document.getElementById(menu+''+i).style.display='none';}
	}
	if (d) {d.style.display='block';}
}


function montre(id) 
{
	var d = document.getElementById(id);
		for (var i = 1; i<=10; i++) {
			if (document.getElementById('smenu'+i)) {document.getElementById('smenu'+i).style.display='none';}
		}
	if (d) {d.style.display='block';}
}

//Permet d'ouvrir les fenetres pop-up -- Action dans un <a href="...>
function ouvreFenetre(page, largeur, hauteur) 
{ 
	var top=(screen.height-hauteur)/2;
  	var left=(screen.width-largeur)/2;
	var fenetre = window.open(page, "", "scrollbars=no,menubar=no,toolbar=no,resizable=no,top="+top+",left="+left+",width="+largeur+",height="+hauteur);
}

//permet d'ouvrir une image dans un popup aux dimensions indiquees
function popupimage(img,l,h) { 
	titre="CCI Ajaccio"; 
	var top=(screen.height-h)/2;
  	var left=(screen.width-l)/2;
	
	w=open("","image","scrollbars=no,menubar=no,toolbar=no,resizable=no,top="+top+",left="+left+",width="+l+",height="+h); 
	w.document.write("<BODY style='margin:0px; padding:0px'>");
	w.document.write("<IMG src='"+img+"' border='0' style='position:absolute;top:0px;left:0px;'>"); 
	w.document.write("</BODY></HTML>"); 
	w.document.close(); 
} 

//Permet de supprimer la valeur par defaut d'un input -- action: OnFocus d'un input (text ou area)
function focusInput(Obj,val) { 
	if (Obj.value == val)
		Obj.value = '';
}

//R�ecrire la valeur par defaut de l'input s'il est vide -- action: OnBlur d'un input (text ou area)
function blurInput(Obj) { 
	if (Obj.value == '')
		Obj.value = Obj.defaultValue;
}

//Permet d'inserer des tags de mise en page dans une zone de texte input ou textearea
//nom_du_textarea = document."nomformulaire"."nomTextarea"
function AddText(startTag,defaultText,endTag,nom_du_textarea) 
{
	 if (nom_du_textarea.createTextRange) 
	 {
		  var text;
		  nom_du_textarea.focus(nom_du_textarea.caretPos);
		  nom_du_textarea.caretPos = document.selection.createRange().duplicate();
		  if(nom_du_textarea.caretPos.text.length>0)
		  {
			nom_du_textarea.caretPos.text = startTag + nom_du_textarea.caretPos.text + endTag;
		  }
		  else
		  {
			nom_du_textarea.caretPos.text = startTag+defaultText+endTag;
		  }
	 }
	 else 
		nom_du_textarea.value += startTag+defaultText+endTag;
}

//Permet d'inserer des tags de mise en page dans une zone de texte input ou textearea
//nom_du_textarea = document."nomformulaire"."nomTextarea"
function bbcode(code,nom_du_textarea) 
{
	 if (nom_du_textarea.createTextRange) 
	 {
		  var text;
		  nom_du_textarea.focus(nom_du_textarea.caretPos);
		  nom_du_textarea.caretPos = document.selection.createRange().duplicate();
		  if(nom_du_textarea.caretPos.text.length>0)
		  {
				switch (code)
				{
					case "b": nom_du_textarea.caretPos.text = "[b]" + nom_du_textarea.caretPos.text + "[/b]";break;
					case "i": nom_du_textarea.caretPos.text = "[i]" + nom_du_textarea.caretPos.text + "[/i]";break;
					case "u": nom_du_textarea.caretPos.text = "[u]" + nom_du_textarea.caretPos.text + "[/u]";break;
					case "lien":nom_du_textarea.caretPos.text = "[url=]" + nom_du_textarea.caretPos.text + "[/url]";break;
					case "email":nom_du_textarea.caretPos.text = "[email=]" + nom_du_textarea.caretPos.text + "[/email]";break;
					default : nom_du_textarea.caretPos.text = "[color="+code+"]" + nom_du_textarea.caretPos.text + "[/color]";break;
				}
		  }
	 }
}