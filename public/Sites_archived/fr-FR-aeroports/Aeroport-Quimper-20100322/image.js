function getImageY(img){
	var y,obj;
	if((document.layers && !document.getElementById) || (document.getElementById && parseInt(navigator.appVersion) >= 5)){
		if(img.container!=null){
			return img.container.pageY+img.y;
		}else{
			return img.y;
		}
	}else{
		y=0; obj=img;
		while (obj.offsetParent!=null){
			y += obj.offsetTop;
			obj=obj.offsetParent;
		}
		y += obj.offsetTop;
		return y;
	}
	return -1;
}

function getImageX(img){
	var x,obj;
	if((document.layers && !document.getElementById) || (document.getElementById && parseInt(navigator.appVersion) >= 5)){
		if(img.container!=null){
			return img.container.pageX+img.x;
		}else{
			return img.x;
		}
	}else{
		x=0; obj=img;
		while (obj.offsetParent!=null){
			x += obj.offsetLeft;
			obj=obj.offsetParent;
		}
		x += obj.offsetLeft;
		return x;
	}
	return -1;
}