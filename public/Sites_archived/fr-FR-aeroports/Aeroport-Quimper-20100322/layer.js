
// classe Layer()
// fonctions de manipulation de calques html en javascript quelque soit le navigateur
// copyright 2002-2005 http://www.icomme.fr
// derni�re modification : 13/12/2004 dc@icomme.fr (fonctions de scrolling vertical)

function layer_move(x_pos,y_pos){
 var obj=this.style;
 if(this.NS){obj.moveTo(x_pos,y_pos);}
 else{obj.top=y_pos;obj.left=x_pos;}
}

function layer_size(width,height){
 var obj=this.style;
 if(this.NS){obj.clip.width=width;obj.clip.height=height;}
 else{
  if(this.MZ){obj.width=width;obj.height=height;}
  else{obj.clip='rect(auto '+(width)+' '+(height)+' auto)';}
 }
}

function layer_clear(){
 if(this.NS){
  this.document.open();
  this.document.write('');
  this.document.close();
 }
 else{
  this.layer.innerHTML='';
 }
}

function layer_write(htmlCode){
 if(this.NS){
  this.document.write(htmlCode);
  this.document.close();
 }
 else{
  this.layer.innerHTML+=htmlCode;
 }
}

function layer_read(){
 if(this.NS){
  this.document.open();
  this.document.close();
 }
 else{
  return this.layer.innerHTML;
 }
}

function layer_setVisible(){
	this.style.visibility=this.SHOW;
	if(this.isVScroll) this.VScrollLayer.layer.setVisible();
}
function layer_setInvisible(){
	this.style.visibility=this.HIDE;
	if(this.isVScroll) this.VScrollLayer.layer.setInvisible();
}
function layer_isVisible(){return(this.style.visibility==this.SHOW);}
function layer_getWidth(){return((this.NS)?this.style.clip.width:(this.MZ)?parseInt(this.style.width):this.style.pixelWidth);}
function layer_getLeft(){return((this.NS)?this.style.left:(this.MZ)?parseInt(this.style.left):this.style.pixelLeft);}
function layer_getTop(){return((this.NS)?this.style.top:(this.MZ)?parseInt(this.style.top):this.style.pixelTop);}
function layer_getHeight(){return((this.NS)?this.style.clip.height:(this.MZ)?parseInt(this.style.height):this.style.pixelHeight);}
function layer_setWidth(width){this.size(width,this.getHeight());}
function layer_setLeft(left){this.move(left,this.getTop());}
function layer_setTop(top){this.move(this.getLeft(),top);}
function layer_setHeight(height){this.size(this.getWidth(),height);}

function VScroll(l,h){
 // classe d'initialisation du scrolling vertical dans le layer
 this.layer = new Layer(l.name+"_VScroll");
 this.parentHeight = l.getHeight();
 this.clipTop = 0;
 this.clipBottom = h;
}

function layer_addVScroll(h,fg1,fg2){
 // h = hauteur de la barre verticale
 // fg1 = couleur de fond active
 // fg2 = couleur de fond inactive
 // �crire sur this.d un nouveau layer pour le scrolling
 sc_html  = '<div id="'+this.name+'_VScroll" style="position:absolute;top:'+(this.getTop())+'px;left:'+(this.getLeft()+this.getWidth()-20)+'px;width:10px;height:'+h+'px;visibility:visible;">';
 sc_html += '<table border=0 cellspacing=0 cellpadding=0 width=20 height='+h+'>';
 sc_html += '<tr><td valign="top" onmouseout="LayerVScrollKill();" onmouseover="LayerVScrollUp('+this.index+');" height=10 style="border-top:1px solid #FFFFFF;border-left:1px solid #FFFFFF;border-right:1px solid #000000;border-bottom:1px solid #000000;" bgcolor="'+fg1+'">&nbsp;</td></tr>';
 sc_html += '<tr><td valign="top" height="100%" bgcolor="'+fg2+'">&nbsp;</td></tr>';
 sc_html += '<tr><td valign="bottom" onmouseout="LayerVScrollKill();" onmouseover="LayerVScrollDown('+this.index+');" height=10 style="border-top:1px solid #FFFFFF;border-left:1px solid #FFFFFF;border-right:1px solid #000000;border-bottom:1px solid #000000;"bgcolor="'+fg1+'">&nbsp;</td></tr>';
 sc_html += '</table>';
 sc_html += '</div>';
 this.document.write(sc_html);
 // initialiser un objet vscroll li� � this
 this.VScrollLayer = new VScroll(this,h);
 // var s = 'rect ('+0+' auto '+h+' auto)';
 // this.layer.style.clip = s;
 this.setHeight(h);
 this.isVScroll = 1;
}

var tab_layer = new Array();

function Layer(layerName,parentLayer){
 var d=document;
 this.IE = (document.all)?1:0;
 this.NS = (document.layers)?1:0;
 this.MZ = ((navigator.appName=='Netscape')&&(parseInt(navigator.appVersion)>=5))?1:0;
 this.SHOW = (this.NS)?'show':'visible';
 this.HIDE = (this.NS)?'hide':'hidden';
 this.name = layerName;
 this.layer = (this.NS)?d.layers[layerName]:(this.MZ)?(d.getElementById(layerName))?d.getElementById(layerName):d.getElementByName(layerName):d.all[layerName];
 this.style = (this.NS)?this.layer:this.layer.style;
 this.document = (this.NS)?this.layer.document:d;
 this.getWidth = layer_getWidth;
 this.getLeft = layer_getLeft;
 this.getTop = layer_getTop;
 this.getHeight = layer_getHeight;
 this.setWidth = layer_setWidth;
 this.setLeft = layer_setLeft;
 this.setTop = layer_setTop;
 this.setHeight = layer_setHeight;
 this.isVisible = layer_isVisible;
 this.setVisible = layer_setVisible;
 this.setInvisible = layer_setInvisible;
 this.move = layer_move;
 this.size = layer_size;
 this.write = layer_write;
 this.read = layer_read;
 this.clear = layer_clear;
 this.index = tab_layer.length;
 this.parent = parentLayer;
 this.actif = false;
 tab_layer[this.index] = this;

 this.VScrollLayer = null;
 this.isVScroll = 0;
 this.addVScroll = layer_addVScroll;

}

var layerscrolltimer;

function LayerVScrollUp(LayerIndex){
 if (tab_layer[LayerIndex].VScrollLayer.clipTop>0) {
  tab_layer[LayerIndex].VScrollLayer.clipTop-=9;
  tab_layer[LayerIndex].VScrollLayer.clipBottom-=9;
  var s = 'rect ('+tab_layer[LayerIndex].VScrollLayer.clipTop+' auto '+tab_layer[LayerIndex].VScrollLayer.clipBottom+' auto)';
  tab_layer[LayerIndex].layer.style.clip = s;
  tab_layer[LayerIndex].setTop(tab_layer[LayerIndex].getTop()+9);
  layerscrolltimer = setTimeout('LayerVScrollUp('+LayerIndex+')',100);
 }
}

function LayerVScrollDown(LayerIndex){
 if (tab_layer[LayerIndex].VScrollLayer.clipTop<tab_layer[LayerIndex].VScrollLayer.parentHeight){
  tab_layer[LayerIndex].VScrollLayer.clipTop+=9;
  tab_layer[LayerIndex].VScrollLayer.clipBottom+=9;
  var s = 'rect ('+tab_layer[LayerIndex].VScrollLayer.clipTop+' auto '+tab_layer[LayerIndex].VScrollLayer.clipBottom+' auto)';
  tab_layer[LayerIndex].layer.style.clip = s;
  tab_layer[LayerIndex].setTop(tab_layer[LayerIndex].getTop()-9);
  layerscrolltimer = setTimeout('LayerVScrollDown('+LayerIndex+')',100);
 }
}

function LayerVScrollKill(){
 if (layerscrolltimer) clearTimeout(layerscrolltimer);
}

