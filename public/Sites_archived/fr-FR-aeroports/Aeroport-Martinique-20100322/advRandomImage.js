/******************************************************************************
* advRandomImage.js
*******************************************************************************

*******************************************************************************
*                                                                             *
* Copyright 2000-2002							      *
*                                                                             *
******************************************************************************/

///////////////////////////////////////////////////////////////////////////////
//
// advRandomImage Class
//
function advRandomImage()
{
	this.children = new Array();
}

advRandomImage.prototype.addChild = function(src, enlargedLink, align, width, link, newWindow, legend, brUnderImg, alt, longdesc)
{
	//alert("add image");
	var child = new advRandomImageItem(src, enlargedLink, align, width, link, newWindow, legend, brUnderImg, alt, longdesc);
	this.children[this.children.length] = child;
}

advRandomImage.prototype.dumpRandomImage = function()
{
	if (this.children.length > 0)
	{
		// calculate a random integer between 0 and this.children.length-1
		var i = Math.round( Math.random() * (this.children.length-1) );

		// get child associated to the random index
		var child = this.children[i];

		if (child == null)
		{
			alert("impossible d'afficher l'image " + i + "/" + this.children.length);
			return;
		}

		// dump HTML source code
		var html = child.getHTML();

		document.write(html);
	}
}
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// advRandomImageItem Class
//
function advRandomImageItem(src, enlargedLink, align, width, link, newWindow, legend, brUnderImg, alt, longdesc)
{
	this.src			= src;
	this.enlargedLink	= enlargedLink;
	this.align			= align;
	this.width			= width;
	this.link			= link;
	this.newWindow		= newWindow;
	this.legend			= legend;
	this.brUnderImg		= brUnderImg;
	this.alt			= alt;
	this.longdesc		= longdesc;
}

advRandomImageItem.prototype.getHTML = function()
{
	var html = "<div align='" + this.align + "'>";

	if (this.legend != "" && this.legend != null)
	{
		html += "<table border=0 cellpadding=0 cellspacing=0>"
		html += "<tr>";
		html += "<td align='center'>"
	}
	
	if (this.src != "" && this.src != null)
	{
		if (this.link != "" && this.link != null)
		{
			//html += unescape(this.link);
			html += "<a href='";
			if (this.newWindow == true || this.newWindow == "true")
				html += "' onclick='" +unescape(this.link)+ ";return false'>";
			else
				html += unescape(this.link) + "'>";
		}
		
		html += "<img";
		html += " src='" + this.src + "' border=0";
		
		if (this.width != 0 && this.width != null)
			html += " width='" + this.width + "'";

		if (this.alt != "" && this.alt != null)
		{
			html += " title=\"" + this.alt.replace(/"/g, "&quot;") + "\" alt=\"" + this.alt.replace(/"/g, "&quot;") + "\"";
		}
		
		if (this.longdesc != "" && this.longdesc != null)
		{
			html += " longdesc=\"" + this.longdesc.replace(/"/g, "&quot;") + "\"";
		}

		html += "/>";

		if (this.link != "" && this.link != null)
			html += "</a>";

		if (this.enlargedLink != "" && this.enlargedLink != null)
		{
			html += "<br/>";
			html += this.enlargedLink;
		}
	}

	if (this.legend != "" && this.legend != null)
	{
		html += "</td></tr><tr><td align='center'>"
		html += "<span class='fontFamilySpecialText fontSizeSmall fontColorLegend'>" + this.legend + "</SPAN>";
		html += "</td></tr></table>";
	}

	html += "</div>";

  if (this.brUnderImg == true)
  {
    html += "<br/>";
  }

	return html;
}
///////////////////////////////////////////////////////////////////////////////