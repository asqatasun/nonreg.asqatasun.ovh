var globalRootPath;
var tocRequiredSubsites;
var tbNames = new Array();
var tbTypes = new Array();
var tocGids = new Array();

// the TB constructor is now generated in the page to avoid course
// between .js loading and the execution of the TB function in the page
//

function TB_unused(description, children)
{
	this.children = children;
	var pairs = description.split("|");
	for(var i=0;i<pairs.length;i++) {
		var pair = pairs[i];
		var pos = pair.indexOf("=");
		if(pos < 0) continue;
		var code = parseInt(pair.substr(0,pos), 10);
		var name = tbNames[code];
		if(name == null) continue;
		var value = pair.substr(pos + 1).replace(/%7c/gi,"|").replace(/%25/gi,"%");
		switch(tbTypes[name]) {
		case "integer":
		case "date":
			if(value != "") {
				this[name] = parseInt(value, 10);
			}
			break;
		case "htmlBreak":
			this[name] = value.replace(/\n/g, "<BR>");
			break;
		default:
			this[name] = value;
		}
	}
	if(this.categories == null) this.categories = "";
	if(this.slogan == null) this.slogan = "null";
	if(this.summary == null) this.summary = "null";
	if(this.srcGID != null)	tocGids[this.srcGID] = this;
}

function TocManager(subsites, globalRootPath)
{
	this.tocs = new Array();
}

TocManager.prototype.load = function(subsites, rootPath)
{
	if(rootPath == null) rootPath = "./";
	if(subsites == null) subsites = new Array();
	var name;
	for(name in subsites) {
		var path = subsites[name];
		if(path == "") continue;
		if(path == ".") path = rootPath;
		else if(rootPath != ".") path = rootPath + path + "/";
		path += "iso_resource/toc.js";
		var scr = "<scr" + "ipt type='text/javascript' src='" + path + "'></scr" + "ipt>";
		document.write(scr);
	}
}

TocManager.prototype.getTocBlock = function(tocName, gid)
{
	var toc = this.tocs[tocName];
	if(toc == null) return;
	return toc.gids[gid];
}

TocManager.prototype.getSubsiteToc = function(tocName, lang, sid)
{
	var toc = this.tocs[tocName];
	if(toc == null) return;
	var array = toc[lang];
	if(array == null) return;
	return array[sid];
}

var tocManager = new TocManager(tocRequiredSubsites, globalRootPath);
tocManager.load(tocRequiredSubsites, globalRootPath);