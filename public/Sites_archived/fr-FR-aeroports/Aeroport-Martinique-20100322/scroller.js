///////////////////////////////////////////////////////////////////////////////////////////
// script for scrolling
///////////////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CONSTRUCTOR FOR SCROLLER OBJECT
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
function Scroller (id, width, height, border, speed, nbItems)
{
	this.id		= id;
	this.created	= false;
	
	this.scrollLayer = document.getElementById("scrollLayer"+id);
	
	if ( this.scrollLayer == null )
	{
		alert("error while trying to initialize scroller object id=" + id);
		return;
	}

	this.scrollLayer.style.clip = "rect(0px," + (width - 2*border) + "px," + (height - 2*border) + "px,0px)";
	this.scrollLayer.style.height	= nbItems * height;
	var scrollSpace = document.getElementById("scrollSpace"+id);
	if(scrollSpace != null) {
		scrollSpace.style.height = nbItems * height;
	} 

	this.width	= width;
	this.height	= height;
	this.speed	= speed;
	this.pauseTime	= 2000;

	// init scrolling variables :
	this.stopped	= false;
	this.currentY	= 0;
	this.stepY	= speed / (1000 / scrollerInterval);
	this.stepY	= Math.min(height, this.stepY);
	this.nextY	= height;
	this.maxY	= height * (nbItems - 1);
	this.paused	= true;
	this.counter	= 0;

	// register this scroller in the list of all scroller
	scrollerList[scrollerList.length] = this;
	// On first scroller, start interval timer.
	if (scrollerList.length == 1)
		setInterval('scrollerGo()', scrollerInterval);
	
	// mark this scroller as created so it can start to scroll
	this.created = true;
}

Scroller.prototype.stop = function()
{
	this.stopped = true;
}

Scroller.prototype.start = function()
{
	this.stopped = false;
}
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CODE FOR SCROLLING
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

// An array is used to hold a pointer to each scroller that is defined. The
// scrollerGo() function runs at regular intervals (defined by scrollerInterval)
// and updates each scroller in this list.
var scrollerList  = new Array();
var scrollerInterval = 20;

// scrollerGo is called by the first scroller constructor
function scrollerGo()
{
	// look each scroller object in the list.
	var i;
	for (i = 0; i < scrollerList.length; i++)
	{
		var scroller = scrollerList[i];
		
		// If not created yet or stopped, skip.
		if (scroller.created != true || scroller.stopped == true);
		// If paused, update counter.
		else if (scroller.paused == true) 
		{
			scroller.counter += scrollerInterval;
			if (scroller.counter > scroller.pauseTime)
				scroller.paused = false;
		}
		// Else scroll it !
		else 
		{
			scroller.currentY += scroller.stepY;

			// Pause it if the next item has scrolled into view.
			if (scroller.currentY >= scroller.nextY)
			{
				scroller.paused		 = true;
				scroller.counter	 = 0;
				scroller.currentY	 = scroller.nextY;
				scroller.nextY		+= scroller.height;
			}

			// When we reach the end, start over.
			if (scroller.currentY >= scroller.maxY)
			{
				scroller.currentY	-= scroller.maxY;
				scroller.nextY		 = scroller.height;
			}

			scrollLayerTo(scroller.scrollLayer, 0, Math.round(scroller.currentY));
		}
	}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// UTILS OBJECT AND FUNCTIONS
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//-----------------------------------------------------------------------------
// Rect object
//-----------------------------------------------------------------------------
function ClipRect(a1, a2, a3, a4)
{
	this.top	= 0;
	this.right	= 0;
	this.bottom	= 0;
	this.left	= 0;

	// first arg may looks like : rect(top, right, bottom, left)
	if (typeof(a1) == 'string')
	{
		var val;
		var ca;
		var i;

		if (a1.indexOf('rect(') == 0)
		{
			ca = a1.substring(5, a1.length-1).split(' ');

			for (i = 0; i < 4; ++i)
			{
				val = parseInt(ca[i]);
				if (val != 0 && ca[i].indexOf('px') == -1)
				{
					alert('The clipping region ' + a1 + ' do not use pixels as units.');
					return;
				}
				ca[i] = val;
			}
			
			this.top	= ca[0];
			this.right	= ca[1];
			this.bottom	= ca[2];
			this.left	= ca[3];
		}
	}
	else if (typeof(a1) == 'number' && typeof(a2) == 'number' && typeof(a3) == 'number' && typeof(a4) == 'number')
	{
		this.top	= a1;
		this.right	= a2;
		this.bottom	= a3;
		this.left	= a4;
	}
}


//-----------------------------------------------------------------------------
// Layer scrolling utilities
//-----------------------------------------------------------------------------
function scrollLayerTo(layer, x, y)
{
	var clip = new ClipRect(layer.style.clip);

	var dx = clip.left - x;
	var dy = clip.top - y;

	scrollLayerBy(layer, x - clip.left , y-clip.top);
}

function scrollLayerBy(layer, dx, dy)
{
	var clip = new ClipRect(layer.style.clip);

	var clipStr = "rect(" + (clip.top + dy) + "px " + (clip.right + dx) + "px " + (clip.bottom + dy) + "px " + (clip.left + dx) + "px )";

	layer.style.clip = clipStr;

	var x = getPageX(layer)-dx;

	setPageX(layer, x);
	setPageY(layer, getPageY(layer)-dy);
}

function getPageX(layer)
{
	var x	= 0;
	var elm	= layer;

	while (elm)
	{
		x	+= elm.offsetLeft;
		elm	 = elm.offsetParent;
	}

	return x;
}		

function setPageX(layer, x)
{
	var xParent	= 0;
	var elm		= layer.offsetParent;

	while (elm)
	{
		xParent	+= elm.offsetLeft;
		elm	 = elm.offsetParent;
	}

	layer.style.left = (x - xParent) + "px";
}

function getPageY(layer)
{
	var y	= 0;
	var elm	= layer;

	while (elm)
	{
		y	+= elm.offsetTop;
		elm	 = elm.offsetParent;
	}

	return y;
}

function setPageY(layer, y)
{
	var yParent	= 0;
	var elm		= layer.offsetParent;

	while (elm)
	{
		yParent	+= elm.offsetTop;
		elm	 = elm.offsetParent;
	}

	layer.style.top	= (y - yParent) + "px";
}


