var seo = function(swfaddress, base) {
    var hash = location.href.indexOf('#');
    if (swfaddress != '/' && (hash == -1 || hash == location.href.length - 1)) {
        var xhr;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        } else if (window.ActiveXObject) {
            try {
                xhr = new ActiveXObject('Msxml2.XMLHTTP');
            } catch(e) {
                xhr = new ActiveXObject('Microsoft.XMLHTTP');
            }
        }
        xhr.open('get', ((typeof base != 'undefined') ? base : '') + '/?' + swfaddress, false);                
        xhr.setRequestHeader('Content-Type', 'application/x-swfaddress');
        xhr.send('');
        eval(xhr.responseText);
    }
}