/*
 * Visa Gallery Javascript
 *
 * Orig. Author : Michel Ypma
 * Date         : 12-10-2007
 *
 *
 * Documentation:
 *   This js works on 'a' elements. It requires 2 properties.
 *   1.  add a classname 'visa_gallery' to it. So it can be found/initialized by this script.
 *   2.  add a 'href' attribute to it. This attribute needs to point to the xml
 *
 *   There is also a third and optional property.
 *   3.  add an 'id' attribute to the 'a' element. This attribute gives the id for the first image to be shown
 *
 * Example without id:
 *   <a class="visa_gallery" href="./xml?id=idXml">click</span>
 *
 * Example with id:
 *   <a id="image_1" class="visa_gallery" href="./xml?id=idXml">click</span>
 *
 * CSS:
 *   This piece of CSS needs to be included on the page for the overlay and flashwrapper:
 *
 *   #gallery_window {
 *       position:fixed;
 *       *position:absolute;
 *       width:100%;
 *       height:100%;
 *       z-index:100;
 *       left:0;
 *       top:0;
 *    }
 *
 * Notes:
 *   - This js requires SWF Object to function
 *   - The gallery.swf is placed in the assets/swf folder.
 */

/**
 * Opens the overlay and loads the flash in the browser.
 *
 * @param _xml location of the xml file
 * @param _id  id of the first image to be shown
 */
function GalleryShow( _xml, _id )
{
    // Get base href
    var baseHref = $('base').attr('href');

    // Setup overlay
    GalleryOverlay();

    // Show overlay
    $('#gallery_window').css('display', 'block');

    // Load and show flash
    var so = new SWFObject("./assets/swf/gallery.swf", "gallery_window", "990", "760", "8", "#000000");
    so.addVariable        ("xmlFile",       _xml);
    so.addVariable        ("firstPicture", _id);
    so.addVariable        ("baseHref",     baseHref );
    so.addParam           ('wmode',        'transparent');
    so.write              ("gallery_window");

    // Bind events
    $(window).bind        ('resize', GalleryOverlay);
    $(window).bind        ('scroll', GalleryOverlay);

    $(document).keyup( function(event) {
        if (event.keyCode == 27) { // esc key
            GalleryClose();
        }
    });
};

/**
 * Closes the overlay and unloads the flash in the browser.
 */
function GalleryClose()
{
  $('#gallery_window').remove('embed');
  $('#gallery_window').empty ();
  $('#gallery_window').css   ('display', 'none');

  $('html').css('overflow', 'auto');
  $(window).unbind('resize', GalleryOverlay);
};

/**
 * Setup the overlay
 */
function GalleryOverlay()
{
    $('#gallery_window').css('height', '100%');
    $('#gallery_window').css('width',  '100%');
    
    if (jQuery.browser.msie && jQuery.browser.version.indexOf('7') > -1) {
        $('#gallery_window').css('top', document.documentElement.scrollTop);

    } else if(jQuery.browser.version.indexOf('6') > -1) {
        $('#gallery_window').css('position', 'absolute');
	$('html').css('overflow', 'hidden');
    }
};

/**
 * Initializes all gallery elements (when page is ready with loading).
 */
$(document).ready( function()
{
    /* Temp not available in firefox 2 on mac (it crashes)
     * JP: Fixed now
    if( /Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent) ) {
        var ffversion = new Number(RegExp.$1);
        if( ffversion < 3 && navigator.platform.indexOf("Mac")> -1 ) {
            $('a.visa_gallery').parent().parent().remove();
        }
    } */

    // Bind click events
    $('a.visa_gallery').click( function()
    {
        var xml = $(this).attr('href');
        var id  = $(this).attr('id');

        // Stripping 'image_' from id
        if (id != null && id.length > 0) {
            id = id.substr(6);
        }

        GalleryShow(xml, id);

        return false;
    })

    // Provide gallery_window div
    $('#footer').after('<div id="gallery_window"></div>');
    $('#gallery_window').css('display', 'none');

    if (jQuery.browser.msie) {
        $('#gallery_window').css('postion', 'absolute');
    }
});
