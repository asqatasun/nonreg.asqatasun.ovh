var bas_de_page=40; // Position en bas de fenetre
var Hauteur=195; //Hauteur du div
var pos_x=0; //Position du div (horizontal)
var pos_y=bas_de_page; //Position du div (vertical)
var MonObjet;

function GetObject(ID) {
 if (document.getElementById) {
 return document.getElementById(ID);
 }
 if (document.layers) {
 return eval('document.'+ID);
 }
 if (document.all) {
 return eval('document.all.'+ID);
 }
}

function MoveTo(MyObject, x, y) { // Déplacement du DIV
 if (document.getElementById||document.all) {
 MyObject.style.top = y + "px";
 MyObject.style.left = x + "px";

 return;
 }
 if (document.layers) MyObject.moveTo(x, y);
}

function InitObjet(ID) { //Initialisation du DIV
 MonObjet = GetObject(ID);
 MoveTo(MonObjet, pos_x, bas_de_page);
 scroll();
}

function scroll() { // Défilement du DIV
 if (pos_y > (-1 * Hauteur)) { //Teste si le DIV est completement sorti
 pos_y--;
 MoveTo(MonObjet, pos_x, pos_y)
 }
 else {
 pos_y=bas_de_page;
 MoveTo(MonObjet, pos_x, pos_y)
 }
 var timer = setTimeout('scroll();',120);
}

$(document).ready(function(){
	 
	/* lance le script en "non-intrusif */
	/*-----------------------------------------------------------------------------*/
	$(function(){
		InitObjet('breve');
	});
});