jQuery('#stratis_newsletter_subscription').submit(function(event) {
    executeSubmit = true;
    jQuery('#stratis_newsletter_subscription').find('.error_field').hide();
    if (!jQuery('#fe_ttaddress_last_name_input').val()) {
        jQuery('#fe_ttaddress_last_name_input').parent().find('.error_field.required').show();
        executeSubmit = false;
    }
    if (!jQuery('#fe_ttaddress_first_name_input').val()) {
        jQuery('#fe_ttaddress_first_name_input').parent().find('.error_field.required').show();
        executeSubmit = false;
    }
    if (!jQuery('#fe_ttaddress_first_name_input').val()) {
        jQuery('#fe_ttaddress_first_name_input').parent().find('.error_field.required').show();
        executeSubmit = false;
    }
    if (!jQuery('#fe_ttaddress_zip_input').val()) {
        jQuery('#fe_ttaddress_zip_input').parent().find('.error_field.required').show();
        executeSubmit = false;
    }
    if (!jQuery('#email').val()) {
        jQuery('#email').parent().find('.error_field.required').show();
        executeSubmit = false;
    } else {
        var re = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        if (!re.test(jQuery('#email').val())) {
            jQuery('#email').parent().find('.error_field.mail').show();
            executeSubmit = false;
        }
    }
    if (!executeSubmit) {
        event.preventDefault();
    }
    return true;
});

jQuery('#stratis_newsletter_unsubscription').submit(function(event) {
    executeSubmit = true;
    jQuery('#stratis_newsletter_subscription').find('.error_field').hide();
    if (!jQuery('#fe_ttaddress_email_input').val()) {
        jQuery('#fe_ttaddress_email_input').parent().find('.error_field.required').show();
        executeSubmit = false;
    } else {
        var re = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        if (!re.test(jQuery('#fe_ttaddress_email_input').val())) {
            jQuery('#fe_ttaddress_email_input').parent().find('.error_field.mail').show();
            executeSubmit = false;
        }
    }
    if (!executeSubmit) {
        event.preventDefault();
    } else {
        if (!jQuery('#newsList').is(':visible') || !jQuery('#thematiqueList').is(':visible')) {
            jQuery.ajax({
                    'url': currentSubscriptionUrl,
                    'method': 'POST',
                    'dataType': "json",
                    'data': {
                        'email': jQuery('#fe_ttaddress_email_input').val(),
                        'tx_stratisnewslettersubscription_subscription[action]': 'usersubscription'
                    },
                    'success': function (response) {
                        jQuery('#newsList').show();
                        jQuery('#thematiqueList').show();
                        if (typeof response.lists !== 'undefined' ) {
                            jQuery.each(response.lists, function (k, list) {
                                if(list.status === '1' && jQuery('.userSubscriptions input[type=checkbox][value=' + k + ']').length) {
                                    jQuery('.userSubscriptions input[type=checkbox][value=' + k + ']').attr('checked', 'checked');
                                }
                            });
                        }
                    }
                }
            );
            event.preventDefault();
        }
    }
});
