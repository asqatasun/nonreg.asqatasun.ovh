// -*- c-mode -*- 

function select_all ( classname, ref ) {
  for ( var i = 0; i < document.getElementsByTagName('input').length; i++ ) {
    if ( document.getElementsByTagName('input')[i].className == classname )
      {
	document.getElementsByTagName('input')[i].checked = ref.checked;
      }
  }
}


function showhidestuff(boxid) {
   box = document.getElementById(boxid);
   switch (box.style.visibility) {
      case '': box.style.visibility='visible'; break
      case 'hidden': box.style.visibility='visible'; break
      case 'visible': box.style.visibility='hidden'; break
   }
   switch (box.style.display) {
      case '': box.style.display='block'; break
      case 'none': box.style.display='block'; break
      case 'block': box.style.display='none'; break
   }
}

function hidestuff ( boxid ) {
  box = document.getElementById ( boxid );
  box.style.display='none'; 
  box.style.visibility='hidden';
}


function showstuff ( boxid ) {
  box = document.getElementById ( boxid );
  box.style.display='block'; 
  box.style.visibility='visible';
}


function submit_form ()
{
  // Be sure that *all* form elements are not disabled so that they
  // are submited as well.
  for ( var i = 0 ; i < document.search_form.length ; i ++ )
    {
      document.search_form.elements [ i ] . disabled = false;
    }

  $("form[name='search_form']").append('<input type="hidden" name="dont_do_it" value="1">');

  // Submit form to be sure all elements are up to date.
  document.search_form.submit();
}



var active;
var last_color;
function change_color ( objectid )
{
    object = document.getElementById ( objectid );
    if ( object )
    {
	if ( active ) 
	{
	    active.style.background = last_color;
	    active.style.border = '0px';
	}
	active = object;
	last_color = object.style.background;
	object.style.background = '#fce94f';
	object.style.border = '1px solid #c4a000;';
  }
}



function insert_at_cursor ( fieldid, value )
{
    field = document.getElementById ( fieldid );

    if ( document.selection ) {
	field.focus();

	sel = document.selection.createRange();
	sel.text = value;
    }

    else if ( field.selectionStart || field.selectionStart == '0' )
    {
	var startPos = field.selectionStart;
	var endPos = field.selectionEnd;
	field.value = field.value.substring ( 0, startPos ) 
	    + '{$' + value + '}' + field.value.substring ( endPos, field.value.length );
    } 
    else
    {
	field.value += value;
    }

    field.focus();
} 


function makepassword(inputid,generate) {
    input = document.getElementById(inputid);
    if ( ! generate )
	value = input.value;
    else
    {
	value = 1e6 * Math.random();
	value = value.toString ();
    }
    input.value = SHA1(value);
    if ( input.disabled )
	input.disabled = 0;
}


function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
	var c = ca[i];
	while (c.charAt(0) == ' ') {
	    c = c.substring(1);
	}
	if (c.indexOf(name) == 0) {
	    return c.substring(name.length, c.length);
	}
    }
    return "";
} 


function ptab_onclick ( i )
{
    SetCookie ( 'ptab', i );
}


function SetCookie (name, value)
{
    var aujourdhui = new Date() ;
    var expdate = new Date() ;
    expdate.setTime( aujourdhui.getTime() + ( 365*24*60*60*1000 ) )
    document.cookie = name + "=" + value + ";expires=" + expdate.toGMTString() ;
}




function secondsToString(seconds)
{
    var numhours = Math.floor(((seconds % 31536000) % 86400) / 3600);
    var numminutes = Math.floor((((seconds % 31536000) % 86400) % 3600) / 60);
    var numseconds = (((seconds % 31536000) % 86400) % 3600) % 60;
    return ( numhours ? numhours + " heure" + ( numhours>1?'s ':' ')  : '' ) +
	( numminutes ? numminutes + " minute" + (numminutes>1?'s ':' ') :  '' ) +
	numseconds + " seconde" + ( numseconds>1?'s':'');
}




/* Local Variables: */
/* c-basic-offset: 4 */
/* End: */
