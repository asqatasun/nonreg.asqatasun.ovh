/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
  // Store aliases for common key codes that we'll be using
  var keys = {
    tab:    9,
    enter:  13,
    esc:    27,
    space:  32,
    up:     38,
    down:   40,
    home:   36,
    end:    35,
  };

  /**
   * Are we on desktop view right now?
   */
  var isDesktop = function () {
    return window.matchMedia("(min-width: 950px)").matches;
  };

  /**
   * Check if a Web-One collapsible element is collapsed.
   *
   * @param element
   *
   * @returns {boolean} True if the given element is collapsed
   */
  function wo_isCollapsed(element) {
    var $el = (element instanceof jQuery) ? element : $(element);

    if (!$el.hasClass('wo-collapsible')) {
      return false;
    }

    if ($el.hasClass('wo-collapsed--mobile') && !isDesktop()) {
      return true;
    }

    if ($el.hasClass('wo-collapsed--desktop') && isDesktop()) {
      return true;
    }

    return false;
  }

  /**
   * Toggle a Web-One collapsible element.
   *
   * @param element
   */
  function wo_toggleCollapsible(element) {
    var $el = (element instanceof jQuery) ? element : $(element);
    var isExpanding = wo_isCollapsed($el);

    $el.addClass('wo-collapsing');

    $el.attr('aria-hidden', function(i, val) {
      return val === 'true' ? 'false' : 'true';
    });

    $el.animate({
      height: isExpanding ? $el.get(0).scrollHeight : 0,
    }, 300, function () {
      if ($el.hasClass('wo-collapsed--mobile') || $el.hasClass('wo-expanded--mobile')) {
        $el.toggleClass('wo-collapsed--mobile wo-expanded--mobile')
      }

      if ($el.hasClass('wo-collapsed--desktop') || $el.hasClass('wo-expanded--desktop')) {
        $el.toggleClass('wo-collapsed--desktop wo-expanded--desktop')
      }

      $el.removeClass('wo-collapsing');

      $(this).css('height', '');
    });
  }

  // Fix issue with tabbing to hidden mobile only menu links
  $(document).ready(function () {
    //Force HTML5 player for Youtube embedded links with iframe
    $("iframe[src^='http://www.youtube.com/embed/']").each(function () {
      this.src += "&html5=1";
    });

    // Blog Entires to Remove (by nid) START
    $("head meta[name='entity_to_hide_nid']").each(function () {
      $nid = $(this).attr("value");
      $(
        ".node-" +
        $nid +
        ".node.node-blog.node-promoted.node-teaser.clearfix, .node-"
      ).each(function () {
        if (!$(this).hasClass("node-unpublished")) {
          $(this).remove();
        }
      });
    });
    $(
      ".node.node-blog.node-promoted.node-unpublished.node-teaser.clearfix"
    ).each(function () {
      if (!$("div").hasClass("toolbar")) {
        $(this).remove();
      }
    });
    // Blog Entires to Remove (by nid) END

    // Append gid to tags
    $(
        "div[class='field field-name-field-blog-tags field-type-taxonomy-term-reference field-label-above']"
      )
      .find("a[href^='/taxonomy/term']")
      .each(function () {
        $gid = Drupal.settings.ogContext.gid;
        $(this).attr("href", $(this).attr("href") + "?gid=" + $gid);
      });

    //Add class to allow css to fix mini cal ff issue
    $("table .mini")
      .closest(".view-content")
      .prev()
      .find(".date-heading")
      .attr("class", "mini-date-heading");

    //Script to append gid to load on the mini calendar as a hyperlink
    var headerlink = $("table .mini")
      .closest(".view-content")
      .prev()
      .find(".mini-date-heading > h2 > a")
      .attr("href");
    var gid = Drupal.settings.ogContext.gid;
    $("table .mini")
      .closest(".view-content")
      .prev()
      .find(".mini-date-heading > h2 > a")
      .attr(
        "href",
        headerlink +
        "?og_ajax_context__gid=" +
        gid +
        "&og_ajax_context__group_type=node"
      );

    var panelCount = 0;
    $(".panel-pane.pane-node, .panel-pane.pane-node-content").each(function (
      index
    ) {
      if (!$(this).hasClass("pane-entity-view")) {
        if (
          $(this).find("div.ui-accordion").length != 0 ||
          ($(this).find("article > div").length == 0 &&
            !$(this).hasClass("pane-node-content"))
        ) {
          $(this).attr("id", "accordion" + panelCount);
          panelCount++;
        }
      }
    });

    /* accordion hash-tag/anchor-tag feature */
    $(
      "h2[class='field field-name-field-title-text field-type-text field-label-hidden ui-accordion-header ui-helper-reset ui-state-default ui-corner-all']:visible"
    ).each(function (index) {
      var contentOfAccordionHeader = $.trim(
        $(this)
        .text()
        .replace("To activate tabpage press spacebar.", "")
        .replace(/ /g, "")
      );
      $(this).before("<a name='" + contentOfAccordionHeader + "'></a>");
      var urlHash = $.trim(window.location.hash).replace(/#/g, "");
      if (contentOfAccordionHeader == urlHash) {
        $(this).trigger("click");
      }
    });

    $(window).bind("hashchange", function (e) {
      $(
        "h2[class='field field-name-field-title-text field-type-text field-label-hidden ui-accordion-header ui-helper-reset ui-state-default ui-corner-all']:visible"
      ).each(function (index) {
        var contentOfAccordionHeader = $.trim(
          $(this)
          .text()
          .replace("To activate tabpage press spacebar.", "")
          .replace(/ /g, "")
        );
        $(this).before(
          "<a name='" + contentOfAccordionHeader + "'></a>"
        );
        var urlHash = $.trim(window.location.hash).replace(/#/g, "");
        if (contentOfAccordionHeader == urlHash) {
          $(this).trigger("click");
        }
      });
    });

    if (
      $(
        "div[class='ui-accordion ui-widget ui-helper-reset ui-accordion-icons']:visible"
      ).length > 1
    ) {
      $(
        "div[class='ui-accordion ui-widget ui-helper-reset ui-accordion-icons']:visible"
      ).each(function (index) {
        if (index > 0) {
          $(this)
            .detach()
            .appendTo("#accordion" + index);
        }
      });
    }

    $(".form-required")
      .parents("form")
      .before(
        "<div class='form-required' id='comment_instruct'> * Indicates a required field. </div>"
      );
    $("input[class='form-text required']:visible").each(function () {
      $(this).attr("aria-describedby", "comment_instruct");
      $(this).attr("aria-required", "true");
    });

    $("article[class*='node-blog']:visible h3.field-label").replaceWith(
      $("<h2 class='field-label'> Tags </h2>")
    );
    $(
      "article[class*='node-blog']:visible h2.comments--title.title"
    ).replaceWith($("<h3 class='comments--title title'>Comments</h3>"));

    $(
      "article[class*='node-blog']:visible #edit-comment-body-und-0-value"
    ).attr("aria-describedby", "comment_instruct");
    $(
      "article[class*='node-blog']:visible #edit-comment-body-und-0-value"
    ).attr("aria-required", "true");
    $(".blog-summary-by-tag a").each(function () {
      $(this).attr("title", $(this).text() + " tag");
    });

    // Accordion
    $("h2.ui-accordion-header").click(function () {
      accordion_open_close($(this));
    });

    $("h2.ui-accordion-header").keydown(function (e) {
      $("div.ui-accordion-content").removeAttr("tabindex");

      if (e.which == 13 || e.which == 32) {
        accordion_open_close($(this));
      }
      if (e.which == 38 || e.which == 40) {
        if ($(this).hasClass("ui-state-active")) {
          if (e.which == 38) {
            $(this)
              .get(0)
              .focus();
          }
          if (e.which == 40) {
            $("div.ui-accordion-content-active").attr(
              "tabindex",
              0
            );
            $("div.ui-accordion-content-active")
              .get(0)
              .focus();
          }
        }

        $("h2.ui-accordion-header").attr("tabindex", 0);
      }
    });

    function accordion_open_close(v) {
      // Set tabindex back to 0
      $("h2.ui-accordion-header").attr("tabindex", 0);

      if ($(v).hasClass("ui-state-default")) {
        $(v).attr("aria-expanded", "false");
        $(v)
          .next()
          .attr("aria-hidden", "true");
        // Focus bug in Chrome
        $("h2.ui-accordion-header").removeClass("ui-state-focus");
        $(v).addClass("ui-state-focus");
        $(v)
          .get(0)
          .focus();
      } else if ($(v).hasClass("ui-state-active")) {
        $("div.ui-accordion-content-active").attr(
          "aria-hidden",
          "true"
        );
        $(v)
          .next()
          .attr("aria-hidden", "false");
      }
    }
  });

  Drupal.behaviors.csunThemeLoad = {
    attach: function (context, settings) {
      $('html')
        .removeClass('no-js')
        .addClass('js')
      ;

      // Use jQuery UI Accordion.
      $accordion = $("#accordion", context);
      if ($accordion.length) {
        $accordion.accordion({
          collapsible: true,
          active: false,
          autoHeight: false
        });
      }
      // Add tr class on even/odd in wysiwyg
      $(".field-name-field-body tr:even").addClass("even");
      $(".field-name-field-body tr:odd").addClass("odd");
      // Remove tab index for accordian
      $("h2.ui-accordion-header").attr("tabindex", 0);
      $("h2.ui-accordion-header").attr("aria-expanded", "false");
      $("div.ui-accordion-content").attr("aria-hidden", "true");
      //Alen's changes
      $("div.ui-accordion").wrapAll(
        '<div id="accordion-instruction"  tabindex="0" />'
      );
      $("#accordion").prepend(
        '<span id="accordion-instruction-at-message">You have reached an accordion control. The following tabs will be activated by spacebar.</span>'
      );
      $("h2.ui-accordion-header").append(
        '<span id="accordion-header-guide-at-message">To activate tabpage press spacebar.'
      );
      //End Alen's changes
    }
  };

  Drupal.behaviors.csunSlideshow = {
    attach: function (context, settings) {
      var $slideWrapper = $(".slide-wrapper", context);
      var slideshowSize = $slideWrapper.find("figure").size();

      // If only one item, don't run flexslider.
      if (slideshowSize > 1) {
        var getActiveSlide = function() {
          return $('.flex-active-slide', $slideWrapper.parent()).first();
        };

        var allowFocusOnActiveSlide = function() {
          // Any focusable slides should be reset. These slides are only focusable
          // when the slideshow is paused with the Play/Pause button.
          var $focusableSlides = $('.slide[tabindex="0"]', $slideWrapper);
          $focusableSlides.removeAttr('tabindex');

          // Any slides that are off-screen should not be reachable via Tab
          var $tabIndices = $('.slide a[tabindex="0"]', $slideWrapper);
          $tabIndices.attr('tabindex', '-1');

          getActiveSlide().find('a').attr('tabindex', '0');
        };

        $slideWrapper
          .flexslider({
            animation: "slide",
            prevText: "Previous Slide",
            nextText: "Next Slide",
            pauseOnHover: false,
            pausePlay: true,
            keyboard: true,
            start: function() {
              allowFocusOnActiveSlide();

              // Go through all of the "Read More" links for each slide and add
              // screenreader only text to dictate what that link is going to
              // send you to.
              $('a', $slideWrapper).each(function (index, anchor) {
                var $anchor = $(anchor);

                // There may be other links in the slideshow, so just modify
                // "Read More" links
                if ($anchor.text() === 'Read More') {
                  var articleTitle = $anchor.parents('figcaption').find('.field-name-field-title-text').text().trim();
                  $anchor.html('Read More <span class="sr-only">about "' + articleTitle + '"</span>')
                }

                $anchor
                  .focusin(function() {
                    $slideWrapper.flexslider('pause');
                  })
                  .focusout(function(e) {
                    var tryingToPlay = $(e.relatedTarget).parent().hasClass('flex-pauseplay');

                    // If our focus goes to the "Play" button, don't play the
                    // slideshow through the API, let the code natively handle
                    // it
                    if (!tryingToPlay) {
                      $slideWrapper.flexslider('play');
                    }
                  })
                ;
              });
            },
            after: allowFocusOnActiveSlide,
            onPaused: function(slider, manualPause, pausedViaKeyboard) {
              // Only focus on the "Read More" link if it's a manual pause, meaning
              // a user clicked on the "Pause" button. This callback is executed
              // on API pauses and other situations.
              if (manualPause && pausedViaKeyboard) {
                $('a[tabindex="0"]', $slideWrapper).focus();
              }
            }
          })
          .removeClass("flexslider-off");

        $slideWrapper
          .find(".flex-direction-nav div:first-child")
          .after('<div class="flex-page-ctrl"></div>')
          .next()
          .append($slideWrapper.find(".flex-control-nav"))
        ;
      }
    }
  };

  //
  // Mega Menu functionality
  // ---
  // Follows the behavior described here:
  //   https://adobe-accessibility.github.io/Accessible-Mega-Menu/
  //   https://www.w3.org/TR/wai-aria-practices-1.1/examples/menu-button/menu-button-links.html
  //
  Drupal.behaviors.csunDropNav = {
    attach: function (context, settings) {
      /**
       * Set the focus to a menu item.
       *
       * @param {number} indexOrIncrement
       * @param {jQuery} $context The scope of where to look for anchors
       * @param {boolean} increment When set to true, `indexOrIncrement` is treated as an increment value rather than an index
       * @param {Element} element (Optional) When `increment` is set to true, the focus will shift relative to this item.
       */
      var setFocusMegaMenuItem = function (indexOrIncrement, $context, increment, element) {
        if (window.matchMedia("(min-width: 950px)").matches) {
          $context = $context.find('.mega-menu__item:not(.mobile-only) a');
        } else {
          $context = $context.find('.mega-menu__item a');
        }

        if (element) {
          if (increment) {
            indexOrIncrement = $context.index(element) + 1;
          } else {
            indexOrIncrement = $context.index(element) - 1;
          }
        }

        if (indexOrIncrement < 0) {
          $context.last().focus();
        } else if (indexOrIncrement >= $context.length) {
          $context.first().focus();
        } else {
          $context.get(indexOrIncrement).focus();
        }
      };

      /**
       * Close every mega menu that may be open.
       */
      var closeMegaMenu = function () {
        // Mega menu behaves differently on mobile with explicit clicks, so don't
        // automatically close things.
        if (!isDesktop()) {
          return;
        }

        // When a new "top level" menu item is focused, close any open mega menu
        var $target = $('.menu--leaf.hovered');

        $target
          .removeClass('hovered')
          .find('.menu--link')
          .attr('aria-expanded', 'false')
        ;
      };

      /**
       * Toggle a boolean as a string; useful for HTML attributes.
       *
       * @param {number} index The index position of the element in the set
       * @param {string} attr  The old attribute value
       *
       * @returns {string} The new attribute value
       */
      var toggleStringBoolean = function(index, attr) {
          return attr === 'true' ? 'false' : 'true';
      };

      // If we're expanding from the mobile view to the desktop view, close the
      // mega menu in case it is open.
      $(window).resize(function () {
        if (isDesktop()) {
          closeMegaMenu();
        }
      });

      // Our "top level" menu items should have these attributes. Only apply ARIA
      // popup-related tags to menu items with children.
      $('.menu--leaf.mega-menu__trigger > a')
        .mouseenter(function () {
          if (!isDesktop()) {
            return;
          }

          $(this).attr('aria-expanded', 'true');
        })
        .mouseleave(function () {
          if (!isDesktop()) {
            return;
          }

          $(this).attr('aria-expanded', 'false');
        })
      ;

      // This is the container for the grey sub menu of a top level navigation
      // menu. When we're hovering over this, the tied top level nav should have
      // it's `aria-expanded` updated accordingly.
      $('.mega-menu__wrapper')
        .mouseenter(function () {
          if (!isDesktop()) {
            return;
          }

          $(this).prev().attr('aria-expanded', 'true');
        })
        .mouseleave(function () {
          if (!isDesktop()) {
            return;
          }

          $(this).prev().attr('aria-expanded', 'false');
        })
      ;

      // Duplicate our "top level" menu items into the mega submenus so they're
      // accessible on our mobile menu
      $('.menu-block-custom-primary-links .menu--leaf > a').each(function (key, elem) {
        var $this = $(elem);
        var href = $this.attr('href');

        if (href === '/') {
          return;
        }

        var output = '<li class="mega-menu__item mobile-only"><a href="' + href + '" class="mega-menu__link">' + $this.text() + ' Main</a></li>';

        var $targetList = $this.parent().find('.mega-menu__column-list').first();
        $targetList.prepend(output);
      });

      // Whenever a new top level menu item takes focus, close anything that's
      // open. This also works when a menu item without a submenu is focused on.
      $('.menu--link').focusin(closeMegaMenu);

      // Add some custom functionality to the actual mega menu items
      $('.mega-menu__link')
        .bind('keydown', function (e) {
          var keysToListenTo = [
            keys.esc,
            keys.up,
            keys.down,
            keys.home,
            keys.end,
          ];

          // Don't intercept any of the other keys
          if (keysToListenTo.indexOf(e.keyCode) < 0) {
            return;
          }

          var $this = $(this);

          // Links inside our mega menu should support escaping out of the mega menu
          if (e.keyCode === keys.esc) {
            // Set the focus back on the top level menu item when we escape out of
            // opened mega menu
            var $parentMenuItem = $this.parents('.menu--leaf');
            $parentMenuItem.removeClass('hovered');
            $parentMenuItem.find('.menu--link').attr('aria-expanded', 'false');
            $parentMenuItem.find('a').first().focus();
          }
          // Support for up/down arrow key navigation
          else if (e.keyCode === keys.up || e.keyCode === keys.down) {
            e.preventDefault();

            var $siblingLinks = $this.parents('.mega-menu');
            setFocusMegaMenuItem(1, $siblingLinks, (e.keyCode !== keys.up), this);
          }
          // Support Home and End keys
          else if (e.keyCode === keys.home || e.keyCode === keys.end) {
            e.preventDefault();

            var $siblingLinks = $this.parents('.mega-menu');

            if (e.keyCode === keys.home) {
              setFocusMegaMenuItem(0, $siblingLinks);
            }
            else {
              setFocusMegaMenuItem(-1, $siblingLinks);
            }
          }
        })
        .focusout(function (e) {
          var $newFocus = $(e.relatedTarget);

          // When our mega menu loses focus and moves on to another element, close it
          if (!$newFocus.hasClass('mega-menu__link') && !$newFocus.hasClass('menu--link')) {
            closeMegaMenu();
          }
        })
      ;

      $(".mega-menu__trigger > a", context)
        .bind('keydown', function (e) {
          // "Top level" menu links support up, down, and escape keys to open/close
          // the mega menu

          var keysToListenTo = [
            keys.esc,
            keys.space,
            keys.up,
            keys.down,
          ];

          // Don't intercept any of the other keys
          if (keysToListenTo.indexOf(e.keyCode) < 0) {
            return;
          }

          e.preventDefault();

          var $topLevelMenuItem = $(this).parent();

          if (e.keyCode === keys.space) {
            // Close the menu if it's active and you hit space
            if ($topLevelMenuItem.hasClass('hovered')) {
              $topLevelMenuItem.removeClass('hovered');
              $topLevelMenuItem.find('.menu--link').attr('aria-expanded', 'false');
            }
            // Open the menu and set focus on the first menu item
            else {
              $topLevelMenuItem.addClass('hovered');
              $topLevelMenuItem.find('.menu--link').attr('aria-expanded', 'true');

              setFocusMegaMenuItem(0, $topLevelMenuItem);
            }
          }
          else if (e.keyCode === keys.down) {
            $topLevelMenuItem.addClass('hovered');
            $topLevelMenuItem.find('.menu--link').attr('aria-expanded', 'true');

            setFocusMegaMenuItem(0, $topLevelMenuItem);
          }
          else if (e.keyCode === keys.up) {
            // If it's open, close the mega menu
            if ($topLevelMenuItem.hasClass('hovered')) {
              $topLevelMenuItem.remove('hovered');
              $topLevelMenuItem.find('.menu--link').attr('aria-expanded', 'false');
            }
            // Open the mega menu and set the focus to the last menu item
            else {
              $topLevelMenuItem.addClass('hovered');
              $topLevelMenuItem.find('.menu--link').attr('aria-expanded', 'true');

              setFocusMegaMenuItem(-1, $topLevelMenuItem);
            }
          }
          else if (e.keyCode === keys.esc) {
            $topLevelMenuItem.removeClass('hovered');
            $topLevelMenuItem.find('.menu--link').attr('aria-expanded', 'true');
          }
        })
        .click(function (e) {
          // Only function on mobile breakpoint
          if (isDesktop()) {
            return;
          }

          e.preventDefault();

          var $this = $(e.target);

          // Hide all other open menus
          $(".mega-menu__trigger.hovered").each(function (key, elem) {
            if (elem !== $this.parent().get(0)) {
              $(elem)
                .removeClass('hovered')
                .attr('aria-expanded', 'false')
              ;
            }
          });

          // Toggle all other expanded icons
          $('.menu--link.expanded-open').each(function (key, elem) {
            if (elem !== $this.get(0)) {
              $(elem).removeClass('expanded-open');
            }
          });

          $this
            .toggleClass('expanded-open')
            .attr('aria-expanded', toggleStringBoolean)
          ;

          var $parent = $this.parent();
          $parent.toggleClass('hovered');
        })
      ;

      //
      // Mobile menu toggle button aka "the hamburger menu"
      //

      $('.nav-dropdown__label', context).click(function(e) {
        e.preventDefault();

        // Find the links wrapper. Using a wrapper since there are multiple menus.
        // toggle the links open or close. Currently no animation being used.
        $('.nav-dropdown__container', context).slideToggle();
        $('.layout-csun--navbar').slideToggle();

        $(this)
          .toggleClass('active expanded-open')
          .attr('aria-expanded', toggleStringBoolean)
          .attr('aria-pressed', toggleStringBoolean)
        ;
      });
    }
  };

  Drupal.behaviors.csunCollapsible = {
    attach: function (context, settings) {
      var $collapseControls = $('[data-role="wo-collapsible-control"]', context);

      $collapseControls.each(function (index, el) {
        var $el = $(el);
        var target = $el.attr('data-target');
        var $target = target === '@sibling' ? $el.next('.wo-collapsible') : $(target);

        //
        // Accessibility visibility handling
        //

        /** Handle setting `aria-hidden` on elements that are collapsible. */
        var ariaHiddenTarget = function () {
          $target.attr('aria-hidden', function() {
            return wo_isCollapsed(this) ? 'true' : 'false';
          });
        };

        ariaHiddenTarget();

        $(window).resize(function () {
          ariaHiddenTarget();
        });

        //
        // Toggling functionality
        //

        var toggle = function(event) {
          event.preventDefault();

          if ($el.attr('aria-expanded')) {
            $el.attr('aria-expanded', function (idx, value) {
              return value === 'true' ? 'false' : 'true';
            });
          }

          wo_toggleCollapsible($target);
        };

        $el
          .click(function (event) {
            toggle(event);
          })
          .keydown(function (event) {
            if (event.which !== keys.enter && event.which !== keys.space) {
              return;
            }

            toggle(event);
          })
        ;
      });
    },
  };

  // Create a behavior for the sidebar accordions.
  Drupal.behaviors.csunSidebarAccordion = {
    attach: function (context, settings) {
      var $accordion = $(".sidebar .pane-title");

      $.each($accordion, function () {
        $(this)
          .nextAll("div")
          .wrapAll('<div class="accordio-wrapper" />');
      });

      $accordion.click(function (e) {
        $(e.target)
          .filter(".accordion")
          .toggleClass("expanded-open")
          .next("div")
          .slideToggle();
      });

      $(window)
        .bind("orientationchange resize", function (e) {
          $accordion.removeClass("expanded-open");
          if ($(window).width() <= 933) {
            $accordion
              .addClass("accordion")
              .next("div")
              .slideUp();
          } else {
            $accordion
              .removeClass("accordion")
              .next("div")
              .slideDown();
          }
        })
        .trigger("resize");
    }
  };
})(jQuery, Drupal, this, this.document);
;
function openPortal() {
  jQuery("#CollapsiblePanelContent1").slideDown("fast");
  window.location.hash = "CollapsiblePanel1";
  popCasBox();
}

function gotoPortal() {
  jQuery("#CollapsiblePanelContent1").slideToggle("slow");
  window.location.hash = "CollapsiblePanel1";
  popCasBox();
}

function popCasBox() {
  o = document.getElementById("csunPortalLoginFrame");
  if (o) {
    o.src =
      "https://auth.csun.edu/cas/login?service=https://mynorthridge.csun.edu/psp/PANRPRD/?cmd=login&languageCd=ENG&embedform=true";
  }
  document.getElementById("userID").focus();
}
;
