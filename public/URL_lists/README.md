# List of URLS

## Description of lists

### Directory `Unique-domains`

This directory contains a single list of unique domains.

### Directory `Unique-pages`

This directory holds text files containing URLs. All URLs are unique in a file.
Actually there are three files:

* `LIST_URLs_01.txt` containing about 28'000 unique URLs
* `LIST_URLs_02.txt` containing about 10'900 unique URLs
* `LIST_URLs_03.txt` containing about 243 unique URLs
* `LIST_URLs_04.txt` containing about 5'500 unique URLs
* `LIST_URLs_05_Glenans.txt` containing about 380 unique URLs

## Export URLs from an Asqatasun instance

How to export already audited URLs from an Asqatasun instance

```bash
# Dump de la table WEB_RESOURCE
mysqldump --user=root asqatasun WEB_RESOURCE  > dump_WEB_RESOURCE.sql
```

```sql
-- Export CSV de la table WEB_RESOURCE
SELECT t.*
FROM asqatasun.WEB_RESOURCE t
  INTO OUTFILE '/tmp/asqatasun_WEB_RESOURCE.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n';

-- Export CSV de toutes les URLs de la table WEB_RESOURCE
SELECT t.url
FROM asqatasun.WEB_RESOURCE t
ORDER BY t.url ASC
  INTO OUTFILE '/tmp/asqatasun_WEB_RESOURCE_all-URL.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n';

-- Export CSV des URLs de la table WEB_RESOURCE
--      -> sans les URLs en doublons
--      -> sans les audits de fichier
--      -> sans les URL vers les fichiers "robots.txt"
SELECT DISTINCT t.url
FROM asqatasun.WEB_RESOURCE t
WHERE (
    t.url LIKE ('http://%')
    OR t.url LIKE ('https://%')
  )
  AND t.url NOT LIKE ('%robots.txt')
ORDER BY t.url ASC
  INTO OUTFILE '/tmp/asqatasun_WEB_RESOURCE_clean-url.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n';

-- Export CSV des URLs de la table WEB_RESOURCE
--      -> sans les URLs en doublons
--      -> sans les audits de fichier
--      -> sans les URLs vers les fichiers "robots.txt"
--      -> sans les URLs des sites web Adullact et Asqatasun
SELECT DISTINCT t.url
FROM asqatasun.WEB_RESOURCE t
WHERE (
    t.url LIKE ('http://%')
    OR t.url LIKE ('https://%')
  )
  AND t.url NOT LIKE ('%robots.txt')
  AND t.url NOT LIKE ('%asqatasun%')
  AND t.url NOT LIKE ('http://localhost%')
  AND t.url NOT LIKE ('https://localhost%')
  AND t.url NOT LIKE ('http://comptoir-du-libre.org%')
  AND t.url NOT LIKE ('https://comptoir-du-libre.org%')
  AND t.url NOT LIKE ('http://www.comptoir-du-libre.org%')
  AND t.url NOT LIKE ('https://www.comptoir-du-libre.org%')
  AND t.url NOT LIKE ('http://adullact.org%')
  AND t.url NOT LIKE ('https://adullact.org%')
  AND t.url NOT LIKE ('http://www.adullact.org%')
  AND t.url NOT LIKE ('https://www.adullact.org%')
  AND t.url NOT LIKE ('http://territoire-numerique-libre.org%')
  AND t.url NOT LIKE ('https://territoire-numerique-libre.org%')
  AND t.url NOT LIKE ('http://congres.adullact.org%')
  AND t.url NOT LIKE ('https://congres.adullact.org%')
  AND t.url NOT LIKE ('http://depnot.ovh.adullact.org%')
ORDER BY t.url ASC
  INTO OUTFILE '/tmp/asqatasun_WEB_RESOURCE_clean-url_without-AdullactWebsites.csv'
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n';

```
